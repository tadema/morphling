#!/bin/bash
#启动db_exporter脚本
# db_exporter_path
exporter_dir=$(cd `dirname $0`;pwd)
db_exporter_path=$exporter_dir/db_exporter
source $exporter_dir/DBExporter.properties

if [ ! -d "$db_exporter_path" ]; then
        mkdir -p $db_exporter_path; 
fi

if [ -d "$db_exporter_path" ]; then
       cd $db_exporter_path
else 
        echo "db_exporter dir is not found ..."
        exit -1
fi
server_type="$1"
db_user_name="$2"
db_passwd="$3"
#192.168.174.17:1521/ora11g 
host_name="$4"
db_exporter_port="$5"

if [ "$server_type" = $ORACLE_NAME ]; then
        oracle_client_path=$db_exporter_path/instantclient_18_5
        oracle_exporter_path=$db_exporter_path/oracledb_exporter
         echo "start oracle exporter ..."

        export LD_LIBRARY_PATH=$oracle_client_path
        export DATA_SOURCE_NAME=$db_user_name/$db_passwd@$host_name
        export TNS_ADMIN=$oracle_client_path/network/admin

        cd $oracle_exporter_path
        #start exporter 
        pid=$(ps -ef |grep $oracledb_exporter |grep  "$db_exporter_port"  |awk '{print $2}' | xargs)
        if [ "$pid" ]; then
            echo "export 进程已经存在"
        fi
        #默认$db_exporter_port=9161
        ./oracledb_exporter -log.level error -web.listen-address ":$db_exporter_port" > oracledb_exporter.log 2>&1 &
        #ps -ef|grep oracledb_exporter |grep 9161
        str="start oracle exporter port : $db_exporter_port"
         echo -e "\033[32m ${str}\033[0m"

elif [ $server_type = $MYSQL_NAME ]; then
        cd $mysqld_exporter
        #export DATA_SOURCE_NAME='user:password@(hostname:3306)/'
        #export DATA_SOURCE_NAME='root:root@(192.168.174.21:3306)/morphling'
        export DATA_SOURCE_NAME="$db_user_name:$db_passwd@($host_name)/"
        echo "DATA_SOURCE_NAME:$DATA_SOURCE_NAME"
         #start exporter 
        pid=$(ps -ef |grep $mysqld_exporter |grep  "$db_exporter_port"  |awk '{print $2}' | xargs)
        if [ "$pid" ]; then
            echo "export 进程已经存在"
        fi
        #./mysqld_exporter --web.listen-address=":9104"
        ./mysqld_exporter --web.listen-address=":$db_exporter_port" > mysqld_exporter.log 2>&1 &
        str="start mysql exporter port : $db_exporter_port"
        echo -e "\033[32m ${str}\033[0m"

elif [ $server_type = $POSTGRES_NAME ]; then
        echo "install Postgres exporter"
elif [ $server_type = $REDIS_NAME ]; then
        cd $redis_exporter
         #start exporter 
        pid=$(ps -ef |grep $redis_exporter |grep  "$db_exporter_port"  |awk '{print $2}' | xargs)
        if [ "$pid" ]; then
           echo "export 进程已经存在"
        fi
        #./redis_exporter -redis.addr 192.168.174.5:6379 -redis.password 123456  -web.listen-address=":9121"
        ./redis_exporter -redis.addr $host_name -web.listen-address=":$db_exporter_port" > redis_exporter.log 2>&1 &
        if [ $? == 0 ]; then
        str="start redis exporter port : $db_exporter_port"
        echo -e "\033[32m ${str}\033[0m"
        fi
fi
