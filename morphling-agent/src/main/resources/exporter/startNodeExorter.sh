#!/bin/bash
#启动node_exporter
if [ -z $1 ]; then
    echo "must add first args port ,exit"
    exit -1;
fi
port=$1
NODE_FILE="node_exporter-0.17.0.linux-amd64.tar.gz"
NODE_FILE_DIR="node_exporter-0.17.0.linux-amd64"
uname=$(uname -m)
#echo $uname
if [ $uname = "aarch64" ]; then
  NODE_FILE="node_exporter-0.17.0.linux-arm64.tar.gz"
  NODE_FILE_DIR="node_exporter-0.17.0.linux-arm64"
fi

exporter_dir=$(cd `dirname $0`;pwd)
cd ../../
app_dir=$(cd `dirname $0`;pwd)
tmp_dir=$app_dir/tmp
agent_dir=$app_dir/morphling-agent

cd $tmp_dir
if [ -f $NODE_FILE ]; then 
    tar -zxf $NODE_FILE
    if [ -d node_exporter ]; then 
        rm -rf node_exporter
    fi
    if [ -d $NODE_FILE_DIR ]; then 
        mv $NODE_FILE_DIR node_exporter 
        cp -rf node_exporter $exporter_dir
        rm -rf node_exporter
    else 
        echo "dir : $NODE_FILE_DIR is not found ..."
        exit -1
    fi
    cd $exporter_dir
    if [ -d node_exporter ]; then 
        #$1 port of node
        pid=$(ps -ef |grep node_exporter | grep $port | awk '{print $2}')
        if [ "${pid}" ]; then
            kill -9 ${pid}
        fi
        echo "start node_exporter ... "
        ./node_exporter/node_exporter --web.listen-address=":${port}" > node_exporter.log 2>&1 &
        echo "node_exporter  --web.listen-address=":${port}" is start "
    else 
        echo "dir : node_exporter is not found ..."
        exit -1
    fi
else 
    echo " file : $NODE_FILE  is not found ..."
    exit -1
fi

