#!/bin/bash
#server端新增监控，填写具体信息 确定后 调用morphling-agent/exporter目录下的脚本安装exporter 
#/home/weblogic/app/morphling-agent/exporter
# db_exporter_path
exporter_dir=$(cd `dirname $0`;pwd)
source $exporter_dir/DBExporter.properties
db_exporter_path=$exporter_dir/db_exporter
#echo "exporter_dir:$exporter_dir"
#echo "db_exporter_path:$db_exporter_path"

if [ ! -d "$db_exporter_path" ]; then
        mkdir -p $db_exporter_path; 
fi

if [ -d "$db_exporter_path" ]; then
       cd $db_exporter_path
else 
        echo "db_exporter dir is not found ..."
        exit -1
fi

server_type="$1"
db_user_name="$2"
db_passwd="$3"
#192.168.174.17:1521/ora11g 
host_name="$4"
db_exporter_port="$5"

#oracle 需要首先安装 oracle client java先scp拷贝文件oracle_client oracle_exporter
if [ $server_type = $ORACLE_NAME ]; then
        echo "install Oracle instantclient ..."
        #check files
        if [ ! -f "$oracle_client_pkg" ]; then
                echo "$oracle_client_pkg is not found ... "
                 exit -1
        fi

        if [ ! -f "$oracle_exporter_pkg" ]; then
                echo "$oracle_exporter_pkg is not found ..."
                exit -1
        fi
        #install oracle client  instantclient_18_5
        unzip -q -o $oracle_client_pkg
        #
        tar -zxvf $oracle_exporter_pkg

        if [ -d $oracledb_exporter ]; then
                rm -rf $oracledb_exporter
        fi

        mv oracledb_exporter.0.2.2.linux-amd64 $oracledb_exporter

        oracle_client_path=$db_exporter_path/instantclient_18_5
        oracle_exporter_path=$db_exporter_path/$oracledb_exporter

        echo "start oracle exporter ..."

        #export LD_LIBRARY_PATH=$oracle_client_path:$LD_LIBRARY_PATH
        export LD_LIBRARY_PATH=$oracle_client_path
        export DATA_SOURCE_NAME=$db_user_name/$db_passwd@$host_name
        export TNS_ADMIN=$oracle_client_path/network/admin
        #export  NLS_LANG='simplified chinese_china'.ZHS16GBK
        #export  LD_LIBRARY_PATH=$ORACLE_HOME/lib
        #export  PATH=$ORACLE_HOME/bin:$PATH


        #echo "LD_LIBRARY_PATH:$LD_LIBRARY_PATH"
        #echo "DATA_SOURCE_NAME:$DATA_SOURCE_NAME"
        #echo "TNS_ADMIN:$TNS_ADMIN"
        cd $oracle_exporter_path
        #start exporter 
        pid=$(ps -ef |grep $oracledb_exporter |grep  "$db_exporter_port"  |awk '{print $2}' | xargs)
        if [ "$pid" ]; then
                kill -9 $pid
        fi
        #默认$db_exporter_port=9161
        ./oracledb_exporter -log.level error -web.listen-address ":$db_exporter_port" > oracledb_exporter.log 2>&1 &
        #ps -ef|grep oracledb_exporter |grep 9161
        str="start oracle exporter port : $db_exporter_port"
         echo -e "\033[32m ${str}\033[0m"
elif [ $server_type = $MYSQL_NAME ]; then
        echo "install Mysql exporter ..."

        if [ ! -f "$mysql_exporter_pkg" ]; then
                echo "$mysql_exporter_pkg is not found ... "
                exit -1
        fi
        tar -zxvf $mysql_exporter_pkg

        if [ -d $mysqld_exporter ]; then
                rm -rf $mysqld_exporter
        fi
        mv mysqld_exporter-0.11.0.linux-amd64  $mysqld_exporter
        cd $mysqld_exporter
        #export DATA_SOURCE_NAME='user:password@(hostname:3306)/'
        #export DATA_SOURCE_NAME='root:root@(192.168.174.21:3306)/morphling'
        export DATA_SOURCE_NAME="$db_user_name:$db_passwd@($host_name)/"
        echo "DATA_SOURCE_NAME:$DATA_SOURCE_NAME"
         #start exporter 
        pid=$(ps -ef |grep $mysqld_exporter |grep  "$db_exporter_port"  |awk '{print $2}' | xargs)
        if [ "$pid" ]; then
                kill -9 $pid
        fi
        #./mysqld_exporter --web.listen-address=":9104"
        ./mysqld_exporter --web.listen-address=":$db_exporter_port" > mysqld_exporter.log 2>&1 &
        str="start mysql exporter port : $db_exporter_port"
        echo -e "\033[32m ${str}\033[0m"
elif [ $server_type = $POSTGRES_NAME ]; then

        echo "install Postgres exporter ..."

elif [ $server_type = $REDIS_NAME ]; then
       
        if [ ! -d $redis_exporter ]; then
                mkdir $redis_exporter
        fi
        echo "install redis exporter ..."
        if [ ! -f "$redis_exporter_pkg" ]; then
                echo "$redis_exporter_pkg is not found ... "
                exit -1
        fi

        cp $redis_exporter_pkg  $redis_exporter
        cd $redis_exporter
        tar -zxvf $redis_exporter_pkg
        rm -f $redis_exporter_pkg
         #start exporter 
        pid=$(ps -ef |grep $redis_exporter |grep  "$db_exporter_port"  |awk '{print $2}' | xargs)
        if [ "$pid" ]; then
                kill -9 $pid
        fi
        #./redis_exporter -redis.addr 192.168.174.5:6379 -redis.password 123456  -web.listen-address=":9121"
        ./redis_exporter -redis.addr $host_name -web.listen-address=":$db_exporter_port" > redis_exporter.log 2>&1 &
        if [ $? == 0 ]; then
        str="start redis exporter port : $db_exporter_port"
        echo -e "\033[32m ${str}\033[0m"
        fi
fi
