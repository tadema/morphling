#!/bin/bash
#停止脚本
# db_exporter_path
exporter_dir=$(cd `dirname $0`;pwd)
db_exporter_path=$exporter_dir/db_exporter
source $exporter_dir/DBExporter.properties

if [ ! -d "$db_exporter_path" ]; then
        mkdir -p $db_exporter_path; 
fi

if [ -d "$db_exporter_path" ]; then
       cd $db_exporter_path
else 
        echo "db_exporter dir is not found ..."
        exit -1
fi
server_type="$1"
db_user_name="$2"
db_passwd="$3"
#192.168.174.17:1521/ora11g 
host_name="$4"
db_exporter_port="$5"

if [ "$server_type" = $ORACLE_NAME ]; then
        oracle_client_path=$db_exporter_path/instantclient_18_5
        oracle_exporter_path=$db_exporter_path/oracledb_exporter
         echo "stop oracle exporter ..."


        cd $oracle_exporter_path
        #start exporter 
        pid=$(ps -ef |grep $oracledb_exporter |grep  "$db_exporter_port"  |awk '{print $2}' | xargs)
        if [ "$pid" ]; then
            kill -9 $pid
        fi
        str="stop oracle exporter port : $db_exporter_port"
         echo -e "\033[32m ${str}\033[0m"

elif [ $server_type = $MYSQL_NAME ]; then
        cd $mysqld_exporter
        pid=$(ps -ef |grep $mysqld_exporter |grep  "$db_exporter_port"  |awk '{print $2}' | xargs)
        if [ "$pid" ]; then
              kill -9 $pid
        fi
        str="start mysql exporter port : $db_exporter_port"
        echo -e "\033[32m ${str}\033[0m"

elif [ $server_type = $POSTGRES_NAME ]; then
        echo "install Postgres exporter"
elif [ $server_type = $REDIS_NAME ]; then
        cd $redis_exporter
         #start exporter 
        pid=$(ps -ef |grep $redis_exporter |grep  "$db_exporter_port"  |awk '{print $2}' | xargs)
        if [ "$pid" ]; then
            kill -9 $pid
        fi
        str="stop redis exporter port : $db_exporter_port"
        echo -e "\033[32m ${str}\033[0m"
fi
