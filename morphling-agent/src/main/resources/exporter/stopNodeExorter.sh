#!/bin/bash
#停止node脚本
if [ -z $1 ]; then
    echo "must add first args port ,exit"
    exit -1;
fi
 pid=`ps -ef|grep node_exporter | grep $1 | awk '{print $2}'`
    if [ $pid ]; then 
        kill -9 $pid
    fi
