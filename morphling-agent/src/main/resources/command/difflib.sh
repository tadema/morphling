#!/bin/bash
export base_dir=$(cd `dirname $0`;pwd)
chmod 755 $base_dir/*

export WORKSPACE_BASE_DIR="/home/$agent_username"
export WORKSPACE_DIR="$WORKSPACE_BASE_DIR/workspace"
export deploy_path="$WORKSPACE_DIR/deploy/$project_name"
export diff_path="$WORKSPACE_DIR/diff/$project_name"
export project_file="$project_file"
mkdirFunction()
{
	if [ ! -d "$1" ]; then
		 mkdir -p $1;
		if [ ! -d "$1" ]; then
			echo $line
			echo "创建$1 目录失败！"
			echo $line
			exit -1;
		fi
	fi
}

# source $base_dir/common.sh


mkdirFunction $diff_path/$deploying_version

download_url=http://$morphling_server_ip:$morphling_server_port/download/${project_file}_${deploying_version}_${app_id}.tar.gz
#diff path
cd $diff_path/$deploying_version
#wget -q -O $diff_path/$deploying_version/$project_file $download_url
curl -s -o $diff_path/$deploying_version/$project_file $download_url
unzip_name=`unzip -l $project_file |awk 'NR==4{print}'|awk '{print $4}'`
unzip -q -o  $project_file
#mv $unzip_name $project_name

new_dir=$diff_path/$deploying_version/$unzip_name
old_dir=$deploy_path/$deployed_version/$project_name/ 

sh $base_dir/diff.sh $new_dir $old_dir
#对比后移除目录下的程序包和文件
if [ -d "$new_dir" ]; then
		rm -rf $new_dir
        rm -f $project_file
fi
echo "$diff_path/$deploying_version/diff.html"
