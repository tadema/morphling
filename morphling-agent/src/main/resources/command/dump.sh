#!/bin/bash
#=====================================================================
#
#根据参数查询进程PID生成dump文件
#脚本接收参数：应用名、服务IP、服务PORT
#参数名称：$project_name $dump_port
#=====================================================================
isNotExists(){
    if [ ! -d $1 ]; then
        echo "$1目录不存在,无法生成dump"
        exit -1;
    fi
}
line="=========================================================="
#dump
#/home/weblogic
#/home/weblogic/app/jdk1.8/bin


do_dump(){

    if [ -z "${dump_port}" ]; then
        echo $line
        echo "dump端口为空，生成dump文件失败，退出!"
        echo $line
        exit 1
    fi

    if [ -z "$jdk_path" ]; then
        export java_home_bin="$BASE_DIR/app/jdk1.8/bin"
        echo "使用默认JDK生成：$BASE_DIR/app/jdk1.8"
        ${java_home_bin}/java -version
    else
        export java_home_bin="${jdk_path}/bin"
        echo "使用配置JDK生成：${jdk_path}"
        ${java_home_bin}/java -version
    fi

    export dump_port=$dump_port
    export dump_port_dir=${dump_path}/${dump_port}

    #生成dump文件目录（agent下）:一台服务器上的一个应用可能有多个端口服务，所以按照端口生成
        if [ -d $dump_port_dir ]; then
            echo "删除上次生成的dump文件"
            rm -rf $dump_port_dir
        fi
    mkdirFunction $dump_port_dir
    dump_date=`date +%Y%m%d%H%M%S`
    export dump_date_dir=${dump_port_dir}/${dump_date}
    mkdirFunction $dump_date_dir

    #通过端口查找PID
    PIDS=`ps -ef | grep java | grep $dump_port |awk '{print $2}'`
    if [ -z "$PIDS" ]; then
        echo $line
        echo "服务进程PID不存在，生成dump文件失败，退出!"
        echo $line
        exit 1
    fi
	#echo $dump_date_dir
    echo  "Dumping the $PIDS ...\c"
    for PID in $PIDS ; do
        #Full thread dump 用来查线程占用，死锁等问题
        echo -e "生成 jstack dump ..."
        ${java_home_bin}/jstack $PID > ${dump_date_dir}/jstack-$PID-${dump_port}-${dump_date}.dump 2>&1

        #打印出一个给定的Java进程、Java core文件或远程Debug服务器的Java配置信息，具体包括Java系统属性和JVM命令行参数
        echo -e "生成 jinfo dump ..."
        ${java_home_bin}/jinfo $PID > ${dump_date_dir}/jinfo-$PID-${dump_port}-${dump_date}.dump 2>&1
#         echo -e "生成 jinfo dump 文件结束"
        # jstat -gc $PID > ${dump_date_dir}/jstat-gc-$PID.dump 2>&1
        # if [ $? != 0 ]; then
        #     $JAVA_HOME_APP/jstat $PID > ${dump_date_dir}/jstat-$PID.dump 2>&1
        # fi
        # echo -e "生成 jstat -gc dump 文件结束"

        #jstat能够动态打印jvm(Java Virtual Machine Statistics Monitoring Tool)的相关统计信息。如young gc执行的次数、full gc执行的次数，各个内存分区的空间大小和可使用量等信息。
        echo -e "生成 jstat -gcutil dump ..."
        ${java_home_bin}/jstat -gcutil $PID > ${dump_date_dir}/jstat-gcutil-$PID-${dump_port}-${dump_date}.dump 2>&1

        echo -e "生成 jstat -gccapacity dump ..."
        ${java_home_bin}/jstat -gccapacity $PID > ${dump_date_dir}/jstat-gccapacity-$PID-${dump_port}-${dump_date}.dump 2>&1
        # # echo -e ".\c"

        #未指定选项时，jmap打印共享对象的映射。对每个目标VM加载的共享对象，其起始地址、映射大小及共享对象文件的完整路径将被打印出来，
        # jmap $PID > ${dump_date_dir}/jmap-$PID.dump 2>&1
        # if [ $? != 0 ]; then
        #     $JAVA_HOME_APP/jmap $PID > ${dump_date_dir}/jmap-$PID.dump 2>&1
        # fi
        # echo -e "生成 jmap dump 文件结束"

        #-heap打印堆情况的概要信息，包括堆配置，各堆空间的容量、已使用和空闲情况
        echo -e "生成 jmap -heap dump ..."
        ${java_home_bin}/jmap -heap $PID > ${dump_date_dir}/jmap-heap-$PID-${dump_port}-${dump_date}.dump 2>&1

#        echo -e "生成 jmap -histo dump ..."
#        ${java_home_bin}/jmap -histo $PID > ${dump_date_dir}/jmap-histo-$PID-${dump_port}-${dump_date}.dump 2>&1

        echo -e "生成 java dump 文件结束"
        #端口
        # if [ -r /usr/sbin/lsof ]; then
        # /usr/sbin/lsof -p $PID > ${dump_date_dir}/lsof-$PID.dump
        # echo -e ".\c"
        # fi
    done

    # if [ -r /bin/netstat ]; then
    # /bin/netstat -an > /netstat.dump 2>&1
    # echo -e ".\c"
    # fi
    # if [ -r /usr/bin/iostat ]; then
    # /usr/bin/iostat > ${dump_date_dir}/iostat.dump 2>&1
    # echo -e ".\c"
    # fi
    # if [ -r /usr/bin/mpstat ]; then
    # /usr/bin/mpstat > ${dump_date_dir}/mpstat.dump 2>&1
    # echo -e ".\c"
    # fi
    # if [ -r /usr/bin/vmstat ]; then
    # /usr/bin/vmstat > ${dump_date_dir}/vmstat.dump 2>&1
    # echo -e ".\c"
    # fi
    # if [ -r /usr/bin/free ]; then
    # /usr/bin/free -t > ${dump_date_dir}/free.dump 2>&1
    # echo -e ".\c"
    # fi
    # if [ -r /usr/bin/sar ]; then
    # /usr/bin/sar > ${dump_date_dir}/sar.dump 2>&1
    # echo -e ".\c"
    # fi
    # if [ -r /usr/bin/uptime ]; then
    # /usr/bin/uptime > ${dump_date_dir}/uptime.dump 2>&1
    # echo -e ".\c"
    # fi
}

do_dump
