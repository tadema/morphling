#!/bin/bash
# -------------------------------------------------------------------------------
line="=========================================================="
#=====================================================================
#							备份输出json函数
#=====================================================================
echo_backup_info(){
	echo '{"source_file":'"'$0'"',"target_file":'"'$1'"','"'date'"':'"'$ymd'"',"backup_time":'"'$time'"',"version":'"''"',"source_file_md5":'"'$3'"'}' >> $backup_path/$deployed_version/$backup_info
	#echo $deploy_source_file_name > $backup_path/$backup_info
}
#=====================================================================
#							备份weblogic应用
#=====================================================================
if [ "$server_type" = $WEBLOGIC_NAME ]; then
    if [ "$is_manager_server" = 1 ]; then
        deploy_source_file_name=$project_file
        if [  -z "$deployed_version" ]; then
            echo $line
            log_info "当前无部署版本，无需备份..."
            echo $line
        elif [ -d "$deploy_path/$deployed_version/$project_name" ]; then
            cd $deploy_path/$deployed_version
            echo $line
            log_info "开始备份应用程序..."
            echo $line

            if [ -d "$project_name" ]; then
                deploy_source_file_name=$project_name
                zip -q -r  $backup_path/$deployed_version/${project_name}.zip $project_name
            elif [ -f "${project_name}.war" ]; then
                deploy_source_file_name=${project_name}.war
                zip -q -r  $backup_path/$deployed_version/${project_name}.zip ${project_name}.war
            else
                echo $line
                log_info "部署目录不存在需要备份应用程序文件"
                echo $line
            fi
            if [ $? == 0 ]; then
                echo $line
                log_info "应用程序文件备份成功"
                echo $line
                echo_backup_info
            else 
                echo $line
                log_error "应用程序文件备份失败"
                echo $line
                exit -1
            fi
        fi
      
    fi
#=====================================================================
#							备份tomcat应用
#=====================================================================
elif [ "$server_type" = $TOMCAT_NAME ]; then
    echo $line
    log_info "开始备份应用程序..."
    echo $line
    if [  -z "$deployed_version" ]; then
        echo $line
        log_info "当前无部署版本，无需备份..."
        echo $line
	elif [ -d "$deploy_path/$deployed_version/$project_name" ]; then
        cd $tomcat_webapp_path
        if [ -d "$project_name" ]; then
            zip -q -r  $backup_path/$deployed_version/${project_name}.zip $project_name
        else
            echo $line
            echo "$tomcat_webapp_path部署目录不存在需要备份应用程序文件"
            echo $line
        fi
        if [ $? == 0 ]; then
            echo $line
            log_info "应用程序文件$project_name备份成功"
            echo $line
            echo_backup_info
        else 
            log_error "应用程序文件$project_name备份失败"
            exit -1
        fi
	fi
#=====================================================================
#							备份springboot应用
#=====================================================================
elif [ "$server_type" = $SPRING_BOOT_NAME ]; then
    echo $line
    log_info "开始备份应用程序..."
    echo $line
    deploy_dir=`ls $deploy_path/$deployed_version`
    if [  -z "$deployed_version" ]; then
        echo $line
        log_info "当前无部署版本，无需备份..."
        echo $line
    elif [ -n "deploy_dir" ]; then
        cd $deploy_path/$deployed_version
        zip -q -r ${project_name}.zip *
        mv ${project_name}.zip $backup_path/$deployed_version
        if [ $? == 0 ]; then
            echo $line
            log_info "应用程序文件备份成功"
            echo $line
            echo_backup_info
        else 
            echo $line
            log_error "应用程序文件备份失败"
            echo $line
            exit -1
        fi
    fi
fi
