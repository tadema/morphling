#!/bin/sh 
#当启动日志文件大小增长到某个size的时对日志文件进行切割
#split -a 5 -d -b 100m  spring_boot_start_9999_20190417133630.log  log_split_
#切割目标日志文件
log_file=$1
#文件封顶大小（M）
max_size_m=$2
#拆分后每个文件大小（M）
single_size_m=$3
((single_size_b=$single_size*1024*1024))
((max_size_b=$max_size_m*1024*1024))
#切割后保留份数
splip_num=5
#初始化变量
fix_num=0
#函数Get_Size，获取日志文件的大小(B)。
function Get_Size(){
	logfile=$1
	# if [ ! -f $logfile ]; then
	# 	echo "日志文件$logfile不存在，无法切割"
	# 	exit -1
	# fi
	file_size=`ls -l ${logfile}awk '{print $5}'`
	# if [[ $file_size =~ [1-9]* ]];then
	# echo $file_size
	# fi
}
#拆分文件 
function split_log_file(){
	#每个文件尺寸
	split_size=$1 
	#目标文件
	logfile=$2
	#切割后的文件名
	splited_file_name=$3
	if [ $logfile -le ${split_size}  ]; then 
		split -a 5 -d -b $split_size m $logfile $splited_file_name 
	fi
}
#滚动生成日志文件
function  log_file(){
	if [ $fix_num == $splip_num ]; then
		fix_num=0
	fi
	cp $file  ${log_file}$fix_num.log
	rm -f $file	
	let fix_num=$fix_num+1
}

#split
while [ 1 -ge 0 ]; do
	if [ -f $log_file ]; then
		log_file_size=`Get_Size ${log_file}`
		if [ ${log_file_size} -ge ${max_size_b} ]; then
			split_log_file $single_size_m $log_file ${log_file}_
		fi
	fi
done
