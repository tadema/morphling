#!/bin/bash

if [ "$server_type" = $WEBLOGIC_NAME ]; then
  mkdirFunction $exporter/weblogic
port=""
	if [ "$is_admin_server" = 1 ]; then
    port=$wl_admin_server_port
  fi
  if [ "$is_manager_server" = 1 ]; then
    port=$wl_manager_server_port
  fi
  #jmxUrl: service:jmx:t3://192.168.174.3:7703/jndi/weblogic.management.mbeanservers.runtime 
cat > $exporter/weblogic/jmx_exporter_${port}.yml << EOF 

username: $wl_username 
password: $wl_password 
lowercaseOutputName: false 
lowercaseOutputLabelNames: false 
whitelistObjectNames: 
  - "com.bea:ServerRuntime=*,Type=ApplicationRuntime,*" 
  - "com.bea:Type=WebAppComponentRuntime,*" 
  - "com.bea:Type=ServletRuntime,*" 
 
rules: 
  - pattern: "^com.bea<ServerRuntime=.+, Name=(.+), ApplicationRuntime=(.+), Type=ServletRuntime, WebAppComponentRuntime=(.+)><>(.+): (.+)" 
    attrNameSnakeCase: true 
    name: weblogic_servlet_$4 
    value: $5 
    labels: 
      name: $3 
      app: $2 
      servletName: $1 
 
  - pattern: "^com.bea<ServerRuntime=(.+), Name=(.+), ApplicationRuntime=(.+), Type=WebAppComponentRuntime><>(.+): (.+)$" 
    attrNameSnakeCase: true 
    name: webapp_config_$4 
    value: $5 
    labels: 
      app: $3 
      name: $2

EOF

elif [ "$server_type" = $TOMCAT_NAME ]; then 
  mkdirFunction $exporter/tomcat
cat > $exporter/tomcat/jmx_exporter_${tomcat_server_port}.yml << EOF 
---   
lowercaseOutputLabelNames: true
lowercaseOutputName: true
rules:
- pattern: 'Catalina<type=GlobalRequestProcessor, name=\"(\w+-\w+)-(\d+)\"><>(\w+):'
  name: tomcat_$3_total
  labels:
    port: "$2"
    protocol: "$1"
  help: Tomcat global $3
  type: COUNTER
- pattern: 'Catalina<j2eeType=Servlet, WebModule=//([-a-zA-Z0-9+&@#/%?=~_|!:.,;]*[-a-zA-Z0-9+&@#/%=~_|]), name=([-a-zA-Z0-9+/$%~_-|!.]*), J2EEApplication=none, J2EEServer=none><>(requestCount|maxTime|processingTime|errorCount):'
  name: tomcat_servlet_$3_total
  labels:
    module: "$1"
    servlet: "$2"
  help: Tomcat servlet $3 total
  type: COUNTER
- pattern: 'Catalina<type=ThreadPool, name="(\w+-\w+)-(\d+)"><>(currentThreadCount|currentThreadsBusy|keepAliveCount|pollerThreadCount|connectionCount):'
  name: tomcat_threadpool_$3
  labels:
    port: "$2"
    protocol: "$1"
  help: Tomcat threadpool $3
  type: GAUGE
- pattern: 'Catalina<type=Manager, host=([-a-zA-Z0-9+&@#/%?=~_|!:.,;]*[-a-zA-Z0-9+&@#/%=~_|]), context=([-a-zA-Z0-9+/$%~_-|!.]*)><>(processingTime|sessionCounter|rejectedSessions|expiredSessions):'
  name: tomcat_session_$3_total
  labels:
    context: "$2"
    host: "$1"
  help: Tomcat session $3 total
type: COUNTER

EOF

elif [ "$server_type" = $SPRING_BOOT_NAME ]; then
   mkdirFunction $exporter/springboot
cat > $exporter/springboot/jmx_exporter_${spring_boot_server_port}.yml << EOF 
---
rules:
- pattern: ".*"

EOF
fi
  
