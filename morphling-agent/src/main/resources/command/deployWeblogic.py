import time
import getopt
import sys
import re

#command
#connect('weblogic','weblogic123', 't3://192.168.174.5:7501')
#deploy(appName='test_weblogic12c', targets='server1,server2',stageMode='STAGE', path='/home/deploy/iplsqldevJ.war')
#redeploy('test_weblogic12c' '/home/deploy/iplsqldevJ.war')
#undeploy('test_weblogic12c', targets='server1,server2', timeout=6000)


# Get location of the properties file.
properties = ''
try:
   opts, args = getopt.getopt(sys.argv[1:],"p:h::",["properies="])
except getopt.GetoptError:
   print 'deployWeblogic.py -p <path-to-properties-file>'
   sys.exit(2)
for opt, arg in opts:
   if opt == '-h':
      print 'deployWeblogic.py -p <path-to-properties-file>'
      sys.exit()
   elif opt in ("-p", "--properties"):
      properties = arg
print 'properties=', properties

# Load the properties from the properties file.
from java.io import FileInputStream

propInputStream = FileInputStream(properties)
configProps = Properties()
configProps.load(propInputStream)

filePath=configProps.get("path")
url=configProps.get("url")
username=configProps.get("username")
password=configProps.get("password")
targetServer=configProps.get("targetServer")
name=configProps.get("appName")
stage=configProps.get("stage")

connect(username, password, url)

def editing():
	edit()
	startEdit()

def activating():
	save()
	activate(-1,block='false')

#edit() # 进入可更改的树
#startEdit() # 开启一个start session，获取一个配置锁

# appList = re.findall(appName, ls('/AppDeployments'))
# print "========================="
# print len(appList)
# if len(appList) > 0:
#     redeploy(appName, targets=targetServer, timeout=60000)
#oldestArchiveVersion = min(map(int, appList))
    # print 'come in!!!'
    # undeploy(deployAppName)
#deploy(appName='DeployExample', path = 'helloWebApp', targets = 'examplesServer',timeout=600000, block = 'false')

# undeploy(appName, targets=targetServer, timeout=600000)

#stopApplication
print 'stopApplication ...'
editing()
stopApplication(name,timeout=360000)
activating()

#editing()
#undeploy(name, timeout=360000)
#activating()

#deployApplication
print 'deployApplication ...'
editing()
deploy(appName=name,targets=targetServer,stageMode=stage,path=filePath,timeout=360000)
activating()

#startApplication
print 'startApplication ...'
editing()
startApplication(name,timeout=360000)
activating()

#print dumpStack()
#save() # 保存更改，类似控制台中点击保存按钮
#activate(block="true") # 激活所有更改，类似控制台中应用更改按钮，block参数代表是否等待activate操作完成才进入下一步操作，true代表等待
disconnect() # 断开连接
exit()
