#!/bin/sh

#jdk tomcat 
project_type="jdk"
project_name="$project_name"
project_file="jdk-7u67-linux-x64.tar.gz"
passowrd=""
install_home="/app/workspace/install"

 #切换到root权限
su  root <<EOF
$PASSWORD;
#download jdk 
#wget -qO $install_home/$project_file $remote_ip:$port/download/$project_file
curl -s -o $install_home/$project_file $remote_ip:$port/download/$project_file
#extract jdk
tar -xvf $project_file

#set environment
export JAVA_HOME="/usr/local/java/$project_name"
if ! grep "JAVA_HOME=/usr/local/java/$project_name" /etc/profile 
then
    echo "JAVA_HOME=/usr/local/java/$project_name" | sudo tee -a /etc/profile 
    echo "export JAVA_HOME" | sudo tee -a /etc/profile 
    echo "PATH=$PATH:$JAVA_HOME/bin" | sudo tee -a /etc/profile 
    echo "export PATH" | sudo tee -a /etc/profile 
    echo "CLASSPATH=.:$JAVA_HOME/lib" | sudo tee -a /etc/profile 
    echo "export CLASSPATH" | sudo tee -a /etc/profile 
fi

#update environment
source /etc/profile  
java -version
ehco "jdk is installed !"
exit;
EOF



