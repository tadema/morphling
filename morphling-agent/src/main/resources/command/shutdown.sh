#!/bin/bash
line="=========================================================="
#=====================================================================
#							停止weblogic服务
#=====================================================================
if [ "$server_type" = $WEBLOGIC_NAME ]; then
#判断adminServer情况
	if [ "$is_admin_server" = 1 ]; then
		pid=$(ps -ef |grep java |grep  "$wl_admin_server_name" |grep $wl_admin_server_port |awk '{print $2}' | xargs)
		if [ "$pid" ]; then
			kill -9 $pid
			pid=$(ps -ef |grep java |grep  "$wl_admin_server_name" |grep $wl_admin_server_port |awk '{print $2}' | xargs)
			if [ "$pid" ]; then
				echo $line
				echo "停止 weblogic server $wl_admin_server_name 失败"
				echo $line
			else 
				echo $line
				echo "停止 weblogic server $wl_admin_server_name 成功";
				echo $line
			fi
		else
			echo $line
			echo "weblogic server $wl_admin_server_name 已停止";
			echo $line
		fi
	fi
#wl_server_name_array 数组
	wl_server_name_array=(${wl_server_name//,/ })
#判断managedServer情况
	if [ "$is_manager_server" = 1 ]; then
		for server_name in ${wl_server_name_array[@]}
			do
				pid=$(ps -ef |grep java |grep  "$server_name" |grep $wl_manager_server_port |awk '{print $2}' | xargs)
				if [ "$pid" ]; then
 					kill -9 $pid
					pid=$(ps -ef |grep java |grep  "$server_name" |grep $wl_manager_server_port |awk '{print $2}' | xargs)
					if [ "$pid" ]; then
						echo $line
						echo "停止 weblogic server $server_name 失败"
						echo $line
					else 
						echo $line
						echo "停止 weblogic server $server_name 成功";
						echo $line
					fi
				else
					echo $line
					echo "weblogic server $server_name  已停止";
					echo $line
				fi
			done
	fi
#=====================================================================
#							停止tomcat服务
#=====================================================================
elif [ "$server_type" = $TOMCAT_NAME ]; then
	#JMX端口
	pid=$(ps -ef |grep java |grep  "$tomcat" | grep ${tomcat_server_port} |awk '{print $2}' | xargs)
	if [ "${pid}" ]; then 
		kill -9 ${pid}
		pid=$(ps -ef |grep java |grep  "$tomcat" | grep ${tomcat_server_port} |awk '{print $2}' | xargs)
		if [ "${pid}" ]; then
			echo $line
			echo "停止 tomcat 端口${tomcat_server_port}失败";
			echo $line
		else 
			echo $line
			echo "停止 tomcat  端口${tomcat_server_port}成功";
			echo $line
		fi
	else
		echo $line
		echo "tomcat 端口${tomcat_server_port}服务已停止"
		echo $line
	fi
#=====================================================================
#							停止springboot服务
#=====================================================================
elif [ "$server_type" = $SPRING_BOOT_NAME ]; then
	#pid=`lsof -i :${spring_boot_server_port}|grep -v "PID"|awk '{print $2}'`
	pid=$(ps -ef |grep java |grep  "${project_name}" | grep "\-Dserver.port\=$spring_boot_server_port" |awk '{print $2}' | xargs)
	if [ "$pid" ]; then 
		kill -9 $pid
		#pid=`lsof -i :${spring_boot_server_port}|grep -v "PID"|awk '{print $2}'`
		pid=$(ps -ef |grep java |grep  "${project_name}" | grep "\-Dserver.port\=$spring_boot_server_port" |awk '{print $2}' | xargs)
		if [ "$pid" ]; then
			echo $line
			log_info "停止 spring boot  端口：${spring_boot_server_port}失败";
			echo $line
		else 
			echo $line
			log_info "停止 spring boot 端口：${spring_boot_server_port}成功";
			echo $line
		fi
	else
		echo $line
		echo "spring boot 端口${spring_boot_server_port}服务已停止"
		echo $line
	fi
	
fi 
