# AdminServer connection details.

# admin.username=weblogic
# admin.password=weblogic123
# admin.url=t3://192.168.174.4:6201

# ms.name=app1
# ms.address=192.168.174.4
# ms.port=6203
# ms.cluster=myCluster_1
# ms.sslport=6303
# ms.machine=192.168.174.4


echo "================================================================================================="
echo "==    create weblogic managed server with weblogic.WLST"
echo "================================================================================================="

# Set environment.
export WLS_HOME=$wlServerHome
#export JAVA_HOME=/u01/app/oracle/jdk1.7.0_79
#export PATH=$JAVA_HOME/bin:$PATH
export DOMAIN_HOME=$domainHome/$domainName
. $DOMAIN_HOME/bin/setDomainEnv.sh

# Create the managed servers.
server_properties=$basedir/app/morphling-agent/command/managed.properties
create_py_file=$basedir/app/morphling-agent/command/createManagedServer.py
java weblogic.WLST $create_py_file -p $server_properties
