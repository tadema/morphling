#!/bin/bash
line="=========================================================="
# $base_dir/backup.sh
 cd  $update_path

#=====================================================================
#						如果部署成功，生成版本号文件
#=====================================================================
createDeployVersionInfo(){
	#echo "生成版本号文件: ${deploy_path}/$deploy_version_info"
	echo $deploying_version > ${deploy_path}/$deploy_version_info
}


#=====================================================================
#						下载部署文件函数
#=====================================================================
download(){
	echo $line
	log_info "开始下载部署程序文件..."
	echo $line
	cd  $update_path/$deploying_version
	rm  -rf $update_path/$deploying_version/*
	if [ ! -f $update_path/$deploying_version/$project_file ]; then
#		wget -q -O $update_path/$deploying_version/$project_file $download_url
		curl -s -o $update_path/$deploying_version/$project_file $download_url
		if [ $? == 0 ]; then
			echo $line
			log_info "从服务端下载$project_file成功"
			echo $line
		else
			echo $line
			log_error "从服务端下载$project_file失败,停止执行"
			echo $line
			exit -1
		fi
	fi
}

#=====================================================================
#							部署输出json函数
#=====================================================================
echo_install_info(){
	cd $deploy_path/$deploying_version
	time=`date +%Y%m%d%H%M%S`
	str="$2"
    OLD_IFS="$IFS"
    IFS=","
    arr=($str)
    for name in ${arr[@]}
    do
        echo '{"deploy_status":"'"$1"'","server_name":"'"$name"'","time":"'"$time"'","project_file":"'"$project_file"'"}' > $deploy_path/$deploying_version/"$name"_$install_info
    done
    IFS="$OLD_IFS"
#	echo '{"deploy_status":"'"$1"'","server_name":"'"$2"'","time":"'"$time"'","project_file":"'"$project_file"'"}' > $deploy_path/$deploying_version/$3_$install_info
}

#=====================================================================
#						根据不同类型解压部署文件函数
#=====================================================================
decompress_files(){
	#下载目录
	 cd  $update_path/$deploying_version
	if [ "${project_file##*.}" = "war" ]; then
		deploy_source_file_name="${project_name}.war"
		cp $project_file $deploy_path/${project_name}.war
	elif [ "${project_file##*.}" = "jar" ]; then
		deploy_source_file_name="${project_name}.jar"
		if [ ! -d $deploy_path/$project_name ]; then
			mkdir -p $deploy_path/$project_name
		fi
		cp $project_file $deploy_path/$project_name/${project_name}.jar
	else
		deploy_source_file_name=$project_name
		if [ "${project_file##*.}" = "zip" ]; then
		  echo $line
		  log_info "开始解压程序$project_file文件..."
		  echo $line
		#zip解压后的名字
		  unzip_name=`unzip -l $project_file |awk 'NR==4{print}'|awk '{print $4}'`
			if [ "$server_type" = $SPRING_BOOT_NAME ]; then
			  #mv $project_file ${project_name}.zip
				unzip -q -o -d temp $project_file
				echo "${project_file%.*}"
				cd temp
				# if [ "${unzip_name##*.}" != "jar" ]; then
				# 	cd $unzip_name
				# fi
				if [ -d ${project_file%.*} ]; then
					cd ${project_file%.*}
					if [ -f *.jar ]; then
#						mv *${project_name}*.jar  ${project_name}.jar
            mv *.jar  ${project_name}.jar
					fi
					cp -rf $update_path/$deploying_version/temp/${project_file%.*}/*  $deploy_path/$project_name
				else
					if [ -f *.jar ]; then
#						mv *${project_name}*.jar  ${project_name}.jar
            mv *.jar  ${project_name}.jar
					fi
					cp -rf $update_path/$deploying_version/temp/*  $deploy_path/$project_name
				fi
				rm  -rf $update_path/$deploying_version/temp
			else
			#zip -d $project_file $excloud_files
				#unzip -q -o  $project_file
				#weblogic zip $exclude_files排除部署文件
				unzip -q -o  $project_file -x $exclude_files
				if [ -d $unzip_name ]; then
					mv $unzip_name $project_name
					cp -rf $project_name  $deploy_path
					rm -rf $project_name
				fi
				# if [ "${unzip_name##*.}" = "war" ]; then
				# 	unzip -d $project_name $unzip_name
				# elif  [ -d $unzip_name ]; then
				# 	if [ "${unzip_name%*/}" != "$project_name" ]; then
				# 		mv $unzip_name  $project_name
				# 	fi
				# fi
				# cp -r $project_name  $deploy_path
				# rm -rf  $project_name
			fi

		# elif [ "${project_file##*.}" = "tar" ]; then
		# 	tar -zxf $project_file -C  $deploy_path
		# elif [ "${project_file#*.}" = "tar.gz" ]; then
		# 	tar -zxf $project_file -C $deploy_path
		fi
		if [ $? == 0 ]; then
			echo $line
			echo  "解压程序$project_file文件成功"
			#echo "remove $update_path/$deploying_version/*"
			echo $line
		else
			echo $line
			echo "解压程序$project_file文件失败,停止执行"
			echo $line
			exit -1
		fi
	fi
	rm -rf $update_path/$deploying_version/*
 }

#定义变量区分文件或目录
deploy_source_file_name=$project_name
#=====================================================================
#							weblogic部署
#=====================================================================
if [ "$server_type" = $WEBLOGIC_NAME ]; then
	if [ ! -d "${deploy_path}/$project_name" ]; then
		mkdir -p ${deploy_path}/$project_name;
	fi
 	if [ "$is_manager_server" = 1 ]; then
		download
		decompress_files
		cd $deploy_path
		cp -rf $deploy_source_file_name  $deploy_path/$deploying_version
		echo $line
		log_info "开始部署应用程序$project_name ..."
		echo $line
		lib_dir="$wl_server_home/server/lib"
		# if [ "$is_stage" = 1 ]; then
		# 	java -cp $lib_dir/weblogic.jar weblogic.Deployer -adminurl t3://$wl_admin_server_ip:$wl_admin_server_port -name $project_name -username $wl_username -password $wl_password -redeploy  -source $deploy_path/$deploy_source_file_name -targets $wl_server_name
		# elif [ "$is_stage" = 0 ]; then
		# 	java -cp $lib_dir/weblogic.jar weblogic.Deployer -adminurl t3://$wl_admin_server_ip:$wl_admin_server_port -name $project_name -username $wl_username -password $wl_password -redeploy  -source $deploy_path/$deploy_source_file_name -targets $wl_server_name -nostage
		# fi
		if [ "$is_stage" = 1 ]; then
			export stage="stage"
		elif [ "$is_stage" = 0 ]; then
			export stage="nostage"
		fi
		. $wl_server_home/server/bin/setWLSEnv.sh
		deploy_weblogic=$basedir/app/morphling-agent/command/deployWeblogic.py
		deploy_properties=$deploy_path/deploy.properties

cat > $deploy_properties  << EOF
path=$deploy_path/$deploy_source_file_name
url=t3://$wl_admin_server_ip:$wl_admin_server_port
username=$wl_username
password=$wl_password
targetServer=$wl_server_name
appName=$project_name
stage=$stage
EOF
		java weblogic.WLST $deploy_weblogic -p $deploy_properties

		if [ $? == 0 ]; then
			echo $line
			log_info "$project_name部署成功,请重启weblogic"
			echo $line
			echo_install_info "1" $wl_server_name $wl_server_name
			createDeployVersionInfo
		else
			echo $line
			log_error "$project_name部署失败"
			echo $line
			echo_install_info "0" $wl_server_name $wl_server_name
		fi
	fi
#=====================================================================
#							tomcat部署
#=====================================================================
elif [ "$server_type" = $TOMCAT_NAME ]; then
	echo $line
	log_info "开始部署tomcat 端口:$tomcat_server_port应用程序"
	echo $line
	download
	decompress_files
	cd $deploy_path
	#备份到对应版本
	cp -rf $deploy_source_file_name  $deploy_path/$deploying_version
	cd $tomcat_webapp_path
	if [ "${deploy_source_file_name##*.}" = "war" ]; then
		rm -f ${project_name}.war
	fi
	if [ -d $deploy_source_file_name ]; then
		rm -rf $deploy_source_file_name
	fi
	cp -rf $deploy_path/$deploying_version/$deploy_source_file_name $tomcat_webapp_path
	if [ $? == 0 ]; then
		echo $line
		log_info "$project_name部署成功,请重启tomcat"
		echo $line
		echo_install_info "1" ${project_name}_${tomcat_server_port} ${project_name}_${tomcat_server_port}
		createDeployVersionInfo
	else
		echo $line
		log_error "$project_name部署失败"
		echo $line
		echo_install_info "0" ${project_name}_${tomcat_server_port} ${project_name}_${tomcat_server_port}
	fi
#=====================================================================
#						spring boot 部署
#=====================================================================

elif [ "$server_type" = $SPRING_BOOT_NAME ]; then
	echo $line
	log_info "开始部署spring boot 端口:$spring_boot_server_port应用程序"
	echo $line
	download
	decompress_files
	cd $deploy_path/$project_name
	cp -rf *  $deploy_path/$deploying_version
	if [ $? == 0 ]; then
		echo $line
		log_info "$project_name部署成功,请重启springboot"
		echo $line
		echo_install_info "1" ${project_name} ${project_name}
		createDeployVersionInfo
	else
		echo $line
		log_error "$project_name部署失败"
		echo $line
		echo_install_info "0" ${project_name} ${project_name}
	fi
fi

