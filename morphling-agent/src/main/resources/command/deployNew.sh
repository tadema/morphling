#!/bin/bash

export base_dir=$(cd `dirname $0`;pwd)
chmod 755 $base_dir/*
source $base_dir/common.sh

case "$1" in
    start)
       $base_dir/startup.sh
    ;;
    stop)
        $base_dir/shutdown.sh
    ;;
    # status)
    #     /app/${project_name}/server.sh status
    # ;;
    restart)
      $base_dir/restart.sh
    ;;
    dump)
        $base_dir/dump.sh
    ;;
    install)
         $base_dir/install.sh
    ;;
    uninstall)
         $base_dir/uninstall.sh
    ;;
    update)
       $base_dir/install.sh
    ;;
     rollback)
       $base_dir/rollback.sh
    ;;
    log)
       $base_dir/log.sh
    ;;
    *)
        echo "Usage ${0} <start|stop|status|restart|dump>"
        exit 1
    ;;
esac
