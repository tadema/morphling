#!/bin/bash

if [ ! -d $1 ]; then
    echo "$1目录不存在"
    exit -1;
fi

if [ ! -d $2 ]; then
    echo "$2目录不存在"
    exit -1;
fi

IFS=$'\n\n'
diff_html=diff.html
new_dir="$1"
old_dir="$2"


# echo "${project_file}"
echo '<!DOCTYPE html><html lang="zh-CN">' > $diff_html
echo '<head><meta charset="UTF-8"><title>'${project_file}'部署文件MD5对比列表</title></head>' >> $diff_html
echo '<style type="text/css">'   >> $diff_html    
echo 'tr td { '   >> $diff_html       
echo 'word-break: break-all;' >> $diff_html       
echo 'word-wrap:break-word; '>> $diff_html       
echo '} '>> $diff_html       
echo '</style>' >> $diff_html
echo '<body>' >> $diff_html
echo '<h1>部署文件差异对比列表</h1>' >> $diff_html
echo '<h4>待部署版本:'${deploying_version}'</h4>' >> $diff_html
echo '<h4>已部署版本:'${deployed_version}'</h4>' >> $diff_html


# new_files_txt="new_files.txt"
# old_files_txt="old_files.txt"
# find $new_dir -type f -print | xargs md5sum > $new_files_txt
# find $old_dir -type f -print | xargs md5sum > $old_files_txt

# new_files_txt=`find $new_dir -type f -print | xargs md5sum`
# old_files_txt=`find $old_dir -type f -print | xargs md5sum`


declare -A old_map
declare -A new_map


# find $old_dir -type f -print | xargs md5sum |while read line
find $old_dir -type f  | xargs md5sum  |sed "s:${old_dir}::g"> out_old_file
while read  line
do

    # array=(${line// / })  
    IFS=" "
    array=($line)
    old_value=${array[0]}
    old_key=${array[1]} 
    # echo "数组元素个数为: ${#array[*]}"
    old_map["$old_key"]="$old_value"

    #cut 性能慢
    # old_key=`echo $line | cut -f1 -d " "`
   #  old_value=`echo $line | cut -f3 -d " " |sed "s:${old_dir}::g"` 
   #  echo $old_key
   #  echo $old_value
   # old_map["$old_value"]="$old_key"
    # echo $old_key
    # echo $line | cut -f3 -d " " |sed "s:${old_dir}::g"
   
done < out_old_file
# echo ${#old_map[@]}
rm -f out_old_file
# find $new_dir -type f -print | xargs md5sum|while read line
find $new_dir -type f  | xargs md5sum  |sed "s:${new_dir}::g"> out_new_file
while read  line
do
    # echo $line
    IFS=" "
    array=($line)
    new_value=${array[0]} 
    new_key=${array[1]} 
    new_map["$new_key"]="$new_value"
    # new_key=`echo $line | cut -f1 -d " "`
    # new_value=`echo $line | cut -f3 -d " " |sed "s:${new_dir}::g"`
    # new_map["$new_value"]="$new_key"
    # echo $new_key
    # echo $line | cut -f3 -d " " |sed "s:${old_dir}::g"
done < out_new_file
rm -f out_new_file
# echo ${#new_map[@]}

# for i in `cat $old_files_txt`; do
#  		old_key=`echo $i | cut -f1 -d " "`
#         echo $i
 		# old_value=`echo $i | cut -f3 -d " " |sed "s:${old_dir}::g"` 
 		# echo $old_key
 		# echo $i | cut -f3 -d " " |sed "s:${old_dir}::g"
 		# old_map["$old_value"]="$old_key"
# done


# for j in `cat $new_files_txt`; do
#  		new_key=`echo $j | cut -f1 -d " "`
        # echo $new_key
#  		new_value=`echo $j | cut -f3 -d " " |sed "s:${new_dir}::g"`
#  		new_map["$new_value"]="$new_key"
# done

# echo ${!old_map[@]}
# echo ${old_map[@]}


echo '<div>'  >> $diff_html
echo '<table border="1" cellspacing="0" cellpadding="0" align="right">'   >> $diff_html
echo '<tr><th>待部署文件</th><th>MD5值</th><th>已部署文件</th><th>MD5值</th><th>是否一致</th></tr>'  >> $diff_html

for key in ${!new_map[@]}
do
	#md5
    # echo ${new_map[$key]}
    # echo ${old_map[$key]}

    if [ -n "${old_map[$key]}" ]; then

    	if [ "${new_map[$key]}" = "${old_map[$key]}" ]; then
    		# echo "md5相同"
    		# echo  "$key ${new_map[$key]} $key ${old_map[$key]} " ;
    		# echo '<div>'  >> $diff_html
    		# echo '<table border="1" cellspacing="0" cellpadding="0" align="right">'   >> $diff_html
    		# echo '<tr><th>待部署文件</th><th>MD5值</th><th>已部署文件</th><th>MD5值</th></tr>'  >> $diff_html
    		echo '<tr><td>'$key'</td><td>'${new_map[$key]}'</td><td>'$key'</td><td>'${old_map[$key]}'</td><td>一致</td></tr>'  >> $diff_html
    		# echo '</table>'  >> $diff_html
    		# echo '</div>'  >> $diff_html
    	else 
    		# echo " md5不相同"
    		# echo  " $key  ${new_map[$key]} $key ${old_map[$key]}" ;
    		# echo '<div>'  >> $diff_html
    		# echo '<table border="1" cellspacing="0" cellpadding="0" align="right">'  >> $diff_html
    		# echo '<tr><th>待部署文件</th><th>MD5值</th><th>已部署文件</th><th>MD5值</th></tr>'  >> $diff_html
    		echo '<tr><td>'$key'</td><td>'${new_map[$key]}'</td><td>'$key'</td><td>'${old_map[$key]}'</td><td style="background-color:red">不一致</td></tr>'  >> $diff_html
    		# echo '</table>'  >> $diff_html
    		# echo '</div>'  >> $diff_html
    	fi

    else 
    	# echo  "新增文件"
    	# echo  "$key  ${new_map[$key]} "
    		# echo '<div>'  >> $diff_html
    	    # echo '<table border="1" cellspacing="0" cellpadding="0" align="right">'  >> $diff_html
    		# echo '<tr><th>待部署文件</th><th>MD5值</th><th>已部署文件</th><th>MD5值</th></tr>'  >> $diff_html
    		echo '<tr><td>'$key'</td><td>'${new_map[$key]}'</td><td></td><td></td><td style="background-color:red">不一致</td></tr>'  >> $diff_html
    		# echo '</table>'  >> $diff_html
    		# echo '</div>'  >> $diff_html
    fi
done

echo '</table>'  >> $diff_html
echo '</div>'  >> $diff_html
 	
 		# if [ $new_value = $old_value ]; then
			# echo $j
 		# 	if [ $new_key = $old_key ]; then
 		# 		echo $new_key $new_value $old_key $old_value "MD5相同"	
 		# 	else
			# 	echo $new_key $new_value  $old_key $old_value "MD5不同"
 		# 	fi
 		# fi

 
   # echo '<li><a href="'$i'">'$i'</a></li>' >> $diff_html
  # fi

echo '</body>' >> $diff_html
echo '</html>' >> $diff_html

# echo "MD5对比列表已生成!"
