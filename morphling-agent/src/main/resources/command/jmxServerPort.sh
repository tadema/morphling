#!/bin/bash

 #JMX端口
if [ -z $num ]; then
	#export num=0
	export num=100
fi
if [ -z $weblogic_admin_jmx_port ]; then
	export weblogic_admin_jmx_port=`expr $wl_admin_server_port + $num`
fi
if [ -z $weblogic_managed_jmx_port ]; then
	export weblogic_managed_jmx_port=`expr $wl_manager_server_port + $num`
fi

if [ -z $tomcat_jmx_port ]; then
	export tomcat_jmx_port=` expr $tomcat_server_port + $num`
fi

if [ -z $springboot_jmx_port ]; then 
	export springboot_jmx_port=` expr $spring_boot_server_port + $num`
fi

if [ "$server_type" = $WEBLOGIC_NAME ]; then
    if [ "$is_admin_server" = 1 ]; then
        if [ ! -z $weblogic_admin_jmx_port ]; then
            export JAVA_OPTIONS="${JAVA_OPTIONS} -Dcom.sun.management.jmxremote.port=$weblogic_admin_jmx_port "
        fi
    fi
    if [ "$is_manager_server" = 1 ]; then
        if [ ! -z $weblogic_managed_jmx_port ]; then
            export JAVA_OPTIONS="${JAVA_OPTIONS} -Dcom.sun.management.jmxremote.port=${weblogic_managed_jmx_port} "
        fi
    fi
elif [ "$server_type" = $TOMCAT_NAME ]; then 
    if [ ! -z $tomcat_jmx_port ]; then
        mkdirFunction $exporter/tomcat
       export JAVA_OPTS="${JAVA_OPTS} -Dcom.sun.management.jmxremote.port=${tomcat_jmx_port} "
    fi
elif [ "$server_type" = $SPRING_BOOT_NAME ]; then
    if [ ! -z $springboot_jmx_port ]; then
        mkdirFunction $exporter/springboot
        export OPTS=" -Dcom.sun.management.jmxremote.port=${springboot_jmx_port} "
    fi
fi
  
