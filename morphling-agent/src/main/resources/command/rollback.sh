#!/bin/bash
line="=========================================================="
#=====================================================================
#							部署输出json函数
#=====================================================================
echo_rollback_info(){
	cd $deploy_path/$rollback_version
    time=`date +%Y%m%d%H%M%S`
	str="$2"
    OLD_IFS="$IFS"
    IFS=","
    arr=($str)
    for name in ${arr[@]}
    do
        echo '{"deploy_status":"'"$1"'","server_name":"'"$name"'","time":"'"$time"'","project_file":"'"$project_file"'"}' > $deploy_path/$rollback_version/"$name"_$rollback_info
    done
    IFS="$OLD_IFS"
#	echo '{"rollback_status":"'"$1"'","server_name":"'"$2"'","time":"'"$time"'","project_file":"'"$project_file"'"}' > $deploy_path/$rollback_version/$3_$rollback_info
}
#=====================================================================
#               回退脚本,回滚到某个版本应用程序
#=====================================================================
rollback(){
	 echo $line
	 echo "回滚应用的版本号为：$rollback_version"
	 echo $line

	if [ -d $deploy_path/$rollback_version ]; then
		echo $line
		echo "开始回滚部署$rollback_version版本的${project_name}应用程序文件"
		echo $line
		# deploy_source_file_name=${project_name}
    else
		echo $line
        echo "回滚版本不存在，回滚失败,停止执行"
        exit -1
		echo $line
	fi
}

#=====================================================================
#                      weblogic部署回滚程序
#=====================================================================
if [ "$server_type" = $WEBLOGIC_NAME ]; then
 	if [ "$is_manager_server" = 1 ]; then
        rollback
		cd $deploy_path
		if [ -d $deploy_path/$project_name ]; then
			cd   $deploy_path/$project_name
			rm -rf *
		fi
		cp -rf $deploy_path/$rollback_version/$project_name*  $deploy_path
		deploy_source_file_name=$deploy_path/$rollback_version/$project_name*;
		if [ "${project_file##*.}" = "war" ]; then
			deploy_source_file_name=$project_name.war
		else
			deploy_source_file_name=$project_name
		fi
#		lib_dir="$wl_server_home/server/lib"
#		if [ "$is_stage" = 1 ]; then
#			java -cp $lib_dir/weblogic.jar weblogic.Deployer -adminurl t3://$wl_admin_server_ip:$wl_admin_server_port -name $project_name -username $wl_username -password $wl_password -redeploy  -source $deploy_path/$deploy_source_file_name -targets $wl_server_name
#		elif [ "$is_stage" = 0 ]; then
#			java -cp $lib_dir/weblogic.jar weblogic.Deployer -adminurl t3://$wl_admin_server_ip:$wl_admin_server_port -name $project_name -username $wl_username -password $wl_password -redeploy  -source $deploy_path/$deploy_source_file_name -targets $wl_server_name -nostage
#		fi
        if [ "$is_stage" = 1 ]; then
            export stage="stage"
        elif [ "$is_stage" = 0 ]; then
            export stage="nostage"
        fi
        . $wl_server_home/server/bin/setWLSEnv.sh
        deploy_weblogic=$basedir/app/morphling-agent/command/deployWeblogic.py
        deploy_properties=$deploy_path/deploy.properties

cat > $deploy_properties  << EOF
path=$deploy_path/$deploy_source_file_name
url=t3://$wl_admin_server_ip:$wl_admin_server_port
username=$wl_username
password=$wl_password
targetServer=$wl_server_name
appName=$project_name
stage=$stage
EOF
		java weblogic.WLST $deploy_weblogic -p $deploy_properties
		if [ $? == 0 ]; then
			echo $line
			log_info "$project_name部署回滚程序成功,请重启weblogic服务"
			echo $line
			echo_rollback_info "1" $wl_server_name $wl_server_name
		else
			echo $line
			log_error "$project_name部署回滚程序失败"
			echo $line
			echo_rollback_info "0" $wl_server_name $wl_server_name
		fi
	fi
#=====================================================================
#                          tomcat 部署回滚
#=====================================================================
elif [ "$server_type" = $TOMCAT_NAME ]; then
	rollback
	cd $tomcat_webapp_path
	if [ -d $project_name ]; then
		rm -rf $project_name
	fi
	if [ -f $project_name.war ]; then
		rm  -f $project_name.war
	fi
	cp -rf $deploy_path/$rollback_version/$project_name* $tomcat_webapp_path
	if [ $? == 0 ]; then
		echo $line
		log_info "$project_name部署回滚程序成功"
		log_info "请重启tomcat服务器!"
		echo $line
		echo_rollback_info "1" ${project_name}_${tomcat_server_port} ${project_name}_${tomcat_server_port}
	else
		echo $line
		log_error "$project_name部署回滚程序失败"
		echo $line
		echo_rollback_info "0" ${project_name}_${tomcat_server_port} ${project_name}_${tomcat_server_port}
	fi
#=====================================================================
#						spring boot 部署回滚
#=====================================================================

elif [ "$server_type" = $SPRING_BOOT_NAME ]; then
	rollback
	if [ -d $deploy_path/$project_name ]; then
		cd   $deploy_path/$project_name
		rm -rf *
	fi
	cp -rf  $deploy_path/$rollback_version/* $deploy_path/$project_name
	if [ -f $deploy_path/$project_name/$project_name.jar ]; then
		echo $line
		log_info "$project_name部署回滚程序成功"
		echo $line
		echo_rollback_info "1" ${project_name} ${project_name}
	else
		echo $line
		log_error "$project_name部署回滚程序失败"
		echo $line
		echo_rollback_info "0" ${project_name} ${project_name}
	fi
fi
