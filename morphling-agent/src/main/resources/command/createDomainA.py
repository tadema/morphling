import string
import os
import ConfigParser

configPath = os.path.join(sys.argv[1], "config.txt")
path = os.path.abspath(configPath)
print(path)
config = ConfigParser.ConfigParser()
config.read(path)

print "================================================================================================="
print "==    create weblogic domain with weblogic.WLST"
print "==    author: hongliang-wang@neusoft.com"
print "==    script last updated at: 2017-11-04"
print "================================================================================================="
conf = {
  "TemplateFile": config.get('domain','TemplateFile'),
  "DomainHome": config.get('domain','DomainHome'),
  "JavaHome": config.get('domain','JavaHome'),
  "DomainName": config.get('domain','DomainName'),
  "OverwriteDomain": 'true',
  "ServerStartMode": "dev", #prod, dev
  "UserWeblogicPassword": "weblogic123",
  "Users":[{
    "name": config.get('domain','name'),
    "password": config.get('domain','password'),
    "IsDefaultAdmin": true,
    "Description": "administrator",
    "groups": ["Administrators"]
  },{
    "name": "weblogicUser",
    "password": "weblogic123",
    "IsDefaultAdmin": false,
    "groups": ["Operators"]
  },{
    "name": "administrator",
    "password": "weblogic123",
    "Description": "super administrator",
    "groups": ["Administrators", "AdminChannelUsers", "AppTesters", "CrossDomainConnectors", "Deployers", "Monitors", "Operators", "OracleSystemGroup"]
  }],
  "AdminServer": {
    "ServerName": config.get('domain','ServerName'),
    "ListenAddress": "",
    "ListenPort": int(config.get('domain','ListenPort')),
    "SSLEnabled": true,
    "SSLListenPort": int(config.get('domain','SSLListenPort'))
  },
  "ManagedServers": [],
  "JDBCSystemResources": [],
  "AppDeployment": []
}

	
#=================================================================================================
# Open a domain template.
#=================================================================================================

print 
print "================================================================================================="
print "==    domain infomation"
print "================================================================================================="
print "domain template file: " + conf["TemplateFile"]
readTemplate(conf["TemplateFile"])

print "domain name: %s, domain version is %s" % (conf["DomainName"], cmo.getDomainVersion())
cd("/")
cmo.setName(conf["DomainName"])

#=================================================================================================
# Configure the Administration Server and SSL port.
#=================================================================================================

print 
print "================================================================================================="
print "==    configure the administration server"
print "================================================================================================="
if conf.has_key("AdminServer"):
  cd("/")
  print "create administration server: %s(%s:%d)" % (conf["AdminServer"]["ServerName"], conf["AdminServer"]["ListenAddress"], conf["AdminServer"]["ListenPort"])
  create(conf["AdminServer"]["ServerName"], "AdminServer")
  cd("/Servers/" + conf["AdminServer"]["ServerName"])
  #ls("a")
  set("ListenAddress",conf["AdminServer"]["ListenAddress"])
  set("ListenPort", conf["AdminServer"]["ListenPort"])
  if conf["AdminServer"]["SSLEnabled"] == "True" :
    create(conf["AdminServer"]["ServerName"], "SSL")
    cd("SSL/" + conf["AdminServer"]["ServerName"])
    set("Enabled", "True")
    set("ListenPort", conf["AdminServer"]["SSLListenPort"])

#=================================================================================================
# Configure the Managed Servers and SSL port.
#=================================================================================================

print 
print "================================================================================================="
print "==    configure the managed servers"
print "================================================================================================="
#for server in conf["ManagedServers"] :
#  cd("/")
#  create(server["ServerName"], "Server")
#  cd("/Servers/" + server["ServerName"])
#  set("ListenAddress",server["ListenAddress"])
#  set("ListenPort", server["ListenPort"])

cd("/")
for server in conf["ManagedServers"] :
  print server
  print "create managed server: %s(%s:%d)" % (server["ServerName"], server["ListenAddress"], server["ListenPort"])
  cmo_ser = create(server["ServerName"], "Server")
  cmo_ser.setListenAddress(server["ListenAddress"])
  cmo_ser.setListenPort(server["ListenPort"])

#=================================================================================================
# Configure User settings
#=================================================================================================

print 
print "================================================================================================="
print "==    configure user settings"
print "================================================================================================="
print "current users:"
current_users = ls("/Security/" + conf["DomainName"] + "/User/")
if "weblogic" not in current_users :
  print "create user: weblogic"
  create("weblogic", "User")
  assign("User", "weblogic", "Group", "Administrators")

cd("/Security/" + conf["DomainName"] + "/User/weblogic")
cmo.setIsDefaultAdmin(true)
if conf.has_key("UserWeblogicPassword"):
  cmo.setPassword(conf["UserWeblogicPassword"])
else:
  cmo.setPassword("weblogic123")

for user in conf["Users"] :
  if not user.has_key("name") or user["name"].lower() == "user":
    continue
  cd("/")
  try:
    if user["name"] != "weblogic": 
      print "create user: " + user["name"]
      create(user["name"], "User")
    cd("/Security/" + conf["DomainName"] + "/User/" + user["name"])
    cmo.setPassword(user["password"])
    if user.has_key("Description"): 
      cmo.setDescription(user["Description"])
    if user.has_key("groups"):
      unassign("User", user["name"], "Group", "Administrators")
      assign("User", user["name"], "Group", string.join(user["groups"],","))
    else:
      unassign("User", user["name"], "Group", "Administrators")
    if user.has_key("IsDefaultAdmin") and user["IsDefaultAdmin"]: 
      cmo.setIsDefaultAdmin(true)
      assign("User", user["name"], "Group", "Administrators")
  except:
    dumpStack()

#=================================================================================================
# Configure RDBMS Security Store settings
#=================================================================================================

#print "Configure RDBMS security store"
#create("base_domain","SecurityConfiguration")
#cd("/SecurityConfiguration/base_domain")
#realm=get("DefaultRealm")
#cd("Realm/myrealm")
#rdbms = create("myRDBMSSecurityStore", "RDBMSSecurityStore")
#rdbms.setUsername("system")
#rdbms.setPasswordEncrypted("password")
#rdbms.setConnectionURL("jdbc:bea:oracle://localhost:1521")
#rdbms.setDriverName("weblogic.jdbc.oracle.OracleDriver")
#rdbms.setConnectionProperties("user=system,portNumber=1521,SID=XE,serverName=localhost")

#=================================================================================================
# Create and configure a JDBC Data Source, and sets the JDBC user.
#=================================================================================================

print 
print "================================================================================================="
print "==    configure data sources"
print "================================================================================================="
for ds in conf["JDBCSystemResources"] :
  cd("/")
  print "create data source: %s(JNDIName:%s), use driver: %s, %s@%s, server %s usages" % (ds["DataSourceName"], ds["JNDIName"], ds["DriverName"], ds["user"], ds["URL"], string.join(ds["Target"], ","))
  create(ds["DataSourceName"], "JDBCSystemResource")
  
  cd("JDBCSystemResource/" + ds["DataSourceName"] + "/JdbcResource/" + ds["DataSourceName"])
  create("JdbcDriverParams", "JDBCDriverParams")
  cd("JDBCDriverParams/NO_NAME_0")
  set("DriverName", ds["DriverName"])
  set("URL", ds["URL"])
  set("PasswordEncrypted", ds["password"])
  set("UseXADataSourceInterface", ds["UseXADataSourceInterface"])
  create("Props", "Properties")
  cd("Properties/NO_NAME_0")
  create("user", "Property")
  cd("Property/user")
  cmo.setValue(ds["user"])
  
  cd("/JDBCSystemResource/" + ds["DataSourceName"] + "/JdbcResource/" + ds["DataSourceName"])
  create("JdbcDataSourceParams", "JDBCDataSourceParams")
  cd("JDBCDataSourceParams/NO_NAME_0")
  set("JNDIName", ds["JNDIName"])
  
  cd("/JDBCSystemResource/" + ds["DataSourceName"] + "/JdbcResource/" + ds["DataSourceName"])
  create("myJdbcConnectionPoolParams", "JDBCConnectionPoolParams")
  cd("JDBCConnectionPoolParams/NO_NAME_0")
  set("TestTableName", ds["TestTableName"])
  
  cd("/")
  assign("JDBCSystemResource", ds["DataSourceName"], "Target", string.join(ds["Target"],","))

#=================================================================================================
# Create and configure AppDeployment
#=================================================================================================

print 
print "================================================================================================="
print "==    create and configure application deployment"
print "================================================================================================="
for app in conf["AppDeployment"]:
  cd ("/")
  print "deploy application: %s to server: %s, SourcePath: %s, %s, %s, %s ." % (app["DeployName"], string.join(app["Target"]), app["SourcePath"], app["SecurityDdModel"], app["ModuleType"], app["StagingMode"])
  create (app["DeployName"], "AppDeployment")
  cd ("AppDeployment/" + app["DeployName"])
  if app.has_key("Target"):
    set("Target", string.join(app["Target"], ","))
  if app.has_key("SourcePath"):
    set("SourcePath", app["SourcePath"])
  if app.has_key("SecurityDdModel"):
    set("SecurityDdModel", app["SecurityDdModel"])
  if app.has_key("ModuleType"):
    set("ModuleType", app["ModuleType"])
  if app.has_key("StagingMode"):
    set("StagingMode", app["StagingMode"])
  if app.has_key("DeploymentOrder"):
    set("DeploymentOrder", app["DeploymentOrder"])
  if app.has_key("Notes"):
    set("Notes", app["Notes"])
  if app.has_key("InstallDir"):
    set("InstallDir", app["InstallDir"])

#=================================================================================================
# Write the domain and close the domain template.
#=================================================================================================

print 
print "================================================================================================="
print "==    set domain options and create domain"
print "================================================================================================="
if conf.has_key("JavaHome") and conf["JavaHome"] != "" :
  print "use java home: " +  conf["JavaHome"]
  setOption("JavaHome", conf["JavaHome"])
print "ServerStartMode is " + conf["ServerStartMode"]
setOption("ServerStartMode", conf["ServerStartMode"])
setOption("OverwriteDomain", conf["OverwriteDomain"])
print "domain path: " + conf["DomainHome"] + "/" + conf["DomainName"]
writeDomain(conf["DomainHome"] + "/" + conf["DomainName"])
closeTemplate()

#=================================================================================================
# Reopen the domain.
#=================================================================================================

#readDomain(conf["DomainHome"] + "/" + conf["DomainName"])
#
#mServers = "ms1", "ms2", "ms3"
#startPort = 18001
#for mServer in mServers:
#  cd("/")
#  create(mServer, "Server")
#  cd("Server/"+mServer)
#  set("ListenPort", startPort)
#  startPort += 10
#  set("ListenAddress", "localhost")

#=================================================================================================
# Create and configure a cluster and assign the Managed Servers to that cluster.
#=================================================================================================

#clusterName = "wlsCluster"

#cd("/")
#create(clusterName, "Cluster")
#assign("Server", string.join(mServers,","), "Cluster", clusterName)
#cd("Clusters/"+clusterName)
#set("MulticastAddress", "237.0.0.101")
#set("MulticastPort", 9200)
#set("WeblogicPluginEnabled", "true")

#cd("/SecurityConfiguration/bas_domain/Realm/myrealm")
#ls()

#=================================================================================================
# Write the domain and close the domain template.
#=================================================================================================

#updateDomain()
#closeDomain()

#=================================================================================================
# Exit WLST.
#=================================================================================================

print 
print "================================================================================================="
print "==    domain %s successfully created!" % (conf["DomainName"])
print "==    run %s/startWebLogic.sh to start domain." % (conf["DomainHome"] + "/" + conf["DomainName"])
print "==    enjoy it & bye~"
print "================================================================================================="

exit()

