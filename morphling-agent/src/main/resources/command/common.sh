#!/bin/bash

line="=========================================================="
#=====================================================================
#
# 创建基础目录函数
#
#=====================================================================
mkdirFunction()
{
	if [ ! -d "$1" ]; then
		 mkdir -p $1;
		if [ ! -d "$1" ]; then
			echo $line
			echo "创建$1 目录失败！"
			echo $line
			exit -1;
		fi
	fi
}

#=====================================================================
#
#请求执行脚本的参数配置信息
#
#=====================================================================
#启动服务类型
export server_type=$server_type
#weblogic管理服IP地址
export wl_admin_server_ip=$wl_admin_server_ip
#weblogic管理服端口
export wl_admin_server_port=$wl_admin_server_port
#weblogic受管服务端口
export wl_manager_server_port=$wl_manager_server_port
#springboot启动端口
export spring_boot_server_port=$spring_boot_server_port
#tomcat启动端口
export tomcat_server_port=$tomcat_server_port
#weblogic管理服务名称
export wl_admin_server_name=$wl_admin_server_name
#中间件安装目录
export middleware_home=$middleware_home
#weblogic domain名称
export wl_domain_name=$wl_domain_name
#weblogic 受管server名称
export wl_server_name=$wl_server_name
#weblogic用户
export wl_username=$wl_username
#weblogic密码
export wl_password=$wl_password
#有部署权限的操作系统用户
export os_user=$os_user
#有部署权限的操作系统用户密码
export os_password=$os_password
#发布的应用名称
export project_name=$project_name
#部署文件名称
export project_file=$project_file
#serverIP
export morphling_server_ip=$morphling_server_ip
export morphling_server_port=$morphling_server_port
export client_ip=$client_ip
#判断是否为adminServer
export is_admin_server=$is_admin_server
#判断是否为受管server
export is_manager_server=$is_manager_server
#部署模式是否stage
export is_stage=$is_stage
#是否启用监控
export is_monitor=$is_monitor
#weblogic域路径
export wl_domain_home=$middleware_home
#weblogic wlserver路径
export wl_server_home=$wlserver_home
#weblogic 内存参数 USER_MEM_ARGS="-Xms512m -Xmx768m"
export weblogic_user_mem_args="$weblogic_user_mem_args"
#spring boot 参数
export spring_boot_jvm_params="$param1"
export spring_boot_active_params="$param2"
#tomcat 内存参数 JAVA_OPTS="-server -Xms256m -Xmx512m -XX:PermSize=64M -XX:MaxPermSize=128m"
export tomcat_user_mem_args="$tomcat_user_mem_args"
#部署信息json
export install_info="install.json"
export rollback_info="rollback.json"
export backup_info="backup.json"
export update_info="update.json"
#备份信息
export backup_info="backup.json"
export WEBLOGIC_NAME="weblogic"
export TOMCAT_NAME="tomcat"
export SPRING_BOOT_NAME="spring-boot"
#tomcat拼装路径
export tomcat_webapp_path=$middleware_home/webapps
export tomcat_webapp_bin=$middleware_home/bin
#下载路径
export download_path=$download_path

export app_id=$app_id
#已经部署的版本号
export deployed_version=$deployed_version
#正在部署的版本号
export deploying_version=$file_version
export exclude_files=$exclude_files
#回滚操作的版本号
export rollback_version=$rollback_version
#部署成功的版本信息文件
export deploy_version_info=deploy.version
#weblogic 应用缓存目录
export webloigic_app_tmp=$wl_domain_home/$wl_domain_name/servers/$wl_server_name/tmp
export webloigic_app_cache=$wl_domain_home/$wl_domain_name/servers/$wl_server_name/cache
export webloigic_app_data=$wl_domain_home/$wl_domain_name/servers/$wl_server_name/tmp/data

#拼接下载地址
export download_url=http://$morphling_server_ip:$morphling_server_port/download/${project_file}_${deploying_version}_${app_id}.tar.gz
#dir
export BASE_DIR="/home/$agent_username"
#workspace base dir
export WORKSPACE_BASE_DIR="/home/$agent_username"
export WORKSPACE_DIR="$WORKSPACE_BASE_DIR/workspace"
export workspace_app_dir="$WORKSPACE_DIR/app"
export workspace_backup_dir="$WORKSPACE_DIR/backup"
export workspace_dump_dir="$WORKSPACE_DIR/dump"
export workspace_log_dir="$WORKSPACE_DIR/log"
export workspace_software_dir="$WORKSPACE_DIR/software"
export workspace_update_dir="$WORKSPACE_DIR/update"
export workspace_deploy_dir="$WORKSPACE_DIR/deploy"
#project path
export app_path="$workspace_app_dir/$project_name/"
export backup_path="$workspace_backup_dir/$project_name"
export dump_path="$workspace_dump_dir/$project_name"
export log_path="$workspace_log_dir/$project_name"
export software_path="$workspace_software_dir/$project_name"
#部署程序下载目录
export update_path="$workspace_update_dir/$project_name"
#部署目录
export deploy_path="$workspace_deploy_dir/$project_name"
#exporter
export exporter="$BASE_DIR/app/morphling-agent/exporter"
export jmx_prometheus_javaagent="jmx_prometheus_javaagent-0.3.1.jar"

#jdk路径：/home/weblogic/app/jdk1.8/bin/java
export jdk_path=$java_home

#HOSTNAME
if [ -z $client_ip ]; then
	export client_ip=$HOSTNAME
else
	export client_ip=$client_ip
fi

#mkdirFunction $app_path
mkdirFunction $backup_path
mkdirFunction $dump_path
mkdirFunction $log_path
mkdirFunction $update_path
mkdirFunction $deploy_path
if [ "$server_type" = $SPRING_BOOT_NAME ]; then
	mkdirFunction $deploy_path/$project_name
fi
#mkdirFunction $app_path/$deploying_version
mkdirFunction $backup_path/$deploying_version
#mkdirFunction $dump_path/$deploying_version
mkdirFunction $log_path/$deploying_version
mkdirFunction $update_path/$deploying_version
mkdirFunction $deploy_path/$deploying_version

#exporter dir
mkdirFunction $exporter


#=====================================================================
#日期参数
#=====================================================================
export ymd=''$(date +"%Y%m%d")''
export ym=''$(date +"%Y%m")''
export time=`date +%Y%m%d%H%M%S`
#=====================================================================
#log日志相关
#=====================================================================
export shell_log_path="$basedir/app/logs/shell"
export log_file=$shell_log_path/log_$ymd.log
#=====================================================================
#创建日志目录
#=====================================================================
# if [ ! -d "$shell_log_path" ]; then
# 	mkdir -p $shell_log_path;
# 	if [ $? == 1 ]; then
# 		echo "创建log目录失败";
# 		now_time='['$(date +"%Y-%m-%d %H:%M:%S")']'
# 		msg="{\"time\":\"$now_time\",\"message\":\"创建log目录失败\",\"status\":\"1\"}";
# 		 echo $msg >> ${log_file}
# 		exit -1;
# 	fi
# fi
#=====================================================================
#日志信息输出
#=====================================================================
log_info()
 {
	now_time='['$(date +"%Y-%m-%d %H:%M:%S")']'
	msg="{\"time\":\"$now_time\",\"message\":\"$1\",\"status\":\"0\"}";
	echo "$now_time:$1"
	 #echo $msg >> ${log_file}
 }
#=====================================================================
#日志输出错误信息
#=====================================================================
log_error()
 {
	now_time='['$(date +"%Y-%m-%d %H:%M:%S")']'
	msg="{\"time\":\"$now_time\",\"message\":\"$1\",\"status\":\"1\",\"error\":\"$2\"}";
	echo "$now_time:$1"
	 #echo $msg >> ${log_file}
 }
#=====================================================================
#使日志函数生效
#=====================================================================
export -f log_info
export -f log_error
export -f mkdirFunction
