#!/bin/bash

#Line='==========='

#Linux查看版本当前操作系统发行版信息
if [[ -f /etc/redhat-release ]]; then
    OS_VERSION=$(cat /etc/redhat-release |sed -n '1p')
fi
#echo -e "${Line}\nOS:\n${OS_VERSION}\n${Line}"

#查看系统是否为64位：uname -m，若出现x86_64，则为64位
OS_INFO=$(uname -a)

#系统内核版本
# KERNEL_VERSION=$(uname -r)

#cpu型号
CPU_MODEL=$(grep 'model name' /proc/cpuinfo |uniq |awk -F : '{print $2}' |sed 's/^[ \t]*//g' |sed 's/ \+/ /g')

#物理cpu个数
CPU_COUNTS=$(grep 'physical id' /proc/cpuinfo |sort |uniq |wc -l)

#物理cpu内核数
CPU_CORES=$(grep 'cpu cores' /proc/cpuinfo |uniq |awk -F : '{print $2}' |sed 's/^[ \t]*//g')

#逻辑cpu个数
CPU_PROCESSOR=$(grep 'processor' /proc/cpuinfo |sort |uniq |wc -l)

#查看CPU当前运行模式是64位还是32位
# CPU_BIT=$(getconf LONG_BIT)
#echo -e "Present Mode Of CPU_BIT:\n${CPU_BIT}\n${Line}"


#查看CPU是否支持64位技术：grep 'flags' /proc/cpuinfo，若flags信息中包含lm字段，则支持64位
# Numbers=$(grep 'lm' /proc/cpuinfo |wc -l)
# if (( ${Numbers} > 0)); then lm=64
# else lm=32
# fi
#echo -e "Support Mode Of CPU:\n${lm}\n${Line}"
######################################################################

#MemTotal 物理内存总大小
MEM_TOTAL=$(cat /proc/meminfo |grep 'MemTotal' |awk -F : '{print $2}' |sed 's/^[ \t]*//g')
#MemFree 物理内存剩余大小
MEM_FREE=$(cat /proc/meminfo |grep 'MemFree' |awk -F : '{print $2}' |sed 's/^[ \t]*//g')

#SwapTotal 虚拟内存总大小
SWAP_TOTAL=$(cat /proc/meminfo |grep 'SwapTotal' |awk -F : '{print $2}' |sed 's/^[ \t]*//g')
#SwapFree 虚拟内存剩余大小
SWAP_FREE=$(cat /proc/meminfo |grep 'SwapFree' |awk -F : '{print $2}' |sed 's/^[ \t]*//g')

#Buffers size
#BUFFERS_SIZE=$(cat /proc/meminfo |grep 'Buffers' |awk -F : '{print $2}' |sed 's/^[ \t]*//g')

#Cached size
#CACHED_SIZE=$(cat /proc/meminfo |grep '\<Cached\>' |awk -F : '{print $2}' |sed 's/^[ \t]*//g')

#显示硬盘，以及大小
# Disk=$(fdisk -l |grep 'Disk' |awk -F , '{print $1}' | sed 's/Disk identifier.*//g' | sed '/^$/d')

#各挂载分区使用情况
# Partion=$(df -hlP |sed -n '2,$p')

# echo '{"操作系统发行版":"'"$OS_VERSION"'","系统详情":"'"$OS_INFO"'","CPU型号":"'"$CPU_MODEL"'","物理cpu个数":"'"$CPU_COUNTS"'","物理cpu内核数":"'"$CPU_CORES"'","逻辑cpu个数":"'"$CPU_PROCESSOR"'","物理内存总大小":"'"$MEM_TOTAL"'","物理内存剩余大小":"'"$MEM_FREE"'","虚拟内存总大小":"'"$SWAP_TOTAL"'","虚拟内存剩余大小":"'"$SWAP_FREE"'"}'
echo '{"OS_VERSION":"'"$OS_VERSION"'","OS_INFO":"'"$OS_INFO"'","CPU_MODEL":"'"$CPU_MODEL"'","CPU_COUNTS":"'"$CPU_COUNTS"'","CPU_CORES":"'"$CPU_CORES"'","CPU_PROCESSOR":"'"$CPU_PROCESSOR"'","MEM_TOTAL":"'"$MEM_TOTAL"'","MEM_FREE":"'"$MEM_FREE"'","SWAP_TOTAL":"'"$SWAP_TOTAL"'","SWAP_FREE":"'"$SWAP_FREE"'"}'
