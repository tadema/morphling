#!/bin/sh 
#当启动日志文件大小增长到某个size的时对日志文件进行切割
#split -a 5 -d -b 100m  spring_boot_start_9999_20190417133630.log  log_split_
#日志目录
log_dir=$1
#文件
log_file=$2
#每个文件大小
log_size=$3
new_file_name=$4

if [ ! -d $1 ];then
	echo "日志文件目录不存在"
	exit -1
fi

cd $1
echo "split -a 4 -d -b $log_size $log_file $new_file_name"
split -a 4 -d -b $log_size $log_file  $new_file_name
