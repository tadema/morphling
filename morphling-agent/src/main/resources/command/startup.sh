#!/bin/bash
line="=========================================================="
#weblogic启动前清理缓存
clean_cache() 
{
	echo $line
	echo "开始清理应用程序缓存"
	if [ -d "${webloigic_app_tmp}" ]; then
		rm -rf ${webloigic_app_tmp}
	fi
	if [ -d "${webloigic_app_cache}" ]; then
		rm -rf ${webloigic_app_cache}
	fi
	if [ -d "${webloigic_app_data}" ]; then
		rm -rf ${webloigic_app_data}
	fi
	echo "清理应用程序缓存完成"
	echo $line
}
#启动日志时间
log_date_s=''$(date +"%Y%m%d%H%M%S")''
log_date=''$(date +"%Y%m%d")''
log_bak="bak"
#是否监控jmx
if [ "$is_monitor" = 1 ]; then
	source $base_dir/jmxExporter.sh
fi
#=====================================================================
#							启动weblogic服务
#=====================================================================
if [ "$server_type" = $WEBLOGIC_NAME ]; then
	#逗号分隔数组
	wl_server_name_array=(${wl_server_name//,/ })
	# 启动 admin server
	if [ "$is_admin_server" = 1 ]; then
	#jmx端口
		echo $line
		echo "开始启动weblogic adminServer $wl_admin_server_name ...";
		echo $line
		#security目录
		wl_admin_server_security_home=$wl_domain_home/$wl_domain_name/servers/$wl_admin_server_name/security
		pid=$(ps -ef |grep java |grep  "$wl_admin_server_name" |grep $wl_admin_server_port |awk '{print $2}' | xargs)
		if [ "$pid" ]; then
			echo $line
			echo "weblogic adminServer $wl_admin_server_name 已启动，pid=$pid";
			echo $line
		else
			
			# 生成的log目录
			adminServer_logs_dir=$wl_domain_home/$wl_domain_name/servers/$wl_admin_server_name/logs
			# 生成的log文件名
			adminServer_start_log=$adminServer_logs_dir/${wl_admin_server_name}_start.log
			#log后缀时间
			# log_date=''$(date +"%Y%m%d%H%M%S")''
			#每次启动前将日志备份到/workspcae/log/应用目录下
			if [ -f $adminServer_start_log ]; then
				mv  "$adminServer_start_log"  "$log_path/$deployed_version/${wl_admin_server_name}_start_$log_bak.log"
				if [ $? == 0 ]; then
					echo $line
					echo "备份启动日志 $adminServer_start_log 为${wl_admin_server_name}_start_$log_bak.log "
					echo $line
				fi	
			else
				echo $line
				echo " 首次启动无需备份${wl_admin_server_name}日志"
				echo $line
			fi	
			if [ ! -d "$wl_admin_server_security_home" ]; then
				mkdir -p $wl_admin_server_security_home
			fi
			cd $wl_admin_server_security_home
			if [ ! -f "boot.properties" ]; then
				touch	boot.properties
				echo -e "username=$wl_username\npassword=$wl_password" > boot.properties
			fi
			
			if [  -d "$wl_domain_home/$wl_domain_name" ]; then
				if [ ! -d "$adminServer_logs_dir" ]; then
					mkdir -p $adminServer_logs_dir
				fi
				str=$"\n"
				
				export JAVA_OPTIONS="${JAVA_OPTIONS} -Dcom.sun.management.jmxremote=true " 
				#export JAVA_OPTIONS="${JAVA_OPTIONS} -Dcom.sun.management.jmxremote.port=$weblogic_admin_jmx_port "
				export JAVA_OPTIONS="${JAVA_OPTIONS} -Dcom.sun.management.jmxremote.authenticate=false "
				export JAVA_OPTIONS="${JAVA_OPTIONS} -Dcom.sun.management.jmxremote.ssl=false "
				export JAVA_OPTIONS="${JAVA_OPTIONS} -Djavax.management.builder.initial=weblogic.management.jmx.mbeanserver.WLSMBeanServerBuilder "
				#ps进程中查不到端口号所以启动的时候增加自定义
				export JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.port=$wl_admin_server_port"
				
				$wl_domain_home/$wl_domain_name/bin/startWebLogic.sh > $adminServer_start_log 2>&1 &
				sstr=$(echo -e $str)
				sleep 2s
			else
				echo $line
				echo "此服务器无 $wl_domain_name 目录，不是 $wl_domain_name 管理服务器，无需启动adminserver！"
				echo $line
			fi
			pid=$(ps -ef |grep java |grep  "$wl_admin_server_name" |grep $wl_admin_server_port |awk '{print $2}' | xargs)
			if [ -z "$pid" ]; then
				for((i=0;i<20;i++));
				do
				sleep 3s
				pid=$(ps -ef |grep java |grep  "$wl_admin_server_name" |grep $wl_admin_server_port |awk '{print $2}' | xargs)
				if [ "$pid" ]; then
					break;
				fi
				done
			fi
			pid=$(ps -ef |grep java |grep  "$wl_admin_server_name" |grep $wl_admin_server_port |awk '{print $2}' | xargs)
			if [ "$pid" ]; then
				echo $line
				echo "启动 weblogic adminServer $wl_admin_server_name 成功";
				echo $line
				exit -1
			else 
				echo $line
				echo "启动 weblogic adminServer $wl_admin_server_name 失败,停止执行"
				echo $line
				exit -1;
			fi	
		fi
	fi
	#启动 manager server
	if [ "$is_manager_server" = 1 ]; then
		for managed_server_name in ${wl_server_name_array[@]}
		do
			wl_server_security_home=$wl_domain_home/$wl_domain_name/servers/$managed_server_name/security
			echo $line
			log_info "开始启动 weblogic managedServer $managed_server_name ...";
			echo $line
			pid=$(ps -ef |grep java |grep  "$managed_server_name" |grep $wl_manager_server_port |awk '{print $2}' | xargs)
			if [ "$pid" ]; then
				echo $line
				echo "weblogic managedServer $managed_server_name 已经启动，请先停止服务再启动，pid=$pid";
				echo $line
			else
				#security
				lib_dir="$wl_server_home/server/lib"
				# status=$(java -cp $lib_dir/weblogic.jar weblogic.Admin -username $wl_username -password $wl_password  -url t3://$wl_admin_server_ip:$wl_admin_server_port GETSTATE |grep state |awk '{print $6}')
				# echo $line
				# echo "admin server status is ${status}"
				# echo $line
				# if [ -z "$status" ]; then
				# 	echo "admin server 未启动或启动未完成, 尝试等待"
				# 	for((i=0;i<10;i++));
				# 	do
				# 	sleep 2s
				# 	status=$(java -cp ${lib_dir}/weblogic.jar weblogic.Admin -username $wl_username -password $wl_password  -url t3://$wl_admin_server_ip:$wl_admin_server_port GETSTATE |grep state |awk '{print $6}')
				# 		if [ "$status" ]; then
				# 			log_info "admin server status is $status "
				# 			break
				# 		fi
				# 	done
				# fi	
				if [ ! -d "$wl_server_security_home" ]; then
					mkdir -p $wl_server_security_home
				fi
				cd $wl_server_security_home
				touch	boot.properties
				echo -e "username=$wl_username\npassword=$wl_password" > boot.properties
				# if [ ! -f "boot.properties" ]; then
				# fi
				
				clean_cache
				managedServer_logs_dir=$wl_domain_home/$wl_domain_name/servers/$managed_server_name/logs
				managedServer_start_log=$managedServer_logs_dir/${managed_server_name}_start.log
				if [ -f $managedServer_start_log ]; then
					
					if [ ! -z $deployed_version ]; then
						mv  $managedServer_start_log   $log_path/$deployed_version/${managed_server_name}_start_$log_bak.log
						if [ $? == 0 ]; then
						echo $line
						echo "备份启动日志 $managedServer_start_log 为${managed_server_name}_start_$log_bak.log "
						echo $line
						fi
					fi
				else
					echo $line
					echo " 首次启动无需备份${managed_server_name}日志"
					echo $line
				fi
				if [ ! -d "$managedServer_logs_dir" ]; then
					mkdir -p $managedServer_logs_dir
				fi

				str=$"\n"
				export JAVA_OPTIONS="${JAVA_OPTIONS} -Dcom.sun.management.jmxremote=true "
				#export JAVA_OPTIONS="${JAVA_OPTIONS} -Dcom.sun.management.jmxremote.port=${weblogic_managed_jmx_port} "
				export JAVA_OPTIONS="${JAVA_OPTIONS} -Dcom.sun.management.jmxremote.authenticate=false "
				export JAVA_OPTIONS="${JAVA_OPTIONS} -Dcom.sun.management.jmxremote.ssl=false "
				export JAVA_OPTIONS="${JAVA_OPTIONS} -Djavax.management.builder.initial=weblogic.management.jmx.mbeanserver.WLSMBeanServerBuilder "
				#ps进程中查不到端口号所以启动的时候增加自定义
				export JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.port=$wl_manager_server_port"
				export USER_MEM_ARGS=$weblogic_user_mem_args
				  $wl_domain_home/$wl_domain_name/bin/startManagedWebLogic.sh $managed_server_name t3://$wl_admin_server_ip:$wl_admin_server_port > $managedServer_start_log 2>&1 &
				sstr=$(echo -e $str)
				sleep 2s

				pid=$(ps -ef |grep java |grep  "$managed_server_name" |grep $wl_manager_server_port |awk '{print $2}' | xargs)
				if [ -z "$pid" ]; then
					for((i=0;i<20;i++));
					do
					sleep 3s
					pid=$(ps -ef |grep java |grep  "$managed_server_name" |grep $wl_manager_server_port |awk '{print $2}' | xargs)
					if [ "$pid" ]; then
						break;
					fi
					done
				fi
				pid=$(ps -ef |grep java |grep  "$wl_server_name" |grep $wl_manager_server_port |awk '{print $2}' | xargs)
				if [ "$pid" ]; then
				# echo "检查启动状态..."
				# 	managed_status=$(java -cp $lib_dir/weblogic.jar weblogic.Admin -username $wl_username -password $wl_password  -url t3://$wl_admin_server_ip:$wl_admin_server_port get -pretty -mbean "$wl_domain_name:Location=$wl_server_name,Name=$wl_server_name,Type=ServerRuntime" |grep "State: RUNNING" |awk {'print $2'})
				# if [ -z "$managed_status" ]; then
				# 	for((i=0;i<10;i++));
				# 	do
				# 	sleep 2s
				# 	managed_status=$(java -cp $lib_dir/weblogic.jar weblogic.Admin -username $wl_username -password $wl_password  -url t3://$wl_admin_server_ip:$wl_admin_server_port get -pretty -mbean "$wl_domain_name:Location=$wl_server_name,Name=$wl_server_name,Type=ServerRuntime" |grep "State: RUNNING" |awk {'print $2'})
				# 		if [ "$managed_status" = "RUNNING" ]; then
				# 			break
				# 		fi
				# 	done
				# fi		
					echo $line
					echo "启动 weblogic managed ${wl_manager_server_port} 成功";
					echo $line
				else 
					echo $line
					echo "启动 weblogic managed ${wl_manager_server_port} 失败,停止执行"
					echo $line
					exit -1;
				fi	
			fi
		done
	fi
#=====================================================================
#							启动tomcat服务
#=====================================================================
elif [ "$server_type" = $TOMCAT_NAME ]; then
	tomcat_bin_path=$middleware_home/bin
	echo $line
	log_info "开始启动 tomcat，端口：$tomcat_server_port ...";
	echo $line
	tomcat_start_log=$tomcat_bin_path/${project_name}_start_${tomcat_server_port}.log
	if [ -f $tomcat_start_log ]; then
		mv  $tomcat_start_log  $log_path/$deployed_version/${project_name}_start_${tomcat_server_port}_${log_bak}.log
		if [ $? == 0 ]; then
			echo $line
			echo "备份启动日志 $tomcat_start_log 为${project_name}_start_${tomcat_server_port}_${log_bak}.log"
			echo $line
		fi
	else 
		echo $line
		echo " 首次启动无需备份tomcat启动日志"
		echo $line
	fi
	pid=$(ps -ef |grep java |grep  "tomcat" | grep ${tomcat_server_port} |awk '{print $2}' | xargs)
	if [ "$pid" ]; then 
		echo $line
		echo "tomcat已启动,请停止后再启动 pid=$pid";
		echo $line
	else 
		cd $tomcat_bin_path;
		if [ $? == 1 ]; then
			echo $line
			echo "操作失败：$tomcat_bin_path 目录不存在,停止执行"
			echo $line
			exit -1 ;
		fi
		str=$"\n"
		export JAVA_OPTS="${JAVA_OPTS} ${tomcat_user_mem_args} "
		export JAVA_OPTS="${JAVA_OPTS} -Dcom.sun.management.jmxremote=true "
		#export JAVA_OPTS="${JAVA_OPTS} -Dcom.sun.management.jmxremote.port=${tomcat_jmx_port} "
		export JAVA_OPTS="${JAVA_OPTS} -Dcom.sun.management.jmxremote.ssl=false "
		export JAVA_OPTS="${JAVA_OPTS} -Dcom.sun.management.jmxremote.authenticate=false"
		#ps进程中查不到端口号所以启动的时候增加自定义
		export JAVA_OPTS="${JAVA_OPTS} -Dtomcat.port=$tomcat_server_port"
		$tomcat_bin_path/startup.sh > $tomcat_start_log  &
		sstr=$(echo -e $str)
		sleep 5s
	fi
	pid=$(ps -ef |grep java |grep  "tomcat" | grep $tomcat_server_port |awk '{print $2}' | xargs)
	if [ "$pid" ]; then
		echo $line
		echo "启动tomcat 端口${tomcat_server_port}成功，pid=$pid"
		echo $line
	else
		echo $line 
		echo "启动tomcat 端口${tomcat_server_port}失败,pid=$pid"
		echo $line
	fi
#=====================================================================
#							启动springboot服务
#=====================================================================
elif [ "$server_type" = $SPRING_BOOT_NAME ]; then
	echo $line
	log_info "开始启动 spring boot，端口：$spring_boot_server_port ...";
	echo $line
	
	pid=$(ps -ef |grep java |grep  "$project_name" | grep $spring_boot_server_port |awk '{print $2}' | xargs)
	if [ "$pid" ]; then 
		echo $line
		log_info " spring boot 端口${spring_boot_server_port}已启动 pid=$pid";
		echo $line
		exit -1;
	else
		cd $deploy_path/$deployed_version
		if [ -f "${project_name}.jar" ]; then
			spring_boot_start_log=${project_name}_start_${spring_boot_server_port}.log
			if [ -f $spring_boot_start_log ]; then
				mv  $spring_boot_start_log  $log_path/$deployed_version/${project_name}_start_${spring_boot_server_port}_${log_bak}.log
				if [ $? == 0 ]; then
					echo $line
					echo "备份启动日志 $spring_boot_start_log 为${project_name}_start_${spring_boot_server_port}_${log_bak}.log"
					echo $line
				fi 
			else 
				echo $line
				echo " 首次启动无需备份springboot启动日志"
				echo $line
			fi

			#export OPTS=" -Dcom.sun.management.jmxremote.port=${springboot_jmx_port} "
			export OPTS="${OPTS} -Dcom.sun.management.jmxremote=true "
			export OPTS="${OPTS} -Dcom.sun.management.jmxremote.authenticate=false "
			export OPTS="${OPTS} -Dcom.sun.management.jmxremote.ssl=false "
			str=$"\n"
			cd $deploy_path/$deployed_version
			if [ -z "$jdk_path" ]; then 
				jdk_path="java"
				echo "使用系统默认JDK：${jdk_path}"
				${jdk_path} -version
			else 
				jdk_path="${jdk_path}/bin/java"
				echo "使用配置JDK：${jdk_path}"
				${jdk_path} -version
			fi
			
			${jdk_path} ${OPTS} ${spring_boot_jvm_params//"'"/""} -Dserver.port=$spring_boot_server_port -jar ${project_name}.jar ${spring_boot_active_params//"'"/""} >$spring_boot_start_log  2>&1 &
			sstr=$(echo -e $str)
			sleep 3s
		else 
			echo $line
			log_info "不存在${project_name}.jar文件，无法启动"
			exit -1
			echo $line
		fi
	fi
	pid=$(ps -ef |grep java |grep  "$project_name" | grep "\-Dserver.port\=$spring_boot_server_port" |awk '{print $2}' | xargs)
	if [ "$pid" ]; then
		echo $line
		log_info "启动 spring boot 端口${spring_boot_server_port}完成，pid=$pid,请刷新或查看启动日志"
		echo $line
	else 
		echo $line
		log_info "启动 spring boot 端口${spring_boot_server_port}失败,pid=$pid,请刷新或查看启动日志"
		echo $line
	fi
fi
