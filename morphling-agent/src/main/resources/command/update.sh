#!/bin/sh
# -------------------------------------------------------------------------------
$base_dir/backup.sh
echo_update_info(){
	md5=$(md5sum $app_path/$project_file   | awk '{print $1}')
	echo '{"where":'"'$deploy_path'"',"what":'"'$project_file'"','"'who'"':'"'$os_user'"','"'date'"':'"'$ymd'"',"time":'"'$time'"',"version":'"''"',"md5":'"'$md5'"'}' >> $update_path/$install_info
}
download(){
    log_info "开始下载补丁文件..."
#wget -q -O $update_path/$project_file  $download_url
curl -s -o $update_path/$project_file  $download_url
if [ $? == 0 ]; then
	log_info "从服务端下载补丁文件成功"
else
	log_error "从服务端下载补丁文失败"
	exit -1
fi
}
deploy_source_file_name=$project_file
decompress_files(){
    cd $update_path
if [ "${project_file##*.}" = "war" ]; then
	cp $project_file $deploy_path
    cd $deploy_path
    mv $project_file ${project_name}.war
    deploy_source_file_name="${project_name}.war"
elif [ "${project_file##*.}" = "jar"]; then
	cp $project_file $deploy_path
    cd $deploy_path
    mv $project_file ${project_name}.jar
    deploy_source_file_name="${project_name}.jar"
else
    log_info "开始解压程序$project_file文件..."
	deploy_source_file_name=$project_name
	if [ "${project_file##*.}" = "zip" ]; then
        unzip -q -d $deploy_path $project_file
	elif [ "${project_file##*.}" = "tar" ]; then
		tar -zxvf $project_file -C  $deploy_path
	elif [ "${project_file#*.}" = "tar.gz" ]; then
		tar -zxvf $project_file -C $deploy_path
	fi
    if [ $? == 0 ]; then
        log_info "解压程序文件成功"
    else 
        log_error "解压程序文件失败"
        exit -1
    fi
fi
}

cd $deploy_path
if [ "$server_type" = $WEBLOGIC_NAME ]; then
    if [ "$is_manager_server" = 1 ]; then
    download
    decompress_files
    log_info "开始部署补丁程序$project_name ..."
    sudo su - $os_user << !! 
        if [ "$is_stage" = 1 ]; then
            java -cp $middleware_home/wlserver/server/lib/weblogic.jar weblogic.Deployer -adminurl t3://$wl_admin_server_ip:$wl_admin_server_port -username $wl_username -password $wl_password -redeploy -name $project_name -source $deploy_path/$deploy_source_file_name -targets $wl_server_name -stage
        elif [ "$is_stage" = 0 ]; then
            java -cp $middleware_home/wlserver/server/lib/weblogic.jar weblogic.Deployer -adminurl t3://$wl_admin_server_ip:$wl_admin_server_port -username $wl_username -password $wl_password -redeploy  -name $project_name -source $deploy_path/$deploy_source_file_name -targets $wl_server_name -nostage
        fi
!!
    fi
# backup tomcat app
elif [ "$server_type" = $TOMCAT_NAME ]; then
    download
    decompress_files
    tomcat_webapp_path=$middleware_home/weapp
    sudo su - $os_user << !! 
    cp $deploy_path/deploy_source_file_name  $tomcat_webapp_path
!!
fi

if [ $? == 0 ]; then
	log_info "$project_name补丁更新部署成功"
    echo_update_info
else
	log_error "$project_name补丁更新部署失败"
fi
