import string
import os
import ConfigParser

configPath = os.path.join(sys.argv[1], "config.txt")
path = os.path.abspath(configPath)
print(path)
config = ConfigParser.ConfigParser()
config.read(path)

conf = {
  "TemplateFile": config.get('domain','TemplateFile'),
  "JavaHome": config.get('domain','JavaHome'),
  "DomainHome": config.get('domain','DomainHome'),
  "DomainName": config.get('domain','DomainName'),
  "ServerName": config.get('domain','ServerName'),
  "OverwriteDomain": 'true',
  "ServerStartMode": "dev", #prod, dev
  "User": config.get('domain','name'),
  "Password": config.get('domain','password'),
  "ListenPort": int(config.get('domain','ListenPort'))
#   "AdminServer": {
#     "ServerName": config.get('domain','ServerName'),
#     "ListenAddress": "",
#     "ListenPort": int(config.get('domain','ListenPort')),
#     "SSLEnabled": true,
#     "SSLListenPort": int(config.get('domain','SSLListenPort'))
#   },
#   "ManagedServers": [],
#   "JDBCSystemResources": [],
#   "AppDeployment": []
}

# print "================================================================================================="
# print "==    create weblogic domain with weblogic.WLST"
# print "================================================================================================="
readTemplate(conf["TemplateFile"])
#Configure the domain.
#Configure the Administration Server and SSL port.
# cd("/")
# create(conf["ServerName"], "AdminServer")
# cd("Servers/"+ conf["ServerName"])
cd('Servers/AdminServer')
set('Name',conf["ServerName"])
set('ListenAddress','')
set('ListenPort', conf["ListenPort"])
# create('AdminServer','SSL')
# cd('SSL/AdminServer')
# set('Enabled', 'True')
# set('ListenPort', 7002)
#Define the default user password.
cd('/')
cd("Security/base_domain/User/"+conf["User"])
cmo.setPassword(conf["Password"])
#Create a JMS Server.
# cd('/')
# create('myJMSServer', 'JMSServer')
#Create a JMS system resource.
# cd('/')
# create('myJmsSystemResource', 'JMSSystemResource')
# cd('JMSSystemResource/myJmsSystemResource/JmsResource/NO_NAME_0')

#Create a JMS queue and its subdeployment.
# myq=create('myQueue','Queue')
# myq.setJNDIName('jms/myqueue')
# myq.setSubDeploymentName('myQueueSubDeployment')
# cd('/')
# cd('JMSSystemResource/myJmsSystemResource')
# create('myQueueSubDeployment', 'SubDeployment')

#Create a JDBC data source, configure the JDBC driver, and create a new JDBC user.
# cd('/')
# create('myDataSource', 'JDBCSystemResource')
# cd('JDBCSystemResource/myDataSource/JdbcResource/myDataSource')
# create('myJdbcDriverParams','JDBCDriverParams')
# cd('JDBCDriverParams/NO_NAME_0')
# set('DriverName','com.pointbase.jdbc.jdbcUniversalDriver')
# set('URL','jdbc:pointbase:server://localhost/demo')
# set('PasswordEncrypted', 'PBPUBLIC')
# set('UseXADataSourceInterface', 'false')
# create('myProps','Properties')
# cd('Properties/NO_NAME_0')
# create('user', 'Property')
# cd('Property/user')
# cmo.setValue('PBPUBLIC')

# cd('/JDBCSystemResource/myDataSource/JdbcResource/myDataSource')
# create('myJdbcDataSourceParams','JDBCDataSourceParams')
# cd('JDBCDataSourceParams/NO_NAME_0')
# set('JNDIName', java.lang.String("myDataSource_jndi"))

# cd('/JDBCSystemResource/myDataSource/JdbcResource/myDataSource')
# create('myJdbcConnectionPoolParams','JDBCConnectionPoolParams')
# cd('JDBCConnectionPoolParams/NO_NAME_0')
# set('TestTableName','SYSTABLES')

#Target the resources.
# cd('/')
# assign('JMSServer', 'myJMSServer', 'Target', 'AdminServer')
# assign('JMSSystemResource.SubDeployment', 'myJmsSystemResource.myQueueSubDeployment', 'Target', 'myJMSServer')
# assign('JDBCSystemResource', 'myDataSource', 'Target', 'AdminServer')

#Save the domain.
setOption('OverwriteDomain', 'false')
#Save the JavaHome.
if len(conf["JavaHome"])!=0:
    setOption('JavaHome', conf["JavaHome"])
writeDomain(conf["DomainHome"] + "/" + conf["DomainName"])
#Close the current domain template.
closeTemplate()
#Exit WLST.
exit()
