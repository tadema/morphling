echo "================================================================================================="
echo "==   create weblogic domain with weblogic.WLST"
echo "================================================================================================="
wlServerHome=$wlServerHome
pydir=$basedir/app/morphling-agent/command/
. $wlServerHome/server/bin/setWLSEnv.sh
java weblogic.WLST $pydir/createDomain.py $pydir
