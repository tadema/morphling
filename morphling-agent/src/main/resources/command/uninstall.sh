#!/bin/bash
line="=========================================================="
#=====================================================================
#							卸载应用
#=====================================================================
$base_dir/backup.sh
#=====================================================================
#							weblogic卸载
#=====================================================================
if [ "$server_type" = $WEBLOGIC_NAME ]; then
		if [ "$is_admin_server" = 1 ]; then
sudo su - $os_user << !! 
			lib_dir="$wl_server_home/server/lib"
			java -cp \$lib_dir/weblogic.jar weblogic.Deployer -adminurl $wl_admin_server_ip:$wl_admin_server_port -username $wl_username -password $wl_password -undeploy  -name $project_name -targets $wl_server_name >> $log_file
!!
			if [ $? == 0 ]; then
				echo $line
				log_info "应用程序$project_name卸载成功"
				echo $line
			else
				echo $line
				log_error "应用程序$project_name卸载失败"
				echo $line
			fi
		fi
#=====================================================================
#							tomcat卸载
#=====================================================================
elif [ "$server_type" = $TOMCAT_NAME ]; then
	if [ -d "$tomcat_webapp_path/$project_name" ]; then
	sudo su - $os_user << !! 
	cd $tomcat_webapp_path
		rm -rf $project_name
!!
		if [ $? == 0 ]; then
			echo $line
			log_info "应用程序$project_name卸载成功"
			echo $line
		else
			echo $line
			log_error "应用程序$project_name卸载失败"
			echo $line
		fi
	fi
fi
