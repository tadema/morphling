#!/bin/bash

 if [ ! -f $exporter/$jmx_prometheus_javaagent ]; then 
    echo "$jmx_prometheus_javaagent不存在，请更新客户端程序"
    exit -1;
 fi
$base_dir/createJmxExporter.sh

if [ "$server_type" = $WEBLOGIC_NAME ]; then
    if [ "$is_admin_server" = 1 ]; then
        if [ ! -z $weblogic_admin_jmx_promet_port ]; then
            jmx_promet="-javaagent:$exporter/$jmx_prometheus_javaagent=$client_ip:${weblogic_admin_jmx_promet_port}:$exporter/weblogic/jmx_exporter_${wl_admin_server_port}.yml " 
            export JAVA_OPTIONS="${JAVA_OPTIONS} $jmx_promet "
        fi
    fi
    if [ "$is_manager_server" = 1 ]; then
        if [ ! -z $weblogic_managed_jmx_promet_port ]; then
            jmx_promet="-javaagent:$exporter/$jmx_prometheus_javaagent=$client_ip:${weblogic_managed_jmx_promet_port}:$exporter/weblogic/jmx_exporter_${wl_manager_server_port}.yml " 
            export JAVA_OPTIONS="${JAVA_OPTIONS} $jmx_promet "
        fi
    fi
elif [ "$server_type" = $TOMCAT_NAME ]; then 
    if [ ! -z $tomcat_jmx_promet_port ]; then
        jmx_promet="-javaagent:$exporter/$jmx_prometheus_javaagent=${tomcat_jmx_promet_port}:$exporter/tomcat/jmx_exporter_${tomcat_server_port}.yml " 
        export JAVA_OPTS="${JAVA_OPTS} $jmx_promet "
    fi
elif [ "$server_type" = $SPRING_BOOT_NAME ]; then
    if [ ! -z $springboot_jmx_promet_port ]; then
        jmx_promet="-javaagent:$exporter/$jmx_prometheus_javaagent=${springboot_jmx_promet_port}:$exporter/springboot/jmx_exporter_${spring_boot_server_port}.yml " 
        export OPTS="${OPTS} $jmx_promet "
    fi
fi
  
