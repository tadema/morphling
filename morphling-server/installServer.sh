#!/bin/bash
#########################################################################
# File APP: installServer.sh
# Author: du
# Created Time: 201901
# init install and start morphling server
# must makdir app
#install morphling-server into app dir
#1.	cd /home/{user}; mkdir app ; {user} is current user
#2.	ftp upload file "morphling.zip" to /home/{user}/app 
#########################################################################

function fileExists()
{
    if [ ! -f $1 ]; then
        echo "error file $1 is not exists , shell exit"
        exit -1;
    fi
}


#--files
morphling_zip="morphling.zip"
morphling_tar_gz="morphling-server.tar.gz"
jdk_tar_gz="jdk-8u191-linux-x64.tar.gz"
jdk_source_name="jdk1.8.0_191"
prometheus_tar_gz="prometheus-2.6.0.linux-amd64.tar.gz"
prometheus_source_name="prometheus-2.6.0.linux-amd64"
grafana_tar_gz="grafana-5.4.1.linux-amd64.tar.gz"
grafana_source_name="grafana-5.4.1"
grafana_db_zip="grafana.zip"

alertmanager_source_name="alertmanager-0.16.0.linux-amd64"
alertmanager_tar_gz="alertmanager-0.16.0.linux-amd64.tar.gz"


#--dirs
APP_DIR=$(cd `dirname $0`;pwd)
#/home/{user}/app/data/morphling-server
DATA_MORPHLING_SERVER_DIR=$APP_DIR/data/morphling-server
#/home/{user}/app/morphling-server
MORPHLING_SERVER_DIR=$APP_DIR/morphling-server
MORPHLING_SERVER_COMMAND_DIR=$APP_DIR/morphling-server/command
#/home/{user}/app/morphling-server/prometheus
PROMETHEUS_DIR=$MORPHLING_SERVER_DIR/prometheus
ALERTMANAGER_DIR=$MORPHLING_SERVER_DIR/alertmanager
#/home/{user}/app/morphling-server/grafana
GRAFANA_DIR=$MORPHLING_SERVER_DIR/grafana
GRAFANA_DATASOURCES_DIR=$MORPHLING_SERVER_DIR/grafana/conf/provisioning/datasources

#--install server
echo "start install server ..."
#fileExists $morphling_zip
# unzip -o $morphling_zip
cd morphling
cp -R data $APP_DIR
rm -rf data
mv  $morphling_tar_gz $APP_DIR
cd $APP_DIR
tar -zxvf $morphling_tar_gz

#/home/deploy/app/data/morphling-server
cd $DATA_MORPHLING_SERVER_DIR

#--install jdk1.8
echo "start install jdk1.8 ..."
tar -zxvf $jdk_tar_gz -C $APP_DIR
mv $APP_DIR/jdk1.8.0_191/ $APP_DIR/jdk1.8
echo "install jdk1.8 over..."

#--install prometheus
echo "start install prometheus ..."
tar -zxvf $prometheus_tar_gz
mv $prometheus_source_name $PROMETHEUS_DIR
echo "copy startPrometheus.sh... "
cp $MORPHLING_SERVER_COMMAND_DIR/startPrometheus.sh $PROMETHEUS_DIR
echo "install prometheus over..."

 #--install grafana
echo "start install grafana ..."
tar -zxvf $grafana_tar_gz
mv $grafana_source_name $GRAFANA_DIR
unzip -q -o -d $GRAFANA_DIR $grafana_db_zip
echo "copy startGrafana.sh... "
cp $MORPHLING_SERVER_COMMAND_DIR/startGrafana.sh $GRAFANA_DIR
echo "copy grafana.sh... "
cp $MORPHLING_SERVER_COMMAND_DIR/grafana.ini $GRAFANA_DIR
echo "copy datasources.yml... "
cp $MORPHLING_SERVER_COMMAND_DIR/datasources.yml $GRAFANA_DATASOURCES_DIR
echo "install grafana over..."

#--install alertmanager
echo "start install alertmanager ..."
tar -zxvf $alertmanager_tar_gz
mv $alertmanager_source_name $ALERTMANAGER_DIR
cp $MORPHLING_SERVER_COMMAND_DIR/alertmanager.yml $ALERTMANAGER_DIR
echo "copy startAlertmanager.sh... "
cp $MORPHLING_SERVER_COMMAND_DIR/startAlertmanager.sh $ALERTMANAGER_DIR
echo "install alertmanager over..."

#--mail html template 
# cd $ALERTMANAGER_DIR
# mkdir template
# cd  template
# cat > email.tmpl << EOF 
# {{ define "test.html" }}
# <table border="1">
#         <tr>
#                 <td>报警项</td>
#                 <td>实例</td>
#                 <td>报警阀值</td>
#                 <td>开始时间</td>
#         </tr>
#         {{ range $i, $alert := .Alerts }}
#                 <tr>
#                         <td>{{ index $alert.Labels "alertname" }}</td>
#                         <td>{{ index $alert.Labels "instance" }}</td>
#                         <td>{{ index $alert.Annotations "value" }}</td>
#                         <td>{{ $alert.StartsAt }}</td>
#                 </tr>
#         {{ end }}
# </table>
# {{ end }}
# EOF


if [ $? != 0 ]; then
    echo "程序安装失败!"
    exit 1
else
    echo "程序安装成功!"
    rm -r  $APP_DIR/morphling
    rm -f  $APP_DIR/$morphling_tar_gz
fi

chmod -R 755 $MORPHLING_SERVER_DIR