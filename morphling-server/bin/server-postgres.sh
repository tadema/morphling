#!/bin/bash
cd `dirname $0`
dir1=$(cd `dirname $0`;pwd)
#echo "$dir1"
cd ../
dir2=$(cd `dirname $0`;pwd)
#echo $dir2
java="$dir2/jdk1.8/bin/java"
cd $dir1

if [ -f "$java" ]; then
 echo change jdk to 1.8
 export JAVA_HOME=$dir2/jdk1.8
 export JRE_HOME=$JAVA_HOME/jre
 export CLASSPATH=.:$JAVA_HOME/lib:$JRE_HOME/lib:$CLASSPATH
 export PATH=$JAVA_HOME/bin:$JRE_HOME/bin:$PATH
 java -version
fi

# 使用自己的conf文件传递到spring启动脚本种
source ./conf/run.conf


DEPLOY_DIR=`pwd`

SERVER_NAME=morphling-server

if [ -z "$SERVER_NAME" ]; then
    echo 'package error,cant get server name!'
    exit 1;
fi

LOGS_DIR=../logs/morphling-server
if [ -z "$LOGS_DIR" ]; then
    LOGS_DIR=../logs/morphling-server/
fi
if [ ! -d ${LOGS_DIR} ]; then
    mkdir -p ${LOGS_DIR}
fi

PID_DIR=../data/run
if [ -z "$PID_DIR" ]; then
    PID_DIR=../data/run/
fi
if [ ! -d ${PID_DIR} ]; then
    mkdir -p ${PID_DIR}
fi

if [ $ENV = "product" ]; then
    # 线上环境增加全异步日志
    # JAVA_OPTS=$JAVA_OPTS" -DLog4jContextSelector=org.apache.logging.log4j.core.async.AsyncLoggerContextSelector -DAsyncLogger.WaitStrategy=busyspin"
    JAVA_OPTS=$JAVA_OPTS" -DLog4jContextSelector=org.apache.logging.log4j.core.async.AsyncLoggerContextSelector"
fi

case "$1" in
    start)
     nohup java -jar ${JAVA_OPTS} morphling-server.jar --spring.profiles.active=postgres &
    ;;
    stop)
     ./ morphling-server.jar stop
    ;;
    status)
      ./morphling-server.jar status
    ;;
    restart)
      ./morphling-server.jar restart
    ;;
    dump)
        do_dump
    ;;
    *)
        echo "Usage ${0} <start|stop|status|restart|dump>"
        exit 1
    ;;
esac
