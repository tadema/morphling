#!/bin/bash
line="=========================================================="
mkdirFunction()
{
	if [ ! -d "$1" ]; then
		 mkdir -p $1;
		if [ ! -d "$1" ]; then
			echo $line
			echo "创建$1 目录失败！"
			echo $line
			exit -1;
		fi
	fi
}

#/home/deploy/app/morphling-server
export SYSTEM="client"
export WEBLOGIC_JMX="weblogic-jmx"
export TOMCAT_JMX="tomcat-jmx"
export SPRING_BOOT_JMX="springboot-jmx"
export SHELL_LOG_FILE="shell.log"
export ORACLE_EXPORTER="oracle_exporter"
export MYSQL_EXPORTER="mysql_exporter"
export POSTGRES_EXPORTER="postgres_exporter"
export REDIS_EXPORTER="redis_exporter"
#变量
#是否监控标志1：是，0：否
export is_monitor=$is_monitor
#监控定义中的监控类型
export MONITOR_TYPE=$monitor_type
#目标的监控IP和端口
export IP=$ip
export PORT=$port
#prometheus服务器IP和PORT
export PROMETHEUS_IP=$prometheus_ip
export PROMETHEUS_PORT=$prometheus_port

export -f mkdirFunction

#command dir
COMMAND_DIR=$(cd `dirname $0`;pwd)
SERVER_DIR=$(cd `dirname $COMMAND_DIR`;pwd)

#echo $SERVER_DIR/prometheus
export prometheus_config_dir=$SERVER_DIR/prometheus/config
export prometheus_config_client_dir=$SERVER_DIR/prometheus/config/client
export prometheus_config_tomcat_jmx_dir=$SERVER_DIR/prometheus/config/tomcat-jmx
export prometheus_config_weblogic_jmx_dir=$SERVER_DIR/prometheus/config/weblogic-jmx
export prometheus_config_springboot_jmx_dir=$SERVER_DIR/prometheus/config/springboot-jmx
export prometheus_config_oracle_exporter_dir=$SERVER_DIR/prometheus/config/db_exporter/oracle-exporter
export prometheus_config_mysql_exporter_dir=$SERVER_DIR/prometheus/config/db_exporter/mysql-exporter
export prometheus_config_postgres_exporter_dir=$SERVER_DIR/prometheus/config/db_exporter/postgres-exporter
export prometheus_config_redis_exporter_dir=$SERVER_DIR/prometheus/config/db_exporter/redis-exporter
export prometheus_config_alertmanager_dir=$SERVER_DIR/prometheus/config/alertmanager
export prometheus_config_rule_dir=$SERVER_DIR/prometheus/config/rule

mkdirFunction $prometheus_config_dir
mkdirFunction $prometheus_config_tomcat_jmx_dir
mkdirFunction $prometheus_config_client_dir
mkdirFunction $prometheus_config_weblogic_jmx_dir
mkdirFunction $prometheus_config_springboot_jmx_dir
mkdirFunction $prometheus_config_oracle_exporter_dir
mkdirFunction $prometheus_config_mysql_exporter_dir
mkdirFunction $prometheus_config_postgres_exporter_dir
mkdirFunction $prometheus_config_redis_exporter_dir

mkdirFunction $prometheus_config_alertmanager_dir
mkdirFunction $prometheus_config_rule_dir
