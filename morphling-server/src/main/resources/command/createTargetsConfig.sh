#!/bin/bash
#================================================
#创建分片配置文件，每个监控节点一个配置
#参数$1:配置文件名称
#参数$2:{IP:PORT}
#================================================
#echo "start exec createTargetsConfig.sh" >>  $LOG_FILE
echo "参数：$1,$2,$is_monitor"
if [ $is_monitor = 1 ]; then
cat > $1 << EOF 
  - targets:
    - $2
EOF

else
  if [ -f $1 ]; then 
  echo "rm file $1"
    rm -f $1
#   else
# cat > $1 << EOF 
#   - targets:
#     -
# EOF
  fi
fi
