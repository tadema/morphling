#!/bin/bash
echo "执行停止命令..."
if [ "$1" = "prometheus" ]; then
    pid=$(ps -ef |grep prometheus |grep config |awk '{print $2}' | xargs)
    echo "pid:$pid"
    if [ "$pid" ]; then
      kill -9 $pid
      echo "kill pid : $pid"
    fi
fi

if [ "$1" = "grafana" ]; then
    pid=$(ps -ef |grep grafana |grep config |awk '{print $2}' | xargs)
    echo "pid:$pid"
    if [ "$pid" ]; then
        kill -9 $pid
         echo "kill pid : $pid"
    fi
fi
if [ "$1" = "alertmanager" ]; then
    pid=$(ps -ef |grep alertmanager |grep config |awk '{print $2}' | xargs)
    echo "pid:$pid"
    if [ "$pid" ]; then
        kill -9 $pid
         echo "kill pid : $pid"
    fi
fi
