#!/bin/bash
#git
# tar -zxvf git-2.9.5.tar.gz
# cd git-2.9.5
# ./configure prefix=/app/git/
# make && make install
# echo "export PATH=$PATH:/app/git/bin" >> ~/.bash_profile
# echo "export PATH=$PATH:/app/git/libexec/git-core" >> ~/.bash_profile
# source  ~/.bash_profile

#mvn
# tar -zxvf apache-maven-3.6.0-bin.tar.gz
# echo "export PATH=$PATH:/app/apache-maven-3.6.0/bin" >> ~/.bash_profile
# source  ~/.bash_profile

#line="=========================================================="

START_TIME=`date +%s`
#应用名称
project_name=${project_name}
#程序版本
package_version=${package_version}
#git地址
git_url=${git_url}
#分支名
git_branch=${git_branch}
git_user=${git_user}
git_pwd=${git_pwd}
#模块名
mvn_module=${mvn_module}
#mvn命令参数
mvn_command=${mvn_param}
#mvn_args=${mvn_param}
pkg_suffix=${pkg_suffix}


# git_tmp_name=${git_url##*/}
# git_project_name=${git_tmp_name%.*}

#校验
echo $line
echo "检查服务器Git软件版本:";
echo $line
sleep 0.5
git --version

if [ $? != 0 ]; then
    echo '失败:未找到Git命令,请先安装Git软件环境!'
    exit 1
fi
echo $line
echo "检查服务器Maven软件版本:";
echo $line
sleep 0.5
#export
mvn -v

if [ $? != 0 ]; then
    echo '失败:未找到Maven命令,请安装Maven软件环境!'
    exit 1
fi

# if [ -z "${git_url}" ]; then
#     echo '失败:git url is empty... '
#     exit 1
# fi
# if [ -z "${git_branch}" ]; then
#     echo 'Error:git branch is empty... '
#     exit 1
# fi

#当前目录
export CURRENT_DIR=$(cd `dirname $0`;pwd)
#上层目录
tmp_dir_a=${CURRENT_DIR%/*}
#上层目录
tmp_dir_b=${tmp_dir_a%/*}
#/home/deploy
export BASE_DIR=${tmp_dir_b%/*}
#echo "app目录:$BASE_DIR"

#git源码仓库目录
GIT_SOURCE_HOME=${BASE_DIR}/workspace/server/git_source
#项目源码目录
GIT_PROJECT_DIR=${GIT_SOURCE_HOME}/${project_name}

#打包目录
PACKAGE_HOME=${BASE_DIR}/workspace/server/update/${project_name}
VERSION_HOME=${BASE_DIR}/workspace/server/update/${project_name}/${package_version}

echo $line
echo "开始从Git服务器拉取${git_branch}分支项目源码:";
echo "拉取源码存储目录:$GIT_PROJECT_DIR"
echo $line

if [ ! -d ${GIT_SOURCE_HOME} ]; then
    mkdir -p ${GIT_SOURCE_HOME}
fi

if [ ! -d ${PACKAGE_HOME} ]; then
    mkdir -p ${PACKAGE_HOME}
fi

if [ ! -d ${VERSION_HOME} ]; then
    mkdir -p ${VERSION_HOME}
fi

cd $GIT_SOURCE_HOME

if [ -d ${GIT_PROJECT_DIR} ]; then
    rm -rf ${GIT_PROJECT_DIR}
fi

if [ -z "${git_user}" -o -z "${git_pwd}" ]; then
    git clone ${git_url} ${project_name}
else
    git clone ${git_url%%//*}//${git_user}:${git_pwd}@${git_url#*//} ${project_name}
fi

if [ $? != 0 ]; then
    echo $line
    echo "错误:拉取${git_branch}分支项目失败,打包失败!"
    echo $line
    exit 1
fi

#source /etc/profile

#切换分支
if [ ! -z "${git_branch}" ];then
    echo $line
    echo "切换到指定分支:${git_branch}"
    echo $line
    cd ${project_name}
    git checkout ${git_branch}
    if [ $? != 0 ]; then
        echo "错误:Git切换分支${git_branch}失败,打包失败!"
        exit 1
    fi
    git branch
fi

#查看是否clone成功
if [ ! -d ${GIT_PROJECT_DIR} ]; then
    echo $line
	echo "错误:Git项目目录不存在,代码拉取失败，打包失败!"
    echo $line
	exit 1
fi

echo $line
echo "使用Maven工具对源码进行构建...";
echo $line
cd ${GIT_PROJECT_DIR}

#检查pom.xml文件
if [ ! -f pom.xml ]; then
    echo "错误:pom.xml 文件不存在,打包失败!"
    exit 1
fi

#若没有命令参数 默认mvn命令
if [[ -z ${mvn_command} ]];then
    mvn_command=" mvn clean package -U -Dmaven.test.skip=true -q "
fi

echo $line
echo "本次执行的Maven命令为:${mvn_command}"
echo $line

$mvn_command -q

if [ $? != 0 ]; then
    echo $line
    echo "错误:Maven构建执行失败,打包失败!"
    echo $line
    exit 1
fi

if [ -d ${GIT_PROJECT_DIR}/target ]; then
    cd ${GIT_PROJECT_DIR}/target
    #maven构建成功后再update下版本目录
    echo $line
    echo "将Target下的程序包文件拷贝到${VERSION_HOME}目录下"
    echo $line
    if [ -f ${project_name}*.${pkg_suffix} ]; then
        if [ ! -d ${VERSION_HOME} ]; then
            mkdir -p ${VERSION_HOME}
        fi
        cp ${project_name}*.${pkg_suffix}  ${VERSION_HOME}
    fi
else
    echo $line
    echo "错误:未找到编译后的Target目录,打包失败!"
    echo $line
    exit 1
fi


echo $line
END_TIME=`date +%s`
echo "执行时长: $((END_TIME-START_TIME)) s"
echo $line

exit 0


# echo "执行部署命令"
# echo $mvn_module
# if [ ! -z "${mvn_module}" ]; then

# cat > ./tmp.sh << EOF
#  #!/bin/bash
#  ${mvn_module}
# EOF
# sh tmp.sh
#     #echo ${mvn_module}|awk '{run=$0;system(run)}'
# fi
#################################################
# if [ -d "${mvn_module}" ]; then
#     cd ${mvn_module}
#     if [ ! -d target ]; then
#         echo "当前pom文件目录下，未找到${mvn_module}/target目录，构建失败，操作中断"
#         exit -1
#     fi
#     cd target
# else
#     if [ ! -d target ]; then
#         echo "未找到${mvn_module}/target目录，构建失败，操作中断"
#         exit -1
#     fi
#     cd target
# fi

# if [ ! -f ${project_name}*.${pkg_suffix} ];  then
#     echo "未找到程序文件，构建失败，操作中断"
#     exit -1
# fi

#cp ${project_name}*.${pkg_suffix}  ${VERSION_HOME}
##################################
#echo "待部署文件目录：${VERSION_HOME}"


# if [ -f ${project_name}*.${pkg_suffix} ]; then
#     echo "move package to update dir : ${VERSION_HOME} "
#     cp ${project_name}*.${pkg_suffix}  ${VERSION_HOME}
# fi

# cd ${PACKAGE_HOME}
# mv  ${project_name}*.${pkg_suffix} ${project_name}.${pkg_suffix}

#\cp -pr -f ${project_name}.tar.gz ${PACKAGE_HOME}/${project_name}_${git_branch}.tar.gz
# \cp -pr -f ${project_name}.tar.gz ${PACKAGE_HOME}/${project_name}.tar.gz


# if [ ! -d ${GIT_PROJECT_DIR} ]; then
#     git clone -b ${git_branch} ${git_url} ${project_name}
# else
#     cd ${GIT_PROJECT_DIR}
#     git checkout -f ${git_branch}
#     git pull origin ${git_branch}
#     git reset --hard HEAD
# fi

# if [ ! -d ${GIT_PROJECT_DIR} ]; then
#     echo ">>> init project..."
#     mkdir -p ${GIT_PROJECT_DIR}
#     cd ${GIT_PROJECT_DIR}
#     git init ${GIT_PROJECT_DIR}
#     git fetch --tags --progress ${git_url} +refs/heads/*:refs/remotes/origin/*
#     git config remote.origin.url ${git_url} # timeout=10
#     git config --add remote.origin.fetch +refs/heads/*:refs/remotes/origin/* # timeout=10
#     git config core.sparsecheckout # timeout=10
# fi
# cd ${GIT_PROJECT_DIR}
# git fetch --tags --progress ${git_url} +refs/heads/*:refs/remotes/origin/*
# git checkout -f ${git_branch}
# git pull origin ${git_branch}
# git reset --hard HEAD
