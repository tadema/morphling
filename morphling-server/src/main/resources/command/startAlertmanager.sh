#!/bin/bash

port=9093
if [ -z $1 ]; then 
    echo "default port is '9093' !"
else 
    port=$1
fi

nohup ./alertmanager --web.listen-address=":$port" --config.file=alertmanager.yml > start.log & 
