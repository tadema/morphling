#!/bin/bash
echo "开始启动服务..."
command_dir=$(cd `dirname $0`;pwd)
#source ./command/common.sh
if [ "$1" = "prometheus" ]; then
    pid=$(ps -ef |grep prometheus |grep config |awk '{print $2}' | xargs)
    if [ "$pid" ]; then
        echo "prometheus服务已启动"
        exit -1;
    fi
    cd prometheus
    ./startPrometheus.sh > startPrometheus.log 2>&1 &
    # pid1=$(ps -ef |grep prometheus |grep config |awk '{print $2}' | xargs)
    # echo "pid:"$pid1
    echo "服务启动完成..."
fi

if [ "$1" = "grafana" ]; then
    pid=$(ps -ef |grep grafana |grep config |awk '{print $2}' | xargs)
    if [ "$pid" ]; then
        echo "grafana服务已启动"
        exit -1;
    fi
	cd grafana
	#sh startGrafana.sh 
    ./bin/grafana-server -config grafana.ini start > startGrafana.log 2>&1 &
    # pid1=$(ps -ef |grep grafana |grep config |awk '{print $2}' | xargs)
    # echo "pid:"$pid1
    echo "服务启动完成..."
fi

if [ "$1" = "alertmanager" ]; then
    pid=$(ps -ef |grep alertmanager |grep config |awk '{print $2}' | xargs)
    if [ "$pid" ]; then
        echo "alertmanager服务已启动"
        exit -1;
    fi
	cd alertmanager
    ./startAlertmanager.sh  > startAlertmanager.log 2>&1 &
    # pid1=$(ps -ef |grep alertmanager |grep config |awk '{print $2}' | xargs)
    #  echo "pid:"$pid1
    echo "服务启动完成..."
fi
