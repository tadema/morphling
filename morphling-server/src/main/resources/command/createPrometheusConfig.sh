#!/bin/bash
####创建监控配置入口程序
export COMMAND_DIR=$(cd `dirname $0`;pwd)
chmod 755 $COMMAND_DIR/*.sh
source $COMMAND_DIR/common.sh
export LOG_FILE=$COMMAND_DIR/$SHELL_LOG_FILE
echo "is_monitor:$is_monitor monitor_type:$monitor_type ip:$ip port:$port prometheus_ip:$prometheus_ip prometheus_port:$prometheus_port" >  $LOG_FILE
$COMMAND_DIR/createConfig.sh >> $LOG_FILE 2>&1 &
