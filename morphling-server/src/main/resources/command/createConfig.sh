#!/bin/bash
#================================================
#创建Prometheus分片配置文件主脚本
#参数$IP:监控目标的IP
#参数$PORT:监控目标的监控端口
#参数$PROMETHEUS_IP:Prometheus服务器IP
#参数$PROMETHEUS_$PORT:Prometheus服务器端口
#================================================
echo "start exec createConfig.sh" >> $LOG_FILE
# SERVER_DIR=$PWD

#linux
if [ "$MONITOR_TYPE" = $SYSTEM ]; then
  target_file=$prometheus_config_client_dir/${IP}_${PORT}.yml
   $COMMAND_DIR/createTargetsConfig.sh $target_file ${IP}:${PORT}
#jmx
elif [ "$MONITOR_TYPE" = $WEBLOGIC_JMX ]; then
  target_file=$prometheus_config_weblogic_jmx_dir/${IP}_${PORT}.yml
   $COMMAND_DIR/createTargetsConfig.sh $target_file ${IP}:${PORT}
elif [ "$MONITOR_TYPE" = $TOMCAT_JMX ]; then 
  target_file=$prometheus_config_tomcat_jmx_dir/${IP}_${PORT}.yml
   $COMMAND_DIR/createTargetsConfig.sh $target_file ${IP}:${PORT}
elif [ "$MONITOR_TYPE" = $SPRING_BOOT_JMX ]; then
  target_file=$prometheus_config_springboot_jmx_dir/${IP}_${PORT}.yml
   $COMMAND_DIR/createTargetsConfig.sh $target_file ${IP}:${PORT}
#db
elif [ "$MONITOR_TYPE" = $ORACLE_EXPORTER ]; then
  target_file=$prometheus_config_oracle_exporter_dir/${IP}_${PORT}.yml
   $COMMAND_DIR/createTargetsConfig.sh $target_file ${IP}:${PORT}
elif [ "$MONITOR_TYPE" = $MYSQL_EXPORTER ]; then
  target_file=$prometheus_config_mysql_exporter_dir/${IP}_${PORT}.yml
   $COMMAND_DIR/createTargetsConfig.sh $target_file ${IP}:${PORT}
elif [ "$MONITOR_TYPE" = $POSTGRES_EXPORTER ]; then
  target_file=$prometheus_config_postgres_exporter_dir/${IP}_${PORT}.yml
   $COMMAND_DIR/createTargetsConfig.sh $target_file ${IP}:${PORT}
elif [ "$MONITOR_TYPE" = $REDIS_EXPORTER ]; then
  target_file=$prometheus_config_redis_exporter_dir/${IP}_${PORT}.yml
   $COMMAND_DIR/createTargetsConfig.sh $target_file ${IP}:${PORT}
fi
$COMMAND_DIR/reload.sh $PROMETHEUS_IP $PROMETHEUS_PORT 5
