#!/bin/bash
#================================================
#Prometheus服务已启动脚本
#参数$1:启动端口号,默认为9090
#此文件在：/home/deploy/app/data/morphling-server目录下存放
#================================================
port=9090
if [ -z $1 ]; then 
    echo "default port is '9090' !"
else 
    port=$1
fi

cat > prometheus.yml << EOF 

# my global config
global:
  scrape_interval:     15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
  evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.
  # scrape_timeout is set to the global default (10s).

# Alertmanager configuration
alerting:
  alertmanagers:
  # - static_configs:
  #   - targets:
  #     - 192.168.129.164:9093
  - file_sd_configs:
    - files: ['config/alertmanager/*.yml']
    #- files:
      #- config/alertmanager/*.yml

rule_files:
  # - "first_rules.yml"
  # - "second_rules.yml"
  - "config/rule/*.yml"

scrape_configs:
  - job_name: 'prometheus'
    scrape_interval: 10s
    scrape_timeout: 10s
    metrics_path: /metrics
    scheme: http
    # metrics_path defaults to '/metrics'
    # scheme defaults to 'http'.

    static_configs:
    - targets: ['localhost:$port']

  - job_name: 'tomcat-jmx'
    scrape_interval: 10s
    scrape_timeout: 10s
    metrics_path: /metrics
    scheme: http
    file_sd_configs:
      - files: ['config/tomcat-jmx/*.yml']

  - job_name: 'weblogic-jmx'
    scrape_interval: 10s
    scrape_timeout: 10s
    metrics_path: /metrics
    scheme: http
    file_sd_configs:
      - files: ['config/weblogic-jmx/*.yml']

  - job_name: 'springboot-jmx'
    scrape_interval: 10s
    scrape_timeout: 10s
    metrics_path: /metrics
    scheme: http
    file_sd_configs:
      - files: ['config/springboot-jmx/*.yml']

  - job_name: 'client'
    scrape_interval: 10s
    scrape_timeout: 10s
    metrics_path: /metrics
    scheme: http
    file_sd_configs:
      - files: ['config/client/*.yml']
 
EOF


nohup ./prometheus  --web.listen-address="0.0.0.0:$port" --web.enable-lifecycle --config.file=prometheus.yml > start.log & 
