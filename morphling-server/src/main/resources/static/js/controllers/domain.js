'use strict';

// 域控制器
let DomainController = app.controller('DomainController', ['$scope','$rootScope','$modal','$state','dbUtils','EchoLog','DomainService','$sce',
                                    function($scope,$rootScope,$modal,$state,dbUtils,echoLog,domainService,$sce) {
	function setAllHidden(){
		$scope.isShowMain = false;
		$scope.isShowConsole = false;
	}
	setAllHidden();
	$scope.isShowMain = true;
	
	$scope.$on('returnMain',function(event,data){
		setAllHidden();
		$scope.isShowMain = true;
		if(data && data.reloadData)
			$scope.table.operations.reloadData();
	})
	
	$scope.t_id = dbUtils.guid();
	let healthCheckRows = [];
	$scope.temp = {"status":{}};
    var table = {
    	url:"/domain/listAll", 
    	showUpload: false,
        pageSql:true,
		showText:true,waringText:"控制台中可以执行创建域、受管以及服务的启停操作",
        params:[
            {name:"domainName",label:"域名称",labelCols:4,type:"text"}
        ],
        headers: [
            {name:"域名称",field:"domainName",style:{width:"12%"}},
            {name:"管理服务名",field:"adminName",style:{width:"10%"}},
            {name:"状态",field:"status",style:{"width":"6%"},compile:true,formatter:function(value,row){
            	healthCheckRows.push(row);
            	$scope.temp.status[row.id] = 2;
            	return "<span ng-if='temp.status["+row.id+"] == 2'><i class='fa fa-spin fa-spinner '></i></span>" + 
            		"<span ng-if='temp.status["+row.id+"] != 2'><i style='margin-right:5px;' class='fa fa-circle' ng-class='{1:\"text-success\",0:\"text-danger\"}[temp.status["+row.id+"]]'></i>{{{1:\"运行中\",0:\"已停止\"}[temp.status["+row.id+"]]}}</span>";
            }},
            {name:"主机名",field:"hostName",style:{width:"14%"}},
            {name:"WebLogic版本",field:"weblogicVersion",style:{width:"9%"}},
	        {name:"创建用户",field:"createUsername",style:{width:"8%"}},
            {name:"创建时间",field:"createTime",style:{width:"8%"}},
            {name:"描述",field:"description",style:{width:"10%"}},
        ],
        operationEvents: [{
            class:"btn-info",id:"addDomain",icon:"glyphicon glyphicon-plus",name:"配置域",
            click:function(){
            	dbUtils.openModal('tpl/app/domain_edit.html','DomainEditController',"lg","",{},function (right) {
            		if(right){
                	    $scope.table.operations.reloadData()
                	}
            	});
            }
        }],
        rowEvents:[{
        	class:"btn-info",
            icon:"fa fa-pencil",
            title:"控制台",
            click: function(row){
            	setAllHidden();
            	$scope.isShowConsole = true;
            	$scope.$broadcast("sentDataToConsole",row);
            }
        },{
        	class:"btn-info",
            icon:"fa fa-pencil",
            title:"编辑域",
            click: function(row){
            	dbUtils.openModal('tpl/app/domain_edit.html','DomainEditController',"lg","",angular.copy(row),function (right) {
            		if(right)
            			$scope.table.operations.reloadData()
            	});
            }
        },{
        	class:"btn-info",
            icon:"fa fa-pencil",
            title:"编辑受管",
            click: function(row){
            	dbUtils.openModal('tpl/app/managed_server_edit.html','ManagedServerEditController',"lg","",angular.copy(row),function (right) {
					$scope.table.operations.reloadData();
            	});
            }
        },{
        	class:"btn-danger",
            icon:"fa fa-trash",
            title:"删除",
            isShow:function(row){
                return row.username != "admin";
            },
            isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	domainService.deleteDomain(row.id).then(function(response){
            		if(response.code == SUCCESS_CODE){
                        dbUtils.success("记录删除成功","提示");
                        $(event.target).parents(".popover").prev().click();
                        $scope.table.operations.reloadData()
                    }else {
                        dbUtils.error(response.message,"提示");
                    }
              	})
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        settings:{
            cols:2,
            showCheckBox:false,
            filterId: "domainFilter",
            operationId: "domainOperation",
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-34-56-20)+"px",
            },
        },
        afterReload:function(){
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight21+"px";
        	},200)
			$scope.$emit("pageLoadSuccess",{"name":"domain"});
        	healthCheckHandle();
        }
    }
    $scope.table = table;
    
    function healthCheckHandle(){
    	domainService.checkStatusAll(healthCheckRows).then(function(response){
    		if(response.code == SUCCESS_CODE){
    			let respData = response.data;
    			for(let item of healthCheckRows){
					item.status = respData[item.id] == true ? 1 : 0;
    				$scope.temp.status[item.id] = respData[item.id] == true ? 1 : 0;
    			}
			}else{}
		});
    }
    
}]);

// 域修改
let DomainEditController = app.controller('DomainEditController',
		['$scope','$rootScope','$modalInstance','$compile','$modal','dbUtils','source','DomainService',
		 function($scope,$rootScope,$modalInstance,$compile,$modal,dbUtils,source,domainService) {	
			
	//主机
	$scope.client = {};
	$scope.managedServer = new Object();
	$scope.monitorParams = [{key:0,value:"否"},{key:1,value:"是"}];
	// 控制显示“新建”还是“编辑”
    if(!source.id){
        $scope.editData = false;
        $scope.domain = {isAdminMonitor:0,weblogicVersion:"11g"};
        $scope.managedServer = [];
    }else{
        $scope.editData = true;
        $scope.domain = source;
        $scope.managedServer = $scope.domain.managedServers;
    }
    canRemove();
    
    function setClientForManaged(){
    	for(let item of $scope.managedServer){
    		if(item.isAdmin == 1)
    			continue;
    		
			for(let t of $scope.host){
				if(item.deployFileId == t.id){
					item.client = t;
					break;
				}
			}
    	}
    }
    
    //查询agent主机
    domainService.getAgentPost().then(function(response){
        if (response.code == SUCCESS_CODE) {
        	$scope.host = response.data; 
        	if($scope.editData == true){
        		for(let item of $scope.host){
        			if(item.username == $scope.domain.osUsername && item.hostAddress == $scope.domain.adminIp){
        				$scope.client = item;
        				break;
        			}
        		}
        		setClientForManaged();
        	}
        } else {
        	$scope.host = []; 
        }
  	})
    
    //主机下拉框change时间
    $scope.clientChangeHandle = function () {
    	$scope.domain.hostName = $scope.client.name;
 		$scope.domain.hostIp = $scope.client.hostAddress;
 		$scope.domain.adminIp = $scope.client.hostAddress;
 		$scope.domain.osUsername = $scope.client.username;
 		$scope.domain.osPassword = $scope.client.password;
    }; 

    function canRemove(){
    	let length;
    	if($scope.editData == true){
    		length = $scope.managedServer.length - 1; 
    	}else{
    		length = $scope.managedServer.length;
    	}
    	
    	if (length <= 0) {
            $scope.canRemove = false;
        }else{
        	$scope.canRemove = true;
        }
    }
    // 增加回复数
	$scope.incre = function($index) {
        $scope.managedServer.splice($index, 0,
            {key: new Date().getTime(), value: "",id:null,domainId:null,isAdmin:0,isMonitor:0});   // 用时间戳作为每个item的key
        canRemove();
    }

    // 减少回复数
    $scope.remove = function($index) {
    	if($scope.canRemove){
    		$scope.managedServer.splice($index, 1);
    		canRemove();
    	}
    }
    
    $scope.msClientChangeHandle = function(reply){
    	reply.deployFileId = reply.client.id;
    	reply.serverIp = reply.client.hostAddress;
    }
    
    $scope.domainPortBlur = function (row,data,propName) {
    	let port = data[propName];
		if(!port || port == 0){
			dbUtils.info("端口号请输入数字！","提示");
			return;
		}
		
		var value = Number(port);
		if(row < 0)
			$scope.checkPortDuplicate(data.adminIp,data,[propName],"adminIp");
    };
    
//	把controller中对端口检验的配置转移到ui-dbUtils.js中，方便管理和使用,提供ipPortList和oldData的初始化
	dbUtils.startPortCheck($scope,$scope.editData,$scope.domain);
    
    $scope.checkPortDuplicate = function(ip,data,propNameList,ipProp){
    	return dbUtils.checkPortDuplicate(ip,data,propNameList,$scope.ipPortList,$scope.oldData,$scope.editData,ipProp);
    }
    
    $scope.$watch('domain',function(newValue,oldValue){
    	if(newValue === oldValue){
    		return;
    	}
    	
    	if(newValue != oldValue){
    		if(oldValue.isAdminMonitor == 0 && newValue.isAdminMonitor == 1 && newValue.prometPort == 0){
    			newValue.prometPort = "";
    		}
    	}
    },true)
    
    $scope.submitApply = function(){
        if($scope.form.$invalid){
            return;
        }
        if(!$scope.client){
			dbUtils.error("请选择主机","提示");
            return;
        }
        if($scope.domain.isAdminMonitor == 1){
			var port = $scope.domain['prometPort'];
			var value = Number(port);
			if(!port || port == 0){
				dbUtils.error("请填写监控端口","提示");
				return;
			}
		}
        var domain = {};
    	for (var key in $scope.domain) {
    		if(key == "domainPort"||key == "adminPort"||key == "jmxPort"){
    			domain[key] = parseInt($scope.domain[key]);
    		}else if(key == "prometPort"){
    			if($scope.domain.isAdminMonitor == 1){
    				domain[key] = parseInt($scope.domain[key]);
    			}
    		}else{
    			domain[key] = $scope.domain[key];
    		}
		}
    	//额外放置监控的端口 当为否时
    	if($scope.domain.isAdminMonitor == 0){
			domain.prometPort = parseInt(0);
    	}
    	var server_arr = new Array();
    	for (var i = 0; i < $scope.managedServer.length; i++) {
    		var obj = {};
    		var j = i + 1;
    		if($scope.managedServer[i].isAdmin == 1){
            	obj.isAdmin = 1;
            	obj.domainId = $scope.domain.id;
            	obj.serverName = $scope.domain.adminName;
        		obj.serverIp = $scope.domain.hostIp;
        		obj.serverPort = parseInt($scope.domain.adminPort);
        		obj.serverDomainName = $scope.domain.domainName;
        		obj.isMonitor = $scope.domain.isAdminMonitor;
        		obj.prometPort = $scope.domain.isAdminMonitor == 1 ? parseInt($scope.domain.prometPort) : 0;
            }else{
            	if(!$scope.managedServer[i].serverName ||$scope.managedServer[i].serverName == "" ){
        			dbUtils.error("请填写受管服务名","提示");
                    return;
                }
            	if(!$scope.managedServer[i].serverIp || $scope.managedServer[i].serverIp == ""){
        			dbUtils.error("请填写受管服务IP","提示");
                    return;
                }
            	if(!$scope.managedServer[i].serverPort || $scope.managedServer[i].serverPort == ""){
        			dbUtils.error("请填写受管服务端口","提示");
                    return;
                }
            	if($scope.managedServer[i].isMonitor == 1 && !$scope.managedServer[i].prometPort){
        			dbUtils.error("请填写监控端口","提示");
                    return;
                }
            	obj.isAdmin = 0;
            	obj.domainId = $scope.managedServer[i].domainId;
            	obj.serverName = $scope.managedServer[i].serverName;
        		obj.serverIp = $scope.managedServer[i].serverIp;
        		obj.serverPort = parseInt($scope.managedServer[i].serverPort);
        		obj.serverDomainName = $scope.managedServer[i].serverDomainName;
        		obj.isMonitor = $scope.managedServer[i].isMonitor;
        		obj.prometPort = $scope.managedServer[i].isMonitor == 1 ? parseInt($scope.managedServer[i].prometPort) : 0;
            }
    		obj.id = $scope.managedServer[i].id;
    		obj.deployFileId = $scope.managedServer[i].deployFileId;
    		obj.deployInfo = $scope.managedServer[i].deployInfo;
    		obj.lastestFile = $scope.managedServer[i].lastestFile;
    		obj.serverIsStart = $scope.managedServer[i].serverIsStart;
			obj.ipAndPortAndFlag = $scope.managedServer[i].ipAndPortAndFlag;
			if(!obj.ipAndPortAndFlag)
				obj.ipAndPortAndFlag = obj.serverIp + ":0,0";
    		server_arr.push(obj);
    	}	
    	if($scope.editData == false){
    		var obj = {};
        	obj.isAdmin = 1;
        	obj.id = null;
        	obj.domainId = null;
        	obj.serverName = $scope.domain.adminName;
    		obj.serverIp = $scope.domain.hostIp;
    		obj.serverPort = parseInt($scope.domain.adminPort);
    		obj.serverDomainName = $scope.domain.domainName;
    		obj.deployFileId = $scope.client.id;
    		obj.isMonitor = $scope.domain.isAdminMonitor;
    		obj.prometPort = parseInt($scope.domain.isAdminMonitor) == 1 ? parseInt($scope.domain.prometPort) : 0;
			obj.ipAndPortAndFlag = obj.serverIp + ":0,0";
    		server_arr.push(obj);
        }
    	domain.managedServers = server_arr;
    	//校验adminserver端口是否重复
    	var idDuplicate = $scope.checkPortDuplicate(domain.adminIp,domain,['adminPort','prometPort'],"adminIp");
        if(idDuplicate == true)
        	return;
        
    	domainService.doSaveDomain(JSON.stringify(domain)).then(function(response){
    		if (response.code == SUCCESS_CODE) {
    			dbUtils.info("操作成功","提示");            
    			$modalInstance.close(true);
    		} else {
    			dbUtils.error(response.message,"提示");
    		}
    	})
    } 
    
    $scope.cancel = function () {
        $modalInstance.close(false);
    };
}]);

//受管服务编辑
let ManagedServerEditController = app.controller('ManagedServerEditController',
		['$scope','$rootScope','$modalInstance','$compile','$modal','dbUtils','source','DomainService',
		 function($scope,$rootScope,$modalInstance,$compile,$modal,dbUtils,source,domainService) {	
			
	//主机
    $scope.domain = source;
    $scope.managedServer = $scope.domain.managedServers;
    $scope.noDirtyData = {};
    $scope.managedServer.forEach(item => {	
    	item.editMode = false;item.deleteMode = true;
    })
    $scope.monitorParams = [{key:0,value:"否"},{key:1,value:"是"}];
    $scope.clients = [];
    $scope.clientsIdToObj = {};
    $scope.copyData = {};
    canRemove();
    
    //查询agent主机，为受管服务赋主机
    domainService.getAgentPost().then(function(response){
        if (response.code == SUCCESS_CODE) {
        	$scope.clients = response.data; 
        	$scope.clients.map(item => {	$scope.clientsIdToObj[item.id] = item;item.id += ""; 	}) 
        	
			for(let item of $scope.managedServer){
				if(item.isAdmin == 1)
	    			continue;
				for(let client of $scope.clients){
					if(parseInt(item.deployFileId) == client.id){
						item.clientName = client.name;
						$scope.noDirtyData[item.id] = angular.copy(item);
						break;
					}
				}
    		}
        } else {
        	dbUtils.error(response.message,"提示");
        }
  	})
    

    function canRemove(){
    	let length;
    	if($scope.editData == true){
    		length = $scope.managedServer.length - 1; 
    	}else{
    		length = $scope.managedServer.length;
    	}
    	
    	if (length <= 0) {
            $scope.canRemove = false;
        }else{
        	$scope.canRemove = true;
        }
    }
    // 增加
	$scope.addManagedServer = function() {
    	let managedServer = {key: new Date().getTime(), value: "", id:null, domainId:$scope.domain.id, isAdmin:0, isMonitor:0, serverDomainName:$scope.domain.domainName, editMode:true, deleteMode:false};
        $scope.managedServer.splice($scope.managedServer.length, 0, managedServer);
        canRemove();
    }

    // 减少
    $scope.remove = function(index) {
    	if($scope.canRemove){
    		$scope.managedServer.splice(index, 1);
    		canRemove();
    	}
    }
    
    //主机下拉框change时间
    $scope.msClientChangeHandle = function(item){
    	let client = $scope.clientsIdToObj[item.deployFileId];
    	item.serverIp = client.hostAddress;
    	item.clientName = client.name;
    }
    
    dbUtils.get("/ports/getPorts",null).then(function(response){
        if (response.code == SUCCESS_CODE) {
        	$scope.ipPortList = response.data; 
        }else {
        	dbUtils.error(response.message,"提示") 
        }
  	})
    
    $scope.checkPortDuplicate = function(ip,data,propNameList,ipProp){
    	//data.deleteMode false:前台新增尚未保存到数据库，true：已保存到数据库 
    	return dbUtils.checkPortDuplicate(ip,data,propNameList,$scope.ipPortList,$scope.oldData,data.deleteMode,ipProp);
    }
    
    $scope.clickCancel = function(event){
    	$(event.target).parents(".popover").prev().click();
    }
    
    $scope.edit = function(item,index){
    	item.editMode = !item.editMode;
    }
    
    $scope.cancelEdit = function(item,index){
    	var noDirty = $scope.noDirtyData[item.id];
    	if(!noDirty)
    		return;
    	$scope.managedServer[index] = angular.copy(noDirty); 
    	$scope.managedServer[index].editMode = false;
    }
    
    $scope.save = function(item,index){
    	if(!item.serverName){
			dbUtils.error("请填写受管服务名","提示");
            return;
        }
    	if(!item.serverIp){
			dbUtils.error("请填写受管服务IP","提示");
            return;
        }
    	if(!item.serverPort){
			dbUtils.error("请填写受管服务端口","提示");
            return;
        }
    	if(item.isMonitor == 1 && !item.prometPort){
			dbUtils.error("请填写监控端口","提示");
            return;
        }
    	item.prometPort = item.isMonitor == 1 ? item.prometPort : 0;
    	
    	$scope.oldData = {};
    	dbUtils.getOldData($scope.oldData,item);
    	//校验adminserver端口是否重复
    	var idDuplicate = $scope.checkPortDuplicate(item.serverIp,item,['serverPort','prometPort'],"serverIp");
        if(idDuplicate == true)
        	return;
        if(!item.ipAndPortAndFlag)
			item.ipAndPortAndFlag = item.serverIp + ":0,0";
    	domainService.saveOrUpdateManagedServer(item).then(function(response){
    		if (response.code == SUCCESS_CODE) {
    			dbUtils.success("保存成功","提示");
    			$scope.noDirtyData[response.data.id] = response.data;
    			item.id = response.data.id;
    			item.editMode = false;
    			item.deleteMode = true;
            } else {
            	dbUtils.error(response.message,"提示");
            }
    	})
    }
    
    $scope.delete = function(item,index,event){
    	//新添加的在前台删除
    	if(item.deleteMode == false){
    		$scope.remove(index);
    	}else{
    		domainService.deleteManagedServer(item).then(function(response){
        		if (response.code == SUCCESS_CODE) {
        			dbUtils.success("删除成功","提示");
        			delete $scope.noDirtyData[item.id];
        			//2.前台删除
        	    	$scope.managedServer.splice(index , 1);
                } else {
                	dbUtils.error(response.message,"提示");
                }
        	})
    	}
    	$scope.clickCancel(event);
    }
    
    $scope.cancel = function () {
    	$modalInstance.close(false);
    };
}]);

//域控制器
let DomainConsoleController = app.controller('DomainConsoleController', ['$scope','$rootScope','$modal','$state','dbUtils','EchoLog','DomainService','$sce',
                                    function($scope,$rootScope,$modal,$state,dbUtils,echoLog,domainService,$sce) {

	$scope.$on("sentDataToConsole",function(event,data){
		$scope.domain = data;
		$scope.showTable = true;
		$scope.initTable();
		$scope.domainConsoleInitData = $scope.domain.managedServers;
		$scope.domainConsoleInitData.sort(function(a,b){
			return b.isAdmin - a.isAdmin;
		})
		$scope.$broadcast("reloadData",{});
	})

	let healthCheckRows = [], rowsObj = {};
    $scope.initTable = function(){
		$scope.queryParams = {isPagination:true,postgroundPagination:false,pageNumber: 1,pageSize:10,prevPageDisabled: 'disabled',nextPageDisabled: 'disabled'};
		let queryParams = {currentPage:$scope.queryParams.pageNumber ,onePageSize:$scope.queryParams.pageSize};
    	$scope.tooltipData = {"status":{}};
		$scope.domainConsoleTableParams = {bodyStyle:{"max-height": "500px"}};
    	$scope.domainConsoleHeaders = [
    		{name:"名称",field:"serverName",style:{width:"20%"},compile:true,formatter:function(value,row){
    			return row.isAdmin == 1 ? value+"(管理)" : value;
    		}},
            {name:"状态",field:"status",style:{"width":"10%"},compile:true,formatter:function(value,row,_row){
				rowsObj[row.id] = _row;
            	healthCheckRows.push({"id":row.id, "ip":row.serverIp, "port":row.serverPort});
            	$scope.tooltipData["status"][row.id] = 2;
            	return "<span ng-if='tooltipData.status["+row.id+"] == 2'><i class='fa fa-spin fa-spinner '></i></span>" +
            			"<span ng-if='tooltipData.status["+row.id+"] != 2'><i style='margin-right:5px;' class='fa fa-circle' ng-class='{1:\"text-success\",0:\"text-danger\"}[tooltipData.status["+row.id+"]]'></i>{{{1:\"运行中\",0:\"已停止\"}[tooltipData.status["+row.id+"]]}}</span>";
            }},
            {name:"ip",field:"serverIp",style:{width:"20%"}},
            {name:"端口",field:"serverPort",style:{width:"10%"}},
	        {name:"是否监控",field:"isMonitor",style:{width:"10%"},compile:true,formatter:function(value,row){
	        	return row.isMonitor == 1 ? '是' : '否';
	        }},
            {name:"监控端口",field:"prometPort",style:{width:"10%"},compile:true,formatter:function(value,row){
	        	return row.isMonitor == 1 ? row.prometPort : '';
	        }},
		];
		$scope.domainConsole = {methods:{}} ;
    	$scope.domainConsoleRowEvents = [{
            class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",name: "控制台首页",
            isShow: function(row){return row.isAdmin == 1;},
            click: function(row){
            	window.open("http://"+row.serverIp+":"+row.serverPort+"/console");
            }
        }];
        $scope.domainConsoleOperationEvents = [{
            class:"btn-info",icon:"fa fa-plus-circle",name:"创建域",
            click:function(rows){
				dbUtils.confirm("是否创建域",function () {
					createDomainHandle();
				})
            	/*if(createDomainCheck()){
					dbUtils.confirmlg("当前有server节点正在运行，继续创建域会覆盖原有的数据，请确认是否继续创建域",function () {
						createDomainHandle();
					})
				}else{
					createDomainHandle();
				}*/
            }
        },{
            class:"btn-info",icon:"fa fa-plus-square",name:"创建受管",
            click:function(){
				let rows = $scope.domainConsole.methods.getAllSelectRows();
				let canCreateMS = true;
				if(rows.length == 0) {
					dbUtils.warning("请选择受管服务","提示");
					canCreateMS = false;
				}

				for(let i=0;i<rows.length;i++){
					if(rows[i].isAdmin == 1){
						canCreateMS = false;
						dbUtils.warning("不能包含管理Server，请重新选择","提示");
						break;
					}
				}
				if(!canCreateMS)
					return;
				dbUtils.confirm("是否创建受管服务",function () {
					createMSHandle(rows,$scope.domain,$scope.domain.id,true);
				})
            }
        },{
            class:"btn-info",icon:"fa fa-power-off",name:"启动",
            click:function(rows){
            	domainService.operateWeblogicServer("start",$scope.domain.id,rows).then(function(response){
                    if (response.code == SUCCESS_CODE) {
                  	  	echoLog.show(response.data,function(){
							$scope.domainConsole.methods.reloadData();
						});
                    } else {
                        dbUtils.error(response.message,"提示");
                    }
              	})
            }
        },{
            class:"btn-info",icon:"fa fa-repeat",name:"重启",
            click:function(rows){
            	domainService.operateWeblogicServer("restart",$scope.domain.id,rows).then(function(response){
                    if (response.code == SUCCESS_CODE) {
                  	  	echoLog.show(response.data,function(){
							$scope.domainConsole.methods.reloadData();
						});
                    } else {
                        dbUtils.error(response.message,"提示");
                    }
              	})
            }
        },{
            class:"btn-danger",icon:"fa fa-stop",name:"停止",
            click:function(rows){
            	domainService.operateWeblogicServer("stop",$scope.domain.id,rows).then(function(response){
                    if (response.code == SUCCESS_CODE) {
                  	  	echoLog.show(response.data,function(){
							$scope.domainConsole.methods.reloadData();
						});
                    } else {
                        dbUtils.error(response.message,"提示");
                    }
              	})
            }
        }];
        $scope.domainAfterReload = function(){
        	domainService.checkHealth(healthCheckRows).then(function(response){
        		if(response.code == SUCCESS_CODE){
        			let respData = response.data;
        			for(let item of healthCheckRows){
						rowsObj[item.id].base_table_status = respData[item.id] == true ? 1 : 0;
        				$scope.tooltipData["status"][item.id] = respData[item.id] == true ? 1 : 0;
        			}
    			}
    		});
        }
    }

    //建域处理
    function createDomainHandle() {
		domainService.createDomain($scope.domain.id).then(function(response){
			if (response.code == SUCCESS_CODE) {
				echoLog.show(response.data,function(){
					$scope.domainConsole.methods.reloadData();
				});
			} else {
				dbUtils.error(response.message,"提示");
			}
		})
	}

    //创建域状态判断
    function createDomainCheck(){
    	let isRunning = false;
    	for(let item in $scope.tooltipData["status"]){
    		if($scope.tooltipData["status"][item] == 1){
				isRunning = true;
				break;
			}
		}
    	return isRunning;
	}

	$scope.$on("baseTableRefreshData",function(event, data){
		$scope.domainAfterReload();
	})

	//创建受管服务
    function createMSHandle(rows,domain,domainId,fristCheck){
		domainService.checkAdminStatus(domainId).then(function(response){
			if(response.data){
				let createMSDomain = Object.assign({},domain);
				createMSDomain.managedServers = rows;

				$scope.loading = true;
				dbUtils.info("正在创建受管服务，请稍等...","提示");
				domainService.createManagedServer(createMSDomain).then(function(response){
					$scope.loading = false;
					if (response.code == SUCCESS_CODE) {
						echoLog.show(response.data,function(){});
					} else {
						dbUtils.error(response.message,"提示");
					}
				},function () {$scope.loading = false;})
			}else{
				startAdminServer(rows,domain,domainId,fristCheck);
			}
		})
	}

	//启动Adminserver
    function startAdminServer(rows,domain,domainId,fristCheck){
    	if(fristCheck == true){
    		dbUtils.confirmlg("由于该域没有启动，系统先启动该域，才会创建受管。该过程可能比较慢，请耐心等待。 </br> 是否继续",function(){
        		// 点击确认按钮
            	domainService.startAdminServer(domainId).then(function(response){
                    if (response.code == SUCCESS_CODE) {
                    	echoLog.show(response.data,function(){
                    		$scope.loading = true;
                			setTimeout(function () {
                				$scope.loading = false;
								$scope.domainConsole.methods.reloadData();
                         		createMSHandle(rows,domain,domainId,false);
                        	}, 3000);
                		}); 
                    } else {
                        dbUtils.error(response.message,"提示");
                    }
              	})
        	},function(){});
    	}else{
    		dbUtils.confirm("AdminServer 启动失败，请返回查看创建域是否有问题",function(){},function(){});
    	}
    }
    
    $scope.returnMain = function(){
    	$scope.showTable = false;
    	$scope.$emit("returnMain",{reloadData:true})
    }
     
}]);