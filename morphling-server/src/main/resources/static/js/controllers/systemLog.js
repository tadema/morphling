'use strict';

// 系统日志控制器
app.controller('SystemLogController', ['$scope','$rootScope','$modal','$state','dbUtils','EchoLog','$sce','SystemLogService',
                                    function($scope,$rootScope,$modal,$state,dbUtils,echoLog,$sce,systemLogService) {
	function setAllHidden(){
		$scope.isShowMain = false;
		$scope.isShowLog = false;
	}
	setAllHidden();
	$scope.isShowMain = true;
	
	$scope.$on('returnMain',function(event,data){
		setAllHidden();
		$scope.isShowMain = true;
		if(data && data.reloadData)
			$scope.table.operations.reloadData();
	})
	
	$scope.searchLog = function(){
		if(!$scope.logPath){
			dbUtils.warning("请输入日志路径","提示");
			return;
		}
	}
	
	$scope.t_id = dbUtils.guid();
    var table = {
    	url:"/system/log/listSystemLog",
    	showUpload: false,
//        pageSql:false,
        headers: [
            {name:"文件名",field:"fileName",style:{width:"30%"}},
            {name:"文件大小",field:"size",style:{width:"10%"},compile:true,formatter:function(value,row){
            	return dbUtils.convertFileUnit(value);
            }},
            {name:"文件地址",field:"longName",style:{"width":"40%"},compile:true,formatter:function(value,row){
            	return value;
            }},
            {name:"时间",field:"mtime",style:{width:"10%"}},
        ],
        operationEvents: [],
        rowEvents:[{
        	class:"btn-info",
            icon:"fa fa-pencil",
            title:"查看",
            click: function(row){
            	setAllHidden();
            	$scope.isShowLog = true;
            	$scope.$broadcast("sendDataToLog",{"data":row,"isShowReturn":true,"baseUrl":"/system/log/readSystemLog"})
            }
        }],
        settings:{
            cols:2,
            showCheckBox:false,
            filterId: "systemLogFilter",
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-34-56-20)+"px",
            },
        },
        afterReload:function(){
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight8+"px";
        	},200)
        }
    }
    $scope.table = table;
    
}]);

//系统日志控制器
app.controller('LogShowController', ['$scope','$rootScope','$modal','$state','dbUtils','EchoLog','$sce','SystemLogService',
                                    function($scope,$rootScope,$modal,$state,dbUtils,echoLog,$sce,systemLogService) {
	$scope.$on('sendDataToLog',function(event,data){
		$scope.log = data.data;
		$scope.isShowReturn = data.isShowReturn ? data.isShowReturn : false;
		$scope.baseUrl = data.baseUrl;
		
    	initDivAndPagination();
    	reloadData(1);
	})
	
    //初始化div和分页参数
    function initDivAndPagination(){
    	var textHeight = $rootScope._screenProp.contentHeight-2*10-5-30-46;//wrapper共2个，一个为15,一个返回按钮30,46分页高度
    	$scope.logStyle = {"height":textHeight+"px"};
    	$scope.scrollTopStyle = {"top":textHeight - 50 +"px"};
    	$scope.scrollBottomStyle = {"top":textHeight - 20 +"px"};
    	
    	$scope.page = {"lineCount":2000,"position":0,"currentPage":1};
    }
	
	//重新查询数据
	function reloadData(currentPage){
    	$scope.loading = true;
    	$scope.page.currentPage = currentPage;
    	delete $scope.page.currentPages;delete $scope.page.prevPageDisabled;delete $scope.page.nextPageDisabled; 
    	dbUtils.post($scope.baseUrl,angular.extend({"file": $scope.log.longName},$scope.page)).then(function(response){
    		$scope.loading = false;
    		if(response.code == SUCCESS_CODE){
    			$scope.log.content = response.data.content;
    			$scope.goToTop();
    			
    			$scope.page.totalPage = response.data.pages;
    			$scope.page.position = response.data.position;
    			systemLogService.pageSet($scope,$scope.page);
            }else{
                dbUtils.error(response.message,"提示"); 
            }
    	},function(){$scope.loading = false;})
    }
	
	$scope.goToTop = function(){
		document.getElementById('systemLog').scrollTop = 0;
	}
	
	$scope.goToBottom = function(){
		document.getElementById('systemLog').scrollTop = document.getElementById('systemLog').scrollHeight ;
	}
    
    //刷新查
    $scope.refreshQuery = function(){
    	reloadData($scope.page.currentPage);
    }
    
    //点击进行分页,查询数据
    $scope.pageNumberClick = function(pageNumber){
    	let currentPage = systemLogService.pageNumberClick($scope,pageNumber);
    	if(currentPage)
    		reloadData(currentPage);
    }
    
    $scope.returnMain = function(){
    	$scope.$emit("returnMain",{});
    	$scope.log = null;
    	$scope.page = null;
    }
    
}]);

