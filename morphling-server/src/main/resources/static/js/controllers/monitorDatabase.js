'use strict';
/* Oracle监控页面 */
let MonitorOracleController = app.controller('MonitorOracleController',
		['$rootScope', '$scope', '$state','$stateParams', '$cookieStore','$window','$modal','dbUtils','$http','EchoLog','$filter', 'MonitorService',
        function ($rootScope, $scope, $state,$stateParams,$cookieStore, $window,$modal,dbUtils,$http,echoLog,$filter,monitorService) {
	
	$scope.$on('sendDataToOracle',function(event,data){
		$scope.data = data.oracleMonitorData;

		$scope.pageData.refresh = "manual";
		$scope.refreshName = "刷新";
		setButtonOff();
		$scope.refreshList = [{value:'manual',show:'手动'},{value:'1',show:'1分钟'},{value:'5',show:'5分钟'},{value:'10',show:'10分钟'},{value:'15',show:'15分钟'},{value:'30',show:'半小时'}];
		$scope.menus = [{name:"概况",id:1,className:"active"},{name:"表空间",id:2},{name:"会话",id:3},{name:"回滚段",id:8},{name:"SGA",id:4},{name:"SQL",id:5},{name:"锁",id:6},{name:"JOB",id:7},
			{name:"REDO日志",id:9},{name:"AWR",id:10}
		];
		$scope.menu = $scope.menus[0];
		$scope.isShow = true;
		initData();
	})		
	
	let intervalId;
	$scope.pageData = {};
	
	$scope.refreshChange = function(){
		if($scope.pageData.refresh != "manual"){
			$scope.refreshName = "停止刷新";
			$scope.refreshTime = parseFloat($scope.pageData.refresh)*60*1000;//按分钟计算
			if(intervalId){
				clearInterval(intervalId);
			}
			setButtonOn();
			intervalId = setInterval(initData,$scope.refreshTime);
		}else{
			$scope.refreshName = "刷新";
			$scope.refreshTime = "off";
			setButtonOff();
			if(intervalId){
				clearInterval(intervalId);
			}
		}
	}
	
	$scope.operateRefresh = function(){
		if($scope.pageData.refresh != "manual"){
			if($scope.isRefreshing == true){
				$scope.refreshTime = "off";//组件监控变量，会停止刷新
				setButtonOff();
				if(intervalId)
					clearInterval(intervalId);//主进程停止定时
				$scope.refreshName = "继续刷新";
			}else{
				$scope.refreshChange();
			}
		}else{
			$scope.refreshTime = 100;
			setButtonOn();
			setTimeout(function(){
				initData();
				$scope.refreshTime = "off";
				setButtonOff();
			},$scope.refreshTime)
		}
	}
	
	function setButtonOn(){
		$scope.isRefreshing = true;
		$scope.refreshClass = "btn-success";
		$scope.refreshIClass = "fa-spin";
	}
	
	function setButtonOff(){
		$scope.isRefreshing = false; 
		$scope.refreshClass = "btn-danger";
		$scope.refreshIClass = "";
	}
	
	$scope.chooseMenu = function(event,menu){
		event = event.currentTarget;
		$(event).closest('ul').find("li").removeClass('active');
		$(event).addClass('active');
		$scope.menu = menu;
		if(!$scope.menu.isInitData){
			initData();
		}
		if($scope.isRefreshing == true){
			$scope.refreshTime = "off";//组件监控变量，会停止刷新
			setTimeout(function(){
				$scope.isRefreshing = false;//标记变化
				if(intervalId)
					clearInterval(intervalId);//主进程停止定时
				
				$scope.refreshChange();//如果切换tab的时候，正在刷新，那么就执行该方法，清除旧的，重新定时执行
			},2000);
		}
	}

	//查询磁盘空间请求
	function queryDiscspaceData() {
		monitorService.monitorOracleQuery($scope.data,"database_files").then(function (response) {
			if(response.code == SUCCESS_CODE){
				$scope.discspace.initData.data1 = response.data[0];
			}else{
				dbUtils.error(response.message);
			}
		})
		monitorService.monitorOracleQuery($scope.data,"flashback_recovery").then(function (response) {
			if(response.code == SUCCESS_CODE){
				$scope.discspace.initData.data2 = response.data[0];
			}else{
				dbUtils.error(response.message);
			}
		})
		monitorService.monitorOracleQuery($scope.data,"redo_logs").then(function (response) {
			if(response.code == SUCCESS_CODE){
				$scope.discspace.initData.data3 = response.data[0];
			}else{
				dbUtils.error(response.message);
			}
		})
		monitorService.monitorOracleQuery($scope.data,"archived_log").then(function (response) {
			if(response.code == SUCCESS_CODE){
				$scope.discspace.initData.data4 = response.data[0];
			}else{
				dbUtils.error(response.message);
			}
		})
	}
	
	function initData(){
		$scope.bodyStyle = JSON.stringify({"max-height": "350px"});
		$scope.discspaceBodyStyle = JSON.stringify({"height": "354px"});
		$scope.methodData = JSON.stringify($scope.data);
		if($scope.menu.id == 1){
			if($scope.menu.isInitData){
				$scope.database.methods.reloadData();
				queryDiscspaceData();
				$scope.SGA_DETAILS.methods.reloadData();
				$scope.SESSION_WAITS.methods.reloadData();
				$scope.READS_WRITES.methods.reloadData();
				$scope.bufferHitRatio.methods.reloadData();
				$scope.dDataDictionaryHitRatio.methods.reloadData();
				$scope.libraryHitRatio.methods.reloadData();
			}else{
				//数据库的明细
				$scope.database = {
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_DATABASE_DETAILS", urlBody:$scope.data, method:"postBody"},
					labelTitle:"数据库的明细",
					bodyStyle:JSON.stringify({"max-height": "380px"}),
					methods: {}
				}

				//磁盘空间
				$scope.discspace = {
					initData: {data1:null, data2:null, data3:null, data4:null},
					labelTitle:"磁盘空间",
					bodyStyle:JSON.stringify({"height": "354px"}),
					methods: {}
				}
				queryDiscspaceData();

				//共享的SGA
				$scope.SGA_DETAILS = {
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_SGA_DETAILS", urlBody:$scope.data, method:"postBody"},
					labelTitle:"SGA明细(M)",
					bodyStyle:JSON.stringify({"height": "300px"}),
					methods: {}
				}

				//等待会话数
				$scope.SESSION_WAITS = {
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_SESSION_WAITS", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"等待会话数",bodyStyle:{"max-height": "350px"}},
					headers: [],
					methods: {}
				}

				//读写次数
				$scope.READS_WRITES = {
					labelTitle:"读写次数",
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_READS_WRITES", urlBody:$scope.data, method:"postBody"},
					bodyStyle:JSON.stringify({"height": "300px"}),
					methods: {}
				}

				//缓冲区命中率
				$scope.bufferHitRatio = {
					labelTitle:"缓冲区命中率",
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_BufferHitRatio", urlBody:$scope.data, method:"postBody"},
					methods: {}
				}

				//数据库命中率
				$scope.dDataDictionaryHitRatio = {
					labelTitle:"数据库命中率",
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_DataDictionaryHitRatio", urlBody:$scope.data, method:"postBody"},
					methods: {}
				}

				//库命中率
				$scope.libraryHitRatio = {
					labelTitle:"库命中率",
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_LibraryHitRatio", urlBody:$scope.data, method:"postBody"},
					methods: {}
				}
			}
		}else if($scope.menu.id == 2){
			if($scope.menu.isInitData){
				$scope.tablespace_usage.methods.reloadData();
				$scope.tablespace.methods.reloadData();
				$scope.TABLESPACE_DETAILS.methods.reloadData();
			}else{
				//表空间使用率
				$scope.tablespace_usage = {
					labelTitle:"表空间使用率",
					urlParams: {url:"/database/monitor/oracle?sqlId=tablespace_usage", urlBody:$scope.data, method:"postBody"},
					props:{yAxis:{name: "使用率(%)"}},
					methods: {}
				}

				//表空间状态
				$scope.tablespace = {
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_TABLESPACE_STATUS", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"表空间状态",bodyStyle:{"max-height": "350px"}},
					methods: {}
				}

				//表空间明细
				$scope.TABLESPACE_DETAILS = {
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_TABLESPACE_DETAILS", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"表空间明细",bodyStyle:{"max-height": "350px"}},
					methods: {}
				}
			}
		}else if($scope.menu.id == 3){
			if($scope.menu.isInitData){
				$scope.SESSION_WAITS.methods.reloadData();
				$scope.SESSION_SUMMARY.methods.reloadData();
				$scope.SESSION_INFO.methods.reloadData();
			}else{
				//等待会话数
				$scope.SESSION_WAITS = {
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_SESSION_WAITS", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"等待会话数",bodyStyle:{"max-height": "350px"}},
					methods: {}
				}

				//会话汇总
				$scope.SESSION_SUMMARY = {
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_SESSION_SUMMARY", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"会话汇总",bodyStyle:{"max-height": "350px"}},
					methods: {}
				}

				//会话明细信息
				$scope.SESSION_INFO = {
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_SESSION_INFO", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"会话明细信息",bodyStyle:{"max-height": "350px"}},
					methods: {}
				}
			}
		}else if($scope.menu.id == 8){
			if($scope.menu.isInitData){
				$scope.RollBack.methods.reloadData();
			}else{
				//回滚段
				$scope.RollBack = {
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_RollBack", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"回滚段",bodyStyle:{"max-height": "350px"}},
					methods: {}
				}
			}
		}else if($scope.menu.id == 4){
			if($scope.menu.isInitData){
				$scope.SGA_DETAILS.methods.reloadData();
			}else{
				//SGA明细(M)
				$scope.SGA_DETAILS = {
					labelTitle:"SGA明细(M)",
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_SGA_DETAILS", urlBody:$scope.data, method:"postBody"},
					props:{yAxis:{name:"MB"},seriesName:""},
					bodyStyle:JSON.stringify({"height": "300px"}),
					methods: {}
				}
			}
		}else if($scope.menu.id == 5){
			//查询
			if($scope.menu.isInitData){
				$scope.BUFFER_GETS.methods.reloadData();
				$scope.DISK_READS.methods.reloadData();
			}else {
				//缓冲区取得数 - 前10查询
				$scope.BUFFER_GETS = {
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_BUFFER_GETS", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"缓冲区取得数 - 前10查询",bodyStyle:{"max-height": "350px"}},
					headers:[
						{field:"SQL_ID"},{field:"缓冲区取得数"},{field:"执行数"},{field:"每次执行的缓冲区取得数"},
					],
					rowEvents: [{
						class:"btn-info",icon:"fa fa-file",title:"格式化SQL",name: "格式化SQL",
						click: function(row){
							dbUtils.openModal('tpl/templates/modal_ace.html','UiBaseTableModalAceController',"lg","modal-large",{"data":row["SQL文本"],"isShowFormatter":true},function (data) {
							},true);
						}
					}],
					methods: {}
				}

				//磁盘读数 - 前10查询
				$scope.DISK_READS = {
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_DISK_READS", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"磁盘读数 - 前10查询",bodyStyle:{"max-height": "350px"}},
					headers:[
						{field:"SQL_ID"},{field:"磁盘读数"},{field:"执行数"},{field:"每次执行的磁盘读数"},
					],
					rowEvents: [{
						class:"btn-info",icon:"fa fa-file",title:"格式化SQL",name: "格式化SQL",
						click: function(row){
							dbUtils.openModal('tpl/templates/modal_ace.html','UiBaseTableModalAceController',"lg","modal-large",{"data":row["SQL文本"],"isShowFormatter":true},function (data) {
							},true);
						}
					}],
					methods: {}
				}
			}
		}else if($scope.menu.id == 6){
			//锁
			if($scope.menu.isInitData){
				$scope.DBA_BLOCKERS.methods.reloadData();
				$scope.DBA_WAITERS.methods.reloadData();
				$scope.LOCKSTATICS.methods.reloadData();
			}else {
				//拥有锁的会话数
				$scope.DBA_BLOCKERS = {
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_DBA_BLOCKERS", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"拥有锁的会话数",bodyStyle:{"max-height": "350px"}},
					methods: {}
				}

				//锁的会话等待数
				$scope.DBA_WAITERS = {
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_DBA_WAITERS", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"锁的会话等待数",bodyStyle:{"max-height": "350px"}},
					methods: {}
				}

				//锁明细
				$scope.LOCKSTATICS = {
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_LOCKSTATICS", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"锁明细",bodyStyle:{"max-height": "350px"}},
					methods: {}
				}
			}
		}else if($scope.menu.id == 7){
			if($scope.menu.isInitData){
				$scope.SCHEDULED_JOBS.methods.reloadData();
				$scope.BackupJobsStats.methods.reloadData();
				$scope.BackUpJobsCount.methods.reloadData();
			}else {
				//计划作业信息
				$scope.SCHEDULED_JOBS = {
					urlParams: {url:"/database/monitor/oracle?sqlId=ORACLE_SCHEDULED_JOBS", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"计划作业信息",bodyStyle:{"max-height": "350px"}},
					methods: {}
				}

				//备份作业信息
				$scope.BackupJobsStats = {
					urlParams: {url:"/database/monitor/oracle?sqlId=Oracle_BackupJobsStats", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"备份作业信息",bodyStyle:{"max-height": "350px"}},
					methods: {}
				}

				//正在运行的备份作业数
				$scope.BackUpJobsCount = {
					urlParams: {url:"/database/monitor/oracle?sqlId=Oracle_BackUpJobsCount", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"正在运行的备份作业数",bodyStyle:{"max-height": "350px"}},
					methods: {}
				}
			}
		}else if($scope.menu.id == 9){
			//重做日志
			if($scope.menu.isInitData){
				$scope.REDOPERFORMANCE.methods.reloadData();
				$scope.OracleRedoLogGroups.methods.reloadData();
				$scope.OracleRedoLogMembers.methods.reloadData();
			}else {
				//重做日志性能
				$scope.REDOPERFORMANCE = {
					labelTitle:"重做日志性能",
					urlParams: {url:"/database/monitor/oracle?sqlId=REDOPERFORMANCE", urlBody:$scope.data, method:"postBody"},
					props:{yAxis:{name:"百分比(%)"}},
					bodyStyle:JSON.stringify({"height": "300px"}),
					methods: {}
				}

				//重做日志组信息
				$scope.OracleRedoLogGroups = {
					urlParams: {url:"/database/monitor/oracle?sqlId=OracleRedoLogGroups", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"重做日志组信息",bodyStyle:{"max-height": "350px"}},
					methods: {}
				}

				//重做日志成员信息
				$scope.OracleRedoLogMembers = {
					urlParams: {url:"/database/monitor/oracle?sqlId=OracleRedoLogMembers", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"重做日志成员信息",bodyStyle:{"max-height": "350px"}},
					methods: {}
				}
			}
		}else if($scope.menu.id == 10){//AWR
			$scope.queryParams = {}
			let startTime = new Date().setHours(new Date().getHours() - 4);
			$scope.queryParams.startTime = new Date(startTime).format("yyyy-MM-dd hh:mm:ss");
			$scope.queryParams.endTime = new Date().format("yyyy-MM-dd hh:mm:ss");

			if($scope.menu.isInitData){
				$scope.AwrList.methods.reloadData();
			}else{
				$scope.AwrList = {
					urlParams: {url:"/database/monitor/oracle/getOracleAWRList?startDate=''&endDate=''", urlBody:$scope.data, method:"postBody"},
					tableParams: {labelTitle:"AWR列表", BodyStyle:{"max-height": "400px"},loading:true},
					headers : [
						{name:"文件名",field:"fileName",style:{width:"20%"}},
						{name:"文件地址",field:"filePath",style:{width:"40%"}},
						{name:"文件大小",field:"fileSize",style:{width:"10%"},compile:true,formatter:function(value,row){
							return dbUtils.convertFileUnit(value);
						}},
						{name:"时间",field:"time",style:{width:"15%"},formatter:function(value,row){
							return new Date(value).format("yyyy-MM-dd hh:mm:ss");
						}}
					],
					rowEvents: [{
						class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"下载文件",name: "下载文件",
						click: function(row){
							var url = "/database/downloadAWRFile";
							var form = $("<form></form>").attr("action", url).attr("method", "get");
							form.append($("<input></input>").attr("type", "hidden").attr("name", "filePath").attr("value", row.filePath));
							form.appendTo('body').submit().remove();
						}
					}],
					afterReload: function(){
						$scope.AwrList.methods.sortTable({},{name:"时间",field:"time"});
					},
					methods: {}
				}
			}
		}
		$scope.menu.isInitData = true;
	}

	$scope.generateAwr = function(){
		let startTime = new Date($scope.queryParams.startTime).format("yyyyMMddhh:mm:ss");
		let endTime = new Date($scope.queryParams.endTime).format("yyyyMMddhh:mm:ss");

		let minusValue = (new Date($scope.queryParams.endTime) - new Date($scope.queryParams.startTime)) / (1000 * 60 * 60);//单位：小时
		if(minusValue > 12){
			dbUtils.warning("查询时间范围不能大于12小时");
			return
		}
		if($scope.AwrList.methods.setLoading)
			$scope.AwrList.methods.setLoading(true);

		monitorService.generateAWR($scope.data, startTime, endTime).then(function (response) {
			if($scope.AwrList.methods.reloadData)
				$scope.AwrList.methods.reloadData();
		},function (error) {
			dbUtils.error(error.message);
			if($scope.AwrList.methods.setLoading)
				$scope.AwrList.methods.setLoading(false);
		})
	}
	
	//返回主页
	$scope.returnMain = function(){
		$scope.refreshTime = "off";
		$scope.isShow = false;
		if(intervalId)
			clearInterval(intervalId);//主进程停止定时
		$window.sessionStorage.removeItem("baseTableBarData");
//		$state.go("app.monitorDatabase");
		$scope.$emit("returnMain",{});
	}
}]);

/* Mysql监控页面 */
let MonitorMysqlController = app.controller('MonitorMysqlController',
	['$rootScope', '$scope', '$state','$stateParams', '$cookieStore','$window','$modal','dbUtils','$http','EchoLog','$filter', 'MonitorService',"$q",'$timeout',
	function ($rootScope, $scope, $state,$stateParams,$cookieStore, $window,$modal,dbUtils,$http,echoLog,$filter,monitorService,$q,$timeout) {

	$scope.$on('sendDataToMysql',function(event,data){
		$scope.data = data.mysqlMonitorData;

		$scope.refresh = "manual";
		$scope.refreshName = "刷新";
		setButtonOff();
		$scope.refreshList = [{value:'manual',show:'手动'},{value:'1',show:'1分钟'},{value:'5',show:'5分钟'},{value:'10',show:'10分钟'},{value:'15',show:'15分钟'},{value:'30',show:'半小时'}];
		$scope.menus = [{name:"概览",id:1,className:"active"},{name:"MyISAM",id:2},{name:"Innodb",id:3},{name:"线程",id:4},{name:"会话",id:11},{name:"SQL",id:5},
						{name:"IO",id:6},{name:"客户端",id:7},{name:"用户",id:8},{name:"自增长字段",id:9},{name:"索引",id:10}];
		$scope.menu = $scope.menus[0];
		$scope.mysqlMonitorData = {};
		initDirectMethods();
		getRefreshData();
	})

	//初始化指令的方法
	function initDirectMethods(){
		$scope.thread = {methods:[]};
		$scope.qpsAndTps = {methods:[]};
		$scope.network = {methods:[]};
		$scope.connect = {methods:[]};
		$scope.buffer = {methods:[]};
		$scope.sqlNum = {methods:[]};
		$scope.readAndWrites = {methods:[]};
		$scope.openNum = {methods:[]};
		$scope.innodb = {methods:[]};
		$scope.bufferHitRatio = {methods:[]};
		$scope.bufferUseRatio = {methods:[]};
		$scope.bufferDirtyRatio = {methods:[]};
		$scope.readAndWrite = {methods:[]};
		$scope.readAndWriteNum = {methods:[]};
	}

	let intervalId;

	//刷新选项变化事件
	$scope.refreshChange = function(){
		if($scope.refresh != "manual"){
			$scope.refreshName = "停止刷新";
			$scope.refreshTime = parseFloat($scope.refresh)*60*1000;//按分钟计算
			if(intervalId){
				clearInterval(intervalId);
			}
			setButtonOn();
			intervalId = setInterval(getRefreshData,$scope.refreshTime);
		}else{
			$scope.refreshName = "刷新";
			$scope.refreshTime = "off";
			setButtonOff();
			if(intervalId){
				clearInterval(intervalId);
			}
		}
	}

	//刷新按钮点击事件
	$scope.operateRefresh = function(){
		if($scope.refresh != "manual"){
			if($scope.isRefreshing == true){
				$scope.refreshTime = "off";//组件监控变量，会停止刷新
				setButtonOff();
				if(intervalId)
					clearInterval(intervalId);//主进程停止定时
				$scope.refreshName = "继续刷新";
			}else{
				$scope.refreshChange();
			}
		}else{
			$scope.refreshTime = 100;
			setButtonOn();
			setTimeout(function(){
				getRefreshData();
				$scope.refreshTime = "off";
				setButtonOff();
			},$scope.refreshTime)
		}
	}

	function setButtonOn(){
		$scope.isRefreshing = true;
		$scope.refreshClass = "btn-success";
		$scope.refreshIClass = "fa-spin";
	}

	function setButtonOff(){
		$scope.isRefreshing = false;
		$scope.refreshClass = "btn-danger";
		$scope.refreshIClass = "";
	}

	$scope.chooseMenu = function(event,menu){
		event = event.currentTarget;
		$(event).closest('ul').find("li").removeClass('active');
		$(event).addClass('active');
		$scope.menu = menu;
		if(!$scope.menu.isInitData){
			initData();
		}
		if($scope.isRefreshing == true){
			$scope.refreshTime = "off";//组件监控变量，会停止刷新
			setTimeout(function(){
				$scope.isRefreshing = false;//标记变化
				if(intervalId)
					clearInterval(intervalId);//主进程停止定时

				$scope.refreshChange();//如果切换tab的时候，正在刷新，那么就执行该方法，清除旧的，重新定时执行
			},2000);
		}
	}

	//先获取刷新数据，再执行
	function getRefreshData() {
		if(!$scope.mysqlVersion){
			monitorService.getMysqlVersion($scope.data).then(function (response) {
				$scope.mysqlVersion = response.data;
				menuShowHandle();//处理显示的菜单
				styleHandle();//处理显示的菜单
				$scope.showContent = true;//显示图形
				initData();
			},function (error) {
				$scope.loading = false;
				dbUtils.error("mysql版本获取失败 "+error.message);
			})
		}else {
			initData();
		}
	}

	//设置菜单权限
	function menuShowHandle(){
		$scope.has5_7Permission = parseFloat($scope.mysqlVersion) >= 5.7 ? true : false;
		if(!$scope.has5_7Permission){
			$scope.menus = [{name:"概览",id:1,className:"active"},{name:"MyISAM",id:2},{name:"Innodb",id:3}];
		}
	}

	//设置样式权限
	function styleHandle(){
		if($scope.has5_7Permission){
			$scope.capacityItemClass = "col-xs-4 col-md-4";
		}else{
			$scope.capacityItemClass = "col-xs-6 col-md-6";
		}
	}

	function initData(){
		if(!$scope.menu.isInitData && $scope.menu.id == 1)
			$scope.showContent = true;

		$scope.bodyStyle = JSON.stringify({"max-height": "350px"});
		$scope.threadBodyStyle = JSON.stringify({"height": "275px"});
		$scope.qpsAndTpsBodyStyle = JSON.stringify({"height": "158px"});
		$scope.qpsAndTpsProps = {"yAxis": {"name":"个","minInterval": 1}};
		$scope.networkBodyStyle = JSON.stringify({"height": "200px"});
		$scope.networkProps = {"yAxis": {"name":"GB"}};

		if($scope.menu.id == 1){//概览
			//如果method.setLoading方法存在，则置为true加载，指令首次加载不会执行，再次刷新会执行
			monitorService.setLoading($scope,1,true);
			$scope.loading = true;
			monitorService.getMysqlMonitorData($scope.data).then(function (response) {
				$scope.loading = false;
				Object.assign($scope.mysqlMonitorData, response.data);
				//添加数据
				$scope.mysqlMonitorData.dataSize = $scope.mysqlMonitorData.data_size + $scope.mysqlMonitorData.index_size;
				$scope.mysqlMonitorData.data = $scope.mysqlMonitorData.data_size;
				$scope.mysqlMonitorData.indexSize = $scope.mysqlMonitorData.index_size;
				$scope.mysqlMonitorData.itemNum = parseFloat(($scope.mysqlMonitorData.total_rows/1000000).toFixed(2));
				$scope.mysqlMonitorData.innodb_buffer_pool_size_tran_Value = dbUtils.convertFileUnit($scope.mysqlMonitorData.innodb_buffer_pool_size);
				$scope.mysqlMonitorData.sort_buffer_size_tran_value = dbUtils.convertFileUnit($scope.mysqlMonitorData.sort_buffer_size);

				$scope.threadInitData = {data:[{"连接线程数":$scope.mysqlMonitorData.threads_connected,"运行线程数":$scope.mysqlMonitorData.threads_running,
						"等待线程数":$scope.mysqlMonitorData.threads_waited,"缓存线程数":$scope.mysqlMonitorData.threads_cached}]};
				$scope.qpsAndTpsInitData = {data:[{"每秒查询请求数":$scope.mysqlMonitorData.qps,"每秒事务处理数":$scope.mysqlMonitorData.tps}]};

				$scope.networkInitData = {data:[{"每秒接收数量":($scope.mysqlMonitorData.bytes_received/1024/1024).toFixed(2),"每秒发送字数量":($scope.mysqlMonitorData.bytes_sent/1024/1024).toFixed(2)}]};

				//如果method.setLoading方法存在，则置为false不加载
				monitorService.setLoading($scope,1,false);
			},function (error) {
				$scope.loading = false;
				monitorService.setLoading($scope,1,false);
				dbUtils.error(error.message);
			})

			if($scope.has5_7Permission){
				monitorService.monitorMysqlExcute($scope.data,"get_memory_global_total").then(function (response) {
					if(response.code == SUCCESS_CODE){
						let data = !response.data ? "-" : response.data[0].total_allocated;
						let index = data.indexOf(" ");
						if(index != -1){
							$scope.mysqlMonitorData.totalMemoryValue = data.split(" ")[0];
							$scope.mysqlMonitorData.totalMemoryUnit = "(" + data.split(" ")[1] + ")";
						}else{
							$scope.mysqlMonitorData.totalMemoryValue = data;
						}
					}else{
						dbUtils.error(response.message);
					}
				})
			}
		}else if($scope.menu.id == 2){//MyISAM
			$scope.connectTableParams = {labelTitle:"连接",bodyStyle:$scope.bodyStyle,loading:true};
			$scope.connectHeaders = [
				{field:"max_connections",name:"最大连接数"},
				{field:"threads_connected",name:"当前连接数"},
				{field:"max_connect_errors",name:"最大连接错误数"},
				{field:"open_files_limit",name:"最大打开文件数量"},
				{field:"open_files",name:"当前打开文件数"},
				{field:"table_open_cache",name:"最大打开表数量"},
				{field:"open_tables",name:"当前打开表数量"},
			];

			$scope.bufferTableParams = {labelTitle:"键缓存",bodyStyle:$scope.bodyStyle,loading:true};
			$scope.bufferHeaders = [
				{field:"key_buffer_size",title:"key_buffer_size",name:"索引缓冲区大小",compile:true,formatter:function (value,row) {
					return dbUtils.convertFileUnit(value);
				}},
				{field:"sort_buffer_size",title:"sort_buffer_size",name:"排序缓冲区大小",compile:true,formatter:function (value,row) {
					return dbUtils.convertFileUnit(value);
				}},
				{field:"join_buffer_size",title:"join_buffer_size",name:"连接缓冲区大小",compile:true,formatter:function (value,row) {
					return dbUtils.convertFileUnit(value);
				}},
				{field:"key_blocks_unused",title:"key_blocks_unused",name:"未使用缓冲block数"},
				{field:"key_blocks_used",title:"key_blocks_used",name:"已使用缓冲block数"},
				{field:"key_blocks_not_flushed",title:"key_blocks_not_flushed",name:"未更新到硬盘的键数"},//已更改但还未更新到硬盘的键数量
				{field:"key_blocks_used_rate",title:"key_blocks_used_rate",name:"已使用缓冲block比率"},
				// {field:"key_buffer_read_rate",title:"key_buffer_read_rate",name:"read_rate"},
				// {field:"key_buffer_write_rate",title:"key_buffer_write_rate",name:"write_rate"}
			];

			$scope.openNumBodyStyle = JSON.stringify({"height": "200px"});
			$scope.openNumProps = {"yAxis": {name:"个","minInterval": 1}};
			$scope.readAndWritesBodyStyle = JSON.stringify({"height": "200px"});
			$scope.readAndWritesProps = {"yAxis": {name:"次","minInterval": 1}};
			$scope.sqlNumBodyStyle = JSON.stringify({"height": "200px"});
			$scope.sqlNumProps = {"yAxis": {name:"次","minInterval": 1}};

			monitorService.setLoading($scope,2,true);
			$scope.loading = true;
			monitorService.getMysqlMonitorData($scope.data).then(function (response) {
				$scope.loading = false;
				Object.assign($scope.mysqlMonitorData, response.data);
				//添加数据
				let connectData = {"max_connections":$scope.mysqlMonitorData.max_connections,"threads_connected":$scope.mysqlMonitorData.threads_connected,"max_connect_errors":$scope.mysqlMonitorData.max_connect_errors,
					"open_files_limit":$scope.mysqlMonitorData.open_files_limit,"open_files":$scope.mysqlMonitorData.open_files,"table_open_cache":$scope.mysqlMonitorData.table_open_cache,
					"open_tables":$scope.mysqlMonitorData.open_tables};
				$scope.connectInitData = [connectData];

				let bufferData = {"key_buffer_size":$scope.mysqlMonitorData.key_buffer_size,"sort_buffer_size":$scope.mysqlMonitorData.sort_buffer_size,"join_buffer_size":$scope.mysqlMonitorData.join_buffer_size,
					"key_blocks_unused":$scope.mysqlMonitorData.key_blocks_unused,"key_blocks_used":$scope.mysqlMonitorData.key_blocks_used,"key_blocks_not_flushed":$scope.mysqlMonitorData.key_blocks_not_flushed,
					"key_blocks_used_rate":"0.00%","key_buffer_read_rate":"NaN%","key_buffer_write_rate":"NaN%"};
				$scope.bufferInitData = [bufferData];

				$scope.openNumInitData = {data:[{"打开文件数量":$scope.mysqlMonitorData.open_files,"打开表数量":$scope.mysqlMonitorData.open_tables}]};

				//扫描次数、慢查询
				$scope.mysqlMonitorData.selectScan = dbUtils.convertNumUnit($scope.mysqlMonitorData.select_scan);
				$scope.mysqlMonitorData.slowQueries = dbUtils.convertNumUnit($scope.mysqlMonitorData.slow_queries);

				//MyISAM读写次数
				$scope.readAndWritesInitData = {data:[{"从缓存读取索引的请求次数":$scope.mysqlMonitorData.key_read_requests,"从磁盘读取索引的请求次数":$scope.mysqlMonitorData.key_reads,"通过缓存写索引的请求次数":$scope.mysqlMonitorData.key_write_requests,"通过磁盘写索引的请求次数":$scope.mysqlMonitorData.key_writes}]};

				//每秒SQL执行次数
				$scope.sqlNumInitData = {data:[{"查询语句数量":$scope.mysqlMonitorData.mysql_sel,"插入语句数量":$scope.mysqlMonitorData.mysql_ins,"更新语句数量":$scope.mysqlMonitorData.mysql_upd,
						"删除语句数量":$scope.mysqlMonitorData.mysql_del,"提交语句数量":$scope.mysqlMonitorData.mysql_com,"回滚语句数量":$scope.mysqlMonitorData.mysql_rol}]};

				monitorService.setLoading($scope,2,false);
			},function (error) {
				$scope.loading = false;
				monitorService.setLoading($scope,2,false);
				dbUtils.error(error.message);
			})
		}else if($scope.menu.id == 3){//innodb
			$scope.innodbTableParams = {labelTitle:"innodb",bodyStyle:$scope.bodyStyle,loading:true};
			$scope.innodbHeaders = [
				{field:"innodb_buffer_pool_size",title:"innodb缓冲池大小",name:"缓冲池大小",hideSort:true,compile:true,formatter:function (value,row) {
					return dbUtils.convertFileUnit(value);
				}},
				{field:"innodb_buffer_pool_pages_total",title:"innodb缓冲池大小(页数量)",name:"缓冲池页数",hideSort:true},
				{field:"innodb_buffer_pool_pages_data",title:"innodb缓冲池中数据页数量(包括脏页)",name:"数据页数量",hideSort:true},
				{field:"innodb_buffer_pool_pages_dirty",title:"innodb缓冲池中脏页数量",name:"脏页数量",hideSort:true},
				{field:"innodb_buffer_pool_pages_flushed",title:"innodb脏页刷新的频率",name:"脏页刷新频率",hideSort:true},
				{field:"innodb_buffer_pool_pages_free",title:"innodb缓冲池中剩余页数量",name:"剩余页数量",hideSort:true},
				{field:"innodb_io_capacity",title:"从缓冲区刷新脏页时一次刷新的数量",name:"刷新数据量",hideSort:true},
				{field:"innodb_read_io_threads",title:"innodb读线程数量",name:"读线程数量",hideSort:true},
				{field:"innodb_write_io_threads",title:"innodb写线程数量",name:"写线程数量",hideSort:true},
				{field:"innodb_rows_read",title:"innodb每秒读取的行数",name:"每秒读取行数",hideSort:true},
				{field:"innodb_rows_inserted",title:"innodb每秒插入的行数",name:"每秒插入行数",hideSort:true},
				{field:"innodb_rows_updated",title:"innodb每秒更新的行数",name:"每秒更新行数",hideSort:true},
				{field:"innodb_rows_deleted",title:"innodb每秒删除的行数",name:"每秒删除行数",hideSort:true}
			];

			//InnoDB读写量
			$scope.readAndWriteProps = {"yAxis": {name:"GB"}};

			//InnoDB读写次数
			$scope.readAndWriteNumProps = {"yAxis": {name:"次","minInterval": 1}};

			$scope.loading = true;
			monitorService.setLoading($scope,3,true);
			monitorService.getMysqlMonitorData($scope.data).then(function (response) {
				$scope.loading = false;
				Object.assign($scope.mysqlMonitorData, response.data);
				//添加数据
				$scope.innodbInitData = [$scope.mysqlMonitorData];
				//innodb缓冲区命中率
				$scope.bufferHitRatioInitData = {data:[{"命中率":$scope.mysqlMonitorData.innodb_buffer_pool_hit}]};
				//InnoDB缓冲区使用率、脏块率
				let useRate = ($scope.mysqlMonitorData.innodb_buffer_pool_pages_total - $scope.mysqlMonitorData.innodb_buffer_pool_pages_free) / $scope.mysqlMonitorData.innodb_buffer_pool_pages_total * 100;
				$scope.bufferUseRatioInitData = {data:[{"使用率":useRate.toFixed(1)}]};
				let dirtyRate = $scope.mysqlMonitorData.innodb_buffer_pool_pages_dirty / $scope.mysqlMonitorData.innodb_buffer_pool_pages_total * 100;
				$scope.bufferDirtyRatioInitData = {data:[{"脏块率":dirtyRate.toFixed(1)}]};

				$scope.mysqlMonitorData.dataFsyncs = dbUtils.convertNumUnit($scope.mysqlMonitorData.innodb_data_fsyncs);
				$scope.mysqlMonitorData.lockWaits = dbUtils.convertNumUnit($scope.mysqlMonitorData.innodb_row_lock_waits);
				if($scope.mysqlMonitorData.innodb_row_lock_waits == 0 && !$scope.mysqlMonitorData.innodb_row_lock_time_avg)
					$scope.mysqlMonitorData.innodb_row_lock_time_avg = 0;
				$scope.mysqlMonitorData.lockTimeAvg = $scope.mysqlMonitorData.innodb_row_lock_time_avg;
				$scope.mysqlMonitorData.logWrites = dbUtils.convertNumUnit($scope.mysqlMonitorData.innodb_log_writes);
				$scope.mysqlMonitorData.pagesDirty = dbUtils.convertNumUnit($scope.mysqlMonitorData.innodb_buffer_pool_pages_dirty);
				$scope.mysqlMonitorData.osLogWritten = dbUtils.convertFileSizeAndUnit($scope.mysqlMonitorData.innodb_os_log_written * 1024);

				$scope.readAndWriteInitData = {data:[{"读数量": $scope.mysqlMonitorData.innodb_data_read, "写数量":$scope.mysqlMonitorData.innodb_data_written}]};
				$scope.readAndWriteNumInitData = {data:[{"读次数": $scope.mysqlMonitorData.innodb_data_reads, "写次数":$scope.mysqlMonitorData.innodb_data_writes}]};
				monitorService.setLoading($scope,3,false);
			},function (error) {
				$scope.loading = false;
				monitorService.setLoading($scope,3,false);
				dbUtils.error(error.message);
			})

			//锁等待
			if($scope.has5_7Permission){
				if($scope.menu.isInitData){
					$scope.lockWaits.methods.reloadData();
					$scope.lockTrx.methods.reloadData();
				}else{
					//锁等待
					$scope.lockWaits = {
						urlParams: {url:"/database/monitor/mysql/excute?sqlId=get_innodb_lock_waits", urlBody:$scope.data, method:"postBody"},
						tableParams: {loading:true,labelTitle:"锁等待信息",bodyStyle:{"max-height": "300px"}},
						headers: [
							{name:"锁等待发生时间",field:"wait_started",style:{width:"100px"}},
							{name:"锁已经等待时长",field:"wait_age",style:{width:"100px"}},
							{name:"锁已经等待时长(秒)",field:"wait_age_secs",style:{width:"120px"}},
							{name:"被锁的表",field:"locked_table",style:{width:"100px"}},
							{name:"被锁住的索引",field:"locked_index",style:{width:"100px"}},
							{name:"锁类型",field:"locked_type",style:{width:"70px"}},
						],
						methods: {}
					}

					//锁事务
					$scope.lockTrx = {
						urlParams: {url:"/database/monitor/mysql/excute?sqlId=get_innodb_lock_waits", urlBody:$scope.data, method:"postBody"},
						tableParams: {loading:true,labelTitle:"当前正在等待锁的事务",bodyStyle:{"max-height": "300px"}},
						headers: [
							{name:"等待事务ID",field:"waiting_trx_id",style:{width:"100px"}},
							{name:"等待事务开始时间",field:"waiting_trx_started",style:{width:"120px"}},
							{name:"已等待事务时长",field:"waiting_trx_age",style:{width:"100px"}},
							{name:"正在等待事务被锁行数量",field:"waiting_trx_rows_locked",style:{width:"150px"}},
							{name:"正在等待行重定义的数量",field:"waiting_trx_rows_modified",style:{width:"150px"}},
							{name:"正在等待事务的线程id",field:"waiting_pid",style:{width:"140px"}},
							{name:"正在等待锁的ID",field:"waiting_lock_id",style:{width:"100px"}},
							{name:"等待锁的模式",field:"waiting_lock_mode",style:{width:"100px"}},
							{name:"阻塞等待锁的事务id",field:"blocking_trx_id",style:{width:"140px"}},
							{name:"正在锁的线程id",field:"blocking_pid",style:{width:"100px"}},
							{name:"正在锁的查询",field:"blocking_query",style:{width:"100px"}},
							{name:"正在阻塞等待锁的锁id",field:"blocking_lock_id",style:{width:"140px"}},
							{name:"阻塞锁模式",field:"blocking_lock_mode",style:{width:"100px"}},
							{name:"阻塞事务开始的时间",field:"blocking_trx_started",style:{width:"140px"}},
							{name:"阻塞的事务已经执行的时间",field:"blocking_trx_age",style:{width:"160px"}},
							{name:"阻塞事务锁住的行的数量",field:"blocking_trx_rows_locked",style:{width:"160px"}},
							{name:"阻塞事务重定义行的数量",field:"blocking_trx_rows_modified",style:{width:"160px"}},
							{name:"kill语句杀死正在运行的阻塞事务",field:"sql_kill_blocking_query",style:{width:"200px"}},
							{name:"kill语句杀死会话中正在运行的阻塞事务",field:"sql_kill_blocking_connection",style:{width:"200px"}},
						],
						methods: {}
					}
				}
			}
		}else if($scope.menu.id == 4){//线程
			if($scope.menu.isInitData){
				$scope.threads.methods.reloadData();
			}else{
				$scope.threads = {
					urlParams: {url:"/database/monitor/mysql/excute?sqlId=get_threads", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"各类线程数量",bodyStyle:{"max-height": "500px"}},
					headers: [
						{name:"用户",field:"user"},{name:"线程数",field:"count(*)"}
					],
					methods: {}
				}
			}
		}else if($scope.menu.id == 11){//会话
			//查看当前正在执行的SQL
			if($scope.menu.isInitData){
				$scope.currSql.methods.reloadData();
			}else{
				$scope.currSql = {
					urlParams: {url:"/database/monitor/mysql/excute?sqlId=get_current_statement", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"当前正在执行的SQL",bodyStyle:{"max-height": "300px"}},
					headers: [
						{name:"连接ID",field:"conn_id",style:{width:"100px"}},
						{name:"db",field:"db",style:{width:"150px"}},
						{name:"用户",field:"user",style:{width:"150px"}},
						{name:"当前语句",field:"current_statement"},
						{name:"上条语句",field:"last_statement"}
					],
					methods: {}
				}
			}
		}else if($scope.menu.id == 5){//sql
			if($scope.menu.isInitData){
				$scope.topTenSql.methods.reloadData();
				$scope.latencySql.methods.reloadData();
			}else{
				//执行下面命令查询TOP20最热SQL，改成展示20条，变量名不变
				$scope.topTenSql = {
					urlParams: {url:"/database/monitor/mysql/excute?sqlId=get_top_ten_sql", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"Top20SQL",bodyStyle:{"max-height": "300px"}},
					headers: [
						{name:"db",field:"SCHEMA_NAME",style:{width:"10%"}},{name:"执行次数",field:"COUNT_STAR",style:{width:"10%"}},
                        {name:"语句",field:"DIGEST_TEXT",style:{width:"70%"},compile:true,formatter:function(value,row) {
                            return dbUtils.shortenText(value,50,30)
                        }}
					],
					rowEvents: [{
						class:"btn-info",icon:"fa fa-file",title:"格式化SQL",name: "格式化SQL",
						click: function(row){
                            dbUtils.openModal('tpl/templates/modal_ace.html','UiBaseTableModalAceController',"lg","modal-large",{"data":row["DIGEST_TEXT"],"isShowFormatter":true},function (data) {
                            },true);
						}
					}],
					methods: {}
				}

				//哪些语句延迟比较严重
				$scope.latencySql = {
					urlParams: {url:"/database/monitor/mysql/excute?sqlId=get_avg_latency_statement", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"延迟语句Top10",bodyStyle:{"max-height": "300px"}},
					headers: [
						{name:"db",field:"SCHEMA_NAME",style:{width:"10%"}},
						{name:"执行次数",field:"COUNT_STAR",style:{width:"8%"}},
						{name:"延迟总时间",field:"SUM_TIMER_WAIT",style:{width:"15%"}},
						{name:"延迟最大时间",field:"MAX_TIMER_WAIT",style:{width:"13%"}},
						{name:"延迟平均时间",field:"AVG_TIMER_WAIT",style:{width:"12%"}},
						{name:"语句",field:"DIGEST_TEXT",style:{width:"32%"},compile:true,formatter:function(value,row) {
							return dbUtils.shortenText(value,50,30)
						}}
					],
					rowEvents: [{
						class:"btn-info",icon:"fa fa-file",title:"格式化SQL",name: "格式化SQL",
						click: function(row){
							dbUtils.openModal('tpl/templates/modal_ace.html','UiBaseTableModalAceController',"lg","modal-large",{"data":row["DIGEST_TEXT"],"isShowFormatter":true},function (data) {
							},true);
						}
					}],
					methods: {}
				}
			}
		}else if($scope.menu.id == 6) {//IO
			if($scope.menu.isInitData){
				$scope.ioReq.methods.reloadData();
				$scope.topTenIoReq.methods.reloadData();
			}else{
				//查看TOp10数据文件上发生了多少IO请求
				$scope.ioReq = {
					urlParams: {url:"/database/monitor/mysql/excute?sqlId=get_io_global_by_file_by_bytes", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"Top10数据文件IO请求信息",bodyStyle:{"max-height": "300px"}},
					headers: [
						{name:"文件",field:"file"},
						{name:"读次数",field:"count_read",style:{width:"100px"}},
						{name:"total_read",field:"total_read",style:{width:"100px"}},
						{name:"avg_read",field:"avg_read",style:{width:"100px"}},
						{name:"写次数",field:"count_write",style:{width:"100px"}},
						{name:"total_written",field:"total_written",style:{width:"100px"}},
						{name:"avg_write",field:"avg_write",style:{width:"100px"}},
						{name:"total",field:"total",style:{width:"100px"}},
						{name:"write_pct",field:"write_pct",style:{width:"100px"}}
					],
					methods: {}
				}

				//哪个表上的IO请求最多
				$scope.topTenIoReq = {
					urlParams: {url:"/database/monitor/mysql/excute?sqlId=get_top_ten_io_req", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"IO请求Top10",bodyStyle:{"max-height": "300px"}},
					headers: [
						{name:"文件",field:"file"},
						{name:"读次数",field:"count_read",style:{width:"100px"}},
						{name:"total_read",field:"total_read",style:{width:"100px"}},
						{name:"avg_read",field:"avg_read",style:{width:"100px"}},
						{name:"写次数",field:"count_write",style:{width:"100px"}},
						{name:"total_written",field:"total_written",style:{width:"100px"}},
						{name:"avg_write",field:"avg_write",style:{width:"100px"}},
						{name:"total",field:"total",style:{width:"100px"}},
						{name:"write_pct",field:"write_pct",style:{width:"100px"}}
					],
					methods: {}
				}
			}
		}else if($scope.menu.id == 7) {//客户端
			if($scope.menu.isInitData){
				$scope.hostSummary.methods.reloadData();
			}else{
				//查看每个客户端IP过来的连接消耗了多少资源
				$scope.hostSummary = {
					urlParams: {url:"/database/monitor/mysql/excute?sqlId=get_host_summary", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"客户端连接信息",bodyStyle:{"max-height": "300px"}},
					headers: [
						{name:"客户端",field:"host",style:{width:"110px"}},
						{name:"执行语句总数",field:"statements",style:{width:"85px"}},
						{name:"语句延迟时间",field:"statement_latency",style:{width:"85px"}},
						{name:"语句平均延迟时间",field:"statement_avg_latency",style:{width:"110px"}},
						{name:"表扫描总次数",field:"table_scans",style:{width:"85px"}},
						{name:"文件I/O总次数",field:"file_ios",style:{width:"90px"}},
						{name:"文件I/O总延迟时间",field:"file_io_latency",style:{width:"120px"}},
						{name:"当前连接数",field:"current_connections",style:{width:"85px"}},
						{name:"总链接数",field:"total_connections",style:{width:"75px"}},
						{name:"不同用户数量",field:"unique_users",style:{width:"85px"}},
						{name:"当前账户分配内存",field:"current_memory",style:{width:"120px"}},
						{name:"分配内存总数",field:"total_memory_allocated",style:{width:"85px"}}
					],
					methods: {}
				}
			}
		}else if($scope.menu.id == 8) {//用户
			if($scope.menu.isInitData){
				$scope.userSummary.methods.reloadData();
			}else{
				//查看每个用户消耗了多少资源
				$scope.userSummary = {
					urlParams: {url:"/database/monitor/mysql/excute?sqlId=get_user_summary", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"用户消耗资源信息",bodyStyle:{"max-height": "300px"}},
					headers: [
						{name:"用户名",field:"user",style:{width:"120px"}},
						{name:"执行语句总数",field:"statements",style:{width:"85px"}},
						{name:"语句延迟时间",field:"statement_latency",style:{width:"85px"}},
						{name:"语句平均延迟时间",field:"statement_avg_latency",style:{width:"110px"}},
						{name:"表扫描总次数",field:"table_scans",style:{width:"85px"}},
						{name:"文件I/O总次数",field:"file_ios",style:{width:"90px"}},
						{name:"文件I/O总延迟时间",field:"file_io_latency",style:{width:"120px"}},
						{name:"当前连接数",field:"current_connections",style:{width:"85px"}},
						{name:"总链接数",field:"total_connections",style:{width:"75px"}},
						{name:"不同主机数量",field:"unique_hosts",style:{width:"85px"}},
						{name:"当前账户分配内存",field:"current_memory",style:{width:"120px"}},
						{name:"历史内存分配量",field:"total_memory_allocated",style:{width:"85px"}}
					],
					methods: {}
				}
			}
		}else if($scope.menu.id == 9) {//自增长字段
			if($scope.menu.isInitData){
				$scope.autoIncreCol.methods.reloadData();
			}else{
				//MySQL自增长字段的最大值和当前已经使用到的值
				$scope.autoIncreCol = {
					urlParams: {url:"/database/monitor/mysql/excute?sqlId=get_schema_auto_increment_columns", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"自增长字段",bodyStyle:{"max-height": "300px"}},
					headers: [
						{name:"table_schema",field:"table_schema",style:{width:"100px"}},
						{name:"table_name",field:"table_name",style:{width:"100px"}},
						{name:"column_name",field:"column_name",style:{width:"85px"}},
						{name:"data_type",field:"data_type",style:{width:"85px"}},
						{name:"column_type",field:"column_type",style:{width:"85px"}},
						{name:"is_signed",field:"is_signed",style:{width:"90px"}},
						{name:"is_unsigned",field:"is_unsigned",style:{width:"85px"}},
						{name:"max_value",field:"max_value",style:{width:"85px"}},
						{name:"auto_increment",field:"auto_increment",style:{width:"100px"}},
						{name:"auto_increment_ratio",field:"auto_increment_ratio",style:{width:"120px"}},
					],
					methods: {}
				}
			}
		}else if($scope.menu.id == 10) {//索引
			if($scope.menu.isInitData){
				$scope.indexInfo.methods.reloadData();
				$scope.indexRedundant.methods.reloadData();
				$scope.indexUnused.methods.reloadData();
			}else{
				//MySQL索引使用情况统计
				$scope.indexInfo = {
					urlParams: {url:"/database/monitor/mysql/excute?sqlId=get_schema_index_statistics", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"索引使用情况统计",bodyStyle:{"max-height": "300px"}},
					headers: [
						{name:"table_schema",field:"table_schema",style:{width:"100px"}},
						{name:"table_name",field:"table_name",style:{width:"100px"}},
						{name:"index_name",field:"index_name",style:{width:"85px"}},
						{name:"rows_selected",field:"rows_selected",style:{width:"100px"}},
						{name:"select_latency",field:"select_latency",style:{width:"100px"}},
						{name:"rows_inserted",field:"rows_inserted",style:{width:"100px"}},
						{name:"insert_latency",field:"insert_latency",style:{width:"100px"}},
						{name:"rows_updated",field:"rows_updated",style:{width:"100px"}},
						{name:"update_latency",field:"update_latency",style:{width:"100px"}},
						{name:"rows_deleted",field:"rows_deleted",style:{width:"100px"}},
						{name:"delete_latency",field:"delete_latency",style:{width:"100px"}},
					],
					methods: {}
				}

				//冗余索引
				$scope.indexRedundant = {
					urlParams: {url:"/database/monitor/mysql/excute?sqlId=get_schema_redundant_indexes", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"冗余索引",bodyStyle:{"max-height": "300px"}},
					headers: [
						{name:"table_schema",field:"table_schema",style:{width:"100px"}},
						{name:"table_name",field:"table_name",style:{width:"100px"}},
						{name:"redundant_index_name",field:"redundant_index_name",style:{width:"160px"}},
						{name:"redundant_index_columns",field:"redundant_index_columns",style:{width:"180px"}},
						{name:"redundant_index_non_unique",field:"redundant_index_non_unique",style:{width:"190px"}},
						{name:"dominant_index_name",field:"dominant_index_name",style:{width:"160px"}},
						{name:"dominant_index_columns",field:"dominant_index_columns",style:{width:"180px"}},
						{name:"dominant_index_non_unique",field:"dominant_index_non_unique",style:{width:"190px"}},
					],
					methods: {}
				}

				//无用索引
				$scope.indexUnused = {
					urlParams: {url:"/database/monitor/mysql/excute?sqlId=get_schema_unused_indexes", urlBody:$scope.data, method:"postBody"},
					tableParams: {loading:true,labelTitle:"无用索引",bodyStyle:{"max-height": "300px"}},
					headers: [
						{name:"object_schema",field:"object_schema",style:{width:"100px"}},
						{name:"object_name",field:"object_name",style:{width:"100px"}},
						{name:"index_name",field:"index_name",style:{width:"100px"}},
					],
					methods: {}
				}
			}
		}
		$scope.menu.isInitData = true;
	}

	function roll(total, idname, step, tabValue, fixedValue) {
		let n = 0;
		return function () {
			let t = (n*1000 + step*1000)/1000;//解决0.1+0.2 = 0.30000000000000004的问题
			n = t >= total ? total : t;
			if (n <= total) {
				if(tabValue == $scope.menu.id){
					$timeout(function () {
						$scope.mysqlMonitorData[idname] = n.toFixed(fixedValue);
					})
				}
				else
					$scope.mysqlMonitorData[idname] = total.toFixed(fixedValue);;
			}
		}
	}
	function start(index, idname, step, tabValue, fixedValue, runtime = 1500){
		fixedValue = !fixedValue ? 2 : fixedValue;
		let rolling = roll(index, idname, step, tabValue, fixedValue);
		let timer;
		runtime = (runtime >= 300) ? runtime : 300;
		for (let i = 0; i < (index/step); i++) {
			timer = setTimeout(rolling, (runtime/index)*i*step)
		}
		if(index == 0)
			$scope.mysqlMonitorData[idname] = index;
	}

	//返回主页
	$scope.returnMain = function(){
		$scope.refreshTime = "off";
		$scope.showContent = false;
		//重置变量
		$scope.mysqlVersion = null;
		$scope.threadInitData = null;
		$scope.qpsAndTpsInitData = null;
		$scope.networkInitData = null;
		$scope.connectInitData = null;
		$scope.bufferInitData = null;
		$scope.openNumInitData = null;
		$scope.readAndWritesInitData = null;
		$scope.sqlNumInitData = null;
		$scope.innodbInitData = null;
		$scope.bufferHitRatioInitData = null;
		$scope.bufferUseRatioInitData = null;
		$scope.bufferDirtyRatioInitData = null;
		$scope.readAndWriteInitData = null;
		$scope.readAndWriteNumInitData = null;
		if(intervalId)
			clearInterval(intervalId);//主进程停止定时
		$window.sessionStorage.removeItem("baseTableBarData");
		$scope.$emit("returnMain",{});
	}
}]);

