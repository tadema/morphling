'use strict';
/* Ogg监控页面 */
app.controller('MonitorOggController',
		['$rootScope', '$scope', '$state', '$cookieStore','AccountService','$modal','dbUtils','$http','EchoLog','$filter', 'MonitorService',
        function ($rootScope, $scope, $state,$cookieStore, accountService,$modal,dbUtils,$http,echoLog,$filter,monitorService) {
	
	function setAllHidden(){
		$scope.isShowMain = false;
		$scope.isShowOgg = false;
	}
	setAllHidden();
	$scope.isShowMain = true;
	
	$scope.$on('returnMain',function(event,data){
		setAllHidden();
		$scope.isShowMain = true;
	})
	
	$scope.t_id = dbUtils.guid();
	$scope.monitor = {"status":{}};
	var table = {
        url:"/goldengate/list",
        showUpload: false,
        pageSize:10,
        headers: [
			{name:"名称",field:"name",style:{"width":"15%"}},
            {name:"类型",field:"oggType",style:{"width":"10%"},compile:true,formatter:function(value,row){
            	if(value == "01"){
            		return "<span>源端服务器</span>";
            	}else if(value == "02"){
            		return "<span>目标端服务器</span>";
            	}
            }},
            {name:"IP",field:"ip",style:{"width":"15%"}},
            {name:"端口",field:"port",style:{"width":"10%"}},
            {name:"用户名",field:"userName",style:{"width":"10%"}},
            {name:"路径",field:"oggPath",style:{"width":"15%"}},
            {name:"状态",field:"status",style:{"width":"10%"},compile:true,formatter:function(value,row){
            	row.status = 0;
            	$scope.monitor['status'][row.id] = 0;
            	var template = "<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-danger\",1:\"text-success\"}[monitor.status["+row.id+"]]'></i>{{{0:\"未连接\",1:\"已连接\"}[monitor.status["+row.id+"]]}}</span>";
            	monitorService.checkConnectOgg(row).then(function(response){
            		if(response.code== SUCCESS_CODE){
						row.status = response.data;
            			$scope.monitor['status'][row.id] = response.data;
            		}
            	});
            	return template;
            }},
        ],
        operationEvents: [{
            class:"btn-info",
            icon:"glyphicon glyphicon-plus",//glyphicon glyphicon-plus
            name:"配置OGG",
            click:function(){
            	dbUtils.openModal('tpl/monitor/monitor_ogg_edit.html','MonitorOggEditController',"lg","",{},function (data) {
            		if(data != false){
            			monitorService.saveMonitorOgg(data).then(function(response){
            				if(response.code == SUCCESS_CODE){
            					dbUtils.info("保存成功","提示");
            					$scope.table.operations.reloadData();
            				}else{
            					dbUtils.error(response.message,"提示");
            				}
            			})
            		}
                });
            }
        }],
        rowEvents:[{
        	class:"btn btn-default btn-info",
        	icon:"fa fa-line-chart",
        	title:"查看监控图表",
        	name:"监控",
        	click: function(row){
//				$state.go("app.monitorOGGProgress",{"oggMonitorData":row});
        		setAllHidden();
        		$scope.isShowOgg = true;
        		$scope.$broadcast("sendDataToOgg",{"oggMonitorData":row});
        	}
        },{
        	class:"btn btn-default btn-info",
        	icon:"fa fa-pencil",
        	title:"编辑",
        	click: function(row){
        		dbUtils.openModal('tpl/monitor/monitor_ogg_edit.html','MonitorOggEditController',"lg","",{'data':angular.copy(row)},function (data) {
            		if(data != false){
            			monitorService.updateMonitorOgg(data).then(function(response){
            				if(response.code == SUCCESS_CODE){
            					dbUtils.info("保存成功","提示");
            					$scope.table.operations.reloadData();
            				}else{
            					dbUtils.error(response.message,"提示");
            				}
            			})
            		}
                });
        	}
        },{
        	class:"btn-danger",
            icon:"fa fa-trash",
            title:"删除",
            isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	monitorService.deleteMonitorOgg(row.id).then(function(response){
    				if(response.code == SUCCESS_CODE){
    					dbUtils.info("删除成功","提示");
    					$(event.target).parents(".popover").prev().click();
    					$scope.table.operations.reloadData();
    				}else{
    					dbUtils.error(response.message,"提示");
    				}
    			})
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        afterReload:function(){
        	//查询条件+按钮--45+42	2个padding--30
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-($rootScope._screenProp.tableUselessHeight1)+"px";
        	},100)
        },
        settings:{
            cols:3,
            filterId: "monitorOggFilter",
            showCheckBox:false,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-34-56-20)+"px",
            },
        }
    }
    $scope.table = table;
	
}]);

app.controller('MonitorOggEditController', ['dbUtils','$scope','$modalInstance', '$state','source','MonitorService',
    function (dbUtils,$scope,$modalInstance, $state,source,monitorService) {

	$scope.data;
	$scope.isEdit;
	$scope.pObject = {};
	$scope.buttonClickFlag = false;
	if(source.data && source.data.id){
		$scope.isEdit = true;
		$scope.data = source.data;
		if($scope.data.interval){
			if(parseInt($scope.data.interval) == 0){
				$scope.data.isStartAlert = 0;
				$scope.data.interval = "";
				$scope.data.receverMails = "";
			}else{
				$scope.data.isStartAlert = 1;
				$scope.data.interval = parseInt($scope.data.interval);
			}
		}else{
			$scope.data.isStartAlert = 0;
		}
	}else{
		$scope.isEdit = false;
		$scope.data = {isStartAlert: 0,oggType:"01"};
	}
	
	function initMailServer(){
		monitorService.getMailServers().then(function(response){
    		if (response.code == SUCCESS_CODE) {
    			$scope.mailServers = response.data;
    			if($scope.isEdit && $scope.data.sendType == "01" && $scope.data.mailId){
    				for(let item of $scope.mailServers){
    					if(item.id == $scope.data.mailId){
    						$scope.pObject.mailServer = item;
    						return;
    					}
    				}
    			}
    		} else {
    			dbUtils.error(response.message,"提示");
    		}
    	})	
	}
	initMailServer();
	
	$scope.mailServerChangeHandle = function(){
		$scope.buttonClickFlag = true;
	}
	
	$scope.$watch('data', function(newValue, oldValue) {
		if (newValue != oldValue){
			$scope.buttonClickFlag = true;
        }
    },true);
	
//	把controller中对端口检验的配置转移到ui-dbUtils.js中，方便管理和使用,提供ipPortList和oldData的初始化
	dbUtils.startPortCheck($scope,$scope.isEdit,$scope.data);

	$scope.checkPortDuplicate = function(ip,data,propNameList){
		return dbUtils.checkPortDuplicate(ip,data,propNameList,$scope.ipPortList,$scope.oldData,$scope.isEdit,"ip");
    }

	$scope.testConnect = function(){
    	if($scope.form.$invalid){
            return;
        }
    	$scope.testFlag = true;
		$scope.connected = 2;//查询中
    	monitorService.checkConnectOgg($scope.data).then(function(response){
    		// $scope.testFlag = true;
    		if(response.code == SUCCESS_CODE){
    			$scope.connected = response.data;
    		}else{
    			dbUtils.error(response.message,"提示");
    		}
    	})
    }
	
	$scope.submitApply = function () {
		if($scope.form.$invalid){
            return;
        }
		if($scope.buttonClickFlag == false){
			dbUtils.info("信息没有改动，不能保存","提示");
			return;
		}else{
			var idDuplicate = $scope.checkPortDuplicate($scope.data.ip,$scope.data,['port']);
	        if(idDuplicate == true)
	        	return;
	        if($scope.data.isStartAlert == 1){
	        	if(!$scope.data.interval || !$scope.data.receverMails){
	        		dbUtils.warning("请将信息填写完整");
	        		return;
	        	}
	        	if($scope.data.interval <= 0){
	        		dbUtils.warning("监控周期必须大于0");
	        		return;
	        	}
	        	if(!$scope.data.sendType){
	        		dbUtils.warning("请选择告警发送类型");
	        		return;
	        	}
	        	if(Object.keys($scope.pObject.mailServer).length == 0){
	        		dbUtils.warning("请选择邮件服务器");
	        		return;
	        	}
	        	if($scope.data.sendType == "01"){
	        		$scope.data.mailId = $scope.pObject.mailServer.id;
	        		$scope.data.messageId = "";
	        	}else if($scope.data.sendType == "02"){
	        		$scope.data.messageId = "";
	        		$scope.data.mailId = "";
	        	}
	        }else{
	        	$scope.data.interval = "";
	        	$scope.data.receverMails = "";
	        	$scope.data.mailId = "";
	        	$scope.data.sendType = "";
	        	$scope.data.messageId = "";
	        }
			$modalInstance.close($scope.data);
		}
	};
	
	$scope.cancel = function () {
	    $modalInstance.close(false);
	};
}]);

app.controller('MonitorOggProgressController',
		['$rootScope', '$scope', '$state','$stateParams', '$cookieStore','$window','$modal','dbUtils','$http','EchoLog','$filter', 'MonitorService',
        function ($rootScope, $scope, $state,$stateParams,$cookieStore, $window,$modal,dbUtils,$http,echoLog,$filter,monitorService) {
	
	$scope.$on('sendDataToOgg',function(event,data){
		$scope.isShowSingleProcess = false;
		$scope.data = data.oggMonitorData;
		$scope.query = {refresh:"manual"};
		$scope.refreshName = "刷新";
		setButtonOff();
		$scope.refreshList = [{value:'manual',show:'手动'},{value:'1',show:'1分钟'},{value:'5',show:'5分钟'},{value:'10',show:'10分钟'},{value:'15',show:'15分钟'},{value:'30',show:'半小时'}];
		initData();	
	})
	let intervalId;
	
	$scope.refreshChange = function(){
		if($scope.query.refresh != "manual"){
			$scope.refreshName = "停止刷新";
			$scope.refreshTime = parseFloat($scope.query.refresh)*60*1000;//按分钟计算
			if(intervalId){
				clearInterval(intervalId);
			}
			setButtonOn();
			if($scope.isShowSingleProcess == false)
				intervalId = setInterval(function () {
					$scope.$broadcast("refreshData",{})
				},$scope.refreshTime);
			else
				intervalId = setInterval(querySingleProcess,$scope.refreshTime);
		}else{
			$scope.refreshName = "刷新";
			$scope.refreshTime = "off";
			setButtonOff();
			if(intervalId){
				clearInterval(intervalId);
			}
		}
	}
	
	$scope.operateRefresh = function(){
		if($scope.query.refresh != "manual"){
			if($scope.isRefreshing == true){
				$scope.refreshTime = "off";//组件监控变量，会停止刷新
				setButtonOff();
				if(intervalId)
					clearInterval(intervalId);//主进程停止定时
				$scope.refreshName = "继续刷新";
			}else{
				$scope.refreshChange();
			}
		}else{
			setButtonOn();
			if($scope.isShowSingleProcess == false)
				$scope.$broadcast("refreshData",{})
			else
				querySingleProcess();
			$scope.refreshTime = "off";
			setTimeout(function () {
				setButtonOff();
			},200)
		}
	}
	
	function setButtonOn(){
		$scope.isRefreshing = true;
		$scope.refreshClass = "btn-success";
		$scope.refreshIClass = "fa-spin";
	}
	
	function setButtonOff(){
		$scope.isRefreshing = false; 
		$scope.refreshClass = "btn-danger";
		$scope.refreshIClass = "";
	}
	
	function initData(){
		//ogg
		$scope.oggProgressTableParams = {labelTitle:"ogg进程查询", bodyStyle:{"max-height": $rootScope._screenProp.contentHeight-30-39-30-31-15+"px"}};
		$scope.oggProgressUrlParams = {url:"/goldengate/listInfoAll", urlBody:$scope.data, method:"postBody"};
		$scope.pageParams = {isPagination:true,postgroundPagination:false,pageNumber: 1,pageSize:10,prevPageDisabled: 'disabled',nextPageDisabled: 'disabled'};
		$scope.tooltipData = {"program":{},"programInfo":{}, "status":{}};
		$scope.currMenuId = 0;
		$scope.oggProgressHeaders = [
			{name:"进程类型",field:"program",style:{"width":"20%"},compile:true,formatter:function(value,row){
	        	let code,text;
	        	switch(value){
	        	case "EXTRACT":text = '抽取进程';code=1;break;
	        	case "REPLICAT":text = '复制进程';code=2;break;
	        	case "DUMP":text = '投递进程';code=3;break;
	        	case "MANAGER":text = '管理进程';code=4;break;
	        	}
				$scope.tooltipData.program[row.id] = code;
				$scope.tooltipData.programInfo[row.id] = getTitleShowValue(row);
				return "<span data-toggle='tooltip' tooltip-placement='auto right' tooltip-trigger='outsideClick' uib-tooltip-html='tooltipData.programInfo["+row.id+"]'>{{{1:\"抽取进程\",2:\"复制进程\",3:\"投递进程\",4:\"管理进程\"}[tooltipData.program[\""+row.id+"\"]]}}</span>";
	        }},
			{name:"状态",field:"status",style:{"width":"10%"},compile:true,formatter:function(value,row){
	        	let style,text,code;
	        	switch(value){
	        	case "STARTING":text = '启动中';style='text-info';code=1;break;
	        	case "RUNNING":text = '运行';style='text-success';code=2;break;
	        	case "STOPPED":text = '停止';style='text-danger';code=3;break;
	        	case "ABENDED":text = '异常';style='text-danger';code=4;break;
	        	}
				$scope.tooltipData.status[row.id] = code;
	        	let tooltipValue = getTitleShowValue(row);
				return "<span data-toggle='tooltip' tooltip-placement='auto right' tooltip-trigger='outsideClick' uib-tooltip-html='tooltipData.programInfo["+row.id+"]'>" +
					"<i style='margin-right:5px;' class='fa fa-circle' ng-class='{1:\"text-info\",2:\"text-success\",3:\"text-danger\",4:\"text-danger\"}[tooltipData.status["+row.id+"]]'></i>{{{1:\"启动中\",2:\"运行\",3:\"停止\",4:\"异常\"}[tooltipData.status["+row.id+"]]}}</span>";
	        	// return "<span data-toggle='tooltip' data-placement='auto right' data-trigger='click hover' onmouseenter='$(this).tooltip(\"show\")' data-html='true' title='"+tooltipValue+"'><i style='margin-right:5px;' class='fa fa-circle "+style+"'></i>"+text+"</span>";
	        }},
			{name:"进程名称",field:"group",style:{"width":"15%"},compile:true,formatter:function(value,row){
				if(row.program == "MANAGER"){
					value = "--";
				}
	        	let tooltipValue = getTitleShowValue(row);
	        	return "<span data-toggle='tooltip' tooltip-placement='auto right' tooltip-trigger='outsideClick' uib-tooltip-html='tooltipData.programInfo["+row.id+"]'>"+value+"</span>";
	        }},{name:"检查点延时",field:"lagatChkpt",title:"最后一个检查点的最后一条记录与当前系统时间的时间差",style:{"width":"20%"},compile:true,formatter:function(value,row){
	        	if(row.program == "MANAGER"){
					value = "--";
				}
	        	let tooltipValue = getTitleShowValue(row);
	        	return "<span data-toggle='tooltip' tooltip-placement='auto right' tooltip-trigger='outsideClick' uib-tooltip-html='tooltipData.programInfo["+row.id+"]'>"+value+"</span>";
	        }},{name:"检查点未更新时长",field:"timeSinceChkpt",title:"最近一个检查点与当前系统时间的时间差",style:{"width":"20%"},compile:true,formatter:function(value,row){
	        	if(row.program == "MANAGER"){
					value = "--";
				}
	        	let tooltipValue = getTitleShowValue(row);
	        	return "<span data-toggle='tooltip' tooltip-placement='auto right' tooltip-trigger='outsideClick' uib-tooltip-html='tooltipData.programInfo["+row.id+"]'>"+value+"</span>";
	        }}
		];
		$scope.oggProgressRowEvents = [{
            class:"btn-info",icon:"",name:"更多信息",
            click: function(row){
            	if(row.program == "MANAGER"){
            		dbUtils.info("管理进程不能访问，请选择其他进程")
            		return;
            	}
            	$scope.refreshTime = "off";
            	$scope.query.refresh = "manual";
            	$scope.refreshName = "刷新";
            	setButtonOff();
        		if(intervalId)
        			clearInterval(intervalId);//主进程停止定时
            	$scope.isShowSingleProcess = true;
            	$scope.process = row;
            	$scope.menu = $scope.menus[0];
            	initProcessFile();
            }
        }]
	}
	
	function getTitleShowValue(row){
		let titleShowValue = "";
		let title = {};
		switch(row.program){
	    	case "EXTRACT":title.programCode = 1;title.programValue = '抽取进程';titleShowValue += (title.programValue + "<br>进程名称："+row.group);break;
	    	case "REPLICAT":title.programCode = 2;title.programValue = '复制进程';titleShowValue += (title.programValue + "<br>进程名称："+row.group);break;
	    	case "DUMP":title.programCode = 3;title.programValue = '投递进程';titleShowValue += (title.programValue + "<br>进程名称："+row.group);break;
	    	case "MANAGER":title.programCode = 4;title.programValue = '管理进程';titleShowValue += (title.programValue);break;
    	}
		
		switch(row.status){
	    	case "STARTING":title.statusCode = 1;title.statusValue = '启动中';titleShowValue += ("<br>状态："+title.statusValue);break;
	    	case "RUNNING":title.statusCode = 2;title.statusValue = '运行';titleShowValue += ("<br>状态："+title.statusValue);break;
	    	case "STOPPED":title.statusCode = 3;title.statusValue = '停止';titleShowValue += ("<br>状态："+title.statusValue);break;
	    	case "ABENDED":title.statusCode = 4;title.statusValue = '异常';titleShowValue += ("<br>状态："+title.statusValue);break;
    	}
		if(title.programCode != 4 && (title.statusCode == 3 || title.statusCode == 4)){
			titleShowValue += ("<br>检查点未更新时长：" + row.timeSinceChkpt+"(小时)");
		}
    	return titleShowValue;
	}
	
	function querySingleProcess(){
		if($scope.process){
			//显示单个进程，并执行刷新时的操作
			monitorService.listInfoAll($scope.data).then(function(response){
				if(response.code == SUCCESS_CODE){
					for(let item of response.data){
						if($scope.process.group == item.group){
							$scope.process = item;
							return;
						}
					}
				}else{
					dbUtils.error(response.message,"提示");
				}
			})
		}
	}
	
	//返回主页
	$scope.returnMain = function(){
		$scope.refreshTime = "off";
		if(intervalId)
			clearInterval(intervalId);//主进程停止定时
//		$state.go("app.monitorOGG");
		$scope.isShowSingleProcess = true;
		$scope.$emit("returnMain",{});
	}
	//单个进程页
	$scope.menus = [{name:"prm文件",id:5,className:"active"},{name:"rpt文件",id:6},{name:"进程详细信息",id:7},{name:"checkPoint信息",id:1},{name:"进程处理记录数",id:2},{name:"延时信息",id:3}];
	
	
	$scope.processStyle = {"height":$rootScope._screenProp.contentHeight - 172 -3+ "px"};
	
	$scope.chooseMenu = function(event,menu){
		event = event.currentTarget;
		$(event).closest('ul').find("li").removeClass('active');
		$(event).addClass('active');
		$scope.menu = menu;
		initProcessFile();
		/*if(!$scope.menu.isInitData || $scope.menu.isInitData == false){
			initProcessFile();
		}*/
	}
	
	function initProcessFile(){
//		$scope.menu.isInitData = true;
		if($scope.currMenuId == $scope.menu.id)
			return;
		$scope.loading = true;
		$scope.currMenuId = $scope.menu.id;
		if($scope.menu.id == 1){
			monitorService.getCheckpointInfo($scope.data,$scope.process["group"]).then(function(response){
				if(response.code == SUCCESS_CODE){
					$scope.loading = false;
					$scope.checkpointInfoData = response.data;
				}else{
					dbUtils.error(response.message,"提示");
				}
			})
		}else if($scope.menu.id == 2){
			monitorService.getStats($scope.data,$scope.process["group"]).then(function(response){
				if(response.code == SUCCESS_CODE){
					$scope.loading = false;
					$scope.statsData = response.data;
				}else{
					dbUtils.error(response.message,"提示");
				}
			})
		}else if($scope.menu.id == 3){
			monitorService.getLagProcess($scope.data,$scope.process["group"]).then(function(response){
				if(response.code == SUCCESS_CODE){
					$scope.loading = false;
					$scope.lagProcessData = response.data;
				}else{
					dbUtils.error(response.message,"提示");
				}
			})
		}else if($scope.menu.id == 5){
			if(!$scope.process.group){
				dbUtils.info("group不能为空");
				return;
			}
			let name = "/dirprm/" + $scope.process.group.toLowerCase() + ".prm";
			monitorService.getDirPrmFile($scope.data,name).then(function(response){
				if(response.code == SUCCESS_CODE){
					$scope.loading = false;
					$scope.prmData = response.data;
				}else{
					dbUtils.error(response.message,"提示");
				}
			})
		}else if($scope.menu.id == 6){
			if(!$scope.process.group){
				dbUtils.info("group不能为空");
				return;
			}
			let name = "/dirrpt/" + $scope.process.group + ".rpt";
			monitorService.getDirPrmFile($scope.data,name).then(function(response){
				if(response.code == SUCCESS_CODE){
					$scope.loading = false;
					$scope.rptData = response.data;
				}else{
					dbUtils.error(response.message,"提示");
				}
			})
		}else if($scope.menu.id == 7){
			monitorService.getProcessDetail($scope.data,$scope.process["group"]).then(function(response){
				if(response.code == SUCCESS_CODE){
					$scope.loading = false;
					$scope.processDetailData = response.data;
				}else{
					dbUtils.error(response.message,"提示");
				}
			})
		}else{
			$scope.loading = false;
		}
	}
	
	function showFileDetail(name,fileTitle){
		monitorService.getDirPrmFile($scope.data,name).then(function(response){
			if(response.code == SUCCESS_CODE){
				var instance = $modal.open({
			        animation: true,
			        templateUrl: 'tpl/monitor/monitor_show_detail.html',
			        controller: function ($modalInstance, $scope,source) {
			        	$scope.detailData = source.data;
			        	$scope.fileTitle = source.fileTitle;
			        	
			        	$scope.cancel = function () {
			        	    $modalInstance.close(false);
			        	};
	                },
			        size: "lg",
			        backdrop: "static",
			        resolve: {
			            source: function () {
			                return {"data":response.data,"fileTitle":fileTitle};
			            }
			        }
			    });
			}else{
				dbUtils.error(response.message,"提示");
			}
    	})
	}
		
	$scope.returnUpperLevel = function(){
		$scope.refreshTime = "off";
		if(intervalId)
			clearInterval(intervalId);//主进程停止定时
		$scope.isShowSingleProcess = false;
		$scope.menu = null;
		$scope.process = null;
	}
}]);

