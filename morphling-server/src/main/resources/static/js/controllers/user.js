'use strict';

let UserController = app.controller('UserController', ['$scope','$rootScope','$modal','$compile','dbUtils','UserService','$sce','OrganizationService',
                                  function($scope,$rootScope,$modal,$compile,dbUtils,userService,$sce,organizationService) {
	//$scope.ztreeData,$scope.idToLabel
    organizationService.getOrgZTreeData($scope);
    
	$scope.errorNum = 0;
    $scope.isAdmin = $rootScope._userinfo.isAdmin;
    $scope.t_id = dbUtils.guid();
    var table = {
        url:"/user",
        showUpload: false,
        pageSql:true,
        /*params:[
            {name:"name",label:"姓名",labelCols:4,type:"text"},
            {name:"username",label:"用户名",labelCols:4,type:"text"}
        ],*/
        headers: [
            {name:"姓名",field:"name",style:{width:"10%"}},
            {name:"用户名",field:"username",style:{"width":"8%"}},
            {name:"组织机构",field:"orgNo",style:{width:"14%"},compile:true,formatter:function(value,row){
            	return row.orgNo ? $scope.idToLabel[value] : "--";
            }},
            {name:"电话",field:"phone",style:{width:"10%"}},
            {name:"邮箱",field:"email",style:{width:"12%"}},
            {name:"上次登录IP",field:"lastLoginIp",style:{width:"10%"},compile:true,formatter:function(value,row){
            	return row.lastLoginIp ? value : "--";
            }},
            {name:"上次登录时间",field:"lastLoginTime",style:{width:"14%"}},
        ],
        operationEvents: [{
            class:"btn-info",
            icon:"glyphicon glyphicon-plus",
            name:"新增用户",
            click:function(){
            	if($scope.isAdmin == 0){
        			dbUtils.error("普通用户没有权限添加用户","提示");
        			return;
        		}else{
        			var instance = $modal.open({
                        animation: true,
                        templateUrl: 'tpl/system/user_add.html',
                        controller: 'UserAddController',
                        size: "lg",
                        backdrop: "static",
                        resolve: {
                            source: function () {
                                return {"data":null,"action":"add","ztreeData":$scope.ztreeData,"idToLabel":$scope.idToLabel};
                            }
                        }
                    });
                    instance.result.then(function (result) {
                    	if(result)
                    		$scope.table.operations.reloadData()
                    });
        		}
            }
        }],
        rowEvents:[{
            class:"btn-info",
            icon:"fa fa-user",
            title:"角色",
            isShow:function(row){
                return row.isAdmin == 0;
            },
            click: function(row){
            	if($scope.isAdmin == 0 && $rootScope._userinfo.id != row.id){
        			dbUtils.error("普通用户没有权限修改其他用户信息","提示");
        			return;
        		}else{
        			var instance = $modal.open({
                        animation: true,
                        templateUrl: 'tpl/system/user_role.html',
                        controller: 'UserRoleController',
                        size: "lg",
                        backdrop: "true",
                        resolve: {
                            source: function () {
                                return angular.copy(row);
                            }
                        }
                    });
                    instance.result.then(function (result) {
                    	if(result)
                    		$scope.table.operations.reloadData()
                    });
        		}
            }
        },{
            class:"btn-info",
            icon:"fa fa-list",
            title:"项目",
            isShow:function(row){
                return row.isAdmin == 0;
            },
            click: function(row){
            	if($scope.isAdmin == 0 && $rootScope._userinfo.id != row.id){
        			dbUtils.error("普通用户没有权限修改其他用户信息","提示");
        			return;
        		}else{
        			var instance = $modal.open({
                        animation: true,
                        templateUrl: 'tpl/system/user_app.html',
                        controller: 'UserAppController',
                        size: "lg",
                        backdrop: "static",//true
                        windowClass: "modal-large",
                        resolve: {
                            source: function () {
                                return row;
                            }
                        }
                    });
                    instance.result.then(function () {
                    });
        		}
            }
        },{
            class:"btn-info",
            icon:"fa fa-pencil",
            title:"编辑",
            isShow:function(row){
                return row.isAdmin == 0;
            },
            click: function(row){
                if($scope.isAdmin == 0 && $rootScope._userinfo.id != row.id){
                    dbUtils.error("普通用户没有权限修改其他用户信息","提示");
                    return;
                }else{
                    var instance = $modal.open({
                        animation: true,
                        templateUrl: 'tpl/system/user_add.html',
                        controller: 'UserAddController',
                        size: "lg",
                        backdrop: "static",
                        resolve: {
                            source: function () {
                                return {"data":angular.copy(row),"action":"edit","ztreeData":$scope.ztreeData,"idToLabel":$scope.idToLabel};
                            }
                        }
                    });
                    instance.result.then(function (result) {
                        if(result)
                            $scope.table.operations.reloadData()
                    });
                }
            }
        },{
        	class:"btn-info",
        	icon:"fa fa-key",
        	title:"修改密码",
        	isShow:function(row){
        		return row.isAdmin == 0;
        	},
        	click: function(row){
	        	if($scope.isAdmin == 0 && $rootScope._userinfo.id != row.id){
	    			dbUtils.error("普通用户没有权限修改其他用户信息","提示");
	    			return;
	    		}else{
	    			var instance = $modal.open({
	                    animation: true,
	                    templateUrl: 'tpl/system/user_modifypwd.html',
	                    controller: 'UserAddController',
	                    size: "lg",
	                    backdrop: "true",
	                    resolve: {
	                        source: function () {
	                            return {"data":angular.copy(row),"action":"modifypwd"};
	                        }
	                    }
	                });
	                instance.result.then(function (result) {
	                	if(result)
	                		$scope.table.operations.reloadData()
	                });
	    		}
	        }
        },{
        	class:"btn-danger",
           	icon:"fa fa-trash",
           	title:"删除",
            isShow:function(row){
                return row.isAdmin == 0;
            },
            isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	if($scope.isAdmin == 0 && $rootScope._userinfo.id != row.id){
        			dbUtils.error("普通用户没有权限删除其他用户","提示");
        			$(event.target).parents(".popover").prev().click();
        			return;
        		}else{
                	userService.delete(row.id).then(function(response){
                        if (response.code == SUCCESS_CODE) {
                            dbUtils.success("删除成功","提示");
                            $(event.target).parents(".popover").prev().click();
                            $scope.table.operations.reloadData()
                        } else {
                            dbUtils.error(response.message,"提示");
                        }
                    })
        		}
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        settings:{
            cols:3,
            showCheckBox:false,
            filterId: "userFilter",
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-34-56-20)+"px",
            },
        },
        afterReload:function(){
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight2+"px";
        	},200)
        }
    }
    $scope.table = table;
    
    $scope.deleteCancel = function (row){
    	console.log(row)
    	$('popover').popover("hide");
    }
}]);

let UserAppController = app.controller('UserAppController', ['$scope','$rootScope','$modalInstance','$compile','dbUtils','source','UserService','RoleService','ApplicationService',
                                     function($scope,$rootScope,$modalInstance,$compile,dbUtils,source,userService,roleService,appService) {
    $scope.userId = source.id;
    $scope.username = source.name;
    $scope.t_id = dbUtils.guid();
    if(angular.isUndefined($scope.userId)){
    	console.log("userId undefined");
        $modalInstance.close(false);
    }else{
    	loadApps();
    }

    function loadApps(){
        userService.listApps($scope.userId).then(function(response){
            $scope.apps = response.data;
        })
    }

     function initTableData() {
         $scope.urlParams = {url:"/user/"+$scope.userId+"/apps?exclude=true", urlBody:{}, method:"get"};
         $scope.userAppTableParams = {loading:true,bodyStyle:{"max-height": "500px"}};
         $scope.pageParams = {isPagination:true,postgroundPagination:true,pageNumber: 1,pageSize:10,prevPageDisabled: 'disabled',nextPageDisabled: 'disabled'};
         $scope.userAppHeaders = [
             {name:"名称",field:"name"},{name:"描述",field:"description"},{name:"文件类型",field:"fileType"},{name:"部署类型",field:"middlewareType"},
             {name:"环境",field:"env"},{name:"创建时间",field:"createTime"},
         ];
         $scope.userAppRowEvents = [{
             class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"添加项目",name: "添加",
             click: function(row){
                 userService.addApp($scope.userId,row.id).then(function(response){
                     loadApps();
                     $scope.$broadcast("refreshData",{});
                 });
             }
         }];
         $scope.userAppOperationEvents = [{
             class:"btn-info",icon:"fa fa-css3",name:"添加",
             click:function(rows){
                 if(angular.isUndefined(rows) || rows.length == 0){
                     dbUtils.info("请选择一条数据");
                     return;
                 }
                 var appIds = [];
                 for(var i in rows){
                     appIds.push(rows[i].id);
                 }
                 userService.addApp($scope.userId,appIds.join()).then(function(response){
                     loadApps();
                     $scope.$broadcast("refreshData",{});
                 });
             }
         }];
     }

    initTableData();

    $scope.cancel = function () {
    	$modalInstance.close();
    };
    
    $scope.addSelected = function(rows){
        if(angular.isUndefined(rows) || rows.length == 0){
        	dbUtils.info("请选择一条数据");
            return;
        }
        var appIds = [];
        for(var i in rows){
            appIds.push(rows[i].id);
        }
        userService.addApp($scope.userId,appIds.join()).then(function(response){
            loadApps();
            $scope.$broadcast("refreshData",{});
        });
    }
    
    $scope.deleteApp = function(id){
        userService.deleteApp($scope.userId,id).then(function(response){
            dbUtils.success("删除成功","提示");
            loadApps();
            $scope.$broadcast("refreshData",{});
        })
    }
}]);

let UserRoleController = app.controller('UserRoleController', ['$scope','$rootScope','$modalInstance','$compile','dbUtils','source','UserService','RoleService',
                                      function($scope,$rootScope,$modalInstance,$compile,dbUtils,source,userService,roleService) {
    roleService.list().then(function (response){
        $scope.roles = response.data;
        roleService.listByUser(source.id).then(function(response){
        	for(var i in response.data){
        		var r = response.data[i];
        		for(var j in $scope.roles){
        			if(r.id == $scope.roles[j].id){
        				$scope.roles[j].checked = true;
        			}
        		}
            }
        });
    })
    
    $scope.checked = [];
    $scope.selectOne = function () {
        angular.forEach($scope.roles , function (i) {
            var index = $scope.checked.indexOf(i.id);
            if(i.checked && index == -1) {
                $scope.checked.push(i.id);
            } else if (!i.checked && index !== -1){
                $scope.checked.splice(index, 1);
            };
        })
//        console.log($scope.checked);
    }


    $scope.cancel = function () {
        $modalInstance.close(false);
    };

    $scope.submitApply = function(){
        if($scope.form.$invalid){
            return;
        }
        if($scope.checked.length == 0){
        	dbUtils.info("角色不能为空","提示");
        	return;
        }
        roleService.setByUser(source.id,$scope.checked).then(function(response){//$scope.roleId
            if (response.code == SUCCESS_CODE) {
                dbUtils.success("配置成功","提示");
                $modalInstance.close(true);
            } else {
                dbUtils.error(response.message,"提示");
            }
        })
    }
}]);


let UserAddController = app.controller('UserAddController', ['$scope','$rootScope','$modalInstance','$compile','source','dbUtils','UserService',
                                     function($scope,$rootScope,$modalInstance,$compile,source,dbUtils,userService) {
	if(source.action == "add"){
		$scope.addUser = true;
		$scope.user = {};
		$scope.ztreeData = source.ztreeData;
		$scope.idToLabel = source.idToLabel;
    }else if(source.action == "edit"){
    	$scope.addUser = false;
    	$scope.user = source.data;
    	$scope.user.userId = $scope.user.id;
    	$scope.ztreeData = source.ztreeData;
		$scope.idToLabel = source.idToLabel;
		$scope.orgName = $scope.idToLabel[$scope.user.orgNo];
    }else if(source.action == "modifypwd"){
    	$scope.addUser = false;
    	$scope.user = source.data;
    	$scope.user.userId = $scope.user.id;
    }
	
	$scope.orgNoClickHandle = function(){
		dbUtils.openModal('tpl/system/user_org_select.html','OrgNoEditController',"sm","",{"orgs":angular.copy($scope.ztreeData),"selectId":$scope.user.orgNo},function (result) {
    		if(result){
    			$scope.orgName = result.orgName;
    			$scope.user.orgNo = result.orgNo;
        	}
    	},true);
	}
	
	function checkOrg(){
		let isValid = true;//为了演示，先放开
//		let isValid = $scope.user.orgNo ? true : false;
		if(!isValid)
			dbUtils.warning("请选择组织机构");
		return isValid;
			
	}

    $scope.submitApply = function(){
        if($scope.form.$invalid){
            return;
        }
        if(source.action == "add"){
        	if(!checkOrg())
        		return;
    		userService.add($scope.user).then(function(response){
                if (response.code == SUCCESS_CODE) {
                    dbUtils.success("添加成功","提示");
                    $modalInstance.close(true);
                } else {
                    dbUtils.error(response.message,"提示");
                }
            })
        }else if(source.action == "edit"){
        	if(!checkOrg())
        		return;
        	userService.edit($scope.user).then(function(response){
                if (response.code == SUCCESS_CODE) {
                    dbUtils.success("添加成功","提示");
                    $modalInstance.close(true);
                } else {
                    dbUtils.error(response.message,"提示");
                }
            })
        }else if(source.action == "modifypwd"){
    		if($scope.isAdmin == 0 && (angular.isUndefined($scope.oldpwd) || $scope.oldpwd)){
    			dbUtils.error("请填写旧密码","提示");
        		return;
    		}
    		userService.modifypwd($scope.user.userId,$scope.oldpwd,$scope.newpwd).then(function(response){
                if (response.code == SUCCESS_CODE) {
                    dbUtils.success("修改成功","提示");
                    $modalInstance.close(true);
                } else {
                    dbUtils.error(response.message,"提示");
                }
            })
        }
    }
    
    $scope.cancel = function () {
        $modalInstance.close(false);
    };
}]);

//组织机构编码新增或修改
let OrgNoEditController = app.controller('OrgNoEditController',
		['$scope','$rootScope','$modalInstance','$compile','$modal','dbUtils','source',
		 function($scope,$rootScope,$modalInstance,$compile,$modal,dbUtils,source) {	
	$scope.orgs = source.orgs;
	$scope.selectId;
	let isExpand = false;
	// 控制显示“新建”还是“编辑”
    if(!source.selectId){
        $scope.isEdit = false;
    }else{
        $scope.isEdit = true;
        $scope.selectId = source.selectId;
    }
    
    var zTreeObj, curMenu = null;
    // zTree 的参数配置，深入使用请参考 API 文档（setting 配置详解）
    var setting = {
		view: {
			selectedMulti: false,
			showLine: false
		},
		callback: {
			onClick: zTreeOnClick
		}
    };
    // zTree 的数据属性，深入使用请参考 API 文档（zTreeNode 节点数据详解）
    var zNodes = $scope.orgs;
    
    $scope.$watch('$viewContentLoaded', function() {
    	zTreeObj = $.fn.zTree.init($("#ztree-user-org"), setting, zNodes);
    	fuzzySearch('ztree-user-org','#key-user-org',null,true);
    	if($scope.selectId){
    		curMenu = zTreeObj.getNodeByParam("id", $scope.selectId, null);
    		zTreeObj.selectNode(curMenu);
    	}
    })
    
    function zTreeOnClick(event, treeId, treeNode) {
	    $scope.org = treeNode;
	};
	
	$scope.toggleAllTree = function(){
		isExpand = !isExpand;
		zTreeObj.expandAll(isExpand);
	}
    
    $scope.submitApply = function () {
    	if(!$scope.org){
    		dbUtils.warning("请选择一条数据","提示");
    		return;
    	}
        $modalInstance.close($scope.org);
    };
    
    $scope.cancel = function () {
        $modalInstance.close(false);
    };
}]);
