'use strict';
// nginx控制器
let NginxController = app.controller('NginxController', ['$scope','$rootScope','$modal','EchoLog','$state','dbUtils','NginxService',
			function($scope,$rootScope,$modal,echoLog,$state,dbUtils,nginxService) {
	$scope.isShowMain = true;
	$scope.isShowConfig = false;

	$scope.$on('returnMain',function(event,data){
		setAllHidden();
		$scope.isShowMain = true;
	})

	function setAllHidden(){
		$scope.isShowMain = false;
		$scope.isShowConfig = false;
	}

	$scope.t_id = dbUtils.guid();
	var table = {
    	url:"/nginx/list",
    	showUpload: false,
		showText:true,waringText:"重新加载：加载配置文件nginx.conf；操作中的配置文件提供查看、下载和上传文件功能",
        headers: [
            {name:"名称",field:"name",style:{"width":"10%"}},
            {name:"状态",field:"status",style:{"width":"6%"},compile:true,formatter:function(value,row){
				row.status = 0;
            	$scope.temp = {"status":{}};
            	$scope.temp["status"][row.id] = 0;
            	var template = "<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{1:\"text-success\",0:\"text-danger\"}[temp.status["+row.id+"]]'></i>{{{1:\"运行中\",0:\"已停止\"}[temp.status["+row.id+"]]}}</span>";
            	nginxService.queryNginxStatus(row.clientId,row.installPath).then(function(response){
            		if(response.code == SUCCESS_CODE){
        				if(response.data == null || response.data == ""){
        					$scope.temp["status"][row.id] = 0;
        				}else{
							row.status = 1;
        					$scope.temp["status"][row.id] = 1;
        				}
        			}else{
            			dbUtils.error(response.message,"提示");
            		}
        		});
            	return template;
            }},
            {name:"IP",field:"ip",style:{"width":"12%"}},
            {name:"端口",field:"port",style:{"width":"6%"}},
            {name:"主机名",field:"hostName",style:{"width":"12%"}},
            {name:"安装路径",field:"installPath",style:{"width":"15%"}},
	        {name:"描述",field:"description",style:{"width":"12%"}},
            {name:"创建时间",field:"createTime",style:{"width":"10%"}}
        ],
        operationEvents: [{
            class:"btn-info",
            icon:"glyphicon glyphicon-plus",
            name:"配置Nginx",
            click:function(){
            	dbUtils.openModal('tpl/nginx/nginx_edit_modal.html','NginxEditController',"lg","",{},function (data) {
            		if(data == false){
                		return;
                	}
            		nginxService.saveNginx(JSON.stringify(data)).then(function(response){
                		if(response.code == SUCCESS_CODE){
                			dbUtils.success("新建成功","提示");
                			$scope.table.operations.reloadData();
                		}else{
                			dbUtils.error(response.message,"提示");
                		}
                	})
            	});
            }
        },{
            class:"btn-info",
            icon:"fa fa-play",
            name:"启动",
            click:function(rows){
            	if(rows.length == 0){
            		dbUtils.warning("请选择一条数据","提示");
            		return;
            	}
            	dbUtils.confirm("确认是否启动",function(){
            		nginxService.nginxOperationHandle1($scope,rows,"start");
            	},function(){},true)
            }
        },{
            class:"btn-info",
            icon:"fa fa-cog",
            name:"重新加载",
            click:function(rows){
            	if(rows.length == 0){
            		dbUtils.warning("请选择一条数据","提示");
            		return;
            	}
            	dbUtils.confirm("确认是否重新加载",function(){
            		nginxService.nginxOperationHandle1($scope,rows,"reload");
            	},function(){},true)
            }
        },{
            class:"btn-danger",
            icon:"fa fa-stop",
            name:"停止",
            click:function(rows){
            	if(rows.length == 0){
            		dbUtils.warning("请选择一条数据","提示");
            		return;
            	}
            	dbUtils.confirm("确认是否停止",function(){
            		nginxService.nginxOperationHandle1($scope,rows,"stop");
            	},function(){},true)
            }
        }],
        rowEvents:[{
        	class:"btn-info",
            icon:"fa fa-upload",
            title:"上传或查看配置文件",
            name:"配置文件",
            click: function(row){
            	nginxService.getClientById(row.clientId).then(function(response){
            		if(response.code == SUCCESS_CODE){
//            			$state.go('app.nginxConfig',{nginxConfigData:row,from:$state.current.name,client:response.data});
            			setAllHidden();
            			$scope.isShowConfig = true;
            			$scope.$broadcast("sendDataToConfig",{nginxConfigData:row,from:$state.current.name,client:response.data})
            		}else{
            			dbUtils.error(response.message,"提示");
            		}
            	})
            }
        },{
        	class:"btn-info",
            icon:"fa fa-pencil",
            title:"编辑",
            click: function(row){
            	var instance = $modal.open({
                    animation: true,
                    templateUrl: 'tpl/nginx/nginx_edit_modal.html',
                    controller: 'NginxEditController',
                    size: "lg",
                    backdrop: "static",
                    resolve: {
                        source: function () {
                            return {data:angular.copy(row)}
                        }
                    }
                });
                instance.result.then(function (data) {
                	if(data == false){
                		return;
                	}
                	nginxService.saveNginx(JSON.stringify(data)).then(function(response){
                		if(response.code == SUCCESS_CODE){
                			dbUtils.info("修改成功","提示");
                			$scope.table.operations.reloadData();
                		}else{
                			dbUtils.error(response.message,"提示");
                		}
                	})
                });
            }
        },{
        	class:"btn-danger",
            icon:"fa fa-trash",
            title:"删除",
            isShow:function(row){
                return row.username != "admin";
            },
            isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	nginxService.deleteNginx(row.id).then(function(response){
            		if(response.code == SUCCESS_CODE){
            			dbUtils.info("删除成功","提示");
            			$(event.target).parents(".popover").prev().click();
            			$scope.table.operations.reloadData();
            		}else{
            			dbUtils.error(response.message,"提示");
            		}
            	})
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        settings:{
            cols:2,
            filterId: "nginxListFilter",
            showCheckBox:true,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-34-56-20)+"px",
            },
        },
        afterReload:function(){
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight11+"px";
        	},200)
        }
    }
    $scope.table = table;
}]);

'use strict';
/* nginx新增/修改Controllers */
let NginxEditController = app.controller('NginxEditController',['dbUtils','$scope','$modalInstance', '$state','source','NginxService',
		    function (dbUtils,$scope,$modalInstance, $state,source,nginxService) {

	$scope.isEdit;
	$scope.buttonClickFlag = false;
	$scope.client;

	if(angular.isUndefined(source.data)){
		$scope.isEdit = false;
		$scope.data = {port:"",monitorPort:0,isMonitor:0};
	}else{
		$scope.isEdit = true;
		$scope.data = source.data;
	}

	nginxService.getClient().then(function(response){
		if(response.code == SUCCESS_CODE){
			$scope.clients = response.data;
			if($scope.isEdit == true){
				for(var i in $scope.clients){
					if($scope.clients[i].name == $scope.data.hostName){
						$scope.client = $scope.clients[i];
						break;
					}
				}
			}
		}else{
			dbUtils.error(response.message,"提示");
		}
	})

//	把controller中对端口检验的配置转移到ui-dbUtils.js中，方便管理和使用,提供ipPortList和oldData的初始化
	dbUtils.startPortCheck($scope,$scope.isEdit,$scope.data);

    $scope.checkPortDuplicate = function(ip,data,propNameList){
    	if($scope.isEdit == false && (!$scope.data.monitorPort || $scope.data.monitorPort == 0)){
			$scope.data.monitorPort = parseInt($scope.data.port) + 100;
		}
		return dbUtils.checkPortDuplicate(ip,data,propNameList,$scope.ipPortList,$scope.oldData,$scope.isEdit,"ip");
    }

	$scope.$watch('{a:data,b:client}', function(newValue, oldValue) {
		if (newValue != oldValue){
			if(newValue.a.isMonitor == 1 && oldValue.a.isMonitor == 0 && $scope.data.monitorPort == 0){
				$scope.data.monitorPort = "";
				return;
			}
			$scope.buttonClickFlag = true;
        }
    },true);

	$scope.submitApply = function () {
		$scope.data.ip = $scope.client.hostAddress;
		$scope.data.hostName = $scope.client.name;
		$scope.data.clientId = $scope.client.id;

		var idDuplicate = $scope.checkPortDuplicate($scope.client.hostAddress,$scope.data,['port','monitorPort']);
        if(idDuplicate == true)
        	return;

		if($scope.buttonClickFlag == false)
			dbUtils.info("信息没有改动，不能保存","提示");
		else{
			if($scope.data.isMonitor == 0)
				$scope.data.monitorPort = 0;

			$modalInstance.close($scope.data);
		}
	};

	$scope.cancel = function () {
	    $modalInstance.close(false);
	};

}]);

// nginx修改
let NginxConfigController = app.controller('NginxConfigController',['$scope','$rootScope','$compile','$modal','EchoLog','$http','$state','$stateParams','dbUtils','NginxService','$timeout',
		 function($scope,$rootScope,$compile,$modal,echoLog,$http,$state,$stateParams,dbUtils,nginxService,$timeout) {

	$scope.$on('sendDataToConfig',function(event,data){
		$scope.data = data.nginxConfigData;
		$scope.client = data.client;

		//路径导航的单个路径集合
		$scope.pathObjs = [
			{name:$scope.data.installPath,path:$scope.data.installPath},//+"/conf"
		];
		//保存当前路径的信息
		$scope.pathNav = {};
		$scope.pathNav.index = 0;
		$scope.pathNav.reloadUrl = "/nginx/getFiles?path=";
		$scope.pathNav.reloadParams = {data:$scope.client};

		$scope.t_id = dbUtils.guid();
		$scope.showFooTable = true;
		$scope.table = table;
		$scope.table.url = $scope.pathNav.reloadUrl + $scope.pathObjs[0].path;
		$scope.table.params = {data:$scope.client};
	})

	var table = {
		url:"/nginx/getFiles?path=",//+$scope.pathObjs[0].path
		hideQuery:true,
        showUpload: true,
        method:"postBody",
		showText:true,waringText:"点击下方目录地址栏可跳转目录；上传文件会覆盖文件",
//	        pageSql:true,
//        pageSize:20,
        startTime:'',
        endTime:'',
        headers: [
            {name:"文件名",field:"fileName",style:{"width":"30%","cursor":"pointer"},compile:true,formatter:function(value,row){
            	if(row.dir == true){
            		return "<span class='cusor-pointer'><i class='fa fa-folder text-warning' style='margin-right: 5px;'></i>"+value+"</span> ";
            	}else{
            		return "<span class='cusor-pointer'><i class='fa fa-file text-muted' style='margin-right: 5px;'></i>"+value+"</span> ";
            	}
            },clickEvent:function(value,row){
            	if(row.dir == true)
            		$scope.table.operations.pathNavNextHandle(row);
            	else
            		getFileContent(row);
            }},
            {name:"文件类型",field:"dir",style:{"width":"10%"},compile:true,formatter:function(value,row){
            	if(value == "true"){
            		return "<span >文件夹</span> ";
            	}else{
            		return "<span >文件</span> ";
            	}
            }},
            {name:"大小",field:"size",style:{"width":"10%"}},
            {name:"修改时间",field:"mtime",style:{"width":"15%"}},
            {name:"属性",field:"permission",style:{"width":"10%"}},
            {name:"所有者",field:"user",style:{"width":"10%"}},
        ],
        operationEvents: [{
            class:"btn-info",
            icon:"fa fa-play",
            name:"启动",
            click:function(){
            	dbUtils.confirm("确认是否启动",function(){
            		nginxService.nginxOperationHandle2($scope,"start");
            	},function(){},true)
            }
        },{
            class:"btn-info",
            icon:"fa fa-cog",
            name:"重新加载",
            click:function(){
            	dbUtils.confirm("确认是否重新加载",function(){
            		nginxService.nginxOperationHandle2($scope,"reload");
            	},function(){},true)
            }
        },{
            class:"btn-danger",
            icon:"fa fa-stop",
            name:"停止",
            click:function(){
            	dbUtils.confirm("确认是否停止",function(){
            		nginxService.nginxOperationHandle2($scope,"stop");
            	},function(){},true)
            }
        },{
            class:"btn-success",
            icon:"fa fa-play",
            name:"路径导航栏",
            type:'pathNav',
            click:function(event,r_index,obj){
            	$scope.table.operations.pathNavClickHandle(event,r_index,obj);
        	}
        }],
        rowEvents:[{
        	class:"btn btn-info",
        	icon:"fa fa-folder-open",
        	title:"下一级",
        	isShow:function(row){
                return row.dir == true;
            },
        	click: function(row){
        		//需要row.fileName，当文件名为其他字段时，为fileName赋值
        		$scope.table.operations.pathNavNextHandle(row);
        	}
        },{
        	class:"btn btn-info",
        	icon:"fa fa-file-text",
        	title:"查看",
        	isShow:function(row){
                return row.dir == false;
            },
        	click: function(row){
        		getFileContent(row);
        	}
        },{
        	class:"btn btn-info",
        	icon:"fa fa-download",
        	title:"下载",
        	isShow:function(row){
                return row.dir == false;
            },
        	click: function(row){
            	var url = "/nginx/downloadFile?file="+$scope.pathObjs[$scope.pathNav.index].path + "/" + row.fileName+"&client="+JSON.stringify($scope.client);
		        var form = $("<form></form>").attr("action", url).attr("method", "post");
		        form.append($("<input></input>").attr("type", "hidden").attr("path", $scope.pathObjs[$scope.pathNav.index].path + "/" + row.fileName).attr("client", JSON.stringify($scope.client)));
		        form.appendTo('body').submit().remove();
		        $scope.loading = false;
        	}
        }],
        afterReload:function(){
        	//查询条件--45 按钮--38 2个padding--30
        	$scope.table.operations.pathNavStatusHandle();
			$timeout(function () {
				$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight41+"px";
			})
        	/*setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight4+"px";
				$scope.$apply();
				console.log($scope.table.settings.tbodyStyle.height)
        	},200)*/
        },
        settings:{
            cols:3,
            filterId: "nginxConfigFilter",
            showCheckBox:true,
            pageSize:20,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-45-38-60-30-40)+"px",
            }
        }
    }

    //上传文件处理程序
	$scope.uploadHandle = function(file){
    	var formData = new FormData();
        formData.append("multipartFile",file);
        formData.append("client",JSON.stringify($scope.client));
        var deployType = 2;
        $scope.loading = true;
        $http({
            method:'post',
            url:'/nginx/uploadFile?path='+$scope.pathObjs[$scope.pathNav.index].path,
            data:formData,
            headers:{'Content-Type':undefined} ,
            transformRequest:angular.identity//浏览器会帮我们把Content-Type 设置为 multipart/form-data.
        }).success(function(response){
        	 $scope.loading = false;
        	 if(response.code == SUCCESS_CODE && response.data == true){
        		 dbUtils.success("上传成功","上传");
             }else{
                 dbUtils.error("上传失败","提示");
             }
        	 $scope.table.operations.reloadData();
        }).error(function(data){
        	$scope.loading = false;
        	if(data == null){
        		dbUtils.error("上传失败","上传");
        	}else{
        		dbUtils.error(data.message,"提示");
        	}
        });
    }

	function getFileContent(row){
		var instance = $modal.open({
            animation: true,
            templateUrl: 'tpl/nginx/nginx_config_edit_modal.html',
            controller: 'NginxConfigEditModalController',
            size: "lg",
            windowClass: "modal-large",
            backdrop: "true",
            resolve: {
                source: function () {
                    return {client:$scope.client,filePath:$scope.pathObjs[$scope.pathNav.index].path + "/" + row.fileName}
                }
            }
        });
        instance.result.then(function (data) {
//        	dbUtils.info("执行保存配置文件方法","提示");
        });
	}

    $scope.returnMain = function (){
//		$state.go("app.nginx",{})
    	$scope.showFooTable = false;
    	$scope.$emit("returnMain",{});
    }

}]);

'use strict';
/* nginx配置文件修改Controllers */
let NginxConfigEditModalController = app.controller('NginxConfigEditModalController',['dbUtils','$scope','$modalInstance', '$state','source','NginxService','$rootScope',
		    function (dbUtils,$scope,$modalInstance, $state,source,nginxService,$rootScope) {

	$scope.client = source.client;
	$scope.filePath = source.filePath;
	var editor;
	$scope.data = "";
	$scope.buttonClickFlag = false;

	$scope.$watch('$viewContentLoaded', function() {
		editor = ace.edit("pre_code_editor_nginxconfig"); //初始化对象
	    editor.setTheme("ace/theme/eclipse");//设置风格和语言 tomorrow_night_eighties chrome github
	    editor.session.setMode("ace/mode/yaml");
	    editor.$blockScrolling = Infinity;
	    editor.setOptions({
	   	    enableBasicAutocompletion: true,
	   	    enableSnippets: true,
	   	    enableLiveAutocompletion: true
	    });
	    let modalHeight = parseInt($(".modal-dialog").css("height"))*$rootScope._screenProp.HeightValue/100;
	    $("#fa_container").css("height",modalHeight - 120 + "px");
	    $("#pre_code_editor_nginxconfig").css("height",modalHeight - 120 + "px");
	});

	nginxService.getFileinfo($scope.client,$scope.filePath).then(function(response){
		if(response.code == SUCCESS_CODE){
			$scope.data = response.data;
			editor.setValue($scope.data);
		}else{
			dbUtils.error(response.message,"提示");
		}
	})

	$scope.submitApply = function () {
		$scope.data = editor.getValue();
		if($scope.data == ""){
			dbUtils.info("信息不能为空","提示");
			return;
		}
		$modalInstance.close($scope.data);
	};

	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};

}]);