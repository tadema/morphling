'use strict';
/* 告警信息菜单Controllers */
let AlarmManagerController = app.controller('AlarmManagerController',
		['$rootScope', '$scope', '$state', '$cookieStore','AccountService','$modal','dbUtils','$http','EchoLog','$filter', 'AlarmService','$sce',
        function ($rootScope, $scope, $state,$cookieStore, accountService,$modal,dbUtils,$http,echoLog,$filter,alarmService,$sce) {
	
	$scope.customTimeFlag = false;
	$scope.t_id = dbUtils.guid();
	var table = {
        url:"/alert/listAlertInfo",
        showUpload: false,
        pageSql:true,
        hideQuery:true,
        startTime:'',
        endTime:'',
        headers: [
			{name:"告警等级",field:"severity",class:"text-center",style:{"width":"9%"},compile:true,formatter:function(value,row){
				var code = 1,title;
				if(value == 'yellow'){
					title = '一般';
					code = 1;
				}else if(value == 'orange'){
					title = '中等';
					code = 2;
				}else if(value == 'red'){
					title = '严重';
					code = 3;
				}
        		return "<i class='fa fa-circle' ng-class='{1:\"text-warning\",2:\"text-danger-lter\",3:\"text-danger\"}["+code+"]' title='"+title+"'></i>";
            }},
            {name:"状态",field:"alertsStatus",style:{"width":"6%"},compile:true,formatter:function(value,row){
            	var code = 0;
            	if(value == 'resolved'){
            		code = 1;
            	}
            	return "<span class='badge sm-br' ng-class='{1:\"bg-success\",0:\"bg-danger\"}["+code+"]'>{{{1:\"已解决\",0:\"未解决\"}['"+code+"']}}</span>"
            }},
            {name:"描述信息",field:"description",class:"text-center",style:{"width":"9%"},compile:true,formatter:function(value,row){
            	/*value = $sce.trustAsHtml(value);
            	return "<span class='label cusor-pointer bg-success' data-toggle='tooltip' tooltip-placement='auto right' uib-tooltip-html='"+value+"'>查看详情</span>";*/
            	return "<span class='badge sm-br bg-light cusor-pointer' data-toggle='tooltip' onmouseenter='$(this).tooltip(\"show\")' data-trigger='click hover' data-html='true' data-placement='auto right' title=\""+value+"\" >查看详情</span>";
            }},
            {name:"告警内容",field:"summary",style:{"width":"18%"}},
            {name:"资源名称",field:"instance",style:{"width":"15%"}},
            {name:"IP地址",field:"ip",style:{"width":"12%"}},
            {name:"资源类型",field:"job",style:{"width":"11%"}},
            {name:"发生时间",field:"startsAt",style:{"width":"10%"}},
            {name:"最新时间",field:"timestamp",style:{"width":"10%"}},
//            {name:"确认状态",field:"isFlag",compile:true,formatter:function(value,row){
//                return "<span class='badge' ng-class='{'firing':\"bg-dark\",'on':\"bg-success\"}["+value+"]'>{{{'firing':\"未确认\",'on':\"已确认\"}['"+value+"']}}</span>"
//            }},
        ],
        operationEvents: [/*{
            class:"btn-info",
            icon:"fa fa-play",
            name:"告警动作",
            type:'dropdown',
            child:[{
            	name:'1',
            },{
            	name:'2',
            }],
            click:function(){
            	dbUtils.info("后续添加该功能","提示");
            }
        },*/{
            class:"btn-success",
            icon:"fa fa-play",//glyphicon glyphicon-plus
            name:"导航栏",
            type:'nav',
            child:[{
            	name:'全部',icon:'',active:'active'
            },{
            	name:'一般',icon:'fa fa-circle  text-warning',
            },{
            	name:'中等',icon:'fa fa-circle  text-danger-lter',
            },{
            	name:'严重',icon:'fa fa-circle  text-danger',
            }],
            click:function(event,obj,child){
            	var childs = obj.child;
            	event = event.currentTarget;//绑定元素
            	$(event).closest('ul').find("li").removeClass('active');
//        		$("#nav-pill li").siblings().removeClass('active');  // 删除其他兄弟元素的样式
                $(event).parent().addClass('active');
                var searchText
                if(child.name == "一般")
                	searchText = "yellow";
                else if(child.name == "中等")
                	searchText = "orange";
                else if(child.name == "严重")
                	searchText = "red";
                else
                	searchText = "";
                $scope.searchContent(searchText);
            }
        },{
            class:"btn-light",
            icon:"fa fa-circle fa-lg text-warning",
            name:"请选择发生时间",
            type:'dropdown',
            child:[{
            	name:'请选择发生时间',value:'0',
            },{
            	name:'最近1小时',value:'1',
            },{
            	name:'最近4小时',value:'2',
            },{
            	name:'最近12小时',value:'3',
            },{
            	name:'最近24小时',value:'4',
            },{
            	name:'今天',value:'5',
            },{
            	name:'昨天',value:'6',
            },{
            	name:'自定义',value:'7',
            }],
            click:function(event,obj,child){
            	event = event.target;
            	obj.name = child.name;
            	$(event).closest('ul').find("li").removeClass('selected');
//            	$("#dropdown li").siblings().removeClass('selected');  // 删除其他兄弟元素的样式
                $(event).parent().addClass('selected');      
                
                $scope.customTimeFlag = false;
                var reduceTime = 0;
                var stime,etime;
                if(child.value == "0")
                	return;
                else if(child.value == "1"){
                	stime = $scope.getTime(1*60)[0];etime = $scope.getTime(1*60)[1]; 
                }else if(child.value == "2"){
                	stime = $scope.getTime(4*60)[0];etime = $scope.getTime(4*60)[1]; 
                }else if(child.value == "3"){
                	stime = $scope.getTime(12*60)[0];etime = $scope.getTime(12*60)[1]; 
                }else if(child.value == "4"){
                	stime = $scope.getTime(24*60)[0];etime = $scope.getTime(24*60)[1]; 
                }else if(child.value == "5"){
                	 var day1 = new Date();
                	 day1.setDate(day1.getDate());
                	 stime = day1.format("yyyy-MM-dd 00:00:00");
                	 etime = day1.format("yyyy-MM-dd 23:59:59");
                }else if(child.value == "6"){
                	var day1 = new Date();
               	 	day1.setDate(day1.getDate() - 1);
               	 	stime = day1.format("yyyy-MM-dd 00:00:00");
               	 	etime = day1.format("yyyy-MM-dd 23:59:59");
                }else if(child.value == "7")
                	$scope.customTimeFlag = true;
                
                if($scope.customTimeFlag == false){
//                    console.log(stime,etime);
                    $scope.table.url = "/alert/listAlertInfoByDateCondition";
                    $scope.table.params = [{name:"start",initValue:stime,type:"text"},{name:"end",initValue:etime,type:"text"}];
                    $scope.table.operations.reloadData();
                    $scope.table.url = "/alert/listAlertInfo";
                    $scope.table.params = {};
                }
            }
        }],
        /*rowEvents:[{
        	class:"btn btn-default btn-primary",
        	icon:"fa fa-pencil",
        	title:"编辑",
        	click: function(row){
        		var instance = $modal.open({
                    animation: true,
                    templateUrl: 'tpl/monitor/monitor_modify_modal.html',
                    controller: 'MonitorModifyController',
                    size: "lg",
                    backdrop: "static",
                    resolve: {
                        source: function () {
                            return {data:row};
                        }
                    }
                });
                instance.result.then(function (data) {
               	 	console.log(data);
                });
        	}
        }],*/
        afterReload:function(){
        	//查询条件--45 按钮--38 2个padding--30
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight2+"px";
        	},200)
        },
        settings:{
            cols:3,
            filterId: "alarmManagerFilter",
            showCheckBox:true,
            pageSize:20,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-34-56-20)+"px",
            }
        }
    }
	
    $scope.table = table;
	
	$scope.searchContent = function(searchText){
		$("#"+$scope.t_id+" #alarmManagerFilter").focus();
        $scope.table.filterText = searchText;
        var evt = $.Event('keyup', {keyCode: 13});
        $("#"+$scope.t_id+" #alarmManagerFilter").trigger(evt);
	}
	
	$scope.$on('timeMonitor',function(event,data){
		$scope.table.url = "/alert/listAlertInfoByDateCondition";
        $scope.table.params = [{name:"start",initValue:data[0],type:"text"},{name:"end",initValue:data[1],type:"text"}];
        $scope.table.operations.reloadData();
        $scope.table.url = "/alert/listAlertInfo";
        $scope.table.params = {};
	})
	
	$scope.getTime = function(reduceTime){
		var dataArr = [];
		var now = new Date;
    	now.setMinutes(now.getMinutes() - reduceTime);
    	dataArr.push(now.format("yyyy-MM-dd hh:mm:ss"));
    	dataArr.push(new Date().format("yyyy-MM-dd hh:mm:ss"));
    	return dataArr;
	}
	
	Date.prototype.format = function(fmt) {
	    var o = {
	        "M+": this.getMonth() + 1, //月份 
	        "d+": this.getDate(), //日 
			"h+": this.getHours(), //小时 
			"m+": this.getMinutes(), //分 
			"s+": this.getSeconds(), //秒 
			"q+": Math.floor((this.getMonth() + 3) / 3), //季度 
			"S": this.getMilliseconds() //毫秒 
		};
		if(/(y+)/.test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 -RegExp.$1.length));
		}
		for(var k in o) {
			if(new RegExp("(" + k + ")").test(fmt)) {
			    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
			}
		}
		return fmt;
	}
	
}]);

'use strict';
/* 告警规则Controllers */
let AlarmRuleController = app.controller('AlarmRuleController',
		['$rootScope', '$scope', '$state', '$cookieStore','AccountService','$modal','dbUtils','$http','EchoLog','$filter', 'AlarmService',
        function ($rootScope, $scope, $state,$cookieStore, accountService,$modal,dbUtils,$http,echoLog,$filter,alarmService) {
	$scope.t_id = dbUtils.guid();
	$scope.toolObject = {};//表格提示用的
	var table = {
        url:"/alertRules/list",
        pageSql:true,
        headers: [
			{name:"规则组名",field:"groupName",style:{"width":"20%"}},
            {name:"规则名称",field:"alertName",style:{"width":"25%"}},
            {name:"规则公式",field:"exprInfo",style:{"width":"15%"},compile:true,formatter:function(value,row){
            	$scope.toolObject[row.id] = value;
            	return "<span class='badge sm-br bg-light cusor-pointer' data-toggle='tooltip' tooltip-trigger='outsideClick' tooltip-placement='auto top' uib-tooltip-html='toolObject["+row.id+"]'>查看详情</span>";
            }},
            {name:"持续时长",field:"forTime",style:{"width":"10%"}},
            {name:"告警重要性等级",field:"labelsServerity",class:"text-center",style:{"width":"15%"},compile:true,formatter:function(value,row){
				var code = 1;
				if(value == 'yellow')
					code = 1;
				else if(value == 'orange')
					code = 2;
				else if(value == 'red')
					code = 3;
        		return "<i class='fa fa-circle' title='{{{1:\"一般\",2:\"中等\",3:\"严重\"}["+code+"]}}' ng-class='{1:\"text-warning\",2:\"text-danger-lter\",3:\"text-danger\"}["+code+"]'><span class='text-dark' style='margin-left:5px;'>{{{1:\"一般\",2:\"中等\",3:\"严重\"}["+code+"]}}</span></i>";
            }},
        ],
        operationEvents: [{
            class:"btn-info",
            icon:"glyphicon glyphicon-plus",
            name:"新增告警规则",
            click:function(){
            	var instance = $modal.open({
                    animation: true,
                    templateUrl: 'tpl/alarm/alarm_edit_modal.html',
                    controller: 'AlarmEditController',
                    size: "lg",
                    backdrop: "static",
                    resolve: {
                        source: function () {
                            return {};
                        }
                    }
                });
                instance.result.then(function (data) {
                	if(data != false){
                		var arr = [];
                		arr.push(data);
                		alarmService.saveOrUpdate(JSON.stringify(arr)).then(function(response){
                			if(response.code == SUCCESS_CODE){
                				dbUtils.info("新增成功","提示");
                				$scope.table.operations.reloadData();
                			}else{
                				dbUtils.error(response.message,"提示"); 
                			}
                		},function(){
                		})
                	}
                });
            }
        }],
        rowEvents:[{
        	class:"btn btn-info",
        	icon:"fa fa-pencil",
        	title:"编辑",
        	click: function(row){
        		var instance = $modal.open({
                    animation: true,
                    templateUrl: 'tpl/alarm/alarm_edit_modal.html',
                    controller: 'AlarmEditController',
                    size: "lg",
                    backdrop: "static",
                    resolve: {
                        source: function () {
                            return {data:angular.copy(row)};
                        }
                    }
                });
                instance.result.then(function (data) {
                	if(data != false){
                		var arr = [];
                		arr.push(data);
                		alarmService.saveOrUpdate(JSON.stringify(arr)).then(function(response){
                			if(response.code == SUCCESS_CODE){
                				dbUtils.info("修改成功","提示");
                				$scope.table.operations.reloadData();
                			}else{
                				dbUtils.error(response.message,"提示"); 
                			}
                		},function(){
                		})
                	}
                });
        	}
        },{
        	class:"btn-danger",
            icon:"fa fa-trash",
        	title:"删除",
        	isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	alarmService.delete(row.id,row.alertName).then(function(response){
            		if(response.code == SUCCESS_CODE){
            			dbUtils.info("删除成功","提示");
            			$(event.target).parents(".popover").prev().click();
            			$scope.table.operations.reloadData();
                    }else{
                        dbUtils.error(response.message,"提示"); 
                    }
            	},function(){
            	})
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        settings:{
            cols:3,
            filterId: "alarmRuleFilter",
//            pageSize:20,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-34-56-20)+"px",
            },
        },
        afterReload:function(){
        	//查询条件--45 按钮--38 2个padding--30
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight2+"px";
        	},200)
        }
    }
    $scope.table = table;
	
}]);

'use strict';
/* 告警规则新增/修改Controllers */
let AlarmEditController = app.controller('AlarmEditController',['dbUtils','$scope','$modalInstance', '$state','source',
		    function (dbUtils,$scope,$modalInstance, $state,source) {
	
	$scope.data = source.data;
	$scope.isEdit;
	$scope.forTime;
	
	if(angular.isUndefined($scope.data)){
		$scope.data = {};
		$scope.isEdit = false;
		$scope.timeUnit = "s";
		$scope.data.labelsServerity = "yellow";
	}else{
		$scope.forTime = $scope.data.forTime.substring(0,$scope.data.forTime.length-1);
		$scope.timeUnit = $scope.data.forTime.substring($scope.data.forTime.length-1,$scope.data.forTime.length);
		$scope.isEdit = true;
	}
	$scope.buttonClickFlag = false;
	$scope.$watch('{a:data,b:forTime,c:timeUnit}', function(newValue, oldValue) {
		if (newValue != oldValue){
			$scope.buttonClickFlag = true;
        }
    },true);
	
	$scope.submitApply = function () {
		if($scope.form.$invalid){
            return;
        }
		if($scope.buttonClickFlag == false)
			dbUtils.info("信息没有改动，不能保存","提示");
		else{
			$scope.data.forTime = $scope.forTime + $scope.timeUnit;
			$modalInstance.close($scope.data);
		}
	};
	
	$scope.cancel = function () {
	    $modalInstance.close(false);
	};
	
}]);

'use strict';
/* 告警配置Controllers */
let AlarmConfigController = app.controller('AlarmConfigController',
		['$rootScope', '$scope', '$state', '$cookieStore','AccountService','$modal','dbUtils','$http','EchoLog','$filter', 'AlarmService','AlarmConfigService',
        function ($rootScope, $scope, $state,$cookieStore, accountService,$modal,dbUtils,$http,echoLog,$filter,alarmService,alarmConfigService) {
	
	function setAllHidden(){
		$scope.isShowMain = false;
		$scope.isShowEdit = false;
	}
	
	setAllHidden();
	$scope.isShowMain = true;
	
	$scope.$on('returnMain',function(event,data){
		setAllHidden();
		$scope.isShowMain = true;
		if(data && data.reloadData)
			$scope.table.operations.reloadData();
	})
	
    $scope.t_id = dbUtils.guid();			
	var table = {
        url:"/alertManagerServer/list",
//        pageSql:true,
        headers: [
			{name:"名称",field:"name",},
            {name:"IP",field:"ip"},
            {name:"端口",field:"port",style:{"width":"10%"}},
            {name:"状态",field:"status",style:{"width":"10%"},compile:true,formatter:function(value,row){
            	var status = 0;
                if(value == "UP"){
                	status = 1;
                }else{
                	status = 0;
                }
                return "<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-danger\",1:\"text-success\"}["+status+"]'></i>"+value+"</span>"
            }},
            {name:"创建时间",field:"createTime"},
            {name:"备注",field:"remark"},
        ],
        operationEvents: [{
            class:"btn-info",
            icon:"glyphicon glyphicon-plus",
            name:"新增告警配置",
            click:function(){
            	if($scope.table._rows.length >= 1){
            		dbUtils.warning("告警服务最多只能有1条","提示");
            		return;
            	}
            	setAllHidden();
            	$scope.isShowEdit = true;
            	$scope.$broadcast("sendDataToEdit",{isEdit:false});
//            	$state.go('app.alarmConfigEdit.view01',{from:$state.current.name,isEdit:false});
            }
        }],
        rowEvents:[{
            class:"btn-info",
            icon:"fa fa-play",//glyphicon glyphicon-plus
            title:"启动",
            click:function(row){
            	var alarmServers = [];
            	var alarmServer = {
            			ip : row.ip,
            			port : row.port,
            			action : 'start',
            			name : row.name,
            			typeName : row.typeName
            	};
            	alarmServers.push(alarmServer);
            	alarmConfigService.startOrStopAlarmServer({info:JSON.stringify(alarmServers)}).then(function(response){
            		if(response.code == SUCCESS_CODE){
            			echoLog.show(response.data,function(){
            				$scope.table.operations.reloadData();
            			});
                    }else{
                        dbUtils.error(response.message,"提示"); 
                    }
            	});
            }
        },{
            class:"btn-danger",
            icon:"fa fa-stop",
            title:"停止",
            click:function(row){
            	var alarmServers = [];
            	var alarmServer = {
            			ip : row.ip,
            			port : row.port,
            			action : 'stop',
            			name : row.name,
            			typeName : row.typeName
            	};
            	alarmServers.push(alarmServer);
            	alarmConfigService.startOrStopAlarmServer({info:JSON.stringify(alarmServers)}).then(function(response){
            		if(response.code == SUCCESS_CODE){
            			echoLog.show(response.data,function(){
            				$scope.table.operations.reloadData();
            			});
                    }else{
                        dbUtils.error(response.message,"提示"); 
                    }
            	});
            }
        },{
        	class:"btn btn-info",
        	icon:"fa fa-pencil",
        	title:"编辑",
        	click: function(row){
        		setAllHidden();
            	$scope.isShowEdit = true;
            	$scope.$broadcast("sendDataToEdit",{isEdit:true,'cdata':row});
//        		$state.go('app.alarmConfigEdit.view01',{from:$state.current.name,isEdit:true,'cdata':row});
        	}
        },{
        	class:"btn-danger",
            icon:"fa fa-trash",
        	title:"删除",
        	isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	alarmConfigService.delete(row.id).then(function(response){
            		if(response.code == SUCCESS_CODE){
            			dbUtils.info("删除成功","提示");
            			$(event.target).parents(".popover").prev().click();
            			$scope.table.operations.reloadData();
                    }else{
                        dbUtils.error(response.message,"提示"); 
                    }
            	},function(){
            	})
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        settings:{
            cols:3,
            filterId: "alarmConfigFilter",
//            pageSize:20,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-34-56-20)+"px",
            },
        },
        afterReload:function(){
        	//查询条件--45 按钮--38 2个padding--30
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight1+"px";
        	},200)
        }
    }
    $scope.table = table;
}]);

/* 告警配置新增/修改Controllers */
let AlarmConfigEditController = app.controller('AlarmConfigEditController',['$rootScope', '$scope', '$state','$stateParams', '$cookieStore','$cacheFactory','$modal','dbUtils','$http','EchoLog','$filter', 'AlarmConfigService','WizardHandler',
    function ($rootScope, $scope, $state,$stateParams,$cookieStore, $cacheFactory,$modal,dbUtils,$http,echoLog,$filter,alarmConfigService,WizardHandler) {
	
	var editor = null;
	$scope.isShowPageThreeTable = false;
	$scope.t_id = dbUtils.guid();
	
	$scope.$on("sendDataToEdit",function(event,data){
		$scope.data = data.cdata;
		$scope.isEdit = data.isEdit;
		alarmConfigService.initViewAndDataHandle($scope);
		
//		把controller中对端口检验的配置转移到ui-dbUtils.js中，方便管理和使用,提供ipPortList和oldData的初始化
		dbUtils.startPortCheck($scope,$scope.isEdit,$scope.pageOneData);
	})
	
	$scope.finishedWizard = function(){
		alarmConfigService.saveOrUpdateConfig($scope);
	}
	
	$scope.cancelledWizard = function(){
		$scope.returnMain();
	}
	
	$scope.checkPortDuplicate = function(ip,data,propNameList){
		return dbUtils.checkPortDuplicate(ip,data,propNameList,$scope.ipPortList,$scope.oldData,$scope.isEdit,"ip");
    }
	
	//第一页	告警服务器配置	退出校验
	$scope.pageOneExitValidation = function(context){
		var flag = alarmConfigService.pageOneExitValidation(context,$scope);
		if(flag){
			//加载第二页
			editor = alarmConfigService.initEditor();
			if($scope.isEdit)
            	editor.setValue($scope.pageTwoData.data);			
		}
		return flag;
	}
	
	//第二页	告警配置文件	退出校验
	$scope.pageTwoExitValidation = function(context){
		var flag = alarmConfigService.pageTwoExitValidation(context,$scope,editor);
		if(flag){
			$scope.isShowPageThreeTable = true;
			alarmConfigService.initPageThreeTable($scope);			
		}
		return flag;
	}
	
	//从第二页返回上一页之前先保存当前内容
	$scope.savePageTwoContent = function(){
		$scope.pageTwoData.data = editor.getValue();
	}
	
	$scope.cancelMain = function () {
		$scope.returnMain();
	};
	
	$scope.returnMain = function (){
		$scope.isShowPageThreeTable = false;
		WizardHandler.wizard("alarmConfigEditWizard").reset();
		$scope.$emit("returnMain",{reloadData:true})
    }
	
}]);

'use strict';
/* 告警提醒新增/修改Controllers */
let RemindEditModalController = app.controller('RemindEditModalController',['dbUtils','$scope','$modalInstance', '$state','source',
		    function (dbUtils,$scope,$modalInstance, $state,source) {
	
	$scope.data = source.data;
	$scope.isEdit;
	
	if(angular.isUndefined($scope.data)){
		$scope.data = {};
		$scope.isEdit = false;
	}else{
		$scope.isEdit = true;
	}
	$scope.buttonClickFlag = false;
	$scope.$watch('data', function(newValue, oldValue) {
		if (newValue != oldValue){
			$scope.buttonClickFlag = true;
        }
    },true);
	
	$scope.submitApply = function () {
		if($scope.form.$invalid){
            return;
        }
		if($scope.buttonClickFlag == false)
			dbUtils.info("信息没有改动，不能保存","提示");
		else{
			$modalInstance.close($scope.data);
		}
	};
	
	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};
	
}]);