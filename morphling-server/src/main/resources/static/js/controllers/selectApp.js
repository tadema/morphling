'use strict';

app.controller('SelectAppController', ['$scope','$rootScope','$modal','$state','$stateParams','dbUtils','ApplicationService','$timeout',
                                       function($scope,$rootScope,$modal,$state,$stateParams,dbUtils,appService,$timeout) {

   	appService.userPreview().then(function(response){
		$scope.apps = response.data;
	   	$timeout(function() {
			$scope.styleValue = {"height":$("#apps").height()};
		    $scope.$emit("pageLoadSuccess",{})
	   	})
    })

    //方块的随机颜色的随机数
    $scope.random = function(){
        return parseInt(5*Math.random())
    }

    $scope.middlewareTypes = [{name:"全部",value:"",className:"active"}].concat($rootScope.consts.middlewareType);
   	$scope.middlewareTypes.push({name:"其他",value:"other"});

   	$scope.chooseMenu = function(event,menu){
	   	event = event.currentTarget;
	   	$(event).closest('ul').find("li").removeClass('active');
	   	$(event).addClass('active');

		if($state.current.name == 'app.selectAppDeploy')
			$scope.sort_deploy.middlewareType = menu.value;
		else if($state.current.name == 'app.selectAppVision')
			$scope.sort_vision.middlewareType = menu.value;
		else if($state.current.name == 'app.selectAppLogtable')
			$scope.sort_logtable.middlewareType = menu.value;
  	}
    
    function setAllHidden(){
		$scope.isShowMain = false;
		$scope.isShowVersion = false;
	}
	setAllHidden();
	$scope.isShowMain = true;
	
	$scope.$on('returnMain',function(event,data){
		setAllHidden();
		$scope.isShowMain = true;
	})

    $scope.selectApp = function(app){
    	if($stateParams.to == 'app.deploy' || $stateParams.to == 'app.customDeploy'){
    		if(app.deployType == 1){
    			$state.go('app.customDeploy',{"deployAppId":app.id,'app':app,"fromWhere":$state.current.name})
    		}else{
    			$state.go('app.deploy',{"deployAppId":app.id,'app':app,"fromWhere":$state.current.name})
    		}
    	}else if($stateParams.to == 'app.logtable'){
    		$state.go($stateParams.to,{"logtableAppId":app.id,'app':app,"fromWhere":$state.current.name})
    	}else{
//    		$state.go($stateParams.to,{"visionAppId":app.id})
    		setAllHidden();
    		$scope.isShowVersion = true;
    		$scope.$broadcast('sendDataToVersion',{"visionAppId":app.id, "app":app});
    	}
    }
    
    //之前想改成表格形式，后来取消了
    $scope.fromTitle = "";
    if($stateParams.from == 'app.deploy' || $stateParams.from == 'app.customDeploy')
    	$scope.fromTitle = "应用发布";
    else if($stateParams.from == 'app.vision')
    	$scope.fromTitle = "版本管理";
    else if($stateParams.from == 'app.logtable')
    	$scope.fromTitle = "日志管理";
    
    /*sort*/
	$scope.sort_deploy = {title : '' , middlewareType : ""};
    $scope.sort_vision = {title : '' , middlewareType : ""};
    $scope.sort_logtable = {title : '' , middlewareType : ""};
	/*sort end*/
    
}]);

