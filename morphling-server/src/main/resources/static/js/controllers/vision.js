'use strict';

app.controller('VisionController', 
		['$scope','$rootScope','$modal','$state','$stateParams','dbUtils','$compile','$http','EchoLog','$timeout','VisionService',
        function($scope,$rootScope,$modal,$state,$stateParams,dbUtils,$compile,$http,echoLog,$timeout,visionService) {

	$scope.$on('sendDataToVersion',function(event,data){
		$scope.appId = data.visionAppId;
		$scope.app = data.app;
		$scope.visionTable = [];
		$scope.havingVersion = [];
		queryVision(1);//查询第一页
	})
	
    $scope.t_id = dbUtils.guid();
 // ----------------------------版本查询 start---------------------------------------------
    
    function queryVision(currentPage){
    	visionService.queryVision($scope.appId).then(function(response){
        	$scope.pages = {};
        	$scope.tableAllData = [];
        	$scope.tableData = [];
        	$scope.page = {};
            if(response.code == SUCCESS_CODE){
            	$scope.tableAllData = dataHandle(response.data);
	        	$scope.tableData = pageTableData(1,$scope.tableAllData);
            }else{
                dbUtils.error(response.message,"提示"); 
            }
        })
    }
    
 // ----------------------------版本查询 end---------------------------------------------
    
    // 处理版本数据的选择框权限、存在不存在的处理
    function dataHandle(data){
    	$scope.havingVersion = [];
    	$scope.havingCheck = false;//控制全选框的显示与隐藏
    	$scope.allrow = false;
    	var returnData = [];
    	for (var i = 0; i < data.length; i++) {	
			var newdata = angular.extend({listId:i, checked: false}, data[i]);
        	newdata["deployServersShow"] = dbUtils.toString(newdata.deployServers,true); 
     		if(newdata.isDeploy == "0"){//存在
     			newdata.haveCheck = true;
     			$scope.havingCheck = true;//只要有一个版本存在，则全选框就显示
     			$scope.havingVersion.push(newdata);
     		}else{
     			newdata.haveCheck = false;
     		}
     		returnData.push(newdata);
     	}
    	statVersion($scope.havingVersion);
        return returnData;
    }

	//版本统计
	function statVersion(data){
		$scope.show = {totalResults: 0 , firstVision : "无" , lastVision : "无"};
		var length = data.length;
		if(length > 0){
			$scope.show.totalResults = data.length;
			$scope.show.firstVision = data[length-1].version;
			$scope.show.lastVision = data[0].version;
		}
	}

    $scope.downloadFile = function(vision){
    	var url = "/deploy/app/version/downloadFileVersion";
        var form = $("<form></form>").attr("action", url).attr("method", "get");
        form.append($("<input></input>").attr("type", "hidden").attr("name", "appName").attr("value", $scope.app.name));
        form.append($("<input></input>").attr("type", "hidden").attr("name", "version").attr("value", vision.version));
        form.append($("<input></input>").attr("type", "hidden").attr("name", "fileName").attr("value", vision.fileName));
        form.appendTo('body').submit().remove();
    }
    
 // ----------------------------清除版本 start---------------------------------------------
    $scope.cleanVision = function () {
    	var list = getDeleteVersions();
    	if(list.length > 0){
    		dbUtils.confirm("确定清除"+list.length+"个版本？",function(){
    			doCleanVision(list,true);
        	},function(){},true);
    	}else{
    		dbUtils.warning("请选择版本","提示");
    	}
	}
    
    function getDeleteVersions(){
    	var allData = $scope.tableAllData;
    	var excludeFieldArr = ["filePathClick","deployDirClick","fileUnitSizeShow","listId","haveCheck"];
    	var returnData = [];
    	for (var i = 0; i < allData.length; i++) {
    		var newdata = {};
     		var obj = allData[i];
    		if(obj.checked && obj.haveCheck){
	     		for (var key in obj) {
	     			if(excludeFieldArr.indexOf(key) == -1){
	         			newdata[key] = obj[key];
	         		}
	     		}
	     		returnData.push(newdata);
     		}
     	}
        return returnData;
	}
    
    function doCleanVision(data,isLogicDelete){
    	var copyData = angular.copy(data);
    	for(let i of copyData){
    		delete i.deployServersShow;delete i.checked;delete i.deployServers;
    	}
    	visionService.doCleanVision($scope.appId,copyData,isLogicDelete).then(function(response){
            if(response.code == SUCCESS_CODE){
            	dbUtils.success("清除成功","提示"); 
            	queryVision(1);
            }else{
                dbUtils.error(response.message,"提示"); 
            }
        })
	}
    
 // ----------------------------返回初始页面 start---------------------------------------------

    $scope.goBackSelectApp = function (){
//    	$state.go("app.selectAppVision",{from:$state.current.name,title:"选择应用(版本)"})
    	$scope.$emit("returnMain",{});
    }
    
    // -----------------------------返回初始页面  end------------------------------------------
   // ----------------------------清除版本 start---------------------------------------------  
  //每行点击事件
    $scope.checkedRow = function(listId,i){
    	var check = !$scope.tableAllData[listId].checked;
    	$scope.tableData[i].checked = check;
    	$scope.tableAllData[listId].checked = check;
    }
    
    $scope.allRowCheck = function(){
    	var check = !$scope.allrow;
    	$scope.allrow = check;
   	 	for (var i in $scope.tableAllData) {
   	 		if($scope.tableAllData[i].haveCheck){
   	 			$scope.tableAllData[i].checked = check;
   	 		}
		}
   	 	for (var i in $scope.tableData) {
	 		if($scope.tableData[i].haveCheck){
	 			$scope.tableData[i].checked = check;
	 		}
		}
   	 	var a = 1;
    }
	  
 // ----------------------------表格前台分页 start---------------------------------------------
    $scope.pageSelect = [5,10,15,20,50,100];
    $scope.pageSize = 10;
    //应用分页设定
    function pageTableData(currentPage,data){	    	
    	 $scope.pages.totalResults = data.length;
    	 $scope.pages.currentPage = currentPage;
    	 $scope.pages.onePageSize = $scope.pageSize;
    	 $scope.pages.totalPage =  Math.ceil($scope.pages.totalResults/$scope.pages.onePageSize);
         pageSet($scope.pages);
         var startOne = (currentPage - 1) * $scope.pageSize;
         var endOne =  startOne + $scope.pageSize;
         if($scope.pages.totalPage == 0){ endOne =  0;} 
         else if(currentPage == $scope.pages.totalPage){ endOne =  $scope.pages.totalResults;} 
         var table = [];
         for(var i=startOne ; i < endOne ; i++){
        	 table.push(data[i]);
         }
         return table;
    }
    
    //刷新应用列表
    $scope.reloadData = function(){
    	$scope.tableData = pageTableData(1,$scope.tableAllData);
    }
    
    //点击应用页数查询
    $scope.pageNumberClick = function(pageNumber){
    	var currentPage = $scope.page.pageNumber;
    	var prevPage = $scope.page.prevPageDisabled;
    	var nextPage = $scope.page.nextPageDisabled;
        if (pageNumber === "prev" && prevPage && prevPage != "") {return false;}
        if (pageNumber === "next" && nextPage && nextPage != "") { return false;}
        if (pageNumber == currentPage) {return false;}
        if (pageNumber === "...") {return false;}
        if (pageNumber === "prev") {
        	currentPage--;
        } else if (pageNumber === "next") {
        	currentPage++;
        } else {
        	currentPage = pageNumber;
        }
        $scope.tableData = pageTableData(currentPage,$scope.tableAllData);
    }

    //分页处理
    function pageSet(data){
    	//分页数据处理
    	 var pages = {};
         pages.totalRecords = data.totalResults;
         pages.pageNumber = data.currentPage;
         pages.pageSize = data.onePageSize;
         pages.totalPages = data.totalPage;
         var totalPage = pages.totalPages;
         //分页算法，页面只显示固定数量的分页按钮。
         var pageNumbers = [];
         var startPage = 1;
         var endPage = totalPage;
         var pageStep = 2;//以当前页为基准，前后各显示的页数量
         if (totalPage >= 6) {
             startPage = pages.pageNumber;
             if (startPage >= pageStep) {startPage -= pageStep;}
             if (startPage <= 1) {startPage = 1;}
             endPage = (totalPage - pages.pageNumber) >= pageStep ? pages.pageNumber + pageStep : totalPage;
             if (endPage > totalPage) {endPage = totalPage;}
             if (startPage != 1) {
                 pageNumbers.push({number: "1"});
                 if (startPage - 1 != 1) {
                     pageNumbers.push({number: "...", disabled: "disabled"});
                 }
             }
         }
         for (var i = startPage; i <= endPage; i++) {
             if (i == pages.pageNumber) {
                 pageNumbers.push({number: i, active: "active"});
             } else {
                 pageNumbers.push({number: i});
             }
         }
         if (endPage != totalPage) {
             if (endPage + 1 != totalPage) {
                 pageNumbers.push({number: "...", disabled: "disabled"});
             }
             pageNumbers.push({number: totalPage});
         }
         pages.pageNumbers = pageNumbers;
         if (pages.pageNumber == 1 || pages.totalPages == 0) {
             pages.prevPageDisabled = "disabled";
         }
         if (pages.pageNumber == totalPage || pages.totalPages == 0) {
             pages.nextPageDisabled = "disabled";
         }
         $scope.page = pages;
         $scope.pageSize = Number($scope.page.pageSize);
         setTimeout(function () {$("#"+$scope.t_id+" #page_select").val($scope.pageSize);}, 20);
    }
    // -----------------------------表格前台分页  end------------------------------------------
}]);