'use strict';

// tomcat控制器
app.controller('TomcatController', ['$scope','$rootScope','$modal','$state','dbUtils','TomcatService',function($scope,$rootScope,$modal,$state,dbUtils,tomcatService) {
	$scope.t_id = dbUtils.guid();
	let healthCheckRows = [],rowsObj = {};
	$scope.temp = {"status":{}};
	var table = {
    	url:"/middleware/tomcat/listAll",
    	showUpload: false,
        pageSql:true,
        params:[
            {name:"version",label:"Tomcat版本",labelCols:4,type:"text"}
        ],
        headers: [
            {name:"Tomcat版本",field:"version",style:{width:"15%"}},
            {name:"状态",field:"status",style:{"width":"6%"},compile:true,formatter:function(value,row){
                rowsObj[row.id] = row;
            	healthCheckRows.push({"id":row.id, "ip":row.ip, "port":row.port});
            	$scope.temp["status"][row.id] = 2;
            	return "<span ng-if='temp.status["+row.id+"] == 2'><i class='fa fa-spin fa-spinner '></i></span>" +
    					"<span ng-if='temp.status["+row.id+"] != 2'><i style='margin-right:5px;' class='fa fa-circle' ng-class='{1:\"text-success\",0:\"text-danger\"}[temp.status["+row.id+"]]'></i>{{{1:\"运行中\",0:\"已停止\"}[temp.status["+row.id+"]]}}</span>";
            }},
            {name:"主机名",field:"hostName",style:{width:"14%"}},
            {name:"端口号",field:"port",style:{width:"6%"}},
            {name:"安装路径",field:"installPath",style:{width:"15%"}},
	        {name:"创建用户",field:"createUsername",style:{width:"8%"}},
            {name:"创建时间",field:"createTime",style:{width:"12%"}},
	        {name:"描述",field:"description",style:{width:"14%"}},
        ],
        operationEvents: [{
            class:"btn-info",id:"addTomcat",icon:"glyphicon glyphicon-plus",name:"配置Tomcat",
            click:function(){
                var instance = $modal.open({
                    animation: true,
                    templateUrl: 'tpl/tomcat/tomcat_edit.html',
                    controller: 'TomcatEditController',
                    size: "lg",
                    backdrop: "static",
                    resolve: {
                        source: function () {
                            return {}
                        }
                    }
                });
                instance.result.then(function (right) {
                	if(right){
                        $scope.table.operations.reloadData()
                	}
                });
            }
        }],
        rowEvents:[/*{
            class:"btn-success",
            name:"中间件管理",
            click: function(row){
                //
                $state.go("system.instance", {appId: row.id})
            }
        },*/{
        	class:"btn-info",
            icon:"fa fa-pencil",
            title:"编辑",
            click: function(row){
                var instance = $modal.open({
                    animation: true,
                    templateUrl: 'tpl/tomcat/tomcat_edit.html',
                    controller: 'TomcatEditController',
                    size: "lg",
                    backdrop: "static",
                    resolve: {
                        source: function () {
                            return angular.copy(row);
                        }
                    }
                });
                instance.result.then(function (right) {
                	if(right)
                		$scope.table.operations.reloadData()
                });
            }
        },{
        	class:"btn-danger",
            icon:"fa fa-trash",
            title:"删除",
            isShow:function(row){
                return row.username != "admin";
            },
            isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	tomcatService.deleteTomcat(row.id).then(function(response){
            		if(response.code == SUCCESS_CODE){
                        dbUtils.success("记录删除成功","提示");
                        $(event.target).parents(".popover").prev().click();
                        $scope.table.operations.reloadData()
                    }else {
                        dbUtils.error(response.message,"提示");
                    }
              	})
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        settings:{
            cols:2,
            showCheckBox:false,
            filterId: "tomcatFilter",
            operationId: "tomcatOperation",
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-34-56-20)+"px",
            },
        },
        afterReload:function(){
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight2+"px";
        	},200)
            $scope.$emit("pageLoadSuccess",{"name":"tomcat"});
        	healthCheckHandle();
        }
    }
    $scope.table = table;
	
	function healthCheckHandle(){
		tomcatService.checkHealth(healthCheckRows).then(function(response){
    		if(response.code == SUCCESS_CODE){
    			let respData = response.data;
    			for(let item of healthCheckRows){
    			    rowsObj[item.id].status = respData[item.id] == true ? 1 : 0;
    				$scope.temp["status"][item.id] = respData[item.id] == true ? 1 : 0;
    			}
			}else{}
		});
	}
}]);



// tomcat修改
app.controller('TomcatEditController', 
		['$scope','$rootScope','$modalInstance','$compile','dbUtils','source','TomcatService',
		 function($scope,$rootScope,$modalInstance,$compile,dbUtils,source,tomcatService) {	
	//主机
	$scope.client = {};
	$scope.managedServer = new Object();
	// 控制显示“新建”还是“编辑”
    if(!source.id){
        $scope.editData = false;
        $scope.tomcat = {ip:null,isMonitor:0};
    }else{
        $scope.editData = true;
        $scope.tomcat = source;
    }

    tomcatService.getAgentPost().then(function(response){
        if (response.code == SUCCESS_CODE) {
        	$scope.host = response.data;
        	if($scope.editData == true){
        		for(let item of $scope.host){
        			if(item.id == $scope.tomcat.deployFileId){
        				$scope.client = item;
        				break;
        			}
        		}
        	}
        } else {
        	$scope.host = []; 
        }
  	})
  	
  	$scope.clientChangeHandle = function () {
    	$scope.tomcat.hostName = $scope.client.name;
 		$scope.tomcat.ip = $scope.client.hostAddress;
 		$scope.tomcat.deployFileId = $scope.client.id;
    }; 

    $scope.tomcatPortBlur = function (data,propName) {
    	let port = data[propName];
		if(!port || port == 0){
			dbUtils.info("请输入端口号","提示");
			return;
		}
		
		$scope.checkPortDuplicate($scope.tomcat.ip,data,[propName]);
    };
    
//	把controller中对端口检验的配置转移到ui-dbUtils.js中，方便管理和使用,提供ipPortList和oldData的初始化
	dbUtils.startPortCheck($scope,$scope.editData,$scope.tomcat);
    
    $scope.checkPortDuplicate = function(ip,data,propNameList){
    	return dbUtils.checkPortDuplicate(ip,data,propNameList,$scope.ipPortList,$scope.oldData,$scope.editData,"ip");
    }
    
    $scope.cancel = function () {
        $modalInstance.close(false);
    };    
       
    $scope.$watch('tomcat.isMonitor',function(newValue,oldValue){
    	if(newValue != oldValue){
    		if(oldValue == 0 && newValue == 1 && $scope.tomcat.prometPort == 0){
    			$scope.tomcat.prometPort = "";
    		}
    	}
    })
    
    $scope.submitApply = function(){
        if($scope.form.$invalid){
            return;
        }
        if(angular.isUndefined($scope.client) || $scope.client == null){
			dbUtils.error("请选择主机信息","提示");
            return;
        }
        if($scope.tomcat.isMonitor == 1){
			var port = $scope.tomcat['prometPort'];
			var value = Number(port);
			if(angular.isUndefined(port) || port == 0){
				dbUtils.error("请准确填写监控端口参数","提示");
				return;
			}
		}
        
        var tomcat = {};
    	for (var key in $scope.tomcat) {
    		if(key == "port"||key == "jmxPort"){
    			tomcat[key] = parseInt($scope.tomcat[key]);
    		}else if(key == "prometPort"){
    			if($scope.tomcat.isMonitor == 1){
    				tomcat[key] = parseInt($scope.tomcat[key]);
    			}else{
    				tomcat[key] = parseInt(0);
    			}
    		}else{
    			tomcat[key] = $scope.tomcat[key];
    		}
		}
    	
    	var idDuplicate = $scope.checkPortDuplicate(tomcat.ip,tomcat,['port','prometPort']);
        if(idDuplicate == true)
        	return;
        
    	tomcatService.doSaveTomcat(JSON.stringify(tomcat)).then(function(response){
          if (response.code == SUCCESS_CODE) {
              dbUtils.success("操作成功","提示");            
              $modalInstance.close(true);
          } else {
              dbUtils.error(response.message,"提示");
          }
    	})
       
    }   
}]);