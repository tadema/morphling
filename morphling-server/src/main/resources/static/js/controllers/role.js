'use strict';

// 
app.controller('RoleManagerController', ['$scope','$rootScope','$modal','$state','dbUtils','RoleManagerService','ApplicationService',
                                       function($scope,$rootScope,$modal,$state,dbUtils,roleManagerService,appService) {
    if($rootScope._userinfo.isAdmin == 1){
    	$scope.showRoleManager = 1;
    }else{
    	$scope.showRoleManager = 0;
    }
    appService.getEnvs().then(function(response){
        $scope.envs = response.data;
    });
    $scope.t_id = dbUtils.guid();
	var table = {
    	url:"/role/listByPage",
    	showUpload: false,
        pageSql:true,
        /*params:[
            {name:"role",label:"角色名称：",labelCols:4,type:"text"}
        ],*/
        headers: [
            {name:"角色名称",field:"roleName"},
            {name:"描述",field:"description"},
        ],
        operationEvents: [{
            class:"btn-info",
            icon:"glyphicon glyphicon-plus",
            name:"新增角色",
            click:function(){
                var instance = $modal.open({
                    animation: true,
                    templateUrl: 'tpl/role/role_edit.html',
                    controller: 'roleManagerEditController',
                    size: "lg",
                    backdrop: "static",
                    resolve: {
                        source: function () {
                            return {}
                        }
                    }
                });
                instance.result.then(function (right) {
                	if(right){
                        $scope.table.operations.reloadData()
                	}
                });
            }
        }],
        rowEvents:[{
            class:"btn-info",
            icon:"fa fa-sitemap",
            title:"分配菜单",
            name:"菜单",
            isShow:function(row){
                return row.roleName != "SUPERADMIN";
            },
            click: function(row){
            	var amenus = $rootScope._userinfo.menus;
                var instance = $modal.open({
                    animation: true,
                    templateUrl: 'tpl/role/role_page.html',
                    controller: 'rolePageEditController',
                    size: "sm",
                    backdrop: "true",
                    resolve: {
                        source: function () {
                            return angular.copy(row);
                        }
                    }
                });
                instance.result.then(function (right) {
                	if(right)
                		$scope.table.operations.reloadData()
                });
            }
        },{
            class:"btn-info",
            icon:"fa fa-list",
            title:"分配用户",
            name:"用户",
            isShow:function(row){
            	return row.roleName != "SUPERADMIN";
            },
            click: function(row){
    			var instance = $modal.open({
                    animation: true,
                    templateUrl: 'tpl/role/role_user.html',
                    controller: 'roleUserController',
                    size: "lg",
                    windowClass: "modal-large",
                    backdrop: "true",
                    resolve: {
                        source: function () {
                            return row;
                        }
                    }
                });
                instance.result.then(function () {

                });
            }
        },{
            class:"btn-info",
            icon:"fa fa-pencil",
            title:"编辑",
            click: function(row){
                var amenus = $rootScope._userinfo.menus;
//            	var b = $menus ;
                var instance = $modal.open({
                    animation: true,
                    templateUrl: 'tpl/role/role_edit.html',
                    controller: 'roleManagerEditController',
                    size: "lg",
                    backdrop: "static",
                    resolve: {
                        source: function () {
                            return angular.copy(row);
                        }
                    }
                });
                instance.result.then(function (right) {
                    if(right)
                        $scope.table.operations.reloadData()
                });
            }
        },{
        	class:"btn-danger",
            icon:"fa fa-trash",
            title:"删除",
            isShow:function(row){
                return row.roleName != "SUPERADMIN";
            },
            isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	roleManagerService.deleteRole(row.id).then(function(response){
            		if(response.code == SUCCESS_CODE){
                        dbUtils.success("删除成功","提示");
                        $(event.target).parents(".popover").prev().click();
                        $scope.table.operations.reloadData()
                    }else {
                        dbUtils.error(response.message,"提示");
                    }
              	})
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        settings:{
            cols:2,
            showCheckBox:false,
            filterId: "roleFilter",
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-34-56-20)+"px",
            },
        },
        afterReload:function(){
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight2+"px";
        	},200)
        }
    }
	if($scope.showRoleManager == 1){
	    $scope.table = table;
	}else{
		dbUtils.confirm("由于您不是管理员,无法进行角色管理",function(){
    		// 点击确认按钮
    	},function(){
    		// 点击取消按钮
    	});  
	}
}]);



// roleManager修改
app.controller('roleManagerEditController', 
		['$scope','$rootScope','$modalInstance','$compile','dbUtils','source','RoleManagerService','ApplicationService',
		 function($scope,$rootScope,$modalInstance,$compile,dbUtils,source,roleManagerService,appService) {	
			
	appService.getEnvs().then(function(response){
        $scope.envs = response.data;
    });
	// 控制显示“新建”还是“编辑”
    if(angular.isUndefined(source.id)){
        $scope.editData = false;
        $scope.newRole = {id:null};
    }else{
        $scope.editData = true;
        $scope.newRole = source;
    }
  
    $scope.cancel = function () {
        $modalInstance.close(false);
    };    
       
	$scope.submitApply = function(){
        if($scope.form.$invalid){
            return;
        }
        //环境判定
        var envs = [];
        angular.forEach(angular.element("input[name=envs]"),function(e,key){
            if(e.checked){
                envs.push(e.value)
            }
        });
        var newRoleData = {};	
        if($scope.newRole.id){newRoleData.id =  $scope.newRole.id;}
        newRoleData.roleName = $scope.newRole.roleName;
        newRoleData.description = $scope.newRole.description;
    	/*for (var key in $scope.newRole) {
    		newRoleData[key] = $scope.newRole[key];
		}*/
//    	var newRoleData = JSON.stringify($scope.newRole);
    	roleManagerService.doSaveRole(newRoleData,envs.join()).then(function(response){
          if (response.code == SUCCESS_CODE) {
              dbUtils.success("操作成功","提示");            
              $modalInstance.close(true);
          } else {
              dbUtils.error(response.message,"提示");
          }
    	})
       
    }   
}]);

//roleManager修改
app.controller('rolePageEditController', 
		['$scope','$rootScope','$modalInstance','$compile','dbUtils','source','RoleManagerService',
		 function($scope,$rootScope,$modalInstance,$compile,dbUtils,source,roleManagerService) {	
	
	$scope.rowSelect = source;
	$scope.page = [];
	roleManagerService.getMenu($scope.rowSelect.id).then(function(response){
        if (response.code == SUCCESS_CODE) {
        	getPage(response.data);
        } else {
            dbUtils.error(response.message,"提示");
        }
  	})
  	
  	function getPage(amenus){
	    for (var i = 0; i < amenus.length; i++) {
	    	_buildBranchTemplate(amenus[i],null,null);
	    }
	}
    
    function _buildBranchTemplate(node,father,fatherNum) {
    	var obj = {};
    	obj.id = Number(node.id);
        if (node.type == 1 && !node.children ) {
        	obj.show = true;
        	obj.haveSelect = true;
        	if(node.isSelected == 1){ obj.checked = true; }
        	else if(node.isSelected == 0){ obj.checked = false; }
        	obj.icon = node.icon;
        	obj.text = node.text;
        	if(node.translate){obj.translate = node.translate;}
        	obj.num = $scope.page.length;
        	if(father){obj.father = father;obj.fatherNum = fatherNum;$scope.page[fatherNum].children.push(obj.num);}
        	$scope.page.push(obj);
        } else if (node.type == 1) {
        	obj.show = true;
        	obj.haveSelect = false;
        	obj.checked = false;
        	obj.icon = node.icon;
        	obj.text = node.text;
        	if(node.translate){obj.translate = node.translate;}
        	obj.num = $scope.page.length;
        	obj.children = [];
        	$scope.page.push(obj);
            for (var i = 0; i < node.children.length; i++) {
            	_buildBranchTemplate(node.children[i],obj.id,obj.num);
            }
            $scope.page[obj.num].fatherChecked = checkFather($scope.page[obj.num]);
        } else if (node.type == 2) {
        	obj.show = true;
        	obj.haveSelect = false;
        	obj.checked = false;
        	obj.text = node.text;
        	if(node.translate){obj.translate = node.translate;}
        	obj.num = $scope.page.length;
        	$scope.page.push(obj);
        } else if (node.type == 3) {
        } else if (node.type == 4) {
        }else{ }
    };

    var b = $scope.page;
    
		
    $scope.pageSelect = function (i,n,row) {
    	if(n == 0){
    		$scope.page[i].checked = false;
    	}else if(n == 1){
    		$scope.page[i].checked = true;
    	}
    	if(row.father){$scope.page[row.fatherNum].fatherChecked = checkFather($scope.page[row.fatherNum]);}
    };
    
    function checkFather(father){
    	var children = father.children;
    	var fatherChecked = true;
	    for (var i = 0; i < children.length; i++) {
	    	var check = $scope.page[children[i]].checked;
	    	if(!check)fatherChecked = false;
	    }
	    return fatherChecked;
	}
    
    $scope.pageFather = function (i,n,row) {
    	if(n == 0){
    		$scope.page[i].fatherChecked = false;
    	}else if(n == 1){
    		$scope.page[i].fatherChecked = true;
    	}
    	for (var j = 0; j < row.children.length; j++) {
    		$scope.page[row.children[j]].checked = $scope.page[i].fatherChecked;
	    }
    };
    
    $scope.cancel = function () {
    	$modalInstance.close(false);
    };    
       
    $scope.submitApply = function(){
        if($scope.form.$invalid){
            return;
        }
        var pageList = [];
        var fatherList = {};
    	for (var key in $scope.page) {
    		if($scope.page[key].checked){
    			pageList.push($scope.page[key].id);
    			if($scope.page[key].father){
    				fatherList[$scope.page[key].father] = true;
    			}
    		}
		}
//    	var b = Object.keys(fatherList);  
    	for (var key in fatherList) {
    		pageList.push(Number(key));
		}
//    	var newRoleData = JSON.stringify($scope.page);
    	roleManagerService.postMenu($scope.rowSelect.id,pageList).then(function(response){
          if (response.code == SUCCESS_CODE) {
              dbUtils.success("操作成功","提示");            
              $modalInstance.close(true);
          } else {
              dbUtils.error(response.message,"提示");
          }
    	})
       
    }   
}]);


app.controller('roleUserController', ['$scope','$rootScope','$modalInstance','$compile','dbUtils','source','RoleService',
    function($scope,$rootScope,$modalInstance,$compile,dbUtils,source,roleService) {
		$scope.rowSelect = source;
		if(angular.isUndefined($scope.rowSelect.id)){
			dbUtils.error("数据错误");
			return;
		}
		
		function loadUsers(){
			roleService.getUser($scope.rowSelect.id).then(function(response){
				$scope.users = response.data;
			})
		}
		
		loadUsers();
		
		$scope.queryParams = {isPagination:true,postgroundPagination:true,pageNumber: 1,pageSize:10,prevPageDisabled: 'disabled',nextPageDisabled: 'disabled'};
	    $scope.initTable = function(){
            $scope.roleUserUrlParams = {url:"/role/"+$scope.rowSelect.id+"/user?exclude=true", urlBody:{}, method:"get"};
            $scope.roleUserTableParams = {bodyStyle:{"max-height": "500px"}};
	    	$scope.roleUserHeaders = [
	    		{name:"姓名",field:"name"},{name:"用户名",field:"username"},{name:"电话",field:"phone"},
	            {name:"邮箱",field:"email"},{name:"上次登录IP",field:"lastLoginIp"},{name:"上次登录时间",field:"lastLoginTime"},{name:"创建时间",field:"createTime"}
			];
	    	$scope.roleUserRowEvents = [{
	            class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"添加项目",name: "添加",
	            click: function(row){
					roleService.addUser($scope.rowSelect.id,row.id).then(function(response){
						loadUsers();
						$scope.$broadcast('reloadData');
					});
				}
	        }];
	        $scope.roleUserOperationEvents = [{
	            class:"btn-info",icon:"fa fa-css3",name:"添加",
	            click:function(rows){
					if(angular.isUndefined(rows) || rows.length == 0){
						dbUtils.info("请选择一条数据");
						return;
					}
					var userIds = [];
					for(var i in rows){
						userIds.push(rows[i].id);
					}
					roleService.addUser($scope.rowSelect.id,userIds.join()).then(function(response){
						loadUsers();
						$scope.$broadcast('reloadData');
					});
				}
	        }];
	    }

	    $scope.initTable();
		
		$scope.cancel = function () {
			$modalInstance.close();
		};
		$scope.deleteApp = function(id){
			roleService.deleteUser($scope.rowSelect.id,id).then(function(response){
				dbUtils.success("删除成功","提示");
				loadUsers();
				$scope.$broadcast('reloadData');
			})
		}
}]);