'use strict';

/* Controllers */
// signin controller
// zhao.jie-neu
app.controller('HomeController',
		['$timeout','$rootScope', '$scope','$window', '$filter', '$modal','$state', '$cookieStore','dbUtils','EchoLog','NewHomeService','ApplicationService',
        function ($timeout,$rootScope, $scope,$window, $filter, $modal, $state,$cookieStore,dbUtils,echoLog,newHomeService,appService) {
    $scope.showWarning = true;
    $scope.closeWarning= function(){
        $scope.showWarning = null;
    }
    $scope.loading = true;
    //加载应用服务信息表格
    initAppServiceInfoTable();

    //发送主机状态信息查询请求
    $rootScope.$broadcast("clientStatusQuery",{});
    //发送告警信息查询的请求
    let isSend = false;
    for(let item of $rootScope._userinfo.menus){
    	if(item.text == "告警管理"){
    		isSend = checkSendAlarmQuery(item);
    	}else if(item.id == 8){
    		isSend = checkSendAlarmQuery(item);
    	}
    }
    if(isSend == true)
    	$rootScope.$broadcast("alarmQuery",{});
    
    function checkSendAlarmQuery(item){
    	if(!item.children)
    		return false;
    	for(let menu of item.children){
    		if(menu.route == "app.alarmManagement"){
    			return true;
    		}
    	}
    	return false;
    }

    $scope.numdata = {};
    //卡片信息查询
    $scope.cardLoading = true;
    newHomeService.getHomeInfo().then(function(response){
    	if(response.code == SUCCESS_CODE){
    		if(response.data){
        		$scope.numdata = response.data;
        		$scope.cardLoading = false;
        	}else{
        		$scope.numdata = {};
        		$scope.cardLoading = false;
        	} 
        }else{
        	$scope.numdata = {};
        	$scope.cardLoading = false;
            dbUtils.error(response.message,"提示");  
        }
    	
    })

	//初始化应用服务信息表格
	function initAppServiceInfoTable(){
    	$scope.appServiceInfo = {
			tableParams: {loading:true,isStartFilter:true,bodyStyle:{"height":$rootScope._screenProp.contentHeight-276 +"px","max-height":$rootScope._screenProp.contentHeight-276 +"px"}},
			tooltipData: {healthStatus:{}},
			headers: [
				{name:"应用",field:"appName",style:{width:"14%"},hideSort:true},
				{name:"部署类型",field:"middlewareType",style:{width:"13%"},hideSort:true},
				{name:"服务名",field:"middlewareName",style:{width:"20%"},hideSort:true},
				{name:"主机名",field:"clientName",style:{width:"20%"},hideSort:true},
				{name:"IP",field:"ip",style:{width:"14%"},hideSort:true},
				{name:"端口",field:"port",style:{width:"7%"},hideSort:true},
				{name:"版本",field:"version",style:{width:"8%"},hideSort:true},
				{name:"健康状态",style:{width:"11%"},hideSort:true,compile:true,formatter:function (value,row) {
					$scope.appServiceInfo.tooltipData.healthStatus[row.id] = 2;
					return "<span><i class='fa' ng-class='{0:\"fa-circle text-danger\",1:\"fa-circle text-success\",2:\"fa-spinner fa-spin\"}[tooltipData.healthStatus["+row.id+"]]'></i>{{{0:\"不可用\",1:\"&nbsp;&nbsp;&nbsp;&nbsp;可用\",2:\"查询中\"}[tooltipData.healthStatus["+row.id+"]]}}</span>";
				}},
				{name:"状态",field:"status",style:{width:"11%"},hideSort:true,compile:true,formatter:function (value,row) {
                    return "<span><i class='fa fa-circle' ng-class='{0:\"text-danger\",1:\"text-success\"}["+value+"]'></i>{{{0:\"已停止\",1:\"运行中\"}["+value+"]}}</span>";
                }},
			],
			methods: {}
		}
	}

    $scope.appServiceTableData = {};
    //应用信息查询
    $scope.getAppInfo = function (){
    	$scope.appServiceTableData = {};
		$scope.appServiceInfo.initData = [];

	    newHomeService.getAppInfo().then(function(response){
			$scope.loading = false;
			$scope.appServiceTableData = setAppInfo(response.data);
			$scope.appServiceInfo.initData = $scope.appServiceTableData;
			healthCheck($scope.appServiceTableData);
	    },function(){
			$scope.loading = false;
			$scope.appServiceTableData = {};
	    })
    }
    
    $scope.getAppInfo();

	//处理信息
	function setAppInfo(data){
		var appInfo = [], clientNameJSON = {};
		var num = 1;
		for (var key in data) {
			var	obj = data[key];
			for (var i in obj) {
				var info = obj[i];
				info["class"] = "info-class-" + num;
				appInfo.push(info);
			}
			num ++;
			if(num == 7)
				num = 1;
		}
		return appInfo;
	}
    
    function healthCheck(data){
    	/*批量查询  begin*/
    	/*var tempList = [];
    	for (var i in data) {
    		data[i].healthStatus = 2;
    		var url = "http://"+data[i].ip+":"+data[i].port+"/"+data[i].startContext;
			tempList.push({id:data[i].id, "url": url})
    	}
    	newHomeService.healthCheckAll(tempList).then(function(response){
			if(response.code == SUCCESS_CODE){
				var healthObject = response.data;
				for (var item of data) {
					item.healthStatus = healthObject[item.id];

		    	}
			}else{
				dbUtils.error(response.message,"提示");  
			}
		})*/
		/*批量查询  end*/
    	/*单次查询 begin*/
    	var tIds = [];
    	for (var i in data) {
    		tIds.push(data[i].id);
			var urlPath = "http://"+data[i].ip+":"+data[i].port+"/"+data[i].startContext;
			newHomeService.healthCheck(urlPath,data[i].id).then(function(response){
				if(response.code == SUCCESS_CODE){
					for(var j in tIds){
						if(response.data.id == tIds[j]){
							// data[j].healthStatus = response.data.status;
                            $scope.appServiceInfo.tooltipData.healthStatus[response.data.id] = response.data.status;
						}
					}
				}else{
					data[i].healthStatus = 0;
					dbUtils.error(response.message,"提示");  
				}
			})
		}
		/*单次查询 end*/
    }

    //运维历史
    $scope.logInfo = {};
    newHomeService.getlogInfo().then(function(response){
    	if(response.code == SUCCESS_CODE){
    		if(response.data){
        		$scope.logInfo = response.data;
        		$scope.loading = false;
        	}else{
        		$scope.logInfo = {};
        		$scope.loading = false;
        	} 
        }else{
        	$scope.logInfo = {};
        	$scope.loading = false;
            dbUtils.error(response.message,"提示");  
        }
    },function(result){
    	$scope.loading = false;
    })

    $scope.showLog = function (log) {
    	var instance = $modal.open({
	        animation: true,
	        templateUrl: 'tpl/logtable/show_logs.html',
	        controller: 'HomeLogController',
	        size: "lg",
	        backdrop: "true",
	        resolve: {
	            source: function () {
	                return {"data":log,"logType":1,"type":"log"};
	            }
	        }
	    });
    };
    
     //添加状态信息
     function setAppData(data){
    	 var app = [];
    	 for (var i = 0; i < data.length; i++) {	
        	var appData = {};
     		var obj = {};
     		obj = data[i];
     		for (var key in obj) {
         		if(key == "CPU"){
         			appData["cpuShow"] = (obj[key] * 100).toFixed(2);
         			appData["cpuPx"] = (1 - obj[key]) * 100;
         		}else if(key == "useMemory"){
         			appData["useMemoryShow"] = (obj[key] * 100).toFixed(2);
         			appData["memoryPx"] = (1 - obj[key]) * 130;
         		}else if(key == "status"){
         			appData["statusName"] =  statusName(obj[key]);
         			appData["status"] = obj[key];
         		}else{
         			appData[key] = obj[key];
         		}
     		}
//     		addDeploy(appData,i,id,appId);
     		app[i] = appData;
     	}
    	 return app;
     }
    
     //添加状态名
     function addDeploy(appData,i,id,appId){
    	 if(appData.isAdmin){ }else if(appData.status == 0){
     		$scope.appData[i]['deployStatus'] = "ERROR";
     		$scope.appData[i]['deployShow'] = "未部署";
        	$scope.appData[i]['deployDetail'] =  "部署情况：" + "未部署" ;
     	}else{
     		if(appData.isAdmin == 0){id = appData.serverId;}
     		newHomeService.checkDeployStatus(appId,id).then(function(response){
                 if(response.code== SUCCESS_CODE){
                	 $scope.appData[i]['deployDetail'] = "部署情况：" + response.data.deploy_status + "\r\n</br>" + 
														  "部署文件：" + response.data.project_file  + "\r\n</br>" +
														  "部署时间：" + response.data.time ;
                 	 if(response.data.deploy_status == "部署成功"){
                 		 $scope.appData[i]['deployStatus'] = "SUCEESS";
                 		$scope.appData[i]['deployShow'] = "成功";
                 	 }else if(response.data.deploy_status == "部署失败"){
                 		 $scope.appData[i]['deployStatus'] = "FAIL";
                 		$scope.appData[i]['deployShow'] = "失败";
                 	 }else{
                 		 $scope.appData[i]['deployStatus']= "ERROR";
                 		$scope.appData[i]['deployShow'] = "未部署";
                 		if(response.data.deploy_status || response.data.project_file  || response.data.time){ }
                 		else{$scope.appData[i]['deployDetail'] =  "部署情况：" + "未部署" ;}
                 	 }
                 }else{
                 	 $scope.appData[i]['deployStatus'] = "ERROR";
                 	$scope.appData[i]['deployShow'] = "未部署";
                 	 $scope.appData[i]['deployDetail'] =  "部署情况：" + "未部署" ;
                 }
             });
     	}
     }
     
     
     $scope.random = function(){
        return parseInt(5*Math.random())
     }
     
     $scope.toAppPage = function(){
    	 $state.go("app.manage",null);
     }
     
     $scope.toClientPage = function(){
    	 $state.go("app.client",null);
     }
  
}]);

app.controller('HomeLogController', ['dbUtils','$scope','$modalInstance', '$state','source','$http',
                                      function (dbUtils,$scope,$modalInstance, $state,source,$http) {
	$scope.title = "显示日志详情";
	$scope.showLogsDetail = "请稍等";
	
	function showLogs1(logId){
		$scope.showListHerf = "/logs/show/" + logId;
		$.ajax({  
	        type: "get", 
	        url: $scope.showListHerf, 
	        data: null,  
	        contentType: "application/json;charset=utf-8",  
	        dataType: "json",  
	        success: function (response, ifo) {  
	        	if (response.code == SUCCESS_CODE) {
	        		$scope.showLogsDetail = response.data; 
	           } else {
	        	   $scope.showLogsDetail = "查询有错误  错误原因"+response.message;
	               dbUtils.error(response.message,"提示");
	           } 
	        	$scope.$apply(function(){
	        		$scope.showLogsDetail = $scope.showLogsDetail;
                });
	        }, error: function () {  
	        	$scope.showLogsDetail = "查询有错误";
	            alert("error");  
	        }
		})
  }
	
	showLogs1(source.data.logId);
	
	
  $scope.cancel = function () {
      $modalInstance.close(false);
  };
  
  $scope.submitApply = function(){
      $modalInstance.close();
  }
  
}]);
