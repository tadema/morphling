'use strict';

let ClientController = app.controller('ClientController', ['$scope','$rootScope','$modal','$compile','EchoLog','multiEchoLog','dbUtils','ClientService',
                                    function($scope,$rootScope,$modal,$compile,echoLog,multiEchoLog,dbUtils,clientService) {
	$scope.errorNum = 0;
    $scope.warnOpenFlag = false;
    $scope.t_id = dbUtils.guid();
    let healthCheckRows = [];
    var table = {
        url:"/client",
        showUpload: true,
        // pageSql:true,
        UploadText:"模板",
		showText:true,waringText:"批量导入主机的步骤：1.导出模板，2.上传模板，3.批量启动Agent；Agent宕机：请重启Agent并刷新状态",
        headers: [
            {name:"名称",field:"name",style:{"width":"12%"}},
            {name:"服务器IP",field:"hostAddress",style:{"width":"12%"}},
            {name:"用户名",field:"username",style:{"width":"8%"}},
            {name:"监控",field:"isNodeMonitor",style:{"width":"6%"},formatter:function(value,row){
	            return value == 1 ? "是" : "否";
	        }},
            {name:"监控端口",field:"performancePort",style:{"width":"6%"},formatter:function(value,row){
            	if(!value || parseInt(value) == 0)
            		value = '';
	            return value ;
	        }},
            {name:"Agent端口",field:"port",style:{"width":"7%"},formatter:function(value,row){
            	if(!value || parseInt(value) == 0)
            		value = '';
	            return value ;
	        }},
            {name:"Agent状态",field:"agentStatus",class:"text-center",style:{"width":"7%"},compile:true,formatter:function(value,row){
				row.agentStatus = "DOWN";//为排序赋值
            	/*批量查询  begin*/
            	/*if(row.isAgent == 1){
            		healthCheckRows.push(row);*/
        		/*批量查询  end*/
        		/*单次查询 begin*/
        		if(row.isAgent == 1){
            		clientService.health({"host":row.hostAddress,"port":row.port}).then(function (response){
                        if(response.code == SUCCESS_CODE){
                            //请求成功，状态判断
							row.agentStatus = response.data.status;
                            $scope.health[row.id] = response.data.status;
                            $scope.healthDetail[row.id] = dbUtils.toString(response.data,true);
                            $scope.messege[row.id] = response.data.status;
                            clientService.messege(JSON.stringify(row)).then(function (response){
                                if(response.code == SUCCESS_CODE){
                                    $scope.clientMessege[row.id] = dbUtils.toString(response.data,true);
                                }else{
                                    $scope.clientMessege[row.id] = "连接异常";
                                }
                            })
                        }else{
                            $scope.errorNum+=1;
                            $scope.health[row.id] = "DOWN";
                            $scope.healthDetail[row.id] = response.message;
                            $scope.messege[row.id] = "DOWN";
                            $scope.clientMessege[row.id] = "连接异常";
                        }
                    })
                /*单次查询 end*/
            	}else{
					row.agentStatus = "NONE";
            		$scope.health[row.id] = "NONE";
                    $scope.healthDetail[row.id] = "无";
                    $scope.messege[row.id] = "NONE";
                    $scope.clientMessege[row.id] = "无";
            	}
                return "<span ng-if='!health["+row.id+"]'><i class='fa fa-spin fa-spinner '></i></span>" +
                    "<span class='cusor-pointer' data-toggle='tooltip' tooltip-placement='auto left' tooltip-trigger='outsideClick' uib-tooltip-html='healthDetail["+row.id+"]'  ng-if='health["+row.id+"] == \"UP\"'><i style='margin-right:5px;' class='fa fa-circle text-success'></i>UP</span>"+
                    "<span class='cusor-pointer' data-toggle='tooltip' tooltip-placement='auto left' tooltip-trigger='outsideClick' uib-tooltip-html='healthDetail["+row.id+"]'  ng-if='health["+row.id+"] == \"DOWN\"'><i style='margin-right:5px;' class='fa fa-circle text-danger'></i>DOWN</span>"+
                    "<span ng-if='health["+row.id+"] == \"NONE\"'>无</span>";
            }},
            {name:"连通性",field:"clientConnection",class:"text-center",style:{"width":"5%"},compile:true,formatter:function(value,row){//field:"clientConnection",class:"text-center",
            	row.clientConnection = 0;//为排序赋值
            	/*批量查询  begin*/
            	/*$scope.clientConnection[row.id] = 2;
            	return "<span ng-if='clientConnection["+row.id+"] == 2'><i class='fa fa-spin fa-spinner '></i></span>" +
            			"<span title='已连接' ng-if='clientConnection["+row.id+"] == 1'><i class='fa fa-link fa-lg ' style='color:#27C24C;'></i></span>"+
                		"<span title='请检查用户名和密码是否正确，若密码已被修改请更新密码' ng-if='clientConnection["+row.id+"] == 0'><i class='fa fa-unlink fa-lg ' style='color:#FE3600;'></i></span>";*/
            	/*批量查询  end*/

            	/*单次查询 begin*/
            	$scope.clientConnection[row.id] = 0;
            	var returnResult = "<span title='已连接' ng-if='clientConnection["+row.id+"] == 1'><i class='fa fa-link fa-lg ' style='color:#27C24C;'></i></span>"+
                "<span title='请检查用户名和密码是否正确，若密码已被修改请更新密码' ng-if='clientConnection["+row.id+"] == 0'><i class='fa fa-unlink fa-lg ' style='color:#FE3600;'></i></span>";

            	clientService.checkConnected(JSON.stringify(row)).then(function (response){
                    if(response.code == SUCCESS_CODE){
						row.clientConnection = response.data.isConnected;
                        $scope.clientConnection[row.id] = response.data.isConnected;
                    }
                    //agent更新条件，用户必须匹配上或者admin用户
                    if(row.isUpdate == "1" && (row.createUsername == $rootScope._userinfo.username || $rootScope._userinfo.isAdmin == 1) && row.isAgent == 1 && $scope.clientConnection[row.id] == 1){
    	        		$scope.warningRepeat = true;
    	        		$scope.agentNeedUpdate.push(row);
    	        		updateWarning();
    	        	}
                    return returnResult;
                },function (response){
                	return returnResult;
                })
                return returnResult;
            	/*单次查询 end*/
            }},
            {name:"主机信息",field:"messege",class:"text-center",style:{"width":"6%"},hideSort:true,compile:true,formatter:function(value,row){
                return "<span ng-if='!clientMessege["+row.id+"]'><i class='fa fa-spin fa-spinner '></i></span>" +
                "<span class='label bg-light cusor-pointer' data-toggle='tooltip' tooltip-trigger='outsideClick' tooltip-placement='auto left' uib-tooltip-html='clientMessege["+row.id+"]' ng-if='clientMessege["+row.id+"] && messege["+row.id+"] == \"UP\"'>查看详情</span>"+
                "<span class='label bg-light cusor-pointer' data-toggle='tooltip' tooltip-trigger='outsideClick' tooltip-placement='auto left' uib-tooltip-html='clientMessege["+row.id+"]' ng-if='clientMessege["+row.id+"] && messege["+row.id+"] == \"DOWN\"'>查看详情</span>"+
                "<span class='label bg-light cusor-pointer' data-toggle='tooltip' tooltip-trigger='outsideClick' tooltip-placement='auto left' uib-tooltip-html='clientMessege["+row.id+"]' ng-if='clientMessege["+row.id+"] && messege["+row.id+"] == \"ERROR\"'>查看详情</span>"+
                "<span ng-if='clientMessege["+row.id+"] && messege["+row.id+"] == \"NONE\"'>无</span>";
            }},
			{name:"创建者",field:"createUsername",style:{"width":"6%"}},
            {name:"更新时间",field:"createTime",style:{"width":"10%"}},
        ],
        rowEvents:[{
			class:"btn-info",icon:"fa fa-pencil",title:"终端",
			click: function(row){
				window.open("tpl/system/client_xterm.html?host="+row.hostAddress+"&port="+row.linuxPort+"&username="+row.username+"&password="+row.password);
			}
		},{
            class:"btn-info",icon:"fa fa-repeat",title:"更新Agent版本",name:"更新",
            isShow:function(row){
				//agent更新条件，用户必须匹配上或者admin用户
                return row.isUpdate == "1" && (row.createUsername == $rootScope._userinfo.username || $rootScope._userinfo.isAdmin == 1) && row.isAgent == 1 && $scope.clientConnection[row.id] == 1;
            },
            click: function(row){
            	dbUtils.openModal('tpl/system/client_remove.html','ClientModifyController',"lg","",{"data":angular.copy(row),"action":"update"},function (id) {
                	if(id){
                		//开启日志查看窗口
                        echoLog.show(id,function(){
                        	$scope.errorNum = 0;
                            $scope.table.operations.reloadData();
                            queryCountNum();
                        });
                	}else{
                		$scope.errorNum = 0;
                	}
                });
            }
        },{
        	class:"btn-info",icon:"fa fa-pencil",title:"编辑",
            isShow:function(row){
            	return row.createUsername == $rootScope._userinfo.username ||$rootScope._userinfo.isAdmin == 1;
            },
            click: function(row){
            	dbUtils.openModal('tpl/system/client_add.html','ClientAddOrEditController',"lg","",{"data":angular.copy(row),"action":"edit"},function (data) {
                	if(data){
                    	$scope.errorNum = 0;
                        $scope.table.operations.reloadData();
                        queryCountNum();
                	}else{
                		$scope.errorNum = 0;
                	}
                });
            }
        },{
        	class:"btn-danger",icon:"fa fa-trash",title:"删除",
            isShow:function(row){
            	return row.createUsername == $rootScope._userinfo.username ||$rootScope._userinfo.isAdmin == 1;
            },
            click: function(row){
                dbUtils.confirm("请先确认已经删除掉与该主机有关联的所有应用！！！！！  </br> 否则会导致系统不稳定！！！",function(){
                	dbUtils.openModal('tpl/system/client_remove.html','ClientModifyController',"lg","",{"data":angular.copy(row),"action":"delete"},function (data) {
	                    //开启日志查看窗口
	                	if(data){
	                		if(data.flag == false){
		                		dbUtils.error("删除失败","提示");
		                	}else if(data.flag == true && data.isAgent == 1){
		                    	echoLog.show(data.id,function(){
		                    		$scope.errorNum = 0;
		                            $scope.table.operations.reloadData();
		                            queryCountNum();
		                        });
		                	}else{
		                		$scope.errorNum = 0;
	                            $scope.table.operations.reloadData();
	                            queryCountNum();
		                	}
	                	}else{
	                		$scope.errorNum = 0;
	                	}
	                });
	            },function(){},true);
            }
        }],
        operationEvents:[{
        	class:"btn-info",icon:"fa fa-plus",name:"配置主机",id:"client-plus",left:true,
            click:function(rows){
            	var a = 1;
            	$scope.addClient();
            }
        },{
        	class:"btn-info",icon:"fa fa-download",name:"导出模板",id:"client-download",left:true,
            click:function(rows){
            	fileDown();
            }
        },{
        	class:"btn-info",icon:"fa fa-play",id:"client-start",name:"启动Agent",
			click:function(rows){
            	if(checkUpdataAll(rows)){updataAll(updataAllData(rows),rows);}
            }
        },{
        	class:"btn-info",icon:"fa fa-repeat",id:"client-restart",name:"重启Agent",
            click:function(rows){
            	if(checkUpdataAll(rows)){updataAll(updataAllData(rows));}
            }
        },{
        	class:"btn-info",icon:"fa fa-sign-out",id:"client-sign-out",name:"导出主机列表",
            click:function(rows){
            	if(rows.length < 1){
                	dbUtils.warning("请至少选择一个主机选项!","提示");
                	return false;
                }else{
                	exportToData(rows);
                }
            }
        }],
        beforeReload:function(){
            $scope.health = {};
            $scope.healthDetail = {};
            $scope.messege = {};
            $scope.clientMessege = {};
            $scope.warningRepeat = false;
			$scope.warnOpenFlag = false;
            $scope.agentNeedUpdate = [];
            $scope.clientConnection = {};
            healthCheckRows = []
        },
        afterReload:function(){
        	//3个方块--77	查询条件--45	按钮---42		页码--60	2个padding--30
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight31+"px";
        	},200)
			$scope.$emit("pageLoadSuccess",{"name":"client"});
//        	healthCheckHandle();
//        	connectCheckHandle();
        },
        settings:{
            showCheckBox:true,
			// pageSize:2,
            filterId: "clientFilter",
			theadId: "clientTheadId",
			operationId: "clientOperationId",
			uploadId: "clientUploadId",
            leftButton:true,
//            tableStyle:{"width":"120%","max-width":"120%"},
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-74-41-43-34-56-20)+"px",
            },
        }
    }

    //健康检查
    function healthCheckHandle(){
    	clientService.healthCheck(healthCheckRows).then(function (response){
    		if(response.code == SUCCESS_CODE){
    			let respData = response.data;
    			for(let item of healthCheckRows){
    				$scope.health[item.id] = respData[item.id].status;
    				$scope.healthDetail[item.id] = dbUtils.toString(respData[item.id].diskSpace,true);
    				$scope.messege[item.id] = $scope.health[item.id];
    				$scope.clientMessege[item.id] = dbUtils.toString(respData[item.id].clientMessege,true);
    			}
            }else{
            }
    	})
    }

    //连接性检查
    function connectCheckHandle(){
    	clientService.checkConnectedAll(healthCheckRows).then(function (response){
            if(response.code == SUCCESS_CODE){
            	let respData = response.data;
            	for(let item of healthCheckRows){
            		$scope.clientConnection[item.id] = respData[item.id].isConnected;

					//agent更新条件，用户必须匹配上或者admin用户
                    if(item.isUpdate == "1" && (item.createUsername == $rootScope._userinfo.username || $rootScope._userinfo.isAdmin == 1) && item.isAgent == 1 && $scope.clientConnection[item.id] == 1){
                		$scope.warningRepeat = true;
                		$scope.agentNeedUpdate.push(item);
                		updateWarning();
                	}
            	}
            }
        })
    }

    function updateWarning(){
    	if($scope.warningRepeat && !$scope.warnOpenFlag) {
    		$scope.warnOpenFlag = true;
        	dbUtils.confirm("有agent版本落后，为保证功能完善，是否更新至最新版",function(){
        		if(checkUpdataAll($scope.agentNeedUpdate)){
        			updataAll(updataAllData($scope.agentNeedUpdate));
        		}
        	},function(){
        	})
        }
    }

	//处理要导出的数据
	function exportToData(rows) {
		var excelData = [];
		for(var i in rows){
			var obj = rows[i];
			var push = {};
			for(var j in $scope.table.headers){
				var header = $scope.table.headers[j];
				if(header.name == "Agent状态" || header.name == "主机信息" ){

				}else{
					push[header.name] = obj[header.field];
				}
			}
			excelData.push(push);
		}

		var exceltitle = '主机信息导出Excel文件'
		exportToExcel(exceltitle,excelData)
	}
	//导出excel 方法
	function exportToExcel(exceltitle,excelData) {
		if(!angular.isArray(excelData)){
			console.info("导出数据为空");
			return;
		}
		// 导出文件的格式
		var excelTyle = {
			headers:true,
			//可以写一些导出来的excel样式
		};
		alasql('SELECT * INTO XLSX("'+exceltitle+'.xlsx",?) FROM ?',[excelTyle,excelData]);
	}

    //显示信息详情
    $scope.messegeShow = function(messege) {
    	dbUtils.openModal('tpl/system/client_messege.html','MessegeShowController',"lg","",{"data":messege},function (data) {});
	};

	$scope.uploadHandle = function(file){
    	var formData = fileExcel(file);
    	if(formData){fileUpdata(formData);}
	}

    //excel文件处理
    function fileExcel(file){
        var formData = new FormData();

    	if(file == null || file == 'undefined'){
    		dbUtils.error("请上传excel文件！","提示");
       	}else{
       		var fileName = file.name.substring(file.name.lastIndexOf(".") + 1);
       		if(fileName =="xlsx" || fileName =="xls" ){
           		formData.append("fileName",file);
           	}else {
           		dbUtils.error("请上传excel类型文件！","提示");
           		formData = false;
           	}
       	}
    	return formData;
	}

    //上传Excel
    function fileUpdata(formData){
    	clientService.fileUpdata(formData).then(function(response){
 	        if (response.code == SUCCESS_CODE) {
 	            dbUtils.success("上传成功","提示");
 	           $scope.table.operations.reloadData();
               queryCountNum();
 	        } else {
 	        	dbUtils.error("上传失败","提示");
 	            dbUtils.error(response.message,"提示");
 	           $scope.table.operations.reloadData();
               queryCountNum();
 	        }
 	    },function(){
 	    	dbUtils.error("上传失败","提示");
 	    })

    }

    //下载Excel
    function fileDown(){
    	var url = "/client/downloadTemplate";
        var form = $("<form></form>").attr("action", url).attr("method", "post");
        form.append($("<input></input>").attr("type", "hidden").attr("name", "filePath"));
        form.appendTo('body').submit().remove();

    }

    //实例数量的接口
    function queryCountNum(){
    	$scope.count = {"total":0,'onLine':0,"offLine":0};
    	clientService.count().then(function(response){
            if(response.code == SUCCESS_CODE){
            	$scope.count = response.data;
//            	$scope.count.total = response.data.total;
//            	$scope.count.onLine = response.data.onLine;
//            	$scope.count.offLine = response.data.offLine;
            }else{
                dbUtils.error(response.message,"提示");
            }
        },function(){
        	alert("error");
    	})
    }

    $scope.table = table;
    queryCountNum();

    $scope.addClient = function(){
    	dbUtils.openModal('tpl/system/client_add.html','ClientAddOrEditController',"lg","",{},function (data) {
        	if(data && data.id){
        		if(data.isAgent == 1){
                	echoLog.show(data.id,function(){
                		$scope.errorNum = 0;
                        $scope.table.operations.reloadData();
                        queryCountNum();
                    });
            	}else{
            		$scope.errorNum = 0;
                    $scope.table.operations.reloadData();
                    queryCountNum();
            	}
        	}else{
        		$scope.errorNum = 0;
                queryCountNum();
        	}
        });
    }

    //检查是否可以 全部更新
    function checkUpdataAll(rows){
    	var isAgent = false;
        for(var i in rows){
        	var row = rows[i];
            if(($rootScope._userinfo.isAdmin != 1)&&(row.createUsername != $rootScope._userinfo.username)){
            	dbUtils.warning("当前账户不是["+row.name+"]主机的创建者</br>不能对其进行更新操作</br>请勾选掉["+row.name+"]主机选项!","提示");
            	return false;
            }
            if(row.isAgent == 1){isAgent = true;}
        }
        if(rows.length < 1){
        	dbUtils.warning("请至少选择一个主机选项!","提示");
        	return false;
        }
        if(isAgent == false){
        	dbUtils.warning("所选主机没有一个有agent，无法更新!","提示");
        	return false;
        }
        return true;
    }

    //全部更新  的数据
    function updataAllData(rows){
    	var ids = [];
        for(var i in rows){
            ids.push(rows[i].id);
        }
        return ids;
    }

    //全部更新
    function updataAll(clientIds,rows){
		 dbUtils.confirm("确定继续操作？",function(){
			 clientService.updataAll({clientIds:clientIds}).then(function(response){
				if(response.code == SUCCESS_CODE){
					echoLog.show(response.data,function(){
						$scope.errorNum = 0;
						$scope.table.operations.reloadData();
						queryCountNum();
					});
				}else{
					dbUtils.error(response.message,"提示");
				}
			 })
			 /*clientService.batchUpdateNew({clientIds:clientIds}).then(function(response){
				if(response.code == SUCCESS_CODE){
					multiEchoLog.show(response.data, function(){
						$scope.errorNum = 0;
						$scope.table.operations.reloadData();
						queryCountNum();
					},rows,"id","name");
				}else{
					dbUtils.error(response.message,"提示");
				}
			 })*/
	     },function(){},true);
    }

    // ----------------------------功能解析 start---------------------------------------------

    $scope.explain = function (){
    	dbUtils.get("guide.json",{}).then(function (response) {
			console.log(response)
		},function (respone){
			console.log(respone)
		})

    	/*var tour = {
			  data : [
			    { element: '#client-plus', 'tooltip' : '点击这个按钮，添加一个主机信息',position:'B',text:'添加主机'},
			    { element: '#client-download', 'tooltip' : '如果想要批量添加主机信息，先点击下载模板按钮，按照模板格式填写多个主机信息',position:"T" ,text:'批量添加主机'},
			    { element: '#foo-choose-file', 'tooltip' : '确保信息没有填写错误后，点击选择文件，选择已填好的文件',position:"T" ,text:'批量添加主机'},
			    { element: '#upload', 'tooltip' : '信息框显示上次的文件名，确认无误后，点击上传按钮，便可批量添加主机信息',position:"T" ,text:'批量添加主机'},
	            { element: '#client-start', 'tooltip' : '启动选中的agent主机',position:"T" ,text:'启动主机agent'},
			    // { element: '#foo-row-events', 'tooltip' : '当morphling-agent.tar.gz有更新时，对应主机会显示更新按钮，点击便可更新对应主机',position:"T" ,text:'更新主机'},
			    // { element: '#foo-row-events', 'tooltip' : '点击编辑按钮便可修改对应主机内信息',position:"L" ,text:'编辑主机信息'},
	            // { element: '#foo-row-events', 'tooltip' : '点击垃圾桶删除按钮便可删除对应主机',position:"L" ,text:'删除主机'},
				  { element: '#client-repeat', 'tooltip' : '把选中的主机的agent更新到最新版本',position:"T" ,text:'更新主机agent'},
				  { element: '#client-sign-out', 'tooltip' : '导出选中的主机数据',position:"T" ,text:'导出数据'},
				  { element: '#foo-row-events', 'tooltip' : '这里显示的操作都是可以对每条对应主机进行的操作',position:"L" ,text:'单个主机操作'},
				  /!*{ element: '.fa-pencil', 'tooltip' : 'button 3 tooltip ',position:"L" ,text:'按钮4的作用是',callback:function(){
                      var a = 1;
                      document.getElementById("client-addClient").click();
                  }},*!/
			  ],
			  welcomeMessage : '主机页面主要功能步骤指引',
			  buttons:{
				    next  : { text : '下一步', class : 'btn btn-sm btn-info'},
				    prev  : { text : '上一步', class: 'btn btn-sm btn-info' },
				    start : { text : '开始', class: ' btn btn-sm btn-success' },
				    end   : { text : '结束', class: ' btn btn-sm btn-danger' }
			  },
			  controlsPosition : 'TR',
			  tooltipCss:{
				  color : '#ffffff',
				  background : 'rgba(0, 0, 0, 0.45)'
			  },
			  controlsCss:{
				  color : '#ffffff',
				  background : 'rgba(0, 0, 0, 0.45)',
			  }
		};*/
    	// $.aSimpleTour(tour,1);
    }


    // -----------------------------功能解析  end------------------------------------------
}]);



let ClientAddOrEditController = app.controller('ClientAddOrEditController', ['dbUtils','$scope','$modalInstance', '$state','source','ClientService',
                                       function (dbUtils,$scope,$modalInstance, $state,source,clientService) {

	$scope.testFlag = false;
	if(source.data && source.data.id){
		$scope.isEdit = true;
		$scope.pwdFlag = false;
		$scope.client = source.data;
	}else{
		$scope.isEdit = false;
		$scope.pwdFlag = true;//代表密码为明文
		$scope.client = {port:21111,username:"",linuxPort:22,performancePort:9100,isNodeMonitor:1,isAgent:1};
	}
	//自动补全client模型的name
	$scope.fillClientName = function(value,position){
		clientService.fillClientName($scope,value,position,$scope.isEdit,true);//true为启动自动补全，false为取消自动补全
	}

    $scope.clientPortBlur = function (data,propName) {
    	let port = data[propName];
		if(angular.isUndefined(port) || port == 0){
			dbUtils.info("端口号请输入数字！","提示");
			return;
		}else{
			var value = Number(port);
			$scope.checkPortDuplicate($scope.client.hostAddress,$scope.client,[propName]);
			if(angular.isUndefined($scope.client.performancePort) || $scope.client.performancePort == 0)
				$scope.client.performancePort = value + 100;
		}
    };

//	把controller中对端口检验的配置转移到ui-dbUtils.js中，方便管理和使用,提供ipPortList和oldData的初始化
	dbUtils.startPortCheck($scope,$scope.isEdit,$scope.client);

    $scope.checkPortDuplicate = function(ip,data,propNameList){
    	return dbUtils.checkPortDuplicate(ip,data,propNameList,$scope.ipPortList,$scope.oldData,$scope.isEdit,"hostAddress");
    }

    $scope.testClient = function(){
    	if($scope.form.$invalid){
            return;
        }
    	$scope.testFlag = true;
		$scope.connected = 2;//查询中
    	if($scope.pwdFlag == false){
    		//密码没有改变，密码是编码的
    		clientService.checkConnected(JSON.stringify($scope.client)).then(function(response){
        		// $scope.testFlag = true;
        		if(response.code == SUCCESS_CODE){
        			$scope.connected = response.data.isConnected;
        		}else{
        			dbUtils.error(response.message,"提示");
        		}
        	})
    	}else{
    		//密码改变了，密码没有被编码
    		clientService.testConnected(JSON.stringify($scope.client)).then(function(response){
        		// $scope.testFlag = true;
        		if(response.code == SUCCESS_CODE){
        			$scope.connected = response.data.isConnected;
        		}else{
        			dbUtils.error(response.message,"提示");
        		}
        	})
    	}
    }

    $scope.$watch('client',function(newValue,oldValue){
    	if(newValue != oldValue){
    		if($scope.isEdit == true && newValue.password != oldValue.password)
    			$scope.pwdFlag = true;
    		if(oldValue.isNodeMonitor == 0 && newValue.isNodeMonitor == 1 && $scope.client.performancePort == 0){
    			$scope.client.performancePort = "";
    		}
    		if(oldValue.isAgent == 0 && newValue.isAgent == 1 && $scope.client.port == 0){
    			$scope.client.port = "";
    		}
    	}
    },true)

    $scope.submitApply = function(){
        if($scope.form.$invalid){
            return;
        }
        if($scope.client.isNodeMonitor == 1){
			var port = $scope.client['performancePort'];
			if(!port || port == "" || port == 0){
				dbUtils.error("请填写监控端口参数","提示");
				return;
			}
		}
        if($scope.client.isAgent == 1){
			var port = $scope.client['port'];
			if(!port || port == "" || port == 0){
				dbUtils.error("请填写Agent端口","提示");
				return;
			}
		}
        var client = {};
    	for (var key in $scope.client) {
    		if(key == "jmxPort"||key == "prometPort"){
    			client[key] = parseInt($scope.client[key]);
    		}else if(key == "performancePort"){
    			if($scope.client.isNodeMonitor == 1){
    				client[key] = parseInt($scope.client[key]);
    			}else{
    				client[key] = parseInt(0);
    			}
    		}else if(key == "port"){
    			if($scope.client.isAgent == 1){
    				client[key] = parseInt($scope.client[key]);
    			}else{
    				client[key] = 0;
    			}
    		}else{
    			client[key] = $scope.client[key];
    		}
		}

    	var idDuplicate = $scope.checkPortDuplicate(client.hostAddress,client,['port','performancePort']);
    	if(idDuplicate == true)
    		return;

    	if($scope.isEdit == false){
    		clientService.save(client).then(function(response){
    			if (response.code == SUCCESS_CODE) {
    				var data = {};
    				data.id = response.data;
    				data.isAgent = $scope.client.isAgent;
    				dbUtils.success("创建成功","提示");
    				$modalInstance.close(data);
    			} else {
    				dbUtils.error(response.message,"提示");
    			}

    		});
    	}else{
    		clientService.edit(client,0).then(function (response) {
                if (response.code == SUCCESS_CODE) {
                    dbUtils.success("修改成功","提示");
                    $modalInstance.close(true);
                } else {
                    dbUtils.error(response.message,"提示");
                }
            })
    	}
    }

    $scope.cancel = function () {
        $modalInstance.close(false);
    };
}]);

let ClientModifyController = app.controller('ClientModifyController', ['dbUtils','$scope','$modalInstance', '$state','source','ClientService',
                                          function (dbUtils,$scope,$modalInstance, $state,source,clientService) {

    $scope.client = source.data;
    $scope.client.isAgent = parseInt($scope.client.isAgent);

    $scope.$watch("$viewContentLoaded",function () {
		var input = $("input[name='isAgent']");
		input.each(function(){
			if($(this).val() == $scope.client.isAgent)
				$(this).attr("checked","checked");
		});
	})

    if(source.action == "delete"){
    	$scope.deleteData = true;
    }else{
    	$scope.deleteData = false;
    }

    $scope.cancel = function () {
    	$modalInstance.close(false);
    };

    $scope.$watch('client.isNodeMonitor',function(newValue,oldValue){
    	if(newValue != oldValue){
    		if(oldValue == 0 && newValue == 1 && $scope.client.performancePort == 0){
    			$scope.client.performancePort = "";
    		}
    	}
    })

    $scope.clientPortBlur = function (data,propName) {
    	let port = data[propName];
		if(angular.isUndefined(port) || port == 0){
			dbUtils.info("端口号请输入数字！","提示");
			return;
		}else{
			var value = Number(port);
			$scope.checkPortDuplicate($scope.client.hostAddress,$scope.client,[propName]);
			if(angular.isUndefined($scope.client.performancePort) || $scope.client.performancePort == 0)
				$scope.client.performancePort = value + 100;
		}
    };

//	把controller中对端口检验的配置转移到ui-dbUtils.js中，方便管理和使用,提供ipPortList和oldData的初始化
	dbUtils.startPortCheck($scope,true,$scope.client);

    $scope.checkPortDuplicate = function(ip,data,propNameList){
    	return dbUtils.checkPortDuplicate(ip,data,propNameList,$scope.ipPortList,$scope.oldData,true,"hostAddress");
    }

    $scope.submitApply = function(){
        if($scope.form.$invalid){
            return;
        }
        if($scope.client.isNodeMonitor == 1){
			var port = $scope.client['performancePort'];
			var agentPort = $scope.client['port'];
			var value = Number(port);
			agentPort = Number(agentPort);
			if(angular.isUndefined(port) || port == ""){
				dbUtils.error("请填写监控端口参数","提示");
				return;
			}else{
				if(isNaN(value)){
					dbUtils.error("请准确填写监控端口参数","提示");
					return;
				}else if(value < 0 ||value > 65535 || agentPort < 0 || agentPort > 65535){
					dbUtils.error("请准确填写监控端口参数,只能填写0~65535数字","提示");
					return;
				}
			}
		}
        var client = {};
    	for (var key in $scope.client) {
    		if(key == "jmxPort"||key == "prometPort"){
    			client[key] = parseInt($scope.client[key]);
    		}else if(key == "performancePort"){
    			if($scope.client.isNodeMonitor == 1){
    				client[key] = parseInt($scope.client[key]);
    			}else{
    				client[key] = parseInt(0);
    			}
    		}else if(key == "port"){
    			if($scope.client.isAgent == 1){
    				client[key] = parseInt($scope.client[key]);
    			}else{
    				client[key] = 0;
    			}
    		}else{
    			client[key] = $scope.client[key];
    		}
		}

    	var idDuplicate = $scope.checkPortDuplicate(client.hostAddress,client,['performancePort']);
    	if(idDuplicate == true)
    		return;

        if(source.action == "delete"){
        	dbUtils.confirm("确定删除？",function(){
        		clientService.remove(client).then(function(response){
	                if (response.code == SUCCESS_CODE) {
	                    var id = response.data;
	                    id.isAgent = $scope.client.isAgent;
	                    dbUtils.success("删除成功","提示");
	                    $modalInstance.close(id);
	                } else {
	                    dbUtils.error(response.message,"提示");
	                }
        		});
        	},function(){

        	});
        }else if(source.action == "update") {
            clientService.update(client).then(function (response) {
                if (response.code == SUCCESS_CODE) {
                    var id = response.data;
                    dbUtils.success("更新成功","提示");
                    $modalInstance.close(id);
                } else {
                    dbUtils.error(response.message,"提示");
                }
            })
        }
    }
}]);

let MessegeShowController = app.controller('MessegeShowController', ['dbUtils','$scope','$modalInstance', '$state','source',
                                        function (dbUtils,$scope,$modalInstance, $state,source) {

    $scope.messege = source.data;
    $scope.show = [];
    var b = $scope.messege.OS_VERSION;

    $scope.submitApply = function () {
    	$modalInstance.close(false);
    };

	$scope.cancel = function () {
	    $modalInstance.close(false);
	};
}]);