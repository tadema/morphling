'use strict';
let DeployController = app.controller('DeployController',
		['$scope','$rootScope','$modal','$state','$stateParams','dbUtils','EchoLog','$http','ApplicationService','EndpointService','DeployService',
		 function($scope,$rootScope,$modal,$state,$stateParams,dbUtils,echoLog,$http,appService,endpointService,deployService) {
    let stateParams;
	if($stateParams.deployAppId && $stateParams.app){
		stateParams = $stateParams;
    }else{
    	if($scope.appCfg.tabManager.TABS && $scope.appCfg.tabManager.TABS.length > 0){
	    	for(let i in $scope.appCfg.tabManager.TABS){
				let tab = $scope.appCfg.tabManager.TABS[i];
				if(tab.hash == "app.deploy"){
					stateParams = tab.params;
					break;
				}
			}
		}else{
			$state.go("app.selectAppDeploy",{title:"选择应用(发布)"})
			return;
		}
    }
	
    $scope.$on('$stateChangeStart', function(event,data) {
		//路由变化，先删除定时器(表格的状态刷新定时)
		if($scope.timeId)
			window.clearInterval($scope.timeId);
	});

    $scope.appId = stateParams.deployAppId;
    $scope.app = stateParams.app;
    $scope.fromWhere = stateParams.fromWhere;
    $scope.t_id = dbUtils.guid();
    $scope.parallel = false;
    let rows;
    var healthCheckList,rowsObj = {};
    $scope.monitor = {"status":{},'registStatus':{},"nginxStatus":{},"healthStatus":{},"healthDetail":{},"info":{},"deployStatus":{},"deployDetail":{},"agentStatus":{},"deployDate":{}};//承载表格中字段的自定义显示时使用

    //根据应用类型加载表格模板
    function beforeLoad(){
        rows = [];
        healthCheckList = [];
        if(Number($scope.app.appExtendVO.versionHoldNum) > 0){
            $scope.versionNumShowText = Number($scope.app.appExtendVO.versionHoldNum);
        }else{
            $scope.versionNumShowText = "否";
        }
        var middlewareType = $scope.app.middlewareType;
        if(middlewareType == 'weblogic'){
            $scope.table = weblogicTable;
        }else if(middlewareType == 'tomcat'){
            $scope.table = tomcatTable;
        }else if(middlewareType == 'springboot'){
            $scope.table = springBootTable;
        }else{
            //如果新增了其他类型的应用，这里注意继续添加，默认选择springboot类型
            $scope.table = springBootTable;
        }
        queryCountNum();
    }
    
    //实例数量的接口
    function queryCountNum(){
    	$scope.count = {"total":0,'onLine':0,"offLine":0};
    	deployService.queryCountNum($scope.appId,$scope.app.middlewareType).then(function(response){
            if(response.code == SUCCESS_CODE){
            	$scope.count = response.data;
            }else{
                dbUtils.error(response.message,"提示");
            }
        })
    }

    let adminServerStatus;
    //weblogic的表格
    var weblogicTable = {
        url:"/app/preview/"+$scope.appId+"/domains",
        showUpload: true,
        pageSql:true,
        isRefreshStatus:true,
        headers: [
            {name:"server",field:"name",style:{"width":"9%"}},
            {name:"角色",field:"isAdmin",style:{"width":"7%"},compile:true,formatter:function(value,row){
            	return row.isAdmin == 1 ? "管理server" : "受管server";
            }},
            {name:"域名称",field:"domainName",style:{"width":"11%"}},
            {name:"IP",field:"ip",style:{"width":"10%"},compile:true,formatter:function(value,row){
            	row.host = value;//刷新按钮会用到
                return value;
            }},
            {name:"端口",field:"port",style:{"width":"5%"}},
            {name:"部署版本",field:"currentVersion",style:{"width":"6%"}},//deployVersion
            {name:"状态",field:"status",style:{"width":"6%"},compile:true,formatter:function(value,row){
                row.status = value;//刷新后，为status赋值
                if(row.isAdmin == 1){
                    adminServerStatus = value;
                }
                $scope.monitor['status'][row.id] = value;//首次初始化该状态
                return "<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-light\",1:\"text-success\",2:\"text-danger\"}[monitor.status["+row.id+"]]'></i>{{{0:\"待部署\",1:\"运行中\",2:\"已停止\"}[monitor.status["+row.id+"]]}}</span>"
            }},
            {name:"健康状态",field:"healthStatus",class:"text-center",style:{"width":"6%"},compile:true,formatter:function(value,row){
                rowsObj[row.id] = row;
            	$scope.monitor['healthStatus'][row.id] = 2;
            	var url = "http://"+row.serverIp+":"+row.serverPort+"/"+$scope.app.startContext;
            	healthCheckList.push({id:row.id, "url": url})
            	return "<span ng-if='monitor.healthStatus["+row.id+"] == 2'><i class='fa fa-spin fa-spinner'></i></span>" +
            			"<span ng-if='monitor.healthStatus["+row.id+"] != 2'><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-danger\",1:\"text-success\"}[monitor.healthStatus["+row.id+"]]'></i>{{{0:\"不可用\",1:\"可用\"}[monitor.healthStatus["+row.id+"]]}}</span>";
            }},
            {name:"Agent",field:"agentStatus",class:"text-center",style:{"width":"6%"},compile:true,formatter:function(value,row){
            	rows.push(row);
            	$scope.monitor['agentStatus'][row.id] = 0;
            	return "<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-danger\",1:\"text-success\"}[monitor.agentStatus["+row.id+"]]'></i>{{{0:\"DOWN\",1:\"UP\"}[monitor.agentStatus["+row.id+"]]}}</span>";
            }},
            {name:"部署情况",class:"text-center",style:{"width":"8%"},hideSort:true,compile:true,formatter:function(value,row){
            	deployService.checkDeployStatus($scope.appId,row.serverId).then(function(response){
                    if(response.code == SUCCESS_CODE){
                        let date = response.data && response.data.time ? response.data.time : '--';
                        row.deployDate = date;
                        $scope.monitor["deployDate"][row.id] = date;
                    	let tooltip_template = "部署情况：" + response.data.deploy_status + "\r\n</br>" + "部署文件：" + response.data.project_file  + "\r\n</br>" +
												"部署文件版本：" + response.data.version  + "\r\n</br>" + "部署时间：" + response.data.time;
                    	 if(response.data.deploy_status == "部署成功"){
                    		 $scope.monitor['deployStatus'][row.id] = "SUCEESS";
                    		 $scope.monitor['deployDetail'][row.id] = tooltip_template;
                    	 }else if(response.data.deploy_status == "部署失败"){
                    		 $scope.monitor['deployStatus'][row.id] = "FAIL";
                    		 $scope.monitor['deployDetail'][row.id] = tooltip_template;
                    	 }else{
                    		 $scope.monitor['deployStatus'][row.id] = "ERROR";
                    		 $scope.monitor['deployDetail'][row.id] =  "部署情况：" + "未部署" ;
                    	 }
                    }else{
                    	 $scope.monitor['deployStatus'][row.id] = "ERROR";
                    	 $scope.monitor['deployDetail'][row.id] = "部署情况：" + "未部署" ;
                    }
                });
                return "<span class='badge sm-br cusor-pointer' ng-class=\"{'SUCEESS':'bg-success','FAIL':'bg-danger','ERROR':'bg-light'}[monitor.deployStatus["+row.id+"]]\"   "+
                    "data-toggle='tooltip' tooltip-trigger='outsideClick' tooltip-placement='auto left' uib-tooltip-html=\"monitor.deployDetail["+row.id+"]\"  >"+
                    "{{{'SUCEESS':'成功','FAIL':'失败','ERROR':'未部署'}[monitor.deployStatus["+row.id+"]]}}</span>";
            }},
            {name:"部署时间",field:"deployDate",class:"text-center",style:{"width":"8%"},compile:true,formatter:function(value,row){
                return "<span>{{monitor['deployDate']["+row.id+"]}}</span>";
            }},
        ],
        rowEvents:[
	        {
	        	title:"点击查看管理首页",class:'btn-info',icon:"fa fa-home",name:"首页",
	            click:function(row){
	            	if(row.isAdmin == 1)
	            		window.open("http://"+row.serverIp+":"+row.serverPort+"/console");
	            	else if(row.isAdmin == 0 && row.status == 1)
	            		window.open("http://"+row.serverIp+":"+row.serverPort+"/"+$scope.app.startContext);
	            	/*else if(row.isAdmin == 1 && row.status != 1)
	            		dbUtils.warning("管理首页未运行不可查看","提示");*/
	            	else if(row.isAdmin == 0 && row.status != 1)
	            		dbUtils.warning("应用首页未运行不可查看","提示");
	            }
	        },
	        {
	        	title:"日志",class:'btn-info',icon:"fa fa-file-text",name:"日志",
                isShowPopover:true,placement:"auto left",cancelName:"查看",cancelClass:"btn-info",okName:"下载",
                clickOk:function(row,event){
                    deployService.formDownloadFile(row);
                    $(event.target).parents(".popover").prev().click();
                },
                clickCancel:function(row,event){
                    dbUtils.checkClientStatus([$scope.monitor['agentStatus'][row.id]],(function () {
                        return function () {
                            /*执行函数 start*/
                            nowLog(row.serverId,row.serverIp,row.serverPort);
                            /*执行函数 end*/
                            $(event.target).parents(".popover").prev().click();
                        }
                    })())
                }
	        },{
	        	title:"查看dump文件",class:'btn-info',icon:"fa fa-desktop",name:"dump",
	            click:function(row){
	            	dbUtils.checkClientStatus([$scope.monitor['agentStatus'][row.id]],(function () {
                        return function () {
                        	/*执行函数 start*/
                        	showDump(row.port,row.deployFileId);
                          	/*执行函数 end*/
                        }
                    })())
	            }
	        },
        ],
        operationEvents:[{
        	name:"GIT打包",class:"btn-info",icon:"fa fa-inbox",id:"git",
            click:function(rows){
            	gitGo($scope.app);
            }
        },{
        	name:"部署",class:"btn-info",icon:"fa fa-send",id:"deploy",
            click:function(rows){
                if(deployService.weblogicCheck("deploy",rows,adminServerStatus)){
                	dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
							if($scope.app.fileType == "catalog")
								deployService.getAdminserverClient($scope);
                            /*let rowData = rows[0];
							for(let i=0;i<rows.length;i++){
							    if(i != 0){
                                    rowData.serverName += ("," + rows[i].serverName);
                                }
                            }*/
                        	var msIds = deployService.getManagedServerId(rows);
                        	queryFile(msIds,deployService.weblogicDataHandle(rows));
                          	/*执行函数 end*/
                        }
                    })())
                }
            }
        },{
        	name:"回退",class:"btn-info",icon:"fa fa-backward",id:"rollback",
            click:function(rows){
                if(deployService.weblogicCheck("deploy",rows,adminServerStatus)){
                	dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	var msIds = deployService.getManagedServerId(rows);
                        	queryRollbackFile(msIds,deployService.weblogicDataHandle(rows));
                          	/*执行函数 end*/
                        }
                    })())
                }
            }
        },{
        	name:"启动",class:"btn-info",icon:"fa fa-power-off",id:"start",
            click:function(rows){
                if(deployService.weblogicCheck("start",rows,adminServerStatus)){
                	dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	deploy("start",deployService.weblogicDataHandle(rows));
                          	/*执行函数 end*/
                        }
                    })())
                }
            }
        },{
        	name:"重启",class:"btn-info",icon:"fa fa-repeat",id:"restart",
            click:function(rows){
            	if(deployService.weblogicCheck("restart",rows,adminServerStatus)){
            		dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	deploy("restart",deployService.weblogicDataHandle(rows));
                          	/*执行函数 end*/
                        }
                    })())
            	}
            }
        },{
        	name:"停止",class:"btn-danger",icon:"fa fa-stop",id:"stop",
            click:function(rows){
                if(deployService.weblogicCheck("stop",rows,adminServerStatus)){
                	dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	deploy("stop",deployService.weblogicDataHandle(rows));
                          	/*执行函数 end*/
                        }
                    })())
                }
            }
        },{
        	name:"生成dump文件",class:"btn-info",icon:"fa fa-desktop",id:"dump",
	        click:function(rows){
                if(deployService.weblogicCheck("dump",rows,adminServerStatus)){
                	dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	deploy("dump",deployService.weblogicDataHandle(rows));
                          	/*执行函数 end*/
                        }
                    })())
                }
	        }
	    }],
        settings:{
            pagerHide:false,
            filterId: "weblogicDeployFilter",
            operationId: "deployOperationId",
            uploadId: "deployUploadId",
            theadId: "deployTheadId",
//            buttomAlign:"right",
//            filterId:true,
            showCheckBox:true,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-139-41-43-56-20-34)+"px",
            },
        },
        afterReload:function(){
            $scope.$emit("pageLoadSuccess",{});//driver.js
        	dbUtils.getClientStatusInfo(rows,setAgentStatus,$scope);
        	healthCheckHandle();
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight7+"px";
        	},200)
        },
        beforeReload:function(){}
    }
    
    //tomcat表格
    var tomcatTable = {
        url:"/app/preview/"+$scope.appId+"/tomcats",
        showUpload: true,
        pageSql:true,
        isRefreshStatus:true,
        headers: [
            {name:"tomcat版本",field:"version",style:{"width":"15%"}},
            {name:"IP",field:"host",style:{"width":"12%"}},
            {name:"端口",field:"port",style:{"width":"8%"}},
            {name:"部署版本",field:"currentVersion",style:{"width":"8%"}},
            {name:"状态",field:"status",style:{"width":"8%"},compile:true,formatter:function(value,row){
                $scope.monitor['status'][row.id] = value;//首次初始化该状态
                return "<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-light\",1:\"text-success\",2:\"text-danger\"}[monitor.status["+row.id+"]]'></i>{{{0:\"待部署\",1:\"运行中\",2:\"已停止\"}[monitor.status["+row.id+"]]}}</span>"
            }},
            {name:"健康状态",field:"healthStatus",style:{"width":"8%"},class:"text-center",compile:true,formatter:function(value,row){
                rowsObj[row.id] = row;
            	$scope.monitor['healthStatus'][row.id] = 2;
            	var url = "http://"+row.host+":"+row.port+"/"+$scope.app.startContext;
            	healthCheckList.push({id:row.id, "url": url})
            	return "<span ng-if='monitor.healthStatus["+row.id+"] == 2'><i class='fa fa-spin fa-spinner'></i></span>" +
            			"<span ng-if='monitor.healthStatus["+row.id+"] != 2'><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-danger\",1:\"text-success\"}[monitor.healthStatus["+row.id+"]]'></i>{{{0:\"不可用\",1:\"可用\"}[monitor.healthStatus["+row.id+"]]}}</span>";
            }},
            {name:"Agent",field:"agentStatus",class:"text-center",style:{"width":"8%"},compile:true,formatter:function(value,row){
            	rows.push(row);
            	$scope.monitor['agentStatus'][row.id] = 0;
            	return "<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-danger\",1:\"text-success\"}[monitor.agentStatus["+row.id+"]]'></i>{{{0:\"DOWN\",1:\"UP\"}[monitor.agentStatus["+row.id+"]]}}</span>";
            }},
            {name:"部署情况",class:"text-center",style:{"width":"8%"},hideSort:true,compile:true,formatter:function(value,row){
            	if(row.status == 0){
            		$scope.monitor['deployStatus'][row.id] = "ERROR";
               	 	$scope.monitor['deployDetail'][row.id] =  "部署情况：" + "未部署" ;
            	}else{
            		deployService.checkDeployStatus($scope.appId,row.tomcatId).then(function(response){
                        if(response.code== SUCCESS_CODE){
                            let date = response.data && response.data.time ? response.data.time : '--';
                            row.deployDate = date;
                            $scope.monitor["deployDate"][row.id] = date;
                        	let tooltip_template = "部署情况：" + response.data.deploy_status + "\r\n</br>" + "部署文件：" + response.data.project_file  + "\r\n</br>" +
													"部署文件版本：" + response.data.version  + "\r\n</br>" + "部署时间：" + response.data.time;
                        	 if(response.data.deploy_status == "部署成功"){
                        		 $scope.monitor['deployStatus'][row.id] = "SUCEESS";
                        		 $scope.monitor['deployDetail'][row.id] = tooltip_template;
                        	 }else if(response.data.deploy_status == "部署失败"){
                        		 $scope.monitor['deployStatus'][row.id] = "FAIL";
                        		 $scope.monitor['deployDetail'][row.id] = tooltip_template;
                        	 }else{
                        		 $scope.monitor['deployStatus'][row.id] = "ERROR";
                        		 $scope.monitor['deployDetail'][row.id] =  "部署情况：" + "未部署" ;
                        	 }
                        }else{
                        	 $scope.monitor['deployStatus'][row.id] = "ERROR";
                        	 $scope.monitor['deployDetail'][row.id] =  "部署情况：" + "未部署" ;
                        }
                    });
            	}
                return "<span class='badge sm-br cusor-pointer' ng-class=\"{'SUCEESS':'bg-success','FAIL':'bg-danger','ERROR':'bg-light'}[monitor.deployStatus["+row.id+"]]\"   "+
                    "data-toggle='tooltip' tooltip-trigger='outsideClick' tooltip-placement='auto left' uib-tooltip-html=\"monitor.deployDetail["+row.id+"]\"  >"+
                    "{{{'SUCEESS':'成功','FAIL':'失败','ERROR':'未部署'}[monitor.deployStatus["+row.id+"]]}}</span>";
            }},
            {name:"部署时间",field:"deployDate",class:"text-center",style:{"width":"8%"},compile:true,formatter:function(value,row){
                return "<span>{{monitor['deployDate']["+row.id+"]}}</span>";
            }},
        ],
        rowEvents:[ 
        	{
            	title:"点击查看应用首页",class:'btn-info',icon:"fa fa-home",name:"首页",
                click:function(row){
                	if(row.status == 1)
                		window.open("http://"+row.host+":"+row.port+"/"+$scope.app.startContext);
                	else
                		dbUtils.warning("应用首页未运行不可查看","提示");
                }
            },
            {
	        	title:"日志",class:'btn-info',icon:"fa fa-file-text",name:"日志",
                isShowPopover:true,placement:"auto left",cancelName:"查看",cancelClass:"btn-info",okName:"下载",
                clickOk:function(row,event){
                    deployService.formDownloadFile(row);
                    $(event.target).parents(".popover").prev().click();
                },
                clickCancel:function(row,event){
                    dbUtils.checkClientStatus([$scope.monitor['agentStatus'][row.id]],(function () {
                        return function () {
                            /*执行函数 start*/
                            nowLog(row.tomcatId,row.host,row.port);
                            /*执行函数 end*/
                            $(event.target).parents(".popover").prev().click();
                        }
                    })())
                }
	        },{
	        	title:"查看dump文件",class:'btn-info',icon:"fa fa-desktop",name:"dump",
	            click:function(row){
	            	dbUtils.checkClientStatus([$scope.monitor['agentStatus'][row.id]],(function () {
                        return function () {
                        	/*执行函数 start*/
                        	showDump(row.port,row.deployFileId);
                          	/*执行函数 end*/
                        }
                    })())
	            }
	        },
        ],
        operationEvents:[
    	{
        	name:"GIT打包",class:"btn-info",icon:"fa fa-inbox",id:"git",
            click:function(rows){
            	gitGo($scope.app);
            }
        },{
        	name:"部署",class:"btn-info",icon:"fa fa-send",id:"deploy",
            click:function(rows){
            	var ids = deployService.tomcatDataHandle(rows);
            	if(deployService.tomcatCheck("deploy",rows)){
            		dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	var tomcatIds = deployService.getTomcatId(rows);
                        	queryFile(tomcatIds,ids);
                          	/*执行函数 end*/
                        }
                    })())
            	}
            }
        },{
        	name:"回退",class:"btn-info",icon:"fa fa-backward",id:"rollback",
            click:function(rows){
            	var ids = deployService.tomcatDataHandle(rows);
            	if(deployService.tomcatCheck("deploy",rows)){
            		dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	var tomcatIds = deployService.getTomcatId(rows);
                        	queryRollbackFile(tomcatIds,ids);
                          	/*执行函数 end*/
                        }
                    })())
            	}
            }
        },{
        	name:"启动",class:"btn-info",icon:"fa fa-power-off",id:"start",
            click:function(rows){
            	var ids = deployService.tomcatDataHandle(rows);
            	if(deployService.tomcatCheck("start",rows)){
            		dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	deploy("start",ids);
                          	/*执行函数 end*/
                        }
                    })())
            	}
            }
        },{
        	name:"重启",class:"btn-info",icon:"fa fa-repeat",id:"restart",
            click:function(rows){
            	var ids = deployService.tomcatDataHandle(rows);
            	if(deployService.tomcatCheck("restart",rows)){
            		dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	deploy("restart",ids);
                          	/*执行函数 end*/
                        }
                    })())
            	}
            }
        },{
        	name:"停止",class:"btn-danger",icon:"fa fa-stop",id:"stop",
            click:function(rows){
                var ids = deployService.tomcatDataHandle(rows);
                if(deployService.tomcatCheck("stop",rows)){
                	dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	deploy("stop",ids);
                          	/*执行函数 end*/
                        }
                    })())
                }
            }
        },{
        	name:"生成dump文件",class:"btn-info",icon:"fa fa-desktop",id:"dump",
	        click:function(rows){
	        	var ids = deployService.tomcatDataHandle(rows);
                if(deployService.tomcatCheck("dump",rows)){
                	dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	deploy("dump",ids);
                          	/*执行函数 end*/
                        }
                    })())
                }
	        }
	    }],
        settings:{
        	filterId: "tomcatDeployFilter",
            operationId: "deployOperationId",
            uploadId: "deployUploadId",
            theadId: "deployTheadId",
            pagerHide:false,showCheckBox:true,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-139-41-43-56-20-34)+"px",
            },
        },
        afterReload:function(){
            $scope.$emit("pageLoadSuccess",{});//driver.js
        	dbUtils.getClientStatusInfo(rows,setAgentStatus,$scope);
        	healthCheckHandle();
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight7+"px";
        	},200)
        },
        beforeReload:function(){}
    }
    
    //springBoot的表格
    var springBootTable = {
        url:"/app/preview/"+$scope.appId+"/springboot",
        showUpload: true,
        pageSql:true,
        isRefreshStatus:true,
        headers: [
            {name:"主机名",field:"hostName",style:{"width":"15%"}},
            {name:"IP",field:"host",style:{"width":"12%"},compile:true,formatter:function(value,row){
            	row.host = value;//首次初始化该状态
                return value;
            }},
            {name:"端口",field:"port",style:{"width":"6%"}},
            {name:"部署版本",field:"currentVersion",style:{"width":"8%"}},
            {name:"状态",field:"status",style:{"width":"8%"},compile:true,formatter:function(value,row){
                $scope.monitor['status'][row.id] = value;//首次初始化该状态
                return "<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-light\",1:\"text-success\",2:\"text-danger\"}[monitor.status["+row.id+"]]'></i>{{{0:\"待部署\",1:\"运行中\",2:\"已停止\"}[monitor.status["+row.id+"]]}}</span>"
            }},
            {name:"健康状态",field:"healthStatus",class:"text-center",style:{"width":"8%"},compile:true,formatter:function(value,row){
                rowsObj[row.id] = row;
            	$scope.monitor['healthStatus'][row.id] = 2;
            	var url = "http://"+row.host+":"+row.port+"/"+$scope.app.startContext;
            	healthCheckList.push({id:row.id, "url": url})
            	return "<span ng-if='monitor.healthStatus["+row.id+"] == 2'><i class='fa fa-spin fa-spinner'></i></span>" +
            			"<span ng-if='monitor.healthStatus["+row.id+"] != 2'><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-danger\",1:\"text-success\"}[monitor.healthStatus["+row.id+"]]'></i>{{{0:\"不可用\",1:\"可用\"}[monitor.healthStatus["+row.id+"]]}}</span>";
            }},
            {name:"Agent",field:"agentStatus",class:"text-center",style:{"width":"8%"},compile:true,formatter:function(value,row){
            	rows.push(row);
            	$scope.monitor['agentStatus'][row.id] = 0;
            	return "<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-danger\",1:\"text-success\"}[monitor.agentStatus["+row.id+"]]'></i>{{{0:\"DOWN\",1:\"UP\"}[monitor.agentStatus["+row.id+"]]}}</span>";
            }},
            {name:"部署情况",class:"text-center",style:{"width":"8%"},hideSort:true,compile:true,formatter:function(value,row){
            	if(row.status == 0){
            		$scope.monitor['deployStatus'][row.id] = "ERROR";
               	 	$scope.monitor['deployDetail'][row.id] =  "部署情况：" + "未部署" ;
            	}else{
            		 deployService.checkDeployStatus($scope.appId,row.id).then(function(response){
                         let date = response.data && response.data.time ? response.data.time : '--';
                         row.deployDate = date;
                         $scope.monitor["deployDate"][row.id] = date;
                         if(response.code== SUCCESS_CODE){
      	               		let tooltip_template = "部署情况：" + response.data.deploy_status + "\r\n</br>" + "部署文件：" + response.data.project_file  + "\r\n</br>" +
      	               								"部署文件版本：" + response.data.version  + "\r\n</br>" + "部署时间：" + response.data.time;
      	               		if(response.data.deploy_status == "部署成功"){
      	               			$scope.monitor['deployStatus'][row.id] = "SUCEESS";
	      	               		$scope.monitor['deployDetail'][row.id] = tooltip_template;
      	               		}else if(response.data.deploy_status == "部署失败"){
      	               			$scope.monitor['deployStatus'][row.id] = "FAIL";
	      	               		$scope.monitor['deployDetail'][row.id] = tooltip_template;
      	               		}else{
      	               			$scope.monitor['deployStatus'][row.id] = "ERROR";
      	               			$scope.monitor['deployDetail'][row.id] =  "部署情况：" + "未部署" ;
      	               		}
      	                 }else{
      	            	   	$scope.monitor['deployStatus'][row.id] = "ERROR";
      	            	   	$scope.monitor['deployDetail'][row.id] =  "部署情况：" + "未部署" ;
      	                 }
      	           });
            	}
	           return "<span class='badge sm-br cusor-pointer' ng-class=\"{'SUCEESS':'bg-success','FAIL':'bg-danger','ERROR':'bg-light'}[monitor.deployStatus["+row.id+"]]\"   "+
	               "data-toggle='tooltip' tooltip-trigger='outsideClick' tooltip-placement='auto left' uib-tooltip-html=\"monitor.deployDetail["+row.id+"]\"  >"+
	               "{{{'SUCEESS':'成功','FAIL':'失败','ERROR':'未部署'}[monitor.deployStatus["+row.id+"]]}}</span>";
	        }},
            {name:"部署时间",field:"deployDate",class:"text-center",style:{"width":"8%"},compile:true,formatter:function(value,row){
                return "<span>{{monitor['deployDate']["+row.id+"]}}</span>";
            }},
        ],
        rowEvents:[
           {
        	    title:"点击查看应用首页",class:'btn-info',icon:"fa fa-home",name:"首页",
	            click:function(row){
	            	if(row.status == 1)
	            		window.open("http://"+row.host+":"+row.port+"/"+$scope.app.startContext);
	            	else
	            		dbUtils.warning("应用首页未运行不可查看","提示");
	            }
	        },
	        {
	        	title:"日志",class:'btn-info',icon:"fa fa-file-text",name:"日志",
                isShowPopover:true,placement:"auto left",cancelName:"查看",cancelClass:"btn-info",okName:"下载",
                clickOk:function(row,event){
                    deployService.formDownloadFile(row);
                    $(event.target).parents(".popover").prev().click();
                },
                clickCancel:function(row,event){
                    dbUtils.checkClientStatus([$scope.monitor['agentStatus'][row.id]],(function () {
                        return function () {
                            /*执行函数 start*/
                            nowLog(row.id,row.host,row.port);
                            /*执行函数 end*/
                            $(event.target).parents(".popover").prev().click();
                        }
                    })())
                }
	        },
	        {
	        	title:"查看dump文件",class:'btn-info',icon:"fa fa-desktop",name:"dump",
	            click:function(row){
	            	dbUtils.checkClientStatus([$scope.monitor['agentStatus'][row.id]],(function () {
                        return function () {
                        	/*执行函数 start*/
                        	showDump(row.port,row.deployFileId);
                          	/*执行函数 end*/
                        }
                    })())
	            }
	        },
        ],
        operationEvents:[
    	{
        	name:"GIT打包",class:"btn-info",icon:"fa fa-inbox",id:"git",
	        click:function(rows){
	        	gitGo($scope.app);
	        }
	    },{
        	name:"部署",class:"btn-info",icon:"fa fa-send",id:"deploy",
            click:function(rows){
            	var sbData = deployService.springBootDataHandle(rows);
                if(deployService.springBootCheck("deploy",rows)){
                	dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	var sbIds = deployService.getSpringBootId(rows);
                        	queryFile(sbIds,sbData);
                          	/*执行函数 end*/
                        }
                    })())
                }
            }
        },{
        	name:"回退",class:"btn-info",icon:"fa fa-backward",id:"rollback",
            click:function(rows){
//            	var sbData = deployService.springBootDataHandle(rows);
                if(deployService.springBootCheck("deploy",rows)){
                	dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	var sbIds = deployService.getSpringBootId(rows);
                        	queryRollbackFile(sbIds,rows);
                          	/*执行函数 end*/
                        }
                    })())
                }
            }
        },{
        	name:"启动",class:"btn-info",icon:"fa fa-power-off",id:"start",
            click:function(rows){
            	var sbData = deployService.springBootDataHandle(rows);
                if(deployService.springBootCheck("start",rows)){
                	dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	deploy("start",sbData);
                          	/*执行函数 end*/
                        }
                    })())
                }
            }
        },{
        	name:"重启",class:"btn-info",icon:"fa fa-repeat",id:"restart",
            click:function(rows){
            	var sbData = deployService.springBootDataHandle(rows);
                if(deployService.springBootCheck("restart",rows)){
                	dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	deploy("restart",sbData);
                          	/*执行函数 end*/
                        }
                    })())
                }
            }
        },{
        	name:"停止",class:"btn-danger",icon:"fa fa-stop",id:"stop",
            click:function(rows){
                var sbData = deployService.springBootDataHandle(rows);
                if(deployService.springBootCheck("stop",rows)){
                	dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                        return function () {
                        	/*执行函数 start*/
                        	deploy("stop",sbData);
                          	/*执行函数 end*/
                        }
                    })())
                }
            }
        },{
	    	name:"生成dump文件",class:"btn-info",icon:"fa fa-desktop",id:"dump",
	        click:function(rows){
	        	 var sbData = deployService.springBootDataHandle(rows);
	             if(deployService.springBootCheck("dump",rows)){
	            	 dbUtils.checkClientStatus(rows.map(item => { return $scope.monitor['agentStatus'][item.id] }),(function () {
                 		return function () {
                        	/*执行函数 start*/
                        	deploy("dump",sbData);
                          	/*执行函数 end*/
                        }
	                 })())
	            }
	        }
	    }],
        settings:{
        	filterId: "springbootFilter",
            operationId: "deployOperationId",
            uploadId: "deployUploadId",
            theadId: "deployTheadId",
            pagerHide:false,showCheckBox:true,
	        tbodyStyle:{
	        	"height":(Number($rootScope._screenProp.contentHeight)-139-41-43-56-20-34)+"px",
	        },
        },
        afterReload:function(){
            $scope.$emit("pageLoadSuccess",{});
        	dbUtils.getClientStatusInfo(rows,setAgentStatus,$scope);
        	healthCheckHandle();
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight7+"px";
        	},200)
        },
        beforeReload:function(){}
    }
    
//  	初始加载数据和表格
    beforeLoad();
    
    //健康状态检查
    function healthCheckHandle(){
    	deployService.healthCheckAll(healthCheckList).then(function(response){
    		if(response.code== SUCCESS_CODE){
    			var respData = response.data;
				for (var item of healthCheckList) {
				    rowsObj[item.id].healthStatus = respData[item.id];
					$scope.monitor['healthStatus'][item.id] = respData[item.id];
		    	}
    		}else{}
    	});
    }
    
    function setAgentStatus(response){
    	for(let row of rows){
			let status = response.data[row.deployFileId].status == "UP" ? 1 : 0;
			row.agentStatus = status;
			$scope.monitor['agentStatus'][row.id] = status;
		}
	}
    
    //展示dump文件
    function showDump(port,clientId){
    	dbUtils.openModal('tpl/deploy/dump_list.html','DumpListController',"lg","",{urlParams:{appName:$scope.app.name,port:port,clientId:clientId},"clientId":clientId},function (data) {},true);
    }
    
    //git
    function gitGo(app){
    	dbUtils.openModal('tpl/deploy/git_package.html','GitPackageController',"lg","",{"app":angular.copy(app),"version":app.currentPackVersion},function (data) {
       	 	if(data){
       	 		dbUtils.get("/app/preview/"+$scope.appId).then(function(response){
        			$scope.app = response.data;
        		})
       	 	}
    	},true);
    }
    
    //实时日志
    function nowLog(id,ip,port){
        deployService.nowLog($scope.appId,id,500).then(function(response){
            if(response.code == SUCCESS_CODE){
            	window.open("tpl/log/newTabLogWs.html?logId="+response.data+"&title="+($scope.app.name+"_"+ip+"_"+port));
            }else{
                dbUtils.error(response.message,"提示");
            }
        })
    }

    //上传文件处理
    $scope.uploadHandle = function(file){
    	dbUtils.openModal('tpl/deploy/file_version.html','FileVersionController',"lg","",{"file":file, "version":$scope.app.currentPackVersion,"app":angular.copy($scope.app),"appType":0},function (data) {
        	if(data){
        		upload(file,data.version,data.description);
        	}
        });
    }
    
    function upload(file,version,description){
    	var formData = new FormData();
        formData.append("file",file);
        var deployType = 2;
        $scope.loading = true;
        $http({
            method:'post',
            url:'/deploy/uploadFile?appId='+$scope.appId+'&deployType='+deployType+"&currentPackVersion="+version+"&description="+description,
            data:formData,
            headers:{'Content-Type':undefined} ,
            transformRequest:angular.identity//浏览器会帮我们把Content-Type 设置为 multipart/form-data.
        }).success(function(response){
        	 $scope.loading = false; 
        	 if(response.code == SUCCESS_CODE){
        		 dbUtils.success("上传成功","上传");
                 $.ajax({
                     url: "/app/preview/"+$scope.appId,type: "get",async: false,dataType: 'json',
                     success: function(response){
                         if(response.code == SUCCESS_CODE){
                             $scope.app = response.data;
                         }else{
                             dbUtils.error(response.message,"提示");
                         }
                     },
                 })
             }else if(response.code == 1110005){
                 dbUtils.error("请先部署之前上传的文件","提示");
             }else if(response.code == 1503){
                 dbUtils.error("文件路径匹配失败","提示");
             }else if(response.code == 1110007){
                 dbUtils.error("文件上传失败，请稍后再试","提示");
             }else{
                 dbUtils.error(response.message,"提示");
             }
        	 $scope.table.operations.reloadData();
        }).error(function(data){
        	$scope.loading = false; 
        	if(data == null){
        		dbUtils.error("上传失败","上传");
        	}else{
        		dbUtils.error(data.message,"提示");
        	}
        });
    }

    //查询文件
    function queryFile(msIds,data){
        dbUtils.openModal('tpl/deploy/deploy_file_single.html','FileSingleChooseDeployController',"lg","",{"data":data,"url":"/deploy/getDeployFiles","urlParams":{appId:$scope.appId, deployType:$scope.app.middlewareType, fileType:$scope.app.fileType, msIds:msIds.join()}, "type":'deploy', "adminserverClient":$scope.adminserverClient, "app":$scope.app},function (data) {
    		if(data){
            	deploy("deploy",data);
        	}
        },false);
    }
    
    //查询回滚文件
    function queryRollbackFile(msIds,data){
    	 dbUtils.openModal('tpl/deploy/rollback_file.html','RollbackFileChooseController',"lg","",{"data":data,"url":"/deploy/getRollbackFileList","urlParams":{appId:$scope.appId,deployType:$scope.app.middlewareType,fileType:$scope.app.fileType,info:JSON.stringify(data)},"type":'rollback'},function (data) {
     		if(data){
             	deploy("rollback",data);
         	}
         },true);
	}
    
    //按钮
    function deploy(action,instances){
        if(instances && instances.length != 0){
            dbUtils.confirm("确定继续操作？",function(){
				deployHandle(action,instances);
            },function(){},true);
        }else{
        	dbUtils.error("请选择被操作对象","提示");
        }
    }
    
    //部署/回滚成功后是否重新启动
    function deployAgain(action,instances){
    	action = "restart";
        if(instances && instances.length != 0){
            dbUtils.confirm("是否立即重启",function(){
				deployHandle(action,instances);
            },function(){
            	$scope.table.operations.reloadData();
            	queryCountNum(); 
            });
        }else{
        	dbUtils.error("请选择被操作对象","提示");
        }
    }

    //weblogic,springboot,tomcat
    function deployHandle(action,instances){
    	deployService.deploy({action:action,info:JSON.stringify(instances),appId:$scope.appId,parallel:$scope.parallel}).then(function(response){
            if(response.code == SUCCESS_CODE){
        		echoLog.show(response.data,function(){
        			if(action == "deploy" || action == "rollback"){
                		deployService.deployCheckFile1({action:action,info:JSON.stringify(instances),appId:$scope.appId,parallel:$scope.parallel}).then(function(response){
                        	if (response.code == SUCCESS_CODE) {
                        		if(action == "rollback"){
                        			dbUtils.successing("回滚成功","提示",10000);
                        		}else{
                        			dbUtils.successing("部署成功","提示",10000);
                        		}
                        		deployAgain(action,instances);
                            } else {
                                dbUtils.error(response.message,"提示");
                            }
                        })
        			}else{
        				dbUtils.success("操作成功","提示");
        				$scope.table.operations.reloadData();
        				queryCountNum();
        			}
                });
            }else{
                dbUtils.error(response.message,"提示");
            }
        })
    }

    $scope.goBackSelectApp = function (){
    	$scope.$emit("deleteCurrentTab",{});
    	$state.go(stateParams.fromWhere)
    }

}]);

//deploy按钮 文件选择  表格单选框
let FileSingleChooseDeployController = app.controller('FileSingleChooseDeployController', ['dbUtils','$scope','$modalInstance', '$state','source','DeployService','$http',
    function (dbUtils,$scope,$modalInstance, $state,source,deployService,$http) {

    $scope.deployData = source.data;//发布页选中数据
    $scope.type = source.type;
    $scope.adminserverClient = source.adminserverClient;
    $scope.app = source.app;
    $scope.params = {editMode: false};
    let url = source.url;
    let urlParams = source.urlParams;

    var str = [];
    for (var p in urlParams) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(urlParams[p]));
    }
    url = url + "?" + str.join("&");
    //idToVersionObj保存id与row，idArr按顺序添加id
    let idToVersionObj = {},idArr = [],diffVersionObj = {};

    /*ui-base-table*/
    $scope.fileChooseUrlParams = {url:url, urlBody:{}, method:"get"};
    $scope.tooltipData = {"status":{}};
    $scope.queryParams = {isPagination:true,postgroundPagination:false,pageNumber: 1,pageSize:10,prevPageDisabled: 'disabled',nextPageDisabled: 'disabled'};
    $scope.tableParams = {loading:true, bodyStyle:{"max-height": "500px"} };
    $scope.fileChooseHeaders = [
        {name:"文件名",field:"fileName",style:{width:"25%"}},
        {name:"状态",field:"isDeploy",style:{width:"10%"},compile:true,formatter:function(value,row){
            if(row.isDeploy == 0){//存在
                idToVersionObj[row.id] = row;
                idArr.push(row.id);
            }
            $scope.tooltipData.status[row.id] = row.isDeploy;
            return "<span class='label' ng-class='{0:\"bg-success\",1:\"bg-danger\"}[tooltipData.status["+row.id+"]]'>{{{0:\"存在\",1:\"已删除\"}[tooltipData.status["+row.id+"]]}}</span>";
            // return "<span class='label' data-toggle='popover' title=\"Popover title\" content='input框' onmouseenter='$(this).popover(\"show\")' data-placement=\"left\" ng-class='{0:\"bg-success\",1:\"bg-danger\"}["+row.isDeploy+"]'>{{{0:\"存在\",1:\"已删除\"}["+row.isDeploy+"]}}</span>";
        }},
        {name:"上传人",field:"uploadUser",style:{width:"10%"}},
        {name:"版本",field:"version",style:{width:"10%"}},
        {name:"创建时间",field:"createTime",style:{width:"20%"}},
        {name:"版本描述",field:"description",style:{width:"10%"},compile:true,formatter:function(value,row){
            let content = row.description?row.description:'--';
            return content;
        }}
    ];
    $scope.fileChooseRowEvents = [{
        class:"btn-default",showIcon:true,icon:"iconfont icon-diff",title:"文件对比",name:" ",
        isShow:function(row){
            let index = idArr.indexOf(row.id);
            //存在的最后一个版本、id找不到的、已删除的不用比较
            if(idArr.length-1 == index || index == -1 || row.isDeploy == 1)
                return false;
            diffVersionObj[row.id] = {deployed_version:idToVersionObj[idArr[index+1]].version};
            return true;
        },
        click: function(row){
            if(!diffVersionObj[row.id].deployed_version)
                diffVersionObj[row.id].deployed_version = "";
            let params = {agent_username:$scope.adminserverClient.username, project_file:row.fileName, project_name:$scope.app.name, deploying_version:row.version, deployed_version:diffVersionObj[row.id].deployed_version, app_id:$scope.app.id}

            $http({
                method : "post",
                url : "/deploy/createDiffHtml?json="+JSON.stringify(params),
                data : JSON.stringify($scope.adminserverClient),
                responseType: "blob"   //注意此参数
            }).success(function(response, status, headers, config){
                var blob = response;
                if(blob.size > 0){
                    var fileName = headers("Content-Disposition").split(";")[1].split("filename=")[1];
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    a.download = fileName;
                    a.href = URL.createObjectURL(blob);
                    a.click();
                }else{
                    dbUtils.error("下载失败 "+response);
                }
            }).error(function(response, status, headers, config){
                dbUtils.error("下载失败 "+response);
            });
        }
    }];
    /*ui-base-table end*/

    //更新应用表excluideFiles字段
    $scope.saveExcludeFile = function(){
        if(!$scope.app.excludeFiles)
            $scope.app.excludeFiles = null;

        dbUtils.put("/app/saveApp?envs=[]",$scope.app).then(function (response) {
            if(response.code == SUCCESS_CODE){
                $scope.params.editMode = false;
                dbUtils.success("排除文件保存成功");
            }else{
                dbUtils.error(response.message);
            }
        },function (error) {});
    }

    $scope.$on("refresh",function () {
        initData();
    })

    $scope.submitApply = function(){
        $scope.$broadcast('getSelectedData',{})
    }

    //获取选中数据处理并返回
    $scope.$on('sendSelectedData',function(event,selectedData){
        if(!selectedData || selectedData.length == 0 || selectedData.length > 1){
            dbUtils.warning("请选择一条数据","提示");
            return;
        }
        selectedData = selectedData[0];
        if(selectedData.isDeploy == 1){//已删除禁止部署
            dbUtils.warning("禁止部署已删除版本","提示");
            return;
        }

        var id = selectedData.id;
        if($scope.type == 'rollback'){
            id = selectedData.fileName;
        }
        var data = deployData(id,$scope.deployData);
        $modalInstance.close(data);
    })

    function excludeFile(row){
        /*dbUtils.openModal('tpl/deploy/exclude_file.html','ExcludeFileController',"lg","modal-larger",{"row":row, "adminserverClient":$scope.adminserverClient, "appName":$scope.app.name},function (data) {
            if(data){
                console.log("exclude file success")
            }
        },true);*/
        dbUtils.openModal('tpl/deploy/exclude_file_simple.html',function ($scope,$modalInstance, $state,source) {
            $scope.excludeFiles = source.excludeFiles;

            $scope.submitApply = function(){
                $modalInstance.close($scope.excludeFiles);
            }

            $scope.cancel = function () {
                $modalInstance.close(false);
            };
        },"md","",{excludeFiles:$scope.app.excludeFiles},function (data) {
            if(data){
                $scope.app.excludeFiles = data;
            }
        });
    }

    function deployData(id,date){
        var deployDate = new Array();
        for (var i in date){
            var obj = new Object();
            obj = date[i];
            if($scope.type == 'rollback'){
                obj.fileVersion = id;
            }else{
                obj.fileId = id;
            }
            deployDate.push(obj);
        }
        return deployDate;
    }

    $scope.cancel = function () {
        $modalInstance.close(false);
    };

}]);

//rollback文件选择
let RollbackFileChooseController = app.controller('RollbackFileChooseController', ['dbUtils','$scope','$modalInstance', '$state','source','DeployService',
                                        function (dbUtils,$scope,$modalInstance, $state,source,deployService) {

    $scope.deployData = source.data;//发布页选中数据
    $scope.type = source.type;//rollback
    $scope.adminserverClient = source.adminserverClient;
    $scope.app = source.app;
    $scope.params = {editMode: false};
    let url = source.url;
    let urlParams = source.urlParams;
    
    var str = [];
    for (var p in urlParams) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(urlParams[p]));
    }
    url = url + "?" + str.join("&");

    /*ui-base-table*/
    $scope.fileChooseUrlParams = {url:url, urlBody:{}, method:"get"}
    $scope.queryParams = {isPagination:true,postgroundPagination:false,pageNumber: 1,pageSize:10,prevPageDisabled: 'disabled',nextPageDisabled: 'disabled'};
    $scope.tableParams = {loading:true, bodyStyle:{"max-height": "500px"} };
    //回滚的表格
    $scope.fileChooseHeaders = [
        {name:"版本号",field:"fileName",style:{width:"20%"}},{name:"上传人",field:"uploadUser",style:{width:"15%"}},{name:"上传时间",field:"time",style:{width:"20%"}},
        {name:"版本描述",field:"description",style:{width:"45%"},compile:true,formatter:function(value,row){
            return "<textarea  class='form-control' rows='2' ng-model='file.description' readonly='readonly'/></textarea>";
        }}
    ];

    /*ui-base-table end*/
    
    $scope.submitApply = function(){
    	$scope.$broadcast('getSelectedData',{})
    }

    //获取选中数据处理并返回
    $scope.$on('sendSelectedData',function(event,selectedData){
    	if(!selectedData || selectedData.length == 0 || selectedData.length > 1){
			dbUtils.warning("请选择一条数据","提示");	
			return;
		}
    	selectedData = selectedData[0];
    	if(selectedData.isDeploy == 1){//已删除禁止部署
    		dbUtils.warning("禁止部署已删除版本","提示");	
			return;
    	}

		var id = selectedData.id;
		if($scope.type == 'rollback'){
			id = selectedData.fileName;
		}
		var data = deployData(id,$scope.deployData);
		$modalInstance.close(data);
    })

    function deployData(id,date){
    	var deployDate = new Array();
		for (var i in date){
	    	var obj = new Object();
	    	obj = date[i];
	    	if($scope.type == 'rollback'){
	    		obj.fileVersion = id;
	        }else{
	        	obj.fileId = id;
	        }
	    	deployDate.push(obj);
		}
        return deployDate;
    }

    $scope.cancel = function () {
    	$modalInstance.close(false);
    };
    
}]);

//文件上传
let FileVersionController = app.controller('FileVersionController', ['dbUtils','$rootScope','$scope','$modalInstance','$modal', '$state','source','DeployService', 'CustomManagerService',
                                        function (dbUtils,$rootScope,$scope,$modalInstance,$modal, $state,source,deployService,customManagerService) {

    $scope.nowVersion = source.version;
    $scope.app = source.app;
    $scope.appType = source.appType;
    $scope.version = source.version;
    $scope.file = source.file;

    $scope.versionType = 1;
    $scope.fileDescription = "";
    $scope.isDelete = 0;
    $scope.versionNow = $scope.app.versionNum;
    $scope.versionHoldNum = 1;
    $scope.thead = {};
    $scope.thead.allrow = false;//初始化为不选
    $scope.targetServers = [];
    $scope.middlewareArr = [];

    $scope.isFileNameCheckSucc = deployService.fileCheck($scope);
    deployService.getVersionNum($scope);
    
    $scope.versionCheck = function (n,Z) {
    	deployService.versionCheck($scope,n,Z);
    };
    
	//当为自定义应用时，加载主机和中间件信息
    if($scope.appType == 1){
    	$scope.health = {};
        $scope.healthDetail = {};
    	deployService.getAppRelationClient($scope.app.id,{}).then(function(response){
    		if(response.code == SUCCESS_CODE){
    			//获取主机并去重
    			$scope.client = deployService.addStatusForClient($scope,response.data);
    			$scope.client = deployService.distinctClient($scope);
    			$scope.targetServers = $scope.client;
    			
    			if($scope.app.middlewareType == "nginx"){
    	    		customManagerService.getNginxList($scope,$scope.getMiddlewareListAndSelected);//获取nginx列表,并在回调函数中获取到被选中的nginx
    	    	}else if($scope.app.middlewareType == "weblogic"){
    	    		customManagerService.getWeblogicList($scope,$scope.getMiddlewareListAndSelected);
    	    	}else if($scope.app.middlewareType == "tomcat"){
    	    		customManagerService.getTomcatList($scope,$scope.getMiddlewareListAndSelected);
    	    	}
    		}else{
    			dbUtils.error(response.message,"错误");
    		}
    	})
   	}

    //获取中间件列表和选中的中间件
	$scope.getMiddlewareListAndSelected = function(response){
		if(response.code == SUCCESS_CODE){
			if($scope.app.middlewareType == "nginx"){
				$scope.middlewareArr = response.data;
			}else{
				$scope.middlewareArr = response.data.data;
			}
			deployService.addUploadPathToClient($scope);
		}else{
			dbUtils.error(response.message,"提示");
		}
	};
    
    $scope.formEdit = function (value,title,name1,name2) {
    	//form-edit.js
    	dbUtils.openModal('tpl/templates/form_edit.html','FormEditController',"md","",{"value":value,"title":title,},function (result) {
    		if(result){
        		if(name2 == ""){
        			$scope.thead[name1] = result;
        			$scope.unitPath($scope.thead[name1]);
        		}else{
    				$scope.targetServers[name1][name2] = result;
        		}
        	}
    	},true);
	}; 
	
	//每行点击事件
    $scope.checkedRow = function(){
    	var data = $scope.targetServers;
    	checkAllrow(data);
    }
    
    function checkAllrow(data){
    	var dataSize = 0;
    	var flag = true;
        if (!data || data.length == 0) {
            flag = false;
        }
        for (var i in data) {
        	if(!data[i].checked){
        		 flag = false;
        	}else{
        		dataSize++;
        	}
		}
        $scope.thead.allrow = flag;
    }
	
	$scope.allRowCheck = function(){
		for (var i in $scope.targetServers) {
			$scope.targetServers[i].checked = $scope.thead.allrow;
		}
	}
	
	$scope.unitPath = function(uploadPath){
		if(angular.isUndefined(uploadPath) || uploadPath == ""){
			dbUtils.info("请填写地址","提示");
		}else{
			for(var i in $scope.targetServers){
				$scope.targetServers[i].uploadPath = uploadPath;
			}
		}
	}
    
    $scope.style1 = {'border':'1px solid #CFDADD'};
    $scope.style2 = {'border':'1px solid #23B7E5'};
    
    $scope.submitApply = function(){
    	if($scope.form.$invalid){
            return;
        }else{
            var backData = {"version":deployService.getVersion($scope),"description":$scope.fileDescription};
            //选中主机的地址校验
            var clients = [];
            var submitFlag = true;
            for(var i in $scope.targetServers){
            	if($scope.targetServers[i].checked == true){
            		if(angular.isUndefined($scope.targetServers[i].uploadPath) || $scope.targetServers[i].uploadPath == ""){
            			$scope.targetServers[i].writeFlag = false;
            			submitFlag = false;
            		}else{
            			$scope.targetServers[i].writeFlag = true;
            		}
            		clients.push($scope.targetServers[i]);
            	}else if($scope.targetServers[i].checked == false || angular.isUndefined($scope.targetServers[i].checked)){
            		$scope.targetServers[i].writeFlag = true;
            	}
            }
            
            if($scope.appType == 1 && clients.length == 0){
            	dbUtils.warning("请选择数据","提示");
            	return;
            }else{
            	if(submitFlag == false){
            		dbUtils.warning("请填写上传目录","提示");
            		return;
            	}else{
            		backData.clients = clients;
            		$modalInstance.close(backData);
            	}
            }
        }
    }

    $scope.cancel = function () {
        $modalInstance.close(false);
    };
    
}]);

//修改控制器
let GitPackageController = app.controller('GitPackageController',
		['$scope','$rootScope','$modalInstance','$compile','dbUtils','source','EchoLog','DeployService',
		 function($scope,$rootScope,$modalInstance,$compile,dbUtils,source,echoLog,deployService) {
	
	$scope.gitPackage = source.app;
	/*添加版本号*/
	$scope.nowVersion = source.version;
    $scope.versionType = 1;
    $scope.versionDescription;

    deployService.getVersionNum($scope);
    
    $scope.versionCheck = function (n,Z) {
    	deployService.versionCheck($scope,n,Z);
    };
    /*添加版本号end*/

    //点击取消
    $scope.cancel = function () {
    	$modalInstance.close(false);
    };
    
    //点击确定
    $scope.submitApply = function(){
    	//判定表格数据填写是否完整
        if($scope.form.$invalid){
            return;
        }
        $scope.gitPackage.currentPackVersion = deployService.getVersion($scope); 
        if(!$scope.gitPackage.mvnParam)
        	$scope.gitPackage.mvnParam = "";
        if(!$scope.gitPackage.gitUser)
        	$scope.gitPackage.gitUser = "";
        if(!$scope.gitPackage.gitPwd)
        	$scope.gitPackage.gitPwd = "";
        //判定全部信息添加无误，执行传入后台
        delete $scope.gitPackage.settings;
        delete $scope.gitPackage.appExtendVO;
        delete $scope.gitPackage.properties;
        deployService.gitPackage($scope.gitPackage,$scope.versionDescription ? $scope.versionDescription : "").then(function(response){
            if (response.code == SUCCESS_CODE) {
                echoLog.show(response.data,function(){});
                $modalInstance.close(true);
            } else {
                dbUtils.error(response.message,"提示");
            }
        })
    }       
}]);


let DumpListController = app.controller('DumpListController', ['dbUtils','$scope','$modalInstance', '$state','source','EchoLog','DeployService',
                                        function (dbUtils,$scope,$modalInstance, $state,source,echoLog,deployService) {

    $scope.clientId = source.clientId;
    $scope.urlParams = source.urlParams;
    $scope.fileList = [];
   
    /*ui-base-table*/
    $scope.queryParams = {isPagination:true,postgroundPagination:false,pageNumber: 1,pageSize:10,prevPageDisabled: 'disabled',nextPageDisabled: 'disabled'};
    $scope.initTable = function(){
        $scope.dumpListUrl = {url:"/deploy/listDumpFile", urlBody:$scope.urlParams, method:"get"};
		$scope.dumpListTableParams = {BodyStyle:{"max-height": "500px"}};
		$scope.dumpListHeaders = [
			{name:"文件名",field:"fileName",style:{width:"33%"}},{name:"主机地址",field:"clientIp",style:{width:"13%"}},{name:"文件地址",field:"filePath",style:{width:"10%"},compile:true,formatter:function(value,row){
				return "<span class='label bg-light cusor-pointer' data-toggle='tooltip' onmouseenter='$(this).tooltip(\"show\")' data-trigger='click hover' data-html='true' data-placement='auto top' title=\""+value+"\" >查看详情</span>";
			}},{name:"文件大小",field:"fileSize",style:{width:"10%"},compile:true,formatter:function(value,row){
				return dbUtils.convertFileUnit(value);
			}},
			{name:"时间",field:"time",style:{width:"15%"}}
		];
		
		$scope.dumpListRowEvents = [{
            class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"查看详情",name: "查看详情",
            click: function(row){
            	deployService.showDump(row.clientIp,row.clientPort,row.filePath).then(function(response){
                    if (response.code == SUCCESS_CODE) {
                    	echoLog.show(response.data,function(){});
                    } else {
                        dbUtils.error(response.message,"提示");
                    }
                })
            }
        },{
            class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"下载文件",name: "下载文件",
            click: function(row){
                var form = $("<form></form>").attr("action", "/logs/downloadFileCros").attr("method", "post");
                form.append($("<input></input>").attr("type", "hidden").attr("name", "filePath").attr("value", row.filePath));
                form.append($("<input></input>").attr("type", "hidden").attr("name", "ip").attr("value", row.clientIp));
                form.append($("<input></input>").attr("type", "hidden").attr("name", "port").attr("value", row.clientPort));
                form.appendTo('body').submit().remove();
            }
        }];
    }

    $scope.initTable();
    /*ui-base-table end*/
    
    $scope.cancel = function () {
        $modalInstance.close(false);
    };
    
    $scope.submitApply = function(){
        $modalInstance.close(true);
    }
}]);
