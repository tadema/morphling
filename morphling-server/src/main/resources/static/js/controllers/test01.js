'use strict';
/* Controllers */
app.controller('Test01Controller', ['$rootScope', '$scope', '$state','$stateParams', '$cookieStore',"$cacheFactory",'AccountService','$modal','dbUtils','$http','EchoLog', 'AlarmConfigService',
	function ($rootScope, $scope, $state,$stateParams,$cookieStore, $cacheFactory,accountService,$modal,dbUtils,$http,echoLog,alarmConfigService) {
	
	$rootScope.$on('$stateChangeStart',function(event,toState,toParams,fromState,fromParams){
		if(websocket != null){
			websocket.close();
		}
	});
	
	//路径跳转test
	var pathArrs = [
		{name:'dir1',path:"/home/deploy/dir1"},
		{name:'dir2',path:"/dir1/dir2"},
		{name:'dir3',path:"/dir1/dir2/dir3"},
		{name:'dir4',path:"/dir1/dir2/dir3/dir4"}
	];
	$scope.pathObjs = [];
	$scope.index = 0;
	$scope.pathObjs.push(pathArrs[$scope.index]);
	var strTemp = $scope.pathObjs[$scope.index].path; 
	var indexT = parseInt(strTemp.length) - 1;
	if(strTemp.charAt(indexT) == "/"){
		$scope.pathObjs[0].path = strTemp.substr(strTemp.length-1,1);
	}
	$scope.prevPath = $scope.pathObjs[0].path;
	$scope.currPath = $scope.pathObjs[0].path;
	
	$scope.routeChange = function(event,r_index,obj){
		if($scope.index == r_index)
			return;
		$scope.prevPath = $scope.pathObjs[$scope.index].path;
		$scope.index = r_index;
		$scope.pathObjs = $scope.pathObjs.slice(0,$scope.index+1);
		$scope.currPath = $scope.pathObjs[$scope.index].path;
		statusHandle();
	}
	$scope.prevPage = function(){
		$scope.prevPath = $scope.pathObjs[$scope.index].path;
		$scope.index--;
		$scope.pathObjs = $scope.pathObjs.slice(0,$scope.index+1);
		$scope.currPath = $scope.pathObjs[$scope.index].path;
		statusHandle();
	}
	$scope.nextPage = function(){
		$scope.prevPath = $scope.pathObjs[$scope.index].path;
		$scope.index++;
		$scope.pathObjs.push(pathArrs[$scope.index]);
		$scope.currPath = $scope.currPath + "/" +$scope.pathObjs[$scope.index].name;
		$scope.pathObjs[$scope.index].path = $scope.currPath; 
		statusHandle();
	}
	
	function statusHandle(){
		for(var i=0;i<$scope.pathObjs.length;i++){
			if(i == $scope.pathObjs.length-1){
				$scope.pathObjs[i].class = "a-disabled";
			}else{
				$scope.pathObjs[i].class = "a-able";
			}
		}
		console.log("当前路径："+$scope.currPath+",上一步路径："+$scope.prevPath);
	}
	statusHandle();
	//路径跳转test
	$scope.main = $stateParams.data;
	$scope.parentSubmit = function(){
		console.log($scope.main.stateObjectArr);
		dbUtils.info("执行提交方法","提示");
	}
	
/*	$scope.main = $stateParams.data;
	$scope.prevAction = "prev";
	$scope.nextAction = "next";
	$scope.submitAction = "submit";
	
	//修改title样式
	$scope.changeTitleCss = function(){
		if(angular.isUndefined($scope.main.action) || $scope.main.action == null)
			return;
		setTimeout(function(){
			if($scope.main.action == $scope.prevAction){
				$("#nav-step li").eq($scope.main.stateIndex+1).removeClass('active ');
				$("#nav-step li").eq($scope.main.stateIndex).removeClass('finish-step');
				for(var i=1;i<=$scope.main.stateIndex;i++){
					$("#nav-step li").eq(i).addClass('active');
					$("#nav-step li").eq(i-1).addClass('active finish-step');
				}
			}
			if($scope.main.action == $scope.nextAction){
				for(var i=1;i<=$scope.main.stateIndex;i++){
					$("#nav-step li").eq(i).addClass('active');
					$("#nav-step li").eq(i-1).addClass('active finish-step');
				}
			}
		},200)
	}

	$scope.cast = function(action){
		$scope.main.action = action;
		if(action == $scope.prevAction && $scope.canPrev){
			$scope.$broadcast($scope.main.stateObject.state, action);
		}else if(action == $scope.nextAction && $scope.canNext){
			$scope.$broadcast($scope.main.stateObject.state, action);
		}else if(action == $scope.submitAction){
			$scope.$broadcast($scope.main.stateObject.state, action);
		}
	}
	
	$scope.$on("parent", function(event,data) {
		if(data.flag == true){
			if(data.action == $scope.prevAction){
				$scope.prevHandle();
			}else if(data.action == $scope.nextAction){
				$scope.nextHandle();
			}else if(data.action == $scope.submitAction){
				$scope.submit();
			}
		}else{
			if(data.action == $scope.prevAction){
				$scope.prevHandle();
			}else if(data.action == $scope.nextAction){
				dbUtils.warning("请将表单填写完整","提示");
			}
			else if(data.action == $scope.submitAction){
				dbUtils.warning("请将表单填写完整","提示");
			}
		}
	});
	
	//前一步按钮
	$scope.prevHandle = function(){
		$scope.main.stateObject.data = getStateData();
		$scope.main.stateIndex--;
		$scope.main.stateObject = $scope.main.stateObjectArr[$scope.main.stateIndex];
		$state.go($scope.main.stateObject.state,{data:$scope.main,cdata:$scope.main.stateObject.data});//getStateData()
	}
	
	//下一步按钮
	$scope.nextHandle = function(){
		$scope.main.stateObject.data = getStateData();
		$scope.main.stateIndex++;
		$scope.main.stateObject = $scope.main.stateObjectArr[$scope.main.stateIndex];
		$state.go($scope.main.stateObject.state,{data:$scope.main,datas:{},cdata:$scope.main.stateObject.data});//getStateData()
	}
	
	//完成
	$scope.submit = function(){
		if($scope.canPrev == true && $scope.canNext == false){
			$scope.main.stateObject.data = getStateData();
			console.log($scope.main.stateObjectArr);
			dbUtils.info("执行提交方法","提示");
		}
	}
	
	//获取缓存数据
	function getStateData(){
		var cache = $cacheFactory.get($scope.main.stateObject.state);
		var data = {};
		if(!angular.isUndefined(cache)){
			if(!angular.isUndefined(cache.get('data'))){
				data = cache.get('data');
			}
		}else{
			data = $scope.main.stateObject.data;
		}
		return data;
	}
	
	$scope.stateHandle = function(){
		if($scope.main.stateIndex < $scope.main.begin)
			$scope.main.stateIndex = $scope.main.begin;
		if($scope.main.stateIndex > $scope.main.end)
			$scope.main.stateIndex = $scope.main.end;
		
		if($scope.main.end <= $scope.main.begin){
			$scope.canPrev = false;
			$scope.canNext = false;
		}else if($scope.main.stateIndex == $scope.main.begin && $scope.main.end > $scope.main.stateIndex){
			$scope.canPrev = false;
			$scope.canNext = true;
		}else if($scope.main.stateIndex > $scope.main.begin && $scope.main.end > $scope.main.stateIndex){
			$scope.canPrev = true;
			$scope.canNext = true;
		}else if($scope.main.stateIndex > $scope.main.begin && $scope.main.end == $scope.main.stateIndex){
			$scope.canPrev = true;
			$scope.canNext = false;
		}
	};*/
	
	
	
	
	$scope.info = {
		name:'张三',
		age: '23',
		type: '1',
		sex: 'man',
		hobbies: [{name:'songs',value:'songs'},{name:'basketball',value:'basketball'}],
		course: ''
	}
	$scope.$watch('info.name', function(newValue, oldValue) {
        if ($scope.info.name != oldValue){
            //当value改变时执行的代码
        	alert("name的值被改变了哦，oldValue="+oldValue+",当前值="+$scope.info.name+",newValue="+newValue);
        }
    });
	$scope.isShowImg = false;
	
	$scope.middleware = {
		middlewareType: 1,//"weblogic",
		middlewareName: "222",//"weblogic"
		description: "weblogic18d",
//	    installPath: "/home/ora18c/xxx/18c_domains/",
//	    adminPort: "7001",
//	    adminUsername: "weblogic",
//	    adminPassword: "weblogic123",
//	    osUsername: "root",
//	    osPassword: "dell@105B.NEU",
	    managedServers: [{name:"01",value:1232},{name:"02"},{name:"03"}]
	}
	
	$scope.domain = {
			domainName:"testdomain",
		    description:"testdomain111",
		    domainPath:"/home/deploy/Oracle/Middleware/Oracle_Home",
		    domainPort:7011,
		    hostName:"dell01",
		    hostIp:"0",
		    adminUsername: "weblogic",
		    adminPassword: "weblogic123",
		    adminName: "ad-1",
		    adminIp: "0",
		    osUsername: "root",
		    osPassword: "dell@105B.NEU",
		    serverName:"Server_1",
		    serverIp:"192.168.174.4",
		    serverPort:1
		}
	$scope.data1 = {
			'appid':40,
			'domainId':16
	}
	
	
	var data0 =  [
		{
			maiName: "01",
			description: "01des",
			domains: [{domainName:"testdomain11",
			    description:"testdomain111",
			    domainPath:"/home/deploy/Oracle/Middleware/Oracle_Home",
			    domainPort:7011,
			    hostName:"dell01",},
			    {domainName:"testdomain12",
				    description:"testdomain12",
				    domainPath:"/home/deploy/Oracle/Middleware/Oracle_Home",
				    domainPort:7012,
				    hostName:"dell01",}
			]
		},
		{
			maiName: "02",
			description: "02des",
			domains: [{domainName:"testdomain21",
			    description:"testdomain211",
			    domainPath:"/home/deploy/Oracle/Middleware/Oracle_Home",
			    domainPort:7021,
			    hostName:"dell02",},
			    {domainName:"testdomain22",
				    description:"testdomain22",
				    domainPath:"/home/deploy/Oracle/Middleware/Oracle_Home",
				    domainPort:7022,
				    hostName:"dell02",}
			]
		}
	]
	var data1 = {
//			clientIp:"192.168.174.3",
//        	filePath:"/home/weblogic/Oracle/Middleware",
//        	createTime:"2018-12-29~2018-12-30"
				appChineseName: "1",
				currentPackVersion: "0.0.0",
				description: "1",
				developer: "neu",
				fileType: "war",
				middlewareType: "tomcat",
				name: "1"
	}
	var data2 = {
			clientIp:"192.168.174.4",
        	clientPort:21111,
        	filePath:"/workspace/log/iplsqldevJ/app1_start.log"
	}
	var array = new Array();
	array.push(data1);
	array.push(data2);
//	console.log(array);
	$scope.dataArray = JSON.stringify(data1);
//	console.log($scope.dataArray);
	var role = {roleName:"test1",description:"test11"}//id:null,
//	console.log(role);
	var ids = [1,2,3,4];
	var client = {
			name:'without-02',
			username: 'root',
			password: 'dell@105B',
			hostAddress: '192.168.20.20',
			linuxPort: 22,
			remark: 'create a client without agent'
	};
	var client1 = {
			name:'without-02',
			username: 'root',
			password: 'dell@105B',
			hostAddress: '192.168.20.20',
			linuxPort: 22,
			remark: 'create a client without agent'
	};
	var client2 = {
			name:'without-02',
			username: 'root',
			password: 'dell@105B',
			hostAddress: '192.168.20.20',
			linuxPort: 22,
			remark: 'create a client without agent'
	};
	var clients = new Array();
	clients.push(client1);
	clients.push(client2);
	var fileInfo = {
			fileId:2180,
			fileName:'readme.txt',
			fileSize: '1450',
			filePath: '/tmp/files',
			mdfiveValue: '5c52adcd2bcc000a8b4bd5eb36b84cde1',
			isPathNotExist: '1',
			isDuplicate: '1',
			serverType: 'Linux主机',
			fileType: 'linux软件'
	};
	var scriptInfo = {};
	var clientIds = [476,477];
	var fileInfoVO = {
			clientId:clientIds,
			parallel:false,
			fileInfo:fileInfo
	}
	var infos = new Array();
	var info1 = {fileName:"3.0.0"};
	infos.push(info1);
	$scope.submitOk = function(){
		scriptInfo.scriptContent = $scope.info.result;
//		console.log(dbUtils.put("/test/method03",$scope.dataArray));
//		console.log(dbUtils.post("/deploy/delete/app/version",{appId:1337,info:JSON.stringify(infos)}));//JSON.stringify(scriptInfo)
//		var url = "/deploy/downloadDumpFile?filePath=/home/weblogic/workspace/dump/iplsqldevJ/8080/20190403093207&clientId=477";
//		var form = $("<form></form>").attr("action", url).attr("method", "post");
//        form.append($("<input></input>").attr("type", "hidden").attr("name", "filePath"));
//        form.appendTo('body').submit().remove();
//		console.log(dbUtils.postBody("/app/batchSaveAppInstance",JSON.stringify(appInfoVOs)).then(function(response){//?clientId="+clientIds.join()+"&parallel=false
//            if(response.code == SUCCESS_CODE){
////        		echoLog.show(response.data.mainLogId,function(){
////                });
//            }else{
//                dbUtils.error(response.message,"提示");
//            }
//        }));
		console.log(dbUtils.get("/monitor/getAllInfo",{}));
		/*dbUtils.get("/logs/searchLogByCommand",
				{ip:"192.168.174.3",port:21111,filePath:"/home/weblogic/app/morphling-agent/command/update.sh",searchContent:"weblogic"}).
				then(function(response){
					debugger
		            if(response.code == SUCCESS_CODE){
		        		echoLog.show(response.data,function(){
		                });
		            }else{
		                dbUtils.error(response.message,"提示");
		            }
		        })*/
	}
	
	/*$scope.myFunction1 = function (type) {
	   console.log("触底了哦！type="+type);
	};*/
	
	/*test websocket start*/
	var websocket = null;
	
	// 监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
	window.onbeforeunload = function() {
		websocket.close();
	}
	
	// 关闭连接
	$scope.closeWebSocket = function() {  
		websocket.close();
	}
	
	// 发送消息
	$scope.send = function() {  
		var message = document.getElementById('text').value;
		websocket.send(message);
	}
	
	//连接
	$scope.connect = function(){
		// 判断当前浏览器是否支持WebSocket
		if ('WebSocket' in window) {
			websocket = new WebSocket("ws://localhost/websocket/11/22");
		} else {
			alert('Not support websocket')
		}
		// 连接成功建立的回调方法
		websocket.onopen = function(event) {
			setMessageInnerHTML("open");
		}
		
		// 连接发生错误的回调方法
		websocket.onerror = function() {
			setMessageInnerHTML("error");
		};
		
		// 接收到消息的回调方法
		websocket.onmessage = function(event) {
			setMessageInnerHTML(event.data);
		}
		
		// 连接关闭的回调方法
		websocket.onclose = function() {
			setMessageInnerHTML("close");
		}
		
		// 将消息显示在网页上
		function setMessageInnerHTML(innerHTML) {
			document.getElementById('message').innerHTML += innerHTML+ '<br/>';
		}
	}
	/* test websocket end */
	
	var fileInfo1 = {
			fileName:'test1.sh',
			fileSize: '10kb',
			filePath: '/tmp/files',
			isPathNotExist: '0',
			isDuplicate: '1',
			serverType: 'Linux主机',
	};
	var fileInfo2 = {
			fileName:'test2.sh',
			fileSize: '10kb',
			filePath: '/tmp/files',
			isPathNotExist: '0',
			isDuplicate: '1',
			serverType: 'Linux主机',
	};
	$scope.fileInfos = new Array();
	$scope.fileInfos.push(fileInfo1);
	$scope.fileInfos.push(fileInfo2);
	$scope.files = new Array();
	$scope.changeVal = function() {  
    	$('input[id=lefile]').click();
    	$('input[id=lefile]').change(function(){
    		var $file = $(this);
            var fileObj = $file[0];
            $scope.files.push(fileObj.files[0]);
//            console.log($scope.files);
            var windowURL = window.URL || window.webkitURL;
            var dataURL;
            if (fileObj && fileObj.files && fileObj.files[0]) {	      
            	dataURL = windowURL.createObjectURL(fileObj.files[0]);	            
            } else {	            
            	dataURL = $file.val();	
            }
    		$('#photoCover').val($(this).val()); 
    	});
	}; 
	
	$scope.uploadFile = function(){
        var formData = new FormData(document.getElementById("uploadForm"));
//        console.log(formData);
        var file = document.getElementById("lefile").files[0];
//        console.log($scope.files);
//        formData.append("fileName",$scope.files);
        for (var i=0; i<$scope.files.length; i++) {
        	formData.append("files", $scope.files[i]);
        }
        $http({
            method:'post',
            url:'/file/manage/uploadFiles',
            data:formData,
            headers:{'Content-Type':undefined} ,
            transformRequest:angular.identity//浏览器会帮我们把Content-Type 设置为 multipart/form-data.
        }).success(function(data){
        	if(data.code = "1000000"){
        		dbUtils.success("上传成功","上传");
        	}
        }).error(function(data){
//        	console.log(data);
        	if(data == null){
        		dbUtils.error("上传失败","上传");
        	}
        });
	}
	$scope.imgPath = null;
	$scope.getImg = function(){
		$scope.isShowImg = true;
		$("#imgDetail").attr("src",'/welcome/getImg?developer=neu');
//		console.log(document.getElementById("imgDetail"));
//		$.get('/test/getImg',function(data){
//			$("#imgDetail").attr("src",data.substring(1,data.length-1));//"data:image/jpg;base64,"+
////            var imgWindow = $("#imgDetail").html("<img src="+data+">");//接收Base64字符串，并转换为图片显示
//            $scope.isShowImg = true;
////            $scope.imgPath = data;
//            $scope.$apply();
////            console.log($scope.imgPath);
////            console.log($scope.isShowImg);
//            console.log(document.getElementById("imgDetail"));
//        })
	}
	
	/*$scope.uploadFile = function(){
        var formData = new FormData(document.getElementById("uploadForm"));
        var file = document.getElementById("lefile").files[0];
        formData.append("file",file);
        $http({
            method:'post',
            url:'/test/uploadImg',
            data:formData,
            headers:{'Content-Type':undefined} ,
            transformRequest:angular.identity//浏览器会帮我们把Content-Type 设置为 multipart/form-data.
        }).success(function(data){
        	if(data.code = "1000000"){
        		dbUtils.success("上传成功","上传");
        	}
        }).error(function(data){
//        	console.log(data);
        	if(data == null){
        		dbUtils.error("上传失败","上传");
        	}
        });
	}*/
	
	$("#dropdown-course li").on('click',function(){//点击事件      
		var courses=$("#dropdown-course li");
		var count=$(this).index();//获取li的下标         
		var result=courses.eq(count).text();      
		$scope.info.course = result;
		
		$("#dropdownMenu1").html(result+"<span class='caret'></span>");
	})
	
	$scope.managedServer = new Object();
    $scope.managedServer.replies = [{key: 0, value: "",ip:0,name:"",port:""}];
    // 初始化时由于只有1条回复，所以不允许删除
    $scope.managedServer.canRemove = false;

    // 增加回复数
    $scope.managedServer.incre = function($index) {
        $scope.managedServer.replies.splice($index + 1, 0,
            {key: new Date().getTime(), value: ""});   // 用时间戳作为每个item的key
        // 增加新的回复后允许删除
        $scope.managedServer.canRemove = true;
    }

    // 减少回复数
    $scope.managedServer.remove = function($index) {
        // 如果回复数大于1，删除被点击回复
        if ($scope.managedServer.replies.length > 1) {
            $scope.managedServer.replies.splice($index, 1);
        }
        // 如果回复数为1，不允许删除
        if ($scope.managedServer.replies.length == 1) {
            $scope.managedServer.canRemove = false;
        }
    }

    $scope.managedServer.combineReplies = function() {
        var cr = "";
        for (var i = 0; i < $scope.managedServer.replies.length; i++) {
//        	console.log($scope.managedServer.replies[i].name);
//        	console.log($scope.managedServer.replies[i].ip);
//        	console.log($scope.managedServer.replies[i].port);
            cr += "#" + $scope.managedServer.replies[i].value;
        }
        cr = cr.substring(1);
//        console.log(cr);
//        $log.debug("Combined replies: " + cr);

        return cr;
    }
	
    /*测试springboot类型应用批量保存数据 start*/
    var appInfoVO1 = {
			name:'test1',
			appChineseName:"测试1",
			fileType:'jar',
			middlewareType:"springboot",
			springbootPort:6091,
			isMonitor:1,
			prometPort:6191,
			description:"appInfoVO1",
			paramFront:"-server -Xms256m -Xmx256m",
			paramBehind:"",
			clientId:476,
			hostAddress:"192.168.174.3",
			username:"weblogic"
	}
	var appInfoVO2 = {
    		name:'test1',
			appChineseName:"测试1",
			fileType:'jar',
			middlewareType:"springboot",
			springbootPort:6092,
			isMonitor:0,
			prometPort:0,
			description:"appInfoVO2",
			paramFront:"-server -Xms256m -Xmx256m",
			paramBehind:"",
			clientId:477,
			hostAddress:"192.168.174.4",
			username:"weblogic"
	}
	var appInfoVO3 = {
			name:'test3',
			appChineseName:"测试1",
			fileType:'jar',
			middlewareType:"springboot",
			springbootPort:6093,
			isMonitor:0,
			prometPort:10,
			description:"appInfoVO3",
			paramFront:"-server -Xms256m -Xmx256m",
			paramBehind:"",
			clientId:479,
			hostAddress:"192.168.174.5",
			username:"weblogic"	
	}
	var appInfoVO4 = {
			name:'test3',
			appChineseName:"测试1",
			fileType:'jar',
			middlewareType:"springboot",
			springbootPort:6094,
			isMonitor:1,
			prometPort:6194,
			description:"appInfoVO4",
			paramFront:"-server -Xms256m -Xmx256m",
			paramBehind:"",
			clientId:477,
			hostAddress:"192.168.174.4",
			username:"weblogic"
	}
	var appInfoVO5 = {
    		name:'test1',
			appChineseName:"测试1",
			fileType:'jar',
			middlewareType:"springboot",
			springbootPort:6095,
			isMonitor:1,
			prometPort:6195,
			description:"appInfoVO5",
			paramFront:"-server -Xms256m -Xmx256m",
			paramBehind:"",
			clientId:476,
			hostAddress:"192.168.174.3",
			username:"weblogic"
	}
	var appInfoVOs = new Array(appInfoVO1,appInfoVO2,appInfoVO3,appInfoVO4,appInfoVO5);
    /*测试springboot类型应用批量保存数据 end*/
}]);

//上下滚动指令
app.directive('barScrolled', function() {
    return function(scope, elm, attr) {
//        console.log(scope, elm, attr);
        scope.pageType = 0; 
        var obj = elm[0];//document对象
        scope.scrollFlag = true;
        elm.bind('scroll', function() {
            //scrollTop:网页上部分不可见的高度；clientHeight：当前容器的高度；scrollHeight:内容总高度。
        	var currHeight = Math.round(obj.scrollTop)+parseInt(obj.clientHeight);
            
//          var resetDown=obj.scrollHeight*0.8;
//        	var resetUp=obj.scrollHeight*0.2;
        	var resetCenter=obj.scrollHeight*0.5;
        	var offsetDown=obj.scrollHeight*0.9;
        	var offsetUp=obj.scrollHeight*0.1;
        	if(Math.round(obj.scrollTop) == resetCenter){
        		scope.scrollFlag = true;//在中间时，才可以执行翻页操作
        	}
        	if((currHeight >= offsetDown || currHeight <= offsetUp) && scope.scrollFlag == false){
        		$(obj).scrollTop(resetCenter);//当在底部/顶部且翻页标记为否时，重新设置翻页标记
        	}
        	
            if (currHeight >= offsetDown && scope.scrollFlag == true) {
            	scope.scrollFlag = false;
            	scope.pageType = 1; 
            	$(obj).scrollTop(resetCenter);
                scope.$apply(function(){
                	console.log("执行向下查询一页的方法");
                });
            }
            if (currHeight <= offsetUp && scope.pageType!=0 && scope.scrollFlag == true) {
            	scope.scrollFlag = false;
            	scope.pageType = -1; 
            	$(obj).scrollTop(resetCenter);
            	scope.$apply(function(){
            		console.log("执行向上查询一页的方法");
            	});
            }
        });
    };
});

'use strict';
/* Controllers */
app.controller('Test01View01Controller', ['$rootScope', '$scope','$state','$stateParams', '$cookieStore',"$cacheFactory",'AccountService','$modal','dbUtils','$http','EchoLog', 
	function ($rootScope, $scope,$state,$stateParams,$cookieStore, $cacheFactory,accountService,$modal,dbUtils,$http,echoLog) {

	var table = {
	        url:"/alertSenderConfig/list",
	        headers: [
//	        	{name:"id",field:"id",style:{"width":"8%"}},
				{name:"客户端编号",field:"appNumber",style:{"width":"12%"}},
	            {name:"类型",field:"type",style:{"width":"8%"}},
	            {name:"标题",field:"titlel",style:{"width":"10%"}},
	            {name:"邮箱/手机/微信",style:{"width":"25%"},compile:true,formatter:function(value,row){
	            	var val;
	                if("email" == row.type){
	                	val = row.email;
	                }else if("mobile" == row.type){
	                	val = row.mobile;
	                }else if("wechat" == row.type){
	                	val = row.wechat;
	                }
	                return "<span >"+val+"</span>"
	            }},
	            {name:"api地址",field:"apiUrl",style:{"width":"25%"}},
	        ],
	        operationEvents: [{
	            class:"btn-info",
	            icon:"glyphicon glyphicon-plus",
	            name:"新增",
	            click:function(){
	            	var instance = $modal.open({
	                    animation: true,
	                    templateUrl: 'tpl/alarm/remind_edit_modal.html',
	                    controller: 'RemindEditModalController',
	                    size: "lg",
	                    backdrop: "static",
	                    resolve: {
	                        source: function () {
	                            return {};
	                        }
	                    }
	                });
	                instance.result.then(function (data) {
	                	alarmConfigService.alarmRemindSave(JSON.stringify(data)).then(function(response){
	                		if(response.code == SUCCESS_CODE){
	                			dbUtils.info("保存成功","提示");
	                			$scope.table.operations.reloadData();
	                		}else{
	                			dbUtils.error(response.message,"提示");
	                		}
	                	})
	                });
	            }
	        }],
	        rowEvents:[{
	        	class:"btn btn-primary",
	        	icon:"fa fa-pencil",
	        	title:"编辑",
	        	click: function(row){
	        		var instance = $modal.open({
	                    animation: true,
	                    templateUrl: 'tpl/alarm/remind_edit_modal.html',
	                    controller: 'RemindEditModalController',
	                    size: "lg",
	                    backdrop: "static",
	                    resolve: {
	                        source: function () {
	                            return {data:row};
	                        }
	                    }
	                });
	                instance.result.then(function (data) {
	                	alarmConfigService.alarmRemindSave(JSON.stringify(data)).then(function(response){
	                		if(response.code == SUCCESS_CODE){
	                			dbUtils.info("保存成功","提示");
	                			$scope.table.operations.reloadData();
	                		}else{
	                			dbUtils.error(response.message,"提示");
	                		}
	                	})
	                });
	        	}
	        },{
	        	class:"btn-danger",
	            icon:"fa fa-trash",
	        	title:"删除",
	        	click: function(row){
	        		dbUtils.confirm("请确认是否删除",function(){
	        			alarmConfigService.alarmRemindDelete(row.id).then(function(response){
	                		if(response.code == SUCCESS_CODE){
	                			dbUtils.info("删除成功","提示");
	                			$scope.table.operations.reloadData();
	                        }else{
	                            dbUtils.error(response.message,"提示"); 
	                        }
	                	},function(){
	                	})
	        		})
	        	}
	        }],
	        settings:{
	        	cols:3,
//	            showCheckBox:true,
	            tbodyStyle:{
	            	"height":(Number($rootScope._screenProp.contentHeight)-50-40-45-38-30-30-100)+"px",
	            },
	        },
	        afterReload:function(){
	        	setTimeout(function(){
	        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight+(-50-40-45-38-30-30)-57-39+"px";
	        	},200)
	        }
	    }
		$scope.table = table;
	$scope.datas = $stateParams.cdata;
	$scope.action;
	if($.isEmptyObject($stateParams.data) == true){
		$scope.$parent.main.stateObjectArr = [{
			state:'app.test01.view01',
			name:'Home',
			active:'active',
			data:$scope.datas
		},{
			state:'app.test01.view02',
			name:'Profile',
			data:{groupName:'22',ruleName:'22'}
		},{
			state:'app.test01.view03',
			name:'Messages',
			data:{groupName:'33',ruleName:'33'}
		}];
	}
	$scope.$parent.main.begin = 0;
	$scope.$parent.main.end = $scope.$parent.main.stateObjectArr.length-1;
	$scope.$parent.main.stateIndex = $scope.$parent.main.begin;
	$scope.$parent.main.stateObject = $scope.$parent.main.stateObjectArr[$scope.$parent.main.stateIndex];
	
	function getInitData(){
		var data = {};
		data = $scope.$parent.main.stateObject.data;
		return data;
	}
	
	$scope.$parent.changeTitleCss();
	$scope.$parent.stateHandle();
	$scope.data = getInitData();
	
	$scope.$on($state.current.name, function(event,data) {
		$scope.action = data;
		console.log($state.current.name+' 数据进入缓存了');
		$scope.submitApply();
	});
	
	$scope.submitApply = function () {
		var myCache;
		if(angular.isUndefined($cacheFactory.get($state.current.name))){
			myCache = $cacheFactory($state.current.name);
		}else{
			myCache = $cacheFactory.get($state.current.name)
		}
		if($scope.form.$invalid){
			myCache.put("data", {});
			$scope.$emit("parent",{state:$state.current.name,flag:false,action:$scope.action});
			return;
        }
		myCache.put("data", $scope.data);
        $scope.$emit("parent",{state:$state.current.name,flag:true,action:$scope.action});
	};
	
	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};
}]);

'use strict';
/* Controllers */
app.controller('Test01View02Controller', ['$rootScope', '$scope', '$state','$stateParams', '$cookieStore',"$cacheFactory",'AccountService','$modal','dbUtils','$http','EchoLog', 
	function ($rootScope, $scope, $state,$stateParams,$cookieStore, $cacheFactory,accountService,$modal,dbUtils,$http,echoLog) {
	
	$scope.data2 = $stateParams.cdata;
	$scope.action;
	$scope.$parent.changeTitleCss();
	$scope.$parent.stateHandle();
	$scope.$on($state.current.name, function(event,data) {
		$scope.action = data;
		console.log($state.current.name+' 数据进入缓存了');
		$scope.submitApply();
	});
	
	$scope.submitApply = function () {
		var myCache;
		if(angular.isUndefined($cacheFactory.get($state.current.name))){
			myCache = $cacheFactory($state.current.name);
		}else{
			myCache = $cacheFactory.get($state.current.name)
		}
		if($scope.form.$invalid){
			myCache.put("data", {});
			$scope.$emit("parent",{state:$state.current.name,flag:false,action:$scope.action});
			return;
        }
        myCache.put("data", $scope.data2);
        $scope.$emit("parent",{state:$state.current.name,flag:true,action:$scope.action});
	};
	
	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};
}]);

'use strict';
/* Controllers */
app.controller('Test01View03Controller', ['$rootScope', '$scope', '$state','$stateParams', '$cookieStore',"$cacheFactory",'AccountService','$modal','dbUtils','$http','EchoLog', 
	function ($rootScope, $scope, $state,$stateParams,$cookieStore, $cacheFactory,accountService,$modal,dbUtils,$http,echoLog) {
	$scope.data3 = $stateParams.cdata;
	$scope.action;
	$scope.$parent.changeTitleCss();
	$scope.$parent.stateHandle();
	$scope.$on($state.current.name, function(event,data) {
		$scope.action = data;
		console.log($state.current.name+' 数据进入缓存了');
		$scope.submitApply();
	});
	
	$scope.submitApply = function () {
		var myCache;
		if(angular.isUndefined($cacheFactory.get($state.current.name))){
			myCache = $cacheFactory($state.current.name);
		}else{
			myCache = $cacheFactory.get($state.current.name)
		}
		if($scope.form.$invalid){
			myCache.put("data", {});
			$scope.$emit("parent",{state:$state.current.name,flag:false,action:$scope.action});
			return;
        }
        myCache.put("data", $scope.data3);
        $scope.$emit("parent",{state:$state.current.name,flag:true,action:$scope.action});
	};
	
	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};
}]);

