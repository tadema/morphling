﻿'use strict';

// 应用控制器
let ApplicationController = app.controller('ApplicationController', ['$scope','$rootScope','$modal','$state','dbUtils',function($scope,$rootScope,$modal,$state,dbUtils) {
	
	function setAllHidden(){
		$scope.isShowMain = false;
		$scope.isShowCustomRelation = false;
		$scope.isShowWeblogicRelation = false;
		$scope.isShowTomcatRelation = false;
		$scope.isShowSpringbootRelation = false;
	}
	
	setAllHidden();
	$scope.isShowMain = true;
	
	$scope.$on('returnMain',function(event,data){
		setAllHidden();
		$scope.isShowMain = true;
	})
		
	$scope.radioValue = 0;
	$scope.t_id = dbUtils.guid();

    var table = {        url:"/app",
        upfile:true,//批量上传
        showUpload: false,
        pageSql:true,
		hideQuery1: true,//隐藏查询当前框
        params:[
            {name:"name",label:"应用名",labelCols:4,type:"text"},
        ],
        headers: [
            {name:"应用名称",field:"name",style:{width:"20%"}},
            {name:"文件类型",field:"fileType",style:{width:"8%"},compile:true,formatter:function(value,row){
                var show = value;
            	if(value == "catalog"){
                    show = "zip";
                }else if(!value || value == "other" || value == 'undefined'){
                	show = "其他";
                }
                return "<span>"+ show +"</span> ";
            }},
            {name:"部署类型",field:"middlewareType",style:{width:"8%"},compile:true,formatter:function(value,row){
                if(value == "other"){
                	return "<span>其他</span> ";
                }
                return "<span>"+ value +"</span> ";
            }},
            // {name:"应用类型",field:"deployType",style:{width:"9%"},compile:true,formatter:function(value,row){
            //     return value == 1 ? "自定义" : "默认";
            // }},
            {name:"描述",field:"description",style:{width:"20%"}},
            {name:"创建人",field:"createUsername",style:{width:"10%"}},
            {name:"创建时间",field:"createTime",style:{width:"15%"}}
        ],
        operationEvents: [{
        	name:"应用类型",class:"btn-info",icon:"glyphicon glyphicon-plus",type:"radio",id:"appType",
            child:[{
            	name: "默认应用",value: 0,
            },{
            	name: "自定义应用",value: 1,
            }],
            check:true,
            click:function(event, value){
            	$scope.radioValue = value;
            }
        },{
        	name:"新增应用",class:"btn-info",icon:"glyphicon glyphicon-plus",id:"addApp",
            click:function(){
            	var templateUrl,controller,isEdit;
            	if($scope.radioValue == 0){
            		templateUrl = 'tpl/app/app_edit.html';
            		controller = 'ApplicationEditController';
            	}else{
            		templateUrl = 'tpl/app/app_custom_edit.html';
            		controller = 'ApplicationCustomEditController';
            	}
            	dbUtils.openModal(templateUrl,controller,"lg","",{},function (right) {
            		if(right){
               		 	$scope.table.operations.reloadData()
            		}
                });
            }
        },{
        	name:"模板下载",class:"btn-info",icon:"fa fa-download",id:"appDownload",
            click:function(){
            	if($scope.radioValue == 0){
            		var url = "/app/downloadTemplate";
    		        var form = $("<form></form>").attr("action", url).attr("method", "post");
    		        form.append($("<input></input>").attr("type", "hidden"));
    		        form.appendTo('body').submit().remove();
            	}else if($scope.radioValue == 1){
            		var url = "/app/downloadCustomTemplate";
    		        var form = $("<form></form>").attr("action", url).attr("method", "post");
    		        form.append($("<input></input>").attr("type", "hidden"));
    		        form.appendTo('body').submit().remove();
            	}
            }
        },{
        	name:"批量导入",class:"btn-info",icon:"fa fa-upload",id:"appUpload",
            click:function(){
            	if($scope.radioValue == 0){
            		$scope.click = true;
            		$("#"+$scope.t_id+" #lefileForm").click();
            		$("#"+$scope.t_id+" #lefileForm").change(function(){
                    	if($scope.click){
                    		var header_array=["appChineseName","name","fileType","middlewareType","springbootPort","isMonitor","prometPort","description","paramFront","paramBehind","hostAddress","username"];
                    		importf(header_array,"normal");
                    		$scope.click = false;
                    		$("#"+$scope.t_id+" #lefileForm").val('');
                    	}
                	});
            	}else if($scope.radioValue == 1){
            		$scope.click2 = true;
            		$("#"+$scope.t_id+" #lefileForm").click();
            		$("#"+$scope.t_id+" #lefileForm").change(function(){
                    	if($scope.click2){
                    		var header_array=["appChineseName","name","middlewareType","port","description","ip","username","command1","command2","command3","command4","command5"];
                    		importf(header_array,"custom");
                    		$scope.click2 = false;
                    		$("#"+$scope.t_id+" #lefileForm").val('');
                    	}
                	});
            	}
            }
        }],
        rowEvents:[{
        	name:"发布",title:"应用发布",class:"btn-info",icon:"icon-cloud-upload",
        	click: function(row){
        		if(row.deployType == 0)
        			$state.go("app.deploy",{"deployAppId":row.id,fromWhere:"app.manage",'app':row});
        		else if(row.deployType == 1)
        			$state.go("app.customDeploy",{"deployAppId":row.id,fromWhere:"app.manage",'app':row});//自定义应用的发布
        	}
        },{
        	name:"日志",title:"日志管理",class:"btn-info",icon:"fa fa-file-text",
        	click: function(row){
        		$state.go("app.logtable",{"logtableAppId":row.id,'fromWhere':"app.manage",'app':row});
        	}
        },{
        	name:"实例",title:"实例管理",class:"btn-info",icon:"fa fa-exchange",
            isShow:function(row){
                return row.deployType == 1;
            },
            click: function(row){
            	setAllHidden();
            	$scope.isShowCustomRelation = true;
            	$scope.$broadcast("sendDataToCustomRelation",{app: row});
            }
        },{
        	name:"实例",title:"实例管理",class:"btn-info",icon:"fa fa-exchange",
            isShow:function(row){
                return row.deployType == 0 && row.middlewareType != "weblogic";
            },
            click: function(row){
            	if(row.middlewareType == "tomcat"){
//            		$state.go("app.managerTomcat", {appId: row.id})
            		setAllHidden();
                	$scope.isShowTomcatRelation = true;
                	$scope.$broadcast("sendDataToTomcatRelation",{appId: row.id});
            	}else if(row.middlewareType == "springboot"){
//                	$state.go("app.managerSpringBoot", {appId: row.id})
            		setAllHidden();
                	$scope.isShowSpringbootRelation = true;
                	$scope.$broadcast("sendDataToSpringbootRelation",{appId: row.id});
            	}else{
            		dbUtils.error("请填写部署类型信息","提示");
            	}
            }
        },{
        	name:"实例",title:"域关联",class:"btn-info",icon:"fa fa-exchange",
            isShow:function(row){
                return row.deployType == 0 && row.middlewareType == "weblogic";
            },
            click: function(row){
//                $state.go("app.managerWeblogic", {appId: row.id})
                setAllHidden();
            	$scope.isShowWeblogicRelation = true;
            	$scope.$broadcast("sendDataToWeblogicRelation",{appId: row.id});
            }
        },{
        	title:"编辑",class:"btn-info",icon:"fa fa-pencil",
            click: function(row){    
            	var tUrl,tController;
            	if(row.deployType == 1){
            		tUrl = 'tpl/app/app_custom_edit.html';
            		tController = 'ApplicationCustomEditController';
            	}else{
            		tUrl = 'tpl/app/app_edit.html';
            		tController = 'ApplicationEditController';
            	}
            	dbUtils.openModal(tUrl,tController,"lg","",angular.copy(row),function (right) {
            		if(right){
               		 	$scope.table.operations.reloadData()
            		}
                });
            }
        },{
        	title:"删除",class:"btn-danger",icon:"fa fa-trash",
            isShow:function(row){
                return row.username != "admin";
            },
            isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	var obj = {"id":row.id};
            	dbUtils.delete("/app",obj).then(function (response) {
            		if(response.code == SUCCESS_CODE){
                        dbUtils.success("记录删除成功","提示");
                        $(event.target).parents(".popover").prev().click();
                        $scope.table.operations.reloadData()
                    }
            	});
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        settings:{
            cols:3,pageSize:15,
            filterId: "appListFilter",
            operationId: "appOperation",
            showCheckBox:false,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-34-56-20)+"px",
            },
        },
        afterReload:function(){
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight2 +"px";
        	},200)
			$scope.$emit("pageLoadSuccess",{})
        }
    }
    
    $scope.table = table;
    
    // ----------------------------读取文件信息 start---------------------------------------------

    var rABS = false; //是否将文件读取为二进制字符串   
    function importf(header_array,appType) {//appType--普通或者自定义
    		var $file = $("#"+$scope.t_id+" #lefileForm");//$('input[id=lefileForm]');
            var obj = $file[0];
	        if (!obj.files) { return; }
	        var reg = /^.*\.(?:xls|xlsx)$/i;//文件名可以带空格
            if (!reg.test(obj.files[0].name)) {//校验不通过
            	$scope.$apply(function(){
            		dbUtils.info("请上传excel格式的文件!","提示");
                });
                return;
            }
	        var f = obj.files[0];
            var reader = new FileReader();
            var name = f.name;
            var regg = JSON.stringify("");
            reader.onload = function (e) {
                var data = e.target.result;
                var wb;
                if (rABS) {
                    wb = XLSX.read(data, { type: 'binary' });
                } else {
                    var arr = fixdata(data);
                    wb = XLSX.read(btoa(arr), { type: 'base64' });
                }
                regg = JSON.stringify(XLSX.utils.sheet_to_json(wb.Sheets[wb.SheetNames[0]],{header:header_array,raw:false}));
                var b = angular.fromJson(regg);
                if(b.length >= 2)
                	showFile(b,appType);
                else 
                	$scope.$apply(function(){dbUtils.info("请填写至少一条数据!","提示"); });
            };
            if (rABS) reader.readAsBinaryString(f);
            else reader.readAsArrayBuffer(f);           
    }
    
    //appType--普通或者自定义
    function showFile(data,appType) {
    	dbUtils.openModal('tpl/app/app_file.html','ApplicationFileController',"lg","",{'data':data,'appType':appType},function (data) {
			$scope.table.operations.reloadData()
        });
    }
    
    function fixdata(data) {
        var o = "", l = 0, w = 10240;
        for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
        o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
        return o;
    }
    
    // -----------------------------读取文件信息  end------------------------------------------
    
}]);

//应用批量导入controller
let ApplicationFileController = app.controller('ApplicationFileController',
		['$scope','$rootScope','$modalInstance','$compile','dbUtils','source','ApplicationService',
		 function($scope,$rootScope,$modalInstance,$compile,dbUtils,source,appService) {

	$scope.sourceFile = [];
	$scope.checkAll = false;
	$scope.checking = false;
	
	var dataAll = source.data;
	$scope.appType = source.appType;
	for(var i = 1 ; i< dataAll.length; i++){
		var obj = dataAll[i];
		if(!obj.fileType){
			obj.fileType = "other";//为自定义批量导入增加文件类型字段
		}
		var isMonitor = obj.isMonitor;
		if(isMonitor == "YES")obj.isMonitor = 1;
		else if(isMonitor == "NO")obj.isMonitor = 0;
		$scope.sourceFile.push(obj);
	}
			    		
	//查询环境信息
    appService.getCheckDate().then(function(response){
        $scope.clientList = response.data.clientList;
        $scope.appList = response.data.appList;
        $scope.ipGroup = response.data.ipGroup;
        if(checkNow ($scope.sourceFile)){}
        else{ showError(); }
    });
    
    function checkData(data){
    	var list = [];
    	$scope.checkAll = true;
		$scope.msg = "";
    	for(var i in data){
    		var obj = data[i];
    		obj.list = i + 1;
			obj.msg = $scope.msg == "" ? "" : "<br>";
    		if($scope.appType == 'normal'){
    			if(obj.middlewareType == "springboot"){
        			obj.checkApp = checkApp(obj,$scope.appList);
        			obj.checkClient = checkClient(obj,$scope.clientList);
        			obj.springbootPort = Number(obj.springbootPort);
        			obj.checkSpringPort = checkPort(obj,obj.hostAddress,obj.springbootPort,data);
        			if(obj.isMonitor){obj.prometPort = Number(obj.prometPort);obj.checkPrometPort = checkPort(obj,obj.hostAddress,obj.prometPort,data);}
        			else{ obj.prometPort = 0; obj.checkPrometPort = "正确";}
        			obj.checkSame = checkSame(obj,data);
        		}else{
        			obj.checkApp = checkApp(obj,$scope.appList);
        			obj.checkClient = checkClient(obj,$scope.clientList);
        			obj.checkSpringPort = "当前类型 无法判定当前这列值正确性";
        			obj.checkPrometPort = "当前类型 无法判定当前这列值正确性";
        			obj.checkSame = checkSame(obj,data);
        		}
        		if(!obj.checkApp || !obj.checkClient || obj.checkSpringPort != "正确" || obj.checkPrometPort != "正确"
        			|| !obj.checkSame.appChineseName || !obj.checkSame.fileType || !obj.checkSame.middlewareType)
        			$scope.checkAll = false;
        		list.push(obj);
    		}else{//'custom'
    			obj.checkApp = checkApp(obj,$scope.appList);
    			obj.checkClient = checkClient(obj,$scope.clientList);
    			obj.port = Number(obj.port);
    			obj.checkSpringPort = checkPort(obj,obj.ip,obj.port,data);
    			obj.checkSame = customCheckSame(obj,data);
    			if(!obj.checkApp || !obj.checkClient || obj.checkSpringPort != "正确" || !obj.checkSame.appChineseName || !obj.checkSame.middlewareType)
    				$scope.checkAll = false;
        		list.push(obj);
    		}
			$scope.msg += obj.msg;
    	}
    	return list;
    }
    
    function checkApp(data,app){//check为true 代表正确 代表没有相同的app  check为false 代表错误  代表有相同的app 
    	var check = true;
    	for(var i in app){
    		if(data.name == app[i].name){
    			var msg = "应用名："+data.name+"重复   "
				data.msg = data.msg ? data.msg + msg : msg;
				return false;
			}
    	}
    	return check;
    }
    
    function checkSame(obj,data){//check为true 代表正确 代表多条相同  check为false 代表错误  代表多条有不同
    	var check = {"appChineseName":true,"fileType":true,"middlewareType":true};
    	for(var i in data){
    		if(obj.name == data[i].name){
    			if(obj.appChineseName != data[i].appChineseName){check.appChineseName = false;}
    			if(obj.fileType != data[i].fileType){check.fileType = false;}
    			if(obj.middlewareType != data[i].middlewareType){check.middlewareType = false;}
    		}
    	}
    	return check;
    }
    
    function customCheckSame(obj,data){//check为true 代表正确 代表多条相同  check为false 代表错误  代表多条有不同
    	var check = {"appChineseName":true,"middlewareType":true};
    	for(var i in data){
    		if(obj.name == data[i].name){
    			if(obj.appChineseName != data[i].appChineseName){check.appChineseName = false;}
    			if(obj.middlewareType != data[i].middlewareType){check.middlewareType = false;}
    		}
    	}
    	return check;
    }
    
    function checkClient(data,client){//check为主机对应的id 代表正确 代表存在这种主机  check为0 代表错误  代表不存在这种主机
    	var check = 0;
		var ip;
		if($scope.appType == 'normal')
			ip = data.hostAddress;
		else
			ip = data.ip;
    	for(var i in client){
    		if(ip == client[i].hostAddress && data.username == client[i].username)
				check = client[i].id;
    	}
    	if(check == 0){
			var msg = "主机校验："+ip+"不存在   "
			data.msg = data.msg ? data.msg + msg : msg;
		}
    	return check;
    }
    
    function checkPort(row,hostAddress,portParam,data){//check为1 代表正确 代表端口号没问题  check为2 代表错误  代表不在合理范围内 check为3 代表该主机不存在 check为4 代表已存在端口号
    	var check = "未查询到";
    	var count = 0;//计重复的数量，1次为正常，2次以上为重复
    	if(portParam < 65535 && portParam >= 0){
    		if($scope.ipGroup[hostAddress]){
    			check = "正确";//1
        		var port = $scope.ipGroup[hostAddress];
        		for(var i in port){
            		if(portParam == port[i].springbootPort){
						check = "在该主机上,该端口号已为它用,请修改为其他端口号";//4
						var msg = "端口校验："+portParam+"重复   "
						row.msg = row.msg ? row.msg + msg : msg;
					}
            		if(port[i].isMonitor)
            			if(portParam == port[i].prometPort){
            				check = "在该主机上,该端口号已为它用,请修改为其他端口号";//4
							var msg = "端口校验："+portParam+"重复   "
							row.msg = row.msg ? row.msg + msg : msg;
						}
            	}
        		for(var i in data){
        			var tIp = !data[i].hostAddress ? data[i].ip : data[i].hostAddress;
        			var tPort = !data[i].springbootPort ? data[i].port : data[i].springbootPort;
            		if(hostAddress == tIp && portParam == tPort){
            			count++;
            			if(count >= 2){
            				check = "ip和端口出现重复，请进行修改";
							var msg = "IP、端口校验：重复   "
							row.msg = row.msg ? row.msg + msg : msg;
            				return check;
            			}
            		}
            	}
        	}else{
        		check = "正确";//3
        	}
    	}else{
    		check = "该端口号不在合理范围内,请填写0~65535范围内数值";//2
			var msg = "端口校验：填写0~65535范围内数值   "
			row.msg = row.msg ? row.msg + msg : msg;
    	}  	
    	return check;
    }
    
    //再次校验
    function checkNow (data){
    	var nowdata = data;
    	$scope.ApplicationFile = [];
    	$scope.checking = true;
    	$scope.ApplicationFile = checkData(nowdata);
    	$scope.checking = false;
    	if($scope.checkAll)dbUtils.info("数据全部正确，可以保存!","提示");
		else dbUtils.confirmlg($scope.msg,function(){});
    	return $scope.checkAll;
    };
    
    //错误信息
    function showError(){
    	$scope.errorAll = [];  	
    };
    
    //点击取消
    $scope.cancel = function () {
    	$scope.appList = "";
    	$modalInstance.close(false);
    };
   
    
    //点击确定
    $scope.submitApply = function(){
    	//判定表格数据填写是否完整
    	$scope.appList = "";
    	if(checkNow($scope.ApplicationFile)){
    		var saveData = [];
    		for(var i in $scope.ApplicationFile){
    			var obj = $scope.ApplicationFile[i];
    			var newObj = {};
    			if($scope.appType == 'normal'){
    				for(var key in obj){
        				if(key == "name" || key == "appChineseName" || key == "fileType" || key == "middlewareType" || key == "springbootPort" 
        					|| key == "isMonitor" || key == "prometPort" || key == "description" || key == "paramFront" || key == "paramBehind" 
        						|| key == "hostAddress" || key == "username"){
        					newObj[key] = obj[key];
        				}
        				if(key == "checkClient"){
        					newObj.clientId = obj.checkClient;
        				}
        			}
    			}else{//custom
    				for(var key in obj){
        				if(key == "name" || key == "appChineseName" || key == "middlewareType" || key == "port" || key == "description" 
        						|| key == "ip" || key == "username" || key == "command1" || key == "command2" || key == "command3" || key == "command4" || key == "command5"){
        					newObj[key] = obj[key];
        				}
        				if(key == "checkClient"){
        					newObj.clientId = obj.checkClient;
        				}
        			}
    			}
    			saveData.push(newObj);
    		}
    		if($scope.appType == 'normal'){
    			appService.saveData(saveData).then(function(response){
         	        if (response.code == SUCCESS_CODE) {
         	            dbUtils.success("批量添加成功","提示");
         	            $modalInstance.close(true);
         	        } else {
         	        	dbUtils.error("批量添加失败","提示");
         	            dbUtils.error(response.message,"提示");
         	        }
         	    })
    		}else{
    			appService.importAppDeployCustom(saveData).then(function(response){
         	        if (response.code == SUCCESS_CODE) {
         	            dbUtils.success("批量添加成功","提示");
         	            $modalInstance.close(true);
         	        } else {
         	        	dbUtils.error("批量添加失败","提示");
         	            dbUtils.error(response.message,"提示");
         	        }
         	    })
    		}
    	}else{
    		showError();
    	}
  
    }       
}]);

// 应用编辑controller
let ApplicationEditController = app.controller('ApplicationEditController',
		['$scope','$rootScope','$modalInstance','$compile','$modal','dbUtils','source','ApplicationService',
		 function($scope,$rootScope,$modalInstance,$compile,$modal,dbUtils,source,appService) {
	//查询环境信息
    appService.getEnvs().then(function(response){
        $scope.envs = response.data;
    });
    
    //判定是创建还是编辑，对数据信息进行处理
    if(!source.id){
        $scope.editData = false;
        $scope.app = {isMonitor:1};
        $scope.versionHoldNum = 1;
    	$scope.isDeleteVersion = "no";
    	$scope.customLogPath = "";
    }else{
        $scope.editData = true;
        delete source.properties;
        $scope.app = source;
        $scope.customLogPath = $scope.app.appExtendVO.customLogPath;
        if(Number($scope.app.appExtendVO.versionHoldNum) > 0){
        	$scope.versionHoldNum = Number($scope.app.appExtendVO.versionHoldNum);
        	$scope.isDeleteVersion = "yes";
        }else{
        	$scope.versionHoldNum = 1;
        	$scope.isDeleteVersion = "no";
        }
    }

    //部署类型change事件
    $scope.queryMiddleware = function () {    	    	    	
    	if($scope.app.middlewareType == "weblogic"){
        	if(!$scope.app.isStage || $scope.app.isStage == ""){
         		$scope.app.isStage = 1;
         	};
    	}
	};      
    
//	保留最近版本数change事件
    $scope.versionHoldNumChange = function(versionHoldNum){
    	$scope.versionHoldNum = Number(versionHoldNum);
    	
    	if($scope.versionHoldNum <= 0)
    		dbUtils.error("保留版本数必须大于等于1","提示");
    }
    
    //点击确定
    $scope.submitApply = function(){
        if($scope.form.$invalid){
            return;
        }
        //环境判定
        var envs = [];
        angular.forEach(angular.element("input[name=envs]"),function(e,key){
            if(e.checked){
                envs.push(e.value)
            }
        });
        envs.push($scope._userinfo.currentEnv);
        if(envs.length == 0){
            dbUtils.error("请至少选择一个环境","提示");
            return;
        }
        var apolloIdJSON = {};//存放扩展属性
        if($scope.customLogPath && $scope.customLogPath != ""){
        	if($scope.app.middlewareType == "springboot"){
        		apolloIdJSON.customLogPath = $scope.customLogPath;
        	}
        }
        if($scope.isDeleteVersion == "yes"){
        	var versionHoldNum = Number($scope.versionHoldNum);
        	if(versionHoldNum > 0){
        		apolloIdJSON.versionHoldNum = versionHoldNum;
        	}else{
        		dbUtils.error("保留版本数必须大于等于1","提示");
        		return;
        	}
        }else{
        	$scope.app.versionHoldNum = 0;
        }
        $scope.isQueryAppEdit = true;
        
        //判定是否填全各类信息
        if($scope.app.middlewareType == "tomcat"){
        }else if($scope.app.middlewareType == "springboot"){
        }else if($scope.app.middlewareType == "weblogic"){
        	if($scope.app.isStage != 1 && $scope.app.isStage != 0){
        		 $scope.isQueryAppEdit = false;
                 dbUtils.error("请填写isStage信息","提示");
            }
        }
        
        //判定全部信息添加无误，执行传入后台
        if($scope.isQueryAppEdit == true){
        	$scope.app.apolloId = JSON.stringify(apolloIdJSON);
        	appService.saveOrUpdate($scope.app,envs.join()).then(function(response){
                if (response.code == SUCCESS_CODE) {
                    dbUtils.success("操作成功","提示");
                    $modalInstance.close(true);
                } else {
                    dbUtils.error(response.message,"提示");
                }
            })
        }
    }   
    
    $scope.cancel = function () {
    	$modalInstance.close(false);
    };
    
}]);

//Weblogic关联controller
let WeblogicAdminController = app.controller('WeblogicAdminController',
		['$scope','$rootScope','$compile','dbUtils','$stateParams','EchoLog','$modal','$state','ManagerWeblogicService','$timeout',
		 function($scope,$rootScope,$compile,dbUtils,$stateParams,echoLog,$modal,$state,managerWeblogicService,$timeout) {
	
	$scope.$on('sendDataToWeblogicRelation',function(event,data){
		$scope.appId = data.appId;
		setTimeout(function(){
			loadWeblogic();
		},100)
		$scope.showFooTable = true;
		$scope.table = table;
		$scope.table.url = "/app/"+$scope.appId+"/free/domains";
	})
	
    $scope.t_id = dbUtils.guid();
    function loadWeblogic(){
    	$scope.loadingBusy = true;
    	managerWeblogicService.getWeblogic($scope.appId).then(function(response){
            $scope.weblogic = response.data;
            $scope.loadingBusy = false;
        })
    }

    var table = {
    	url:"/app/"+$scope.appId+"/free/domains",
    	showText:true,
    	waringText:"选择要使用的域（Domain）实例",
        headers: [
            {name:"域名称",field:"domainName"},
            {name:"Adminserver名",field:"adminName"},
            {name:"主机名",field:"hostName"},
            {name:"描述",field:"description"},
        ],
        rowEvents:[
        {
            class:"btn-info",
            icon:"glyphicon glyphicon-plus",
            title:"关联域（Domain）",
            name:"关联",
            click: function(row){
            	managerWeblogicService.addWeblogic($scope.appId,row.id).then(function(){
                	loadWeblogic();
                    $scope.table.operations.reloadData();
                })
            }                        
            
        }],
        beforeReload:function(){ },
        afterReload:function(){
			$timeout(function () {
				$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight6+"px";
			})
	    	// setTimeout(function(){
        	// 	$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight6+"px";
        	// },200)
	    },
	    settings:{
	    	filterId: "weblogicRelationFilter",
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-40-137-41-52-20-30-34)+"px",
            }
        }
    
    }

    $scope.deleteWeblogic = function(domainId,domainName){
        dbUtils.confirm("确定要删除【"+domainName+"】上的实例吗？",function(){
        	managerWeblogicService.deleteWeblogic($scope.appId,domainId).then(function(response){
                if(angular.isNumber(response.data)){
                    //需要查看日志
                    echoLog.show(response.data,function(){
                    });
                }
                loadWeblogic();
                $scope.table.operations.reloadData();
            });
        },function(){},true)
    }
    
    $scope.goBackSelectApp = function (){
//    	$state.go("app.manage");
    	$scope.showFooTable = false;
    	$scope.$emit('returnMain',{});
    }

}]);

//tomcat - tomcat
let TomcatAdminController = app.controller('TomcatAdminController',
		['$scope','$rootScope','$compile','dbUtils','$stateParams','EchoLog','$modal','$state','ManagerTomcatService','$timeout',
		 function($scope,$rootScope,$compile,dbUtils,$stateParams,echoLog,$modal,$state,managerTomcatService,$timeout) {
			
	$scope.$on('sendDataToTomcatRelation',function(event,data){
		$scope.appId = data.appId;
		setTimeout(function(){
			loadTomcat();
		},100)
		$scope.showFooTable = true;
		$scope.table = table;
		$scope.table.url = "/middleware/tomcat/"+$scope.appId+"/free"
	})
    $scope.t_id = dbUtils.guid();
    function loadTomcat(){
    	$scope.loadingBusy = true;
    	managerTomcatService.getTomcat($scope.appId).then(function(response){
            $scope.tomcat = response.data;
            $scope.loadingBusy = false;
        })
    }

    var table = {
        url:"/middleware/tomcat/"+$scope.appId+"/free",
        showText:true,
    	waringText:"选择要使用的Tomcat实例",
        headers: [
            {name:"Tomcat版本",field:"version"},
//            {name:"主机名",field:"ip"},
            {name:"主机名",field:"hostName"},
            {name:"端口号",field:"port"},
            {name:"描述",field:"description"},
        ],
        rowEvents:[
        {
            class:"btn-info",
            icon:"glyphicon glyphicon-plus",
            title:"关联Tomcat",
            name:"关联",
            click: function(row){
            	managerTomcatService.addTomcat($scope.appId,row.id).then(function(){
                	loadTomcat();
                    $scope.table.operations.reloadData();
                })
            }                        
        }],
        beforeReload:function(){ },
        afterReload:function(){
			$timeout(function () {
				$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight6+"px";
			})
	    },
	    settings:{
	    	filterId: "tomcatRelationFilter",
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-40-137-41-52-20-30-34)+"px",
            }
        }
    }

    $scope.deleteTomcat = function(tomcatId,tomcatName){
        dbUtils.confirm("确定要删除【"+tomcatName+"】上的实例吗？",function(){
        	managerTomcatService.deleteTomcat($scope.appId,tomcatId).then(function(response){
                if(angular.isNumber(response.data)){
                    //需要查看日志
                    echoLog.show(response.data,function(){

                    });
                }
                loadTomcat();
                $scope.table.operations.reloadData();
            });
        },function(){},true)
    }
    
    $scope.goBackSelectApp = function (){
//    	$state.go("app.manage");
    	$scope.showFooTable = false;
    	$scope.$emit('returnMain',{});
    }

}]);

//springboot关联页
let SpringbootRelationController = app.controller('SpringbootRelationController',
		['$scope','$rootScope','$compile','dbUtils','$stateParams','EchoLog','$modal','$state','ManagerSpringBootService','$timeout',
		 function($scope,$rootScope,$compile,dbUtils,$stateParams,echoLog,$modal,$state,managerSpringBootService,$timeout) {
	
	$scope.$on('sendDataToSpringbootRelation',function(event,data){
		$scope.appId = data.appId;
		$scope.monitorStatus = {};
		$scope.showFooTable = true;
		$scope.table = table;
		$scope.table.url = "/app/"+$scope.appId+"/busy/springboots";
		setTimeout(function(){
			$scope.clients = managerSpringBootService.getAgentList($scope);
		},100)
	})
	
	$scope.t_id = dbUtils.guid();
	
	var table = {
	    url:"/app/"+$scope.appId+"/busy/springboots",
		showText:true,waringText:"配置应用与端口、主机之间的联系",
	    headers: [
	        {name:"IP",field:"springbootHost",style:{"width":"20%"}},
	        {name:"端口",field:"springbootPort",style:{"width":"20%"}},
	        {name:"监控状态",field:"isMonitor",style:{"width":"20%"},compile:true,formatter:function(value,row){
	        	$scope.monitorStatus[row.id] = value;
	        	return "<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-danger\",1:\"text-success\"}[monitorStatus["+row.id+"]]'></i>{{{0:\"未监控\",1:\"监控\"}[monitorStatus["+row.id+"]]}}</span>"
	        }},
	        {name:"监控端口",field:"prometPort",style:{"width":"20%"},compile:true,formatter:function(value,row){
	        	if(value == "undefined" || value == 0)
	        		value = "--";
	        	return value;
	        }},
	    ],
	    operationEvents: [{
            class:"btn-info",
            icon:"glyphicon glyphicon-plus",
            name:"新增关联",
            click:function(){
            	dbUtils.openModal('tpl/app/springboot_relation_edit.html','SpringbootRelationEditController',"lg","",{clients:$scope.clients},function (relationObj) {
            		if(relationObj == false){
            			return;
            		}
                	managerSpringBootService.addSpringbootRelations($scope.appId,relationObj).then(function(response){
                        if (response.code == SUCCESS_CODE) {
                        	dbUtils.success("保存成功","提示");
                        	$scope.table.operations.reloadData();
                        } else {
                        	dbUtils.error(response.message,"提示");
                        }
                    })
                });
            }
        }],
        rowEvents:[{
        	class:"btn-info",
            icon:"fa fa-pencil",
            title:"编辑",
            click: function(row){
            	dbUtils.openModal('tpl/app/springboot_relation_edit.html','SpringbootRelationEditController',"lg","",{clients:$scope.clients,data:angular.copy(row)},function (relationObj) {
            		if(relationObj == false){
            			return;
            		}
                	managerSpringBootService.addSpringbootRelations($scope.appId,relationObj).then(function(response){
                        if (response.code == SUCCESS_CODE) {
                        	dbUtils.success("保存成功","提示");
                        	$scope.table.operations.reloadData();
                        } else {
                        	dbUtils.error(response.message,"提示");
                        }
                    })
                });
            }                        
        },{
        	class:"btn-danger",
            icon:"fa fa-trash",
            title:"删除",
            isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	managerSpringBootService.deleteSpringBoot($scope.appId,row.id).then(function(response){
            		if (response.code == SUCCESS_CODE) {
            			dbUtils.success("删除成功","提示");
            			$(event.target).parents(".popover").prev().click();
            			$scope.table.operations.reloadData();
            		} else {
            			dbUtils.error(response.message,"提示");
            		}
                });
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
	    beforeReload:function(){ 
	    },
	    afterReload:function(){
			$timeout(function () {
				$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight10 +"px";
			})
	    	// setTimeout(function(){
        	// 	$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight5 +"px";
        	// },200)
	    },
	    settings:{
            cols:3,
            showCheckBox:false,
            filterId: "springbootRelationFilter",
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-40-20-41-43-34-56-20)+"px",
            }
        }
	}
	
	// ----------------------------返回初始页面 start---------------------------------------------
	$scope.goBackSelectApp = function (){
//		$state.go("app.manage");
		$scope.showFooTable = false;
		$scope.$emit('returnMain',{})
	}
	// -----------------------------返回初始页面  end------------------------------------------
}]);

//命令模态框
let SpringbootRelationEditController = app.controller('SpringbootRelationEditController',
		['$scope','$rootScope','$modalInstance','$compile','dbUtils','source','ManagerSpringBootService',
		 function($scope,$rootScope,$modalInstance,$compile,dbUtils,source,managerSpringBootService) {
	
	$scope.clients = source.clients;
	$scope.clientsWithId = {};
	for(var i in $scope.clients){
		$scope.clientsWithId[$scope.clients[i].id] = $scope.clients[i];
	}
	$scope.buttonClickFlag = false;
	$scope.selectData = [];
	$scope.returnData = [];
	
	if(angular.isUndefined(source.data) || angular.isUndefined(source.data.id)){
		$scope.isEdit = false;
		$scope.relatedObj = {isMonitor:0,prometPort:""};
    }else{
    	$scope.isEdit = true;
    	$scope.relatedObj = source.data; 
    	var monitorPort = angular.isUndefined($scope.relatedObj.prometPort)||$scope.relatedObj.prometPort==""?0:$scope.relatedObj.prometPort;
    	$scope.relatedObj.ipAndPortAndFlag = $scope.relatedObj.springbootHost + ":" + monitorPort + "," + $scope.relatedObj.isMonitor;
        for(var i in $scope.clients){
			if($scope.relatedObj.deployFileId == $scope.clients[i].id){
				$scope.selectData.push($scope.clients[i].id);
				break;
			}
		}
    }
	
	$scope.dropdownClick = function(event,clientTemp){
		$scope.client = clientTemp;
	}
	
    $scope.cancel = function () {
        $modalInstance.close(false);
    };  
    
//	把controller中对端口检验的配置转移到ui-dbUtils.js中，方便管理和使用,提供ipPortList和oldData的初始化
	dbUtils.startPortCheck($scope,$scope.isEdit,$scope.relatedObj);
    
    $scope.checkPortDuplicate = function(data,propNameList){
    	if($scope.isEdit == true){
    		var isChange = dbUtils.checkChange($scope.oldData,data,propNameList,"springbootHost");
    		if(isChange == false)
    			return;
    	}
    	var isDuplicate = false;
    	let warningText = "";
    	var ipIsChange = false;
		if($scope.oldData["springbootHost"] && $scope.oldData["springbootHost"].isChange)
			ipIsChange = $scope.oldData["springbootHost"].isChange;
		
    	for(let i in $scope.selectData){
    		let clientId = $scope.selectData[i];
    		var tClient = $scope.clientsWithId[clientId];
    		if(angular.isUndefined(tClient))
    			continue;
    		//校验端口是否重复
    		let ip = tClient.hostAddress;
        	if(!ip || ip == ""){
        		dbUtils.warning("请先选择主机","提示");
        		return;
        	}
    		let ports = $scope.ipPortList[ip];
    		if(ports && ports.length > 0){
    			for(let propName of propNameList){
    				if($scope.isEdit == true && ipIsChange == false){
    					if(!$scope.oldData[propName] || !$scope.oldData[propName].isChange)
    						continue;
    				}
    				if(ports.indexOf(parseInt(data[propName])) != -1){
    					warningText += "<h4>ip: "+ip+"</h4><h4>端口: "+data[propName]+"</h4>已被占用，请重新输入端口";
    					isDuplicate = true;
//    					$scope.selectData.splice(i, 1);
    				}
    			}
    		}
    	}
		if(warningText != "")
			dbUtils.warning(warningText,"提示");
		return isDuplicate;
    }
    
    $scope.$watch('{a:relatedObj,b:selectData}', function(newValue, oldValue) {
		if (newValue != oldValue){
			if($scope.isEdit == true){
				if(newValue.b.length != 0 && newValue.b.length == 0)
					return;
			}
			if(oldValue.a.isMonitor == 0 && newValue.a.isMonitor == 1 && newValue.a.prometPort == 0){
				newValue.a.prometPort = "";
			}
			$scope.buttonClickFlag = true;
        }
    },true);
    
    $scope.submitApply = function(){   
        if($scope.form.$invalid){
            return;
        }
        if($scope.selectData.length == 0){
        	dbUtils.warning("请选择主机","提示");
        	return;
        }
        if(angular.isUndefined($scope.relatedObj.springbootPort) || $scope.relatedObj.springbootPort == ""){
			dbUtils.warning("请填写应用端口","提示");
			return;
		}
        if($scope.relatedObj.isMonitor == 1 && (angular.isUndefined($scope.relatedObj.prometPort) || $scope.relatedObj.prometPort == "")){
        	dbUtils.warning("请填写监控端口信息","提示");
        	return;
        }
        if(parseInt($scope.relatedObj.springbootPort) > 65535 || parseInt($scope.relatedObj.prometPort) > 65535){
			dbUtils.warning("端口值过大，请重新输入","提示");
			return;
		}
        if($scope.buttonClickFlag == false){
        	dbUtils.info("信息没有改动，不能保存","提示");
        }else{
        	$scope.returnData = [];
        	if($scope.relatedObj.isMonitor == 0)
        		$scope.relatedObj.prometPort = 0;
        	for(var i of $scope.selectData){
        		var tClient = $scope.clientsWithId[i];
        		var tData = angular.copy($scope.relatedObj);
        		tData.springbootHost = tClient.hostAddress;
        		tData.deployFileId = tClient.id;
        		
                if($scope.isEdit == false){
            		var monitorPort = angular.isUndefined($scope.relatedObj.prometPort)||$scope.relatedObj.prometPort==""?0:$scope.relatedObj.prometPort;
            		tData.ipAndPortAndFlag = tData.springbootHost + ":" + monitorPort + ",0";
            	}
        		$scope.returnData.push(tData);
        	}
        	var idDuplicate;
        	if($scope.isEdit == false)
        		idDuplicate = $scope.checkPortDuplicate($scope.relatedObj,['springbootPort','prometPort']);
        	else
        		idDuplicate = $scope.checkPortDuplicate($scope.returnData[0],['springbootPort','prometPort']);
            if(idDuplicate == true)
            	return;
            $modalInstance.close($scope.returnData);
        }
    }       
}]);

//自定义类型应用实例关联
let CustomManagerController = app.controller('CustomManagerController',
		['$scope','$rootScope','$compile','dbUtils','$stateParams','EchoLog','$modal','$state','CustomManagerService','$timeout',
		 function($scope,$rootScope,$compile,dbUtils,$stateParams,echoLog,$modal,$state,customManagerService,$timeout) {
	
	$scope.$on('sendDataToCustomRelation',function(event,data){
		$scope.app = data.app;
		$scope.showFooTable = true;
		$scope.table = table;
		$scope.table.url = "/app/"+$scope.app.id+"/appDeployCustom/get";
	})
	$scope.t_id = dbUtils.guid();
	
    var table = {
        url:"/app/"+$scope.app.id+"/appDeployCustom/get",
        showText:false,
        showUpload: false,
    	waringText:"",
        headers: [
            {name:"IP",field:"ip"},
            {name:"端口",field:"port"},
            {name:"类型",field:"appType"},
            {name:"主机用户",field:"userName"},
        ],
        operationEvents: [{
            class:"btn-info",
            icon:"glyphicon glyphicon-plus",//glyphicon glyphicon-plus
            name:"新增关联",
            click:function(){
            	dbUtils.openModal('tpl/app/custom_relation_edit.html','CustomRelationEditController',"lg","modal-large",{app:$scope.app},function (relatedObj) {
            		if(relatedObj == false){
                		return;
                	}
                	customManagerService.addAppDeployCustom(relatedObj).then(function(response){
                		if (response.code == SUCCESS_CODE) {
             	            dbUtils.success("添加成功","提示");
             	            $scope.table.operations.reloadData();
             	        } else {
             	        	dbUtils.error("添加失败","提示");
             	        }
                	})
                });
            }
        }],
        rowEvents:[{
        	class:"btn-info",
            icon:"fa fa-pencil",
            title:"编辑",
            click: function(row){
            	dbUtils.openModal('tpl/app/custom_relation_edit.html','CustomRelationEditController',"lg","modal-large",{app:$scope.app,data:angular.copy(row)},function (relatedObj) {
            		if(relatedObj == false){
                		return;
                	}
            		customManagerService.addAppDeployCustom(relatedObj).then(function(response){
                		if (response.code == SUCCESS_CODE) {
             	            dbUtils.success("修改成功","提示");
             	            $scope.table.operations.reloadData();
             	        } else {
             	        	dbUtils.error("修改失败","提示");
             	        }
                	})
                });
            }                        
        },{
        	class:"btn-danger",
            icon:"fa fa-trash",
            title:"删除",
            isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	customManagerService.deleteAppDeployCustom(row).then(function(response){
            		if (response.code == SUCCESS_CODE) {
         	            dbUtils.success("删除成功","提示");
         	           $(event.target).parents(".popover").prev().click();
         	            $scope.table.operations.reloadData();
         	        } else {
         	        	dbUtils.error("删除失败","提示");
         	        }
            	})
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        beforeReload:function(){ 
        },
        afterReload:function(){
			$timeout(function () {
				$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight5 +"px";
			})
	    	// setTimeout(function(){
        	// 	$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight5 +"px";
        	// },200)
	    },
	    settings:{
	    	filterId: "customRelationFilter",
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-40-20-41-43-34-56-20)+"px",
            }
        }
    }

    // ----------------------------返回初始页面 start---------------------------------------------

    $scope.goBackSelectApp = function (){
//    	$state.go("app.manage");
    	$scope.showFooTable = false;
    	$scope.$emit("returnMain",{});
    }
    
    // -----------------------------返回初始页面  end------------------------------------------

}]);

//命令模态框
let CustomRelationEditController = app.controller('CustomRelationEditController',
		['$scope','$rootScope','$modalInstance','$compile','dbUtils','source','CustomManagerService','ClientService',
		 function($scope,$rootScope,$modalInstance,$compile,dbUtils,source,customManagerService,clientService) {
	
	$scope.relatedObj = {};
	$scope.app = source.app;
	$scope.middlewareArr = [];
	$scope.middleware = {};
	$scope.client = {};
	
	if(angular.isUndefined(source.data) || angular.isUndefined(source.data.id)){
		$scope.relatedObj = {};
		$scope.isEditModel = false;
    }else{
    	$scope.relatedObj = source.data; 
        $scope.isEditModel = true;
    }
	$scope.relatedObj.appId = $scope.app.id;
	$scope.relatedObj.appType = $scope.app.middlewareType;
	
	//获取中间件列表和选中的中间件
	$scope.getMiddlewareListAndSelected = function(response){
		if(response.code == SUCCESS_CODE){
			if($scope.app.middlewareType == "nginx"){
				$scope.middlewareArr = response.data;
			}else{
				$scope.middlewareArr = response.data.data;
			}
			if(angular.isDefined($scope.relatedObj) || angular.isDefined($scope.relatedObj.id)){
				for(var i in $scope.middlewareArr){
					if($scope.relatedObj.typeId == $scope.middlewareArr[i].id){
						$scope.middleware = $scope.middlewareArr[i];
						break;
					}
				}
			}
		}else{
			dbUtils.error(response.message,"提示");
		}
	};
	
	//获取主机信息
	clientService.getAgentList().then(function(response){
	    if (response.code == SUCCESS_CODE) {
	    	$scope.clients = response.data; 
	    	if($scope.relatedObj || $scope.relatedObj.id){
	    		for(var i in $scope.clients){
	    			if($scope.relatedObj.clientId == $scope.clients[i].id){
	    				$scope.client = $scope.clients[i];
	    				break;
	    			}
	    		}
	    	}
	    	//nginx情况获取nginx信息
	    	if($scope.app.middlewareType == "nginx"){
	    		customManagerService.getNginxList($scope,$scope.getMiddlewareListAndSelected);//获取nginx列表,并在回调函数中获取到被选中的nginx
	    	}else if($scope.app.middlewareType == "weblogic"){
	    		customManagerService.getWeblogicList($scope,$scope.getMiddlewareListAndSelected);
	    	}else if($scope.app.middlewareType == "tomcat"){
	    		customManagerService.getTomcatList($scope,$scope.getMiddlewareListAndSelected);
	    	}
	    } else {
	    	$scope.clients = []; 
	    }
	});
	
	$scope.buttonClickFlag = false;
	
    $scope.cancel = function () {
        $modalInstance.close(false);
    };  
    
//	把controller中对端口检验的配置转移到ui-dbUtils.js中，方便管理和使用,提供ipPortList和oldData的初始化
	dbUtils.startPortCheck($scope,$scope.isEditModel,$scope.relatedObj);
	
	$scope.checkPortDuplicate = function(ip,data,propNameList){
		return dbUtils.checkPortDuplicate(ip,data,propNameList,$scope.ipPortList,$scope.oldData,$scope.isEdit,"ip");
    }
    
    $scope.$watch('{a:relatedObj,b:client,c:middleware}', function(newValue, oldValue) {
		if (newValue != oldValue){
			if($scope.isEditModel == true){
				if(!angular.equals({}, newValue.b) && newValue.b.id == undefined && angular.equals({}, oldValue.b))
					return;
				if(angular.isDefined(newValue.c) && angular.isUndefined(oldValue.c))
					return;
				$scope.buttonClickFlag = true;
			}
			if($scope.isEditModel == false){
				$scope.buttonClickFlag = true;
			}
        }
    },true);
    
    $scope.submitApply = function(){   
        if($scope.form.$invalid){
            return;
        }
        if(parseInt($scope.relatedObj.port) > 65535){
			dbUtils.warning("端口值过大，请重新输入","提示");
			return;
		}
        
        if($scope.buttonClickFlag == false){
        	dbUtils.info("信息没有改动，不能保存","提示");
        }else{
        	if($scope.app.middlewareType != "springboot"){
        		$scope.relatedObj.typeId = $scope.middleware.id;
        		if($scope.app.middlewareType == "nginx"){
        			$scope.relatedObj.clientId = $scope.middleware.clientId;
        			for(var i in $scope.clients){
    	    			if($scope.relatedObj.clientId == $scope.clients[i].id){
    	    				$scope.client = $scope.clients[i];
    	    				break;
    	    			}
    	    		}
        		}
        		else if($scope.app.middlewareType == "tomcat"){
        			$scope.relatedObj.clientId = $scope.middleware.deployFileId;
        			for(var i in $scope.clients){
    	    			if($scope.relatedObj.clientId == $scope.clients[i].id){
    	    				$scope.client = $scope.clients[i];
    	    				break;
    	    			}
    	    		}
        		}
        		else if($scope.app.middlewareType == "weblogic"){
        			for(var i in $scope.clients){
    	    			if($scope.middleware.adminUsername == $scope.clients[i].username && $scope.middleware.adminIp == $scope.clients[i].hostAddress){
    	    				$scope.client = $scope.clients[i];
    	    				break;
    	    			}
    	    		}
        		}
        	}
        	
        	if($scope.relatedObj.port == ""){
    			return;
    		}
        	if($scope.app.middlewareType == "springboot"){
        		$scope.relatedObj.clientId = $scope.client.id;
        		$scope.relatedObj.typeId = $scope.client.id;
        	}
        		
			$scope.relatedObj.userName = $scope.client.username;
            $scope.relatedObj.ip = $scope.client.hostAddress;
            
            var idDuplicate = $scope.checkPortDuplicate($scope.client.hostAddress,$scope.relatedObj,['port']);
            if(idDuplicate == true)
            	return;
            
            $modalInstance.close($scope.relatedObj);
        }
    }       
}]);


//应用修改控制器
let ApplicationCustomEditController = app.controller('ApplicationCustomEditController',
		['$scope','$rootScope','$modalInstance','$compile','$modal','dbUtils','source','ApplicationService',
		 function($scope,$rootScope,$modalInstance,$compile,$modal,dbUtils,source,appService) {
	//查询环境信息
    appService.getEnvs().then(function(response){
        $scope.envs = response.data;
    });
    
    //判定是创建还是编辑，对数据信息进行处理
    if(!source.id){
        $scope.isEdit = false;
    	$scope.customLogPath = "";
    	$scope.startLogPath = "";
    }else{
        $scope.isEdit = true;
        delete source.properties;
        $scope.app = source;
        $scope.customLogPath = $scope.app.appExtendVO.customLogPath;
        $scope.startLogPath = $scope.app.appExtendVO.startLogPath;
    }

    //点击确定
    $scope.submitApply = function(){
        if($scope.form.$invalid){
            return;
        }
        $scope.app.deployType = 1;//设定为自定义类型
        //环境判定
        var envs = [];
        angular.forEach(angular.element("input[name=envs]"),function(e,key){
            if(e.checked){
                envs.push(e.value)
            }
        });
        envs.push($scope._userinfo.currentEnv);
        if(envs.length == 0){
            dbUtils.error("请至少选择一个环境","提示");return;
        }
        var apolloIdJSON = {};//存放扩展属性
        if($scope.customLogPath != null && $scope.customLogPath != ""){
//        	if($scope.app.middlewareType == "springboot"){
        		apolloIdJSON.customLogPath = $scope.customLogPath;
//        	}
        }
        if($scope.startLogPath != null && $scope.startLogPath != ""){
    		apolloIdJSON.startLogPath = $scope.startLogPath;
        }
        
        //判定全部信息添加无误，执行传入后台
    	$scope.app.apolloId = JSON.stringify(apolloIdJSON);
    	appService.saveOrUpdate($scope.app,envs.join()).then(function(response){
            if (response.code == SUCCESS_CODE) {
                dbUtils.success("操作成功","提示");
                $modalInstance.close(true);
            } else {
                dbUtils.error(response.message,"提示");
            }
        })
    }  
    
    $scope.cancel = function () {
        $modalInstance.close(false);
    };
}]);
