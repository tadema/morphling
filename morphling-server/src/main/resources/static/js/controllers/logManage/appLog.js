'use strict';
app.controller('AppLogController',
		['$scope','$rootScope','$modal','$state','$stateParams','dbUtils','EchoLog','$http','$filter','LogTableService','$timeout',
		 function($scope,$rootScope,$modal,$state,$stateParams,dbUtils,echoLog,$http,$filter,logTableService,$timeout) {

	let stateParams;
	if($stateParams.logtableAppId && $stateParams.app){
		stateParams = $stateParams;
    }else{
    	if($scope.appCfg.tabManager.TABS && $scope.appCfg.tabManager.TABS.length > 0){
	    	for(let i in $scope.appCfg.tabManager.TABS){
				let tab = $scope.appCfg.tabManager.TABS[i];
				if(tab.hash == "app.logtable"){
					stateParams = tab.params;
					break;
				}
			}
		}else{
			$state.go("app.selectAppLogtable",{from:$state.current.name,title:"选择应用(日志)"})
			return;
		}
    }
	
    $scope.appId = stateParams.logtableAppId;
    $scope.application = stateParams.app;
    $scope.fromWhere = stateParams.fromWhere;
    $scope.showDetail = false;
    
    $scope.logsTable = [];
    $scope.logType = 0;
    $scope.pageSelect = [10,20,50,100];
    $scope.pageSize = 10;
    $scope.clientList = [];

    //初始化部署、启动备份、运行日志表格参数
    $scope.tooltipData = {"deployType":{}, "deployInfo":{}, "startBakFilePath":{}, "runningFilePath":{}};
	logTableService.initDeployTable($scope,loadingLogsTmpl);
	logTableService.initStartBakTable($scope,loadingLogsTmpl);
	logTableService.initRunningTable($scope,loadingLogsTmpl);

	$scope.$on("baseTableRefreshData",function(event, data){
		loadingLogsTmpl(1);
	})
	$scope.$on("baseTablePageChangeHandle",function(event, data){
		loadingLogsTmpl(data.pageNumber);
	})

    // ----------------------------查询 start---------------------------------------------
    //条件查询
	$scope.queryLogs = function(){	
		if($scope.application.middlewareType !='springboot' || $scope.logType != 3){
			var time;
			if($scope.query.startDay && $scope.query.endDay){
				if(!dbUtils.isDate($scope.query.startDay) || !dbUtils.isDate($scope.query.endDay)){
					dbUtils.warning("起始时间或结束时间格式错误","提示");
	    			return;
				}
				if($scope.query.startDay > $scope.query.endDay){
	    			dbUtils.warning("起始时间不能大于结束时间","提示");
	    			return;
	    		}
				$scope.query.startDay = $filter('date')($scope.query.startDay, "yyyy-MM-dd"); 
				$scope.query.endDay = $filter('date')($scope.query.endDay, "yyyy-MM-dd"); 
				time = $scope.query.startDay + "~" + $scope.query.endDay;
    		}
			$scope.queryParams = queryParamsHandle(time);
		}else{
			if($scope.query.customLogPath && $scope.query.customLogPath != ""){ 
				$scope.queryParams.customLogPath = $scope.query.customLogPath;
			}else{
				dbUtils.warning("请填写路径信息","提示");
				return;
			}
		}
		loadingLogsTmpl(1);
	}
	
	//重置查询
	$scope.resetQuery = function(){
		resetParams();
		loadingLogsTmpl(1);
	}
	
	function queryParamsHandle(time){
		var queryParams = {};
		if($scope.logType == 1){
			if(time && time != "none"){ 
				queryParams['createTime'] = time; 
			}
			if($scope.query.clientIp && $scope.query.clientIp != ""){ 
				queryParams['clientIp'] = $scope.query.clientIp; 
			}
			if($scope.query.createUsername && $scope.query.createUsername != ""){ 
				queryParams['createUsername'] = $scope.query.createUsername; 
			}
		}else if($scope.logType == 2 || $scope.logType == 3){
			if(time && time != "none"){
				queryParams['time'] = time; 
			}
			if($scope.query.clientIp && $scope.query.clientIp != ""){ 
				queryParams['clientIp'] = $scope.query.clientIp; 
			}
			if($scope.query.fileName && $scope.query.fileName != ""){ 
				queryParams['fileName'] = $scope.query.fileName; 
			}
			if($scope.query.filePath && $scope.query.filePath != ""){ 
				queryParams['filePath'] = $scope.query.filePath; 
			}
		}
		return queryParams;
    }
    // -----------------------------查询  end------------------------------------------
    
	// ----------------------------部署日志表格 start---------------------------------------------
    //日志类型tab页切换
    $scope.chooseLogType = function(logType){
    	if($scope.logType != logType){
    		$scope.logType = logType;
    		resetParams();
    		loadingLogsTmpl(1);
    	}
    }
    
    //加载对应的日志类型{currentPage:页码}
    function loadingLogsTmpl(currentPage){
    	if($scope.logType == 1){
    		logsTableType1(currentPage);
    	}else if($scope.logType == 2){
    		logsTableType2(currentPage);
    	}else{
    		logsTableType3(currentPage);
    	}
    }

    //查询类型为1的日志数据
    function logsTableType1(currentPage){
		$scope.deployTableParams.loading = true;
		$scope.clientList = [];
		logTableService.deployLogTable1({appId:$scope.appId , info:JSON.stringify($scope.queryParams) , currentPage:$scope.deployPageParams.pageNumber , onePageSize:$scope.deployPageParams.pageSize}).then(function(response){
			$scope.deployTableParams.loading = false;
			if(response.code == SUCCESS_CODE){
				$scope.clientList = response.data.clients;
				$scope.deployInitData = response.data.pagers;
			}else{
				dbUtils.error(response.message,"提示");
			}
		},function(result){	$scope.deployTableParams.loading = false;})
    }

	
    //查询类型为2的日志数据
    function logsTableType2(currentPage){
		$scope.startBakTableParams.loading = true;
		$scope.clientList = [];
    	logTableService.deployLogTable2($scope.appId,$scope.queryParams).then(function(response){
			$scope.startBakTableParams.loading = false;
			if(response.code == SUCCESS_CODE){
				$scope.clientList = response.data.clients;
				$scope.startBakInitData = response.data.pages.data;
            }else{
                dbUtils.error(response.message,"提示"); 
            }
        },function(result){	$scope.startBakTableParams.loading = false;})
    }
    
	//查询类型为3的日志数据
    function logsTableType3(currentPage){
    	$scope.runningTableParams.loading = true;
    	if($scope.application.deployType == 1){ //自定义类型
    		logTableService.getCustomAppRunningLog($scope.appId,$scope.query.customLogPath).then(function(response){
				$scope.runningTableParams.loading = false;
				if(response.code == SUCCESS_CODE){
	            	$scope.runningInitData = response.data.pages.data;
	            }else{
	                dbUtils.error(response.message,"提示"); 
	            }
	        },function(result){$scope.runningTableParams.loading = false;})
    	}else{// 普通类型
			$scope.clientList = [];
    		logTableService.deployLogTable3($scope.appId,$scope.queryParams).then(function(response){
				$scope.runningTableParams.loading = false;
				if(response.code == SUCCESS_CODE){
	            	$scope.clientList = response.data.clients;
					$scope.runningInitData = response.data.pages.data;
		        	/*if($scope.application.middlewareType == "springboot"){
		        		$scope.query.customLogPath = $scope.application.appExtendVO.customLogPath;
		        	}*/
	            }else{
	                dbUtils.error(response.message,"提示"); 
	            }
	        },function(result){$scope.runningTableParams.loading = false;})
    	}
    }
    
    //初始化加载日志列表
    $scope.chooseLogType(1);
    
    function resetParams() {
		$scope.query = {};
		if($scope.application && $scope.application.appExtendVO){//当定义了自定义查询路径，则赋值
			$scope.query.customLogPath = $scope.application.appExtendVO.customLogPath;
		}
		$scope.queryParams = {};
		$scope.clientList = [];
	}
    // -----------------------------部署日志表格  end------------------------------------------
	    
    // -----------------------------日志清除 start------------------------------------------
    $scope.cleanVision = function(){
    	var removeLogList = [];
    	var data;
    	if($scope.logType == 1){
    		data = $scope.logTable1;
    	}else if($scope.logType == 2 || $scope.logType == 3){
    		data = $scope.logTable;
    	}
    	if(data && data.length != 0){
    		for(var i in data){
    			if(data[i].checked == true){
    				removeLogList.push(data[i]);
    			}
    		}
    	}
    }
    // -----------------------------日志清除  end------------------------------------------
    $scope.formEdit = function (value,name,title) {
    	dbUtils.openModal('tpl/templates/form_edit.html','FormEditController',"md","",{"value":value,"title":title,},function (result) {
    		if(result){
        		$scope.query[name] = result;
        	}
    	},true);
    };   
    
    // -----------------------------日志拆分 start------------------------------------------
    $scope.splitLog = function(rows){
		//需要确定拆分规则，一次一条还是可以传多条
		if(rows.length == 0 || rows.length > 1){
			dbUtils.warning("请选择一条数据","提示");
			return;
		}
		dbUtils.openModal('tpl/logtable/log_modal.html','LogModalController',"lg","",{"data":angular.copy(rows),"action":"split"},function (data) {
			if(data){
				logTableService.splitLogFile({info:JSON.stringify(data)}).then(function(response){
					if(response.code == SUCCESS_CODE){
						dbUtils.info("拆分成功","提示");
						$scope.queryLogs();
					}else{
						dbUtils.error(response.message,"提示");
					}
				});
			}
		},true);
    }
    // -----------------------------日志拆分  end------------------------------------------
    
    // -----------------------------日志合并 start------------------------------------------
    $scope.mergeLog = function(){
    	var mergeLogList = [];
    	var data = $scope.logTable;
    	if(data && data.length != 0){
    		for(var i in data){
    			if(data[i].checked == true){
    				mergeLogList.push(data[i]);
    			}
    		}
    		if(mergeLogList.length < 2){
    			dbUtils.info("至少选择两条数据","提示");
    			return;
    		}
    		dbUtils.confirm("请确实是否执行日志合并",function(){
    			console.log("执行日志合并");
    		},function(){
    		})
    	}
    }
    // -----------------------------日志合并  end------------------------------------------
    
    // ----------------------------部署日志详情 start---------------------------------------------
    
    $scope.showLogs1 = function (log){
    	dbUtils.openModal('tpl/logtable/show_logs.html','ShowLogsController',"lg","",{"data":log,"logType":$scope.logType,"type":"log"},function (result) {
    	},true);
    }

    $scope.showLogs2 = function (log,type,keyword){
    	$scope.searchDetail = false;
    	var textHeight = $rootScope._screenProp.contentHeight-2*10-5-2-30;//,一个返回按钮30
    	$("#logResult").css("height",textHeight+"px");
    	$scope.showDetail = true;
    	$scope.logMessege = {
    		clientId:477,type:1,position:0,rows:1000,keyword:'',filePath:log.filePath
    	};
    	$scope.logMessege.clientId = log.clientId;
    	$scope.logMessege.type = type;
    	$scope.logMessege.keyword = keyword;
    	$scope.info = {result:'',result1:'',result2:'',length:0,rows:1000,position1:0,position2:0,position3:0,downScrollFlag:false,upScrollFlag:false,querying:true};
    	dbUtils.get("/logs/readLogPartInAgent",$scope.logMessege).then(function(response){
		    if(response.code == SUCCESS_CODE){
//		    	var list = response.data.data.split(/\r?\n|\r/).length;
 		    	$scope.info.length = Number(response.data["MinderLength"]);
		    	$scope.info.result2 = response.data.data;
		    	$scope.info.position1 = 0;
		    	$scope.info.position2 = 0;
		    	$scope.info.position3 = $scope.info.length;
		    	$scope.info.totalLength = Number(response.data["MinderTotalLength"]);
		    	$scope.info.result = $scope.info.result1 + $scope.info.result2;
//		    	console.log($scope.info.position1 +"@----------@"+ $scope.info.position2+"@----------@"+$scope.info.position3);
//		    	console.log($scope.info.length +"@----------@"+ $scope.info.totalLength);
		    	if($scope.logMessege.type == 2){//关键字高亮
		    		$scope.info.result = $scope.info.result.replace(new RegExp($scope.logMessege.keyword,"gm"),"<span style='background-color:yellow'>"+$scope.logMessege.keyword+"</span>");
		    	}
//		    	console.log($scope.info.result);
		    	$("#logResult").html($scope.info.result); 
		    	if($scope.info.length == 0){
		    		$scope.info.result = "无结果";
		    	}else{
		    		$scope.info.downScrollFlag = true;
		    	}
		    	$scope.info.querying = false;
		    }else{
		        dbUtils.error(response.message,"提示");
		    }
		})
    }
    
    $scope.backToUpperLevel = function(){
    	$scope.showDetail = false;
    }

    // -----------------------------部署日志详情 end------------------------------------------
    
    // ----------------------------搜索部署日志详情 start---------------------------------------------
    
    $scope.searchLog = function (log){
    	$scope.searchDetail = true;
    	dbUtils.openModal('tpl/logtable/search_detail.html','LogsDetailController',"lg","modal-content-center",{"data":log,"logType":$scope.logType},function (result) {
//          查询信息随着滚动条会继续查询的形式  
//    		$scope.showLogs2(log,2,result)
//    		新窗口展现的形式,且logtype为1时，不会执行，因为没有返回result
    		var client = null;
    		for(var i in $scope.clientList){
    			if(log.clientId == $scope.clientList[i].id){
    				client = $scope.clientList[i];
    				break;
    			}
    		}
    		if(client == null){
    			dbUtils.warning("主机信息匹配失败","提示");
    			return;
    		}
    		logTableService.searchLogByCommand({"ip":client.hostAddress,"port":client.port,"filePath":log.filePath,"searchContent":result}).then(function(response){
    			if(response.code == SUCCESS_CODE){
    				window.open("tpl/log/newTabLogWs.html?logId="+response.data,"_blank");
    			}else{
    				dbUtils.error(response.message,"提示");
    			}
    		})
    	},true);
    }

    // -----------------------------搜索部署日志详情 end------------------------------------------
    
    // ----------------------------实时日志查询 start---------------------------------------------
    $scope.runningLog = function (log){
    	dbUtils.openModal('tpl/logtable/log_runninglog.html','LogsRunningController',"lg","modal-content-center",{"data":log,"logType":$scope.logType},function (result) {
    		if(result){
    			var client = null;
    			for(var i in $scope.clientList){
    				if(log.clientId == $scope.clientList[i].id){
    					client = $scope.clientList[i];
    					break;
    				}
    			}
    			if(client == null){
    				dbUtils.warning("主机信息匹配失败","提示");
    				return;
    			}
    			logTableService.tailStartedLog(JSON.stringify(client),log.filePath,result).then(function(response){
    				if(response.code == SUCCESS_CODE){
//    				echoLog.show(response.data,function(){})
    					window.open("tpl/log/newTabLogWs.html?logId="+response.data,"_blank");
    				}else{
    					dbUtils.error(response.message,"提示");
    				}
    			})
    		}
    	},true);
    }
    // -----------------------------实时日志查询 end------------------------------------------
    
    $scope.serverDownload = function(log){
//    	/app/menu.sql	Win10PE_amd64.iso	main.js
    	var xmlHttp = new XMLHttpRequest();
    	let params = {"logPath":"D:\\main.js"}
    	xmlHttp.open('GET', "http://localhost:21111/execute/readBackupLog?params="+JSON.stringify(params), true);
    	xmlHttp.setRequestHeader("Authorization", "Basic bWFuYWdlOjk5ODg3NzY2NTU0NDMzMjIxMQ==");
    	xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    	xmlHttp.responseType = "blob";
    	xmlHttp.onreadystatechange = function () {
    		if (xmlHttp.readyState == 4) {
    			console.log(xmlHttp.response);
		/*        var filename = 'main.js';
	            var blob = new Blob([xmlHttp.response], {type: 'application/octet-stream'});
	            var csvUrl = URL.createObjectURL(blob);
	            var link = document.createElement('a');
	            link.href = csvUrl;
	            link.download = filename;
	            link.click();*/
	        };
	    };
	    /*xmlHttp.addEventListener("load", function(ev) {
	    		// 下载完成事件：处理下载文件
	    		window.webkitRequestFileSystem(TEMPORARY, 1024 * 1024, function(fs) {
	    		    fs.root.getFile('Win10PE_amd64.iso', {create: true}, function(fileEntry) {
	    		      fileEntry.createWriter(function(writer) {

	    		        writer.onwrite = function(e) { console.log("onwrite",e) };
	    		        writer.onerror = function(e) { console.log("onerror",e) };

	    		        var bb = new BlobBuilder();
	    		        bb.append(xmlHttp.response);

	    		        writer.write(bb.getBlob());

	    		      }, onError);
	    		    }, onError);
	    		  }, onError);
        });*/
	    xmlHttp.send();
    	
    }
    
    function onError(e) {console.log('Error', e);	}
    
    // ----------------------------下载日志 start---------------------------------------------
    
    $scope.downLogs = function (log,l_index){
    	var logData  = {};
 		logData.clientIp = log.clientIp;
 		logData.clientId = log.clientId;
 		logData.clientPort = log.clientPort;
 		logData.filePath = log.filePath;
 		var str = "确认下载"+log.fileName+"?";
 		str=str.replace(/[^\x00-\xff]/g,"$&\x01").replace(/.{35}\x01?/g,"$&\n").replace(/\x01/g,"");
 		dbUtils.confirm(str,function(){
			var url = "/logs/downloadFileCros";
			var form = $("<form></form>").attr("action", url).attr("method", "post");
			form.append($("<input></input>").attr("type", "hidden").attr("name", "filePath").attr("value", logData.filePath));
			form.append($("<input></input>").attr("type", "hidden").attr("name", "ip").attr("value", logData.clientIp));
			form.append($("<input></input>").attr("type", "hidden").attr("name", "port").attr("value", logData.clientPort));
			form.appendTo('body').submit().remove();
    		// 点击确认按钮
 			/*$scope.loading = true;
 			logTableService.downloadBackup(logData).then(function(response){
	        	 if(response.code == SUCCESS_CODE){
	        		var url = "/logs/downloadZipLog";
    		        var fileName = response.data;
    		        var form = $("<form></form>").attr("action", url).attr("method", "post");
    		        form.append($("<input></input>").attr("type", "hidden").attr("name", "filePath").attr("value", fileName));
    		        form.appendTo('body').submit().remove();
    		        $scope.loading = false;
	             }else{
	                 dbUtils.error(response.message,"提示");
	                 $scope.loading = false;
	             }
 			},function(result){$scope.loading = false;})*/
    	},function(){},true);
    }
    // -----------------------------下载 end------------------------------------------
    
    // ----------------------------返回初始页面 start---------------------------------------------

    $scope.goBackSelectApp = function (){
    	if($scope.showDetail == true){
    		$scope.showDetail = false;
    	}else{
//        	$state.go("app.selectApp",{from:$state.current.name})
        	if(!angular.isUndefined($scope.fromWhere) && $scope.fromWhere!="" && $scope.fromWhere != null){
        		$state.go($scope.fromWhere,{from:$state.current.name})
        	}else{
        		$state.go("app.selectAppLogtable",{from:$state.current.name,title:"选择应用(日志)"})
        	}
    	}
    }
    
    // -----------------------------返回初始页面  end------------------------------------------
}]);

app.controller('ShowLogsController', ['dbUtils','$scope','$modalInstance', '$state','source','$http', 
                                        function (dbUtils,$scope,$modalInstance, $state,source,$http) {
	$scope.title = "显示日志详情";
	if(source.type == "log"){
		if(source.logType == 1){
			showLogs1(source.data.logId);
		}
	}else if(source.type == "detail"){
		if(source.logType == 1){
			$scope.showLogsDetail = source.data;
		}else if(source.logType == 2){
			$scope.showLogsDetail = "待开发";
		}else if(source.logType == 3){
			$scope.showLogsDetail = "待开发";
		}
	}
	
	function showLogs1(logId){
		$scope.showLogsDetail = "查询中...";
		dbUtils.get("/logs/show/" + logId).then(function(response){
			if (response.code == SUCCESS_CODE) {
	        	$scope.showLogsDetail = response.data; 
	      	} else {
	        	$scope.showLogsDetail = "查询出现错误";
	        	dbUtils.error(response.message,"提示");
	      	} 
		})
    }
	
    $scope.cancel = function () {
        $modalInstance.close(false);
    };
    
    $scope.submitApply = function(){
        $modalInstance.close();
    }
    
}]);


app.controller('LogsDetailController', ['dbUtils','$scope','$modalInstance', '$state','source','$http','$modal','EchoLog','LogTableService', 
                                      function (dbUtils,$scope,$modalInstance, $state,source,$http,$modal,echoLog,logTableService) {

	function queryLogs1(logId){		
		logTableService.queryDetail1({
			id:logId,
			searchContent:$scope.searchContent
        }).then(function(response){
            if(response.code == SUCCESS_CODE){
            	openLog1(response.data); 
            }else{
            	$scope.showLogsDetail = "";
	            dbUtils.error(response.message,"提示");
            }
        },function(){
        	alert("error"); 
    	})
	}
	
	//部署记录的关键字查询
	function openLog1(data){
		var instance = $modal.open({
	        animation: true,
	        templateUrl: 'tpl/logtable/show_logs.html',
	        controller: 'ShowLogsController',
	        size: "lg",
	        backdrop: "static",
	        resolve: {
	            source: function () {
	                return {"data":data,"logType":source.logType,"type":"detail"};
	            }
	        }
	    });
    }
	
	//日志文件的关键字查询
	function queryLogs2(log){
		$modalInstance.close($scope.searchContent);
	}

	$scope.queryDetail = function(){
	  	if($scope.form.$invalid){
	  		dbUtils.warning("请填写查询条件","提示");
            return;
        }
	  	if(source.logType == 1){
	  		queryLogs1(source.data.logId);
		}else if(source.logType == 2){
			queryLogs2(source.data);
		}else if(source.logType == 3){
			queryLogs2(source.data);
		}
	}
  
	$scope.cleanQuery = function(){
		$scope.searchContent = "";
	}
  
	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};  
  
}]);

//上下滚动指令
app.directive('barScrolled', ['dbUtils',function (dbUtils) {
    return function(scope, elm, attr) {
        var obj = elm[0];//document对象
        scope.scrollFlag = true;
        elm.bind('scroll', function() {
            //scrollTop:网页上部分不可见的高度；clientHeight：当前容器的高度；scrollHeight:内容总高度。
        	var currHeight = Math.round(obj.scrollTop)+parseInt(obj.clientHeight);
        	var resetCenter=obj.scrollHeight*0.5;
        	var offsetDown=obj.scrollHeight*0.9;
        	var offsetUp=obj.scrollHeight*0.1;
        	var addCenter = obj.scrollHeight*0.05;
        	if(currHeight >= offsetDown || currHeight <= offsetUp){
        		//当在底部/顶部且翻页标记为否时，重新设置翻页标记
        		scope.scrollFlag = true;
        		if(scope.info.position1 > 1){scope.info.upScrollFlag = true;}else{scope.info.upScrollFlag = false;}
            	if(scope.info.position3 < scope.info.totalLength){scope.info.downScrollFlag = true;}else{scope.info.downScrollFlag = false;}//dbUtils.info("到底部了","提示");
        	}
            if (currHeight >= offsetDown && scope.scrollFlag == true && scope.info.downScrollFlag == true && scope.info.querying == false) {//&& scope.logMessege.type!=2 
            	scope.info.querying = true;
            	$(obj).scrollTop(resetCenter);
            	scope.scrollFlag = false;
            	scope.info.downScrollFlag = false;
            	scope.logMessege.position = scope.info.position3;
            	scope.logMessege.rows = scope.info.rows;
            	get(scope.logMessege,"down");
            }
            if (currHeight <= offsetUp && scope.logMessege.type!=2 && scope.scrollFlag == true && scope.info.upScrollFlag == true && scope.info.querying == false) {
            	scope.info.querying = true;
            	$(obj).scrollTop(resetCenter);
            	scope.scrollFlag = false;
            	scope.info.upScrollFlag = false;
            	if(scope.logMessege.type != 2){
            		scope.logMessege.type = -1;
            	}
            	scope.logMessege.position = scope.info.position1;
            	scope.logMessege.rows = scope.info.rows;
            	get(scope.logMessege,"up");
            }
            function get(logMessege,type){
            	 dbUtils.get("/logs/readLogPartInAgent",logMessege).then(function(response){
         		    if(response.code == SUCCESS_CODE){
    			    	var list = response.data.data.split(/\r?\n|\r/).length;
         		    	scope.info.length = Number(response.data["MinderLength"]);
         		    	if(type == "down"){
         		    		scope.info.result1 = scope.info.result2;
         		    		scope.info.result2 = response.data.data;
         		    		scope.info.position1 = scope.info.position2;
         		    		scope.info.position2 =  scope.info.position3;
         			    	scope.info.position3 = scope.info.position3 + scope.info.length;
         		    	}else{
         		    		scope.info.result2 = scope.info.result1;
         		    		scope.info.result1 = response.data.data;
         			    	scope.info.position3 = scope.info.position2;
         		    		scope.info.position2 = scope.info.position1;
         		    		scope.info.position1 = scope.info.position1 - scope.info.length;
         		    	}
        		    	scope.info.result = scope.info.result1 + scope.info.result2;
        		    	var content = scope.info.result;
        		    	if(scope.logMessege.type == 2){//关键字高亮
        		    		content = content.replace(new RegExp(scope.logMessege.keyword,"gm"),"<span style='background-color:yellow'>"+scope.logMessege.keyword+"</span>");
        		    	}
        		    	$("#logResult").html(content); 
        		    	$(obj).scrollTop(resetCenter);
        		    	scope.info.querying = false;
//        		    	console.log(scope.info.position1 +"@----------@"+ scope.info.position2+"@----------@"+scope.info.position3 )
         		    }else{
         		    	$(obj).scrollTop(resetCenter);
         		        dbUtils.error(response.message,"提示");
         		    }
         		})
        	}
        });
    };
}]);

app.controller('LogModalController', ['dbUtils','$scope','$modalInstance', '$state','source','$http','$modal','EchoLog','LogTableService', 
    function (dbUtils,$scope,$modalInstance, $state,source,$http,$modal,echoLog,logTableService) {
	$scope.logs = source.data;
	$scope.action = source.action;
	$scope.obj = {splitFileSize:""};
	$scope.unit;
	
	if($scope.action == "split"){
		$scope.title = "拆分";
		
		if($scope.logs[0].fileSizes.indexOf("KB") != -1){
			$scope.unit = "k";
		}else if($scope.logs[0].fileSizes.indexOf("MB") != -1){
			$scope.unit = "m";
		}else if($scope.logs[0].fileSizes.indexOf("GB") != -1){
			$scope.unit = "g";
		}else if($scope.logs[0].fileSizes.indexOf("B") != -1){
			$scope.unit = "";
		} 
	}
	
	$scope.cancel = function () {
       $modalInstance.close(false);
	};
	
	$scope.submitApply = function(){
		if($scope.action == "split"){
			if($scope.form.$invalid){
		  		dbUtils.warning("请填写拆分文件大小","提示");
	            return;
	        }
			$scope.logs[0].splitFileSize = $scope.obj.splitFileSize + $scope.unit;
			$modalInstance.close($scope.logs);
		}
	}
}]);

//实时日志查询controller
app.controller('LogsRunningController', ['dbUtils','$scope','$modalInstance', '$state','source','$http','$modal','EchoLog','LogTableService', 
    function (dbUtils,$scope,$modalInstance, $state,source,$http,$modal,echoLog,logTableService) {
	$scope.lineNum = 300;
	
	$scope.cancel = function () {
       $modalInstance.close(false);
	};
	
	$scope.submitApply = function(){
		if($scope.form.$invalid){
            return;
        }
		$modalInstance.close($scope.lineNum);
	}
}]);
