﻿'use strict';

// 应用控制器
app.controller('MonitorDefineController', ['$scope','$rootScope','$modal','$state','dbUtils','MonitorDefineService','EchoLog',
                                           function($scope,$rootScope,$modal,$state,dbUtils,monitorDefineService,echoLog) {
	$scope.t_id = dbUtils.guid();
	var table = {
        url:"/monitor/listAllMonitor",
//        listName:"图形化页面配置————",
        /*params:[
            {name:"name",label:"名称",labelCols:4,type:"text"},
            {name:"url",label:"url",labelCols:4,type:"text"},
            {name:"type",label:"监控类型",labelCols:4,type:"text"}
        ],*/
        showUpload: false,
        pageSql:true,
        headers: [
            {name:"名称",field:"name",style:{width:"20%"}},
//            {name:"URL",field:"url"},
            {name:"URL",field:"url",style:{width:"15%"},compile:true,formatter:function(value,row){
            	return "<span class='badge sm-br bg-light' data-toggle='tooltip' data-trigger='click hover' data-placement='auto right' onmouseenter='$(this).tooltip(\"show\")'data-html='true' title='"+value+"' style='cursor:pointer;'>查看详情</span>";
	        }},
		    {name:"监控类型",field:"type",style:{width:"15%"}},
            {name:"创建时间",field:"createTime",style:{width:"15%"}},
            {name:"备注",field:"remark",style:{width:"20%"}},
        ],
        operationEvents: [{
            class:"btn-info",
            icon:"glyphicon glyphicon-plus",
            name:"新增图形化页面配置",
            click:function(){
                var instance = $modal.open({
                    animation: true,
                    templateUrl: 'tpl/monitor/monitor_edit.html',
                    controller: 'MonitorEditController',
                    size: "lg",
                    backdrop: "static",
                    resolve: {
                        source: function () {
                            return {}
                        }
                    }
                });
                instance.result.then(function (right) {
                	if(right){
                        $scope.table.operations.reloadData()
                	}
                });
            }
        }],
        rowEvents:[{
        	class:"btn-info",
            icon:"fa fa-pencil",
            title:"编辑",
            click: function(row){                
                var instance = $modal.open({
                    animation: true,
                    templateUrl: 'tpl/monitor/monitor_edit.html',
                    controller: 'MonitorEditController',
                    size: "lg",
                    backdrop: "static",
                    resolve: {
                        source: function () {
                            return angular.copy(row);
                        }
                    }
                });
                instance.result.then(function (right) {
                	if(right){
                        $scope.table.operations.reloadData()
                	}
                });
            }
        },{
        	class:"btn-danger",
            icon:"fa fa-trash",
            title:"删除",
            isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	monitorDefineService.deleteMonitor(row.id).then(function(response){
                    if (response.code == SUCCESS_CODE) {
                        dbUtils.success("删除成功","提示");
                        $(event.target).parents(".popover").prev().click();
                        $scope.table.operations.reloadData();
                    } else {
                        dbUtils.error(response.message,"提示");
                    }
                })
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        settings:{
            cols:3,
            showCheckBox:false,
            filterId: "monitorDefineFilter",
            tbodyStyle:{
//            	"height":(Number($rootScope._screenProp.contentHeight)-2*154-41-43-34-56-20-10)+"px",
            },
        },
        afterReload:function(){
        	setTimeout(function(){
//        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight+(-2*154-41-43-56-20-10)-parseInt($("#"+$scope.t_id+" #footable thead").height())+"px";
        	},200)
        }
    }
    
    //同时赋值监控服务器和图形服务的信息
    function getMonitorServer(){
    	monitorDefineService.getMonitorServer().then(function(response){
    		$scope.monitorServer = {};
    		$scope.graphServers = {};
            if(response.code == SUCCESS_CODE){
            	$scope.monitorServer = showStatus(response.data,"prometheus");
            	for(var i in $scope.monitorServer){
            		if(angular.isUndefined($scope.monitorServer[i].typeName) || $scope.monitorServer[i].typeName != "prometheus"){
            			$scope.monitorServer[i].typeName = "prometheus";
            		}
            	}
            	
            	$scope.graphServers = showStatus(response.data,"grafana");
            	for(var i in $scope.graphServers){
            		if(angular.isUndefined($scope.graphServers[i].typeName) || $scope.graphServers[i].typeName != "grafana"){
            			$scope.graphServers[i].typeName = "grafana";
            		}
            	}
            }else{
                dbUtils.error(response.message,"提示"); 
            }
        });
    }
    
    //添加状态信息
    function showStatus(data,typeName){
    	var monitorServer = [];
    	for (var i = 0; i < data.length; i++) {
    		var obj = data[i];
//    		{0:'待部署',1:'运行中',2:'已停止'}[{{app.type}}]
    		if(obj["status"] == "UP"){obj["statusClass"] = 1;}
    		else if(obj["status"] == "DOWN"){obj["statusClass"] = 2;}
    		if(angular.isUndefined(obj.name) || typeName == obj.name || typeName == obj.typeName){
    			monitorServer.push(obj);
    		}
     	}
    	return monitorServer;
    }

    $scope.clickOk = function(row,event){
    	monitorDefineService.deleteMonitorServer(row.id).then(function(response){
            if (response.code == SUCCESS_CODE) {
                dbUtils.success("删除成功","提示");
                $scope.clickCancel(event);
                getMonitorServer();
            } else {
                dbUtils.error(response.message,"提示");
            }
        })
    }
    
    $scope.clickCancel = function(event){
    	$(event.target).parents(".popover").prev().click();
    }
    
    $scope.editServer = function(row){
    	row.typeName = "prometheus";
    	dbUtils.openModal('tpl/monitor/monitor_server.html','MonitorServerEditController',"lg","",angular.copy(row),function (data) {
    		if(data)
        		getMonitorServer();
    	});
    }
    
    $scope.addServer = function(){
    	if($scope.monitorServer.length >= 1){
    		dbUtils.info("监控服务器只能保存一条","提示");
    		return;
    	}else{
    		$scope.editServer({});
    	}
    }
    
    /*----------------------------图形服务方法start----------------------------*/
    
    //新建或者编辑
    $scope.addOrEditGraphServer = function(row){
    	if($.isEmptyObject(row) && $scope.graphServers.length >= 1){
    		dbUtils.info("图形服务只能保存一条数据","提示"); 
    	}else{
    		row.typeName = "grafana";
    		dbUtils.openModal('tpl/monitor/monitor_server.html','MonitorServerEditController',"lg","",angular.copy(row),function (data) {
        		if(data)
        			getMonitorServer();
        	});
    	}
    }
    
    $scope.startOrStopGraphServer = function(row,command){
    	var graphServers = [];
    	var graphServer = {
    			ip : row.ip,
    			port : row.port,
    			action : command,
    			name : row.name,
    			typeName: row.typeName
    	};
    	graphServers.push(graphServer);
    	monitorDefineService.startOrStopGraphServer({info:JSON.stringify(graphServers)}).then(function(response){
    		if(response.code == SUCCESS_CODE){
    			echoLog.show(response.data,function(){
    				getMonitorServer();
    			});
            }else{
                dbUtils.error(response.message,"提示"); 
            }
    	});
    }
    
    $scope.refreshPrometheus = function(){
    	getMonitorServer();
    }
    
    /*----------------------------图形服务方法end----------------------------*/
    getMonitorServer();
    $scope.table = table;
    
}]);


//修改控制器
app.controller('MonitorEditController', 
		['$scope','$rootScope','$modalInstance','$compile','dbUtils','source','MonitorDefineService',
		 function($scope,$rootScope,$modalInstance,$compile,dbUtils,source,monitorDefineService) {
//	debugger;
    //查询监控类型信息
	monitorDefineService.getMonitorType().then(function(response){
        $scope.monitorType = response.data;
    });
	
    //判定是创建还是编辑，对数据信息进行处理
    if(angular.isUndefined(source.id)){
        $scope.editData = false;
        $scope.monitor = {};
    }else{
        $scope.editData = true;
        delete source.properties;
        $scope.monitor = source;
        $scope.type = $scope.monitor.type;
        setTimeout(function () { $("#monitor_type").val($scope.type);}, 200);
    }

    //点击取消
    $scope.cancel = function () {
//    	$scope.monitor = {};
        $modalInstance.close(false);
    };
         
    
    //点击确定
    $scope.submitApply = function(){
//    	debugger
    	//判定表格数据填写是否完整
        if($scope.form.$invalid){
            return;
        }

        $scope.monitor.type = $scope.type;
        //判定全部信息添加无误，执行传入后台
        monitorDefineService.saveOrUpdateMonitor($scope.monitor).then(function(response){
            if (response.code == SUCCESS_CODE) {
                dbUtils.success("操作成功","提示");
                $modalInstance.close(true);
            } else {
                dbUtils.error(response.message,"提示");
            }
        })

        
    }       
}]);

//修改控制器
app.controller('MonitorServerEditController', 
		['$scope','$rootScope','$modalInstance','$compile','dbUtils','source','MonitorDefineService',
		 function($scope,$rootScope,$modalInstance,$compile,dbUtils,source,monitorDefineService) {
    //判定是创建还是编辑，对数据信息进行处理
    if(angular.isUndefined(source.id)){
        $scope.editData = false;
        $scope.monitorServer = source;
    }else{
        $scope.editData = true;
        delete source.properties;delete source.statusClass;
        $scope.monitorServer = source;
    }

    $scope.cancel = function () {
    	$modalInstance.close(false);
    };
    
//	把controller中对端口检验的配置转移到ui-dbUtils.js中，方便管理和使用,提供ipPortList和oldData的初始化
	dbUtils.startPortCheck($scope,$scope.editData,$scope.monitorServer);
         
    $scope.checkPortDuplicate = function(ip,data,propNameList){
    	return dbUtils.checkPortDuplicate(ip,data,propNameList,$scope.ipPortList,$scope.oldData,$scope.editData,"ip");
    }
    
    //点击确定
    $scope.submitApply = function(){
    	//判定表格数据填写是否完整
        if($scope.form.$invalid){
            return;
        }
        var idDuplicate = $scope.checkPortDuplicate($scope.monitorServer.ip,$scope.monitorServer,['port']);
        if(idDuplicate == true)
        	return;
        //判定全部信息添加无误，执行传入后台
        monitorDefineService.saveOrUpdateMonitorServer($scope.monitorServer).then(function(response){
            if (response.code == SUCCESS_CODE) {
                dbUtils.success("操作成功","提示");
                $modalInstance.close(true);
            } else {
                dbUtils.error(response.message,"提示");
            }
        })
    }       
}]);
