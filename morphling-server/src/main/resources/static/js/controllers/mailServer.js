'use strict';

// 软件安装list控制器
app.controller('MailServerListController', ['$scope','$rootScope','$modal','$state','dbUtils','$stateParams','EchoLog','$http','MailServerService',
                                       function($scope,$rootScope,$modal,$state,dbUtils,$stateParams,echoLog,$http,mailServerService) {
	
	$scope.t_id = dbUtils.guid();
	
	var table = {
    	url:"/mailSmtpConfig/list",
    	showUpload: false,
        pageSql:false,
        headers: [
            {name:"名称",field:"mailName",style:{width:"20%"}},
            {name:"类型",field:"mailType",style:{width:"10%"}},
            {name:"地址",field:"host",style:{width:"15%"}},
            {name:"端口",field:"port",style:{width:"10%"}},
            {name:"发件人邮箱",field:"fromUser",style:{width:"15%"}},
            {name:"启用",field:"isEnable",style:{width:"10%"},compile:true,formatter:function(value,row){
            	return value == 1 ? "是" : "否";
            }},
        ],
        operationEvents: [{
            class:"btn-info",
            icon:"glyphicon glyphicon-plus",
            name:"新增邮件服务器",
            click:function(){
            	addOrEditMailServer({})
            }
        }],
        rowEvents:[{
            class:"btn-info",
            icon2:"fa fa-play-circle",
            icon:"fa fa-pencil",
            title:"编辑",
            name:"编辑",
            click: function(row){
            	addOrEditMailServer(angular.copy(row))
            }
        },{
        	class:"btn-danger",
            icon:"fa fa-trash",
            title:"删除",
            isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	mailServerService.delete(row.id).then(function(response){
            		if (response.code == SUCCESS_CODE) {
            			dbUtils.success("删除成功","提示");           
            			$scope.table.operations.reloadData();
            		} else {
            			dbUtils.error(response.message,"提示");
            		}
            	})	
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        settings:{
            cols:2,
            filterId: "mailServerListFilter",
            showCheckBox:false,
            pageSize:10,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-34-56-20)+"px",
            },
        },
        afterReload:function(){
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight1+"px";
        	},200)
        },
        beforeReload:function(){}
    }
  	
	$scope.table = table;
	
	
	function addOrEditMailServer(data){
		dbUtils.openModal('tpl/system/mailServer_edit.html','MailServerEditController',"lg","",data,function (result) {
    		if(result){
        	    $scope.table.operations.reloadData()
        	}
    	});
	}
	
}]);

//邮件服务器新增或修改
app.controller('MailServerEditController', 
		['$scope','$rootScope','$modalInstance','$compile','$modal','dbUtils','source','MailServerService',
		 function($scope,$rootScope,$modalInstance,$compile,$modal,dbUtils,source,mailServerService) {	
			
	// 控制显示“新建”还是“编辑”
    if(!source.id){
        $scope.isEdit = false;
        $scope.mailServer = {"mailType":"smtp", "isEnable":"1"};
    }else{
        $scope.isEdit = true;
        $scope.mailServer = source;
    }
    
//	把controller中对端口检验的配置转移到ui-dbUtils.js中，方便管理和使用,提供ipPortList和oldData的初始化
	dbUtils.startPortCheck($scope,$scope.editData,$scope.domain);
    
    $scope.checkPortDuplicate = function(ip,data,propNameList,ipProp){
    	return dbUtils.checkPortDuplicate(ip,data,propNameList,$scope.ipPortList,$scope.oldData,$scope.editData,ipProp);
    }
    
    $scope.$watch('domain',function(newValue,oldValue){
    	if(newValue === oldValue){
    		return;
    	}
    	
    	if(newValue != oldValue){
    		if(oldValue.isAdminMonitor == 0 && newValue.isAdminMonitor == 1 && newValue.prometPort == 0){
    			newValue.prometPort = "";
    		}
    	}
    },true)
    
    $scope.submitApply = function(){
    	if($scope.form.$invalid){
            return;
        }
    	mailServerService.saveOrUpdate($scope.mailServer).then(function(response){
    		if (response.code == SUCCESS_CODE) {
    			dbUtils.success("操作成功","提示");            
    			$modalInstance.close(true);
    		} else {
    			dbUtils.error(response.message,"提示");
    		}
    	})	
    } 
    
    $scope.cancel = function () {
        $modalInstance.close(false);
    };
}]);