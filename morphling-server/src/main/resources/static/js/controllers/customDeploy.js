'use strict';
app.controller('CustomDeployController', 
		['$scope','$rootScope','$modal','$state','$stateParams','dbUtils','EchoLog','$http','ApplicationService','EndpointService','DeployService','CustomManagerService',
		 function($scope,$rootScope,$modal,$state,$stateParams,dbUtils,echoLog,$http,appService,endpointService,deployService,customManagerService) {
	
	let stateParams;
	if($stateParams.deployAppId && $stateParams.app){
		stateParams = $stateParams;
    }else{
    	if($scope.appCfg.tabManager.TABS && $scope.appCfg.tabManager.TABS.length > 0){
	    	for(let i in $scope.appCfg.tabManager.TABS){
				let tab = $scope.appCfg.tabManager.TABS[i];
				if(tab.hash == "app.customDeploy"){
					stateParams = tab.params;
					break;
				}
			}
		}else{
			$state.go("app.selectAppDeploy",{title:"选择应用(发布)"})
			return;
		}
    }
	
    $scope.$on('$stateChangeStart', function(event,data) {
		//路由变化，先删除定时器
		if($scope.timeId)
			window.clearInterval($scope.timeId);
	});

    $scope.appId = stateParams.deployAppId;
    $scope.app = stateParams.app;
    $scope.fromWhere = stateParams.fromWhere;
    $scope.t_id = dbUtils.guid();
    $scope.parallel = false;
    var healthCheckList = [];
    //应用详情数据
    function beforeLoad(){
        if(Number($scope.app.appExtendVO.versionHoldNum) > 0){
            $scope.versionNumShowText = Number($scope.app.appExtendVO.versionHoldNum);
        }else{
            $scope.versionNumShowText = "否";
        }
        $scope.table = customTable;
        queryCountNum();
        $scope.monitor = {"status":{},'registStatus':{},"nginxStatus":{},"healthStatus":{},"healthDetail":{},"info":{},"deployStatus":{},"deployDetail":{}};
        $scope.stoppedCount = 0;
        $scope.offlineCount = 0;
        $scope.illedCount = 0;        
    }
    
    //实例数量的接口
    function queryCountNum(){
    	$scope.count = {"total":0,'onLine':0,"offLine":0};
    	deployService.queryCountNum($scope.appId,$scope.app.middlewareType).then(function(response){
            if(response.code == SUCCESS_CODE){
            	$scope.count = response.data;
            }else{
                dbUtils.error(response.message,"提示");
            }
        })
    }

    //自定义应用发布表格
    var customTable = {
        url:"/app/preview/"+$scope.appId+"/customs",
        showUpload: true,
        pageSql:true,
        isRefreshStatus:true,
        headers: [
            {name:"主机名",field:"hostName",style:{"width":"20%"},compile:true,formatter:function(value,row){
            	if($scope.app.middlewareType == "nginx"){
            		this.name == "nginx名称";
            	}
            	return value;
            }},
            {name:"IP",field:"ip",style:{"width":"15%"},compile:true,formatter:function(value,row){
            	row.host = value;
                return value;
            }},
            {name:"端口",field:"port",style:{"width":"10%"}},
            {name:"部署版本",field:"deployVersion",style:{"width":"14%"},compile:true,formatter:function(value,row){
            	if(value == "undefined")
            		return "--";
            	else 
            		return value;
            }},
            {name:"状态",field:"status",compile:true,style:{"width":"8%"},formatter:function(value,row){
                $scope.monitor['status'][row.id] = value;//首次初始化该状态
                if(value == 2){
                    $scope.stoppedCount+=1;
                }
                return "<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-dark\",1:\"text-success\",2:\"text-danger\"}[monitor.status["+row.id+"]]'></i>{{{0:\"待部署\",1:\"运行中\",2:\"已停止\"}[monitor.status["+row.id+"]]}}</span>"
            }},
            {name:"健康状态",field:"healthStatus",class:"text-center",style:{"width":"8%"},compile:true,formatter:function(value,row){
            	$scope.monitor['healthStatus'][row.id] = 2;
            	var startContext;
            	if($scope.app.startContext != null && $scope.app.startContext != "" && $scope.app.startContext != " ")
            		startContext = $scope.app.startContext;
            	else
            		startContext = "";
            	var url = "http://"+row.ip+":"+row.port+"/"+startContext;
            	healthCheckList.push({id:row.id, "url": url})
            	return "<span ng-if='monitor.healthStatus["+row.id+"] == 2'><i class='fa fa-spin fa-spinner'></i></span>" +
            			"<span ng-if='monitor.healthStatus["+row.id+"] != 2'><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-danger\",1:\"text-success\"}[monitor.healthStatus["+row.id+"]]'></i>{{{0:\"不可用\",1:\"可用\"}[monitor.healthStatus["+row.id+"]]}}</span>";
            }},
        ],
        rowEvents:[{
        		class:"btn-info",
                icon:"fa fa-pencil",
                title:"编辑脚本",
                click: function(row){ 
                	var instance = $modal.open({
                        animation: true,
                        templateUrl: 'tpl/app/custom_relation_edit.html',
                        controller: 'CustomRelationEditController',
                        windowClass: "modal-large",
                        size: "lg",
                        backdrop: "static",
                        resolve: {
                            source: function () {
                                return {appId:$scope.appId,data:angular.copy(row),app:$scope.app}
                            }
                        }
                    });
                    instance.result.then(function (app) {
                    	if(app == false){
                    		return;
                    	}
                    	customManagerService.addAppDeployCustom(app).then(function(response){
                    		if (response.code == SUCCESS_CODE) {
                 	            dbUtils.success("修改成功","提示");
                 	            $scope.table.operations.reloadData();
                 	        } else {
                 	        	dbUtils.error("修改失败","提示");
                 	        }
                    	})
                    });
                }
        	},
        	{
	        	class:'btn-info',
	            icon:"fa fa-home",
	            title:"点击查看应用首页",
	            name:"首页",
	            click:function(row){
	            	if(row.status == 1)
	            		window.open("http://"+row.ip+":"+row.port+"/"+$scope.app.startContext);
	            	else
	            		dbUtils.warning("应用首页未运行不可查看","提示");
	            }
	        },
	        {
	            class:'btn-info',
	            name:"日志",
	            title:"启动日志",
	            icon:"fa fa-file-text",
	            click:function(row){
	            	 nowLog(row);
	            }
	        },
	        {
	            class:'btn-info',
	            name:"dump",
	            title:"查看dump文件",
	            icon:"fa fa-desktop",
	            click:function(row){
	            	showDump(row.port,row.clientId);
	            }
	        },
        ],
        operationEvents:[
    	{
       	 	class:"btn-info",icon:"fa fa-inbox",name:"GIT打包",id:"git",
       	 	click:function(rows){
       	 		gitGo($scope.app);
       	 	}
   	    },{
            class:"btn-info",icon:"fa fa-send",name:"部署",id:"deploy",
            click:function(rows){
            	var dataWithId = dataPreHandle(rows);
            	var ids = [];
            	for(var i in dataWithId){
            		ids.push(dataWithId[i].id);
            	}
                if(operationCheck("deploy",rows))
                	queryFile(ids,dataWithId,rows);
            }
        },{
        	class:"btn-info",icon:"fa fa-power-off",name:"启动",id:"start",
            click:function(rows){
            	var dataWithId = dataPreHandle(rows);
                if(operationCheck("start",rows))
                	deploy("start",'',rows);
            }
        },{
            class:"btn-info",icon:"fa fa-repeat",name:"重启",id:"restart",
            click:function(rows){
            	var dataWithId = dataPreHandle(rows);
                if(operationCheck("restart",rows))
                	deploy("restart",'',rows);
            }
        },{
            class:"btn-danger",icon:"fa fa-stop",name:"停止",id:"stop",
            click:function(rows){
                var dataWithId = dataPreHandle(rows);
                if(operationCheck("stop",rows))
                	deploy("stop",'',rows);
            }
        },/*{
            class:"btn-warning",
            icon:"fa fa-backward",
            name:"回退",
            click:function(rows){
            	var dataWithId = dataPreHandle(rows);
                if(operationCheck("rollback",rows))
                	deploy("rollback",'',rows);
            }
        },*/{
	       	 class:"btn-info",icon:"fa fa-desktop",name:"生成dump文件",id:"dump",
	         click:function(rows){
	        	 var dataWithId = dataPreHandle(rows);
	             if(operationCheck("dump",rows))
	             	deploy("dump",'',dataWithId);
	         }
	    }],
        settings:{
            pagerHide:false,
            filterId: "customDeployFilter",
            operationId: "deployOperationId",
            uploadId: "deployUploadId",
            theadId: "deployTheadId",
            showCheckBox:true,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-139-41-43-56-20-34)+"px",
            },
        },
        afterReload:function(){
            $scope.$emit("pageLoadSuccess",{});
        	//3个方块--220	查询条件--45	按钮---42		页码--60	2个padding--30
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight7+"px";
        	},200)
        	healthCheckHandle();
        },
        beforeReload:function(){
        	 $scope.monitor = {"status":{},'registStatus':{},"nginxStatus":{},"healthStatus":{},"healthDetail":{},"info":{},"deployStatus":{},"deployDetail":{}};
        }
    }
    
    //初始加载
    beforeLoad();
    
    //健康状态检查
    function healthCheckHandle(){
    	deployService.healthCheckAll(healthCheckList).then(function(response){
    		if(response.code== SUCCESS_CODE){
    			var respData = response.data;
    			for (var item of healthCheckList) {
    				$scope.monitor['healthStatus'][item.id] = respData[item.id];
    			}
    		}else{}
    	});
    }
    //传入后台的数据，加入了id值
    function dataPreHandle(rows){
        var sbInfo = new Array();
		for (var i in rows){
	    	var obj = new Object();
	    	obj.id = rows[i].id;
	    	sbInfo.push(obj);
		}
        return sbInfo;
    }
    
    //判定是否执行指令
    function operationCheck(type,rows){
    	var check = true;
    	if(rows.length == 0){
    		check = false;
    		dbUtils.error("请选择被操作对象！","提示");
    		return check;
    	}
    	//待后续添加各种操作的校验条件
		for (var i in rows){
	    	var status = rows[i].status;
	    	var name = rows[i].host + ":" + rows[i].port ;
	    	var id = rows[i].id;
	    	if(type == "start"){
	    		check = startCheck(status,name);
	    	}else if(type == "stop" || type == "dump"){
	    		check = stopCheck(status,name,type);
	    	}
	    	if(check == false){
	    		return check;
	    	}
		}
        return check;
    }
    
    //启动  已经启动不可再次启动
    function startCheck(status,name){
    	var check = true;
		if(status == 1){
			check = false;
			dbUtils.error(name+"已经启动！","提示");  
		}
        return check;
    }
    
    //停止  尚未启动不可停止 
    function stopCheck(status,name,type){
    	var check = false;
		if(status == 1){
			check = true;
		}else{
			if(type == "stop"){
				dbUtils.error(name+"尚未启动！无法停止","提示"); 
			}else if(type == "dump"){
				dbUtils.error(name+"尚未启动！无法生成dump文件","提示"); 
			}
		}
        return check;
    }
    
    //按钮
    function deploy(action,appFileId,instances){
        if(instances && instances.length != 0){
            dbUtils.confirm("确定继续操作？",function(){
				deployHandle(action,appFileId,instances);
            },function(){},true);
        }else{
        	dbUtils.error("请选择被操作对象","提示");
        }
    }
    
    function deployHandle(action,appFileId,instances){
    	deployService.customDeploy(action,$scope.appId,$scope.parallel,appFileId,JSON.stringify(instances)).then(function(response){
            if(response.code == SUCCESS_CODE){
        		echoLog.show(response.data,function(){
    				dbUtils.success("操作成功","提示");
    				$scope.table.operations.reloadData();
    				queryCountNum();
                });
            }else{
                dbUtils.error(response.message,"提示");
            }
        })
    }
    
    //查询部署文件
    function queryFile(ids,idData,rowData){
    	dbUtils.openModal('tpl/deploy/custom_choose_file.html','CustomdeployFileChooseController',"lg","",{"data":idData,"url":"/deploy/getDeployFiles","urlParams":{appId:$scope.appId,deployType:$scope.app.middlewareType,msIds:ids.join()},"type":'deploy'},function (data) {
    		if(data){
            	deploy("deploy",data[0].fileId,rowData);
        	}
        },true);
    }
    
    //查看dump文件
    function showDump(port,clientId){
    	dbUtils.openModal('tpl/deploy/dump_list.html','DumpListController',"lg","",{urlParams:{appName:$scope.app.name,port:port,clientId:clientId},"clientId":clientId},function (data) {},true);
    }
    
    //git
	function gitGo(app){
		dbUtils.openModal('tpl/deploy/git_package.html','GitPackageController',"lg","",{"app":angular.copy(app),"version":app.currentPackVersion},function (data) {
       	 	if(data){
	       	 	$.ajax({
	        		url: "/app/preview/"+$scope.appId,
	        		type: "get",
	        		async: false,
	        		dataType: 'json',
	        		success: function(response){
	        			$scope.app = response.data;
	        		}
	        	})
       	 	}
    	},true);
    }
    
    //实时日志
    function nowLog(row){
        deployService.queryCustomLog($scope.app,row,500).then(function(response){
            if(response.code == SUCCESS_CODE){
            	window.open("tpl/log/newTabLogWs.html?logId="+response.data+"&title="+($scope.app.name+"_"+row.ip+"_"+row.port));
            }else{
                dbUtils.error(response.message,"提示");
            }
        })
    }
    
	//文件上传方法
	$scope.uploadHandle = function(file){
        dbUtils.openModal('tpl/deploy/file_version.html','FileVersionController',"lg","",{"file":file, "version":$scope.app.currentPackVersion,"app":angular.copy($scope.app),"appType":1},function (data) {
        	if(data){
        		uploadFile(file,data.version,data.description,data.clients);
        	}
        });
	}
    
    //文件上传方法
    function uploadFile(file,version,description,clients){
    	var formData = new FormData();
        formData.append("file",file);
        formData.append("clients",JSON.stringify(clients));
        var deployType = 2;
        $scope.loading = true;
        $http({
            method:'post',data:formData,
            url:'/deploy/uploadFileCustom?appId='+$scope.appId+'&deployType='+deployType+"&currentPackVersion="+version+"&description="+description,
            headers:{'Content-Type':undefined} ,
            transformRequest:angular.identity//浏览器会帮我们把Content-Type 设置为 multipart/form-data.
        }).success(function(response){
        	 $scope.loading = false; 
        	 if(response.code == SUCCESS_CODE){
        		 dbUtils.success("上传成功","上传");
        		 echoLog.show(response.data+"",function(){
        			 beforeLoad();
        		 });
             }else if(response.code == 1110005){
                 dbUtils.error("请先部署之前上传的文件","提示");
             }else if(response.code == 1503){
                 dbUtils.error("文件路径匹配失败","提示");
             }else if(response.code == 1110007){
                 dbUtils.error("文件上传失败，请稍后再试","提示");
             }else{
                 dbUtils.error(response.message,"提示");
             }
        }).error(function(data){
        	$scope.loading = false; 
        	if(data == null){
        		dbUtils.error("上传失败","上传");
        	}else{
        		dbUtils.error(data.message,"提示");
        	}
        });
    }
    
    $scope.goBackSelectApp = function (){
    	$scope.$emit("deleteCurrentTab",{});
		$state.go(stateParams.fromWhere);
    }
}]);

app.controller('CustomdeployFileChooseController', ['dbUtils','$scope','$modalInstance', '$state','source','DeployService',
    function (dbUtils,$scope,$modalInstance, $state,source,deployService) {

    $scope.deployData = source.data;//发布页选中数据
    $scope.type = source.type;//deploy  自定义类型目前仅为deploy
    $scope.adminserverClient = source.adminserverClient;
    $scope.app = source.app;
    $scope.params = {editMode: false};
    let url = source.url;
    let urlParams = source.urlParams;

    var str = [];
    for (var p in urlParams) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(urlParams[p]));
    }
    url = url + "?" + str.join("&");

    /*ui-base-table*/
    $scope.fileChooseUrlParams = {url:url, urlBody:{}, method:"get"}
    $scope.tooltipData = {"status":{}};
    $scope.queryParams = {isPagination:true,postgroundPagination:false,pageNumber: 1,pageSize:10,prevPageDisabled: 'disabled',nextPageDisabled: 'disabled'};
    $scope.tableParams = {loading:true, bodyStyle:{"max-height": "500px"} };
    //部署的表格
    $scope.fileChooseHeaders = [
        {name:"文件名",field:"fileName",style:{width:"30%"}},
        {name:"状态",field:"isDeploy",style:{width:"10%"},compile:true,formatter:function(value,row){
            $scope.tooltipData.status[row.id] = row.isDeploy;
            return "<span class='label' ng-class='{0:\"bg-success\",1:\"bg-danger\"}[tooltipData.status[\""+row.id+"\"]]'>{{{0:\"存在\",1:\"已删除\"}[tooltipData.status[\""+row.id+"\"]]}}</span>";
            // return "<span class='label' ng-class='{0:\"bg-success\",1:\"bg-danger\"}["+row.isDeploy+"]'>{{{0:\"存在\",1:\"已删除\"}["+row.isDeploy+"]}}</span>";
        }},
        {name:"创建时间",field:"createTime",style:{width:"20%"}},
        {name:"上传人",field:"uploadUser",style:{width:"10%"}},{name:"版本",field:"version",style:{width:"10%"}},
        {name:"版本描述",field:"description",style:{width:"10%"},compile:true,formatter:function(value,row){
            let content = row.description?row.description:'--';
            return content;
        }}
    ];
    /*ui-base-table end*/

    $scope.submitApply = function(){
        $scope.$broadcast('getSelectedData',{})
    }

    //获取选中数据处理并返回
    $scope.$on('sendSelectedData',function(event,selectedData){
        if(!selectedData || selectedData.length == 0 || selectedData.length > 1){
            dbUtils.warning("请选择一条数据","提示");
            return;
        }
        selectedData = selectedData[0];
        if(selectedData.isDeploy == 1){//已删除禁止部署
            dbUtils.warning("禁止部署已删除版本","提示");
            return;
        }

        var id = selectedData.id;
        if($scope.type == 'rollback'){
            id = selectedData.fileName;
        }
        var data = deployData(id,$scope.deployData);
        $modalInstance.close(data);
    })

    function deployData(id,date){
        var deployDate = new Array();
        for (var i in date){
            var obj = new Object();
            obj = date[i];
            if($scope.type == 'rollback'){
                obj.fileVersion = id;
            }else{
                obj.fileId = id;
            }
            deployDate.push(obj);
        }
        return deployDate;
    }

    $scope.cancel = function () {
        $modalInstance.close(false);
    };

}]);
