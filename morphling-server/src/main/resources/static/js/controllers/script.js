'use strict';

// Script控制器
app.controller('ScriptListController', ['$scope','$rootScope','$modal','$state','dbUtils','$stateParams','EchoLog','$http','ScriptService',
                                       function($scope,$rootScope,$modal,$state,dbUtils,$stateParams,echoLog,$http,scriptService) {
	
	function setAllHidden(){
		$scope.isShowMain = false;
		$scope.isShowHistory = false;
		$scope.isShowRun = false;
	}
	setAllHidden();
	$scope.isShowMain = true;
	
	$scope.$on('returnMain',function(event,data){
		setAllHidden();
		$scope.isShowMain = true;
		if(data.reloadData)
			$scope.table.operations.reloadData();
	})
	
	$scope.scriptData = {"category":{},"type":{}};
	$scope.t_id = dbUtils.guid();
	
	function beforeReload(){
    	scriptService.getScriptType().then(function(response){
            if (response.code == SUCCESS_CODE) {
//            	$scope.category = {"01":"运维脚本","02":"巡检脚本"}; 
//                $scope.type = {"01":"shell","02":"python"}; 
            	$scope.scriptData.category = response.data.scriptCategory; 
            	$scope.scriptData.type = response.data.scriptType; 
//            	$scope.table.params[1].items = keyValue($scope.scriptData.category); 
//            	$scope.table.params[2].items = keyValue($scope.scriptData.type);
            }
      	})
    }
	
  	function keyValue(data){
		var returnData = [];
		for (var key in data) {
			var obj = {};
			obj.key = key;
			obj.value = data[key];
			returnData.push(obj);
		}
		return returnData;
    }

	var table = {
    	url:"/script/listAll",
    	showUpload: false,
        pageSql:true,
        params:[
            {name:"scriptName",label:"脚本名称",labelCols:4,cols:4,type:"text"},
//	        {name:"scriptCategory",label:"脚本类别",labelCols:4,cols:4,type:"select",items:[]},
//	        {name:"scriptType",label:"脚本类型",labelCols:4,cols:4,type:"select",items:[]}
        ],
        headers: [
            {name:"脚本名称",field:"scriptName",style:{width:"20%"}},
            {name:"脚本类别",field:"scriptCategory",style:{width:"13%"},compile:true,formatter:function(value,row){
            	var show = $scope.scriptData.category[value];     	
	            return show ;
	        }},
	        {name:"脚本类型",field:"scriptType",style:{width:"10%"},compile:true,formatter:function(value,row){
            	var show = $scope.scriptData.type[value];
	            return show ;
	        }},
            {name:"更新时间",field:"updateDate",style:{width:"15%"}},
            {name:"脚本说明",field:"scriptDesc",style:{width:"25%"}},
        ],
        operationEvents: [{
            class:"btn-info",
            icon:"glyphicon glyphicon-plus",
            name:"新增脚本",
            click:function(){
            	dbUtils.openModal('tpl/script/script_creat.html','ScriptCreatController',"lg","",{category:keyValue($scope.scriptData.category),type:keyValue($scope.scriptData.type),language:$scope.scriptData.type},function (data) {
        			if(data){
        				$scope.table.operations.reloadData()
        			}
        		});
            }
        }],
        rowEvents:[{
            class:"btn-info",
            icon2:"fa fa-play-circle",
            icon:"fa fa-pencil",
            title:"编辑/运行",
            name:"编辑/运行",
            click: function(row){
//            	$state.go("app.scriptEdit",{scriptId:row.scriptId});
            	setAllHidden();
            	$scope.isShowRun = true;
            	$scope.$broadcast("sendDataToRun",{scriptId:row.scriptId});
            }
        },{
            class:"btn-info",
            icon:"fa fa-file-text",
            title:"历史结果",
            name:"历史记录",
            click:function(row){
//            	$state.go("app.scriptHistory",{scriptId:row.scriptId});
            	setAllHidden();
            	$scope.isShowHistory = true;
            	$scope.$broadcast("sendDataToHistory",{scriptId:row.scriptId});
            }
        },{
        	class:"btn-danger",
            icon:"fa fa-trash",
            title:"删除",
            isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	scriptService.deleteScript(row.scriptId).then(function(response){
            		if(response.code == SUCCESS_CODE){
                        dbUtils.success("记录删除成功","提示");
                        $(event.target).parents(".popover").prev().click();
                        $scope.table.operations.reloadData()
                    }else {
                        dbUtils.error(response.message,"提示");
                    }
              	})
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        settings:{
            cols:2,
            filterId: "scriptListFilter",
            showCheckBox:false,
            pageSize:15,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-34-56-20)+"px",
            },
        },
        afterReload:function(){
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight2+"px";
        	},200)
        },
        beforeReload:function(){
        	beforeReload();
        }
    }
  	
	$scope.table = table;
	
}]);



// 创建
app.controller('ScriptCreatController', 
		['$scope','$rootScope','$modalInstance','$compile','dbUtils','source','ScriptService',
		 function($scope,$rootScope,$modalInstance,$compile,dbUtils,source,scriptService) {	
	
	// 控制显示“新建”还是“编辑”
	$scope.script = {savePath:"",timeOut:10};  
    
    $scope.category = source.category;
    $scope.type = source.type;
    $scope.language = source.language;
    
    var editor ="";
    if($scope.category.length >0){$scope.scriptCategory = $scope.category[0].key;}
    if($scope.type.length >0){$scope.scriptType = $scope.type[0].key;}
    setTimeout(function () { $("#scriptCategory").val($scope.scriptCategory);}, 200);
	setTimeout(function () { $("#scriptType").val($scope.scriptType);}, 200);
	setTimeout(function () {
		editor = ace.edit("pre_editor"); 
	    editor.setTheme("ace/theme/tomorrow_night_eighties");
	    editor.session.setMode("ace/mode/sh");
	    editor.$blockScrolling = Infinity;
	    editor.setOptions({
	   	    enableBasicAutocompletion: true,
	   	    enableSnippets: true,
	   	    enableLiveAutocompletion: true
	   });
	    editor.setValue("");
	    $scope.setLanguage($scope.scriptType);
	}, 200);
	
    $scope.setLanguage = function(data){
		var language = $scope.language[data];
		if(language == "shell"){
			language = 'ace/mode/' + 'sh';
		}else{
			language = 'ace/mode/' + language;
		}
		editor.session.setMode(language);
    }
    
    
    $scope.cancel = function () {
        $modalInstance.close(false);
    };    
       
    $scope.submitApply = function(){
        if($scope.form.$invalid){
            return;
        }
        if($scope.scriptCategory == null ||$scope.scriptCategory == "" ){
			dbUtils.error("请选择 脚本类别","提示");
            return;
        }else{ $scope.script.scriptCategory = $scope.scriptCategory; }
        if($scope.scriptType == null ||$scope.scriptType == "" ){
			dbUtils.error("请选择脚本类型","提示");
            return;
        }else{ $scope.script.scriptType = $scope.scriptType; }
        $scope.script.scriptContent = editor.getValue();
        if($scope.script.scriptContent == null ||$scope.script.scriptContent == "" ){
			dbUtils.error("请填写脚本内容","提示");
            return;
        }
    	scriptService.doSaveScript($scope.script).then(function(response){
    		if (response.code == SUCCESS_CODE) {
    			dbUtils.success("操作成功","提示");            
    			$modalInstance.close(response.data);
    		} else {
    			dbUtils.error(response.message,"提示");
    		}
    	})
    }   
}]);


//编辑
app.controller('ScriptEditController', 
		['$scope','$rootScope','$compile','dbUtils','$stateParams','EchoLog','$modal','$state','$interval','ScriptService',
		 function($scope,$rootScope,$compile,dbUtils,$stateParams,echoLog,$modal,$state,$interval,scriptService) {
	//初始化设置，否则会报错
	$scope.dataSource = [];		
	$scope.$on("sendDataToRun",function(event,data){
		$scope.script = {};
		$scope.category = []; 
		$scope.type = [];
		$scope.language = {};
		$scope.scriptId = Number(data.scriptId);
		
	    $scope.result = [];
	    $scope.health = {};
        $scope.healthDetail = {};
        
        $scope.clients = [];
		$scope.clientIdtoObj = {}; 
        /*transfer */
		$scope.dataSource = [];
		$scope.targetKeys = [];
		$("#script_transfer .transfer-list").css("width",Math.floor((($rootScope._screenProp.contentWidth - 20) - 20 - 30 -50)/2)+"px");
//		$("#script_transfer .transfer-list").css("height","250px");
		/*transfer */
		$scope.progressResults = [];
		getClient();
        $scope.showAce = true;
        setTimeout(initEditor,200);
        
	})
	$scope.isEleExist = false;
    var editor;
    
    function initEditor(){
    	editor = ace.edit("pre_code_editor_scriptedit"); //初始化对象
		editor.setTheme("ace/theme/tomorrow_night_eighties");//设置风格和语言
		editor.session.setMode("ace/mode/sh");
		editor.$blockScrolling = Infinity;
		editor.setOptions({
			enableBasicAutocompletion: true,
			enableSnippets: true,
			enableLiveAutocompletion: true
		});
		getScript();
    }
    
    $scope.transferBoxOption = {
        orientation : 'horizontal',
        render : function(item){
            return "主机名：" + item.name + '，IP：' + item.hostAddress + '，用户名：' + item.username;
        },
        titles : {
            origin : '未选主机',target : '已选主机'
        },
        showSearch : true,
        //classes : '',
        searchPlaceholder : '请输入搜索内容',
        notFoundContent : '找不到东西'
    };
    
  	// ----------------------------获取选值/左侧  start---------------------------------------------
    function getScript(){
    	scriptService.getScript($scope.scriptId).then(function(response){
            if (response.code == SUCCESS_CODE) {
            	$scope.script = response.data; 
            	$scope.script.timeOut = Number(response.data.timeOut); 
            	$scope.scriptCategory = response.data.scriptCategory;
            	$scope.scriptType = response.data.scriptType;
            	var domain = {};
            	for (var key in $scope.script) {
            		domain[key] = $scope.script[key];
        		}
            	$scope.scriptSave = domain;
            	editor.setValue($scope.script["scriptContent"]);
            } else {
            	$scope.script = {};
            }
      	})
      	
      	scriptService.getScriptType().then(function(response){
	        if (response.code == SUCCESS_CODE) {
	        	$scope.category = keyValue(response.data.scriptCategory); 
	        	$scope.type = keyValue(response.data.scriptType);
	        	$scope.language = response.data.scriptType;
	        } else {
	        	$scope.category = []; 
	        	$scope.type = [];
	        }
	  	})
    }
    
    $scope.setLanguage = function(data){
		var language = $scope.language[data];
		if(language == "shell"){
			language = 'ace/mode/' + 'sh';
		}else{
			language = 'ace/mode/' + language;
		}
		editor.session.setMode(language);
    }
  	
  	function keyValue(data){
		var returnData = [];
		for (var key in data) {
			var obj = {};
			obj.key = key;
			obj.value = data[key];
			returnData.push(obj);
		}
		return returnData;
    }
    
    $scope.submitApply = function(){
        if($scope.form.$invalid){
            return;
        }
        if($scope.scriptCategory == null ||$scope.scriptCategory == "" ){
			dbUtils.error("请选择 脚本类别","提示");
            return;
        }else{ $scope.script.scriptCategory = $scope.scriptCategory; }
        if($scope.scriptType == null ||$scope.scriptType == "" ){
			dbUtils.error("请选择脚本类型","提示");
            return;
        }else{ $scope.script.scriptType = $scope.scriptType; }
        $scope.script["scriptContent"] = editor.getValue();
    	scriptService.doSaveScript($scope.script).then(function(response){
          if (response.code == SUCCESS_CODE) {
              dbUtils.success("操作成功","提示");            
              getScript();
          } else {
              dbUtils.error(response.message,"提示");
          }
    	}) 
    }  
    
    // -----------------------------获取选值/左侧   end------------------------------------------
    
  	// ----------------------------获取选值/右侧  start---------------------------------------------
    function getClient(){
    	$scope.health = {};
    	$scope.healthDetail = {};
    	scriptService.getClient(null).then(function(response){
            if (response.code == SUCCESS_CODE) {
        	    $scope.clients = response.data;
        	    
        	    //测试ui-grid
        	    $scope.showUiGrid = false;
        	    $scope.gridOptions = scriptService.gridOptions; 
        	    $scope.gridOptions.data = $scope.clients;
        	    //测试ui-grid end
        	    
        	    $scope.dataSource = $scope.clients.map( item =>  {
        	    	item = angular.extend(item,{key:item.id});
        	    	$scope.clientIdtoObj[item.id] = item;
        	    	return item;
        	    });
            } else {
            	$scope.clients = {};
            }
    	})
    }
	    
    $scope.doScript = function(){
    	if(angular.isUndefined($scope.targetKeys) || $scope.targetKeys.length == 0){
        	dbUtils.info("请至少选择一个主机");
            return;
        }
    	
        $scope.script["scriptContent"] = editor.getValue();
        var isModify = angular.equals($scope.script,$scope.scriptSave);
        let isBusy = false,busyMsg = "";
        
        for(let key of $scope.targetKeys){
        	let client = $scope.clientIdtoObj[key];
        	if(client.isBusy &&　client.isBusy　== true){
        		isBusy = true;
        		busyMsg += client.name + "正在执行，不能重复执行<br>";
				continue; 
			}else{
				let result_length = addDataToExecuteResults(client);
    			var scriptVO = {client:client, parallel:false, scriptInfo:$scope.script, isModify: isModify ? 1 : 0};
    	        scriptService.doClient(scriptVO).then(function(response){
    	            if (response.code == SUCCESS_CODE) {
    	            	updateStatus(result_length,response.data,client);//{status,result}
    	            } else {
    	            	dbUtils.error(response.message,"提示");      
    	            }
    	      	},function(result){})	
			}
        }
        if(isBusy)
        	dbUtils.warning(busyMsg,"提示");      
    }  
    
    //显示执行结果表格
    function addDataToExecuteResults(client){
    	client.isBusy = true;//为主机赋繁忙的标志，防止重复运行
		let result = {clientId:client.id,status:2,scriptName:$scope.script.scriptName,clientIp:client.hostAddress,progressNum:"0%",clientUsername:client.username,createTime:new Date().format("yyyy-MM-dd hh:mm:ss"),logId:""};
		$scope.progressResults.push(result);
		return $scope.progressResults.length;
    }
    
    //更新状态
    function updateStatus(length,data,client){
    	client.isBusy = false;
    	let num = 0;
    	if(parseInt(data.status) == 1 || parseInt(data.status) == 3){
    		num = 100;
    	}
    	$scope.progressResults[length-1].progressNum = num+"%";
    	$scope.progressResults[length-1].progressStyle = {width:num+"%"};
    	$scope.progressResults[length-1].status = data.status;
    	$scope.progressResults[length-1].content = data.result;
    }
	    
    $scope.show = function(row){
    	dbUtils.openModal('tpl/logtable/show_logs.html','ScriptDetailController',"lg","",{"data":row},function (data) {},true);
    }

    $scope.goBackScript = function (){
//    	$state.go("app.script");
    	$scope.showAce = false;
    	$scope.$emit("returnMain",{reloadData:true});
    }

}]);


app.controller('ScriptDetailController', ['dbUtils','$scope','$modalInstance', '$state','source','$http', 
                                      function (dbUtils,$scope,$modalInstance, $state,source,$http) {
	  $scope.title = "显示结果详情";
	  $scope.showLogsDetail = source.data.content;
	
	  $scope.cancel = function () {
	      $modalInstance.close(false);
	  };
	  
	  $scope.submitApply = function(){
	      $modalInstance.close();
	  }
  
}]);

//编辑
app.controller('ScriptHistoryController', 
		['$scope','$rootScope','$compile','dbUtils','$stateParams','EchoLog','$modal','$state','$interval','ScriptService','$timeout',
		 function($scope,$rootScope,$compile,dbUtils,$stateParams,echoLog,$modal,$state,$interval,scriptService,$timeout) {

	$scope.$on("sendDataToHistory",function(event,data){
		$scope.scriptId = Number(data.scriptId);
		scriptService.getScript($scope.scriptId).then(function(response){
			if (response.code == SUCCESS_CODE) {
				$scope.script = response.data; 
				$scope.queryOK = true;
				$scope.table  = table;
				$scope.table.params = [{name:"scriptName",label:"文件名",type:"text",initValue:$scope.script.scriptName}];
			} else {
				$scope.script = {};
			}
		})
	})
	$scope.t_id = dbUtils.guid();
  	
  	var table = {
    	url:"/logs/getHistoryLogs",
    	pageSql:true,
    	hideQuery:true,
        headers: [
            {name:"运行脚本",field:"serviceName"},
            {name:"ip",field:"clientIp"},
            {name:"执行用户",field:"clientUsername"},
            {name:"执行状态",field:"status",formatter:function(value,row){
            	var show = {'0':'执行成功','1':'执行失败','2':'执行中','3':'执行结束'}[row.status];
	            return show ;
	        }},
            {name:"执行脚本时间",field:"createTime"},
        ],
        rowEvents:[
        {
            class:"btn-info",
            icon:"fa fa-search-plus",
            title:"查看详情",
            click: function(row){
            	var instance = $modal.open({
        	        animation: true,
        	        templateUrl: 'tpl/logtable/show_logs.html',
        	        controller: 'ScriptDetailController',
        	        size: "lg",
        	        backdrop: "true",
        	        resolve: {
        	            source: function () {
        	                return {"data":row};
        	            }
        	        }
        	    });
            }                        
            
        }],
        beforeReload:function(){ },
        settings:{
        	filterId: "scriptHistoryFilter",
        	pageSize:15,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-40-41-34-56-20-15)+"px",
            },
        },
        afterReload:function(){
        	//查询条件--45 按钮--38 2个padding--30
			$timeout(function () {
				$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight9+"px";
			})
        	// setTimeout(function(){
        	// 	$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight9+"px";
        	// },200)
        }
    }
    
    // ----------------------------返回初始页面 start---------------------------------------------

    $scope.goBackScript = function (){
//    	$state.go("app.script");
  		$scope.queryOK = false;
  		$scope.$emit("returnMain",{});
    }
    
    // -----------------------------返回初始页面  end------------------------------------------

}]);
