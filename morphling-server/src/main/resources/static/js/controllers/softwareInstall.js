'use strict';

// 软件安装list控制器
app.controller('SoftwareInstallListController', ['$scope','$rootScope','$modal','$state','dbUtils','$stateParams','EchoLog','$http','SoftwareInstallService',
                                       function($scope,$rootScope,$modal,$state,dbUtils,$stateParams,echoLog,$http,softwareService) {
	
	function setAllHidden(){
		$scope.isShowMain = false;
		$scope.isShowConfig = false;
	}
	setAllHidden();
	$scope.isShowMain = true;
	
	$scope.$on('returnMain',function(event,data){
		setAllHidden();
		$scope.isShowMain = true;
		if(data.reloadData)
			$scope.table.operations.reloadData();
	})
	
	$scope.t_id = dbUtils.guid();
	
	var table = {
    	url:"/script/listAll",
    	showUpload: false,
        pageSql:true,
        /*params:[
            {name:"scriptName",label:"脚本名称",labelCols:4,cols:4,type:"text"},
        ],*/
        headers: [
            {name:"名称",field:"scriptName",style:{width:"20%"}},
            // {name:"软件类型",field:"scriptCategory",style:{width:"13%"}},
            {name:"备注",field:"scriptDesc",style:{width:"25%"}},
            {name:"安装时间",field:"updateDate",style:{width:"15%"}},
        ],
        operationEvents: [{
            class:"btn-info",
            icon:"glyphicon glyphicon-plus",
            name:"新建安装配置",
            click:function(){
            	setAllHidden();
            	$scope.isShowConfig = true;
            	//传递的数据，新建可能不需要传值，编辑的时候需要传
            	$scope.$broadcast("sendDataToConfig",{});
            }
        }],
        rowEvents:[{
            class:"btn-info",
            icon2:"fa fa-play-circle",
            icon:"fa fa-pencil",
            title:"编辑",
            name:"编辑",
            click: function(row){
            	setAllHidden();
            	$scope.isShowConfig = true;
            	//传递的数据，新建可能不需要传值，编辑的时候需要传
            	$scope.$broadcast("sendDataToConfig",{scriptId:row.scriptId});
            }
        },{
            class:"btn-info",
            icon:"fa fa-file-text",
            title:"历史结果",
            name:"历史记录",
            click:function(row){
            	dbUtils.info("暂未开放");
//            	setAllHidden();
//            	$scope.isShowHistory = true;
//            	$scope.$broadcast("sendDataToHistory",{scriptId:row.scriptId});
            }
        },{
        	class:"btn-danger",
            icon:"fa fa-trash",
            title:"删除",
            isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	dbUtils.info("暂未开放");
            	/*scriptService.deleteScript(row.scriptId).then(function(response){
            		if(response.code == SUCCESS_CODE){
                        dbUtils.success("记录删除成功","提示");
                        $(event.target).parents(".popover").prev().click();
                        $scope.table.operations.reloadData()
                    }else {
                        dbUtils.error(response.message,"提示");
                    }
              	})*/
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        settings:{
            cols:2,
            filterId: "softwareInstallListFilter",
            showCheckBox:false,
            pageSize:15,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-34-56-20)+"px",
            },
        },
        afterReload:function(){
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight1+"px";
        	},200)
        },
        beforeReload:function(){}
    }
  	
	$scope.table = table;
	
}]);

// 软件安装配置
app.controller('SoftwareInstallConfigController', ['$scope','$rootScope','$compile','dbUtils','SoftwareInstallService',
		 function($scope,$rootScope,$compile,dbUtils,softwareService) {	
	
	$scope.clientDataSource = [];//预防filter的报错
	$scope.timers = [];
	
	$scope.$on("sendDataToConfig",function(event,data){
		if(!data.id){
			$scope.isEdit = false;
			$scope.softwareConfig = {file:{},script:{}};//初始化
			$scope.fileProgressResults = [];
			$scope.scriptProgressResults = [];
		}else{
			$scope.isEdit = true;
			$scope.softwareConfig = data;
			$scope.softwareConfig.file = {};
			$scope.softwareConfig.script = {};
			//已选文件赋值处理
			$scope.fileProgressResults = [];
			$scope.scriptProgressResults = [];
		}
		
		$scope.selectFiles = [];
		$scope.selectScripts = [];
        $scope.clients = [];
		$scope.clientIdtoObj = {}; 
		//加载主机和穿梭框，为已选主机赋值
		initClientAndTransfer();
	})  
	
	//加载主机穿梭框配置
	$scope.clientTransferBoxOption = softwareService.clientTransferBoxOption();
	
	//文件选择
	$scope.chooseFile = function(){
		dbUtils.openModal('tpl/software_install/file_choose.html','FileChooseController',"lg","",{},function (data) {
			if(data){
				$scope.selectFiles = angular.copy(data);
				softwareService.reloadFileTableNoClient($scope);
			}
		});
	}
	
	//新文件上传
	$scope.uploadFile = function(){
		var fileUploadController = function(dbUtils,$scope,$modalInstance, $state,source,$http){
			$scope.submitApply = function(){
				$scope.$broadcast("getUploadFilesHandle",{})
			}
			
			$scope.$on("receiveFileHandle",function(event,data){
				$modalInstance.close(data.files);
			})
			
			$scope.cancel = function () {
				$modalInstance.close(false);
			};
		}
		dbUtils.openModal('tpl/software_install/file_upload.html',fileUploadController,"lg","modal-large",{},function (data) {
			if(data){
				$scope.selectFiles = $scope.selectFiles.concat(data);
				softwareService.reloadFileTableNoClient($scope);
			}
		});
	}
	
	//文件分发
	$scope.distributeHandle = function(){
		var check = softwareService.distributeCheck($scope);
		if(!check)
			return;
		$scope.fileProgressResults = [];
		for(let file of $scope.selectFiles){
			var fileInfo = softwareService.initFileInfo($scope,file);
			softwareService.distributeFile($scope.clientTargetKeys,fileInfo).then(function(response){
	            if (response.code == SUCCESS_CODE) {
	            	$scope.logIds = response.data;
	            	file.fileAbsoPath = fileInfo.filePath + "/" + file.fileName;
	            	softwareService.reloadFileTableWithClient($scope,file);
	            } else {
	            	dbUtils.error(response.message,"提示");
	            }
	      	})
		}
	}
	
	$scope.showResultLog = function(row){
		softwareService.showResultLog(row);
	}
	
	//取消文件分发
	$scope.cancelDistribute = function(row){
    	//1.取消定时查询
    	softwareService.clearTimers(row.timer,$scope.timers);
    	row.status = 3;
    	//2.向后台发送取消请求
    	softwareService.cancelTask(row.fileName,$scope.clientIdtoObj[row.clientId]);
    }
	
	//脚本选择
	$scope.chooseScript = function(){
		dbUtils.openModal('tpl/software_install/script_choose.html','ScriptChooseController',"lg","",{},function (data) {
			if(data){
				$scope.selectScripts = angular.copy(data);
				softwareService.reloadScriptTableNoClient($scope);
			}
		},true);
	}
	
	//新建脚本
	$scope.createScript = function(){
		if(!$scope.scriptData){
			softwareService.getScriptTypeData($scope,createScriptHandle)
		}else{
			createScriptHandle();
		}
	}
	
	function createScriptHandle(){
		let types = [];
		for (var key in $scope.scriptData.type) {
			types.push({key:key,value:$scope.scriptData.type[key]});
		}
		dbUtils.openModal('tpl/script/script_creat.html','ScriptCreatController',"lg","",{category:$scope.scriptData.category,type:types,language:$scope.scriptData.type},function (data) {
			if(data){
				$scope.selectScripts = $scope.selectScripts.concat(data);
				softwareService.reloadScriptTableNoClient($scope);
			}
		});
	}
	
	//脚本执行
	$scope.executeHandle = function(){
		var check = softwareService.executeCheck($scope);
		if(!check)
			return;
		$scope.scriptProgressResults = [];
		for(let script of $scope.selectScripts){
			for(let clientId of $scope.clientTargetKeys){
				let client = $scope.clientIdtoObj[clientId];
				
				let result_length = softwareService.reloadScriptTableWithClient($scope,client,script);
				
				var scriptVO = {client:client, parallel:false, scriptInfo:script, isModify: 0};
				
				softwareService.executeScript(scriptVO).then(function(response){
    	            if (response.code == SUCCESS_CODE) {
    	            	softwareService.updateExecuteStatus($scope,result_length,response.data,client);//{status,result}
    	            } else {
    	            	dbUtils.error(response.message,"提示");      
    	            }
    	      	},function(result){})	
			}
		}
	}
	
	$scope.showContent = function(row){
    	dbUtils.openModal('tpl/logtable/show_logs.html','DetailShowController',"lg","",{"data":row},function (data) {},true);
    }
	
	function initClientAndTransfer(){
		$scope.clientDataSource = [];
		$scope.clientTargetKeys = [];
		softwareService.getClient(null).then(function(response){
            if (response.code == SUCCESS_CODE) {
        	    $scope.clients = response.data;
        	    $scope.clientDataSource = $scope.clients.map( item => {
        	    	$scope.clientIdtoObj[item.id] = item;
        	    	return angular.extend(item,{key:item.id})
        	    });
        	    if($scope.isEdit){
        	    	//编辑状态下的主机处理
//        	    	$scope.clientTargetKeys = [476,477];
        	    }
        	    $("#client_transfer .transfer-list").css("width",Math.floor((($rootScope._screenProp.contentWidth - 20)*11/12 - 20 - 30 -50)/2)+"px");
            } else {
            	dbUtils.warning(response.message,"提示");
            }
    	})
    }
    
    $scope.returnMain = function (){
    	$scope.timers.forEach(function(item){
        	clearInterval(item);
        })
        $scope.timers = [];
    	$scope.$emit("returnMain",{reloadData:true});
    }
    
}]);

//文件选择控制器
app.controller('FileChooseController', ['dbUtils','$scope','$modalInstance', '$state','source','$http', 
                                      function (dbUtils,$scope,$modalInstance, $state,source,$http) {
	
	$scope.queryParams = {isPagination:true,postgroundPagination:false,pageNumber: 1,pageSize:10,prevPageDisabled: 'disabled',nextPageDisabled: 'disabled'};
    $scope.initTable = function(){
		$scope.fileChooseUrlParams = {url:"/file/manage/listAllOnServer", urlBody:{}, method: "get"};
		$scope.fileChooseTableParams = {bodyStyle:{"max-height": "500px"}};
    	$scope.fileChooseHeaders = [
			{name:"文件名",field:"fileName"},{name:"文件大小",field:"fileSize",compile:true,formatter:function(value,row){
				return dbUtils.convertFileUnit(value);
			}},{name:"创建时间",field:"createTime"}
		];
    }

    $scope.initTable();
    
	$scope.submitApply = function(){
		$scope.$broadcast("getSelectedData",{});
	}
	
	$scope.$on("sendSelectedData",function(event,data){
		if(!data || data.length == 0){
			dbUtils.warning("请选择一条数据","提示");
			return;
		}
		$modalInstance.close(data);
	})
	  
	$scope.cancel = function () {
		$modalInstance.close(false);
	};
  
}]);

//脚本选择控制器
app.controller('ScriptChooseController', ['dbUtils','$scope','$modalInstance', '$state','source','$http', 'SoftwareInstallService',
                                      function (dbUtils,$scope,$modalInstance, $state,source,$http,softwareService) {
	
	$scope.scriptData = {}
	softwareService.getScriptType().then(function(response){
        if (response.code == SUCCESS_CODE) {
        	$scope.scriptData.category = response.data.scriptCategory; 
        	$scope.scriptData.type = response.data.scriptType; 
        }
  	})
	
	$scope.queryParams = {isPagination:true,postgroundPagination:true,pageNumber: 1,pageSize:10,prevPageDisabled: 'disabled',nextPageDisabled: 'disabled'};
    $scope.initTable = function(){
		$scope.scriptChooseUrlParams = {url:"/script/listAll", urlBody:{}, method:"get"};
		$scope.scriptChooseTableParams = {bodyStyle:{"max-height": "500px"}};

    	$scope.scriptChooseHeaders = [
			{name:"脚本名称",field:"scriptName",style:{width:"30%"}},{name:"脚本类别",field:"scriptCategory",style:{width:"10%"},compile:true,formatter:function(value,row){
				var show = $scope.scriptData.category[value];     	
	            return show ;
			}},
			{name:"脚本类型",field:"scriptType",style:{width:"10%"},compile:true,formatter:function(value,row){
            	var show = $scope.scriptData.type[value];
	            return show ;
	        }},
			{name:"更新时间",field:"updateDate",style:{width:"20%"}},
            {name:"脚本说明",field:"scriptDesc",style:{width:"30%"}},
		];
    }

    $scope.initTable();
    
	$scope.submitApply = function(){
		$scope.$broadcast("getSelectedData",{});
	}
	
	$scope.$on("sendSelectedData",function(event,data){
		if(!data || data.length == 0){
			dbUtils.warning("请选择一条数据","提示");
			return;
		}
		$modalInstance.close(data);
	})
	  
	$scope.cancel = function () {
		$modalInstance.close(false);
	};
  
}]);

app.controller('DetailShowController', ['dbUtils','$scope','$modalInstance', '$state','source','$http', 
    function (dbUtils,$scope,$modalInstance, $state,source,$http) {
	
	$scope.title = "显示结果详情";
	$scope.showLogsDetail = source.data.content;
	
	$scope.cancel = function () {
		$modalInstance.close(false);
	};
	
	$scope.submitApply = function(){
		$modalInstance.close();
	}

}]);
