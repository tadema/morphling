'use strict';

app.controller('FileUploaderFilelistController', ['$scope','$rootScope','$modal','$compile','EchoLog','dbUtils','$state','$stateParams','FileUploader',
                                    function($scope,$rootScope,$modal,$compile,echoLog,dbUtils,$state,$stateParams,FileUploader) {
	
	$scope.uploadClick = function(event){
		$(event.target).next().click();
	}
	
	$scope.tableStyle = {
			"height":$rootScope._screenProp.contentHeight - 10 -200+ "px"
	}
	
	$scope.uploader = new FileUploader({
        url: '/file/manage/uploadFiles?isDuplicate=1',//url表示所要call的api路径
      //alias 表示对面的后端file参数的名字
    	/*alias: "xlsFile",*/
    	//headers里面可以加入各种http请求参数，如常用的token的值
        headers: [
        	{'Content-Type':'application/x-www-form-urlencoded'},
        	{'Access-Control-Allow-Origin':'*'}
        ],
    });
	
	//filter是对你文件的过滤进行的处理，这个功能是在所有功能之前执行。在Html中配置filters="uploadeFilter"
	// a sync filter
//	$scope.uploader.filters.push({
//        name: 'syncFilter',
//        fn: function(item /*{File|FileLikeObject}*/, options) {
//            console.log('syncFilter');
//            return this.queue.length < 10;
//        }
//    });
  
    // an async filter
//	$scope.uploader.filters.push({
//        name: 'asyncFilter',
//        fn: function(item /*{File|FileLikeObject}*/, options, deferred) {
//            console.log('asyncFilter');
//            setTimeout(deferred.resolve, 1e3);
//        }
//    });

    // CALLBACKS
	$scope.uploader.onWhenAddingFileFailed = function(item, filter, options) {
//        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    $scope.uploader.onAfterAddingFile = function(fileItem) {//添加文件到上传队列后
        // console.info('onAfterAddingFile', fileItem);
        clearUploader();
    };
    $scope.uploader.onAfterAddingAll = function(addedFileItems) {
//        console.info('onAfterAddingAll', addedFileItems);
    };
    $scope.uploader.onBeforeUploadItem = function(item) {
    	var formData = new FormData();
        formData.append("file",item._file);
        Array.prototype.push.apply(item.formData, formData);
//        console.info('onBeforeUploadItem', item);
    };
    $scope.uploader.onProgressItem = function(fileItem, progress) {
    	if(progress == 100)
    		fileItem.isSuccess = false;
//        console.info('onProgressItem', fileItem, progress);
    };
    $scope.uploader.onProgressAll = function(progress) {
//        console.info('onProgressAll', progress);
    };
    $scope.uploader.onSuccessItem = function(fileItem, response, status, headers) {
    	if(response.code == SUCCESS_CODE)
    		fileItem.isSuccess = true;
    	else
    		fileItem.isError = true;
//        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    $scope.uploader.onErrorItem = function(fileItem, response, status, headers) {
//        console.info('onErrorItem', fileItem, response, status, headers);
    };
    $scope.uploader.onCancelItem = function(fileItem, response, status, headers) {
//        console.info('onCancelItem', fileItem, response, status, headers);
    };
    $scope.uploader.onCompleteItem = function(fileItem, response, status, headers) {
//        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    $scope.uploader.onCompleteAll = function() {
//        console.info('onCompleteAll');
    };

    function clearUploader(){
        if($("#singleUploader").val() != ""){    //当file有地址才进行清空
            $("#singleUploader").val("");
        }
        if($("#multiUploader").val() != ""){    //当file有地址才进行清空
            $("#multiUploader").val("");
        }
        if($("dropUploader").val() != ""){    //当file有地址才进行清空
            $("dropUploader").val("");
        }
    }

    $scope.returnMainPage = function(){
    	$scope.$emit("returnMain",{reloadData:true});
    }
//    console.info('uploader', $scope.uploader);
}]);

app.controller('FileUploaderSoftwareInstallController', ['$scope','$rootScope','$modal','$compile','EchoLog','dbUtils','$state','$stateParams','FileUploader',
    function($scope,$rootScope,$modal,$compile,echoLog,dbUtils,$state,$stateParams,FileUploader) {
	
	$scope.uploadClick = function(event){
		$(event.target).next().click();
	}
	
	$scope.tableStyle = {
		"height":$rootScope._screenProp.contentHeight - 10 -200+ "px"
	}
	
	$scope.uploader = new FileUploader({
        url: '/file/manage/uploadFiles?isDuplicate=1',//url表示所要call的api路径
      //alias 表示对面的后端file参数的名字
    	/*alias: "xlsFile",*/
    	//headers里面可以加入各种http请求参数，如常用的token的值
        headers: [
        	{'Content-Type':'application/x-www-form-urlencoded'},
        	{'Access-Control-Allow-Origin':'*'}
        ],
    });

    // CALLBACKS
	$scope.uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
//        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    $scope.uploader.onAfterAddingFile = function(fileItem) {//添加文件到上传队列后
//        console.info('onAfterAddingFile', fileItem);
        clearUploader();
    };
    $scope.uploader.onAfterAddingAll = function(addedFileItems) {
//        console.info('onAfterAddingAll', addedFileItems);
    };
    $scope.uploader.onBeforeUploadItem = function(item) {
    	var formData = new FormData();
        formData.append("file",item._file);
        Array.prototype.push.apply(item.formData, formData);
//        console.info('onBeforeUploadItem', item);
    };
    $scope.uploader.onProgressItem = function(fileItem, progress) {
    	if(progress == 100)
    		fileItem.isSuccess = false;
//        console.info('onProgressItem', fileItem, progress);
    };
    $scope.uploader.onProgressAll = function(progress) {
//        console.info('onProgressAll', progress);
    };
    $scope.uploader.onSuccessItem = function(fileItem, response, status, headers) {
    	if(response.code == SUCCESS_CODE){
    		fileItem.isSuccess = true;
    		// console.log(response.data)
    		fileItem.fileId = response.data[0].fileInfo.fileId;
    	}
    	else
    		fileItem.isError = true;
//        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    $scope.uploader.onErrorItem = function(fileItem, response, status, headers) {
//        console.info('onErrorItem', fileItem, response, status, headers);
    };
    $scope.uploader.onCancelItem = function(fileItem, response, status, headers) {
//        console.info('onCancelItem', fileItem, response, status, headers);
    };
    $scope.uploader.onCompleteItem = function(fileItem, response, status, headers) {
//        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    $scope.uploader.onCompleteAll = function() {
//        console.info('onCompleteAll');
    };

    function clearUploader(){
        if($("#singleUploader").val() != ""){    //当file有地址才进行清空
            $("#singleUploader").val("");
        }
        if($("#multiUploader").val() != ""){    //当file有地址才进行清空
            $("#multiUploader").val("");
        }
        if($("dropUploader").val() != ""){    //当file有地址才进行清空
            $("dropUploader").val("");
        }
    }
    
    $scope.$on("getUploadFilesHandle",function(evevnt,data){
    	let uploadFiles = [];
    	for(let item of $scope.uploader.queue){
    		if(item.isSuccess){
    			let file = {fileName:item._file.name,fileSize:item._file.size,fileId:item.fileId};//需要文件的其他信息，在这里继续添加
    			uploadFiles.push(file);
    		}
    	}
    	$scope.$emit("receiveFileHandle",{"files":uploadFiles});
    })

}]);