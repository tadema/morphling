'use strict';

(function() {
	window.onbeforeunload = function() {
		//关闭websocket
		if(!logId)
			return;
		$.ajax({  
            type: "get", 
            url: "/logs/closeLog", 
            data: {logId:logId},  
            contentType: "application/json;charset=utf-8",  
            dataType: "json",  
            success: function (response, ifo) {  
            	
            }, error: function () {  
                alert("error");
            }
		})
	}
		
	function getQueryString(name){
	     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	     var r = window.location.search.substr(1).match(reg);
	     if(r!=null)return  unescape(r[2]); return null;
	}
	var logId = getQueryString("logId");
	var title = getQueryString("title");
	if(title)
		document.title = title;
	// 调用方法
	var websocket = new WebSocket("ws://"+window.location.host+"/echo?logId="+logId);

	var printing = false;

	var logInternal = setInterval(function(){
		var e = document.getElementById("log_container");
		if(printing){//&& angular.element("#scrollControl")[0].checked
			e.scrollTop=e.scrollHeight;
			printing = false;
		}
	},200);
  
	websocket.onopen = function(event) {
	
	}


  // 监听消息
	websocket.onmessage = function(event) {
		printing = true;
		log(event.data);
	};

  	// 监听Socket的关闭
  	websocket.onclose = function(event) {
		window.clearInterval(logInternal);
  	};
  
	function log(line){
		var e = document.getElementById("log_container");
		e.innerHTML = e.innerHTML + line + "<br/>";
	}
})();
