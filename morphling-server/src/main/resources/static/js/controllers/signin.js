'use strict';

/* Controllers */
// signin controller
app.controller('SigninFormController', ['$rootScope', '$scope', '$state', '$cookieStore','AccountService','$modal','dbUtils','$window', function ($rootScope, $scope, $state,$cookieStore, accountService,$modal,dbUtils,$window) {
    //默认自动登录为true
    $scope.user={rememberMe:true};
    $scope.codeurl = accountService.getValidImgCode();
    $('body').css("background","url('img/bg02.jpg')no-repeat");
    $('body').css("background-size","100% 100%");
    $scope.login = function () {
        $scope.errInfo = null;
        $scope.errCode = null;
            accountService.login($scope.user).then(function (response) {
            if (response.code == SUCCESS_CODE) {
                $rootScope._userinfo = response.data;
                $rootScope._userinfo.currentEnv = accountService.getEnv($rootScope._userinfo.envs);
                dbUtils.success("登录成功<br><h5>License有效期："+$rootScope._userinfo.licenseStartTime+"--"+$rootScope._userinfo.licenseEndTime+"</h5>","提示");
                setTimeout(function(){
                	$("#toast-container .toast").css("width","500px");
                },100)
                $('body').css("background","url('')");
                $('body').css("background-color","transparent");
                /*$window.localStorage["tabs"] = null;
        		$window.localStorage["tabIndex"] = null;*/
                $window.sessionStorage.removeItem("tabs");
            	$window.sessionStorage.removeItem("tabIndex");
                $state.go("app.home");
            } else {
                $scope.errInfo = response.message;
                $scope.errCode = response.code;
                if(response.code != 1101){
                	$scope.codeurl = accountService.getValidImgCode();//刷新验证码
                }
            }
        });
    };
    
    $scope.refreshCaptcha = function () {
        $scope.codeurl = accountService.getValidImgCode();
    }

}]);

