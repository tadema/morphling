'use strict';

// Script控制器
app.controller('FileListController', ['$scope','$rootScope','$modal','$state','dbUtils','$stateParams','EchoLog','$http','FileListService',
                                       function($scope,$rootScope,$modal,$state,dbUtils,$stateParams,echoLog,$http,fileListService) {
		function setAllHidden(){
			$scope.isShowMain = false;
			$scope.isShowUpload = false;
			$scope.isShowDistribute = false;
			$scope.isShowHistory = false;
		}
		setAllHidden();
		$scope.isShowMain = true;
		
		$scope.$on('returnMain',function(event,data){
			setAllHidden();
			$scope.isShowMain = true;
			if(data && data.reloadData)
				$scope.table.operations.reloadData();
		})
		
	   $scope.t_id = dbUtils.guid();	
	   var table = {
	        url:"/file/manage/listAllOnServer",
	        showUpload:false,
//	        params:null,
	        headers: [
				{name:"文件名",field:"fileName",style:{"width":"35%"}},
				{name:"文件大小",field:"fileSize",style:{"width":"10%"},compile:true,formatter:function(value,row){
					row.convertFileSize = dbUtils.convertFileUnit(value);
					return row.convertFileSize;
				}},
//				{name:"文件类型",field:"fileType",style:{"width":"10%"}},
				{name:"创建用户",field:"createUsername",style:{"width":"15%"}},
//				{name:"MD5值",field:"mdfiveValue",style:{"width":"20%"}},
				{name:"创建时间",field:"createTime",style:{"width":"20%"}},
	        ],
	        operationEvents: [/*{
	            class:"btn-info",
	            icon:"glyphicon glyphicon-plus",
	            name:"上传文件",
	            click:function(){
	            	$state.go("app.fileEdit",{file:null,type:'create'});
	            }
	        },*/{
	            class:"btn-info",
	            icon:"fa fa-upload",
	            name:"上传文件",
	            click:function(){
	            	setAllHidden();
	            	$scope.isShowUpload = true;
	            }
	        },{
	        	class:"btn-info",
	        	icon:"fa fa-sign-out",
	        	name:"分发文件",
	            click: function(rows){
	            	if(rows.length == 0){
	            		dbUtils.warning("请选择一条数据","提示");
	            		return;
	            	}
	            	setAllHidden();
	            	$scope.isShowDistribute = true;
	            	for(let file of rows){
	            		file.status = "100%";
	            		file.listId = 1;
	            	}
	            	$scope.$broadcast("sendDataToDistribute",{files:rows,type:'edit'})
//	            	$state.go("app.fileEdit",{file:file,type:'edit'});
	            }
	        }],
	        rowEvents:[
	        {
	            class:"btn-info",
	            icon:"fa fa-file-text",
	            title:"历史结果",
	            click:function(row){
//	            	$state.go("app.filelistHistory",{fileId:row.fileId});    
	            	setAllHidden();
	            	$scope.isShowHistory = true;
	            	$scope.$broadcast("sendDataToHistory",{fileId:row.fileId})
	            }
	        },{
	            class:"btn-info",
	            icon:"fa fa-download",
	            title:"下载",
	            click:function(row){
//	            	debugger   
	            	var str = "确认下载"+row.fileName+"?";
	         		str=str.replace(/[^\x00-\xff]/g,"$&\x01").replace(/.{35}\x01?/g,"$&\n").replace(/\x01/g,"");
	         		dbUtils.confirm(str,function(){
	            		// 点击确认按钮
	         			fileDown(row);
	            	},function(){
	            		// 点击取消按钮
	            	},true);
	            	
	            }
	        },{
	        	class:"btn-danger",
	            icon:"fa fa-trash",
	            title:"删除",
	            isShowPopover:true,placement:"auto left",question:"确定删除吗",
	            clickOk:function(row,event){
	            	fileListService.deleteFile(row.fileId,row.fileName).then(function(response){
                		if(response.code == SUCCESS_CODE){
                            dbUtils.success("文件删除成功","提示");
                            $(event.target).parents(".popover").prev().click();
                            $scope.table.operations.reloadData()
                        }else {
                            dbUtils.error(response.message,"提示");
                        }
                  	})
	            },
	            clickCancel:function(row,event){
	            	$(event.target).parents(".popover").prev().click();
	            }
	        }],
	        settings:{
	        	filterId: "fileListFilter",pageSize:15,
	        	showCheckBox:true,
	            tbodyStyle:{
	            	"height":(Number($rootScope._screenProp.contentHeight)-45-38-30)+"px",
	            },
	        },
	        afterReload:function(){
	        	setTimeout(function(){
	        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight1+"px";
	        	},200)
	        },
	    }
  	
	$scope.table = table;
	   
	//下载文件
    function fileDown(row){
    	var url = "/file/manage/downloadFile";
        var form = $("<form></form>").attr("action", url).attr("method", "get");
        form.append($("<input></input>").attr("type", "hidden").attr("name", "fileId").attr("value", row.fileId));
        form.appendTo('body').submit().remove();
    }  
    
    //文件上传
    $scope.uploadHandle = function(file){
    	fileListService.uploadFile($scope,file);
	}

}]);

//文件分发
app.controller('FileEditController', 
		['$scope','$rootScope','$compile','dbUtils','$stateParams','EchoLog','$modal','$state','$interval','$http','FileListService',
		 function($scope,$rootScope,$compile,dbUtils,$stateParams,echoLog,$modal,$state,$interval,$http,fileListService) {
	//初始化设置，否则会报错
	$scope.dataSource = [];
	let timers = [];
	let interval_time = 2000;
	
	$scope.$on("sendDataToDistribute",function(event,data){
		$scope.filelist = [];
		$scope.file = {serverType:'Linux主机',filePath:'',isPathNotExist:'1',isDuplicate:'1'};
		$scope.filelist = data.files;
		$scope.showFile = true; 
		getfileType();
		//加载client表格
		$scope.clients = [];
		$scope.clientIdtoObj = {}; 
		/*transfer */
		$scope.dataSource = [];
		$scope.targetKeys = [];
		$("#file_transfer .transfer-list").css("width",Math.floor((($rootScope._screenProp.contentWidth - 20) - 20 - 30 -50)/2)+"px");
		/*transfer */
		initClients();
		
	    $scope.logIds = {};
	    $scope.progressResults = [];  
	})
	
	function initClients(){
		dbUtils.get("/client/listByFilter",{}).then(function(response){
			if(response.code == SUCCESS_CODE){
                $scope.clients = response.data;
                $scope.dataSource = $scope.clients.map( item =>  {return angular.extend(item,{key:item.id})});
                $scope.dataSource.forEach(function(item){
                	$scope.clientIdtoObj[item.id] = item;
                });
            }else{
            	dbUtils.error(response.message,"提示");
            }
		})
	}
	
	$scope.transferBoxOption = {
        orientation : 'horizontal',
        render : function(item){
            return "主机名：" + item.name + '，IP：' + item.hostAddress + '，用户名：' + item.username;
        },
        titles : {
            origin : '未选主机',target : '已选主机'
        },
        showSearch : true,searchPlaceholder : '请输入搜索内容',notFoundContent : '找不到东西'
        //classes : '',
    };
	
    function getfileType(){	      	
		fileListService.getScriptType().then(function(response){
	        if (response.code == SUCCESS_CODE) {
	        	$scope.fileType1 = response.data.scriptType;
	        	$scope.fileType = ['软件','安装包','默认'];
//        		$scope.file.fileType = stateParams.file.fileType ? stateParams.file.fileType : $scope.fileType[0];
//	        	setTimeout(function () { $("#fileType").val($scope.file.fileType);}, 100);
	        } else {
	        	$scope.fileType1 = []; 
	        }
	  	})
    }
	
    $scope.distributeHandle = function(){
    	if(angular.isUndefined($scope.targetKeys) || $scope.targetKeys.length == 0){
        	dbUtils.info("请至少选择一个主机");
            return;
        }
    	let rows = [];
        for(let key of $scope.targetKeys){
        	let row = $scope.clientIdtoObj[key];
        	rows.push(row);
        }
        //执行分发操作
        for(let file of $scope.filelist){
        	$scope.distribute(file,rows);
        }
    }
	
    $scope.distribute = function(file,clients){
    	if(!$scope.file.filePath){
    		dbUtils.warning("请填写目标路径","提示");
            return;
        }
    	var fileInfo = angular.extend({} , file);
    	fileInfo.fileSize = Number(file.fileSize);
    	fileInfo.isPathNotExist = $scope.file.isPathNotExist;
    	fileInfo.isDuplicate = $scope.file.isDuplicate;
    	fileInfo.serverType = $scope.file.serverType;
    	fileInfo.filePath = $scope.file.filePath;
    	fileInfo.fileType = "默认";
       
        fileListService.distributeFile(clients.map(client => {return client.id}),fileInfo).then(function(response){
            if (response.code == SUCCESS_CODE) {
            	$scope.logIds = response.data;
            	file.fileAbsoPath = fileInfo.filePath + "/" + file.fileName;
            	showExecuteResults(file,clients);
            } else {
            	dbUtils.error(response.message,"提示");
            	$scope.logIds = {};
            }
      	})	
    }  
    
    //显示执行结果表格
    function showExecuteResults(file,clients){
    	for(let client of clients){
    		let logId = $scope.logIds[client.id];
    		let result = {clientId:client.id,status:2,serviceName:file.fileName,clientIp:client.hostAddress,progressNum:"0%",clientUsername:client.username,createTime:new Date().format("yyyy-MM-dd hh:mm:ss"),logId:logId};
    		$scope.progressResults.push(result);
    		queryFileProgress(file,client,$scope.progressResults.length-1);
    	}
    }
    
    //查询文件分发进度
    function queryFileProgress(file,client,index){
    	let count = 0, max_count = 5, compare_value = 0;
    	let timer = setInterval(function(){
    		fileListService.getRemoteFileSize(file.fileAbsoPath,file.fileSize,client).then(function(response){
    			if (response.code == SUCCESS_CODE) {
    				
	            	$scope.progressResults[index].progressNum = response.data+"%";
	            	$scope.progressResults[index].progressStyle = {width:response.data+"%"};
	            	$scope.progressResults[index].timer = timer;
	            	//满足条件取消定时
	            	if(response.data == 100){
	            		$scope.progressResults[index].status = 1;
	            		clearTimers(timer,timers);
	            	}
	            	if(compare_value == Math.round(response.data)){
	            		count++;
	            		if(count == max_count){//查询5次仍然相同，代表出错了，终止定时
	            			count = 0;
	            			$scope.progressResults[index].status = 0;
	    	            	clearTimers(timer,timers);
	            		}
	            	}else{
	            		compare_value = Math.round(response.data);
	            		count = 0;
	            	}
	            	
	            } else {
	            	$scope.progressResults[index].status = 0;
	            	clearTimers(timer,timers);
	            }
    		},function(){
    			$scope.progressResults[index].status = 0;
    			clearTimers(timer,timers);
    		})
    	},interval_time)
    	timers.push(timer);
    }
    
    //清除interval定时,interval数组
    function clearTimers(intervalTimer,intervalTimerArray){
    	clearInterval(intervalTimer);
    	intervalTimerArray.splice(intervalTimerArray.indexOf(intervalTimer),1);
    }
    
    $scope.cancelDistribute = function(row){
    	//1.取消定时查询
    	clearTimers(row.timer,timers);
    	row.status = 3;
    	//2.向后台发送取消请求
    	fileListService.cancelTask(row.serviceName,$scope.clientIdtoObj[row.clientId]);
    }
    
    //展示分发结果
    $scope.show = function(row){
    	fileListService.showDistributeLog(row);
    }
	    
    $scope.returnMain = function (){
//    	$state.go("app.filelist");
        timers.forEach(function(item){
        	clearInterval(item);
        })
        timers = [];
    	$scope.$emit("returnMain");
    }

}]);


app.controller('FileDetailController', ['dbUtils','$scope','$modalInstance', '$state','source','$http', 
                                      function (dbUtils,$scope,$modalInstance, $state,source,$http) {
	  $scope.title = "显示执行结果详情";
	  $scope.showLogsDetail = source.data.content;
	
	  $scope.cancel = function () {
	      $modalInstance.close(false);
	  };
	  
	  $scope.submitApply = function(){
	      $modalInstance.close();
	  }
  
}]);

//历史
app.controller('FileHistoryController', 
		['$scope','$rootScope','$compile','dbUtils','$stateParams','EchoLog','$modal','$state','$interval','FileListService','$timeout',
		 function($scope,$rootScope,$compile,dbUtils,$stateParams,echoLog,$modal,$state,$interval,fileListService,$timeout) {
	
	$scope.$on('sendDataToHistory',function(event,data){
		$scope.fileId = Number(data.fileId);
		$scope.t_id = dbUtils.guid();
	    $scope.showText = "查询中";
	    fileListService.getFile($scope.fileId).then(function(response){
	        if (response.code == SUCCESS_CODE) {
	        	$scope.fileData = response.data; 
	        	$scope.queryOK = true;
	        	$scope.table = table;
	        	$scope.table.params = [{name:"fileName",label:"文件名",type:"text",initValue:$scope.fileData.fileName}];
	        } else {
	        	$scope.fileData = {};
	        	$scope.showText = "无数据";
	        }
	  	})
	})
	
  	var table = {
    	url:"/logs/getFileInfoHistoryLogs",
    	pageSql:true,
    	hideQuery:true,
        headers: [
            {name:"分发文件",field:"serviceName"},
            {name:"ip",field:"clientIp"},
            {name:"执行用户",field:"clientUsername"},
            {name:"执行状态",field:"status",formatter:function(value,row){
            	var show = {'0':'执行成功','1':'执行失败','2':'执行中','3':'执行结束'}[row.status];
	            return show ;
	        }},
            {name:"执行分发时间",field:"createTime"},
        ],
        rowEvents:[
        {
            class:"btn-info",
            icon:"fa fa-search-plus",
            title:"查看详情",
            click: function(row){
            	dbUtils.openModal('tpl/logtable/show_logs.html','FileDetailController',"lg","",{"data":row},function (id) {},true);
            }                        
            
        }],
        beforeReload:function(){ },
        settings:{
        	filterId: "fileHistoryFilter",
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-40-41-34-56-20-15)+"px",
            },
        },
        afterReload:function(){
        	//查询条件--45 按钮--38 2个padding--30
			$timeout(function () {
				$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight9+"px";
			})
        	// setTimeout(function(){
        	// 	$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight9+"px";
        	// },200)
        }
    }

    $scope.goBackFile = function (){
		$scope.queryOK = false;
		$scope.$emit("returnMain");
    }

}]);
