'use strict';
/* 监控管理菜单Controllers */
let MonitorManagerController = app.controller('MonitorManagerController',
		['$rootScope', '$scope', '$state', '$cookieStore','AccountService','$modal','dbUtils','$http','EchoLog','$filter', 'MonitorService',
        function ($rootScope, $scope, $state,$cookieStore, accountService,$modal,dbUtils,$http,echoLog,$filter,monitorService) {
	$scope.isShowMonitorGraph = false;
	$scope.monitorObject = {};
	$scope.t_id = dbUtils.guid();
	var table = {
        url:"/monitor/getAllInfo",
        showUpload: false,
        headers: [
			{name:"监控服务名",field:"monitorName",class:"text-center",style:{"width":"20%"},compile:true,formatter:function(value,row){
				let monitorName = "";
            	if(row.monitorType == 1){
					monitorName = row.clientName;
            	}else if(row.monitorType == 2 || row.monitorType == 3 || row.monitorType == 4){
					monitorName =  row.appName;
            	}
            	row.monitorName = monitorName;
				return monitorName;
            }},
            {name:"监控类型",field:"monitorTypeValue",style:{"width":"14%"}},
            {name:"IP",field:"ip",style:{"width":"15%"}},
            {name:"端口",field:"port",style:{"width":"8%"}},
            {name:"运行状态",field:"runStatus",style:{"width":"10%"},compile:true,formatter:function(value,row){
            	var classFlag = false; 
            	if(value=='on'){
            		classFlag = true;
            	}
                return "<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{true:\"text-success\",false:\"text-danger\"}["+classFlag+"]'></i>{{{'on':\"运行中\",'off':\"已停止\"}['"+value+"']}}</span>"
            }},
            {name:"监控端口",field:"monitorPort",style:{"width":"10%"},compile:true,formatter:function(value,row){
            	if(row.monitorStatus == 'no'){
            		return "--";
            	}else {
            		if(parseInt(value) == 0)
            			value = "--";
            		return value;
            	}
            }},
            {name:"监控状态",field:"monitorStatus",style:{"width":"10%"},compile:true,formatter:function(value,row){
            	var temp = 0;  
            	if(value == 'no'){
            		temp = 0;
            	}else if(value == 'on'){
            		temp = 1;
            	}else if(value == 'off'){
            		temp = 2;
            	}
                return "<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-light\",1:\"text-success\",2:\"text-danger\"}["+temp+"]'></i>{{{'no':\"未监控\",'on':\"已监控\",'off':\"停止监控\"}['"+value+"']}}</span>"
            }},
        ],
//        operationEvents: [{
//            class:"btn-success",
//            icon:"fa fa-play",//glyphicon glyphicon-plus
//            name:"启动监控",
//            click:function(){
//            	dbUtils.info("后续添加该功能","提示");
//            }
//        },{
//            class:"btn-danger",
//            icon:"fa fa-stop",
//            name:"停止监控",
//            click:function(){
//            	dbUtils.info("后续添加该功能","提示");
//            }
//        }],
        rowEvents:[{
        	class:"btn btn-default btn-info",
        	icon:"fa fa-line-chart",
        	title:"查看监控图表",
        	name:"监控",
        	click: function(row){
        		if(row.monitorStatus == "on"){
	    			$scope.isShowMonitorGraph = true;
	    			var ifm= document.getElementById("monitor");
	    			ifm.height=document.documentElement.clientHeight;
	        		showMonitorGraph(row);
        		}else
        			dbUtils.info("监控未开启或已停止，请检查后再试","提示");
        	}
        },{
        	class:"btn btn-info",
        	icon:"fa fa-pencil",
        	title:"编辑",
        	click: function(row){
        		dbUtils.openModal('tpl/monitor/monitor_modify_modal.html','MonitorModifyController',"lg","",{data:angular.copy(row)},function (data) {
        			if(data == false){
                		return;
                	}
                	monitorService.updateMonitorPort(JSON.stringify(data)).then(function(response){
                		if(response.code == SUCCESS_CODE){
                			dbUtils.confirmlg("请在应用发布菜单中重启应用服务或主机管理菜单中重启主机，监控服务才会生效",function () {
								$scope.table.operations.reloadData();
							},function () {})
                		}else{
                			dbUtils.error(response.message,"提示");
                		}
                	},function (error) {
						dbUtils.error(error.message);
					})
                });
        	}
        }],
        afterReload:function(){
        	//查询条件+按钮--45+42	2个padding--30
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight8+"px";
        	},100)
        },
        settings:{
            cols:3,
            pageSize:20,
            filterId: "monitorManagerFilter",
            showCheckBox:false,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-86-20)+"px",
            },
        }
    }
    $scope.table = table;
	
	var id =  "monitor";
	$scope.refresh = [{value:'off',show:'不刷新'},{value:'5s',show:'5秒'},{value:'10s',show:'10秒'},{value:'30s',show:'30秒'},
	                  {value:'1m',show:'1分钟'},{value:'5m',show:'5分钟'},{value:'15m',show:'15分钟'},{value:'30m',show:'半小时'},
	                  {value:'1h',show:'1小时'},{value:'2h',show:'2小时'},{value:'4h',show:'4小时'},{value:'1d',show:'1天'},];
	$scope.query = {refresh:'5s'};//startDay:'now-24h',endDay:'now',
	
	function showMonitorGraph(data){
		$scope.monitorObject = data;
		$scope.monitorObject.ipMport = $scope.monitorObject.ip +":"+$scope.monitorObject.monitorPort;
		$scope.query = {startDay:$scope.getTime(24*60)[0],endDay:$scope.getTime(24*60)[1],refresh:'5s'};
		//时间startDay endDay
		$scope.startTime = $scope.query.startDay;
		$scope.endTime = $scope.query.endDay;
		// setTimeout(function () {
			// setTimeout(function () {$("#query_refresh").val($scope.query.refresh);}, 20);
			// $scope.queryMoniter();
		// }, 20);
	}
	
	$scope.queryMoniter = function(){
		document.getElementById(id).src = "";
	  	//refresh
	  	var refresh = $scope.query.refresh;
	  	if(refresh != "undefined" && refresh != null){
	  		if(refresh == "off"){ refresh = ""; }else{refresh =  "&refresh="+refresh ;}
	  	}else{
	  		$scope.query.refresh = "5s";
	  		refresh = "&refresh=" + $scope.query.refresh;
	  	}
	  	//time
	  	var time = refresh+"&from="+$scope.startTime+"&to="+$scope.endTime + "&kiosk";
	  	//ip
	  	var ip = "";
	  	if($scope.monitorObject.monitorType == 1){
			ip = "&var-node=" + $scope.monitorObject.ipMport;
		}else if($scope.monitorObject.monitorType == 2 || $scope.monitorObject.monitorType == 3 || $scope.monitorObject.monitorType == 4){
			ip = "&var-instance=" + $scope.monitorObject.ipMport;
		}
	  	//查询url
	  	getUrl($scope.monitorObject.monitorType,ip,time);
//		document.getElementById(id).src="http://192.168.129.78:3000/d/nshAi8pmz/si-jmx-exporter-prometheus-new?orgId=1&from=now%2Fy&to=now&var-job=monitor-jmx&var-instance=192.168.129.78:17812&var-instance1=192.168.129.78:17812&var-job1=All";
	}
	
	$scope.$watch('{a:startTime,b:endTime}',function(newValue,oldValue){
		if(angular.isDefined(oldValue) && newValue != oldValue)
			$scope.queryMoniter();
	});
	
	function getUrl(monitorType,ip,time){
    	monitorService.getUrl(monitorType).then(function(response){
            if(response.code == SUCCESS_CODE){
            	var url = response.data;
            	if(url != "undefined" && url != null){
            		document.getElementById(id).src = url + ip + time;
            	}
            }else{
                dbUtils.error(response.message,"提示"); 
            }
        })
    }
	
	$scope.getTime = function(reduceTime){
		var dataArr = [];
		var now = new Date;
    	now.setMinutes(now.getMinutes() - reduceTime);
    	dataArr.push(now.format("yyyy-MM-dd hh:mm:ss"));
    	dataArr.push(new Date().format("yyyy-MM-dd hh:mm:ss"));
    	return dataArr;
	}
	
	Date.prototype.format = function(fmt) {
	    var o = {
	        "M+": this.getMonth() + 1, //月份 
	        "d+": this.getDate(), //日 
			"h+": this.getHours(), //小时 
			"m+": this.getMinutes(), //分 
			"s+": this.getSeconds(), //秒 
			"q+": Math.floor((this.getMonth() + 3) / 3), //季度 
			"S": this.getMilliseconds() //毫秒 
		};
		if(/(y+)/.test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 -RegExp.$1.length));
		}
		for(var k in o) {
			if(new RegExp("(" + k + ")").test(fmt)) {
			    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
			}
		}
		return fmt;
	}

	//返回主页
	$scope.returnMain = function(){
		$scope.isShowMonitorGraph = false;
		$scope.query = {};
		$scope.monitorObject = {};
		document.getElementById(id).src = "";
	}
	
	//全屏
	$scope.requestFullScreen = function(){
	    var de = document.getElementById("monitor");
	    if (de.requestFullscreen) {
	        de.requestFullscreen();
	    } else if (de.mozRequestFullScreen) {
	        de.mozRequestFullScreen();
	    } else if (de.webkitRequestFullScreen) {
	        de.webkitRequestFullScreen();
	    }
	}
	//退出全屏
	$scope.exitFullscreen = function(){
	    var de = document;
	    if (de.exitFullscreen) {
	        de.exitFullscreen();
	    } else if (de.mozCancelFullScreen) {
	        de.mozCancelFullScreen();
	    } else if (de.webkitCancelFullScreen) {
	        de.webkitCancelFullScreen();
	    }
	}
	
}]);

let MonitorModifyController = app.controller('MonitorModifyController', ['dbUtils','$scope','$modalInstance', '$state','source','MonitorService',
    function (dbUtils,$scope,$modalInstance, $state,source,monitorService) {

	$scope.data = source.data;
	$scope.buttonClickFlag = false;
	var actionArr = ['start','stop','modify'];
	var oldStatus = $scope.data.isMonitor;
	var oldMonitorPort = $scope.data.monitorPort;
	
	$scope.$watch('data', function(newValue, oldValue) {
		if (newValue != oldValue){
			if(oldValue.isMonitor == 0 && newValue.isMonitor == 1 && newValue.monitorPort == 0){
				newValue.monitorPort = "";
			}
			$scope.buttonClickFlag = true;
        }
    },true);
	
//	把controller中对端口检验的配置转移到ui-dbUtils.js中，方便管理和使用,提供ipPortList和oldData的初始化
	dbUtils.startPortCheck($scope,true,$scope.data);
	
	$scope.checkPortDuplicate = function(ip,data,propNameList){
		return dbUtils.checkPortDuplicate(ip,data,propNameList,$scope.ipPortList,$scope.oldData,true,"ip");
    }
	
	$scope.submitApply = function () {
		if($scope.form.$invalid){
			return;
		}
		if(parseInt($scope.data.monitorPort) > 65535){
			dbUtils.warning("端口值过大，请重新输入","提示");
			return;
		}
		if($scope.buttonClickFlag == false){
			dbUtils.info("信息没有改动，不能保存","提示");
			return;
		}else{
			if(parseInt($scope.data.isMonitor) == 1 && parseInt(oldStatus) == 0)
				$scope.data.action = actionArr[0];
			else if(parseInt($scope.data.isMonitor) == 0 && parseInt(oldStatus) == 1){
				$scope.data.action = actionArr[1];
			}
			else{
				if(parseInt($scope.data.isMonitor) == 0 && parseInt(oldStatus) == 0){
					$scope.data.monitorPort = 0;
					$modalInstance.close(false);
				}
				$scope.data.action = actionArr[2];
			}
			var idDuplicate = $scope.checkPortDuplicate($scope.data.ip,$scope.data,['monitorPort']);
	        if(idDuplicate == true)
	        	return;
			$scope.data.monitorPortOld = oldMonitorPort;
			$modalInstance.close($scope.data);
		}
	};
	
	$scope.cancel = function () {
	    $modalInstance.close(false);
	};
}]);

let MonitorDatabaseController = app.controller('MonitorDatabaseController',
		['$rootScope', '$scope', '$state', '$cookieStore','AccountService','$modal','dbUtils','$http','EchoLog','$filter', 'MonitorService',
        function ($rootScope, $scope, $state,$cookieStore, accountService,$modal,dbUtils,$http,echoLog,$filter,monitorService) {
	
	function setAllHidden(){
		$scope.isShowMain = false;
		$scope.isShowOracle = false;
		$scope.isShowMysql = false;
	}
	
	setAllHidden();
	$scope.isShowMain = true;
	
	$scope.$on('returnMain',function(event,data){
		setAllHidden();
		$scope.isShowMain = true;
	})
	
	$scope.t_id = dbUtils.guid();
	$scope.monitor = {"status":{}};
	var table = {
        url:"/database/list",
        showUpload: false,
        pageSize:10,
        headers: [
			{name:"名称",field:"name",style:{"width":"20%"}},
            {name:"类型",field:"type",style:{"width":"8%"}},
            {name:"IP",field:"ip",style:{"width":"12%"}},
            {name:"端口",field:"port",style:{"width":"8%"}},
            {name:"库名",field:"databaseName",style:{"width":"12%"},compile:true,formatter:function(value,row){
            	if(row.type == 'oracle'){
            		return value;
            	}else if(row.type == 'mysql'){
            		return "";
            	}
            }},
            {name:"用户名",field:"username",style:{"width":"15%"}},
            {name:"状态",field:"status",style:{"width":"10%"},compile:true,formatter:function(value,row){
            	row.status = 0;
            	$scope.monitor['status'][row.id] = 0;
            	var template = "<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{0:\"text-danger\",1:\"text-success\"}[monitor.status["+row.id+"]]'></i>{{{0:\"不可用\",1:\"可用\"}[monitor.status["+row.id+"]]}}</span>";
            	monitorService.checkConnected(JSON.stringify(row)).then(function(response){
            		if(response.code== SUCCESS_CODE){
						row.status = response.data;
            			$scope.monitor['status'][row.id] = response.data;
            		}
            	});
            	return template;
            }},
        ],
        operationEvents: [{
            class:"btn-info",
            icon:"glyphicon glyphicon-plus",//glyphicon glyphicon-plus
            name:"配置数据库",
            click:function(){
            	dbUtils.openModal('tpl/monitor/monitor_database_edit.html','MonitorDatabaseEditController',"lg","",{},function (data) {
            		if(data != false){
            			monitorService.saveOrUpdateMonitorDatabase(data).then(function(response){
            				if(response.code == SUCCESS_CODE){
            					dbUtils.info("保存成功","提示");
            					$scope.table.operations.reloadData();
            				}else{
            					dbUtils.error(response.message,"提示");
            				}
            			})
            		}
                });
            }
        }],
        rowEvents:[{
        	class:"btn btn-info",
        	icon:"fa fa-line-chart",
        	title:"查看监控图表",
        	name:"监控",
        	click: function(row){
    			if(row.type == "oracle"){
//    				$state.go("app.oracleMonitor",{"oracleMonitorData":row});
    				setAllHidden();
    				$scope.isShowOracle = true;
    				$scope.$broadcast("sendDataToOracle",{"oracleMonitorData":row});
    			}else if(row.type == "mysql"){
					setAllHidden();
					$scope.isShowMysql = true;
					$scope.$broadcast("sendDataToMysql",{"mysqlMonitorData":row});
    			}
        	}
        },{
        	class:"btn btn-info",
        	icon:"fa fa-pencil",
        	title:"编辑",
        	click: function(row){
        		dbUtils.openModal('tpl/monitor/monitor_database_edit.html','MonitorDatabaseEditController',"lg","",{'data':angular.copy(row)},function (data) {
            		if(data != false){
            			monitorService.saveOrUpdateMonitorDatabase(data).then(function(response){
            				if(response.code == SUCCESS_CODE){
            					dbUtils.info("保存成功","提示");
            					$scope.table.operations.reloadData();
            				}else{
            					dbUtils.error(response.message,"提示");
            				}
            			})
            		}
                });
        	}
        },{
        	class:"btn-danger",
            icon:"fa fa-trash",
            title:"删除",
            isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
            	monitorService.deleteMonitorDatabase(row.id).then(function(response){
    				if(response.code == SUCCESS_CODE){
    					dbUtils.info("删除成功","提示");
    					$(event.target).parents(".popover").prev().click();
    					$scope.table.operations.reloadData();
    				}else{
    					dbUtils.error(response.message,"提示");
    				}
    			})
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        afterReload:function(){
        	//查询条件+按钮--45+42	2个padding--30
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight1+"px";
        	},100)
        },
        settings:{
            cols:3,
            filterId: "monitorDatabaseFilter",
            showCheckBox:true,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-86-20)+"px",
            },
        }
    }
    $scope.table = table;
	
}]);

let MonitorDatabaseEditController = app.controller('MonitorDatabaseEditController', ['dbUtils','$scope','$modalInstance', '$state','source','MonitorService',
    function (dbUtils,$scope,$modalInstance, $state,source,monitorService) {

	$scope.data;
	$scope.isEdit;
	$scope.buttonClickFlag = false;
	if(source.data && source.data.id){
		$scope.isEdit = true;
		$scope.data = source.data;
	}else{
		$scope.isEdit = false;
		$scope.data = {type:'oracle'};
	}
	
	$scope.$watch('data', function(newValue, oldValue) {
		if (newValue != oldValue){
			$scope.buttonClickFlag = true;
        }
    },true);
	
//	把controller中对端口检验的配置转移到ui-dbUtils.js中，方便管理和使用,提供ipPortList和oldData的初始化
	dbUtils.startPortCheck($scope,$scope.isEdit,$scope.data);
	
	$scope.checkPortDuplicate = function(ip,data,propNameList){
		return dbUtils.checkPortDuplicate(ip,data,propNameList,$scope.ipPortList,$scope.oldData,$scope.isEdit,"ip");
    }

	$scope.testConnect = function(){
    	if($scope.form.$invalid){
            return;
        }
    	$scope.testFlag = true;
		$scope.connected = 2;//查询中
    	monitorService.checkConnected(JSON.stringify($scope.data)).then(function(response){
    		// $scope.testFlag = true;
    		if(response.code == SUCCESS_CODE){
    			$scope.connected = response.data;
    		}else{
    			dbUtils.error(response.message,"提示");
    		}
    	})
    }
	
	$scope.submitApply = function () {
		if($scope.form.$invalid){
			return;
		}
		if($scope.buttonClickFlag == false){
			dbUtils.info("信息没有改动，不能保存","提示");
			return;
		}else{
			if($scope.data.type == "oracle" && !$scope.data.databaseName){
				dbUtils.info("请填写库名","提示");
				return;
			}
			if($scope.data.type == "oracle" && !$scope.data.jdbcUrl){
				dbUtils.info("请输入JDBC URL信息","提示");
				return;
			}
			var idDuplicate = $scope.checkPortDuplicate($scope.data.ip,$scope.data,['port']);
	        if(idDuplicate == true)
	        	return;
			$modalInstance.close($scope.data);
		}
	};
	
	$scope.cancel = function () {
	    $modalInstance.close(false);
	};
}]);
