'use strict';
/* 排除文件Controllers */
app.controller('ExcludeFileController',
	['$scope','$rootScope','$modalInstance','$compile','$modal','dbUtils','source','DeployService','$timeout',
		function($scope,$rootScope,$modalInstance,$compile,$modal,dbUtils,source,deployService,$timeout) {

	$scope.row = source.row;
	$scope.adminserverClient = source.adminserverClient;//如果没找到adminserverClient，则为空
	$scope.appName = source.appName;
	$scope.tabs = [];
	let nameToId = {};//保存文件名与id的对应关系
	$scope.menus = [{name:"文件排除",id:2},{name:"数据汇总",id:1,className:"active"}];
	$scope.menu = $scope.menus[0];

/*	deployService.listZipFileInfo($scope.row.fileSavepath , $scope.row.fileName).then(function (response) {
		if(response.code == SUCCESS_CODE){
			$scope.deployingData = response.data;
		}else{
			dbUtils.error(response.message)
		}
	});*/
	$timeout(function () {
		// console.log($(".modal-larger .modal-dialog").height()+","+$(".modal-larger .modal-dialog").css("max-height")+","+$(".modal-larger"))
		//         底部  pad  大标题 pad  小标题  pad  按钮   pad  thead 余量  tab标题    tab内容
		let temp = 62.6 + 20 + 28.6 + 20 + 28.6 + 10 + 37.6 + 10 + 33.6 + 3 + 38.8 + 5 + 83.6 + 15;
		$scope.deployingListParams.bodyStyle.height = parseFloat($(".modal-larger .modal-dialog").height()) - temp + "px";
	})

	function initData(){
		$scope.deployingListParams.loading = true;
		$scope.deployingData = [];
		// deployService.diffDeployFileInfo($scope.row.fileSavepath+"/",$scope.row.fileName,$scope.adminserverClient,$scope.appName).then(function (response) {
		deployService.listZipFileNames($scope.row.fileSavepath+"/",$scope.row.fileName).then(function (response) {
			$scope.deployingListParams.loading = false;
			if(response.code == SUCCESS_CODE){
				$scope.deployingData = response.data;
			}else{
				dbUtils.error(response.message)
			}
		},function(result){$scope.deployingListParams.loading = false;});
	}

	//左侧表格--将要部署的版本文件列表
	let index = 0;
	$scope.deployingData = [];
	// $scope.deployingListParams = {loading:true,bodyStyle: {height:$rootScope._screenProp.contentHeight*0.7+"px"}};
	$scope.deployingListParams = {loading:true,bodyStyle: {}};//height:$rootScope._screenProp.contentHeight*0.7+"px"
	$scope.deployingLoading = true;
	$scope.deployingFilter = {};
	$scope.rowClickEvent = function(row,event){
        let value = row["filePath"];
        row.isSelected = !row.isSelected;
        event = event.currentTarget;
        if(row.isSelected)
            addClass(value,event);
        else
            removeClass(value,event);
    }
	$scope.deployingHeaders = [
		{name:"待部署文件",field:"filePath",style:{"width":"100%"}},

        /*{name:"MD5一致性",field:"isChange",style:{"width":"10%"}},
		{name:"待部署文件",field:"部署文件路径",style:{"width":"35%"}},
        {name:"MD5值",field:"部署文件md5",style:{"width":"10%"}},
		{name:"已部署文件",field:"部署文件路径",style:{"width":"35%"}},
		{name:"MD5值",field:"部署文件md5",style:{"width":"10%"}}*/
	]
	initData();

	$scope.$on("refresh",function (event,data) {
		initData();
	})

	//统计总数和md5变化的数量
	$scope.$on("countChange",function (event,data) {
		$scope.count = {total:data.total, change:data.change};
	})

	//选择菜单
	$scope.chooseMenu = function(event,menu){
		event = event.currentTarget;
		$(event).closest('ul').find("li").removeClass('active');
		$(event).addClass('active');
		$scope.menu = menu;
	}

	$scope.removeTab = function(tab,index){
		$scope.tabs.splice(index,1);
		$scope.$broadcast("setSelect",{index:tab.index,isSelected:false});//设置列表中的数据的isSelected属性
		removeClass(tab.title,"#"+tab.id, true);
	}

	function addClass(value,event){
		$(event).removeClass("info-list-unselect");
		$(event).addClass("info-list-select");
		$(event).append("<i class='fa fa-check fa-2x text-info' style='float: right;margin-right: 20px;'>");
		let idValue = $(event).attr("id");
		$scope.tabs.push({title:value, id:idValue, index:idValue.substring(6)});
		scrollToBottom(event);
	}

	//isDelete 代表数据已从$scope.tabs中删除
	function removeClass(value,event,isTabDelete){
		$(event).removeClass("info-list-select");
		$(event).addClass("info-list-unselect");
		$(event).find("i").remove();
		if(!isTabDelete){
			$scope.tabs.forEach(function(item, index){
				if(item.title == value)
					$scope.tabs.splice(index,1);
			})
			scrollToBottom(event);
		}
	}

	function scrollToBottom(event){
		if($("#tabs-container")[0])
			$("#tabs-container").animate({scrollTop: $("#tabs-container")[0].scrollHeight}, 200);// 速度 毫秒
	}

	$scope.cancel = function () {
		$modalInstance.close(false);
	};
}]);
