'use strict';

// 组织机构管理
app.controller('OrganizationListController', ['$scope','$rootScope','$modal','$state','dbUtils','$stateParams','EchoLog','$http','uiGridTreeViewConstants','OrganizationService','uiGridConstants',
                                       function($scope,$rootScope,$modal,$state,dbUtils,$stateParams,echoLog,$http,uiGridTreeViewConstants,organizationService,uiGridConstants) {
	$scope.orgTypes = {"00":"一级","01":"二级","02":"三级","03":"四级","04":"五级"};
	$scope.gridOptions = {
		enableSorting: false,
		enableFiltering: false,
		enableGridMenu: false,
		enableColumnMenus: false,//隐藏表头的 ^ 符号
	    showTreeExpandNoChildren: false,
	    paginationPageSizes: [5, 10, 15, 20, 50, 100],
	    paginationPageSize: 10,
	    columnDefs: [
	    	{ displayName: '机构名称', field:'orgName',width: '10%', enableHiding: false },
	    	{ displayName: '机构全称', field:'orgFullName',width: '15%', enableHiding: false,  },
	    	{ displayName: '机构编码', field:'orgNo',width: '10%', enableHiding: false,  },
	    	{ displayName: '机构类别', field:'orgType',width: '15%', enableHiding: false, cellTemplate : '<div style="padding: 5px;">{{grid.appScope.orgTypes[row.entity.orgType]}}</div>'},
	    	{ displayName: '本级排序号', field:'orgSort',width: '15%', enableHiding: false,sort: {
	            direction: uiGridConstants.ASC,
	            priority: 1
	        }},
	    	{ displayName: '更新时间', field:'updateDate',width: '15%', enableHiding: false},
	    	{
	    		displayName : "操作",field : 'action',headerCellClass: 'text-center',
                cellTemplate : function(grid, row, col, rowRenderIndex, colRenderIndex) {
                	let temp1 = "<div style='text-align: center; vertical-align: middle;' class='ng-scope'>" +
			                	"<button type='button' style='padding: 1px 8px 2px; margin-right:4px;'" +
			                            "class='btn btn-default btn-info' ng-click='grid.appScope.goToUpdate(row)' title='编辑'>编辑" +
			                    "</button>" +
			                    "<button type='button' style='padding: 1px 8px 2px; margin-right:4px;' class='btn btn-default btn-danger'" +
			                            "data-toggle='popover' popover-placement='auto left' uib-popover-template=\"'myPopoverTemplateOrg.html'\" popover-title='' popover-trigger='outsideClick' title='删除'>删除" +
			                    "</button>" + 
			                    "<script type='text/ng-template' id='myPopoverTemplateOrg.html'>" + 
								"			<div onblur='grid.appScope.clickCancel($event)'> " +
								"				<p><i class='fa fa-warning text-danger' style='padding-left:5px;padding-right:15px;'></i>确认删除吗</p>" + 
								"				<div class='text-center'>" + 
            					"					<button class='btn btn-default btn-sm' style='border-radius: 4px;padding: 3px 10px;' type='button' ng-click='grid.appScope.clickCancel($event)'>取&nbsp;&nbsp;消</button>" + 
            					"					<button class='btn btn-info btn-sm' style='border-radius: 4px;padding: 3px 10px;' type='button' ng-click='grid.appScope.clickOk(row,$event)'>确&nbsp;&nbsp;定</button>" + 
								"				</div>" + 
								"			</div>" + 
								"		</script></div>";
                	return temp1;
                }
            },
	    ],
	    onRegisterApi: function( gridApi ) {
	    	$scope.gridApi = gridApi;
	    	var tableHeight = $rootScope._screenProp.contentHeight - 5 - 30 - 10 - 5 - 20 - 1;
	    	$("#grid1").css("height",tableHeight + "px");
	    }
	};
	queryList();
	
	$scope.clickOk = function(row,event){
		$scope.goToDelete(row);
    }
    
    $scope.clickCancel = function(event){
    	$(event.target).parents(".popover").prev().click();
    }
	
	//删除一行
	$scope.goToDelete = function(row){
		if(!organizationService.permissionControl($rootScope))
    		return;
		
		var entity = row.entity; 
        organizationService.delete(entity.orgNo).then(function(response) {
        	if (response.code == SUCCESS_CODE) {
    			dbUtils.success("删除成功","提示");            
    			queryList();
    		} else {
    			dbUtils.error(response.message,"提示");
    		}
        })
    };
    
    //更新一行
    $scope.goToUpdate = function(row){
    	var entity = row.entity; 
    	saveOrUpdate({row:entity,"parentOrgs":$scope.parentOrgs,"idToLabel":$scope.idToLabel});
    };
    
    $scope.add = function(){
    	saveOrUpdate({row:null,"parentOrgs":$scope.parentOrgs,"idToLabel":$scope.idToLabel});
    }
    
    function saveOrUpdate(data){
    	if(!organizationService.permissionControl($rootScope))
    		return;
    	
    	dbUtils.openModal('tpl/system/organization_edit.html','OrganizationEditController',"lg","modal-large",data,function (result) {
    		if(result){
    			organizationService.saveOrUpdate(result).then(function(response) {
    	        	if (response.code == SUCCESS_CODE) {
    	    			dbUtils.success("保存成功","提示");            
    	    			queryList();
    	    		} else {
    	    			dbUtils.error(response.message,"提示");
    	    		}
    	        })
    		}
    	});
    }
    
    $scope.refreshData = function(){
    	queryList();
    }
    
    function queryList(){
    	$scope.loading = true;
    	organizationService.queryList().then(function(response) {
			$scope.loading = false;
    		var data = response.data;
    		$scope.idToLabel = {};
    		
    		$scope.parentOrgs = organizationService.filterArray(data,null);
    		var treeLevelData = organizationService.treeLevelConvertArray($scope.parentOrgs,[])
    		
    		$scope.gridOptions.data = treeLevelData.map(function(item){
//				if(item.children)
					item.$$treeLevel = parseInt(item.orgType);
    			$scope.idToLabel[item.orgNo] = item.orgName;
    			return item;
    		})
    	},function (error) {
			$scope.loading = false;
			dbUtils.error(error.message);
		});
    }
    
	$scope.expandAll = function(){
		$scope.gridApi.treeBase.expandAllRows();
	};
	  
	$scope.collapseAll = function(){
	    $scope.gridApi.treeBase.collapseAllRows();
	};
	  
	$scope.toggleRow = function( rowNum ){
	    $scope.gridApi.treeBase.toggleRowTreeState($scope.gridApi.grid.renderContainers.body.visibleRowCache[rowNum]);
	};

	$scope.toggleExpandNoChildren = function(){
	    $scope.gridOptions.showTreeExpandNoChildren = !$scope.gridOptions.showTreeExpandNoChildren;
	    $scope.gridApi.grid.refresh();
	};
		 
	
	function addOrEditMailServer(data){
		dbUtils.openModal('tpl/system/mailServer_edit.html','MailServerEditController',"lg","",data,function (result) {
    		if(result){
        	    $scope.table.operations.reloadData()
        	}
    	});
	}
	
}]);

//组织机构新增或修改
app.controller('OrganizationEditController', 
		['$scope','$rootScope','$modalInstance','$compile','$modal','dbUtils','source','OrganizationService',
		 function($scope,$rootScope,$modalInstance,$compile,$modal,dbUtils,source,organizationService) {	
			
	// 控制显示“新建”还是“编辑”
	$scope.parentOrgs = source.parentOrgs;
	$scope.idToLabel = source.idToLabel;
	$scope.parentOrg = {};
    if(!source.row){
        $scope.isEdit = false;
        $scope.data = {};
    }else{
        $scope.isEdit = true;
        $scope.data = source.row;
        $scope.data.orgSort = parseInt($scope.data.orgSort);
        if($scope.data.parentOrgNo)
        	$scope.parentOrg.name = $scope.idToLabel[$scope.data.parentOrgNo];
//        console.log($scope.data)
    }
    
    $scope.parentOrgNoClickHandle = function(){
    	dbUtils.openModal('tpl/system/parentOrgNo_edit.html','ParentOrgNoEditController',"sm","",{"parentOrgs":angular.copy($scope.parentOrgs),"selectId":$scope.data.parentOrgNo},function (result) {
    		if(result){
    			$scope.parentOrg = result;
    			$scope.data.parentOrgNo = result.id;
        	}
    	},true);
    }
    
    $scope.orgTypes = [{"key":"00","value":"一级"},{"key":"01","value":"二级"},{"key":"02","value":"三级"},{"key":"03","value":"四级"},{"key":"04","value":"五级"}];
    
    $scope.submitApply = function(){
    	if($scope.form.$invalid){
            return;
        }
    	$modalInstance.close($scope.data);
    } 
    
    $scope.cancel = function () {
        $modalInstance.close(false);
    };
}]);

//上级组织机构编码新增或修改
app.controller('ParentOrgNoEditController', 
		['$scope','$rootScope','$modalInstance','$compile','$modal','dbUtils','source','OrganizationService',
		 function($scope,$rootScope,$modalInstance,$compile,$modal,dbUtils,source,organizationService) {	
	$scope.parentOrgs = source.parentOrgs;
	$scope.selectId;
	let isExpand = false;
	// 控制显示“新建”还是“编辑”
    if(!source.selectId){
        $scope.isEdit = false;
    }else{
        $scope.isEdit = true;
        $scope.selectId = source.selectId;
    }
    
    var zTreeObj, curMenu = null;
    // zTree 的参数配置，深入使用请参考 API 文档（setting 配置详解）
    var setting = {
		view: {
			selectedMulti: false,
			showLine: false
		},
		callback: {
			onClick: zTreeOnClick
		}
    };
    // zTree 的数据属性，深入使用请参考 API 文档（zTreeNode 节点数据详解）
    var zNodes = $scope.parentOrgs;
    
    $scope.$watch('$viewContentLoaded', function() {
    	zTreeObj = $.fn.zTree.init($("#ztree1"), setting, zNodes);
    	fuzzySearch('ztree1','#key1',null,true);
    	if($scope.selectId){
    		curMenu = zTreeObj.getNodeByParam("id", $scope.selectId, null);
    		zTreeObj.selectNode(curMenu);
    	}
    })
    
    function zTreeOnClick(event, treeId, treeNode) {
//	    console.log(treeNode.tId + ", " + treeNode.name);
	    $scope.parentOrg = treeNode;
	};
	
	$scope.toggleAllTree = function(){
		isExpand = !isExpand;
		zTreeObj.expandAll(isExpand);
	}
    
    $scope.submitApply = function () {
    	if(!$scope.parentOrg){
    		dbUtils.warning("请选择一条数据","提示");
    		return;
    	}
        $modalInstance.close($scope.parentOrg);
    };
    
    $scope.cancel = function () {
        $modalInstance.close(false);
    };
}]);