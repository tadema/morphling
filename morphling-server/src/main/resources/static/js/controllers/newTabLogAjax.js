'use strict';

(function() {
	function getQueryString(name){
	     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	     var r = window.location.search.substr(1).match(reg);
	     if(r!=null)return  unescape(r[2]); return null;
	}
	var url = getQueryString("url");
	var data = getQueryString("data");
	var method = getQueryString("method");
	
	if(title)
		document.title = title;
	if(!method)
		method = "get";
	
	if(method == "get"){
		data = JSON.parse(data);
	}
	// 调用方法
	$.ajax({  
        type: method, 
        url: url, 
        data: data,  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (response, ifo) {  
        	if (response.code == SUCCESS_CODE) {
        		var e = document.getElementById("log_container");
        		e.innerHTML = response.data;
           } else {
               dbUtils.error(response.message,"提示");
           } 
        }, error: function () {  
            alert("error");  
        }
	})
})();
