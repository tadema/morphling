'use strict';
//redis监控管理
let MonitorRedisClusterManagementController = app.controller('MonitorRedisClusterManagementController',
		['$rootScope', '$scope', '$state', '$cookieStore','AccountService','$modal','dbUtils','$http','EchoLog','$filter', 'MonitorRedisService',
        function ($rootScope, $scope, $state,$cookieStore, accountService,$modal,dbUtils,$http,echoLog,$filter,monitorRedisService) {
	
	function setAllHidden(){
		$scope.isShowMain = false;
		$scope.isShowNode = false;
		$scope.isShowMonitor = false;
	}
	
	setAllHidden();
	$scope.isShowMain = true;
	
	$scope.$on('returnMain',function(event,data){
		setAllHidden();
		$scope.isShowMain = true;
	})
	
	$scope.t_id = dbUtils.guid();
	$scope.monitor = {"status":{}};
	var table = {
        url:"/redis/cluster/getClusterList",
        showUpload: false,
        pageSize:10,
        headers: [
			{name:"集群名称",field:"clusterName",style:{"width":"14%"}},
            {name:"模式",field:"redisMode",style:{"width":"7%"}},
            {name:"版本",field:"redisVersion",style:{"width":"5%"}},
            {name:"Master",field:"clusterSize",style:{"width":"6%"}},
            {name:"Node",field:"clusterKnownNodes",style:{"width":"6%"}},
            {name:"内存",field:"totalUsedMemory",style:{"width":"5%"},compile:true,formatter:function(value,row){
				return value + "MB";
			}},
            {name:"操作系统",field:"os",style:{"width":"12%"}},
			// {name:"哈希槽",field:"clusterSlotsAssigned",style:{"width":"6%"},compile:true,formatter:function(value,row){
			// 	if(!row.clusterSlotsAssigned){
			// 	}
			// 	return value;
			// }},
			{name:"DB_Size",field:"dbSize",style:{"width":"6%"},compile:true,formatter:function(value,row){
				return value;
			}},
            {name:"状态",field:"clusterState",style:{"width":"8%"},compile:true,formatter:function(value,row){
            	if(!value || value=="undefined")
					value = "BAD";
				$scope.monitor.status[row.clusterId] = value;
				return "<span class='badge sm-br cusor-pointer' ng-class=\"{'HEALTH':'bg-success','BAD':'bg-danger','WARN':'bg-warning'}[monitor.status["+row.clusterId+"]]\"> "+value+"</span>";
            }},
			{name:"创建人",field:"userName",style:{"width":"6%"}}
        ],
        operationEvents: [{
            class:"btn-info",icon:"glyphicon glyphicon-plus",name:"配置Redis集群",
            click:function(){
            	dbUtils.openModal('tpl/monitor/monitor_redis_cluster_edit.html','MonitorRedisClusterEditController',"lg","",{},function (data) {
            		if(data){
						$scope.table.operations.reloadData();
            		}
                });
            }
        }],
        rowEvents:[{
			class:"btn btn-info",icon:"fa fa-line-chart",title:"查询Key信息",name:"Key",
			click: function(row){
				if(row.clusterState != "HEALTH"){
					dbUtils.info("运行状态已停止或有错误，请检查后再试");
					return;
				}
				dbUtils.openModal('tpl/monitor/monitor_redis_cluster_query.html','MonitorRedisClusterQueryController',"lg","modal-large",{'data':angular.copy(row)},function (data) {
				});
			}
		},{
        	class:"btn btn-info",icon:"fa fa-line-chart",title:"查看监控图表",name:"监控",
        	click: function(row){
                setAllHidden();
                $scope.isShowMonitor = true;
                $scope.$broadcast("sendDataToMonitor",{"data":angular.copy(row)});
        	}
        },{
			class:"btn btn-info",icon:"fa fa-line-chart",title:"节点信息",name:"节点信息",
			click: function(row){
				setAllHidden();
				$scope.isShowNode = true;
				$scope.$broadcast("sendDataToRedisNode",{"data":angular.copy(row)});
			}
		},{
        	class:"btn btn-info",icon:"fa fa-pencil",title:"编辑",
        	click: function(row){
				dbUtils.openModal('tpl/monitor/monitor_redis_cluster_edit.html','MonitorRedisClusterEditController',"lg","",{'data':angular.copy(row)},function (data) {
					if(data){
						$scope.table.operations.reloadData();
					}
				});
        	}
        },{
        	class:"btn-danger",icon:"fa fa-trash",title:"删除",isShowPopover:true,placement:"auto left",question:"确定删除吗",
            clickOk:function(row,event){
				monitorRedisService.deleteCluster(row).then(function(response){
    				if(response.code == SUCCESS_CODE){
    					dbUtils.info("删除成功","提示");
    					$(event.target).parents(".popover").prev().click();
    					$scope.table.operations.reloadData();
    				}else{
    					dbUtils.error(response.message,"提示");
    				}
    			})
            },
            clickCancel:function(row,event){
            	$(event.target).parents(".popover").prev().click();
            }
        }],
        afterReload:function(){
        	//查询条件+按钮--45+42	2个padding--30
        	setTimeout(function(){
        		$scope.table.settings.tbodyStyle.height = $rootScope._screenProp.contentHeight-$rootScope._screenProp.tableUselessHeight1+"px";
        	},100)
        },
        settings:{
            cols:3,
            filterId: "monitorRedisClusterFilter",
            showCheckBox:true,
            tbodyStyle:{
            	"height":(Number($rootScope._screenProp.contentHeight)-41-43-86-20)+"px",
            },
        }
    }
    $scope.table = table;
	
}]);

let MonitorRedisClusterEditController = app.controller('MonitorRedisClusterEditController', ['dbUtils','$scope','$modalInstance', '$state','source','MonitorRedisService',
    function (dbUtils,$scope,$modalInstance, $state,source,monitorRedisService) {

	let nodeIndex = 0;
	$scope.isEdit;
	$scope.buttonClickFlag = false;
	if(source.data && source.data.clusterId){
		$scope.isEdit = true;
		$scope.data = source.data;
		$scope.nodes = [];
		let nodes = $scope.data.nodes.split(",");
		for(let i=0;i<nodes.length;i++){
			$scope.nodes.push({id:nodeIndex++, value:nodes[i]})
		}
	}else{
		$scope.isEdit = false;
		$scope.nodes = [{id:nodeIndex++, value:""}];
	}

	$scope.$watch('{a:data,b:nodes}', function(newValue, oldValue) {
		if (newValue != oldValue){
			$scope.buttonClickFlag = true;
        }
    },true);

    //增加节点
    $scope.addRedisNode = function(){
		$scope.nodes.push({id:nodeIndex++, value:""});
	}

    //删除节点
    $scope.removeRedisNode = function(node,index){
		$scope.nodes.splice(index,1);
	}

	function validateAllNode(){
		let arr = [];
		for(let i=0;i<$scope.nodes.length;i++){
			let node = $scope.nodes[i];

			if(!$scope.data.nodes){
				$scope.data.nodes = node.value;
			} else {
				$scope.data.nodes += ("," + node.value);
			}
			Object.assign(node,{"showConnectInfo":true, "connectInfo":2});
			let validateData = {"redisPassword":$scope.data.redisPassword, "redisNode":{"host":node.value.split(":")[0],"port":node.value.split(":")[1]}};

			arr.push(monitorRedisService.validateRedisNode(validateData));
		}
		dbUtils.all(arr).then(function(response){
			//执行保存方法
			let canSave = true;
			for(let i=0;i<response.length;i++){
				let responseData = response[i];

				$scope.nodes[i].connectInfo = responseData.data;
				if(!$scope.nodes[i].connectInfo){
					canSave = false;//不能保存
					$scope.nodes[i].connectInfo = false;
				}
			}
			if(canSave)
				saveRedisClusterHandle();
			// else
			// 	dbUtils.warning("有Redis节点连接失败，请检查后重试");
		},function(err){
			dbUtils.warning("有Redis节点连接失败，请检查后重试");
		})
	}

	$scope.submitApply = function () {
		if($scope.form.$invalid){
			return;
		}
		if($scope.buttonClickFlag == false){
			dbUtils.info("信息没有改动，不能保存","提示");
			return;
		}
		let queryCount = 0;
		$scope.canSave = false;
		$scope.hasConnectError = false;
		$scope.data.nodes = null;
		if(!$scope.data.redisPassword)
			$scope.data.redisPassword = "";
		validateAllNode();

		/*for(let i=0;i<$scope.nodes.length;i++){
			let node = $scope.nodes[i];

			if(!$scope.data.nodes){
				$scope.data.nodes = node.value;
			} else {
				$scope.data.nodes += ("," + node.value);
			}
			Object.assign(node,{"showConnectInfo":true, "connectInfo":2});
			let validateData = {"redisPassword":$scope.data.redisPassword, "redisNode":{"host":node.value.split(":")[0],"port":node.value.split(":")[1]}};

			monitorRedisService.validateRedisNode(validateData).then(function(response){
				if(response.code == SUCCESS_CODE){
					node.connectInfo = response.data;
					queryCount++;
					if(!node.connectInfo && !$scope.hasConnectError){
						$scope.hasConnectError = true;
					}
					if(i == queryCount - 1){
						$scope.canSave = true;
					}
				}else{
					dbUtils.error(response.message,"提示");
				}
			})
		}*/
	};

	//保存方法
	function saveRedisClusterHandle(){
		if(!$scope.isEdit){
			monitorRedisService.saveCluster($scope.data).then(function(response){
				if(response.code == SUCCESS_CODE){
					dbUtils.info("保存成功");
					$modalInstance.close($scope.data);
				}else{
					dbUtils.error(response.message,"提示");
				}
			})
		}else {
			monitorRedisService.updateCluster($scope.data).then(function(response){
				if(response.code == SUCCESS_CODE){
					dbUtils.info("保存成功");
					$modalInstance.close($scope.data);
				}else{
					dbUtils.error(response.message,"提示");
				}
			})
		}
	}
	
	$scope.cancel = function () {
	    $modalInstance.close(false);
	};
}]);

let MonitorRedisClusterQueryController = app.controller('MonitorRedisClusterQueryController', ['dbUtils','$scope','$rootScope','$modalInstance','$timeout', '$state','source','MonitorRedisService',
	function (dbUtils,$scope,$rootScope,$modalInstance,$timeout,$state,source,monitorRedisService) {

	$scope.redisCluster = source.data;
	$scope.queryParams = {key:""};
	$scope.data = source.data;
	let editor;
	getDBList();

	$scope.$watch('$viewContentLoaded', function() {
		editor = ace.edit("pre_code_editor_show_redis_value"); //初始化对象
		// editor.setTheme("ace/theme/eclipse");//设置风格和语言 tomorrow_night_eighties chrome github
		editor.setReadOnly(true);
		// editor.session.setMode("ace/mode/json");
		editor.$blockScrolling = Infinity;

		let modalHeight = parseInt($(".modal-dialog").css("height"))*$rootScope._screenProp.HeightValue/100;
		let minusHeight = 28.6 + 30 + 50 + 62.6 + 20;
		$("#fa_container").css("height",modalHeight - minusHeight + "px");
		$timeout(function () {
			$scope.bodyStyle = {"height":modalHeight - minusHeight - 28.6 + "px"}
		})
		$("#pre_code_editor_show_redis_value").css("height",modalHeight - minusHeight - 28.6 + "px");
	});

	function getDBList(){
		monitorRedisService.getDBList($scope.redisCluster.clusterId).then(function (response) {
			if(response.code == SUCCESS_CODE){
				$scope.databases = response.data;
				if($scope.databases.length > 0){
					$scope.db = $scope.databases[0];
					$scope.scanKeys();
				}
			}else{
				dbUtils.error(response.message);
			}
		})
	}

	$scope.scanKeys = function(){
		let database = $scope.db.database.substring(2);
		let queryParams = {clusterId:$scope.redisCluster.clusterId, count:$scope.db.keys, database:database};
		monitorRedisService.scan(queryParams).then(function (response) {
			if(response.code == SUCCESS_CODE){
				$scope.keys = response.data;
			}else{
				dbUtils.error(response.message);
			}
		})
	}

	$scope.queryValueByKey = function(key){
		if(!key){
			dbUtils.warning("请输入Key");
			return;
		}
		let database = $scope.db.database.substring(2);
		let queryParams = {clusterId:$scope.redisCluster.clusterId, count:$scope.db.keys, database:database, key:key};
		monitorRedisService.query(queryParams).then(function (response) {
			if(response.code == SUCCESS_CODE){
				editor.setValue(JSON.stringify(response.data, null, '\t'));
			}else{
				dbUtils.error(response.message);
			}
		})
	}

	$scope.resetQuery = function(){
		$scope.queryParams.key = "";
		editor.setValue("");
	}

	$scope.cancel = function () {
		$scope.queryParams = {};
		editor = null;
		$modalInstance.close(false);
	};
}]);

let MonitorRedisNodeManagementController = app.controller('MonitorRedisNodeManagementController', ['$scope','$rootScope','$compile','dbUtils','MonitorRedisService','uiGridTreeViewConstants','uiGridConstants',
	function($scope,$rootScope,$compile,dbUtils,monitorRedisService,uiGridTreeViewConstants,uiGridConstants) {

	$scope.$on('sendDataToRedisNode',function(event,data){
		$scope.redisCluster = data.data;
		isTableExpand = false;
		if($scope.redisCluster.redisMode != 'sentinel')
			queryList();
		else {
			initTable();
		}
	})

	function queryList(){
		$scope.loading = true;
		monitorRedisService.getRedisNodeList($scope.redisCluster).then(function(response) {
			$scope.loading = false;
			if(response.code == SUCCESS_CODE){
				var data = response.data;

				$scope.parentNodes = monitorRedisService.filterArray(data,"-");
				var treeLevelData = monitorRedisService.treeLevelConvertArray($scope.parentNodes,[]);

				$scope.gridOptions.data = treeLevelData.map(function(item){
					item.$$treeLevel = item.children ? 0 : 1;
					return item;
				})
			}else {
				dbUtils.error(response.message)
			}
		},function () {	$scope.loading = false;});
		/*dbUtils.get("configKey.json").then(function(response) {
			if(response.code == SUCCESS_CODE){
				$scope.configKeys = response.data;
			}else {
				dbUtils.error(response.message)
			}
		});*/
	}

	let isTableExpand;

	$scope.gridOptions = {
		enableSorting: false,
		enableFiltering: false,
		enableGridMenu: false,
		enableColumnMenus: false,//隐藏表头的 ^ 符号
		showTreeExpandNoChildren: false,
		paginationPageSizes: [5, 10, 15, 20, 50, 100],
		paginationPageSize: 15,
		columnDefs: [
			{ displayName: 'IP', field:'host',width: '20%', enableHiding: false },
			{ displayName: '端口', field:'port',width: '10%', enableHiding: false,  },
			{ displayName: '槽范围', field:'slotRange',width: '15%', enableHiding: false,  },
			{ displayName: 'flags', field:'flags',width: '10%', enableHiding: false},
			{ displayName: '连接状态', field:'linkState',width: '10%', enableHiding: false, cellTemplate :
				"<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{\"connected\":\"text-success\",\"unconnected\":\"text-danger\"}[row.entity.linkState]'></i>{{row.entity.linkState}}</span>"
			},
			{ displayName: '运行状态', field:'runStatus',width: '10%', enableHiding: false, cellTemplate :
				"<span><i style='margin-right:5px;' class='fa fa-circle' ng-class='{1:\"text-success\",0:\"text-danger\"}[row.entity.runStatus]'></i>{{{1:'运行中',0:'已停止'}[row.entity.runStatus]}}</span>"
			},
			{
				displayName : "配置信息",field : 'action',headerCellClass: 'text-center',
				cellTemplate : function(grid, row, col, rowRenderIndex, colRenderIndex) {
					let temp = "<div style='text-align: center; vertical-align: middle;' class='ng-scope'>" +
						"<button type='button' style='padding: 1px 8px 2px; margin-right:4px;'" +
						"class='btn btn-default btn-info' ng-click='grid.appScope.getInfo(row)' title='Info'>Info" +
						"</button>" +
						"<button type='button' style='padding: 1px 8px 2px; margin-right:4px;'" +
                        "class='btn btn-default btn-info' ng-click='grid.appScope.getConfig(row)' title='Config'>Config" +
                        "</button></div>";
					return temp;
				}
			},
		],
		onRegisterApi: function( gridApi ) {
			$scope.gridApi = gridApi;
			var tableHeight = $rootScope._screenProp.contentHeight - 5 - 30 - 10 - 5 - 20 - 1 - 112.6;
			$("#uiGridRedisNode").css("height",tableHeight + "px");
		}
	};

	function initTable(){
		//master表格
		$scope.masterList = {
			urlParams: {url:"/redis/cluster/getSentinelMasterList/"+$scope.redisCluster.clusterId, urlBody:{}, method:"get"},
			tableParams: {loading:true,labelTitle:"Master列表",bodyStyle:{"max-height": "300px"}},
			headers: [
				{name:"名称",field:"name",style:{width:"10%"}},
				{name:"Flags",field:"flags",style:{width:"10%"}},
				{name:"Master Node",style:{width:"15%"},compile:true,formatter:function (value,row) {
					return row.host + ":" + row.port;
				}},
				{name:"Last Master Node",field:"lastMasterNode",style:{width:"15%"}},
				{name:"Slaves数量",field:"numSlaves",style:{width:"10%"}},
				{name:"Sentinels",field:"sentinels",style:{width:"10%"}},
				{name:"状态",field:"status",style:{width:"10%"},compile:true,formatter:function (value,row) {
					return "<span><i class='fa fa-circle' ng-class='{\"ok\":\"text-success\",false:\"text-danger\"}[\""+value+"\"]'></i>&nbsp;&nbsp;"+value+"</span>";
				}},
				{name:"监控",field:"monitor",style:{width:"10%"},compile:true,formatter:function (value,row) {
					return "<i class='fa fa-circle' ng-class='{true:\"text-success\",false:\"text-danger\"}["+value+"]' ></i>";
				}},
			],
			rowEvents:[{
				class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"Info",name: "Info",
				click: function(row){
					monitorRedisService.getSentinelMasterInfo(row).then(function(response){
						if(response.code == SUCCESS_CODE){
							showInfoHandle(row,response.data,"Info");
						}else{
							dbUtils.error(response.message);
						}
					});
				}
			}],
			methods: {}
		}

		//Node列表
		$scope.nodeList = {
			urlParams: {url:"/redis/cluster/getAllNodeListWithStatus/"+$scope.redisCluster.clusterId, urlBody:{}, method:"get"},
			tableParams: {loading:true,labelTitle:"Node列表",bodyStyle:{"max-height": "300px"}},
			headers: [
				{name:"IP",field:"host",style:{width:"15%"}},
				{name:"端口",field:"port",style:{width:"15%"}},
				{name:"Flags",field:"flags",style:{width:"15%"}},
				{name:"Link State",field:"linkState",style:{width:"15%"},compile:true,formatter:function (value,row) {
					return "<span class='label cusor-pointer' ng-class='{\"connected\":\"bg-success\",\"unconnected\":\"bg-danger\"}[\""+value+"\"]'>"+value+"</span>";
				}},
				{name:"In Cluster",field:"inCluster",style:{width:"15%"},compile:true,formatter:function (value,row) {
					return "<span><i class='fa fa-circle' ng-class='{1:\"text-success\",0:\"text-danger\"}[\""+value+"\"]'></i></span>";
				}},
				{name:"状态",field:"runStatus",style:{width:"15%"},compile:true,formatter:function (value,row) {
					let val = value == 1 ? "UP" : "DOWN";
					return "<span><i class='fa fa-circle' ng-class='{1:\"text-success\",0:\"text-danger\"}["+value+"]'></i>&nbsp;&nbsp;"+val+"</span>";
				}},
			],
			rowEvents:[{
				class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"Info",name: "Info",
				click: function(row){
					monitorRedisService.getNodeInfo(row).then(function(response){
						if(response.code == SUCCESS_CODE){
							showInfoHandle(row,response.data,"Info");
						}else{
							dbUtils.error(response.message);
						}
					});
				}
			}],
			methods: {}
		}
	}

	$scope.getInfo = function(row){
		let entity = row.entity;
		monitorRedisService.getNodeInfo(entity).then(function (response) {
			if(response.code == SUCCESS_CODE){
				showInfoHandle(entity,response.data,"Info");
			}else{
				dbUtils.error(response.message);
			}
		})
	}

	$scope.getConfig = function(row,event){
		let entity = row.entity;
		monitorRedisService.getConfig(entity).then(function (response) {
			if(response.code == SUCCESS_CODE){
				showInfoHandle(entity,response.data,"Config");
			}else{
				dbUtils.error(response.message);
			}
		})
	}

	function showInfoHandle(entity,data,labelValue){
		dbUtils.openModal("tpl/monitor/monitor_redis_infoshow_modal.html",function (dbUtils,$scope,$modalInstance, $state, source) {
			$scope.data = source.data;
			$scope.settings = source.settings;

			$scope.cancel = function () {
				$modalInstance.close(false);
			};
		},"lg","",{data:data, settings:{label:labelValue+" - "+entity.host+":"+entity.port}},function (result) {});
	}

	//更新配置
	$scope.editConfigKey = function(){
		dbUtils.openModal('tpl/monitor/monitor_redis_config_key_edit.html',function (dbUtils,$scope,$modalInstance, $state, source) {
			let monitorRedisService = source.monitorRedisService;
			$scope.configKeys = source.data;

			$scope.configKeyChangeHandle = function(){
				$scope.configKeysValue = [];
				dbUtils.get("configKeyValue.json").then(function(response) {
					if(response.code == SUCCESS_CODE){
						$scope.configKeysValue = response.data[$scope.configKey];
					}else {
						dbUtils.error(response.message)
					}
				});
			}

			$scope.submitApply = function () {
				if($scope.form.$invalid){
					return;
				}
			};

			$scope.cancel = function () {
				$modalInstance.close(false);
			};
		},"lg","",{data:$scope.configKeys, monitorRedisService:monitorRedisService},function (result) {
			if(result){
			}
		});
	}

	//更新一行
	$scope.goToUpdate = function(row){
		var entity = row.entity;
	};

	$scope.add = function(){
	}

	function saveOrUpdate(data){
		if(!monitorRedisService.permissionControl($rootScope))
			return;

		dbUtils.openModal('tpl/system/organization_edit.html','OrganizationEditController',"lg","modal-large",data,function (result) {
			if(result){
				monitorRedisService.saveOrUpdate(result).then(function(response) {
					if (response.code == SUCCESS_CODE) {
						dbUtils.success("保存成功","提示");
						queryList();
					} else {
						dbUtils.error(response.message,"提示");
					}
				})
			}
		});
	}

	$scope.refreshData = function(){
		queryList();
	}

	$scope.toggle = function(){
		isTableExpand = !isTableExpand;
		if(isTableExpand)
			$scope.gridApi.treeBase.expandAllRows();
		else
			$scope.gridApi.treeBase.collapseAllRows();
	};

	/*$scope.toggleRow = function( rowNum ){
		$scope.gridApi.treeBase.toggleRowTreeState($scope.gridApi.grid.renderContainers.body.visibleRowCache[rowNum]);
	};

	$scope.toggleExpandNoChildren = function(){
		$scope.gridOptions.showTreeExpandNoChildren = !$scope.gridOptions.showTreeExpandNoChildren;
		$scope.gridApi.grid.refresh();
	};*/

	function addOrEditMailServer(data){
		dbUtils.openModal('tpl/system/mailServer_edit.html','MailServerEditController',"lg","",data,function (result) {
			if(result){
				$scope.table.operations.reloadData()
			}
		});
	}

	//返回主页
	$scope.returnMain = function(){
		$scope.redisCluster.redisMode = null;
		$scope.$emit("returnMain",{})
	}

}]);

let MonitorRedisMonitorController = app.controller('MonitorRedisMonitorController', ['$scope','$rootScope','$compile','dbUtils','$timeout','MonitorRedisService',
    function($scope,$rootScope,$compile,dbUtils,$timeout,monitorRedisService) {

    $scope.$on('sendDataToMonitor',function(event,data){
        $scope.redisCluster = data.data;
		$scope.queryParams.monitorType = 1;
		$scope.queryParams.timeType = 0;
		let startTime = new Date().setHours(new Date().getHours() -1);
		$scope.queryParams.startTime = new Date(startTime).format("yyyy-MM-dd hh:mm:ss");
		$scope.queryParams.endTime = new Date().format("yyyy-MM-dd hh:mm:ss");
		$scope.isShow = true;
		$scope.monitorData.data = {};
		getNodeList();
    })

	//初始化数据
	$scope.monitorData = {data:{}};
	$scope.lineStyleLeft = {"padding":"0px 5px 0px 0px"};
	$scope.lineStyleRight = {"padding":"0px 0px 0px 10px"};
	$scope.monitorTypes = [{id:1,name:"All"},{id:2,name:"All Master"},{id:3,name:"All Node"}];
	$scope.timeTypes = [{id:0,name:"Minute"},{id:1,name:"Hour"}];
    $scope.queryParams = {monitorType:null, timeType:null, startTime:null, endTime:null};//条件查询
	$scope.infoItems = [
        {value:"connected_clients",name:"connectedClients",label:"连接客户端数",props:{yAxis:{name:"个"}}},
        {value:"blocked_clients",name:"blockedClients",label:"阻塞连接数",props:{yAxis:{name:"个"}}},
		{value:"used_memory",name:"usedMemory",label:"已使用内存",props:{yAxis:{name:"MB"}}},
        {value:"used_memory_rss",name:"usedMemoryRss",label:"物理内存大小",props:{yAxis:{name:"MB"}}},
		{value:"used_memory_overhead",name:"usedMemoryOverhead",label:"内存开销",props:{yAxis:{name:"MB"}}},
        {value:"used_memory_dataset",name:"usedMemoryDataset",label:"数据占用内存大小",props:{yAxis:{name:"MB"}}},
		{value:"mem_fragmentation_ratio",name:"memFragmentationRatio",label:"内存碎片率",props:{yAxis:{name:"%"}}},
        {value:"connections_received",name:"connectionsReceived",label:"新创建连接数",props:{yAxis:{name:"个"}}},
		{value:"rejected_connections",name:"rejectedConnections",label:"拒绝连接数",props:{yAxis:{name:"个"}}},
		{value:"commands_processed",name:"commandsProcessed",label:"处理命令数",props:{yAxis:{name:"个"}}},
		{value:"instantaneous_ops_per_sec",name:"instantaneousOpsPerSec",label:"每秒执行命令数",props:{yAxis:{name:"个"}}},
        {value:"sync_full",name:"syncFull",label:"主从完全同步成功次数",props:{yAxis:{name:"个"}}},
		{value:"sync_partial_ok",name:"syncPartialOk",label:"主从部分同步成功次数",props:{yAxis:{name:"个"}}},
        {value:"sync_partial_err",name:"syncPartialErr",label:"主从部分同步失败次数",props:{yAxis:{name:"个"}}},
		{value:"keyspace_hits_ratio",name:"keyspaceHitsRatio",label:"求键的命中率",props:{yAxis:{name:"%"}}},
		{value:"cpu_sys",name:"cpuSys",label:"",props:{yAxis:{name:"秒"}}},
        {value:"cpu_user",name:"cpuUser",label:"",props:{yAxis:{name:"秒"}}},
        {value:"redis_keys",name:"redisKeys",label:"key总数",props:{yAxis:{name:"个"}}},
        {value:"expires",name:"expires",label:"过期key数",props:{yAxis:{name:"个"}}}
    ];

	function getNodeList() {
		//进入监控页面，先查询node数据，便于生成“系列名称”的数据
		monitorRedisService.getRedisNodeList($scope.redisCluster).then(function (response) {
			if(response.code == SUCCESS_CODE){
				$scope.nodeList = response.data;
				initCharts();
				initData();
			}else{
				dbUtils.error(response.message)
			}
		})
	}

	function initCharts() {
		for(let i=0;i<$scope.infoItems.length;i++){
			let infoItem = $scope.infoItems[i];
			let labelTitle = infoItem.value + "(" + infoItem.label + ")";

			$scope.monitorData.data[infoItem.value] = {
				classGlobal: {labelClass:"nm-font"},
				labelTitle: labelTitle,
				bodyStyle:JSON.stringify({"height": "400px"}),
				props: {legend: {data:[]},yAxis: {name:infoItem.props.yAxis.name},xAxis: {data:[]},series: []},
				methods: {},
			}
		}
	}

	//处理查询参数
	function initQueryParam() {
		let nodeList = [],startTime = "",endTime = "";

		//处理时间
		if($scope.queryParams.startTime && $scope.queryParams.endTime){
		    if($scope.queryParams.startTime > $scope.queryParams.endTime){//如果开始大于终止时间，交换时间
                startTime = $scope.queryParams.endTime;
                endTime = $scope.queryParams.startTime;
            }else{
                startTime = $scope.queryParams.startTime;
                endTime = $scope.queryParams.endTime;
            }
			let minusValue = (new Date(endTime) - new Date(startTime)) / (1000 * 60 * 60);//单位：小时
			if($scope.queryParams.timeType == 0){
		    	if(minusValue > 12){
		    		return "查询时间范围不能大于12小时";//返回字符串，代表时间校验没有通过
				}
			}
        }

		$scope.legendNames = [],$scope.selectLegendNames = {data:[]};
		//处理系列名称的数据
		for(let i=0;i<$scope.nodeList.length;i++){
			let item = $scope.nodeList[i];
			let legendName = item.host + ":" + item.port + " " + item.nodeRole;

			if($scope.queryParams.monitorType == 1) {
				$scope.legendNames.push(Object.assign({name:legendName, isSelect:true},item));
				$scope.selectLegendNames.data.push(legendName);
			}else if(($scope.queryParams.monitorType == 2 && item.nodeRole == "master") || ($scope.queryParams.monitorType == 3 && item.nodeRole == "slave")){
				nodeList.push(item.host+":"+item.port);
				$scope.legendNames.push(Object.assign({name:legendName, isSelect:true},item));
				$scope.selectLegendNames.data.push(legendName);
			}
		}
		return {clusterId:$scope.redisCluster.clusterId, timeType:$scope.queryParams.timeType, nodeList: nodeList, startTime:startTime, endTime:endTime};
	}

	function initData(){
		let queryParams = initQueryParam();
		//返回字符串，代表时间校验没有通过
		if(typeof(queryParams) == "string"){
			dbUtils.error(queryParams);
			return;
		}

		monitorRedisService.getInfoItemMonitorData(queryParams).then(function (response) {
			if(response.code == SUCCESS_CODE){
				for(let i=0;i<$scope.infoItems.length;i++){
					let infoItem = $scope.infoItems[i];
					getInfoItemMonitorDataHandle(infoItem,response.data[infoItem.value]);
				}
			}else{
				dbUtils.error(response.message);
			}
		})
	}

	function getInfoItemMonitorDataHandle(infoItem,responseData){
		$scope.monitorData.data[infoItem.value].props.legend.data = [];
		$scope.monitorData.data[infoItem.value].props.xAxis.data = [];
		$scope.monitorData.data[infoItem.value].props.series = [];
		if(!responseData){
			if($scope.monitorData.data[infoItem.value].methods.clear)
				$scope.monitorData.data[infoItem.value].methods.clear();
			return;
		}

		$scope.monitorData.data[infoItem.value].initData = (function () {
			let data = responseData,hasxAxisData = false;
			for(let i=0;i<data.length;i++){
				let itemData = data[i];
				let legendName = itemData[0].node + " " + itemData[0].role;
				$scope.monitorData.data[infoItem.value].props.series.push({name:legendName, data:[]});
				$scope.monitorData.data[infoItem.value].props.legend.data.push(legendName);

				for(let j=0;j<itemData.length;j++){
					$scope.monitorData.data[infoItem.value].props.series[i].data.push(itemData[j][infoItem.name]);
					if(!hasxAxisData){
						$scope.monitorData.data[infoItem.value].props.xAxis.data.push(itemData[j].updateTime);
						if(j == itemData.length - 1)
							hasxAxisData = true;
					}
				}
			}
		})()
		if($scope.monitorData.data[infoItem.value].methods.reloadData){
			$scope.monitorData.data[infoItem.value].methods.reloadData();
		}else{
			//有的时候line01.js还没有加载，而程序已经执行到这里导致methods没有初始化，这时要触发$watch initdata监听
			$scope.monitorData.data[infoItem.value].initData = responseData;
		}
	}

	$("body").on("select2:unselect",'#select2MonitorRedis',function (e) {
		if(!e || !e.params || !e.params.data || !e.params.data.text){
			console.log("select2:unselect #select2MonitorRedis 不能获取值");
			return;
		}
		changeLegendSelectHandle(e.params.data.text.trim(),false);
	});

	$("body").on("select2:select",'#select2MonitorRedis',function (e) {
		if(!e || !e.params || !e.params.data || !e.params.data.text){
			console.log("select2:select #select2MonitorRedis 不能获取值");
			return;
		}
		changeLegendSelectHandle(e.params.data.text.trim(),true);
	});

	//legend选择取消和选中的处理
	function changeLegendSelectHandle(legendName,selectValue){
		let legendSelectData = {};
		for(let i=0;i<$scope.legendNames.length;i++){
			let item = $scope.legendNames[i];
			if(item.name == legendName)
				item.isSelect = selectValue;

			legendSelectData[item.name] = item.isSelect;
		}
		for(let i=0;i<$scope.infoItems.length;i++){
			$scope.monitorData.data[$scope.infoItems[i].value].methods.setLegendSelect(legendSelectData);
		}
	}

	//查询按钮处理
    $scope.queryHandle = function () {
		initData();
	}

    //返回主页
    $scope.returnMain = function(){
		$scope.isShow = false;
		$scope.queryParams = {};
        $scope.$emit("returnMain",{})
    }
}]);