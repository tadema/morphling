'use strict';

angular.module("app").service('ApplicationService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    return {
        getEnvs:function(){
            return dbUtils.get("/env");
        },        
        saveOrUpdate:function(app,envs){
//            app.envs = envs;
            return dbUtils.put("/app?envs="+envs,app);
        },
        getInstances:function(appId,checkRegist){
            return dbUtils.get("/app/"+appId+"/instances",{"checkRegist":typeof checkRegist === 'undefined' ? false : checkRegist});
        },
        addInstance:function(appId,clientId){
            return dbUtils.post("/app/"+appId+"/instances",{clientId:clientId})
        },
        deleteInstance:function(appId,insId){
            return dbUtils.delete("/app/"+appId+"/instances/"+insId);
        },
        userPreview:function(){
            return dbUtils.get("/app/preview");
        },
        appPreview:function(appId){
            return dbUtils.get("/app/preview/"+appId);
        },
        getWeblogic:function(appId,checkRegist){
            return dbUtils.get("/app/"+appId+"/busy/domains",{"checkRegist":typeof checkRegist === 'undefined' ? false : checkRegist});
        },
        addWeblogic:function(appId,domainId){
            return dbUtils.post("/app/"+appId+"/domain",{appid:appId,domainId:domainId})
        },
        deleteWeblogic:function(appId,domainId){
            return dbUtils.delete("/app/"+appId+"/domain/"+domainId);
        },
        getDeveloper: function(){
            return dbUtils.get("/app/getDeveloperList");
        },
        saveDeveloper: function(developer, formData){
        	return dbUtils.postPicture('/app/uploadImgToApp?developer=' + developer,formData);
        },
        
        //批量新建
        getCheckDate:function(){
            return dbUtils.get("/app/getCheckDate",{})
        },
        saveData:function(appInfoVOs){
            return dbUtils.postBody("/app/batchSaveAppInstance",JSON.stringify(appInfoVOs))
        },
        importAppDeployCustom:function(appInfoVOs){
            return dbUtils.postBody("/app/importAppDeployCustom",JSON.stringify(appInfoVOs))
        },
    }
}]);

angular.module("app").service('InstanceService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    return {        
        saveOrUpdate:function(instance){
            return dbUtils.put("/client/addClientMiddleware?clientId="+instance.id+"&middlewareId="+instance.middlewareId,null);
        },
    }
}]);

angular.module("app").service('ManagerWeblogicService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    return {
        getWeblogic:function(appId,checkRegist){
            return dbUtils.get("/app/"+appId+"/busy/domains",{"checkRegist":typeof checkRegist === 'undefined' ? false : checkRegist});
        },
        addWeblogic:function(appId,domainId){
            return dbUtils.post("/app/"+appId+"/domain",{appid:appId,domainId:domainId})
        },
        deleteWeblogic:function(appId,domainId){
            return dbUtils.delete("/app/"+appId+"/domain/"+domainId);
        },
    }
}]);

angular.module("app").service('ManagerTomcatService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    return {
        getTomcat:function(appId,checkRegist){
            return dbUtils.get("/middleware/tomcat/"+appId+"/busy",null);
        },
        addTomcat:function(appId,tomcatId){
            return dbUtils.post("/middleware/tomcat/"+appId+"/"+tomcatId+"",null)
        },
        deleteTomcat:function(appId,tomcatId){
            return dbUtils.delete("/middleware/tomcat/"+appId+"/"+tomcatId+"",null);
        },
    }
}]);

angular.module("app").service('ManagerSpringBootService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    return {   
    	getAgentList:function(scope){
            dbUtils.get("/client/listAgent",null).then(function(response){
            	if(response.code == SUCCESS_CODE){
            		scope.clients = response.data;
            		return scope.clients; 
            	}else{
            		dbUtils.error(dbUtils.message,"提示");
            	}
            });
        },
        addSpringbootRelation:function(appId,data){//{ip:data.springbootHost,port:data.springbootPort,prometPort:data.prometPort,clientId:data.deployFileId}
        	return dbUtils.postBody("/app/"+appId+"/add/springboot",JSON.stringify(data));
        },
        addSpringbootRelations:function(appId,data){
        	return dbUtils.postBody("/app/"+appId+"/add/springboots",JSON.stringify(data));
        },
        getSpringBoot:function(appId){
            return dbUtils.get("/app/"+appId+"/busy/springboots",null);
        },
        addSpringBoot:function(appId,appSpringbootId,ip,port,prometPort){
//            return dbUtils.post("/app/"+appId+"/springboots/"+appSpringbootId,{ports:ports,prometPorts:prometPorts})
            return dbUtils.post("/app/"+appId+"/add/springboots",{ip:ip,port:port,prometPort:prometPort,clientId:appSpringbootId})
        },
        deleteSpringBoot:function(appId,appSpringbootId){
            return dbUtils.delete("/app/"+appId+"/springboots/"+appSpringbootId,null);
        },
        getPorts:function(){
            return dbUtils.get("/ports/getPorts",null);
        },
        getPortsByIp:function(ip){
            return dbUtils.get("/ports/getPortsByIp",{"ip":ip});
        }
    }
}]);

angular.module("app").service('CustomManagerService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    return {        
    	listAppDeployCustom:function(appId){
            return dbUtils.get("/app/"+appId+"/appDeployCustom/get",null);
        },
        addAppDeployCustom:function(appDeployCustom){
            return dbUtils.put("/app/"+appDeployCustom.appId+"/appDeployCustom/add",appDeployCustom);
        },
        deleteAppDeployCustom:function(appDeployCustom){
            return dbUtils.delete("/app/"+appDeployCustom.appId+"/appDeployCustom/del",{id:appDeployCustom.id});
        },
        getNginxList:function(scope,fun){
        	dbUtils.get("/nginx/list").then(fun);
        },
        getWeblogicList:function(scope,fun){
        	dbUtils.get("/domain/listAll").then(fun);
        },
        getTomcatList:function(scope,fun){
        	dbUtils.get("/middleware/tomcat/listAll").then(fun);
        },
        getNginxListStatus:function(scope,fun){
        	dbUtils.get("/nginx/list").then(fun);
        },
        getPorts:function(){
            return dbUtils.get("/ports/getPorts",null);
        },
        getPortsByIp:function(ip){
            return dbUtils.get("/ports/getPortsByIp",{"ip":ip});
        },
    }
}]);

