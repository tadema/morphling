var multiLog = angular.module("multiEchoLog", []);
multiLog.factory("multiEchoLog", ["$http", "$window", "$q", "toaster", "$modal",'$interval', 'dbUtils',function ($http, $window, $q, toaster, $modal,$interval,dbUtils){

    function LogController ($scope,$modalInstance,source){
        $scope.rowObj = source.rowObj;
        $scope.logs = source.logs;
        var ws = location.protocol === 'https:'? 'wss':'ws';
        $scope.finishNum = 0;
        var printing = false;
        $scope.stopWebsocket = false;

        //初始化数据
        function initData() {
            for(let i=0;i<$scope.logs.length;i++){
                let log = $scope.logs[i];
                websocketLifeCycle(log);
            }
        }
        initData();

        //websocket的生命周期
        function websocketLifeCycle(log) {
            log.websocket = new WebSocket(ws + "://"+window.location.host+"/echo?logId=" + log.logId);

            log.websocket.onopen = function(event) {}

            // 监听消息
            log.websocket.onmessage = function(event) {
                if($scope.stopWebsocket == false){
                    printing = true;
                    doWrite(log.logId, event.data);
                }
            };

            // 监听Socket的关闭
            log.websocket.onclose = function(event) {
                if($scope.stopWebsocket == false){
                    clearInterval();
                    $scope.$apply(function(){
                        $scope.finishNum++;
                        log.isStop = true;
                        if($scope.finishNum == $scope.logs.length)
                            $scope.finished = true;
                    })
                }
            };
        }

        //定时监控，滚动到底部
        var logInternal = setInterval(function(){
        	if($scope.stopWebsocket == false){
        		var e = angular.element("#log_container")[0];
                if(printing && angular.element("#scrollControl")[0].checked){
                    e.scrollTop = e.scrollHeight;
                    printing = false;
                }
            }         
        },200);

        //取消按钮操作
        $scope.cancel = function () {
            if($scope.finished){
                doClose();
                return;
            }
            for(let i=0;i<$scope.logs.length;i++){
                let log = $scope.logs[i];
                if(log.isStop)
                    continue;

                dbUtils.get("/logs/closeLog",{logId:log.logId}).then(function (response) {
                    /*if (response.code == SUCCESS_CODE) {
                        dbUtils.info("关闭成功","提示");
                    } else {
                        dbUtils.error(response.message,"提示");
                    }*/
                    $scope.stopWebsocket = true;
                    printing = false;
                    doClose();
                },function (response) {
                    $scope.stopWebsocket = true;
                    printing = false;
                    doClose();
                })
            }
        };

        let clearInterval = function(){
            window.clearInterval(logInternal);
        }

        $scope.ok = function () {
            doClose();
        };

        let doClose = function () {
            if(logInternal)
                clearInterval();
            $modalInstance.close('cancel');
        };
    }

    //写操作
    function doWrite(divId,line){
        var e = angular.element("#"+divId)[0];
        e.innerHTML = e.innerHTML + line + "<br/>";
    }

    return {
        /**
         *
         * @param logIdArr logId数组，后台返回
         * @param okFun 回调函数
         * @param rows 表格选中的数据
         * @param logIdFieldName logId与数据的对应关系
         * @param titleFieldName title字段
         */
        show:function(logIdArr,okFun,rows,logIdFieldName,titleFieldName){
            if(!logIdFieldName)
                logIdFieldName = "id";
            if(!titleFieldName)
                titleFieldName = "name";
            let rowObj = {},logs = [];
            for(let i=0;i<rows.length;i++){
                rowObj[rows[i][logIdFieldName]] = rows[i];
                let row = Object.assign({"logId":rows[i][logIdFieldName]+"", "title":rows[i][titleFieldName], "isStop":false}, rows[i]);
                logs.push(row);
            }
            dbUtils.openModal('tpl/log/multi_log.html',['$scope', '$modalInstance','source', LogController],"lg","modal-larger",
                {"rowObj":rowObj, "logs":logs},okFun);
        }
    }
}]);
