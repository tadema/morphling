'use strict';

angular.module("app").service('VisionService', ['dbUtils', '$http', '$q','$cookies', function (dbUtils, $http, $q,$cookies) {
    return {
        queryVision:function(appId){
//        	return dbUtils.get("/deploy/getAppVersionList",{appId:appId})
        	return dbUtils.get("/deploy/app/version",{appId:appId});
        },
//        doCleanVision:function(data){
//            return dbUtils.post("/deploy/deleteVersion",{info:data});
//        },
        doCleanVision:function(appId,data,isLogicDelete){
        	return dbUtils.postBody("/deploy/delete/app/version?appId="+appId+"&isLogicDelete="+isLogicDelete,JSON.stringify(data));
        },
        getApp:function(appId){
            return dbUtils.get("/app/preview/"+appId,{});
        },
    }
}]);