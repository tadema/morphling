'use strict';

angular.module("app").service('BaseInfoService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    return {
        getProvinces: function(){
            return dbUtils.get("/base/position/provinces");
        },
        getCities: function(provinceId){
            return dbUtils.post("/base/position/cities",{provinceId:provinceId});
        },
        getAreas: function(cityId){
            return dbUtils.post("/base/position/areas",{cityId:cityId});
        },
        agentSaleStatistics: function(){
            return dbUtils.post("/statistics/agentSaleStatistics");
        },
        getAppsTest: function(queryParams){
            return dbUtils.get("/app", queryParams);
        },
        getDeveloper: function(){
            return dbUtils.get("/welcome/getDeveloperInfo");
        },
        getApps: function(queryParams){
            return dbUtils.get("/welcome/getAppInfo", queryParams);
        },
        /*getApp: function(domainId){
            return dbUtils.get("/welcome/getMServerInfo?domainId="+domainId, null);
        },*/
        getApp: function(id,middlewareType){
            return dbUtils.get("/welcome/getServerInfo", {middlewareType:middlewareType,id:id});
        },
        getStatus: function(id,middlewareType){
            return dbUtils.get("/welcome/checkStatus", {middlewareType:middlewareType,id:id});
        },
        //检查状态
        checkDeployStatus:function(appId,id){
            return dbUtils.get("/endpoint/getDeployInfo?appId="+appId+"&id="+id,null)
        },
    }
}]);

angular.module("app").service('NewHomeService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    return {
        getProvinces: function(){
            return dbUtils.get("/base/position/provinces");
        },
        getCities: function(provinceId){
            return dbUtils.post("/base/position/cities",{provinceId:provinceId});
        },
        getAreas: function(cityId){
            return dbUtils.post("/base/position/areas",{cityId:cityId});
        },
        agentSaleStatistics: function(){
            return dbUtils.post("/statistics/agentSaleStatistics");
        },
        getAppsTest: function(queryParams){
            return dbUtils.get("/app", queryParams);
        },
        getDeveloper: function(){
            return dbUtils.get("/welcome/getDeveloperInfo");
        },
        getApps: function(queryParams){
            return dbUtils.get("/welcome/getAppInfo", queryParams);
        },
        getApp: function(id,middlewareType){
            return dbUtils.get("/welcome/getServerInfo", {middlewareType:middlewareType,id:id});
        },
        getStatus: function(id,middlewareType){
            return dbUtils.get("/welcome/checkStatus", {middlewareType:middlewareType,id:id});
        },
        //检查状态
        checkDeployStatus:function(appId,id){
            return dbUtils.get("/endpoint/getDeployInfo?appId="+appId+"&id="+id,null)
        },
        //卡片信息
        getHomeInfo: function(){
            return dbUtils.get("/welcome/getHomeAppInfo",{});
        },
        //应用信息
        getAppInfo: function(){
            return dbUtils.get("/welcome/app/info",{});
        },
        //运维信息
        getlogInfo: function(){
            return dbUtils.get("/welcome/log/history",{});
        },
        //获取应用服务的健康检查
        healthCheck: function(url,id){
        	return dbUtils.post("/welcome/checkServerAvailable",{url:url,id:id});
        },
        healthCheckAll: function(healthCheckVOs){
        	return dbUtils.postBody("/welcome/checkServersAvailable",JSON.stringify(healthCheckVOs));
        },
    }
}]);


//angular.module("app").factory('echarts', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
////	'use strict';
//    /*return {
//    	init: function (element) {
//            this.chart = echarts.init(element);
//            if(this.option_style === 'style1'){
//                this.option=this.option1;
//            }else if(this.option_style === 'style2'){
//                this.option=this.option2;
//            };
//            this.chart.setOption(this.option);
//            if(this.option_new !== null && typeof this.option_new !== 'undefined') {
//                this.chart.setOption(this.option_new);
//            }
//            this.option = this.chart.getOption();
//            return this.chart;
//        };
//    }*/
//    return echarts;
//}]);

/*
angular.module('hrss.si.nk.monitorModule').factory('baseEchart',function(){
return {};
});

monitorModule.factory('echarts',['$http',function($http){
    'use strict';
    // $http.get('./modules/monitor/base/maps/json/geometryCounties/211100.json').success(function (mapJson) {
//    $http.get('./modules/monitor/base/maps/json/china.json').success(function (mapJson) {
//        echarts.registerMap('china', mapJson);
//    });
    return echarts;
}]);*/