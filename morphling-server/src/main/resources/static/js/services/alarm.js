'use strict';

angular.module("app").service('AlarmService', ['dbUtils', '$http', '$q','$cookies', function (dbUtils, $http, $q,$cookies) {
    return {
        saveOrUpdate:function(data){
            return dbUtils.postBody("/alertRules/save",data)
        },
        delete:function(id,alertName){
            return dbUtils.delete("/alertRules/delete/"+id+"/"+alertName,null);
        },
        listAlertInfoByDateCondition:function(start,end){
            return dbUtils.get("/alert/listAlertInfoByDateCondition",{start:start,end:end})
        },
    }
}]);

'use strict';

angular.module("app").service('AlarmConfigService', ['dbUtils', '$http', '$q','$cookies','$rootScope', function (dbUtils, $http, $q,$cookies, $rootScope) {
    return {
        saveOrUpdate:function(data){
            return dbUtils.postBody("/alertManagerServer/save",data)
        },
        delete:function(id){
            return dbUtils.post("/alertManagerServer/delete",{id:id});
        },
        getConfig:function(){
            return dbUtils.get("/alertManagerServer/getConfig",null)
        },
        saveConfig:function(data){
            return dbUtils.post("/alertManagerServer/saveConfig",{info:data})
        },
        startOrStopAlarmServer:function(data){
            return dbUtils.post("/monitor/controllerMornitorStatus",data);
        },
        alarmRemindList:function(){
            return dbUtils.get("/alertSenderConfig/list",null);
        },
        alarmRemindSave:function(data){
            return dbUtils.postBody("/alertSenderConfig/save",data);
        },
        alarmRemindDelete:function(id){
            return dbUtils.post("/alertSenderConfig/delete",{id:id});
        },
        getPorts:function(){
            return dbUtils.get("/ports/getPorts",null);
        },
        getPortsByIp:function(ip){
            return dbUtils.get("/ports/getPortsByIp",{"ip":ip});
        },
        //初始化页面
        initViewAndDataHandle: function($scope){
        	$scope.title = ["1.告警服务器配置","2.告警配置文件","3.告警提醒方式配置"];
        	if($scope.isEdit){
        		$scope.pageOneData = $scope.data;
        		this.getConfig().then(function(response){
        			if(response.code == SUCCESS_CODE){
        				$scope.pageTwoData = {data:response.data};
        			}else{
        				dbUtils.error(response.message,"提示");
        			}
        		});
        	}else{
        		$scope.pageOneData = {};
        		$scope.pageTwoData = {};
        	}
        	let heightValue1 = $rootScope._screenProp.contentHeight - 20 - 39 -65 -30 -30 -16;
        	$scope.pageOneBodyStyle = {"height": heightValue1 + "px"};
        	
        	let heightValue2 = $rootScope._screenProp.contentHeight - 20 - 39 -65 -30 -30 -16;
        	$scope.pageTwoBodyStyle = {"height": heightValue2 + "px"};
        	$scope.editorStyle = {"height": heightValue2 - 30 + "px"};
        },
        //第一页	告警服务器配置	退出校验
        pageOneExitValidation: function(context,$scope){
        	if(!$scope.pageOneData.name){
        		dbUtils.warning("请输入名称");
                return false;
            }
        	if(!$scope.pageOneData.ip){
        		dbUtils.warning("请输入IP");
                return false;
            }
        	if(!$scope.pageOneData.port){
        		dbUtils.warning("请输入端口");
                return false;
            }
        	if(!$scope.pageOneData.remark){
        		dbUtils.warning("请输入描述");
                return false;
            }
        	var idDuplicate = $scope.checkPortDuplicate($scope.pageOneData.ip,$scope.pageOneData,['port']);
            if(idDuplicate)
            	return false;
        	return true;
        },
        //第二页	告警配置文件	退出校验
        pageTwoExitValidation: function(context,$scope,editor){
        	if(editor.getValue() == "" || editor.getValue() == null){
        		dbUtils.warning("请输入配置文件");
                return false;
        	}
        	$scope.pageTwoData.data = editor.getValue();
        	return true;
        },
        initEditor: function(){
        	var editor = ace.edit("pre_code_editor_alarmconfigview02"); //初始化对象
            editor.setTheme("ace/theme/eclipse");//设置风格和语言 tomorrow_night_eighties chrome github
            editor.session.setMode("ace/mode/yaml");
            editor.$blockScrolling = Infinity;
            editor.setOptions({
           	    enableBasicAutocompletion: true,
           	    enableSnippets: true,
           	    enableLiveAutocompletion: true
            });
            return editor;
        },
        initPageThreeTable: function($scope){
            $scope.alertSenderUrlParams = {url:"/alertSenderConfig/list",urlBody:{},method:"get"};
            var heightValue = $rootScope._screenProp.contentHeight+(-20 - 39 -65 -30 -15-38 -34 -56 -10)+"px";
            $scope.alertSenderTableParams = {bodyStyle:{"height": heightValue,"max-height": heightValue}};
        	$scope.queryParams = {isPagination:true,postgroundPagination:false,pageNumber: 1,pageSize:10,prevPageDisabled: 'disabled',nextPageDisabled: 'disabled'};
        	$scope.alertSenderHeaders = [
        		{name:"客户端编号",field:"appNumber",style:{"width":"12%"}},
                {name:"类型",field:"type",style:{"width":"8%"}},
                {name:"标题",field:"titlel",style:{"width":"10%"}},
                {name:"邮箱/手机/微信",style:{"width":"25%"},compile:true,formatter:function(value,row){
                	var val;
                    if("email" == row.type){
                    	val = row.email;
                    }else if("mobile" == row.type){
                    	val = row.mobile;
                    }else if("wechat" == row.type){
                    	val = row.wechat;
                    }
                    return "<span >"+val+"</span>"
                }},
                {name:"api地址",field:"apiUrl",style:{"width":"25%"}},
    		];
        	$scope.alertSenderRowEvents = [{
            	class:"btn btn-info",icon:"fa fa-pencil",title:"编辑",
            	click: function(row){
            		dbUtils.openModal('tpl/alarm/remind_edit_modal.html','RemindEditModalController',"lg","",{data:angular.copy(row)},function (data) {
            			dbUtils.postBody("/alertSenderConfig/save",JSON.stringify(data)).then(function(response){
                    		if(response.code == SUCCESS_CODE){
                    			dbUtils.info("保存成功","提示");
                    			$scope.$broadcast("refreshData",{});
                    		}else{
                    			dbUtils.error(response.message,"提示");
                    		}
                    	})
                    });
            	}
            },{
            	class:"btn-danger",icon:"fa fa-trash",title:"删除",
            	isShowPopover:true,placement:"auto left",question:"确定删除吗",
                clickOk:function(row,event){
                	dbUtils.post("/alertSenderConfig/delete",{id:row.id}).then(function(response){
                		if(response.code == SUCCESS_CODE){
                			dbUtils.info("删除成功","提示");
                			$(event.target).parents(".popover").prev().click();
                			$scope.$broadcast("refreshData",{});
                        }else{
                            dbUtils.error(response.message,"提示"); 
                        }
                	},function(){
                	})
                },
                clickCancel:function(row,event){
                	$(event.target).parents(".popover").prev().click();
                }
            }];
            $scope.alertSenderOperationEvents = [{
                class:"btn-info",icon:"glyphicon glyphicon-plus",name:"新增告警提醒",
                click:function(){
                	dbUtils.openModal('tpl/alarm/remind_edit_modal.html','RemindEditModalController',"lg","",{},function (data) {
                		dbUtils.postBody("/alertSenderConfig/save",JSON.stringify(data)).then(function(response){
                    		if(response.code == SUCCESS_CODE){
                    			dbUtils.info("保存成功","提示");
                    			$scope.$broadcast("refreshData",{});
                    		}else{
                    			dbUtils.error(response.message,"提示");
                    		}
                    	})
                    });
                }
            }];
        },
        saveOrUpdateConfig: function($scope){
        	this.saveOrUpdate(JSON.stringify($scope.pageOneData)).then(function(response){
    			if(response.code == SUCCESS_CODE){
    				dbUtils.post("/alertManagerServer/saveConfig",{info:$scope.pageTwoData.data}).then(function(response){
    					if(response.code == SUCCESS_CODE){
    						dbUtils.info("保存成功","提示");
    						$scope.returnMain();
    					}else{
    						dbUtils.error(response.message,"提示");
    					}
    				});
    			}else{
    				dbUtils.error(response.message,"提示");
    			}
    		})
        }
    }
}]);


