'use strict';

angular.module("app").service('MonitorService', ['dbUtils', '$http', '$q','$cookies', function (dbUtils, $http, $q,$cookies) {
    return {
        queryVision:function(appId){
            return dbUtils.get("/deploy/getAppVersionList",{appId:appId})
        },
        doCleanVision:function(data){
            return dbUtils.post("/deploy/deleteVersion",{info:data});
        },
        getMonitorData:function(){
            return dbUtils.get("/monitor/getClassifyInfo",{});
        },
        getMonitorType:function(){
            return dbUtils.get("/monitor/getMonitorType",null);
        },
        getUrl:function(id){
            return dbUtils.get("/monitor/getUrlById",{id:id})
        },
        updateMonitorPort:function(data){
            return dbUtils.postBody("/monitor/updateMonitorPort",data)
        },
        getPorts:function(){
            return dbUtils.get("/ports/getPorts",null);
        },
        getPortsByIp:function(ip){
            return dbUtils.get("/ports/getPortsByIp",{"ip":ip});
        },
        saveOrUpdateMonitorDatabase:function(data){
        	return dbUtils.postBody("/database/add",JSON.stringify(data));
        },
        deleteMonitorDatabase:function(id){
        	return dbUtils.post("/database/delete",{id:id});
        },
        checkConnected:function(data){
            return dbUtils.postBody("/database/checkConnect",data);
        },
        monitorOracleQuery:function(data,sqlId){
        	return dbUtils.postBody("/database/monitor/oracle?sqlId="+sqlId,JSON.stringify(data));
        },
        generateAWR:function(data,startTime,endTime){
        	return dbUtils.postBody("/database/monitor/oracle/oracleAWR?startDate="+startTime+"&endDate="+endTime,JSON.stringify(data));
        },
        //ogg
        saveMonitorOgg:function(data){
        	return dbUtils.postBody("/goldengate/add",JSON.stringify(data));
        },
        updateMonitorOgg:function(data){
        	return dbUtils.postBody("/goldengate/save",JSON.stringify(data));
        },
        deleteMonitorOgg:function(id){
        	return dbUtils.post("/goldengate/delete",{id:id});
        },
        listInfoAll:function(data){
        	return dbUtils.postBody("/goldengate/listInfoAll",JSON.stringify(data));
        },
        checkConnectOgg:function(data){
        	return dbUtils.postBody("/goldengate/checkConnect",JSON.stringify(data));
        },
        getDirPrmFile:function(data,name){
        	return dbUtils.postBody("/goldengate/getDirPrmFile?name="+name,JSON.stringify(data));
        },
        getProcessDetail:function(data,name){
        	return dbUtils.postBody("/goldengate/getProcessDetail?name="+name,JSON.stringify(data));
        },
        getCheckpointInfo:function(data,name){
        	return dbUtils.postBody("/goldengate/getCheckpointInfo?name="+name,JSON.stringify(data));
        },
        getStats:function(data,name){
        	return dbUtils.postBody("/goldengate/getStats?name="+name,JSON.stringify(data));
        },
        getLagProcess:function(data,name){
        	return dbUtils.postBody("/goldengate/getLagProcess?name="+name,JSON.stringify(data));
        },
        getMailServers:function(data){
            return dbUtils.get("/mailSmtpConfig/listIsEnable",data)
        },
        getMysqlStatus:function(data,sqlId){
            return dbUtils.postBody("/database/monitor/mysql/getMysqlStatus?sqlId="+sqlId,JSON.stringify(data));
        },
        getMysqlVariables:function(data,sqlId){
            return dbUtils.postBody("/database/monitor/mysql/getMysqlVariables?sqlId="+sqlId,JSON.stringify(data));
        },
        getMysqlMonitorData:function(data){
            return dbUtils.postBody("/database/monitor/mysql",JSON.stringify(data));
        },
        monitorMysqlExcute:function(data,sqlId){
            return dbUtils.postBody("/database/monitor/mysql/excute?sqlId="+sqlId,JSON.stringify(data));
        },
        getMysqlVersion:function(data){
            return dbUtils.postBody("/database/monitor/getMysqlVersion",JSON.stringify(data));
        },
        setLoading:function($scope,num,flag){
            if(num == 1){
                if($scope.thread.methods.setLoading)
                    $scope.thread.methods.setLoading(flag);
                if($scope.qpsAndTps.methods.setLoading)
                    $scope.qpsAndTps.methods.setLoading(flag);
                if($scope.network.methods.setLoading)
                    $scope.network.methods.setLoading(flag);
            }else if(num == 2){
                if($scope.connect.methods.setLoading){
                    $scope.connect.methods.setLoading(flag);
                }else
                    $scope.connectInitData = [];
                if($scope.buffer.methods.setLoading)
                    $scope.buffer.methods.setLoading(flag);
                else
                    $scope.bufferInitData = [];
                if($scope.sqlNum.methods.setLoading)
                    $scope.sqlNum.methods.setLoading(flag);
                if($scope.readAndWrites.methods.setLoading)
                    $scope.readAndWrites.methods.setLoading(flag);
                if($scope.openNum.methods.setLoading)
                    $scope.openNum.methods.setLoading(flag);
            }else if(num == 3){
                if($scope.innodb.methods.setLoading)
                    $scope.innodb.methods.setLoading(flag);
                else
                    $scope.innodbInitData = [];
                if($scope.bufferHitRatio.methods.setLoading)
                    $scope.bufferHitRatio.methods.setLoading(flag);
                if($scope.bufferUseRatio.methods.setLoading)
                    $scope.bufferUseRatio.methods.setLoading(flag);
                if($scope.bufferDirtyRatio.methods.setLoading)
                    $scope.bufferDirtyRatio.methods.setLoading(flag);
                if($scope.readAndWrite.methods.setLoading)
                    $scope.readAndWrite.methods.setLoading(flag);
                if($scope.readAndWriteNum.methods.setLoading)
                    $scope.readAndWriteNum.methods.setLoading(flag);
            }
        },

    }
}]);


angular.module("app").service('MonitorRedisService', ['dbUtils', '$http', '$q','$cookies', function (dbUtils, $http, $q,$cookies) {
    return {
        saveCluster:function(data){
            return dbUtils.postBody("/redis/cluster/importCluster",JSON.stringify(data));
        },
        updateCluster:function(data){
            return dbUtils.postBody("/redis/cluster/updateCluster",JSON.stringify(data));
        },
        deleteCluster:function(data){
            return dbUtils.postBody("/redis/cluster/deleteCluster",JSON.stringify(data));
        },
        getDBList:function(clusterId){
            return dbUtils.get("/redis/cluster/getDBList/"+clusterId,{});
        },
        scan:function(data){
            return dbUtils.postBody("/redis/cluster/scan",JSON.stringify(data));
        },
        query:function(data){
            return dbUtils.postBody("/redis/cluster/query",JSON.stringify(data));
        },
        validateRedisNode:function(data){
            return dbUtils.postBody("/redis/cluster/validate/redisNode",JSON.stringify(data));
        },
        getRedisNodeList:function(data){
            return dbUtils.postBody("/redis/cluster/getRedisNodeList",JSON.stringify(data));
        },
        getSentinelMasterInfo:function(data){
            return dbUtils.postBody("/redis/cluster/getSentinelMasterInfo",JSON.stringify(data));
        },
        getNodeInfo:function(data){
            return dbUtils.postBody("/redis/cluster/getNodeInfo",JSON.stringify(data));
        },
        getConfig:function(data){
            return dbUtils.postBody("/redis/cluster/getConfig",JSON.stringify(data));
        },
        getInfoItemMonitorData:function(data){
            return dbUtils.postBody("/redis/cluster/getInfoItemMonitorData",JSON.stringify(data));
        },
        //获取树状格式数据
        filterArray:function(data, parent) {
            var tree = [];
            var temp;
            for (var i = 0; i < data.length; i++) {
                if (data[i].masterId == parent) {
                    var obj = data[i];
                    temp = this.filterArray(data, data[i].nodeId);
                    if (temp.length > 0) {
                        obj.children = temp;
                    }
                    tree.push(Object.assign({"id":obj.redisNodeId, "name":obj.host+":"+obj.port, "isParent": false, "children": obj.children},obj));
                }
            }
            return tree;
        },
        //按树层级数据转化为数组
        treeLevelConvertArray:function(data,arr){
            for(var i=0; i<data.length; i++){
                arr.push(data[i]);
                if(data[i].children)
                    this.treeLevelConvertArray(data[i].children,arr)
            }
            return arr;
        },
        //获取组织机构树状图数据
        //$scope.ztreeData,$scope.idToLabel
        //idToLabel--id和label的对应关系
        getOrgZTreeData:function($scope){
            this.queryList().then(function(response) {
                var data = response.data;
                $scope.idToLabel = {};

                $scope.ztreeData = org.filterArray(data, null);

                data.map(function(item){
                    $scope.idToLabel[item.orgNo] = item.orgName;
                    return item;
                })
            });
        },
    }
}]);

angular.module("app").service('MonitorDefineService', ['dbUtils', '$http', '$q','$cookies', function (dbUtils, $http, $q,$cookies) {
    return {
    	saveOrUpdateMonitor:function(data){
            return dbUtils.post("/monitor/saveOrUpdateMonitor",data)
        },
        getMonitorType:function(){
            return dbUtils.get("/monitor/getMonitorType",null);
        },
        deleteMonitor:function(id){
            return dbUtils.post("/monitor/deleteMonitor",{id:id});
        },
        
        getMonitorServer:function(){
            return dbUtils.get("/monitor/listAllMonitorServer",null);
        },
        deleteMonitorServer:function(id){
            return dbUtils.post("/monitor/deleteMonitorServer",{id:id});
        },
        saveOrUpdateMonitorServer:function(data){
            return dbUtils.post("/monitor/saveOrUpdateMonitorServer",data)
        },
        getGraphServer:function(){
            return dbUtils.get("/monitor/listAllMonitorServer",null);
        },
        saveOrUpdateGraphServer:function(data){
            return dbUtils.post("/monitor/saveOrUpdateMonitorServer",data)
        },
        startOrStopGraphServer:function(data){
            return dbUtils.post("/monitor/controllerMornitorStatus",data);
        },
        getPorts:function(){
            return dbUtils.get("/ports/getPorts",null);
        },
        getPortsByIp:function(ip){
            return dbUtils.get("/ports/getPortsByIp",{"ip":ip});
        }
    }
}]);