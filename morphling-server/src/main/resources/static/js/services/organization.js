'use strict';

angular.module("app").service('OrganizationService', ['dbUtils', '$http', '$q','$cookies', function (dbUtils, $http, $q,$cookies) {
    let org = {
    	queryList:function(){
            return dbUtils.get("/organizaiton/list",{});
        },
        saveOrUpdate:function(data){
            return dbUtils.postBody("/organizaiton/save",data);
        },
        delete:function(orgNo){
            return dbUtils.post("/organizaiton/delete",{orgNo:orgNo});
        },
        //获取树状格式数据
        filterArray:function(data, parent) {
            var tree = [];
            var temp;
            for (var i = 0; i < data.length; i++) {
                if (data[i].parentOrgNo == parent) {
                    var obj = data[i];
                    temp = this.filterArray(data, data[i].orgNo);
                    if (temp.length > 0) {
                        obj.children = temp;
                    }
                    tree.push(Object.assign({"id":obj.orgNo, "name":obj.orgName, isParent: false, "children": obj.children},obj));
                }
            }
            return tree;
        },
    	//按树层级数据转化为数组
        treeLevelConvertArray:function(data,arr){
    		for(var i=0; i<data.length; i++){
    			arr.push(data[i]);
    			if(data[i].children)
    				this.treeLevelConvertArray(data[i].children,arr)
    		}
    		return arr;
    	},
    	//获取组织机构树状图数据
    	//$scope.ztreeData,$scope.idToLabel
    	//idToLabel--id和label的对应关系
    	getOrgZTreeData:function($scope){
        	this.queryList().then(function(response) {
        		var data = response.data;
        		$scope.idToLabel = {};
        		
        		$scope.ztreeData = org.filterArray(data, null);
        		
        		data.map(function(item){
        			$scope.idToLabel[item.orgNo] = item.orgName;
        			return item;
        		})
        	});
        },
        permissionControl:function(rootScope){
        	let isAdmin = rootScope._userinfo.isAdmin == 1 ? true : false;
        	if(!isAdmin)
        		dbUtils.warning("权限不足，请管理员处理!");
        	return isAdmin;
        }
    }
    return org;
}]);
