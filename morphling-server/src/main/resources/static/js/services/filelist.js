'use strict';


angular.module("app").service('FileListService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    return {        
        //script
        getScriptType:function(){
        	return dbUtils.get("/script/getScriptTypeData",null);
        },
        deleteScript:function(scriptId){
            return dbUtils.post("/script/remove",{scriptId:scriptId});
        },
        doSaveScript:function(script){
            return dbUtils.post("/script/saveOrUpdate",script);
        },
        scriptHistory:function(scriptName){
            return dbUtils.get("/logs/getHistoryLogs",{scriptName:scriptName});
        },
        //scriptEdit
        getScript:function(scriptId){
            return dbUtils.get("/script/find/"+scriptId,null);
        },
        //主机
        getClient:function(query){
            return dbUtils.get("/client/listByFilter",query);
        },
        doClient:function(query){
            return dbUtils.postBody("/script/executeScript",JSON.stringify(query));
        },
        health: function(data){
            return dbUtils.get("/client/health",data);
        },
        getLog:function(logIds){
            return dbUtils.get("/logs/getBy/ids",{logIds:logIds});
        },
        //file
        deleteFile:function(fileId,fileName){
            return dbUtils.post("/file/manage/remove",{fileId:fileId,fileName:fileName});
        },
        distributeFile:function(clientIds,fileInfo){
        	var fileInfoVO = {clientId:clientIds,fileInfo:fileInfo};
            return dbUtils.postBody("/file/manage/distributeFile",JSON.stringify(fileInfoVO));
        },
        getLogs:function(logIds){
            return dbUtils.get("/logs/getFile/byIds",{logIds:logIds});
        },
        getFile:function(fileId){
            return dbUtils.get("/file/manage/findByFileId",{fileId:fileId});
        },
        uploadFile:function($scope,file,isDuplicate){
        	console.log(file)
        	isDuplicate = !isDuplicate ? 1 : isDuplicate;
            var formData = new FormData();
            formData.append("file",file);
            $scope.loading = true;
            $http({
                method:'post',
                url:'/file/manage/uploadFiles?isDuplicate='+isDuplicate,//isDuplicate=1覆盖
                data:formData,
                headers:{'Content-Type':undefined} ,
                transformRequest:angular.identity//浏览器会帮我们把Content-Type 设置为 multipart/form-data.
            }).success(function(data){
            	$scope.loading = false;
            	if(data.code = "1000000"){
            		var response = data.data[0];
            		if(response.status == "1"){//成功
        				dbUtils.success(response.fileName + "上传成功","上传");
        			}else if(response.status == "0"){//失败
        				dbUtils.error(response.fileName + "上传失败","上传");
        			}else if(response.status == "2"){//重复
        				dbUtils.info("服务器存在与"+response.fileName + "完全相同的文件","上传");
        			}
        			$scope.table.operations.reloadData();
            	}
            }).error(function(data){
            	$scope.loading = true;
            	if(data == null){
        			dbUtils.error("上传失败","上传");
            	}
            });
    	},
    	getRemoteFileSize:function(fileAbsoPath,fileSize,client){
//            return dbUtils.post("/file/manage/getRemoteFileSize?fileName="+fileAbsoPath+"&fileSize="+fileSize,client);
            return dbUtils.postBody("/file/manage/getRemoteFileSize?fileName="+fileAbsoPath+"&fileSize="+fileSize,JSON.stringify(client));
        },
        //展示分发结果
        showDistributeLog:function(row){
            dbUtils.get("/logs/getFile/byIds",{logIds:[row.logId]}).then(function(response){
            	if(response.code == SUCCESS_CODE){
            		dbUtils.openModal('tpl/logtable/show_logs.html','FileDetailController',"lg","",{"data":response.data[0]},function (id) {},true);
    			}else{
    				dbUtils.warning(response.message,"提示");
    			}
            },function(response){
            	dbUtils.warning(response.message,"提示");
            });
        },
        cancelTask:function(fileName,client){
            return dbUtils.postBody("/file/manage/cancelTask?fileName="+fileName,JSON.stringify(client));
        },
    }
}]);



