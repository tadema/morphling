'use strict';

angular.module("app").service('RoleService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    return {
        list:function(){
            return dbUtils.get("/role")
        },
        listByUser:function(userId){
            return dbUtils.get("/user/"+userId+"/roles");
        },
        setByUser:function(userId,roleIds){
            return dbUtils.post("/user/"+userId+"/roles",{roleId:roleIds})
        },
        getUser:function(id){
            return dbUtils.get("/role/"+id+"/user",null);
        },
        addUser:function(roleId,userIds){
            return dbUtils.post("/role/"+roleId+"/user",{userIds:userIds});
        },
        deleteUser:function(roleId,id){
            return dbUtils.delete("/role/"+roleId+"/user",{id:id});
        }
    }
}]);

angular.module("app").service('RoleManagerService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    return {        
        getPostIp:function(){
            return dbUtils.get("/client",null);
        },
        deleteRole:function(id){
            return dbUtils.post("/role/remove",{id:id});
        },
        doSaveRole:function(role,envs){
//        	var newRoleData = JSON.stringify(role);
            return dbUtils.post("/role/saveOrUpdate?envs="+envs,role);
        },
        getMenu:function(id){
            return dbUtils.post("/role/"+id+"/menu",null);
        },
        postMenu:function(roleId,menuIds){
            return dbUtils.post("/role/distributeMenu",{roleId:roleId,menuIds:menuIds.join()});
        },
    }
}]);