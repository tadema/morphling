'use strict';

angular.module("app").service('SoftwareInstallService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
	
	let interval_time = 2000;
	//查询文件分发进度
    function queryFileProgress($scope,file,client,index){
    	let count = 0, max_count = 5, compare_value = 0;
    	let timer = setInterval(function(){
    		dbUtils.postBody("/file/manage/getRemoteFileSize?fileName="+file.fileAbsoPath+"&fileSize="+file.fileSize,JSON.stringify(client)).then(function(response){
    			if (response.code == SUCCESS_CODE) {
    				
	            	$scope.fileProgressResults[index].progressNum = response.data+"%";
	            	$scope.fileProgressResults[index].progressStyle = {width:response.data+"%"};
	            	$scope.fileProgressResults[index].timer = timer;
	            	//满足条件取消定时
	            	if(response.data == 100){
	            		$scope.fileProgressResults[index].status = 1;
	            		clearTimers(timer,$scope.timers);
	            	}
	            	if(compare_value == Math.round(response.data)){
	            		count++;
	            		if(count == max_count){//查询5次仍然相同，代表出错了，终止定时
	            			count = 0;
	            			$scope.fileProgressResults[index].status = 0;
	    	            	clearTimers(timer,$scope.timers);
	            		}
	            	}else{
	            		compare_value = Math.round(response.data);
	            		count = 0;
	            	}
	            	
	            } else {
	            	$scope.fileProgressResults[index].status = 0;
	            	clearTimers(timer,$scope.timers);
	            }
    		},function(){
    			$scope.fileProgressResults[index].status = 0;
    			clearTimers(timer,$scope.timers);
    		})
    	},interval_time)
    	$scope.timers.push(timer);
    }
    
    //清除interval定时,interval数组
    function clearTimers(intervalTimer,intervalTimerArray){
    	clearInterval(intervalTimer);
    	intervalTimerArray.splice(intervalTimerArray.indexOf(intervalTimer),1);
    }
	
    return {        
        getScriptType:function(){
        	return dbUtils.get("/script/getScriptTypeData",null);
        },
        deleteScript:function(scriptId){
            return dbUtils.post("/script/remove",{scriptId:scriptId});
        },
        doSaveScript:function(script){
            return dbUtils.post("/script/saveOrUpdate",script);
        },
        scriptHistory:function(scriptName){
            return dbUtils.get("/logs/getHistoryLogs",{scriptName:scriptName});
        },
        //scriptEdit
        getScript:function(scriptId){
            return dbUtils.get("/script/find/"+scriptId,null);
        },
        //主机
        getClient:function(query){
            return dbUtils.get("/client/listByFilter",query);
        },
        doClient:function(query){
            return dbUtils.postBody("/script/executeScript",JSON.stringify(query));
        },
        health: function(data){
            return dbUtils.get("/client/health",data);
        },
        getLog:function(logIds){
            return dbUtils.get("/logs/getBy/ids",{logIds:logIds});
        },
        getFiles:function(){
        	return dbUtils.get("/file/manage/listAllOnServer",{});
        },
        clientTransferBoxOption:function(){
        	return {
                orientation : 'horizontal',
                render : function(item){
                    return "主机名：" + item.name + '，IP：' + item.hostAddress + '，用户名：' + item.username;
                },
                titles : {
                    origin : '未选主机',target : '已选主机'
                },
                showSearch : true,searchPlaceholder : '请输入搜索内容',notFoundContent : '找不到数据'
                //classes : '',
            };
        },
        //文件分发检查
        distributeCheck:function($scope){
        	var flag = true;
        	if(!$scope.selectFiles || $scope.selectFiles.length == 0){
				dbUtils.warning("请选择至少一个文件");
				flag = false;
				return;
			}
        	if(!$scope.clientTargetKeys || $scope.clientTargetKeys.length == 0){
        		dbUtils.warning("请选择至少一个主机");
        		flag = false;
        		return;
        	}
			if(!$scope.softwareConfig.file.filePath){
				dbUtils.warning("请填写远程路径");
				flag = false;
				return;
			}
			return flag;
        },
        initFileInfo:function($scope,file){
        	var fileInfo = angular.extend({} , file);
        	fileInfo.filePath = $scope.softwareConfig.file.filePath;
        	fileInfo.fileSize = Number(file.fileSize);
        	fileInfo.isPathNotExist = 1;
        	fileInfo.isDuplicate = 1;
        	fileInfo.serverType = 'Linux主机';
        	fileInfo.fileType = "默认";
        	return fileInfo;
        },
        distributeFile:function(clientIds,fileInfo){
        	var fileInfoVO = {clientId:clientIds,fileInfo:fileInfo};
            return dbUtils.postBody("/file/manage/distributeFile",JSON.stringify(fileInfoVO));
        },
        reloadFileTableNoClient:function($scope){
    		$scope.fileProgressResults = [];
    		var count = 0;
    		var tempArr = [];
    		for(let file of $scope.selectFiles){
				let progressResult = {id:count, "fileName":file.fileName, "clientUsername":"--", "clientIp":"--", "distributeTime":new Date().format("yyyy-MM-dd hh:mm:ss")};
				tempArr.push(progressResult);
				count++;
    		}
    		$scope.fileProgressResults = tempArr;
    	},
    	reloadFileTableWithClient:function($scope,file){
			for(let clientId of $scope.clientTargetKeys){
				let client = $scope.clientIdtoObj[clientId];
				let logId = $scope.logIds[client.id];
	    		let result = {clientId:client.id,status:2,fileName:file.fileName,clientIp:client.hostAddress,progressNum:"0%",clientUsername:client.username,createTime:new Date().format("yyyy-MM-dd hh:mm:ss"),logId:logId};
				$scope.fileProgressResults.push(result);
				//查询文件分发进度
				queryFileProgress($scope,file,client,$scope.fileProgressResults.length-1);
			}
    	},
    	clearTimers:function(intervalTimer,intervalTimerArray){
    		clearTimers(intervalTimer,intervalTimerArray);
    	},
    	//后台请求取消分发
    	cancelTask:function(fileName,client){
            return dbUtils.postBody("/file/manage/cancelTask?fileName="+fileName,JSON.stringify(client));
        },
        //展示分发结果
        showResultLog:function(row){
            dbUtils.get("/logs/getFile/byIds",{logIds:[row.logId]}).then(function(response){
            	if(response.code == SUCCESS_CODE){
            		dbUtils.openModal('tpl/logtable/show_logs.html','DetailShowController',"lg","",{"data":response.data[0]},function (id) {},true);
    			}else{
    				dbUtils.error(response.message,"提示");
    			}
            },function(response){
            	dbUtils.error(response.message,"提示");
            });
        },
        getScriptType:function(){
        	return dbUtils.get("/script/getScriptTypeData",null);
        },
        //
        reloadScriptTableNoClient:function($scope){
    		$scope.scriptProgressResults = [];
    		var count = 0;
    		var tempArr = [];
    		for(let script of $scope.selectScripts){
				let progressResult = {id:count, "scriptName":script.scriptName, "clientUsername":"--", "clientIp":"--", "distributeTime":new Date().format("yyyy-MM-dd hh:mm:ss")};
				tempArr.push(progressResult);
				count++;
    		}
    		$scope.scriptProgressResults = tempArr;
    	},
    	executeCheck:function($scope){
        	var flag = true;
        	if(!$scope.selectScripts || $scope.selectScripts.length == 0){
				dbUtils.warning("请选择至少一个脚本");
				flag = false;
				return;
			}
        	if(!$scope.clientTargetKeys || $scope.clientTargetKeys.length == 0){
        		dbUtils.warning("请选择至少一个主机");
        		flag = false;
        		return;
        	}
			if(!$scope.softwareConfig.script.scriptPath){
				dbUtils.warning("请填写远程路径");
				flag = false;
				return;
			}
			return flag;
        },
        reloadScriptTableWithClient:function($scope,client,script){
//        	client.isBusy = true;//为主机赋繁忙的标志，防止重复运行
    		let result = {clientId:client.id,status:2,scriptName:script.scriptName,clientIp:client.hostAddress,progressNum:"0%",clientUsername:client.username,createTime:new Date().format("yyyy-MM-dd hh:mm:ss"),logId:""};
    		$scope.scriptProgressResults.push(result);
    		return $scope.scriptProgressResults.length;
    	},
    	executeScript:function(query){
            return dbUtils.postBody("/script/executeScript",JSON.stringify(query));
        },
        //更新执行状态
        updateExecuteStatus:function($scope,length,data,client){
//        	client.isBusy = false;
        	let num = 0;
        	if(parseInt(data.status) == 1 || parseInt(data.status) == 3){
        		num = 100;
        	}
        	$scope.scriptProgressResults[length-1].progressNum = num+"%";
        	$scope.scriptProgressResults[length-1].progressStyle = {width:num+"%"};
        	$scope.scriptProgressResults[length-1].status = data.status;
        	$scope.scriptProgressResults[length-1].content = data.result;
        },
        getScriptTypeData:function($scope,fun){
        	$scope.scriptData = {};
        	dbUtils.get("/script/getScriptTypeData",null).then(function(response){
    			if(response.code == SUCCESS_CODE){
    				$scope.scriptData.category = [];
    				for (var key in response.data.scriptCategory) {
    					$scope.scriptData.category.push({key:key,value:response.data.scriptCategory[key]});
    				}
    				$scope.scriptData.type = response.data.scriptType; 
	            	fun.call();
    			}else{
    				dbUtils.warning(response.message,"提示");
    			}
    		})
        }
    }
}]);



