'use strict';

angular.module("app").service('DomainService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    return {        
        saveOrUpdate:function(middleware){
            return dbUtils.put("/middleware",middleware);
        },
        saveOrUpdateTwo:function(middleware){
            return dbUtils.put("/domain/listAll",middleware);
        },
        submitOk:function(domain){
            return dbUtils.put("/domain",domain);
        },
        doSaveDomain:function(domain){
            return dbUtils.postBody("/domain",domain);
        },
        getPostIp:function(){
            return dbUtils.get("/client",null);
        },
        getAgentPost:function(){
            return dbUtils.get("/client/listAgent",null);
        },
        deleteDomain:function(domainId){
            return dbUtils.delete("/domain",{"domainId":domainId},null);
        },  
        createDomain:function(domainId){
            return dbUtils.postBody("/domain/addDomain?domainId="+domainId,null);
        },
        createManagedServer:function(domain){
            return dbUtils.postBody("/domain/addManagedServer",domain);
        },
        startAdminServer:function(domainId){
            return dbUtils.postBody("/deploy/startAdminServer?domainId="+domainId,null);
        },
        checkAdminStatus:function(domainId){
            return dbUtils.get("/domain/checkAdminIsStart?domainId="+domainId,null);
        },
        checkStatusAll:function(domains){
            return dbUtils.postBody("/domain/checkStatus",JSON.stringify(domains));
        },
        getPorts:function(){
            return dbUtils.get("/ports/getPorts",null);
        },
        getPortsByIp:function(ip){
            return dbUtils.get("/ports/getPortsByIp",{"ip":ip});
        },
        saveOrUpdateManagedServer:function(managedServer){
            return dbUtils.postBody("/domain/saveManagedServer",JSON.stringify(managedServer));
        },
        deleteManagedServer:function(managedServer){
            return dbUtils.post("/domain/delManagedServer",{id:managedServer.id});
        },
        checkStatus:function(ip,port){
        	return dbUtils.post("/middleware/tomcat/getTomcatStatus",{"ip":ip,"port":port});
        },
        checkHealth:function(healthCheckVOs){
        	return dbUtils.postBody("/domain/checkHealth",JSON.stringify(healthCheckVOs));
        },
        operateWeblogicServer:function(action,domainId,managedServer){
        	return dbUtils.postBody("/domain/operateWeblogicServer?action="+action+"&domainId="+domainId,JSON.stringify(managedServer));
        },
    }
}]);