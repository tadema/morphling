'use strict';

angular.module("app").service('MailServerService', ['dbUtils', '$http', '$q','$cookies', function (dbUtils, $http, $q,$cookies) {
    return {
        saveOrUpdate:function(data){
            return dbUtils.postBody("/mailSmtpConfig/save",data)
        },
        delete:function(id){
            return dbUtils.post("/mailSmtpConfig/delete",{id:id});
        },
    }
}]);
