var dbUtils = angular.module("dbUtils", []);

dbUtils.factory("dbUtils", ["$http", "$window", "$q", "toaster", "$modal",'$state','$rootScope', DbFetch])

.directive('bindUnsafeHtml', [function () {
	var success_class = "\"text-success-dker fa-check-circle\"" , info_class = "\"text-info-dker fa-info-circle\"";
	var str_style = "{\"success\":"+success_class+",\"info\":"+info_class+",\"warning\":\"text-warning-dker fa-exclamation-circle\",\"error\":\"text-danger-dker fa-times-circle\"}[directiveData.type]";
	return {
		template: "<i style='margin-right:10px;' class='fa fa-lg' ng-class='"+str_style+"'></i><span ng-bind-html='directiveData.content'></span>"
	};
}]);

function DbFetch($http, $window, $q, toaster, $modal,$state,$rootScope) {
    var promise = false;
    
    Date.prototype.format = function(fmt) {
	    var o = {
	        "M+": this.getMonth() + 1, //月份 
	        "d+": this.getDate(), //日 
			"h+": this.getHours(), //小时 
			"m+": this.getMinutes(), //分 
			"s+": this.getSeconds(), //秒 
			"q+": Math.floor((this.getMonth() + 3) / 3), //季度 
			"S": this.getMilliseconds() //毫秒 
		};
		if(/(y+)/.test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 -RegExp.$1.length));
		}
		for(var k in o) {
			if(new RegExp("(" + k + ")").test(fmt)) {
			    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
			}
		}
		return fmt;
	}

    function getHtml(content, type) {
        var html = [];
        html.push('<div class="modal-header">');
//        html.push('<h3 class="modal-title">提示</h3>');
        html.push('<span class="modal-title lg-font">提示</span>');
        html.push('</div>');
        html.push('<div class="modal-body">');
        html.push(content);
        html.push('</div>');
        html.push('<div class="modal-footer">');
        if (type == "confirm") {
        	html.push('<button class="btn btn-default" type="button" ng-click="cancel()">取消</button>');
        }
        html.push('<button class="btn btn-info" type="button" ng-click="ok()">确定</button>');
        html.push('</div>');
        return html.join('');
    }

    function DialogController($scope, $modalInstance) {
        $scope.ok = function () {
            $modalInstance.close('cancel');
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        }
    }

    function dbAlert(content, okFun,size) {
        $modal.open({
            animation: true,
            template: getHtml(content),
            controller: ['$scope', '$modalInstance', DialogController],
            size: size? size : "sm",
            backdrop: "static"
        }).result.then(function () {
            if (angular.isFunction(okFun)) {
                okFun.call();
            }
        });
    }

    function openModal(html, controller,size,windowClass,sourceObj,okFun,backdrop) {
        $modal.open({
            animation: true,
            templateUrl: html,
            controller: controller,
            size: size? size : "sm",
            windowClass: windowClass?windowClass:"",
            // backdrop: !backdrop?"static":backdrop,//static:点按钮才关闭,true:空白关闭
            backdrop: "static",//static:点按钮才关闭,true:空白关闭
            resolve: {
                source: function () {
                    return sourceObj;
                }
            }
        }).result.then(function (data) {
            if (angular.isFunction(okFun)) {
                okFun.call(this,data);
            }
        });
    }

    function showImg(src){
        var html = [];
        html.push('<div class="modal-body">');
        html.push('<i ng-click="ok()" class="icon-close" style="margin:-15px -15px auto auto;margin-bottom:5px;font-size:30px;color:#131e25;float:right;"></i>');
        html.push('<img class="img-rounded img-responsize" src="'+src+'" style="width:100%" />');
        html.push('</div>');

        $modal.open({
            animation: true,
            template: html.join(""),
            controller: ['$scope', '$modalInstance', DialogController]
        })
    }

    function doTip(content) {
        var html = [];
        html.push('<div class="modal-header">');
//        html.push('<h3 class="modal-title">提示</h3>');
        html.push('<span class="modal-title lg-font">提示</span>');
        html.push('<div class="modal-body">');
        html.push(content);
        html.push('</div>');
        html.push('</div>');
        html.push('<div class="modal-footer">');
        html.push('</div>');

        $modal.open({
            animation: true,
            template: html.join(""),
            controller: ['$scope', '$modalInstance', DialogController],
            size: "sm"
        })
    }

    function doToasterTip(type, title, content) {
    	toaster.clear();
    	/*toaster.pop({
            type: type,
            title: title,
            body: content,
            timeout: 5000,
            bodyOutputType: 'trustedHtml'
        });*/
    	toaster.pop({
            type: type,
            body: 'bind-unsafe-html',
            timeout: 5000,
            bodyOutputType: 'directive',
            directiveData: { type: type, content: content }
        });
    }
    
    function doToasterTiping(type, title, content,timeout) {
        toaster.pop({
            type: type,
            title: title,
            body: content,
            timeout: 20000,
            bodyOutputType: 'trustedHtml'
        });
    }

    //触发按键事件	fireKeyEvent(document.getElementsByTagName("body"), 'keydown', 27);  
	function fireKeyEvent(el, evtType, keyCode){  
    	el = el[0];
        var doc = el.ownerDocument,  
            win = doc.defaultView || doc.parentWindow,  
            evtObj;  
        if(doc.createEvent){  
            if(win.KeyEvent) {  
                evtObj = doc.createEvent('KeyEvents');  
                evtObj.initKeyEvent( evtType, true, true, win, false, false, false, false, keyCode, 0 );  
            }  
            else {  
                evtObj = doc.createEvent('UIEvents');  
                Object.defineProperty(evtObj, 'keyCode', {  
                    get : function() { return this.keyCodeVal; }  
                });       
                Object.defineProperty(evtObj, 'which', {  
                    get : function() { return this.keyCodeVal; }  
                });  
                evtObj.initUIEvent( evtType, true, true, win, 1 );  
                evtObj.keyCodeVal = keyCode;  
            }  
            el.dispatchEvent(evtObj);  
        }   
        else if(doc.createEventObject){  
            evtObj = doc.createEventObject();  
            evtObj.keyCode = keyCode;  
            el.fireEvent('on' + evtType, evtObj);  
        }  
    }
    
    //重新登陆的处理
    function reLoginHandle(){
    	fireKeyEvent(document.getElementsByTagName("body"), 'keydown', 27);
    }
    
    //Invalid remember-me token (Series/token) mismatch. Implies previous cookie theft attack.
    function exceptionHandle(data){
    	console.log(data);
    	let exceptionInfo = "org.springframework.security.web.authentication.rememberme.CookieTheftException";
    	let messageInfo = "Invalid remember-me token (Series/token) mismatch. Implies previous cookie theft attack.";
    	if(data && data['status'] == 500 && data['exception'] == exceptionInfo && data['message'] == messageInfo){
    		 $state.go("access.signin");
             reLoginHandle();
             return;
    	}
    }
    
    let data = {
    	S4: function () {
            return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        },
    	guid: function() {
            return (this.S4()+this.S4()+"-"+this.S4()+"-"+this.S4()+"-"+this.S4()+"-"+this.S4()+this.S4()+this.S4());
        },
        success: function (content, title) { 
            doToasterTip('success', title, content);
        },
        info: function (content, title) {
            doToasterTip('info', title, content);
        },
        warning: function (content, title) {
            doToasterTip('warning', title, content);
        },
        error: function (content, title) {
            doToasterTip('error', title, content);
        },
        erroring: function (content, title,timeout) {
        	doToasterTiping('error', title, content,timeout);
        },
        successing: function (content, title,timeout) {
        	doToasterTiping('success', title, content,timeout);
        },
        alert: function (content, okFun) {
            dbAlert(content, okFun);
        },
        /**
         * 弹出框
         * @param html:传html路径地址
         * @param controller:controller名或者function
         * @param size：sm,md,lg,full,all
         * @param windowClass:"modal-large","modal-larger"
         * @param sourceObj:对象
         * @param okFun:回调函数
         * @param backdrop:boolean类型，true在弹出框外面点击会消失;false反之
         */
        openModal: function(html, controller,size,windowClass,sourceObj,okFun,backdrop){
        	//dbUtils.openModal('tpl/logtable/show_logs.html','ScriptDetailController',"lg","",{"data":row},function (data) {},true);
        	openModal(html, controller,size,windowClass,sourceObj,okFun,backdrop);
        },
        tip: function (content) {
            doToasterTip(content);
        },
        alertInfo: function(content){
            doTip(content);
        },
        confirm: function (content, okFun, cancerFun,backdrop) {
            $modal.open({
                animation: true,
                template: getHtml(content, 'confirm'),
                controller: ['$scope', '$modalInstance', DialogController],
                size: "sm",
                backdrop: !backdrop?"static":backdrop
            }).result.then(function () {
                okFun.call();
            }, function () {
                if (angular.isFunction(cancerFun)) {
                    cancerFun.call();
                }
            });
        },
        confirmlg: function (content, okFun, cancerFun) {
            $modal.open({
                animation: true,
                template: getHtml(content, 'confirm'),
                controller: ['$scope', '$modalInstance', DialogController],
                size: "lg",
                backdrop: "static"
            }).result.then(function () {
                okFun.call();
            }, function () {
                if (angular.isFunction(cancerFun)) {
                    cancerFun.call();
                }
            });
        },
        showImg: function(src){
            showImg(src);
        },
        post: function (transCode, data) {
            var deferred = $q.defer();
            $http({
                method: "POST",
                url: transCode,
                data: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded','Accept': 'application/json;charset=utf-8'},
                transformRequest: function (obj) {
                    var str = [];
                    for (var p in obj) {
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    }
                    return str.join("&");
                }
            }).then(
                function (data, status) {
                    deferred.resolve(data.data);
                },
                function (data, status) {
                    if(data['status'] == 401){
                        $state.go("access.signin");
                        reLoginHandle();
                        return;
                    }
                    exceptionHandle(data);
                    doToasterTip('warning', "warning", (data.data && data.data.message) ? data.data.message : "网络通讯异常，请稍后再试！");
                    deferred.reject(data.data);
                }
            );
            return deferred.promise;
        },
        put: function (transCode, data) {
            var deferred = $q.defer();
            $http({
                method: "PUT",
                url: transCode,
                data: data,
//                headers: {'Content-Type': 'application/x-www-form-urlencoded','Accept': 'application/json;charset=utf-8'},
                headers: {'Content-Type': 'application/json;charset=utf-8','Accept': 'application/json;charset=utf-8'}   
            }).then(
                function (data, status) {
                    deferred.resolve(data.data);
                },
                function (data, status) {
                    if(data['status'] == 401){
                        $state.go("access.signin");
                        reLoginHandle();
                        return;
                    }
                    exceptionHandle(data);
                    doToasterTip('warning', "warning", (data.data && data.data.message) ? data.data.message : "网络通讯异常，请稍后再试！");
                    deferred.reject(data.data);
                }
            );
            return deferred.promise;
        },
        put1: function (transCode, data) {
            var deferred = $q.defer();
            // var apiUrl = "http://hp.xiqing.info/mobile/api.do";
            // var apiUrl = "http://localhost:8080/mobile/api.do";
            $http({
                method: "PUT",
                url: transCode,
                data: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded','Accept': 'application/json;charset=utf-8'},
                transformRequest: function (obj) {
                    var str = [];
                    for (var p in obj) {
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    }
                    return str.join("&");
                }
            }).then(
                function (data, status) {
                    deferred.resolve(data.data);
                },
                function (data, status) {
                    if(data['status'] == 401){
                        $state.go("access.signin");
                        reLoginHandle();
                        return;
                    }
                    exceptionHandle(data);
                    doToasterTip('warning', "warning", (data.data && data.data.message) ? data.data.message : "网络通讯异常，请稍后再试！");
                    deferred.reject(data.data);
                }
            );
            return deferred.promise;
        },
        delete: function (transCode, data) {  
            var deferred = $q.defer();
            if (data) {
                var str = [];
                for (var p in data) {
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(data[p]));
                }
                transCode = transCode + "?" + str.join("&");
            }
            $http({
                method: "DELETE",
                url: transCode,
                data: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded','Accept': 'application/json;charset=utf-8'},
//                headers: {'Content-Type': 'application/json','Accept': 'application/x-www-form-urlencoded'}
            }).then(
                function (data, status) {
                    deferred.resolve(data.data);
                },
                function (data, status) {
                    if(data['status'] == 401){
                        $state.go("access.signin");
                        reLoginHandle();
                        return;
                    }
                    exceptionHandle(data);
                    doToasterTip('warning', "warning", (data.data && data.data.message) ? data.data.message : "网络通讯异常，请稍后再试！");
                    deferred.reject(data.data);
                }
            );
            return deferred.promise;
        },
        postPicture: function(transCode, data){
        	var deferred = $q.defer();
            $http({
                method: "POST",
                url: transCode,
                data: data,
                headers:{'Content-Type':undefined},
                transformRequest:angular.identity//浏览器会帮我们把Content-Type 设置为 multipart/form-data.
            }).then(
                function (data, status) {
                    deferred.resolve(data.data);
                },
                function (data, status) {
                    if(data['status'] == 401){
                        $state.go("access.signin");
                        reLoginHandle();
                        return;
                    }
                    exceptionHandle(data);
                    doToasterTip('warning', "warning", (data.data && data.data.message) ? data.data.message : "网络通讯异常，请稍后再试！");
                    deferred.reject(data.data);
                }
            );
            return deferred.promise;
        },
        postBody: function (transCode, data) {
            var deferred = $q.defer();
            $http({
                method: "POST",
                url: transCode,
                data: data,
                headers: {'Content-Type': 'application/json','Accept': 'application/json;charset=utf-8'}
            }).then(
                function (data, status) {
                    deferred.resolve(data.data);
                },
                function (data, status) {
                    if(data['status'] == 401){
                        $state.go("access.signin");
                        reLoginHandle();
                        return;
                    }
                    exceptionHandle(data);
                    doToasterTip('warning', "warning", (data.data && data.data.message) ? data.data.message : "网络通讯异常，请稍后再试！");
                    deferred.reject(data.data);
                }
            );
            return deferred.promise;

        },
        putBody: function (transCode, reqBody) {
            var deferred = $q.defer();
            var ApiRequest = {};
            ApiRequest["transCode"] = transCode;
            ApiRequest["requestBody"] = reqBody;

            $http({
                headers: {'Content-Type': 'application/x-www-form-urlencoded','Accept': 'application/json;charset=utf-8'}
            }).post(transCode, ApiRequest).then(
                function (data, status) {
                    deferred.resolve(data.data);
                },
                function (data, status) {
                    if(data['status'] == 401){
                        $state.go("access.signin");
                        reLoginHandle();
                        return;
                    }
                    exceptionHandle(data);
                    doToasterTip('warning', "warning", (data.data && data.data.message) ? data.data.message : "网络通讯异常，请稍后再试！");
                    deferred.reject(data.data);
                }
            );
            return deferred.promise;

        },
        get: function (transCode, data) {
            var deferred = $q.defer();
            if (data) {
            	var str = [];
        		for (var p in data) {
        			str.push(encodeURIComponent(p) + "=" + encodeURIComponent(data[p]));
        		}
            	if(transCode.indexOf("?") == -1){
            		transCode = transCode + "?" + str.join("&");
                }else{
                	transCode = transCode + "&" + str.join("&");
                }
        	}
            $http.get(transCode,{
                headers: {'Content-Type': 'application/x-www-form-urlencoded','Accept': 'application/json;charset=utf-8'}
            }).then(
                function (data, status) {
                    deferred.resolve(data.data);
                },
                function (data, status) {
                    if(data['status'] == 401){
                        $state.go("access.signin");
                        reLoginHandle();
                        return;
                    }
                    exceptionHandle(data);
                    doToasterTip('warning', "warning", (data.data && data.data.message) ? data.data.message : "网络通讯异常，请稍后再试！");
                    deferred.reject(data.data);
                }
            );
            return deferred.promise;
        },
        all: function (funs) {
            return $q.all(funs);
        },
        dateFormat: function (date) {
            var year = date.getFullYear();
            var month = date.getMonth() + 1;
            var date = date.getDate();
            return year + "-" + (month > 9 ? month : ("0" + month)) + "-" + (date > 9 ? date : ("0" + date));
        },

        /*
         * 根据指定变量获取集合中此变量的数组数据返回.
         * @param name
         * @param rows
         * @returns {Array}
         */
        getFieldArray: function (objectList, name) {
            var data = [];
            angular.forEach(objectList, function (record) {
                data.push(record[name]);
            });
            return data;
        },
        toString: function(obj,replace){
            var result = JSON.stringify(obj,null,"\t");
            if(replace){
                result = result.replace( /\t/g,"&nbsp;&nbsp;&nbsp;&nbsp;");
                result = result.replace( /\n/g,"<br/>");
            }
            return result;
        },
        showJson: function(str){
            str = "<pre>"+str+"</pre>"
            dbAlert(str,null,"lg");
        },
        /*ip,端口重复性校验 start*/
        /*
         * 把controller中对端口检验的配置转移到ui-dbUtils.js中，方便管理和使用
         * 当isEdit为修改模式时，需要有oldData
         * currData为当前数据，用于转换成oldData
         */
        startPortCheck:function($scope,isEdit,currData){
        	this.get("/ports/getPorts",null).then(function(response){
                if (response.code == SUCCESS_CODE) {
                	$scope.ipPortList = response.data; 
                }else {
                	dbUtils.error(response.message,"提示") 
                }
          	})
          	$scope.oldData = {};
        	if(isEdit == true)
        		$scope.oldData = this.getOldData($scope.oldData,currData);
        },
        getOldData:function(oldData,currData){
        	var titleArr = ["ip","springbootHost","springbootPort","port","performancePort","hostAddress","adminIp","adminPort","prometPort","serverPort","serverIp","monitorPort","managedServer"];
        	if(!currData){
        		return oldData;
        	}
    		for(let item in currData){
				let index = titleArr.indexOf(item);
				if(index != -1){
					//找到属性了
					var obj = {value:currData[item]};
					//给原始数据赋值
					oldData[item] = obj;
				}
    		}
        	return oldData;
        },
        checkChange:function(oldData,data,propNameList,ipProp){
        	var isChange = false;
        	propNameList.push(ipProp);
        	// for(let propName of propNameList){
        	for(let i=0;i<propNameList.length;i++){
        	    let propName = propNameList[i];
        		if(!oldData[propName] || !oldData[propName].value){
        			oldData[propName] = {isChange: true};
        			isChange = true;
        			continue;
        		}
        		if(oldData[propName].value == data[propName]){
        			oldData[propName].isChange = false;
        			continue;
        		}else{
        			oldData[propName].isChange = true;
        			isChange = true;
        			continue;
        		}
        	}
        	return isChange;
        },
        checkPortDuplicate: function(ip,data,propNameList,ipPortList,oldData,isEdit,ipProp){
        	var obj = {warningText: ""};
        	var isDuplicate = false;
        	//校验端口是否重复
        	if(!ip || ip == ""){
//        		this.warning("请先选择主机","提示");
        		return;
        	}
        	if(!ipPortList || ipPortList.length == 0)
        		return;
        	let ports = ipPortList[ip];//根据ip来获取端口数组
        	if(!ports || ports.length == 0)
        		return;
        	
        	if(isEdit == false){
        		for(let propName of propNameList){
        			isDuplicate = this.checkPort(ports,ip,data[propName],obj,isDuplicate);
    			}
        	}else{
        		//判断端口和Ip属性的值相比编辑前是否发生改变,如果没有改变就无需再校验其重复性
        		var isChange = this.checkChange(oldData,data,propNameList,ipProp);
        		if(isChange == false)
        			return;
        		var ipIsChange = false;//如果ip已经被改变了，那么就必须要校验端口的重复性
    			if(oldData[ipProp] && oldData[ipProp].isChange)
    				ipIsChange = oldData[ipProp].isChange;
    			
        		for(let propName of propNameList){
    				if(ipIsChange == false){
    					if(oldData[propName].isChange == false)
    						continue;
    					else{
    						isDuplicate = this.checkPort(ports,ip,data[propName],obj,isDuplicate);
    					}
    				}else{
    					isDuplicate = this.checkPort(ports,ip,data[propName],obj,isDuplicate);
    				}
    			}
        	}
        	if(isDuplicate == true && obj.warningText != "")
				this.warning(obj.warningText,"提示");
        	return isDuplicate;
        },
        checkPort: function(ports,ip,portValue,obj,isDuplicate){
        	if(ports.indexOf(parseInt(portValue)) != -1){
        		obj.warningText += "<h4>ip: "+ip+"</h4><h4>端口: "+portValue+"</h4>已被占用，请重新输入端口";
				isDuplicate = true;
			}
        	return isDuplicate;
        },
        /*ip,端口重复性校验 end*/
        //转换文件大小单位
        convertFileUnit: function(num){
        	if(!this.isNumber(num))
        		return num;
        	num = parseFloat(num);
        	var size = "";
            if(num < 0.1 * 1024){                            //小于0.1KB，则转化成B
                size = num.toFixed(2) + "B"
            }else if(num < 0.1 * 1024 * 1024){            //小于0.1MB，则转化成KB
                size = (num/1024).toFixed(2) + "KB"
            }else if(num < 0.1 * 1024 * 1024 * 1024){        //小于0.1GB，则转化成MB
                size = (num/(1024 * 1024)).toFixed(2) + "MB"
            }else{                                            //其他转化成GB
                size = (num/(1024 * 1024 * 1024)).toFixed(2) + "GB"
            }

            var sizeStr = size + "";                        //转成字符串
            var index = sizeStr.indexOf(".");                    //获取小数点处的索引
            var dou = sizeStr.substr(index + 1 ,2)            //获取小数点后两位的值
            if(dou == "00"){                                //判断后两位是否为00，如果是则删除00                
                return sizeStr.substring(0, index) + sizeStr.substr(index + 3, 2)
            }
            return size;
        },
        //获取文件大小和单位
        convertFileSizeAndUnit: function(num){
            let unitConstArr = ["GB","MB","KB","B"];
            let data = {};
            let size = this.convertFileUnit(num);
            if(!size || size == "")
                return data;
            for(let i=0;i<unitConstArr.length;i++){
                let index = size.indexOf(unitConstArr[i]);
                if(index != -1){
                    data.value = size.substring(0, index);
                    data.unit = unitConstArr[i];
                    break;
                }
            }
            return data;
        },
        //转换数字单位
        convertNumUnit: function(num){
            let data = {};
        	if(!this.isNumber(num))
        		return num;
        	num = parseFloat(num);
        	var size = "";
            if(num < 10000){
                size = num;
                data.unit = "";
            }else if(num < 10000 * 10000){
                size = (num/10000).toFixed(2);
                data.unit = "万";
            }else{                                            //其他转化成GB
                size = (num/(10000 * 10000)).toFixed(2);
                data.unit = "亿";
            }

            var sizeStr = size + "";                        //转成字符串
            var index = sizeStr.indexOf(".");                    //获取小数点处的索引
            var dou = sizeStr.substr(index + 1 ,2)            //获取小数点后两位的值
            if(dou == "00"){                                //判断后两位是否为00，如果是则删除00
                return sizeStr.substring(0, index) + sizeStr.substr(index + 3, 2)
            }
            data.value = size;
            return data;
        },
        //验证字符串是否是数字类型
        isNumber: function(val) {
            var regPos = /^\d+(\.\d+)?$/; //非负浮点数
            var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
            if(regPos.test(val) || regNeg.test(val)) {
                return true;
            } else {
            	return false;
            }
        },
        //验证是否是日期格式
        isDate: function(date){
        	var a = /^(([1-3][0-9]{3})[-]{0,1}(((0[13578]|1[02])[-]{0,1}(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)[-]{0,1}(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8])))\s\d{1,2}:\d{1,2}:\d{1,2})|(([1-3][0-9]{3})[-]{0,1}(((0[13578]|1[02])[-]{0,1}(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8])))\s\d{1,2}:\d{1,2})|(([1-3][0-9]{3})[-]{0,1}(((0[13578]|1[02])[-]{0,1}(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)[-]{0,1}(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))$/;
        	if (!a.test(date)) 
        		return false 
        	else 
        		return true 
    	},
    	getClientStatusInfo: function(rows,fun,scope){
    		this.get("client/listAgent").then(function(response){
            	if(response.code == SUCCESS_CODE){
                    scope.clients = response.data;
            		let clients = [];
            		let obj = {};
            		for(let row of rows){
            			if(obj[row.deployFileId]){
                            row.client = obj[row.deployFileId];
                            continue;
                        }
            			else{
            				response.data.forEach(item => {
            					if(parseInt(row.deployFileId) == item.id){
                                    obj[row.deployFileId] = item;
                                    row.client = item;
                                    clients.push(item);
                                }
            				})
            			}
            		}
            		data.postBody("client/health",JSON.stringify(clients)).then(function(resp){
            			if(resp.code == SUCCESS_CODE){
            				if(typeof fun === "function"){
            					fun.call(this,resp);
            				}
            			}else{
                    		dbUtils.error(resp.message)
                    	}
            		})
            	}else{
            		dbUtils.error(response.message)
            	}
            });
    	},
    	//主机状态校验
    	checkClientStatus: function(check_value_array,fun){
    		let status = true;
    		if(check_value_array.length > 0){
    			for(let check_value of check_value_array){
    				if(check_value == 0){
    					status = false;
    					break;
    				}
    			}
    			if(status == false){
    				this.confirm("该agent主机状态为<span style='color:red;'>DOWN</span><br>继续执行可能会出错<br>建议去主机管理中重启该主机<br>是否继续执行?",function(){
    					fun.call();
    				},function(){
    					return;
    				},true);
    			}else{
    				fun.call();
    			}
    		}else{
    			fun.call();
    		}
    	},
    	//触发按键事件	fireKeyEvent(document.getElementsByTagName("body"), 'keydown', 27);  
    	fireKeyEvent: function(el, evtType, keyCode){
    		fireKeyEvent(el, evtType, keyCode);
    	},
    	//初始化分页参数
    	initPaginationParams: function($scope){
    		$scope.pageSelect = [5,10,15,20,50,100];
    	    $scope.pageSize = 10;
    	},
        //获取一个不向后台请求的promise对象
        getPromise: function (data) {
            let deferred = $q.defer();
            deferred.resolve({code:1000000,data:data});
            deferred.reject({code:1,message:"error"});
            return deferred.promise;
        },
        //省略化长文本
        shortenText: function (text, preLen, sufLen, replaceSymbol) {
            let textPre, textSuf;
            if(!replaceSymbol)
                replaceSymbol = "...";
            if(preLen == 0 || sufLen == 0)
                return text;
            if(text.length <= (preLen + sufLen)){
                return text;
            }else{
                textPre = text.slice(0, preLen);
                textSuf = text.slice(-sufLen);
                return textPre + replaceSymbol + textSuf;
            }
        },
    }
    return data;
}
