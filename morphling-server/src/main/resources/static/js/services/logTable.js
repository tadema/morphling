'use strict';
angular.module("app").service('LogTableService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    let result = {
        deployLogTable1:function(queryParams){
            return dbUtils.get("/logs/getDeployLogs",queryParams)
        },
        deployLogTable2:function(appId,queryParams){
        	var data = JSON.stringify(queryParams);
            return dbUtils.get("/logs/getStartingLog",{appId:appId,info:data})
        },
        deployLogTable3:function(appId,queryParams){
        	var data = JSON.stringify(queryParams);
            return dbUtils.get("/logs/getMiddlewareStartingLog",{appId:appId,info:data})
        },
        getCustomAppRunningLog:function(appId,path){
            return dbUtils.get("/logs/getCustomAppRunningLog",{appId:appId,path:path})
        },
        downloadBackup:function(logData){
            return dbUtils.get("/logs/scpCopyFile",logData)
        },
        downloadFileCros:function(ip, port, filePath){
            return dbUtils.post("/logs/downloadFileCros",{ip:ip, port:port, filePath:filePath})
        },
        queryDetail1:function(data){
            return dbUtils.get("/logs/searchDeployLogFromDataBase",data)
        },
        //弃用，改成下面的方法
        queryDetail2:function(data){
            return dbUtils.get("/logs/searchLogByCommand",data)
        },
        searchLogByCommand:function(data){
            return dbUtils.get("/logs/searchLogByCommand",data)
        },
        batchDeleteLogs:function(data){
            return dbUtils.get("/logs/batchDeleteLogs",data)
        },
        batchDeleteOperateLog:function(data){
            return dbUtils.get("/logs/batchDeleteOperateLog",data)
        },
        splitLogFile:function(data){
            return dbUtils.get("/logs/splitLogFile",data)
        },
        tailStartedLog:function(client,logPath,lineNum){
            return dbUtils.postBody("/logs/tailStartedLog?lineNum="+lineNum+"&logPath="+logPath,client)
        },
        deployLog:function(appId,start){
            return dbUtils.get("/logs/deploy",{appId:appId,start:start})
        },
        //初始化部署日志表格参数
        initDeployTable:function($scope,loadingLogsTmpl){
            $scope.deployTableParams = {loading:true,bodyStyle:{"max-height": "500px"}};
            $scope.deployPageParams = {isPagination:true,postgroundPagination:true,pageNumber: 1,pageSize:10,prevPageDisabled: 'disabled',nextPageDisabled: 'disabled'};
            $scope.deployHeaders = [
                {name:"主机地址",field:"clientIp",style:{"width":"25%"}},
                {name:"操作类型",field:"typeName",style:{"width":"10%"}},
                // {name:"日志类型",field:"type",style:{"width":"10%"},compile:true,formatter:function(value,row){
                //         $scope.tooltipData.deployType[row.id] = value;
                //         return "<span>{{{4:'打包',5:'部署',6:'管理'}[tooltipData.deployType["+row.id+"]]}}</span>";
                //     }},
                {name:"操作用户",field:"createUsername",style:{"width":"10%"}},
                {name:"操作时间",field:"createTime",style:{"width":"20%"}},
                {name:"操作实例",field:"serviceName",style:{"width":"10%"},compile:true,formatter:function(value,row){
                        $scope.tooltipData.deployInfo[row.id] = value;
                        return "<span class='label bg-light cusor-pointer' data-toggle='tooltip' tooltip-placement='auto left' tooltip-trigger='outsideClick' uib-tooltip-html='tooltipData.deployInfo["+row.id+"]'>查看详情</span>";
                    }}
            ];
            $scope.deployRowEvents = [{
                class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"查看详情",name: "查看",
                click: function(row){
                    $scope.showLogs1 && $scope.showLogs1(row);
                }
            },{
                class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"关键字搜索",name: "搜索",
                click: function(row){
                    $scope.searchLog && $scope.searchLog(row);
                }
            }];
            $scope.deployOperationEvents = [{
                class:"btn-info",icon:"fa fa-mail-reply",name:"清除日志",
                click:function(rows){
                    if(rows && rows.length == 0){
                        dbUtils.info("请选择一条数据","提示");
                        return;
                    }
                    dbUtils.confirm("确认是否删除所选日志",function(){
                        var logIds = [];
                        for(var i in rows){
                            logIds.push(rows[i].logId);
                        }
                        $scope.deployTableParams.loading = true;
                        result.batchDeleteOperateLog({ids:logIds}).then(function(response){
                            $scope.deployTableParams.loading = false;
                            if(response.code == SUCCESS_CODE){
                                dbUtils.info("删除成功","提示");
                                loadingLogsTmpl && loadingLogsTmpl(1);
                            }else{
                                dbUtils.error(response.message,"提示");
                            }
                        },function(result){$scope.deployTableParams.loading = false;});
                    },function(){},true);
                }
            }];
        },
        //初始化启动备份日志表格参数
        initStartBakTable:function($scope,loadingLogsTmpl){
            $scope.startBakTableParams = {loading:true,bodyStyle:{"max-height": "500px"}};
            $scope.startBakPageParams = {isPagination:true,postgroundPagination:false,pageNumber: 1,pageSize:10,prevPageDisabled: 'disabled',nextPageDisabled: 'disabled'};
            $scope.startBakHeaders = [
                {name:"文件名",field:"fileName",style:{"width":"20%"}},
                {name:"主机地址",field:"clientIp",style:{"width":"15%"}},
                {name:"文件大小",field:"fileSize",style:{"width":"10%"},compile:true,formatter:function(value,row){
                    row.fileSizes = dbUtils.convertFileUnit(value);
                    return row.fileSizes;
                }},
                {name:"时间",field:"time",style:{"width":"15%"}},
                {name:"文件地址",field:"filePath",style:{"width":"8%"},compile:true,formatter:function(value,row){
                    $scope.tooltipData.startBakFilePath[row.id] = value;
                    return "<span class='label bg-light cusor-pointer' data-toggle='tooltip' tooltip-placement='auto left' tooltip-trigger='outsideClick' uib-tooltip-html='tooltipData.startBakFilePath["+row.id+"]'>查看详情</span>";
                }}
            ];
            $scope.startBakRowEvents = [{
                class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"查看详情",name: "查看",
                click: function(row){
                    $scope.showLogs2 && $scope.showLogs2(row,1,'');
                }
            },{
                class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"下载文件",name: "下载",
                click: function(row){
                    $scope.downLogs && $scope.downLogs(row);
                }
            },{
                class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"关键字搜索",name: "搜索",
                click: function(row){
                    $scope.searchLog && $scope.searchLog(row);
                }
            },{
                class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"实时日志",name: "实时日志",
                click: function(row){
                    $scope.runningLog && $scope.runningLog(row);
                }
            }];
            $scope.startBakOperationEvents = [{
                class:"btn-info",icon:"fa fa-mail-reply",name:"清除日志",
                click:function(rows){
                    if(rows && rows.length == 0){
                        dbUtils.info("请选择一条数据","提示");
                        return;
                    }
                    dbUtils.confirm("确认是否删除所选日志",function(){
                        var info = [];
                        for(var i in rows){
                            info.push({filePath:rows[i].filePath, clientIp:rows[i].clientIp, clientPort:rows[i].clientPort, clientId:rows[i].clientId});
                        }
                        result.batchDeleteLogs({info:JSON.stringify(info)}).then(function(response){
                            if(response.code == SUCCESS_CODE){
                                dbUtils.info("删除成功","提示");
                                loadingLogsTmpl && loadingLogsTmpl(1);
                            }else{
                                dbUtils.error(response.message,"提示");
                            }
                        },function(result){});
                    },function(){},true);
                }
            },{
                class:"btn-info",icon:"fa fa-scissors",name:"拆分",
                click:function(rows){
                    $scope.splitLog && $scope.splitLog(rows);
                }
            }];
        },
        //初始化运行日志表格参数
        initRunningTable:function($scope,loadingLogsTmpl){
            $scope.runningTableParams = {loading:true,bodyStyle:{"max-height": "500px"}};
            $scope.runningPageParams = {isPagination:true,postgroundPagination:false,pageNumber: 1,pageSize:10,prevPageDisabled: 'disabled',nextPageDisabled: 'disabled'};
            $scope.runningHeaders = [
                {name:"文件名",field:"fileName",style:{"width":"20%"}},
                {name:"主机地址",field:"clientIp",style:{"width":"15%"}},
                {name:"文件大小",field:"fileSize",style:{"width":"10%"},compile:true,formatter:function(value,row){
                    row.fileSizes = dbUtils.convertFileUnit(value);
                    return row.fileSizes;
                }},
                {name:"时间",field:"time",style:{"width":"15%"}},
                {name:"文件地址",field:"filePath",style:{"width":"8%"},compile:true,formatter:function(value,row){
                    $scope.tooltipData.runningFilePath[row.id] = value;
                    return "<span class='label bg-light cusor-pointer' data-toggle='tooltip' tooltip-placement='auto left' tooltip-trigger='outsideClick' uib-tooltip-html='tooltipData.runningFilePath["+row.id+"]'>查看详情</span>";
                }}
            ];
            $scope.runningRowEvents = [{
                class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"查看详情",name: "查看",
                click: function(row){
                    $scope.showLogs2 && $scope.showLogs2(row,1,'');
                }
            },{
                class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"下载文件",name: "下载",
                click: function(row){
                    $scope.downLogs && $scope.downLogs(row);
                }
            },{
                class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"关键字搜索",name: "搜索",
                click: function(row){
                    $scope.searchLog && $scope.searchLog(row);
                }
            },{
                class:"btn-info",icon:"glyphicon glyphicon-plus visible-folded",title:"实时日志",name: "实时日志",
                click: function(row){
                    $scope.runningLog && $scope.runningLog(row);
                }
            }];
            $scope.runningOperationEvents = [{
                class:"btn-info",icon:"fa fa-mail-reply",name:"清除日志",
                click:function(rows){
                    if(rows && rows.length == 0){
                        dbUtils.info("请选择一条数据","提示");
                        return;
                    }
                    dbUtils.confirm("确认是否删除所选日志",function(){
                        var info = [];
                        for(var i in rows){
                            info.push({filePath:rows[i].filePath, clientIp:rows[i].clientIp, clientPort:rows[i].clientPort, clientId:rows[i].clientId});
                        }
                        result.batchDeleteLogs({info:JSON.stringify(info)}).then(function(response){
                            if(response.code == SUCCESS_CODE){
                                dbUtils.info("删除成功","提示");
                                loadingLogsTmpl && loadingLogsTmpl(1);
                            }else{
                                dbUtils.error(response.message,"提示");
                            }
                        },function(result){});
                    },function(){},true);
                }
            },{
                class:"btn-info",icon:"fa fa-scissors",name:"拆分",
                click:function(rows){
                    $scope.splitLog && $scope.splitLog(rows);
                }
            }];
        },
    }
    return result;
}]);

angular.module("app").service('SystemLogService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    return {
    	pageNumberClick: function($scope,pageNumber){
        	var currentPage = parseInt($scope.page.currentPage);
        	var prevPage = $scope.page.prevPageDisabled;
        	var nextPage = $scope.page.nextPageDisabled;
            if (pageNumber === "prev" && prevPage && prevPage != "")
            	return false;

            if (pageNumber === "next" && nextPage && nextPage != "")
            	return false;

            if (pageNumber == currentPage || pageNumber === "...")
            	return false;

            if (pageNumber === "prev") {
            	currentPage--;
            } else if (pageNumber === "next") {
            	currentPage++;
            } else {
            	currentPage = pageNumber;
            }
            return currentPage;
        },
        pageSet: function($scope,data){
        	//分页数据处理
        	var pages = {};
            pages.currentPage = parseInt(data.currentPage);
            pages.totalPage = parseInt(data.totalPage);
            var totalPage = pages.totalPage;
            //分页算法，页面只显示固定数量的分页按钮。
            var pageNumbers = [];
            var startPage = 1;
            var endPage = totalPage;
            var pageStep = 2;//以当前页为基准，前后各显示的页数量
            if (totalPage >= 6) {
                startPage = pages.currentPage;
                if (startPage >= pageStep)
               	 startPage -= pageStep;

                if (startPage <= 1)
               	 startPage = 1;

                endPage = (totalPage - pages.currentPage) >= pageStep ? pages.currentPage + pageStep : totalPage;
                if (endPage > totalPage)
               	 endPage = totalPage;
                if (startPage != 1) {
                    pageNumbers.push({number: "1"});
                    if (startPage - 1 != 1) {
                        pageNumbers.push({number: "...", disabled: "disabled"});
                    }
                }
            }

            for (var i = startPage; i <= endPage; i++) {
                if (i == pages.currentPage) {
                    pageNumbers.push({number: i, active: "active"});
                } else {
               	 pageNumbers.push({number: i});
                }
            }
            if (endPage != totalPage) {
                if (endPage + 1 != totalPage) {
                    pageNumbers.push({number: "...", disabled: "disabled"});
                }
                pageNumbers.push({number: totalPage});
            }
            pages.currentPages = pageNumbers;
            if (pages.currentPage == 1 || pages.totalPage == 0) {
                pages.prevPageDisabled = "disabled";
            }
            if (pages.currentPage == totalPage || pages.totalPage == 0) {
                pages.nextPageDisabled = "disabled";
            }
            angular.extend($scope.page,pages);
        },
    }
}]);