TabManager = {
    SELECTEDINDEX: 0,
    MAXTABSIZE: 20,
    TABS: [],
    addTab: function (path, title, icon, hash, params, theme) {
        if (!title || !path)return;
        var paramKey = params ? JSON.stringify(params) : "";
        
        var key = $.md5(path + title );
        var keyParam = $.md5(path + title + paramKey);
        
        var hasTab = this.hasTab(key);
        var hasKeyParam = this.hasKeyParam(keyParam);
        if (hasTab == -1 && hasKeyParam == -1) {
            var size = this.TABS.length;
            if (size > this.MAXTABSIZE) {
                return -9;
            }
            this.TABS.push({key: key, keyParam: keyParam, title: title, path: path, hash: hash, icon: icon,params: params, theme: theme});
            this.SELECTEDINDEX = this.TABS.length-1;
        } 
        else if (hasTab != -1 && hasTab != this.SELECTEDINDEX && hasKeyParam!=-1) {
            this.SELECTEDINDEX = hasTab;
            return hasTab;
        }
        else if(hasTab != -1 && hasKeyParam == -1){
        	let tab = this.findTabByKey(key);
        	if(tab.path != "app.home"){
        		var index = this.TABS.indexOf(tab);
//    			this.TABS.splice(index, 1);
        	}
        	//应用发布页，删除tab页再添加，加载的js和html并没有重新加载
        	let routeArray = ['app.deploy','app.customDeploy','app.logtable'];
    		if(routeArray.indexOf(hash) != -1){
//    			this.TABS.push({key: key, keyParam: keyParam, title: title, path: path, hash: hash, icon: icon,params: params, theme: theme});
    		}
        }
    },
    hasTab: function (key) {
        var foundIdx = -1;
        for (var i = 0; i < this.TABS.length; i++) {
            if (this.TABS[i].key == key) {
                foundIdx = i;
                break;
            }
        }
        return foundIdx;
    },
    hasKeyParam: function (key) {
    	var foundIdx = -1;
    	for (var i = 0; i < this.TABS.length; i++) {
    		if (this.TABS[i].keyParam == key) {
    			foundIdx = i;
    			break;
    		}
    	}
    	return foundIdx;
    },
    findTabByKey: function(key){
    	for (var i = 0; i < this.TABS.length; i++) {
    		if (this.TABS[i].key == key) 
    			return this.TABS[i];
    	}
    },
    findTabByKeyAndParam: function(key,keyParam){
    	for (var i = 0; i < this.TABS.length; i++) {
    		if (this.TABS[i].key == key && this.TABS[i].keyParam != keyParam) 
    			return this.TABS[i];
    	}
    },
    findTabByRole: function(role,roleId){
    	let tabs = [];
    	for (var i = 0; i < this.TABS.length; i++) {
    		if (this.TABS[i].params.role == role && this.TABS[i].params.roleId == roleId) 
    			return this.TABS[i];
    	}
//    	return tabs;
    },
    removeOnly: function (tab) {
    	if(tab.hash != "app.home"){
    		var index = this.TABS.indexOf(tab);
    		this.TABS.splice(index, 1);
    	}
    },
    removeTab: function (tab) {
    	if(tab.hash != "app.home"){
    		var index = this.TABS.indexOf(tab);
    		this.TABS.splice(index, 1);
    		this.SELECTEDINDEX = this.TABS.length - 1; 
    		let lastTab = this.TABS[this.SELECTEDINDEX];
    		var path = lastTab.hash.replace(".","/");
            window.location.href = "#/"+path;
    	}
    },
    removeOtherTab: function (tab) {
    	//删除其他时，保留首页
    	for(let i=0;i<this.TABS.length;i++){
    		let item = this.TABS[i];
    		if(item.hash != "app.home" && item.hash != tab.hash){
    			this.TABS.splice(i,1);
    			i--;
    		}
    	}
        var path = tab.hash.replace(".","/");
        window.location.href = "#/"+path;
    },
    removeAllTab: function () {
        this.TABS = [];
        window.location.href = "#/app/home";
    }
};

