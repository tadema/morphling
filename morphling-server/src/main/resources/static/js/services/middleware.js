'use strict';

angular.module("app").service('NginxService', ['dbUtils', '$http', 'EchoLog','$q', function (dbUtils, $http, echoLog,$q) {
    var nginx = {
    	//主机
        getClient:function(query){
            return dbUtils.get("/client/listByFilter",query);
        },
        saveNginx:function(data){
            return dbUtils.postBody("/nginx/save",data);
        },
        deleteNginx:function(id){
            return dbUtils.post("/nginx/delete?id="+id,null);
        },
        getFileItems:function(clientId,filePath){
            return dbUtils.post("/nginx/getFileItems",{id:clientId,path:filePath});
        },
        getClientById:function(id){
        	return dbUtils.get("/nginx/getClient",{id:id});
        },
        getFileinfo:function(client,filePath){
        	return dbUtils.postBody("/nginx/getFileinfo?file="+filePath,JSON.stringify(client));
        },
        downloadFile:function(client,filePath){
        	return dbUtils.postBody("/nginx/downloadFile?file="+filePath,JSON.stringify(client));
        },
        deployNginx:function(client,filePath,action){
        	return dbUtils.postBody("/nginx/deployNginx?path="+filePath+"&action="+action,JSON.stringify(client));
        },
        //nginx start stop reload main-page
        nginxOperationHandle1:function(scope,rows,action){
    		var row = rows[0];
    		scope.loading = true;
    		nginx.getClientById(row.clientId).then(function(response){
    			if(response.code == SUCCESS_CODE){
    				nginx.deployNginx(response.data,row.installPath,action).then(function(response){
    		    		if(response.code == SUCCESS_CODE){
    		    			scope.loading = false;
    		    			echoLog.show(response.data,function(){
    		                });
    		    		}else{
    		    			dbUtils.error(response.message,"提示");
    		    		}
    		    	})
    			}else{
    				dbUtils.error(response.message,"提示");
    			}
    		})
    	},
    	//nginx start stop reload child-page
    	nginxOperationHandle2: function(scope,action){
    		scope.loading = true;
    		nginx.deployNginx(scope.client,scope.data.installPath,action).then(function(response){
        		if(response.code == SUCCESS_CODE){
        			scope.loading = false;
        			echoLog.show(response.data,function(){
                    });
        		}else{
        			dbUtils.error(response.message,"提示");
        		}
        	})
    	},
    	queryNginxStatus: function(clientId,path){
    		// return dbUtils.post("/nginx/getNginxStatus",{clientId:clientId});
    		return dbUtils.post("/nginx/getNginxStatus",{clientId:clientId,path:path});
    	},
    	getPorts:function(){
            return dbUtils.get("/ports/getPorts",null);
        },
        getPortsByIp:function(ip){
            return dbUtils.get("/ports/getPortsByIp",{"ip":ip});
        },
    }
    return nginx;
}]);