var driver = angular.module("driver", []);
driver.factory("driver", ["$http", "$window", "$state","$rootScope",'$interval', 'dbUtils', function ($http, $window, $state, $rootScope,$interval,dbUtils){

    let driver,
        isRunnnig = false,
        watingClickMenu = false,//等待点击菜单
        stateIndex = 0,
        stateArr = [],
        isShowCover = false,
        time = 500,//ms,等待definedSteps关闭后再执行showNextMenu
        subjectName,
        isPreventOnReset = false,//完成和关闭禁用OnReset方法
        isWatingClickView = false;//当需要等待用户点击页面时，赋true;否则为false。若点击了页面，stateIndex加一，没点击，无操作
    getSettingData();

    function getSettingData(fun) {
        if(!$rootScope.guideSetting){
            dbUtils.get("driver.json",{}).then(function (response) {
                $rootScope.guideSetting = response;
                fun && fun();
            },function (respone){console.log(respone)})
        }
    }

    //初始化数据并执行跳转
    function initDriverAndData(subjectName){
        if(!driver){
            let settings = $rootScope.guideSetting.settings;
            // reset、结束、esc、鼠标遮罩层点击事件
            settings.onReset = Element => {
                //结束事件的执行顺序：1.节点的onNext；2.本事件onReset
                /*if(!watingClickMenu && !isPreventOnReset){//排除  main.js中menu等待pageLoadSuccess中触发的reset(true)事件
                    stateIndex = stateIndex > 0 ? --stateIndex : 0;//stateIndex减一，下次执行时，还从当前开始
                }*/
            }
            /*settings.onHighlightStarted = Element => {
                console.log('onHighlightStarted()被调用了');
            }
            settings.onHighlighted = Element => {
                console.log('onHighlighted()被调用了');
            }
            settings.onDeselected = Element => { // 取消选择时被调用
                console.log('onDeselected()被调用了');
            }
            settings.onReset = Element => { // 关闭
                console.log('onReset()被调用了');
            }
            settings.onNext = Element => {
                console.log('onNext()被调用了');
            }
            settings.onPrevious = Element => {
                console.log('onPrevious()被调用了');
            }*/
            driver = new Driver(settings);
        }
        
        this.subjectName = subjectName;
        isRunnnig = true;
        stateArr = getSubjectData($rootScope.guideSetting, this.subjectName);
        if(!stateArr || stateArr.length == 0){
            dbUtils.info("未配置");
            return;
        }
        showNextMenu();
    }

    //显示下一个菜单，等待用户点击操作
    function showNextMenu(){
        let stateObj = stateArr[stateIndex];
        if(!stateObj){
            closeHandle();
            return;
        }

        if(!stateObj.parentMenu){
            startTourGuide();
        }else{
            let stateName = stateObj.state.replace("-",".");
            if(equalsStateHandle(stateName))
                startTourGuide();
            else{
                //判断将要跳转的菜单是否存在
                if(!$rootScope.stateUrlObj[stateName]){
                    dbUtils.alertInfo("当前用户缺少"+stateObj.title+"菜单权限，请联系管理员添加");
                    stopGuide();
                }
                //++菜单如果左侧折叠，先打开，否则显示有问题++
                if(!$(stateObj.parentMenu).hasClass("active")){
                    $(stateObj.parentMenu).addClass("active");
                }
                // console.log(stateObj.state,$rootScope.stateUrlObj)
                //showmenu
                watingClickMenu = true;
                driver.highlight({
                    element: '#'+stateObj.state,
                    stageBackground: '#5a5a5a',
                    position: "right",
                    popover: {
                        title: "<em>"+ stateObj.title +"</em>",
                        showButtons: true,
                        description: stateObj.description
                    }
                });
            }
        }
    }

    function startTourGuide(){
        let stateObj = stateArr[stateIndex];
        if(!stateObj || stateObj.data.length == 0){
            closeHandle();
            return;
        }
        isPreventOnReset = false;
        watingClickMenu = false;

        if(stateObj.hasFun)
            hasFunHandle(stateObj.data);

        if(!stateObj.data[stateObj.data.length-1].onNext){
            stateObj.data[stateObj.data.length-1].onNext = () => {
                if(!driver.hasNextStep()){//点击结束（完成）的操作
                    isPreventOnReset = true;
                    stateIndex++;
                    closeHandle();
                }
            }
        }

        driver.defineSteps(stateObj.data);
        driver.start(0);
    }

    //处理当前的页面和显示tour的页面是否相同
    function equalsStateHandle(stateName) {
        let spcialState = {
            "app.deploy":"app.selectAppDeploy",
            "app.vision":"app.selectAppVision",
            "app.logtable":"app.selectAppLogtable",
        }
        if(spcialState[stateName])
            stateName = spcialState[stateName];
        return $state.current.name == stateName
    }

    //获取主题数据
    function getSubjectData(setting,subjectName){
        if(!setting[subjectName])
            console.log(setting);
        return setting[subjectName] ? setting[subjectName] : [];
    }

    //停止步骤指引
    function stopGuide(index){
        isRunnnig = false;
        watingClickMenu = false;
        stateIndex = index ? index : 0;
        stateArr = [];
        isShowCover = false;
        subjectName = null;
        $rootScope.guideSetting = null;
        setTimeout(function () {
            driver = null;
        },time)
    }

    function closeHandle(){
        if(stateIndex >= stateArr.length){
            stopGuide();
            return;
        }
        setTimeout(function () {
            showNextMenu();
        },time)
    }

    function hasFunHandle(data){
        for(let i=0;i<data.length;i++){
            let item = data[i];
            if(item.onNext){
                let tempArr = item.onNext.split("-");
                let funName = tempArr[0];
                let params = tempArr.slice(1).join(',');
                if(typeof(eval(funName)) == "function"){
                    eval(funName+"("+params+");");
                }
            }
        }
    }

    function setWatingClickMenu(flag){
        watingClickMenu = flag;
        isWatingClickView = true;
    }

    //监控关闭按钮点击事件
    $("body").on("click",'#driver-popover-item .driver-close-btn',function () {
        setTimeout(function () {
            isPreventOnReset = true;
            stopGuide(stateIndex);
        },time)
    })

    return{
        startGuide: function(name){
            if(!$rootScope.guideSetting){
                getSettingData(function (){
                    initDriverAndData(name);
                });
            }else{
                initDriverAndData(name);
            }
        },
        startTourGuide: function(){
            startTourGuide();
        },
        stopGuide: function () {
            stopGuide();
        },
        getIsRunning: function(){
            return isRunnnig;
        },
        getWatingClickMenu: function(){
            return watingClickMenu;
        },
        getIsWatingClickView: function(){
            return isWatingClickView;
        },
        clickedViewHandle: function(){
            isWatingClickView = false;
            stateIndex++;
        },
        resetDriver: function(flag){
            driver.reset(flag?flag:false)
        },
        hideCover: function() {
            console.log("添加遮罩层")
        },
        showCover:function () {
            console.log("添加遮罩层")
        }
    }
}]);
