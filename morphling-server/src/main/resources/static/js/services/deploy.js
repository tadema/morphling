'use strict';

angular.module("app").service('DeployService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    return {
        regist:function(insId,type){
            return dbUtils.post("/deploy/regist",{id:insId,type:type})
        },
        unRegist:function(insId,type){
            return dbUtils.post("/deploy/unRegist",{id:insId,type:type})
        },
        package:function(appId,tag){
            return dbUtils.get("/deploy/package",{appId:appId,tag:tag})
        },
        deploy:function(data){
            return dbUtils.post("/deploy",data);
        },
        customDeploy:function(action,appId,parallel,appFileId,data){
            return dbUtils.postBody("/deploy/custom?appFileId="+appFileId+"&appId="+appId+"&action="+action+"&parallel="+parallel,data);
        },
        deployClient:function(data){
            return dbUtils.post("/deploy/client",data);
        },
        deployCheckFile1:function(data){
            return dbUtils.post("/deploy/checkFile/status1",data);
        },
        deployCheckFile2:function(data){
            return dbUtils.post("/deploy/checkFile/status2",data);
        },
        deployLog:function(appId,start){
            return dbUtils.get("/logs/deploy",{appId:appId,start:start})
        },
        queryFileList:function(appId,deployType,Ids){
            return dbUtils.getBody("/deploy/getDeployFiles?appId="+appId+"&deployType="+deployType,{msIds:Ids.join()});
        },
		listZipFileInfo:function(path, zipFileName){
			return dbUtils.post("/deploy/listZipFileInfo",{path:path, zipFileName:zipFileName});
		},
		diffDeployFileInfo:function(serverPath, serverZipFileName, client, appName){
			return dbUtils.postBody("/deploy/diffDeployFileInfo?serverPath="+serverPath+"&serverZipFileName="+serverZipFileName+"&appName="+appName,JSON.stringify(client));
		},
		listZipFileNames:function(serverPath, serverZipFileName){
			return dbUtils.post("/deploy/listZipFileNames",{serverPath:serverPath,serverZipFileName:serverZipFileName});
		},
        //表格 start
        deployLogTable1:function(queryParams){
            return dbUtils.get("/logs/getDeployLogs",queryParams)
        },
        deployLogTable2:function(appId){
            return dbUtils.get("/logs/getStartingLog?appId="+appId,null)
        },
        //表格 end
        checkDeployStatus:function(appId,id){
            return dbUtils.get("/endpoint/getDeployInfo?appId="+appId+"&id="+id,null)
        },
        healthCheckAll: function(healthCheckVOs){
        	return dbUtils.postBody("/welcome/checkServersAvailable",JSON.stringify(healthCheckVOs));
        },
        nowLog:function(appId,id,lineNum){
            return dbUtils.get("/logs/showRunningLog",{appId:appId,id:id,lineNum:lineNum})
        },
        queryCustomLog:function(app,appDeployCustom,lineNum){
            return dbUtils.post("/logs/showRunningLogCustom",{app:JSON.stringify(app),appDeployCustom:JSON.stringify(appDeployCustom),lineNum:lineNum})
        },
        queryCountNum:function(appId,middlewareType){
            return dbUtils.get("/deploy/getInstanceNum",{appId:appId,middlewareType:middlewareType})
        },
        //GIT
        gitPackage:function(app,fileDescription){
            return dbUtils.post("/deploy/package?fileDescription="+fileDescription,app)
        },
        //listDump
        listDump:function(list){
            return dbUtils.get("/deploy/listDumpFile",list)
        },
        dowmDump:function(filePath,clientId){
            return dbUtils.post("/deploy/downloadDumpFile",{filePath:filePath, clientId:clientId})
        },
        showDump:function(clientIp,clientPort,filePath){
            return dbUtils.get("/logs/showBackupLog",{clientIp:clientIp, clientPort:clientPort,filePath:filePath})
        },
        getClient:function(query){
            return dbUtils.get("/client/listByFilter",query);
        },
        getAppRelationClient:function(appId,query){
            return dbUtils.get("app/"+appId+"/listByFilterCustom",query);
        },
        health: function(data){
            return dbUtils.get("/client/health",data);
        },
        //获取应用服务的健康检查
        healthCheck: function(url,id){
        	return dbUtils.post("/welcome/checkServerAvailable",{url:url,id:id});
        },
		createDiffHtml: function(client,json){
			return dbUtils.postBody("/deploy/createDiffHtml?json="+json,JSON.stringify(client));
		},
		//form方式下载文件
		formDownloadFile: function(row){
			var form = $("<form></form>").attr("action", "/logs/downloadFileCros").attr("method", "post");
			form.append($("<input></input>").attr("type", "hidden").attr("name", "filePath").attr("value", row.logPath));
			form.append($("<input></input>").attr("type", "hidden").attr("name", "ip").attr("value", row.client.hostAddress));
			form.append($("<input></input>").attr("type", "hidden").attr("name", "port").attr("value", row.client.port));
			form.appendTo('body').submit().remove();
		},
        //weblogic传入后台的数据{serverId,serverPort,serverName,serverIp,domainId,isAdmin}
        weblogicDataHandle:function(rows){
        	var domainInfo = new Array();
    		for (var i in rows){
    	    	var obj = new Object();
	    		obj.serverId = rows[i].serverId;
	    		obj.serverPort = rows[i].serverPort;
	    		obj.serverName = rows[i].serverName;
	    		obj.serverIp = rows[i].serverIp;
    	    	obj.domainId = rows[i].domainId;
    	    	obj.isAdmin = rows[i].isAdmin;
    	    	obj.currentVersion = rows[i].currentVersion;
    	    	domainInfo.push(obj);
    		}
            return domainInfo;
        },
        //weblogic判定是否执行指令
        weblogicCheck:function(type,rows,adminServerStatus){
        	var check = true;
        	if(!rows || rows.length == 0){
        		dbUtils.warning("请选择被操作对象！","提示");
        		return false;
        	}
    		for (var i in rows){
    	    	var adminName = rows[i].adminName;
    	    	var status = rows[i].status;
    	    	var name = rows[i].name;
    	    	if(rows[i].isAdmin == 1){//管理server
    	    		if(type == "start"){check = this.startCheck(status,name);}
    	    		else if(type == "stop" || type == "dump"){check = this.stopCheck(status,name,type);}
    	    		else if(type == "deploy"){
						check = false;
						dbUtils.warning(adminName+"为管理server，禁止部署文件","提示");
						break;
    	    		}
    	    	}else{//受管server
    	    		if(type == "start"){check = this.startCheck(status,name);}
    	    		else if(type == "stop" || type == "dump"){check = this.stopCheck(status,name,type);}
    	    		else if(type == "deploy"){
    	    			if(adminServerStatus == 1){
//    	    				check = deployCheck(status,name);2019-08修改
    	    				check = true;
    		    		}else{
    		    			check = false;
    		    			dbUtils.warning(name+"的管理server:  "+adminName+"尚未启动，不可进行此操作","提示");
    		    		}
    	    		}
    	    	}
    	    	if(check == false)
    	    		return check;
    		}
            return check;
        },
        //启动  已经启动不可再次启动 weblogic+tomcat
        startCheck:function(status,name){
        	var check = true;
    		if(status == 1){
    			check = false;
    			dbUtils.warning(name+"已经启动！","提示");
    		}
            return check;
        },
        //停止  尚未启动不可停止 weblogic+tomcat
        stopCheck:function(status,name,type){
        	var check = false;
    		if(status == 1){
    			check = true;
    		}else{
    			if(type == "stop"){
    				check = true;//命令为停止，就执行
    			}else if(type == "dump"){
    				dbUtils.warning(name+"尚未启动！无法生成dump文件","提示");
    			}
    		}
            return check;
        },
        //部署  尚未启动不可部署 仅weblogic
        deployCheck:function(status,name){
        	var check = false;
    		if(status == 1){
    			check = true;
    		}else{
    			dbUtils.warning(name+"尚未启动！无法进行此操作","提示");
    		}
            return check;
        },
        //weblogic 获取受管服务id
        getManagedServerId:function(rows){
        	var ids = [];
            for(var i in rows){
            	if(rows[i].isAdmin == 0){	    		
    	    		ids.push(rows[i].serverId);
    	    	}
            }
            return ids;
        },
		//weblogic 当部署文件类型为catalog时，需要获取adminserver的agent对象
		getAdminserverClient:function(scope){
        	let adminClientId;
			scope.table._rows.forEach(function(item){
        		if(item.isAdmin == 1)
					adminClientId = item.deployFileId;
			})
			if(!adminClientId){
				console.log("weblogic应用发布，没有找到Adminserver，文件对比时会出错");
				scope.adminserverClient = null;
				return;
			}

			if(!scope.clients){
				dbUtils.get("client/listAgent").then(function(response){
					if(response.code == SUCCESS_CODE){
						response.data.forEach(function(item){
							if(item.id == adminClientId){
								scope.adminserverClient = item;
							}
						})
					}else{
						dbUtils.error(response.message)
					}
				});
			}else{
				scope.clients.forEach(function(item){
					if(item.id == adminClientId){
						scope.adminserverClient = item;
					}
				})
			}
		},
        //tomcat传入后台的数据
        tomcatDataHandle:function(rows){
        	var ids = new Array();
     		for (var i in rows){
     	    	var obj = {tomcatId:rows[i].tomcatId};
     	    	ids.push(obj);
     		}
            return ids;
        },
        //tomcat判定是否执行指令
        tomcatCheck:function(type,rows){
        	var check = true;
        	if(rows.length == 0){
        		check = false;
        		dbUtils.error("请选择被操作对象！","提示");
        		return check;
        	}
    		for (var i in rows){
    	    	var status = rows[i].status;
    	    	var name = rows[i].version;
    	    	if(type == "start"){check = this.startCheck(status,name);}
        		else if(type == "stop" || type == "dump"){check = this.stopCheck(status,name,type);}
    	    	if(check == false){return check;}
    		}
            return check;
        },
        //tomcat 查询文件列表 的数据
        getTomcatId:function(rows){
        	var ids = [];
            for(var i in rows){
                ids.push(rows[i].tomcatId);
            }
            return ids;
        },
        //springBoot传入后台的数据
        springBootDataHandle:function(rows){
            var sbInfo = new Array();
    		for (var i in rows){
    	    	var obj = new Object();
//    	    	obj.host = rows[i].host;
//    	    	obj.hostName = rows[i].hostName;
//    	    	obj.port = rows[i].port;
    	    	obj.id = rows[i].id;
    	    	sbInfo.push(obj);
    		}
            return sbInfo;
        },
        //springBoot判定是否执行指令
        springBootCheck:function(type,rows){
        	var check = true;
        	if(rows.length == 0){
        		check = false;
        		dbUtils.error("请选择被操作对象！","提示");
        		return check;
        	}
    		for (var i in rows){
    	    	var status = rows[i].status;
    	    	var name = rows[i].host + ":" + rows[i].port ;
    	    	var id = rows[i].id;
    	    	if(type == "start"){check = this.startCheck(status,name);}
        		else if(type == "stop" || type == "dump"){check = this.stopCheck(status,name,type);}
    	    	if(check == false){return check;}
    		}
            return check;
        },
        //springBoot 获取springbootid信息
        getSpringBootId:function(rows){
        	var ids = [];
            for(var i in rows){
                ids.push(rows[i].id);
            }
            return ids;
        },
        /*-----------文件上传------------*/
        //获取版本号参数
        getVersionNum:function($scope){
        	var num = $scope.nowVersion.split(".");
        	$scope.versionA = this.getNum(num[0]);
            $scope.versionB = this.getNum(num[1]);
            $scope.versionC = this.getNum(num[2]);
            $scope.versionOne = $scope.versionA +"."+ $scope.versionB +"."+ ($scope.versionC + 1) ;
            $scope.versionTwo = $scope.versionA +"."+ ($scope.versionB + 1)  +"."+ "0";
            $scope.versionThree = ($scope.versionA + 1)  +"."+ "0" +"."+ "0" ;
        },
		fileCheck:function($scope){
        	if(!$scope.app.name || !$scope.file.name)
        		return false;
			let appName = $scope.app.name.toLowerCase();
			let fileName = $scope.file.name.toLowerCase();
			return fileName.indexOf(appName) != -1 ? true : false;
		},
        getNum:function(number){
        	if(!number || number == "undefined"){
        		return 0;
        	}else{
        		number = parseInt(number);
        		if(number >= 0){
        			return  number;
        		}else{
        			return 0;
        		}
        	}
        },
        versionCheck:function($scope,n,Z) {
        	if($scope.form.versionA.$invalid || $scope.form.versionB.$invalid || $scope.form.versionC.$invalid)
				return;
        },
        getVersion:function($scope){
        	var version =  $scope.nowVersion;
            if($scope.versionType == 1){
            	version =  $scope.versionOne;
            }else if($scope.versionType == 2){
            	version =  $scope.versionTwo;
            }else if($scope.versionType == 3){
            	version =  $scope.versionThree;
            }else if($scope.versionType == 4){
            	version =  $scope.versionA +"."+ $scope.versionB +"."+ $scope.versionC;
            }
            return version;
        },
        addStatusForClient:function($scope,data){
        	var returnData = [];
        	for (var key in data) {
        		var obj = data[key];
        		this.getSingleClientStatus($scope,obj);
        		returnData.push(obj);
        	}
        	return returnData;
        },
        //获取单个主机的连接信息
        getSingleClientStatus:function($scope,row){
        	if(row.isAgent == 1){
        		this.health({"host":row.hostAddress,"port":row.port}).then(function (response){
                    if(response.code == SUCCESS_CODE){
                        //请求成功，状态判断
                        $scope.health[row.id] = response.data.status;
                        $scope.healthDetail[row.id] = dbUtils.toString(response.data);
                    }else{
                        $scope.health[row.id] = "DOWN";
                        $scope.healthDetail[row.id] = response.message;
                    }
                })
        	}else{
        		$scope.health[row.id] = "NONE";
                $scope.healthDetail[row.id] = "无";
        	}
        },
        //主机去重
        distinctClient:function($scope){
    		var arrTemp = [];
    		var idTemp = [];
    		for(var i in $scope.client){
    			if(arrTemp.length == 0){
    				arrTemp.push($scope.client[i]);
    				idTemp.push($scope.client[i].id);
    			}else{
    				if(idTemp.indexOf($scope.client[i].id) == -1){
    					arrTemp.push($scope.client[i]);
    					idTemp.push($scope.client[i].id);
    				}
    			}
    		}
    		return arrTemp;
    	},
        //为主机添加上传路径
        addUploadPathToClient:function($scope){
        	for(var i in $scope.client){
        		for(var j in $scope.middlewareArr){
        			var clientId;
        			var username;
        			var ip;
        			if($scope.app.middlewareType == "nginx"){
        				clientId = $scope.middlewareArr[j].clientId;
        	    	}else if($scope.app.middlewareType == "weblogic"){
        	    		if($scope.middlewareArr[j].adminUsername == $scope.client[i].username && $scope.middlewareArr[j].adminIp == $scope.client[i].hostAddress){
        	    			$scope.client[i].uploadPath = $scope.middlewareArr[j].domainPath;
            				$scope.thead.uploadPath = $scope.middlewareArr[j].domainPath;
            				continue;
    	    			}
        	    	}else if($scope.app.middlewareType == "tomcat"){
        	    		clientId = $scope.middlewareArr[j].deployFileId;
        	    	}
        			
        			if(angular.isDefined(clientId)){
        				if($scope.client[i].id == clientId){
            				$scope.client[i].uploadPath = $scope.middlewareArr[j].installPath;
            				$scope.thead.uploadPath = $scope.middlewareArr[j].installPath;
            				continue;
            			}
        			}
        		}
        	}
        }
        /*-----------文件上传 end------------*/
    }
}]);

