'use strict';

angular.module("app").service('ClientService', ['dbUtils', '$http', '$q','$cookies', function (dbUtils, $http, $q,$cookies) {
    return {
        health: function(data){
            return dbUtils.get("/client/health",data);
        },
        healthCheck: function(clients){
            return dbUtils.postBody("/client/healthCheck",JSON.stringify(clients));
        },
        save: function(data){
            return dbUtils.post("/client",data);
        },
        update: function(data){
            return dbUtils.post("/client/update",data);
        },
        edit: function(data,isDeleteAgent){
            return dbUtils.post("/client/modify?isDeleteAgent="+isDeleteAgent,data);
        },
        remove: function(data){
            return dbUtils.delete("/client",data);
        },
        updataAll:function(data){
            return dbUtils.post("/client/batchUpdate",data);
        },
        batchUpdateNew:function(data){
            return dbUtils.post("/client/batchUpdateNew",data);
        },
        count:function(){
            return dbUtils.get("/client/checkAllStatus",null);
        },
        fileUpdata: function(formData){
        	return dbUtils.postPicture('/client/batchImport',formData);
        },
        fileDown:function(){
            return dbUtils.get("/client/downloadTemplate",null);
        },
        messege: function(data){
            return dbUtils.postBody("/client/getSystemInfo",data);
        },
        getAgentList:function(){
            return dbUtils.get("/client/listAgent",null);
        },
        checkConnected:function(data){
            return dbUtils.postBody("/client/checkConnected",data);
        },
        checkConnectedAll:function(clients){
            return dbUtils.postBody("/client/checkConnectedAll",JSON.stringify(clients));
        },
        testConnected:function(data){
            return dbUtils.postBody("/client/testConnected",data);
        },
        getPorts:function(){
            return dbUtils.get("/ports/getPorts",null);
        },
        getPortsByIp:function(ip){
            return dbUtils.get("/ports/getPortsByIp",{"ip":ip});
        },
        //自动补全client模型的name
        fillClientName:function($scope,value,position,isEdit,isStart){
        	if(isEdit || !isStart)//编辑状态失效，启动标记false失效
        		return;
            if(!$scope.form.name.$dirty){
            	if(!value)
            		value = "";
            	let names = !$scope.client.name ? ["",""] : $scope.client.name.split("-");
            	if(position == "front"){
            		$scope.client.name = value + "-" + names[1];
            	}else{
            		$scope.client.name = names[0] + "-" + value;
            	}
            }
        },
    }
}]);