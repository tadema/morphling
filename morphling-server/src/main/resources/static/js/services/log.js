var log = angular.module("EchoLog", []);
log.factory("EchoLog", ["$http", "$window", "$q", "toaster", "$modal",'$interval', 'dbUtils',function ($http, $window, $q, toaster, $modal,$interval,dbUtils){
    function LogController ($scope,$modalInstance,logId){
        $scope.ok = function () {
            $modalInstance.close('cancel');
        };
        var ws = location.protocol === 'https:'? 'wss':'ws';
        var websocket = new WebSocket(ws+"://"+window.location.host+"/echo?logId="+logId);

        var printing = false;
        $scope.stopWebsocket = false;

        var logInternal = setInterval(function(){
        	if($scope.stopWebsocket == false){
        		var e = angular.element("#log_container")[0];
               /* var tmp = e.innerHTML;
                if(tmp.length > 100000){
                    tmp = tmp.substring(50000,tmp.length);
                }*/
//                e.innerHTML=tmp;
                if(printing && angular.element("#scrollControl")[0].checked){
                    e.scrollTop=e.scrollHeight;
                    printing = false;
                }
            }         
        },200);
        websocket.onopen = function(event) {

        }


        // 监听消息
        websocket.onmessage = function(event) {
        	if($scope.stopWebsocket == false){
        		 printing = true;
                 log(event.data);
            }
        };

        // 监听Socket的关闭
        websocket.onclose = function(event) {
            if($scope.stopWebsocket == false){
            	window.clearInterval(logInternal);
            	$scope.$apply(function(){
                    $scope.finished = true;
                })
            }
        };
        
        $scope.cancel = function () {
//        	 $scope.stopWebsocket = true;
//        	 printing = false;
    		 $.ajax({  
                 type: "get", 
                 url: "/logs/closeLog", 
                 data: {logId:logId},  
                 contentType: "application/json;charset=utf-8",  
                 dataType: "json",  
                 success: function (response, ifo) {  
//                 	console.log(response);
                 	 if (response.code == SUCCESS_CODE) {
               	       	  dbUtils.info("关闭成功","提示");
                       } else {
                           dbUtils.error(response.message,"提示");
                       } 
	                   $modalInstance.close('cancel');
                 }, error: function () {  
                     alert("error");
                     $modalInstance.close('cancel');
                 }
     		})
        	 $scope.stopWebsocket = true;
        	 printing = false;
        	 $modalInstance.close('cancel');
        };


    }


    function log(line){
        var e = angular.element("#log_container")[0];
        e.innerHTML = e.innerHTML + line + "<br/>";

    }

    return {
        show:function(logId,okFun){
            $modal.open({
                animation: true,
                templateUrl: "tpl/log/log.html",
                controller: ['$scope', '$modalInstance','logId', LogController],
                size: "lg",
                backdrop: "static",
                windowClass: "modal-large",
                keyboard: false,
                resolve: {
                    logId:function(){
                        return logId+"";
                    }
                }
            }).result.then(function () {
                if (angular.isFunction(okFun)) {
                    okFun.call();
                }
            });
        }
    }
}]);
