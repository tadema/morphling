'use strict';

angular.module("app").service('TomcatService', ['dbUtils', '$http', '$q', function (dbUtils, $http, $q) {
    return {        
        getPostIp:function(){
            return dbUtils.get("/client",null);
        },
        getAgentPost:function(){
            return dbUtils.get("/client/listAgent",null);
        },
        deleteTomcat:function(tomcatId){
            return dbUtils.delete("/middleware/tomcat?id="+tomcatId,null);
        },
        doSaveTomcat:function(tomcat){
            return dbUtils.put("/middleware/tomcat",tomcat);
        },
        getPorts:function(){
            return dbUtils.get("/ports/getPorts",null);
        },
        getPortsByIp:function(ip){
            return dbUtils.get("/ports/getPortsByIp",{"ip":ip});
        },
        checkStatus:function(ip,port){
        	return dbUtils.post("/middleware/tomcat/getTomcatStatus",{"ip":ip,"port":port});
        },
        checkHealth:function(healthCheckVOs){
        	return dbUtils.postBody("/domain/checkHealth",JSON.stringify(healthCheckVOs));
        },
    }
}]);