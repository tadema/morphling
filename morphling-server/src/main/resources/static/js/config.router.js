'use strict';

/**
 * Config for the router
 */
angular.module('app')
    .run(
        ['$rootScope', '$state', '$stateParams', '$cookies', '$cookieStore', 'AccountService','$window',
            function ($rootScope, $state, $stateParams, $cookies, $cookieStore, accountService,$window) {
                $rootScope.$state = $state;   //方便获取状态
                $rootScope.$stateParams = $stateParams;    //方便获取状态
                $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState) {
                    if (toState.name == 'app.logout') {
                    	//移除localStorage中的Tabs和tabindex
                    	/*$window.localStorage["tabs"] = null;
                		$window.localStorage["tabIndex"] = null;*/
                    	$window.sessionStorage.removeItem("tabs");
                    	$window.sessionStorage.removeItem("tabIndex");
                        accountService.logout().then(function () {
                            //$state.go("access.signin", {from: fromState.name, w: 'logout'});
//                        	console.log("登出了，地址跳转为/");
                            window.location.href = "/";
                        });
                    }
                    if (toState.name == 'access.signin' || toState.name == 'access.forgotpwd' || toState.name == 'access.resetpwd' ) {
//                    	console.log("进入登录界面");
                        return;// 如果是进入登录界面则允许
                    }

                    //请求异步导致下面执行出错，可优化,
                    //换成了jquery的
                    if (!$rootScope._userinfo) { //授权页面
//                    	console.log("用户信息不存在，准备获取");
                        accountService.getAccount(function (response) {
                            if (response && response.code == SUCCESS_CODE) {
//                            	console.log("信息获取成功，开始替换");
                                $rootScope._userinfo = response.data;
                                $rootScope._userinfo.currentEnv = accountService.getEnv($rootScope._userinfo.envs);
                            }
                        }, function () {
//                        	console.log("信息获取失败，返回");
                            //请求失败
                            return;
                        });
                    }
                    if ($rootScope._userinfo) {
//                    	console.log("背景赋为空");
                    	//发送告警信息查询的请求
//                    	$rootScope.$broadcast("alarmQuery",{});
                        
                    	$('body').css("background","url('')");
                        $('body').css("background-color","transparent");
                        return;
                    } else {
//                    	console.log("跳到登录界面");
                        event.preventDefault();
                        $state.go("access.signin", {from: fromState.name, w: 'notLogin'});//跳转到登录界面
                    }

                });
            }
        ]
    )
    .config(
        ['$stateProvider', '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {
        	
                $urlRouterProvider.otherwise('/app/home');
                $stateProvider
                    .state('app', {
                        abstract: true,
                        url: '/app',
                        templateUrl: 'tpl/app.html'  // app.html是搭建框架的，相当于sitemesh
                    })
                    .state('app.home', {
                        url: '/home',
                        templateUrl: 'tpl/user/home.html',
                    })
                    .state('app.auth', {
                        url: '/auth',
                        templateUrl: 'tpl/user/auth.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/auth.js', 'js/services/cardaccount.js']);
                                }]
                        }
                    })
                    .state('app.logout', {
                        url: '/logout',
                        template: '<div ui-view class="fade-in-up"></div>'
                    })
                    .state('app.modifypwd', {
                        url: '/modifypwd',
                        templateUrl: 'tpl/access/page_modifypwd.html',
                        params: {title:"修改密码"},
                        /*resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/modifypwd.js']);
                                }]
                        }*/
                    })
                    .state('app.personalinfo', {
                        url: '/personalinfo',
                        templateUrl: 'tpl/user/personalinfo.html',
                        params: {title:"个人信息"},
                        /*resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/personalinfo.js', 'js/services/user.js', 'js/services/baseinfo.js']);
                                }]
                        }*/
                    })
                    .state('app.endpoints', {
                        url: '/endpoints/:appId',
                        templateUrl: 'tpl/endpoints/endpoints_view.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/endpoints.js','js/services/application.js']);
                                }]
                        }
                    })
                    .state('app.degrade', {
                        url: '/degrade/:appId',
                        templateUrl: 'tpl/degrade/degrade_manage.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/degradeManage.js', 'js/services/application.js','js/services/degrade.js']);
                                }]
                        }
                    })
                    .state('app.cache', {
                        url: '/cache/:appId',
                        templateUrl: 'tpl/cache/cache_manage.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/cacheManage.js', 'js/services/application.js','js/services/cache.js']);
                                }]
                        }
                    })
                    .state('app.selectApp', {
                        url: '/selectApp?from',
                        params:{title:""},
                        templateUrl: 'tpl/user/select_app.html',
                        /*resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/selectApp.js', 'js/services/application.js']);
                                }]
                        }*/
                    })
                    .state('app.selectAppDeploy', {
                        url: '/selectAppDeploy',
                        params:{title:"选择应用(发布)",to:"app.deploy"},
                        templateUrl: 'tpl/user/select_app_deploy.html',
                    })
                    .state('app.selectAppVision', {
                        url: '/selectAppVision',
                        params:{title:"选择应用(版本)",to:"app.vision"},
                        templateUrl: 'tpl/user/select_app_vision.html',
                    })
                    .state('app.selectAppLogtable', {
                        url: '/selectAppLogtable',
                        params:{title:"选择应用(日志)",to:"app.logtable"},
                        templateUrl: 'tpl/user/select_app_logtable.html',
                    })
                    
                    .state('app.deploy', {
                    	url: '/deploy/:deployAppId/:fromWhere',
                    	params:{'app':null,role:"c",roleId:"deploy"},//只有role:c，没有role为p，因为只需要当普通和自定义同时出现的时候，相互覆盖
                        templateUrl: 'tpl/deploy/deploy_app.html',
                        /*resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/deploy.js', 'js/services/application.js','js/services/deploy.js', 'js/directives/form-edit.js']);
                                }]
                        }*/
                    })  
                    .state('app.customDeploy', {
                    	url: '/customDeploy/:deployAppId/:fromWhere',
                    	params:{'app':null,title:"应用发布",role:"c",roleId:"deploy"},
                        templateUrl: 'tpl/deploy/custom_deploy.html',
                        /*resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/customDeploy.js', 'js/controllers/application.js','js/services/application.js','js/controllers/deploy.js','js/services/deploy.js', 'js/services/client.js', 'js/directives/form-edit.js']);
                                }]
                        }*/
                    }) 
                    .state('app.logtable', {
                    	url: '/logtable/:logtableAppId/:fromWhere',
                    	params:{'app':null},
                        templateUrl: 'tpl/logtable/app_log.html',//测试新模板
                        /*resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/logTable.js', 'js/services/deploy.js', 'js/directives/form-edit.js']);
                                }]
                        }*/
                    })
                    .state('app.systemLog', {
                    	url: '/systemLog/',
                        templateUrl: 'tpl/log/system_log_list.html',
                    })
                    .state('app.manage', {
                        url: '/manage',
                        templateUrl: 'tpl/app/app_list.html',
                        params: {},
                        /*resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/application.js', 'js/services/application.js', 'js/directives/form-edit.js']);
                                }]
                        }*/
                    })
                    
                    //监控管理
                    .state('app.monitorManager', {
                        url: '/monitorManager',
                        templateUrl: 'tpl/monitor/monitor_manager.html',
                        /*resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/monitor.js', 'js/services/monitor.js']);
                                }]
                        }*/
                    })
                    
                    //数据库监控
                    .state('app.monitorDatabase', {
                        url: '/monitorDatabase',
                        templateUrl: 'tpl/monitor/monitor_database.html',
                        params: {}
                    })
                    //OGG监控
                    .state('app.monitorOGG', {
                        url: '/monitorOGG',
                        templateUrl: 'tpl/monitor/monitor_ogg.html',
                        params: {}
                    })
                    //redis监控
                    .state('app.monitorRedis', {
                        url: '/monitorRedis',
                        templateUrl: 'tpl/monitor/monitor_redis.html',
                        params: {}
                    })
                     //监控定义
                    .state('app.monitorDefine', {
                        url: '/monitorDefine',
                        templateUrl: 'tpl/monitor/monitor_define.html',
                        /*resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/monitorDefine.js', 'js/services/monitor.js']);
                                }]
                        }*/
                    })
                    
                    //脚本管理
                    .state('app.script', {
                        url: '/script',
                        templateUrl: 'tpl/script/script_list.html',
                        params: {},
                        /*resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/script.js', 'js/services/script.js']);
                                }]
                        }*/
                    })
                    
                    //文件管理
                    .state('app.filelist', {
                        url: '/filelist',
                        templateUrl: 'tpl/file/file_list.html',
                        params: {},
                        /*resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/filelist.js', 'js/services/filelist.js']);
                                }]
                        }*/
                    })
                    //软件安装
                    .state('app.softwareInstall', {
                        url: '/softwareInstall',
                        templateUrl: 'tpl/software_install/software_install_list.html',
                        params: {},
                        /*resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/filelist.js', 'js/services/filelist.js']);
                                }]
                        }*/
                    })
                    
                    //告警信息
                    .state('app.alarmManagement', {
                        url: '/alarmManagement',
                        templateUrl: 'tpl/alarm/alarm_manager.html',
                        /*resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/alarm.js', 'js/services/alarm.js']);
                                }]
                        }*/
                    })
                    
                    //告警规则
                    .state('app.alarmRule', {
                        url: '/alarmRule',
                        templateUrl: 'tpl/alarm/alarm_rule.html',
                        /*resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/alarm.js', 'js/services/alarm.js']);
                                }]
                        }*/
                    })
                    //告警配置
                    .state('app.alarmConfig', {
                        url: '/alarmConfig',
                        templateUrl: 'tpl/alarm/alarm_config.html',
                        /*resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/alarm.js', 'js/services/alarm.js']);
                                }]
                        }*/
                    })
                
                    .state('app.test01', {
                        url: '/test01',
                        templateUrl: 'tpl/test/test01.html',//test01  test_home
                        abstract:true,
//                        params:{},
                        params: {data:{},'cdata':{groupName:'11',ruleName:'11'}},
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(['js/controllers/test01.js','js/services/alarm.js']);//test01  testHome
                                }]
                        }
                    })

                    .state('access', {
                        url: '/access',
                        template: '<div ui-view class="fade-in-right-big smooth"></div>'
                    })
                    .state('access.signin', {
                        url: '/signin',
                        templateUrl: 'tpl/access/page_signin.html',
                        resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/signin.js']);
                                }]
                        }
                    })
                    .state('access.forgotpwd', {
                        url: '/forgotpwd',
                        templateUrl: 'tpl/access/page_forgotpwd.html',
                        resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/forgotpwd.js', 'js/controllers/imgvalid.js']);
                                }]
                        }
                    })
                    .state('access.resetpwd', {
                        url: '/resetpwd?{phone:1[345789][0-9]{9}}&code',//
                        templateUrl: 'tpl/access/page_resetpwd.html',
                        resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/resetpwd.js']);
                                }]
                        }
                    })
                    .state('system', {
                        url: '/system',
                        templateUrl: 'tpl/app.html'
                    })
                    .state('app.client', {
                        url: '/client',
                        templateUrl: 'tpl/system/client_list.html',
                        cache:true,
                        /*resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/client.js', 'js/services/client.js']);
                                }]
                        }*/
                    })
                    .state('app.users', {
                        url: '/users',
                        templateUrl: 'tpl/system/user_list.html',
                        /*resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/user.js', 'js/services/user.js','js/services/role.js','js/services/application.js']);
                                }]
                        }*/
                    })
                    .state('app.mailServer', {
                        url: '/mailServer',
                        templateUrl: 'tpl/system/mailServer_list.html',
                    })
                    .state('app.organization', {
                        url: '/organization',
                        templateUrl: 'tpl/system/organization_list.html',
                    })
                     .state('app.roleManager', {
                        url: '/roleManager',
                        templateUrl: 'tpl/role/role_manager.html',
                        /*resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/role.js', 'js/services/role.js','js/services/application.js']);
                                }]
                        }*/
                    })
                    /*.state('system.app', {
                        url: '/app',
                        templateUrl: 'tpl/app/app_list.html',
                        resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/application.js', 'js/services/application.js']);
                                }]
                        }
                    })*/
                    .state('app.domain', {
                        url: '/domain',
                        templateUrl: 'tpl/app/domain_list.html',
//                        templateUrl: 'tpl/domain/domain_list.html',
                        /*resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/domain.js', 'js/services/domain.js', 'js/directives/form-edit.js']);
//                            		return uiLoad.load(['js/controllers/domainServer.js', 'js/services/domainServer.js', 'js/directives/form-edit.js']);
                                }]
                        }*/
                    })
                    .state('system.domainConsole', {
                        url: '/domainConsole',
                        templateUrl: 'tpl/domain/domainConsole_list.html',
                        params: {domainData:{}},
                        /*resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                            		return uiLoad.load(['js/controllers/domainServer.js', 'js/services/domainServer.js']);
                                }]
                        }*/
                    })
                    .state('app.tomcat', {
                        url: '/tomcat',
                        templateUrl: 'tpl/tomcat/tomcat_list.html',
                        /*resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/tomcat.js', 'js/services/tomcat.js']);
                                }]
                        }*/
                    })
                    .state('app.nginx', {
                        url: '/nginx',
                        templateUrl: 'tpl/nginx/nginx_list.html',
                        params: {},
                        /*resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/nginx.js', 'js/services/middleware.js']);
                                }]
                        }*/
                    })
                    .state('system.instance', {
                        url: '/{appId:\\d+}/instance',
                        templateUrl: 'tpl/app/instance_admin.html',
                        resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/application.js', 'js/services/application.js']);
                                }]
                        }
                    })
                    
                    .state('app.managerWeblogic', {
                        url: '/{appId:\\d+}/managerWeblogic',
                        templateUrl: 'tpl/app/weblogic_admin.html',
                        params:{title:"应用管理"},
                        /*resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/application.js', 'js/services/application.js']);
                                }]
                        }*/
                    })
                    .state('app.managerTomcat', {
                        url: '/{appId:\\d+}/managerTomcat',
                        templateUrl: 'tpl/app/tomcat_admin.html',
                        params:{title:"应用管理"},
                        /*resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/application.js', 'js/services/application.js']);
                                }]
                        }*/
                    })
                    .state('app.managerSpringBoot', {
                        url: '/{appId:\\d+}/managerSpringBoot',
                        templateUrl: 'tpl/app/springboot_relation.html',//springBoot_admin
                        params:{title:"应用管理"},
                        /*resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['js/controllers/application.js', 'js/services/application.js']);
                                }]
                        }*/
                    })

            }
        ]
    );