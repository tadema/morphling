angular.module('app').directive('uiBaseTable', ['$rootScope', '$anchorScroll','dbUtils', '$state', '$sce','$modal', function($rootScope, $anchorScroll,dbUtils, $state, $sce,$modal) {
	return {
		restrict: 'EA',
		templateUrl:'tpl/templates/uiBaseTable.html',
		scope: {
			labelTitle:"@?",
			isShowCheckbox: "@",
			bodyStyle: "@",
			headers: "=",
			refresh: "=?",
			tooltipData: "=?",
			operationEvents: "=?",
			rowEvents: "=?",
			page: "=?",
			action:"=?",//请求xxxService.getXXX()返回值
			initData: "=?",//配了该参数，则action和url就不执行了
			url: "@?",//请求url
			method: "@?",//get 或是 post
			methodData: "=?",//请求数据
			afterReload: "=?",//重载后执行函数
		},
		controller: ['$scope', function ($scope,$parent, el, attrs) {
			$scope.t_id = dbUtils.guid();
			if($scope.labelTitle){
				$scope.isHiddenTitle = false;
			}else{
				$scope.isHiddenTitle = true;
				$scope.mainStyle = {"border":"0px solid black"};
			}
			/*sort 使用footable后，自定义的排序不起作用 了*/
			$scope.sort = {title : 'SID' ,desc : false};
			
			$scope.orderIt = function(item){
				return item[$scope.sort.title];
			};
			
			$scope.headerClick = function(event,header){
				for(let item of $scope.headers){
					if(item.field == header.field)
						continue;
					item.desc = false;
				}
				header.desc = !header.desc;
				$scope.sort.desc = header.desc;
				$scope.sort.title = header.field;
			}
			/*sort end*/
			
			/*表格页码*/
			$scope.isShowFooter = false;
			if($scope.page && $scope.page.isPagination){
				if(!$scope.page.postgroundPagination)
					$scope.page.postgroundPagination = false;
				$scope.isShowFooter = true;
				$scope.pageSelect = [5,10,15,20,50,100];
			}
			/*表格页码 end*/
			
			$scope.isShowCheckbox = (!$scope.isShowCheckbox || $scope.isShowCheckbox == "" || $scope.isShowCheckbox == "0") ? false : true;
			
			$scope.bodyStyle = $scope.bodyStyle ? JSON.parse($scope.bodyStyle) : {"max-height": "350px"};
			
			if(!$scope.rowEvents)
				$scope.rowEvents = []
			
			function initData(){
				$scope.loading = true;
				$scope.rows = [];
				$scope.data = [];
				//等待数据查询完毕
				if(!$scope.action && (!$scope.initData || $scope.initData.length == 0))
					return;
				
				if($scope.initData && $scope.initData.length > 0){//如果有传入初始数据，则不通过查询url获取数据
					$scope.loading = false;
					$scope.data = $scope.initData; 
					//处理Header
					headerHandle();
					//转换数据
					dataHandle();
					//分页处理
					if($scope.page &&　$scope.page.isPagination == true && $scope.page.postgroundPagination == true)
						paginationHandle(paginationData);
					else if(!$scope.page){
						setTimeout(function () {
							$("#"+$scope.t_id+" #baseTable").attr("data-page-size",$scope.rows.length);//动态修改table的pageSize的值
//							$("#"+$scope.t_id+" #baseTable").footable();//重新加载表格
						}, 100);
					}
				}else{
					$scope.action.then(function(response){
						if(response.code == SUCCESS_CODE){
							$scope.loading = false;
							var paginationData = response.data;//这个必须是数组数据,用来分页处理时使用
							if($scope.page && $scope.page.isPagination == true && $scope.page.postgroundPagination == true){
								$scope.data = response.data.data;
							}else{
								$scope.data = response.data;
							}
							//处理Header
							headerHandle();
							//转换数据
							dataHandle();
							//分页处理
							if($scope.page &&　$scope.page.isPagination == true && $scope.page.postgroundPagination == true)
								paginationHandle(paginationData);
							else if(!$scope.page){
								setTimeout(function () {
									$("#"+$scope.t_id+" #baseTable").attr("data-page-size",$scope.rows.length);//动态修改table的pageSize的值
//                    			$("#"+$scope.t_id+" #baseTable").footable();//重新加载表格
								}, 100);
							}
						}else{
							dbUtils.error(response.message,"提示");
						}
					},function(response){
						$scope.loading = false;
					})
				}
				$scope.afterReload && $scope.afterReload();
			}
			
//			initData();
			
			//处理Header
			function headerHandle(){
				if(!$scope.headers && $scope.data && $scope.data.length > 0){
					$scope.headers = [];
					let headers = Object.keys($scope.data[0]);
					for(let header of headers){
						$scope.headers.push({field:header});
					}
				}
				if(!$scope.colspanNum && $scope.isShowFooter == true && $scope.page.postgroundPagination == false){
					$scope.colspanNum = $scope.headers.length;
					if($scope.isShowCheckbox)
						$scope.colspanNum++;
					if($scope.rowEvents.length > 0)
						$scope.colspanNum++;
				}
			}
			
			//转换数据
			function dataHandle(){
				for(let i in $scope.data){
    				let _row = $scope.data[i];
    				if(!_row.id)
    					_row.id = i;
    				_row.index = i;
    				let row = {index:_row.index};
    				for(let header of $scope.headers){
						var value = _row[header.field];// + ''
						if(header.formatter){
							value = header.formatter(value,_row);
						}
						if(header.compile && !header.directive){
							value += "";
							value = $sce.trustAsHtml(value);
						}
						row[header.field] = value;
    				}
                    $scope.rows.push(row);
    			}
			}
			
			//定时查询
			let intervalId;
			$scope.$watch('refresh',function(current,old){
				if(current != old){
					if(current != "off"){
						if(intervalId)
							clearInterval(intervalId);
						
						intervalId = setInterval($scope.reloadData,current);//initData
					}else{
						if(intervalId)
							clearInterval(intervalId);
					}
				}
			})
			//刷新数据
			$scope.$on('reloadData', function(event,data) {
				setTimeout(function(){
					$scope.reloadData();
		        },100);
		    });
			//获取选中的数据
			$scope.$on('getSelectedData', function(event,data) {
				$scope.$emit('sendSelectedData',getAllSelectRows());
		    });

			//获取选中的数据，并且返回入参数据
			$scope.$on('getSelectedItem', function(event,data) {
				$scope.$emit('sendSelectedItem',Object.assign(data,{rows:getAllSelectRows()}));
			});
			
			$scope.reloadData = function(){
				if($scope.url){
					if(!$scope.method || $scope.method == "get"){
						let queryParams = {currentPage:$scope.page.pageNumber, onePageSize:$scope.page.pageSize};
						$scope.action = dbUtils.get($scope.url,queryParams);
					}
					else if($scope.method == "post")
						$scope.action = dbUtils.post($scope.url,$scope.methodData);
					else if($scope.method == "postBody")
						$scope.action = dbUtils.postBody($scope.url,$scope.methodData);
				}
            	initData();
			}
			$scope.reloadData();
			
			$scope.operations = {
				refreshData: function(){
                	if($scope.page && $scope.page.isPagination == true ){
                		$scope.page.pageNumber = 1;
                    }
                	//重新查询一次
                	$scope.reloadData();
                },
//              	grid上方按钮点击事件
                operationButtonClick: function(clickFun){
                    var rows = getAllSelectRows();
                    clickFun(rows);
                    $scope.checkedRow();
                },
                checkAllowSelect: function(){
                	$scope.checkedRow();
                },
				pageNumberClick: function(pageNumber){
                    var prevPage = $scope.page.prevPageDisabled;
                    if (pageNumber === "prev" && prevPage && prevPage != "") {
                        return false;
                    }
                    var nextPage = $scope.page.nextPageDisabled;
                    if (pageNumber === "next" && nextPage && nextPage != "") {
                        return false;
                    }
                    if (pageNumber == $scope.page.pageNumber) {
                        return false;
                    }
                    if (pageNumber === "...") {
                        return false;
                    }
                    if (pageNumber === "prev") {
                        $scope.page.pageNumber--;
                    } else if (pageNumber === "next") {
                        $scope.page.pageNumber++;
                    } else {
                        $scope.page.pageNumber = pageNumber;
                    }
                    //重新查询一次
                    $scope.reloadData();
                }
			}
			
			function paginationHandle(data){
            	//分页数据处理
                var pages = {};
            	pages.totalRecords = data.totalResults;
                pages.pageNumber = data.currentPage;
                pages.pageSize = data.onePageSize;
                pages.totalPages = data.totalPage;
                var totalPage = pages.totalPages;

                //分页算法，页面只显示固定数量的分页按钮。
                var pageNumbers = [];
                var startPage = 1;
                var endPage = totalPage;
                var pageStep = 2;//以当前页为基准，前后各显示的页数量
                if (totalPage >= 6) {
                    startPage = pages.pageNumber;
                    if (startPage >= pageStep) {
                        startPage -= pageStep;
                    }
                    if (startPage <= 1) {
                        startPage = 1;
                    }
                    endPage = (totalPage - pages.pageNumber) >= pageStep ? pages.pageNumber + pageStep : totalPage;
                    if (endPage > totalPage) {
                        endPage = totalPage;
                    }
                    if (startPage != 1) {
                        pageNumbers.push({number: "1"});
                        if (startPage - 1 != 1) {
                            pageNumbers.push({number: "...", disabled: "disabled"});
                        }
                    }
                }
                for (var i = startPage; i <= endPage; i++) {
                    if (i == pages.pageNumber) {
                        pageNumbers.push({number: i, active: "active"});
                    } else {
                        pageNumbers.push({number: i});
                    }
                }
                if (endPage != totalPage) {
                    if (endPage + 1 != totalPage) {
                        pageNumbers.push({number: "...", disabled: "disabled"});
                    }
                    pageNumbers.push({number: totalPage});
                }
                pages.pageNumbers = pageNumbers;
                if (pages.pageNumber == 1 || pages.totalPages == 0) {
                    pages.prevPageDisabled = "disabled";
                }
                if (pages.pageNumber == totalPage || pages.totalPages == 0) {
                    pages.nextPageDisabled = "disabled";
                }
                angular.extend($scope.page,pages);
//            	setTimeout(function () {$("#"+$scope.t_id+" #footable_page_select").val($scope.table.page.pageSize);}, 20);
			}
			
			//集成ace信息展示
			$scope.showDetail = function(index,name){
				let row = $scope.data[index];
				dbUtils.openModal('tpl/templates/modal_ace.html','UiBaseTableModalAceController',"lg","modal-large",{"data":row[name],"isShowFormatter":true},function (data) {
				},true);
			}
			
			// -----------------------------日志表格全选/反选  start------------------------------------------
			$scope.thead = {};
		    //每行点击事件
		    $scope.checkedRow = function(tdata){
		    	checkAllrow($scope.rows);
		    }
		    
		    //检测是否全部都选中
		    function checkAllrow(data){
		    	var flag = true;
		        if (!data || data.length == 0) {
		            flag = false;
		        }
		        for (var i in data) {
		        	if(!data[i].checked){
		        		 flag = false;
		        	}
				}
		        $scope.thead.allrow = flag;
		    }
		    
		    //点击全选复选框事件
		    $scope.allRowCheck = function(){
	    		for (var i in $scope.rows) {
		 			$scope.rows[i].checked = $scope.thead.allrow;
	    		}
		    }
		    
		    //获取所有选中的行数据
            function getAllSelectRows() {
                var rows = [];
                angular.forEach($scope.rows, function (row,i) {
                    if (row.checked) {
                    	for(let i of $scope.data){
                    		if(row.index == i.index)
                    			rows.push(i);
                    	}
                    }
                });
                return rows;
            }
		    // -----------------------------日志表格全选/反选  end------------------------------------------
		}],
		
		link: function (scope, elem, attrs) {
			/*
			scope.$watch('$viewContentLoaded', function() {
			}); */
		},
	}
}]);

/*app.controller('UiBaseTableModalAceController',['dbUtils','$scope','$modalInstance', '$state','source',
    function (dbUtils,$scope,$modalInstance, $state,source) {

	$scope.data = source.data;
	$scope.isShowFormatter = source.isShowFormatter;
	if(!$scope.isShowFormatter)
		$scope.isShowFormatter = false;
	var editor;
	
	$scope.load = function() {
    	var eleT = angular.element('#modal_ace')[0]; 
    	if(eleT){
    		initAce();
    	}
    }
	
	function initAce(){
		editor = ace.edit('modal_ace'); //初始化对象
		editor.setTheme("ace/theme/tomorrow_night_eighties");//设置风格和语言
		editor.session.setMode("ace/mode/sql");
		editor.$blockScrolling = Infinity;
		editor.setOptions({
			enableBasicAutocompletion: true,
			enableSnippets: true,
			enableLiveAutocompletion: true
		});
		editor.setValue($scope.data);
		
	}
	
	$scope.formatter = function () {
		editor.setValue("");
		var formatterData = window.sqlFormatter.format($scope.data);
		editor.setValue(formatterData);
	};
	
	$scope.cancel = function () {
		$modalInstance.dismiss(false);
	};

}]);*/
