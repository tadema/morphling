'use strict';

app.controller('FormEditController', ['dbUtils','$scope','$modalInstance', '$state','source','$http', 
                                        function (dbUtils,$scope,$modalInstance, $state,source,$http) {
  	  $scope.title = source.title;
  	  $scope.showDetail = source.value;
  	
  	  $scope.cancel = function () {
  	      $modalInstance.dismiss(false);
  	  };
  	  
  	  $scope.submitApply = function(){
  	      $modalInstance.close($scope.showDetail);
  	  }
    
  }]);