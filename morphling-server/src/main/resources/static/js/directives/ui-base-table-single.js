angular.module('app').directive('uiBaseTableSingle', ['$rootScope', '$anchorScroll','dbUtils', '$state', '$sce','$compile','$modal','$timeout', function($rootScope, $anchorScroll,dbUtils, $state, $sce,$compile,$modal,$timeout) {
	return {
		restrict: 'EA',
		templateUrl:'tpl/templates/uiBaseTableSingle.html',
		scope: {
			//初始化{loading: true,labelTitle:?,bodyStyle:?}
			tableParams:"=?",
			isShowCheckbox: "@",
			headers: "=",
			tooltipData: "=?",//刷新后提示信息不能转码，不要再用{{}} 和ng-class形式
			operationEvents: "=?",
			rowEvents: "=?",
			page: "=?",
			urlParams: "=?",//urlParams = Object.assign(scope.urlParams.urlBody,{currentPage:scope.page.pageNumber, onePageSize:scope.page.pageSize})
			initData:"=?",//初始化数据，如果有初始化数据，就不需要再查询了,优先级高于urlParams
			beforeReload: "=?",//重载前执行函数
			afterReload: "=?",//重载后执行函数
		},
		controller: ['$scope', function ($scope,$parent, el, attrs) {		}],
		
		link: function (scope, elem, attrs) {
			//第一次页面初始化
			$timeout(function () {
				scope.coverStyle.height = $("#"+scope.t_id).height() + "px";
				if(scope.urlParams)
				    initData();
				if(hasInitData && scope.initData)
                    initData();
			})

			//监控initData的变化
			scope.$watch('initData',function (newValue,oldValue) {
				if(newValue != oldValue){
					hasInitData = true;//若定义了scope.initData，则hasInitData为true
					initData();
				}
			},true);

            /*  --------------------------------------------------
                ----------------数据初始化 start ----------------
                --------------------------------------------------*/
			let preFixSourceHeaderName = "base_table_",
                hasInitData = !scope.urlParams ? true : false;//是否有初始化数据标记
            scope.t_id = dbUtils.guid();
            scope.isShowCheckbox = (!scope.isShowCheckbox || scope.isShowCheckbox == "" || scope.isShowCheckbox == "0") ? false : true;
            scope.coverStyle = {top: "38px", height:"30px"};
            scope.rowEvents = !scope.rowEvents ? [] : scope.rowEvents;
			scope.headers = !scope.headers ? [] : scope.headers;

			if(scope.tableParams.labelTitle){
				scope.isHiddenTitle = false;
			}else{
				scope.isHiddenTitle = true;
				scope.mainStyle = {"border":"0px solid black"};
			}
			/*sort 使用footable后，自定义的排序不起作用 了*/
			scope.sort = {title : 'SID' ,desc : false};

			scope.orderIt = function(item){
				return item[scope.sort.title];
			};

			scope.headerClick = function(event,header){
				if(header.hideSort)
					return;
				let flag = !header.desc;
				for(let item of scope.headers){
					item.desc = null;
				}
				header.desc = flag;
				scope.sort.desc = header.desc;
				scope.sort.title = header.field;

				let sortParam = header.desc ? 1 : -1;
                scope.rows = scope.rows.sort(function(a, b) {
                    let headerName = preFixSourceHeaderName + scope.sort.title;
                    if("string"==typeof a[headerName]){
                    	//可转化成数字
                        if (!isNaN(Number(a[headerName])) && !isNaN(Number(b[headerName])))
                        {
							return Number(a[headerName]) == Number(b[headerName]) ? 0 : (Number(a[headerName]) > Number(b[headerName]) ? -1 * sortParam : 1 * sortParam);
                        }
                        //版本号类型
						if(headerName.indexOf("version") != -1){
							return versionSort(a[headerName], b[headerName], sortParam);
						}
						return a[headerName] == b[headerName] ? 0 : (a[headerName] > b[headerName] ? -1 * sortParam : 1 * sortParam);
                    }
                    if("number"==typeof a[headerName]){
						return Number(a[headerName]) == Number(b[headerName]) ? 0 : (Number(a[headerName]) > Number(b[headerName]) ? -1 * sortParam : 1 * sortParam);
                    }
                });
                //当前端分页时，排序之后重新设置display值
                if(scope.page && scope.page.isPagination && !scope.page.postgroundPagination)
                    scope.operations.setDataDisplay();
			}
			/*sort end*/

			/*表格页码*/
			scope.isShowFooter = false;
			if(scope.page && scope.page.isPagination){
                scope.isShowFooter = true;
                scope.pageSelect = [10,20,50,100];

				if(!scope.page.postgroundPagination)
					scope.page.postgroundPagination = false;
			}
			/*表格页码 end*/

            /*  --------------------------------------------------
                ----------------数据初始化 end ----------------
                --------------------------------------------------*/
			/*
			当出现以下情况执行
			1.urlParams存在
			（1）首次加载；（2）后台分页翻页；（3）后台pageSize改变；（4）刷新操作
			2.initData存在
			（1）首次加载；（2）后台分页翻页；（3）后台pageSize改变；(4）刷新操作
			 */
			function initData(){
				scope.beforeReload && scope.beforeReload();
				scope.tableParams.loading = true;
				$timeout(function () {scope.coverStyle.height = $("#"+scope.t_id).height() + 10 + "px";})

				if(hasInitData){//如果有传入初始数据，则不通过查询url获取数据
					scope.responseData = scope.initData;
					//绘制表格
					drawTableHandle();
				}else {
					getAction().then(function (response) {
						if (response.code == SUCCESS_CODE) {
							scope.responseData = response.data;
							//绘制表格
							drawTableHandle();
						} else {
							scope.tableParams.loading = false;
							dbUtils.error(response.message)
						}
					}, function (result) {scope.tableParams.loading = false;});
				}
			}

			//绘制表格处理
			function drawTableHandle() {
				var paginationData;//这个必须是数组数据,用来分页处理时使用
				if (scope.page && scope.page.isPagination && scope.page.postgroundPagination) {//后端分页
					paginationData = scope.responseData;
					scope.data = paginationData.data;
				}else if(scope.page && scope.page.isPagination && !scope.page.postgroundPagination){//前端分页
					let totalPage = getTotalPage(scope.responseData);
					let pageParams = {totalResults:scope.responseData.length, currentPage:scope.page.pageNumber, onePageSize:scope.page.pageSize, totalPage:totalPage};
					paginationData = Object.assign(pageParams,{data:scope.responseData});
					// scope.data = paginationData.data.slice((scope.page.pageNumber-1)*scope.page.pageSize, scope.page.pageNumber*scope.page.pageSize);
                    //前台分页渲染所有数据，通过分页的信息为显示的row设置display:block，隐藏的row设置display:none
					scope.data = paginationData.data;
				}else {
					scope.data = scope.responseData;
				}
				//处理Header
				headerHandle();
				//转换数据
				dataHandle();
				//loading消失
				scope.tableParams.loading = false;
				//分页处理
				if (scope.page && scope.page.isPagination)
					paginationHandle(paginationData);
				//加载后执行函数
				scope.afterReload && scope.afterReload();
			}

			//获取请求promise对象
			function getAction(){
				let action,urlParams = scope.urlParams.urlBody;
				if(scope.page)
					urlParams = Object.assign(scope.urlParams.urlBody,{currentPage:scope.page.pageNumber, onePageSize:scope.page.pageSize});

				if(!scope.urlParams.method || scope.urlParams.method == "get"){
					action = dbUtils.get(scope.urlParams.url, urlParams)
				}else if(scope.urlParams.method == "post"){
					action = dbUtils.post(scope.urlParams.url, urlParams)
				}else if(scope.urlParams.method == "postBody"){
					action = dbUtils.postBody(scope.urlParams.url, JSON.stringify(urlParams))
				}
				return action;
			}

			//处理Header
			function headerHandle(){
				//scope.headers已存在
				if(scope.headers && scope.headers.length > 0){
					for(let header of scope.headers){
						header.desc=null
					}
					return;
				}
				//scope.headers == []
				if(scope.data && scope.data.length > 0){
					let headers = Object.keys(scope.data[0]);
					for(let header of headers){
						scope.headers.push({field:header});
					}
				}
                for(let header of scope.headers){
                    header.desc=null
                }
			}

			//转换数据
			function dataHandle(){
				scope._rows = [];//保存转换前的数据
				scope.rows = [];//保存转换后得数据
                let startShowIndex,endShowIndex;//设置显示和隐藏的index值
                if(scope.page && scope.page.isPagination && !scope.page.postgroundPagination){
                    startShowIndex = (scope.page.pageNumber-1)*scope.page.pageSize;
                    endShowIndex = scope.page.pageNumber*scope.page.pageSize;
                }
				for(let i in scope.data){
					let _row = Object.assign({},scope.data[i]);
					scope._rows.push(_row);
					if(!_row.id)
						_row.id = i;
					_row.index = i;
					let row = {index:_row.index, id:_row.id};
					//如果前端分页，则设置display属性
                    if(scope.page && scope.page.isPagination && !scope.page.postgroundPagination){
                        scope.operations.setRowDisplay(startShowIndex,endShowIndex,row);
                    }
					for(let header of scope.headers){
						var value = _row[header.field] ? _row[header.field] : (_row[header.field]==0 ? 0 : "");
						row[preFixSourceHeaderName+header.field] = value;//在排序时使用
						if(header.formatter){
							value = header.formatter(value,_row,row);//第三个row是当afterReload中增加排序属性时使用的
						}
						if(header.compile && !header.directive){
							value += "";
							value = $sce.trustAsHtml(value);
						}
						row[header.field] = value;
					}
					scope.rows.push(row);
				}
			}

			function getTotalPage(data){
				if(!data || data.length == 0)
					return 1;
				let totalPage;
				let i = parseInt(data.length / scope.page.pageSize);//整数部分
				let j = data.length % scope.page.pageSize;//余数部分
				if(i == 0){
					totalPage = ++i;
				}else if(i > 0 && j == 0){
					totalPage = i;
				}else{
					totalPage = ++i;
				}
				return totalPage;
			}

			//监控父类发送的刷新
			scope.$on('refreshData', function(event,data) {scope.operations.refreshData();});
			scope.$on('reloadData', function(event,data) {scope.operations.refreshData();});

			//获取选中的数据
			scope.$on('getSelectedData', function(event,data) {
				scope.$emit('sendSelectedData',getAllSelectRows());
			});

			//获取选中的数据，并且返回入参数据
			scope.$on('getSelectedItem', function(event,data) {
				scope.$emit('sendSelectedItem',Object.assign(data,{rows:getAllSelectRows()}));
			});

            //监听每页数量切换
            /*scope.$watch('page.pageSize',function(current,old){
                if(current != old){                }
            })*/

            //监听表格首次渲染完成以及表格pageSize变大的情况，其他情况不监听
            /*scope.$on("TableLoadOver",function(){
            })*/

			scope.operations = {
				emit: function (event,data) {
					scope.$emit(event,data);
				},
				refreshData: function(){
					if(scope.page && scope.page.isPagination){
						scope.page.pageNumber = 1;
					}
					//发射刷新信号
					if(hasInitData)
						scope.$emit("baseTableRefreshData",{});
					else
						initData();
				},
                //设置每行的显示、隐藏
                setRowDisplay: function(startShowIndex,endShowIndex,row){
				    let index = parseInt(row.index);
				    if(index >= startShowIndex && index < endShowIndex){
                        row.style = {"display":"table"};
                    }else{
                        row.style = {"display":"none"};
                    }
                },
                //设置所有数据的显示、隐藏
                setDataDisplay:function(){
				    if(!scope.page || !scope.page.isPagination || scope.page.postgroundPagination){
				        return;
                    }
                    //设置显示和隐藏的index值
                    let startShowIndex = (scope.page.pageNumber-1)*scope.page.pageSize,
                        endShowIndex = scope.page.pageNumber*scope.page.pageSize;

                    for(let i in scope.rows){
                        scope.rows[i].index = i;
                        this.setRowDisplay(startShowIndex,endShowIndex,scope.rows[i]);
                    }
                },
             	// grid上方按钮点击事件
				operationButtonClick: function(clickFun){
					var rows = getAllSelectRows();
					clickFun(rows);
					scope.checkedRow();
				},
				checkAllowSelect: function(){
					scope.checkedRow();
				},
				//pageSize变更事件
				pageSizeChange: function(){
					if(scope.page && scope.page.isPagination && scope.page.postgroundPagination){//后台分页
						if(hasInitData)
							this.emit("baseTablePageChangeHandle",{pageNumber: scope.page.pageNumber});
						else
							initData();
					}else if(scope.page && scope.page.isPagination && !scope.page.postgroundPagination){//前台分页
						scope.page.pageNumber = 1;
						frontPaginationHandle();
					}
				},
				//页码变更事件
				pageNumberClick: function(pageNumber){
					var prevPage = scope.page.prevPageDisabled;
					if (pageNumber === "prev" && prevPage && prevPage != "") {
						return false;
					}
					var nextPage = scope.page.nextPageDisabled;
					if (pageNumber === "next" && nextPage && nextPage != "") {
						return false;
					}
					if (pageNumber == scope.page.pageNumber) {
						return false;
					}
					if (pageNumber === "...") {
						return false;
					}
					if (pageNumber === "prev") {
						scope.page.pageNumber--;
					} else if (pageNumber === "next") {
						scope.page.pageNumber++;
					} else {
						scope.page.pageNumber = pageNumber;
					}
					//重新查询一次
					if(scope.page && scope.page.isPagination && scope.page.postgroundPagination) {//后台分页
						if(hasInitData)
							this.emit("baseTablePageChangeHandle",{pageNumber: scope.page.pageNumber});
						else
							initData();
					}else if(scope.page && scope.page.isPagination && !scope.page.postgroundPagination)//前台分页
						frontPaginationHandle();
				}
			}

			/*
				前台翻页、页码变换事件处理
			 */
			function frontPaginationHandle(){
				let totalPage = getTotalPage(scope.responseData);
				let pageParams = {totalResults:scope.responseData.length, currentPage:scope.page.pageNumber, onePageSize:scope.page.pageSize, totalPage:totalPage};
				let paginationData = Object.assign(pageParams,{data:scope.responseData});
				/*scope.data = paginationData.data.slice((scope.page.pageNumber-1)*scope.page.pageSize, scope.page.pageNumber*scope.page.pageSize);
				//转换数据
				dataHandle();*/
				scope.operations.setDataDisplay();
				//分页处理
				paginationHandle(paginationData);
				//加载后执行函数
				scope.afterReload && scope.afterReload();
			}

			//页码处理
			function paginationHandle(data){
				var pages = {};
				pages.totalRecords = parseInt(data.totalResults);
				pages.pageNumber = parseInt(data.currentPage);
				pages.pageSize = parseInt(data.onePageSize);
				pages.totalPages = parseInt(data.totalPage);
				var totalPage = pages.totalPages;

				//分页算法，页面只显示固定数量的分页按钮。
				var pageNumbers = [];
				var startPage = 1;
				var endPage = totalPage;
				var pageStep = 2;//以当前页为基准，前后各显示的页数量
				if (totalPage >= 6) {
					startPage = pages.pageNumber;
					if (startPage >= pageStep) {
						startPage -= pageStep;
					}
					if (startPage <= 1) {
						startPage = 1;
					}
					endPage = (totalPage - pages.pageNumber) >= pageStep ? pages.pageNumber + pageStep : totalPage;
					if (endPage > totalPage) {
						endPage = totalPage;
					}
					if (startPage != 1) {
						pageNumbers.push({number: "1"});
						if (startPage - 1 != 1) {
							pageNumbers.push({number: "...", disabled: "disabled"});
						}
					}
				}
				for (var i = startPage; i <= endPage; i++) {
					if (i == pages.pageNumber) {
						pageNumbers.push({number: i, active: "active"});
					} else {
						pageNumbers.push({number: i});
					}
				}
				if (endPage != totalPage) {
					if (endPage + 1 != totalPage) {
						pageNumbers.push({number: "...", disabled: "disabled"});
					}
					pageNumbers.push({number: totalPage});
				}
				pages.pageNumbers = pageNumbers;
				if (pages.pageNumber == 1 || pages.totalPages == 0) {
					pages.prevPageDisabled = "disabled";
				}
				if (pages.pageNumber == totalPage || pages.totalPages == 0) {
					pages.nextPageDisabled = "disabled";
				}
				scope.page.prevPageDisabled = pages.prevPageDisabled;
				scope.page.nextPageDisabled = pages.nextPageDisabled;
				angular.extend(scope.page,pages);
			}

			//版本号排序
			function versionSort(a, b, sortParam){
				var arr1 = a.split('.');
				var arr2 = b.split('.');
				//将两个版本号拆成数字
				var minL = Math.min(arr1.length,arr2.length);
				var pos = 0;        //当前比较位
				var diff = 0;        //当前为位比较是否相等

				//逐个比较如果当前位相等则继续比较下一位
				while(pos < minL){
					if (!isNaN(Number(arr1[pos])) && !isNaN(Number(arr2[pos])))
						diff = parseInt(arr1[pos]) - parseInt(arr2[pos]);
					else
						diff = arr1[pos] == arr2[pos] ? 0 : (arr1[pos] > arr2[pos] ? 1 : -1);
					if(diff != 0)
						break;
					pos++;
				}
				return diff >= 0 ? -1 * sortParam : 1 * sortParam;
			}

			// -----------------------------日志表格全选/反选  start------------------------------------------
			scope.thead = {};
			//每行点击事件
			scope.checkedRow = function(row){
				let flag = row.checked;
				for (let i=0; i<scope.rows.length; i++) {
					scope.rows[i].checked = false;
				}
				row.checked = flag;
			}

			//获取所有选中的行数据
			function getAllSelectRows() {
				var rows = [];
				angular.forEach(scope.rows, function (row,i) {
					if (row.checked) {
						for(let i=0;i<scope._rows.length;i++){
							if(row.id == scope._rows[i].id)
								rows.push(scope._rows[i]);
						}
					}
				});
				return rows;
			}
			// -----------------------------日志表格全选/反选  end------------------------------------------
		},
	}
}]);