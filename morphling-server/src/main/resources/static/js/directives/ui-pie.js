angular.module('app').directive('uiPie', ['$rootScope', 'dbUtils', '$anchorScroll', function($rootScope, dbUtils, $anchorScroll) {
	return {
		restrict: 'EA',
		templateUrl:'tpl/templates/uiPie.html',
		scope: {
			title:"@",
			headers: "@",
			props: "@",
			data:"@",
		},
		controller: ['$scope', function ($scope, el, attrs) {
			$scope.id = dbUtils.guid();
			$scope.$watch('data', function (newVal,old) {
				if(newVal != old && old == ""){
					$scope.data = JSON.parse(newVal);
					$scope.headers = Object.keys($scope.data[0]);
				}
			},true);
			$scope.option = {
				    title: {
				        text: 'Customized Pie',left: 'center',top: 20,textStyle: {color: '#ccc'}
				    },
				    tooltip : {
				        trigger: 'item',formatter: "{a} <br/>{b} : {c} ({d}%)"
				    },
				    visualMap: {
				        show: false,min: 80,max: 600,inRange: {colorLightness: [0, 1]}
				    },
				    series : [
				        {
				            name:'访问来源',
				            type:'pie',
				            radius : '55%',
				            center: ['50%', '50%'],
				            data:[
				                {value:335, name:'直接访问'},
				                {value:310, name:'邮件营销'},
				                {value:274, name:'联盟广告'},
				                {value:235, name:'视频广告'},
				                {value:400, name:'搜索引擎'}
				            ].sort(function (a, b) { return a.value - b.value; }),
				            roseType: 'radius',
				            label: {
				                normal: {
				                    textStyle: {color: 'rgba(255, 255, 255, 0.3)'}
				                }
				            },
				            labelLine: {
				                normal: {
				                    lineStyle: {color: 'rgba(255, 255, 255, 0.3)'},smooth: 0.2,length: 10,length2: 20
				                }
				            },
				            itemStyle: {
				                normal: {
				                    color: '#c23531',shadowBlur: 200,shadowColor: 'rgba(0, 0, 0, 0.5)'
				                }
				            },
				            animationType: 'scale',
				            animationEasing: 'elasticOut',
				            animationDelay: function (idx) {
				                return Math.random() * 200;
				            }
				        }
				    ]
				};
		}],
		link: function (scope, elem, attrs) {
			scope.$watch('$viewContentLoaded', function() {  
				let myChart = echarts.init(document.getElementById(scope.id));
				myChart.setOption(scope.option);
			}); 
		},
	}
}]);