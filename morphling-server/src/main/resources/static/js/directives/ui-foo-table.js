
'use strict';

var fooTableDirectives = angular.module('ui.components.footable', ['dbUtils']);
fooTableDirectives.directive('uiFooTable', ['dbUtils', 'dateFilter', '$compile','$sce',function (dbUtils, dateFilter,$compile,$sce) {
    return {
        restrict: 'E',
        templateUrl: "tpl/templates/uiFooTable.html",
        replace: true,
        transclude:true,
        controller: ['$scope', function ($scope) {
            //var _tobecompile = [];
            //queryForm
            if (angular.isUndefined($scope.table)) {
                return;
            }
            if(angular.isUndefined($scope.table.settings)){
                $scope.table.settings={};
            }
            if(angular.isUndefined($scope.table.settings.buttomAlign)){
                $scope.table.settings.buttomAlign="center";
            }
            if(angular.isUndefined($scope.table.method)){
                $scope.table.method = "get";
            }
            if(angular.isUndefined($scope.table.queryParams)){
            	$scope.table.queryParams = {};
            }
            if($scope.table.params){
            	if($scope.table.method == "get"){
            		angular.forEach($scope.table.params, function (field) {
            			let value = !field.initValue ? "" : field.initValue;
            			$scope.table.queryParams[field.name] = value;
            		});
            	}else{
            		$scope.table.queryParams = $scope.table.params;
            	}
            }
            //路径条的初始化
            if(angular.isDefined($scope.pathObjs) && angular.isDefined($scope.pathNav)){
            	var strTemp = $scope.pathObjs[$scope.pathNav.index].path; 
            	var indexT = parseInt(strTemp.length) - 1;
            	if(strTemp.charAt(indexT) == "/"){
            		$scope.pathObjs[0].path = strTemp.substr(strTemp.length-1,1);
            	}
            	$scope.pathNav.prevPath = $scope.pathObjs[0].path;
            	$scope.pathNav.currPath = $scope.pathObjs[0].path;
            }
            //计算按钮组的宽度
            if(angular.isUndefined($scope.table.settings.leftButton)){
                $scope.table.settings.leftButton=false;
            }
            if(angular.isUndefined($scope.table.showUpload)){
                $scope.table.showUpload=false;
            }
            if($scope.table.settings.leftButton == true && $scope.table.showUpload == true){
            	$scope.table.settings.widthClass = 0;
            }else if($scope.table.settings.leftButton == false && $scope.table.showUpload == true){
            	$scope.table.settings.widthClass = 1;
            }else {
            	$scope.table.settings.widthClass = 2;
            }
            var bottom = new Array();
    		for (var i in $scope.table.rowEvents){
    	    	var obj = new Object();
    	    	obj = $scope.table.rowEvents[i];
    	    	if(obj.title){}else{obj.title = obj.name;}
    	    	bottom.push(obj);
    		}
    		$scope.table.rowEvents = bottom;
            
            //$scope.table.pageSql = false;为前台;true为后台分页
            if(angular.isUndefined($scope.table.autoLoad) || $scope.table.autoLoad){
            	
                if(!$scope.table.pageSql){
                	$scope.table.pageSql = false;
                	$scope.table.pageUI = true;
                }else if($scope.table.pageSql == true ){
                	$scope.table.pageSql = true;
                	$scope.table.pageUI = false;
                }else{
                	$scope.table.pageSql = false;
                	$scope.table.pageUI = true;
                }
                //定义grid的Page对象
                $scope.table.page={
                		pageNumber: 1,
                		pageSize: 20,//$scope.table.settings.pageSize || 10,
                		prevPageDisabled: 'disabled',
                		nextPageDisabled: 'disabled'
                };
                $scope.table.pageSelect = [10,15,20,50,100];
                queryData();
            }
            
            function queryData(){
                $scope.table.rows = [];
                $scope.table._rows = [];
                $scope.table.beforeReload && $scope.table.beforeReload();
                $scope.loading = true;
//                var queryParams = $scope.table.params || {};
                var queryParams = angular.copy($scope.table.queryParams);
                if($scope.table.pageSql == true ){
                	queryParams['currentPage'] = $scope.table.page.pageNumber;
                    queryParams['onePageSize'] = $scope.table.page.pageSize;
                }
                if($scope.table.method == "get"){
                	dbUtils.get($scope.table.url,queryParams).then(callback,errorCallback)
                }else if($scope.table.method == "postBody"){
                	dbUtils.postBody($scope.table.url,JSON.stringify(queryParams.data)).then(callback,errorCallback);
                }
            }
            //错误回调
            function errorCallback(response){
            	$scope.loading = false;
            }
            //正确回调
            function callback(response){
                //_tobecompile = [];
                if(response.code != SUCCESS_CODE){
                    dbUtils.warning(response.message,"提示");
                    $scope.loading = false;
                    return;
                }
                var data = response.data;//这个必须是数组数据
//                $scope.table._rows = data;

                if($scope.table.pageSql == true ){
                	$scope.table._rows = data.data;
                }else{
                	$scope.table._rows = data;
                } 
                for(var i in $scope.table._rows){
                    var _row = $scope.table._rows[i];
                    var row = {};
                    for(var j in $scope.table.headers){
                        var header = $scope.table.headers[j];
                        var value;
                        if(typeof(_row[header.field]) == "undefined")
                            value = "";
                        else
                            value = _row[header.field] + '';
                        if(header.formatter){
                        	if(header.getI){
                        		 value = header.formatter(value,_row,i);
                        	}else{
                                 value = header.formatter(value,_row);
                        	}
                        }
                        if(header.compile && !header.directive){
                        	value += "";
                            value = $sce.trustAsHtml(value);
                        }
                        row[header.field] = value;
                    }
                    //添加一个是否选择的字段，默认值为false
                    row.checked = $scope.table.settings.allRowChecked ? $scope.table.settings.allRowChecked : false;
                    $scope.table.rows.push(row);
                }
//                $("#table").footable();
//                $('#table').trigger('footable_initialized');
//                $('.table').trigger('footable_redraw');
//                setTimeout(function () {  $('.table').trigger('footable_redraw');}, 100);
               /* $timeout(function(){
                    $('.table').trigger('footable_redraw');
                }, 300);*/
                
                if($scope.table.pageSql == true ){
                	//分页数据处理
                    var pages = {};
                	pages.totalRecords = data.totalResults;
                    pages.pageNumber = data.currentPage;
                    pages.pageSize = data.onePageSize;
                    pages.totalPages = data.totalPage;
                    var totalPage = pages.totalPages;

                    //分页算法，页面只显示固定数量的分页按钮。
                    var pageNumbers = [];
                    var startPage = 1;
                    var endPage = totalPage;
                    var pageStep = 2;//以当前页为基准，前后各显示的页数量
                    if (totalPage >= 6) {
                        startPage = pages.pageNumber;
                        if (startPage >= pageStep) {
                            startPage -= pageStep;
                        }
                        if (startPage <= 1) {
                            startPage = 1;
                        }
                        endPage = (totalPage - pages.pageNumber) >= pageStep ? pages.pageNumber + pageStep : totalPage;
                        if (endPage > totalPage) {
                            endPage = totalPage;
                        }
                        if (startPage != 1) {
                            pageNumbers.push({number: "1"});
                            if (startPage - 1 != 1) {
                                pageNumbers.push({number: "...", disabled: "disabled"});
                            }
                        }
                    }
                    for (var i = startPage; i <= endPage; i++) {
                        if (i == pages.pageNumber) {
                            pageNumbers.push({number: i, active: "active"});
                        } else {
                            pageNumbers.push({number: i});
                        }
                    }
                    if (endPage != totalPage) {
                        if (endPage + 1 != totalPage) {
                            pageNumbers.push({number: "...", disabled: "disabled"});
                        }
                        pageNumbers.push({number: totalPage});
                    }
                    pages.pageNumbers = pageNumbers;
                    if (pages.pageNumber == 1 || pages.totalPages == 0) {
                        pages.prevPageDisabled = "disabled";
                    }
                    if (pages.pageNumber == totalPage || pages.totalPages == 0) {
                        pages.nextPageDisabled = "disabled";
                    }
                    $scope.table.page = pages;
                }

                $scope.loading = false;
                $scope.table.afterReload && $scope.table.afterReload();
            }

            $scope.$watch('table.page.pageSize',function(current,old){
            	if(current != old){
            		$("#"+$scope.t_id+" #footable").attr("data-page-size",current);//动态修改table的pageSize的值
            		if($scope.table.pageSql == true ){
            			//前台分页时，改变每页数量时执行下面的代码会报错
            			setTimeout(function () {
            				$("#"+$scope.t_id+" #footable").footable();//重新加载表格
            			}, 100);
            		}
            	}
            })
            
            $scope.$on("TableLoadOver",function(){
            	$scope.loading = false;
            	if(!$scope.table.pageSql){
//                    	setTimeout(function () {  $('.table').trigger('footable_redraw');}, 100);//放开会导致问题：后台分页选择其他页码后排序，再点击前台分页会报sort的问题，改成下面的方式可以
            		setTimeout(function () {  $("#"+$scope.t_id+" #footable").trigger('footable_redraw');}, 100);
            	} 
            })
            
            //刷新表格状态
            $scope.refreshStatus = {isClick:false,buttonClass:"btn-danger"};
            function refreshStatus(){
            	$scope.refreshStatus.isClick = !$scope.refreshStatus.isClick;
            	$scope.timeId;
            	if($scope.refreshStatus.isClick == true){
            		$scope.refreshStatus.buttonClass = "btn-success";
            		$scope.refreshStatus.iClass = "fa-spin";
            		$scope.timeId = window.setInterval(queryStatus,3000);
            	}else{
            		$scope.refreshStatus.buttonClass = "btn-danger";
            		$scope.refreshStatus.iClass = "";
            		if($scope.timeId)
            			window.clearInterval($scope.timeId);
            	}
            }
            
            function queryStatus(){
            	let ipPortStatue = {};
            	// for(let row of $scope.table._rows){
            	for(let i=0;i<$scope.table._rows.length;i++){
                    let row = $scope.table._rows[i];
                    ipPortStatue[row.host+"_"+row.port] = 0;
            	}
            	dbUtils.postBody("/app/checkServerStatus",JSON.stringify(ipPortStatue)).then(function(response){
            		if(response.code == SUCCESS_CODE){
            			ipPortStatue = {};
            			ipPortStatue = response.data; 
            			for(let i in $scope.table._rows){
            				let _row = $scope.table._rows[i];
            				let row = $scope.table.rows[i];
            				for(let header of $scope.table.headers){
            					if(header.field == "healthStatus" || header.field == "status"){//
            						_row.status = ipPortStatue[_row.host+"_"+_row.port];
            						var value = '';
            						if(header.formatter){
            							if(header.getI){
            								value = header.formatter(value,_row,i);
            							}else{
            								value = header.formatter(ipPortStatue[_row.host+"_"+_row.port],_row);//会执行formatter中的查询方法
            							}
            						}
            						if(header.compile && !header.directive){
            							value += "";
            							value = $sce.trustAsHtml(value);
            						}
            						if(row)
            							row[header.field] = value;
            					}
            				}
            			}
            			$scope.table.afterReload && $scope.table.afterReload();
            		}else{
            			dbUtils.error(response.message,"提示");
            		}
            	})
            }

            $scope.table.operations = {
                reloadData: function(){
                	$scope.table.queryParams = {};
                	if($scope.table.method == "get"){
                		angular.forEach($scope.table.params, function (field) {
                			let value = !field.initValue ? "" : field.initValue;
                			$scope.table.queryParams[field.name] = value;
                		});
                	}else{
                		$scope.table.queryParams = $scope.table.params;
                	}
                    queryData();
                },
                refreshStatus: function(){
                	refreshStatus();
                },
                refreshData: function(){
                	if($scope.table.pageSql == true ){
                		$scope.table.page.pageNumber = 1;
                    }
                	if($scope.table.method == "get"){
                		angular.forEach($scope.table.params, function (field) {
                			if(field.initValue)
                				$scope.table.queryParams[field.name] = field.initValue;
                		});
                	}else{
                		$scope.table.queryParams = $scope.table.params;
                	}
                    queryData();
                },
                enterEvent: function(e) {
                    var keycode = window.event ? e.keyCode : e.which;
                    if(keycode == 13){
                        this.refreshData();
                    }
                },
                resetQuery: function(){
                    for(var p in $scope.table.queryParams){
                        $scope.table.queryParams[p] = '';
                    }
                    $scope.table.filterText = '';
                    this.refreshData();
                },
                cancelTime: function(){
            		$scope.customTimeFlag = false;
            		$scope.table.startTime = '';
            		$scope.table.endTime = '';
            	},
            	submitTime: function(){
            		var timeArr = [];
            		if($scope.table.startTime == '' || $scope.table.startTime == ' ' || $scope.table.startTime == null){
            			dbUtils.warning("起始时间不能为空","提示");
            			return;
            		}else{
            			timeArr.push($scope.table.startTime);
            		}
            		
            		if($scope.table.endTime == '' || $scope.table.endTime == ' ' || $scope.table.endTime == null){
            			dbUtils.warning("结束时间不能为空","提示");
            			return;
            		}else{
            			timeArr.push($scope.table.endTime);
            		}
            		if($scope.table.startTime > $scope.table.endTime){
            			dbUtils.warning("起始时间不能大于结束时间","提示");
            			return;
            		}
            		$scope.$broadcast('timeMonitor', timeArr);
            	},
            	//路径导航点击事件
            	pathNavClickHandle: function(event,r_index,obj){
            		if($scope.pathNav.index == r_index)
            			return;
            		$scope.pathNav.prevPath = $scope.pathObjs[$scope.pathNav.index].path;
            		$scope.pathNav.index = r_index;
            		$scope.pathObjs.splice($scope.pathNav.index+1, $scope.pathObjs.length-$scope.pathNav.index-1);//slice是深复制
            		$scope.pathNav.currPath = $scope.pathObjs[$scope.pathNav.index].path;
            		pathNavChangeHandle();
                },
                //路径导航下一级请求事件
            	pathNavNextHandle: function(row){
            		var fileName = row.fileName;
                	$scope.pathNav.prevPath = $scope.pathObjs[$scope.pathNav.index].path;
            		$scope.pathNav.index++;
            		$scope.pathObjs.push(row);
            		$scope.pathNav.currPath = $scope.pathNav.currPath + "/" + fileName;
            		$scope.pathObjs[$scope.pathNav.index].path = $scope.pathNav.currPath; 
            		$scope.pathObjs[$scope.pathNav.index].name = fileName;
            		pathNavChangeHandle();
                },
                //路径导航的状态切换
                pathNavStatusHandle: function(){
            		for(var i=0;i<$scope.pathObjs.length;i++){
            			if(i == $scope.pathObjs.length-1){
            				$scope.pathObjs[i].class = "a-disabled";
            			}else{
            				$scope.pathObjs[i].class = "a-able";
            			}
            		}
            	},
                //每行点击事件
                checkedRow: function(row){
                    row.checked = !row.checked;
                    checkAllowSelect();
                },
                //点击全选复选框事件
                allRowChecked: function(){
                    $scope.table.settings.allRowChecked = !$scope.table.settings.allRowChecked;
                    angular.forEach($scope.table.rows, function (row) {
                        row.checked = $scope.table.settings.allRowChecked;
                    });
                },
                //grid上方按钮点击事件
                operationButtonClick: function(clickFun){
                    var rows = getAllSelectRows();
                    clickFun(rows);
                    checkAllowSelect();
                },
                checkAllowSelect: function(){
                	checkAllowSelect();
                },
                //分页数量点击事件
                pageNumberClick: function(pageNumber){
                    var prevPage = $scope.table.page.prevPageDisabled;
                    if (pageNumber === "prev" && prevPage && prevPage != "") {
                        return false;
                    }
                    var nextPage = $scope.table.page.nextPageDisabled;
                    if (pageNumber === "next" && nextPage && nextPage != "") {
                        return false;
                    }
                    if (pageNumber == $scope.table.page.pageNumber) {
                        return false;
                    }
                    if (pageNumber === "...") {
                        return false;
                    }
                    if (pageNumber === "prev") {
                        $scope.table.page.pageNumber--;
                    } else if (pageNumber === "next") {
                        $scope.table.page.pageNumber++;
                    } else {
                        $scope.table.page.pageNumber = pageNumber;
                    }
                    queryData();
                },

            };
            function checkAllowSelect() {
                if (!$scope.table.settings.showCheckBox) {
                    return;
                }
                //如果所有行数据为非选中状态，则全选按钮为非选中状态，反之一样
                var flag = true;
                if (!$scope.table.rows || $scope.table.rows.length == 0) {
                    flag = false;
                }
                $scope.selectedRows = new Array();
                angular.forEach($scope.table.rows, function (row) {
                    if (!row.checked) {
                        flag = false;
                    }else{
                    	$scope.selectedRows.push(row);
                    }
                });
                $scope.table.settings.allRowChecked = flag;
            }

            //获取所有选中的行数据
            function getAllSelectRows() {
                var rows = [];
                angular.forEach($scope.table.rows, function (row,i) {
                    if (row.checked) {
                        rows.push($scope.table._rows[i]);
                    }
                });
                return rows;
            }
            //事件处理
            $scope.publishEvent = function(){
//                console.log('publish event...');
                var args = [];
                for(var i = 0;i<arguments.length;i++){
                    args.push(arguments[i]);
                }
                $scope.$emit.apply(this,args);
            }
            
            //路径导航地址变化处理
            function pathNavChangeHandle(){
            	$scope.table.operations.pathNavStatusHandle();
        		$scope.table.url = $scope.pathNav.reloadUrl+$scope.pathNav.currPath;
        		$scope.table.params = $scope.pathNav.reloadParams;
                $scope.table.operations.reloadData();
            }
            
            //上传
            $scope.changeFile = function(event) {
            	let e = event.target;
            	$scope.clicklefile = true;
            	$(e).closest('div').find('.fileFoo').click();
            	$(e).closest('div').find('.fileFoo').change(function(){
            		if($scope.clicklefile){
            			lefileChange(e);
            			$scope.clicklefile = false;
            		}
            	});
        	}; 
        	
        	function lefileChange(e){
//        		var $file = $('input[id=fileFoo]');
        		var $file = $(e).closest('div').find('.fileFoo');
                var fileObj = $file[0];
                var windowURL = window.URL || window.webkitURL;
                var dataURL;
                if (fileObj && fileObj.files && fileObj.files[0]) {	      
                	dataURL = windowURL.createObjectURL(fileObj.files[0]);	            
                } else {	            
                	dataURL = $file.val();	
                }
                let file_name = $file.val();
                if(file_name.indexOf("C:\\fakepath\\") != -1){
                	file_name = file_name.substring(12);
                }
                $(e).closest('div').find('.demoFoo')[0].innerHTML = file_name;
        	}
        	
        	$scope.uploadFileFoo = function(event){
        		let e = event.target;
                var file = $(e).closest('div').siblings('.fileFoo')[0].files[0];
                if(file == null || file == 'undefined'){
            		dbUtils.warning("请先选择文件！","提示");
            		return;
               	}
                $scope.uploadHandle(file);
        	}
        }],
        link: function (scope, element, attrs, controller) {

        }
    }

}]);
/*
fooTableDirectives.directive('listenFooTableFinish', ['$timeout', function ($timeout) {
    return {
        restrict:"A",
        link: function(scope,element,attr) {
            if (scope.$last === true) {
                $timeout(function(){
                    scope.$emit("fooTableLoadOver")
                });
            }
        }
    };
}])*/
