angular.module('app').directive('line01', ['$rootScope', 'dbUtils', '$anchorScroll','$timeout', function($rootScope, dbUtils, $anchorScroll,$timeout) {
	//支持action和initData两种模式
	return {
		restrict: 'EA',
		templateUrl:'tpl/templates/chartTemplate.html',
		scope: {
			labelTitle:"@?",
			props: "=?",
			bodyStyle: "@?",
			classGlobal: "=?",//全局样式
			methods: "=?",
			urlParams: "=?",//urlParams = Object.assign(scope.urlParams.urlBody,{currentPage:scope.page.pageNumber, onePageSize:scope.page.pageSize})
			initData:"=?",//初始化数据，如果有初始化数据，就不需要再查询了,优先级高于urlParams
		},
		controller: ['$scope', function ($scope, el, attrs) {	}],

		link: function (scope, elem, attrs) {
			//第一次页面初始化
			$timeout(function () {
				let widthValue = $("#"+scope.id).css("width");
				if(!widthValue)
					return;
				if(widthValue.lastIndexOf("px") != -1){
					$("#"+scope.id).width($("#"+scope.id).width());
				}else if(widthValue.lastIndexOf("%") != -1){
					let percent = $("#"+scope.id).parent().parent().parent().width();
					let tWidth = ($rootScope._screenProp.contentWidth-90)*parseInt($("#"+scope.id).width())*percent/10000;
					$("#"+scope.id).width(tWidth);
				}

				scope.myChart = echarts.init(document.getElementById(scope.id));

				if(scope.urlParams)
					initData();
				if(hasInitData && scope.initData)
					initData();
			})

			//监控initData的变化
			scope.$watch('initData',function (newValue,oldValue) {
				if(newValue != oldValue){
					hasInitData = true;//若定义了scope.initData，则hasInitData为true
					initData();
				}
			},true);

			/*  --------------------------------------------------
                ----------------数据初始化 start ----------------
                --------------------------------------------------*/
			let hasInitData = !scope.urlParams ? true : false;//是否有初始化数据标记
			scope.id = dbUtils.guid();
			if(scope.bodyStyle){
				scope.bodyStyle = JSON.parse(scope.bodyStyle);
			}else{
				scope.bodyStyle = {height:"300px"}
			}
			scope.myChart;

			scope.methods = {
				refreshData: function () {
					//发射刷新信号
					if(hasInitData)
						scope.$emit("barRefreshData",{});
					else
						initData();
				},
				reloadData: function () {
					initData();
				},
				setLegendSelect: function(data){
					if(scope.myChart){
						scope.option.legend.selected = data;
						scope.myChart.setOption(scope.option);
					}
				},
				clear: function () {
					if(scope.myChart){
						scope.myChart.clear();
						scope.option.series = [];
						scope.myChart.setOption(scope.option);
					}
				}
			}
			/*  --------------------------------------------------
                ----------------数据初始化 end ----------------
                --------------------------------------------------*/

			function initData(){
				scope.loading = true;
				if(hasInitData){//如果有传入初始数据，则不通过查询url获取数据
					scope.responseData = scope.initData;
					drawViewHandle();
				}else {
					getAction().then(function (response) {
						if (response.code == SUCCESS_CODE) {
							scope.responseData = response.data;
							//绘制表格
							drawViewHandle();
						} else {
							scope.loading = false;
							dbUtils.error(response.message)
						}
					}, function (result) {
						scope.loading = false;
						dbUtils.error(response.message)
					});
				}
			}

			//获取请求promise对象
			function getAction(){
				let action,urlParams = scope.urlParams.urlBody;

				if(!scope.urlParams.method || scope.urlParams.method == "get"){
					action = dbUtils.get(scope.urlParams.url, urlParams)
				}else if(scope.urlParams.method == "post"){
					action = dbUtils.post(scope.urlParams.url, urlParams)
				}else if(scope.urlParams.method == "postBody"){
					action = dbUtils.postBody(scope.urlParams.url, JSON.stringify(urlParams))
				}
				return action;
			}

			//绘制图表
			function drawViewHandle(){
				$timeout(function () {
					scope.loading = false;
				},100)

				if(scope.props && scope.props.legend && Object.keys(scope.props.legend).length != 0)
					Object.assign(scope.option.legend,scope.props.legend);

				if(scope.props && scope.props.xAxis && Object.keys(scope.props.xAxis).length != 0)
					Object.assign(scope.option.xAxis[0],scope.props.xAxis);

				if(scope.props && scope.props.yAxis && Object.keys(scope.props.yAxis).length != 0){
					Object.assign(scope.option.yAxis[0],scope.props.yAxis);
				}

				if(scope.props && scope.props.series){
					scope.option.series = [];
					for(let i=0;i<scope.props.series.length;i++){
						let tempSeries = {
							type: "line",smooth: true,symbolSize: 8,zlevel: 3,lineStyle: {normal: {color: colorList[i%6],}},
							/*areaStyle: {
								normal: {
									color: new echarts.graphic.LinearGradient(0,0,0,1,
										[
											{offset: 0,color: hexToRgba(colorList[i%6], 0.3)},
											{offset: 1,color: hexToRgba(colorList[i%6], 0.1)}
										],
										false
									),
									shadowColor: hexToRgba(colorList[i%6], 0.1),shadowBlur: 10
								}
							},*/
						}
						Object.assign(tempSeries, scope.props.series[i]);
						scope.option.series.push(tempSeries);
					}
				}
				if(!scope.myChart)
					scope.myChart = echarts.init(document.getElementById(scope.id));
				scope.myChart.setOption(scope.option);
			}

			//监控父类发送的刷新
			scope.$on('refreshBarData', function(event,data) {methods.refreshData();});
			scope.$on('reloadBarData', function(event,data) {methods.refreshData();});

			// var colorList=['#afa3f5', '#00d488', '#3feed4', '#3bafff', '#f1bb4c','#97FFFF'];
			// var colorList=["#2ec7c9","#b6a2de","#5ab1ef","#ffb980","#d87a80","#8d98b3","#e5cf0d","#97b552","#95706d","#dc69aa","#07a2a4","#9a7fd1","#588dd5","#f5994e",
			// 				"#c05050","#59678c","#c9ab00","#7eb00a","#6f5553","#c14089"];
			// var colorList=["#c12e34","#e6b600","#0098d9","#2b821d","#005eaa","#339ca8","#cda819","#32a487"];
			// var colorList=["#fc97af",
			// 	"#87f7cf",
			// 	"#f7f494",
			// 	"#72ccff",
			// 	"#f7c5a0",
			// 	"#d4a4eb",
			// 	"#d2f5a6",
			// 	"#76f2f2"];

			var colorList=["#c1232b","#27727b","#e87c25",
				"#0098d9","#b5c334","#8c6ac4",
				"#ffee51","#005eaa","#f3a43b",
				"#60c0dd","#d7504b","#c6e579",
				"#f4e001","#f0805a","#26c0c0"];
			const hexToRgba = (hex, opacity) => {
				let rgbaColor = "";
				let reg = /^#[\da-f]{6}$/i;
				if (reg.test(hex)) {
					rgbaColor = `rgba(${parseInt("0x" + hex.slice(1, 3))},${parseInt(
						"0x" + hex.slice(3, 5)
					)},${parseInt("0x" + hex.slice(5, 7))},${opacity})`;
				}
				return rgbaColor;
			}

			scope.option = {
				color: colorList,//['#3398DB'],
				tooltip: {
					trigger: 'axis',axisPointer: {type: 'line',lineStyle: {opacity: 1}},
				},
				legend: {type:'scroll',right: 10,top: 0},
				grid: {show:true,top: 50, bottom: 0,left: 10,right: 10,containLabel: true,},//height: '85%',containLabel: true,z: 22
				xAxis: [{
					type: 'category',data: null,axisTick: {alignWithLabel: true},boundaryGap: false,
					axisLine: {lineStyle: {color: '#0c3b71'}},axisLabel: {show: true,color: 'rgb(170,170,170)',fontSize:12}
				}],
				yAxis: [{
					type: 'value',/*name:scope.props.yAxisName,*/nameTextStyle:{color:"rgb(170,170,170)"},
					splitLine: {show: true},axisTick: {show: false},axisLine: {lineStyle: {color: '#0c3b71'}},
					axisLabel: {color: 'rgb(170,170,170)',formatter: '{value}'}
				}],
				series: []
			};
		},
	}
}]);