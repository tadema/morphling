angular.module('app').directive('bar03', ['$rootScope', 'dbUtils', '$anchorScroll','$timeout', function($rootScope, dbUtils, $anchorScroll,$timeout) {
	//支持action和initData两种模式
	return {
		restrict: 'EA',
		templateUrl:'tpl/templates/chartTemplate.html',
		scope: {
			labelTitle:"@?",
			headers: "@?",
			props: "=?",
			bodyStyle: "@?",
			methods: "=?",
			urlParams: "=?",//urlParams = Object.assign(scope.urlParams.urlBody,{currentPage:scope.page.pageNumber, onePageSize:scope.page.pageSize})
			initData:"=?",//初始化数据，如果有初始化数据，就不需要再查询了,优先级高于urlParams
		},
		controller: ['$scope', function ($scope, el, attrs) {	}],

		link: function (scope, elem, attrs) {
			//第一次页面初始化
			$timeout(function () {
				let widthValue = $("#"+scope.id).css("width");
				if(!widthValue)
					return;
				if(widthValue.lastIndexOf("px") != -1){
					$("#"+scope.id).width($("#"+scope.id).width());
				}else if(widthValue.lastIndexOf("%") != -1){
					let percent = $("#"+scope.id).parent().parent().parent().width();
					let tWidth = ($rootScope._screenProp.contentWidth-90)*parseInt($("#"+scope.id).width())*percent/10000;
					$("#"+scope.id).width(tWidth);
				}

				scope.myChart = echarts.init(document.getElementById(scope.id));
				scope.myChart.setOption(scope.option);
				
				if(scope.urlParams)
					initData();
				if(hasInitData && scope.initData)
					initData();
			})

			//监控initData的变化
			scope.$watch('initData',function (newValue,oldValue) {
				if(newValue != oldValue){
					hasInitData = true;//若定义了scope.initData，则hasInitData为true
					initData();
				}
			},true);

			/*  --------------------------------------------------
                ----------------数据初始化 start ----------------
                --------------------------------------------------*/
			let hasInitData = !scope.urlParams ? true : false;//是否有初始化数据标记
			scope.id = dbUtils.guid();
			if(scope.bodyStyle){
				scope.bodyStyle = JSON.parse(scope.bodyStyle);
			}else{
				scope.bodyStyle = {height:"300px"}
			}
			scope.myChart;
			scope.methods = {
				refreshData: function () {
					initData();
				},
				reloadData: function () {
					initData();
				},
				setLoading: function (flag){
					scope.loading = flag;
				},
				clear: function () {
					if(scope.myChart){
						scope.myChart.clear();
						scope.option.series = [];
						scope.myChart.setOption(scope.option);
					}
				}
			}
			/*  --------------------------------------------------
                ----------------数据初始化 end ----------------
                --------------------------------------------------*/

			function initData(){
				scope.loading = true;
				if(scope.option && scope.option.series){
					scope.option.series[0].data = [];
					if(scope.myChart)
						scope.myChart.setOption(scope.option);
				}
				if(hasInitData){//如果有传入初始数据，则不通过查询url获取数据
					scope.responseData = scope.initData.data[0];
					drawViewHandle();
				}else {
					getAction().then(function (response) {
						if (response.code == SUCCESS_CODE) {
							if(!response.data){
								scope.loading = false;
								return;
							}
							scope.responseData = response.data[0];
							//绘制表格
							drawViewHandle();
						} else {
							scope.loading = false;
							dbUtils.error(response.message)
						}
					}, function (result) {
						scope.loading = false;
						dbUtils.error(response.message)
					});
				}
			}

			//获取请求promise对象
			function getAction(){
				let action,urlParams = scope.urlParams.urlBody;

				if(!scope.urlParams.method || scope.urlParams.method == "get"){
					action = dbUtils.get(scope.urlParams.url, urlParams)
				}else if(scope.urlParams.method == "post"){
					action = dbUtils.post(scope.urlParams.url, urlParams)
				}else if(scope.urlParams.method == "postBody"){
					action = dbUtils.postBody(scope.urlParams.url, JSON.stringify(urlParams))
				}
				return action;
			}

			//绘制图表
			function drawViewHandle(){
				scope.loading = false;
				scope.data = scope.responseData;
				scope.headers = Object.keys(scope.data);

				scope.option.xAxis[0].data = scope.headers;
				let tData = [];
				for(let i in scope.headers){
					let header = scope.headers[i];
					tData.push({value:scope.data[header],itemStyle: {normal:{color: colorList[i%6]}}})
				}
				scope.option.series[0].data = tData;
				if(scope.myChart)
					scope.myChart.setOption(scope.option);
			}

			//监控父类发送的刷新
			scope.$on('refreshBarData', function(event,data) {methods.refreshData();});
			scope.$on('reloadBarData', function(event,data) {methods.refreshData();});

			let mehtods = {
				refreshData: function () {
					//发射刷新信号
					if(hasInitData)
						scope.$emit("barRefreshData",{});
					else
						initData();
				}
			}

			var colorList=['#afa3f5', '#00d488', '#3feed4', '#3bafff', '#f1bb4c','#aff', "rgba(250,250,250,0.5)"];

			scope.option = {
				color: ['#3398DB'],
				tooltip: {
					trigger: 'axis',axisPointer: {type: 'line',lineStyle: {opacity: 0}},
				},
//				    legend: {data: ['直接访问', '背景'],show: false},
				grid: {show:true,top: 30, bottom: 0,left: 0,right: 0,containLabel: true,},//height: '85%',containLabel: true,z: 22
				xAxis: [{
					type: 'category',data: null,axisTick: {alignWithLabel: true},
					axisLine: {lineStyle: {color: '#0c3b71'}},axisLabel: {show: true,color: 'rgb(170,170,170)',fontSize:12}
				}],
				yAxis: [{
					type: 'value',/*name:scope.props.yAxisName,*/nameTextStyle:{color:"rgb(170,170,170)"},
					splitLine: {show: false},axisTick: {show: false},axisLine: {lineStyle: {color: '#0c3b71'}},
					axisLabel: {color: 'rgb(170,170,170)',formatter: '{value}'}
				}],
				series: [{
					/*name: scope.props.seriesName,*/type: 'bar',barWidth: '20%',barMaxWidth:40,xAxisIndex: 0,yAxisIndex: 0,data: [],
				}]
			};

			if(scope.props && scope.props.xAxis && Object.keys(scope.props.xAxis).length != 0)
				Object.assign(scope.option.xAxis[0],scope.props.xAxis);

			if(scope.props && scope.props.yAxis && Object.keys(scope.props.yAxis).length != 0){
				Object.assign(scope.option.yAxis[0],scope.props.yAxis);
				if(scope.props.yAxis.name)
					scope.option.tooltip.formatter =  '{b0}:<br />{c0} ' + scope.props.yAxis.name;
			}

			if(scope.props && scope.props.series && Object.keys(scope.props.series).length != 0)
				Object.assign(scope.option.series[0],scope.props.series);

		},
	}
}]);