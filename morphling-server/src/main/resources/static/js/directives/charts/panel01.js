angular.module('app').directive('panel01', ['$rootScope', 'dbUtils', '$anchorScroll', '$timeout', function($rootScope, dbUtils, $anchorScroll, $timeout) {
	//支持urlParams和initData两种模式
	return {
		restrict: 'EA',
		templateUrl:'tpl/templates/chartTemplate.html',
		scope: {
			labelTitle:"@",
			headers: "@",
			props: "@?",
			bodyStyle: "@",
			methods: "=?",
			urlParams: "=?",//urlParams = Object.assign(scope.urlParams.urlBody,{currentPage:scope.page.pageNumber, onePageSize:scope.page.pageSize})
			initData:"=?",//初始化数据，如果有初始化数据，就不需要再查询了,优先级高于urlParams
		},
		controller: ['$scope', function ($scope, el, attrs) {	}],

		link: function (scope, elem, attrs) {
			//第一次页面初始化
			$timeout(function () {
				let widthValue = $("#"+scope.id).css("width");
				if(!widthValue)
					return;
				if(widthValue.lastIndexOf("px") != -1){
					$("#"+scope.id).width($("#"+scope.id).width() - 1);
				}else if(widthValue.lastIndexOf("%") != -1){
					let percent = $("#"+scope.id).parent().parent().parent().width();
					let tWidth = ($rootScope._screenProp.contentWidth-90)*parseInt($("#"+scope.id).width())*percent/10000;
					$("#"+scope.id).width(tWidth - 1);
				}

				scope.myChart = echarts.init(document.getElementById(scope.id));
				scope.myChart.setOption(scope.option);

				if(scope.urlParams)
					initData();
				if(hasInitData && scope.initData)
					initData();
			})

			//监控initData的变化
			scope.$watch('initData',function (newValue,oldValue) {
				if(newValue != oldValue){
					hasInitData = true;//若定义了scope.initData，则hasInitData为true
					initData();
				}
			},true);

			/*  --------------------------------------------------
                ----------------数据初始化 start ----------------
                --------------------------------------------------*/
			let hasInitData = !scope.urlParams ? true : false;//是否有初始化数据标记
			scope.id = dbUtils.guid();
			if(scope.props){
				scope.props = JSON.parse(scope.props);
			}else{
				scope.props = {
					max:100
				}
			}
			if(scope.bodyStyle){
				scope.bodyStyle = JSON.parse(scope.bodyStyle);
			}else{
				scope.bodyStyle = {height:"200px"}
			}
			scope.myChart;
			scope.methods = {
				refreshData: function () {
					initData();
				},
				reloadData: function () {
					initData();
				},
				setLoading: function (flag){
					scope.loading = flag;
				},
				clear: function () {
					if(scope.myChart){
						scope.myChart.clear();
						scope.option.series = [];
						scope.myChart.setOption(scope.option);
					}
				}
			}
			/*  --------------------------------------------------
                ----------------数据初始化 end ----------------
                --------------------------------------------------*/

			function initData(){
				scope.loading = true;
				if(scope.option && scope.option.series){
					scope.option.series[0].data[0].value = null;
					scope.option.series[0].axisLine.lineStyle.color[0] = [0,color];
					if(scope.myChart)
						scope.myChart.setOption(scope.option);
				}
				if(hasInitData){//如果有传入初始数据，则不通过查询url获取数据
					scope.responseData = scope.initData.data[0];
					drawViewHandle();
				}else {
					getAction().then(function (response) {
						if (response.code == SUCCESS_CODE) {
							scope.responseData = response.data[0];
							//绘制表格
							drawViewHandle();
						} else {
							scope.loading = false;
							dbUtils.error(response.message)
						}
					}, function (result) {
						scope.loading = false;
						dbUtils.error(response.message)
					});
				}
			}

			//获取请求promise对象
			function getAction(){
				let action,urlParams = scope.urlParams.urlBody;

				if(!scope.urlParams.method || scope.urlParams.method == "get"){
					action = dbUtils.get(scope.urlParams.url, urlParams)
				}else if(scope.urlParams.method == "post"){
					action = dbUtils.post(scope.urlParams.url, urlParams)
				}else if(scope.urlParams.method == "postBody"){
					action = dbUtils.postBody(scope.urlParams.url, JSON.stringify(urlParams))
				}
				return action;
			}

			//绘制图表
			function drawViewHandle(){
				scope.loading = false;
				scope.data = scope.responseData;
				scope.headers = Object.keys(scope.data);
				scope.option.series[0].data[0].name = scope.headers[0];
				scope.option.series[0].data[0].value = scope.data[scope.headers[0]];
				scope.option.series[0].axisLine.lineStyle.color[0] = [scope.data[scope.headers[0]] / 100,color];
				if(scope.myChart)
					scope.myChart.setOption(scope.option);
			}

			//监控父类发送的刷新
			scope.$on('refreshPieData', function(event,data) {methods.refreshData();});
			scope.$on('reloadPieData', function(event,data) {methods.refreshData();});

			let mehtods = {
				refreshData: function () {
					//发射刷新信号
					if(hasInitData)
						scope.$emit("pieRefreshData",{});
					else
						initData();
				}
			}

			let color = new echarts.graphic.LinearGradient(0, 0, 1, 0, [{
				offset: 0,
				color: 'rgba(162, 89, 252, 1)' // 0% 处的颜色
			},
				{
					offset: 1,
					color: 'rgba(22, 220, 247, 1)' // 100% 处的颜色
				}
			]);
			scope.option = {
				backgroundColor: "#ffffff",
				color: ["#37A2DA", "#32C5E9", "#67E0E3"],
				series: [{
					name: '业务指标',type: 'gauge',startAngle: 180,endAngle: 0,radius: '100%',center: ['50%', '70%'],title:{show:true,offsetCenter:[0, '-35%']},
					detail: {formatter: '{value}%',offsetCenter: [0, '20%']},
					axisLine: {
						lineStyle: {
							color: [[0, '#5DD1FA'], [1, "#f7f9fc"]],width: 20,shadowColor: '#0093ee', //默认透明
//		                    shadowOffsetX: 0,shadowOffsetY: 0,shadowBlur: 20,opacity: 1,
						}
					},
					axisLabel: {show: false,},axisTick: {show: false},splitLine: {show: false,},itemStyle: {show: false,},pointer: {show: false,},
					data: [{
						value: 50,
						name: '完成率',
					}]

				}]
			};
		},
	}
}]);