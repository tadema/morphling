angular.module('app').directive('pie02', ['$rootScope', 'dbUtils', '$anchorScroll', function($rootScope, dbUtils, $anchorScroll) {
	return {
		restrict: 'EA',
		templateUrl:'tpl/templates/chartTemplate.html',
		scope: {
			labelTitle:"@",
			headers: "@",
			props: "@",
			bodyStyle: "@",
			refresh: "=",
			action: "=?",
		},
		controller: ['$scope', function ($scope, el, attrs) {
			$scope.id = dbUtils.guid();
			if($scope.bodyStyle){
				$scope.bodyStyle = JSON.parse($scope.bodyStyle);
			}else{
				$scope.bodyStyle = {height:"200px"}
			}
			if($scope.props){
				$scope.props = JSON.parse($scope.props);
			}else{
				$scope.props = {
				}
			}
			$scope.myChart;
			function initData(){
				$scope.data = {};
				$scope.loading = true;
				if($scope.option && $scope.option.series){
					$scope.option.series[0].data = [];
					$scope.option.legend.data = []; 
					if($scope.myChart)
						$scope.myChart.setOption($scope.option);
				}
				$scope.action.then(function(response){
					if(response.code == SUCCESS_CODE){
						$scope.loading = false;
						$scope.data = response.data[0];
						$scope.headers = Object.keys($scope.data);
						$scope.option.series[0].data = [];
						for(let i in $scope.headers){
							$scope.option.series[0].data.push({name:$scope.headers[i],value:$scope.data[$scope.headers[i]]});
						}
						$scope.option.legend.data = $scope.headers; 
						if($scope.myChart)
							$scope.myChart.setOption($scope.option);
					}else{
						dbUtils.error(response.message,"提示");
					}
				})
			}
			initData();
			//刷新
			let intervalId;
			$scope.$watch('refresh',function(current,old){
				if(current != old){
					if(current != "off"){
						if(intervalId)
							clearInterval(intervalId);
						
						intervalId = setInterval(initData,current);
					}else{
						if(intervalId)
							clearInterval(intervalId);
					}
				}
			})
			
//			var  colorList=["#8d7fec", "#5085f2","#e75fc3","#f87be2","#f2719a","#fca4bb","#f59a8f","#fdb301","#57e7ec","#cf9ef1"];
			var colorList = ['#feb513', '#30ca40','#a147eb','#8b572a','#e0109c','#fc605f', '#50e3c2','#6879ff','#222222'];
			$scope.option = {
				    grid: {show:false,bottom: 0, bottom: 0,left: 10,right: 0},color:colorList,
				    legend: {
				        orient: "vertical",x: "left",
				        top: "center",
				        left: "70%",
				        bottom: "0%",
				        data: [],
				        itemWidth: 8,
				        itemHeight: 8,
				        itemGap :16,
				        formatter :function(name){
				            return ''+name
				          }
				    },
				    tooltip: {
		                trigger: 'item',
		                formatter:function (params){
		                  return params.data.name+"<br>数量："+ params.data.value+"</br>占比："+ params.percent+"%";
		                }
		            },
				    series: [
				        {
				        	radius: ["40%", "65%"],center: ["45%", "50%"],avoidLabelOverlap: true,type: 'pie',
				            itemStyle: {
				                normal: {borderColor: '#ffffff',borderWidth: 6,}
				            },
				            label:{
				            	normal: {
				                    show: true,position: 'outter',formatter:function (parms){return parms.data.legendname},//formatter: '{text|{b}}\n{c} ({d}%)',
				                    rich: {
				                        text: {color: "#666",fontSize: 14,align: 'center',verticalAlign: 'middle',padding: 8},
				                        value: {color: "#8693F3",fontSize: 16,align: 'center',verticalAlign: 'middle',},
				                    }
				                },
				                emphasis: {
				                    show: true,textStyle: {fontSize: 16,},formatter: function (parms){return parms.data.legendname}
				                }
				            },
				            labelLine: {
				                normal: {show:true,length:10,length2:5,smooth:true,}
				            },
				            data: [{value:15, name:'火焰传感器'},{value:10, name:'烟雾传感器',},{value:25, name:'一氧化碳传感器'},{value:10, name:'蜂鸣器传感器'}],
				        },
				    ]
				};
		}],

		link: function (scope, elem, attrs) {
			scope.$watch('$viewContentLoaded', function() {  
				let widthValue = $("#"+scope.id).css("width");
				if(widthValue.lastIndexOf("px") != -1){
					$("#"+scope.id).width($("#"+scope.id).width());
				}else if(widthValue.lastIndexOf("%") != -1){
					let percent = $("#"+scope.id).parent().parent().parent().width();
					let tWidth = ($rootScope._screenProp.contentWidth-90)*parseInt($("#"+scope.id).width())*percent/10000;
					$("#"+scope.id).width(tWidth);
				}
				scope.myChart = echarts.init(document.getElementById(scope.id));
				scope.myChart.setOption(scope.option);
			}); 
		},
	}
}]);