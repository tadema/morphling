angular.module('app').directive('text01', ['$rootScope', 'dbUtils', '$anchorScroll', function($rootScope, dbUtils, $anchorScroll) {
	return {
		restrict: 'EA',
		templateUrl:'tpl/templates/chartTemplate.html',
		scope: {
			labelTitle:"@",
			headers: "@",
			props: "@",
			bodyStyle: "@",
			refresh: "=",
			action: "="
		},
		controller: ['$scope', function ($scope, el, attrs) {
			$scope.id = dbUtils.guid();
			$scope.isHiddenTitle = true;
			if($scope.bodyStyle){
				$scope.bodyStyle = JSON.parse($scope.bodyStyle);
			}else{
				$scope.bodyStyle = {height:"100px",padding: "0px 15px"}
			}
			if($scope.props){
				$scope.props = JSON.parse($scope.props);
			}else{
				$scope.props = {
				}
			}
			function initData(){
				$scope.div_main,$scope.div_value,$scope.div_title;
				$scope.loading = true;
				
				$scope.action.then(function(response){
					$scope.data = {};
					if(response.code == SUCCESS_CODE){
						$scope.loading = false;
						$scope.data = response.data[0];
						$scope.headers = Object.keys($scope.data);
						
						$scope.value = $scope.data[$scope.headers[0]];  
						$($scope.div_value).text($scope.value);
					}else{
						dbUtils.error(response.message,"提示");
					}
				})
			}
			initData();
			//刷新
			let intervalId;
			$scope.$watch('refresh',function(current,old){
				if(current != old){
					if(current != "off"){
						if(intervalId)
							clearInterval(intervalId);
						
						intervalId = setInterval(initData,current);
					}else{
						if(intervalId)
							clearInterval(intervalId);
					}
				}
			})
		}],

		link: function (scope, elem, attrs) {
			scope.$watch('$viewContentLoaded', function() {  
				scope.div_main = document.getElementById(scope.id);
				$(scope.div_main).css("font-size","18px");
				$(scope.div_main).css("font-weight","bold");
				
				scope.div_value = document.createElement("div");
				$(scope.div_value).css("top","0px");
	            $(scope.div_value).css("height",parseInt(scope.bodyStyle.height)/2+"px");
	            $(scope.div_value).css("line-height",parseInt(scope.bodyStyle.height)/2+"px");
	            $(scope.div_value).css("text-align","center");
	            $(scope.div_value).css("font-size","20px");
	            $(scope.div_value).css("border","0px solid red");
	            $(scope.div_value).text(scope.value);
	            scope.div_main.appendChild(scope.div_value);
	            
	            scope.div_title = document.createElement("div");
	            $(scope.div_title).css("top",parseInt(scope.bodyStyle.height)/2+"px");
	            $(scope.div_title).css("height",parseInt(scope.bodyStyle.height)/2+"px");
	            $(scope.div_title).css("line-height",parseInt(scope.bodyStyle.height)/2+"px");
	            $(scope.div_title).css("text-align","center");
	            $(scope.div_title).css("color","#1c2b36");
	            $(scope.div_title).text(scope.title);
	            scope.div_main.appendChild(scope.div_title);
			}); 
		},
	}
}]);