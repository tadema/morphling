angular.module('app').directive('bar01', ['$rootScope', 'dbUtils', '$anchorScroll', function($rootScope, dbUtils, $anchorScroll) {
	return {
		restrict: 'EA',
		templateUrl:'tpl/templates/chartTemplate.html',
		scope: {
			labelTitle:"@",
			headers: "@",
			props: "@",
			bodyStyle: "@",
			refresh: "=",
			action: "="
		},
		controller: ['$scope', function ($scope, el, attrs) {
			$scope.id = dbUtils.guid();
			if($scope.bodyStyle){
				$scope.bodyStyle = JSON.parse($scope.bodyStyle);
			}else{
				$scope.bodyStyle = {height:"300px"}
			}
			if($scope.props){
				$scope.props = JSON.parse($scope.props);
			}else{
				$scope.props = {}
			}
			$scope.myChart;
			function initData(){
				$scope.data = {};
				$scope.loading = true;
				if($scope.option && $scope.option.series){
					$scope.option.series[0].data = [];
					if($scope.myChart)
						$scope.myChart.setOption($scope.option);
				}
				$scope.action.then(function(response){
					if(response.code == SUCCESS_CODE){
						$scope.loading = false;
						$scope.data = response.data[0];
						$scope.headers = Object.keys($scope.data);
						
						$scope.option.xAxis[0].data = $scope.headers;
						let tData = [];
						for(let i in $scope.headers){
							let header = $scope.headers[i];
							tData.push({value:$scope.data[header],itemStyle: {normal:{color: colorList[i%6]}}})
						}
						$scope.option.series[0].data = tData;
						if($scope.myChart)
							$scope.myChart.setOption($scope.option);
					}else{
						dbUtils.error(response.message,"提示");
					}
				})
			}
			initData();
			//刷新
			let intervalId;
			$scope.$watch('refresh',function(current,old){
				if(current != old){
					if(current != "off"){
						if(intervalId)
							clearInterval(intervalId);
						
						intervalId = setInterval(initData,current);
					}else{
						if(intervalId)
							clearInterval(intervalId);
					}
				}
			})
			
			var colorList=['#afa3f5', '#00d488', '#3feed4', '#3bafff', '#f1bb4c','#aff', "rgba(250,250,250,0.5)"];
			
			$scope.option = {
				    color: ['#3398DB'],
				    tooltip: {
				        trigger: 'axis',axisPointer: {type: 'line',lineStyle: {opacity: 0}},
				        /*formatter: function(prams) {
				            if (prams[0].data === min) {
				                return "合格率：0%"
				            } else {
				                return "合格率：" + prams[0].data + "%"
				            }
				        }*/
				    },
//				    legend: {data: ['直接访问', '背景'],show: false},
				    grid: {show:true,top: 30, bottom: 0,left: 0,right: 0,containLabel: true,},//height: '85%',containLabel: true,z: 22
				    xAxis: [{
				        type: 'category',data: null,axisTick: {alignWithLabel: true},
				        axisLine: {lineStyle: {color: '#0c3b71'}},axisLabel: {show: true,color: 'rgb(170,170,170)',fontSize:12}
				    }],
				    yAxis: [{
				            type: 'value',name:$scope.props.yAxisName,nameTextStyle:{color:"rgb(170,170,170)"},
				            splitLine: {show: false},axisTick: {show: false},axisLine: {lineStyle: {color: '#0c3b71'}},
				            axisLabel: {color: 'rgb(170,170,170)',formatter: '{value}'}
				        },
				    ],
				    series: [{
				            name: $scope.props.seriesName,type: 'bar',barWidth: '20%',barMaxWidth:40,xAxisIndex: 0,yAxisIndex: 0,data: [],
				            /*itemStyle: {
				                normal: {
//				                    barBorderRadius: 30,
				                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{offset: 0,color: '#00feff'},{offset: 0.5,color: '#027eff'},{offset: 1,color: '#0286ff'}])
				                }
				            },*/
				        },
				    ]
				};
		}],

		link: function (scope, elem, attrs) {
			scope.$watch('$viewContentLoaded', function() {
				let widthValue = $("#"+scope.id).css("width");
				if(widthValue.lastIndexOf("px") != -1){
					$("#"+scope.id).width($("#"+scope.id).width());
				}else if(widthValue.lastIndexOf("%") != -1){
					let percent = $("#"+scope.id).parent().parent().parent().width();
					let tWidth = ($rootScope._screenProp.contentWidth-90)*parseInt($("#"+scope.id).width())*percent/10000;
					$("#"+scope.id).width(tWidth);
				}
				
				scope.myChart = echarts.init(document.getElementById(scope.id));
				scope.myChart.setOption(scope.option);
			}); 
		},
	}
}]);