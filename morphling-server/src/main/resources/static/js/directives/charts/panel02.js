angular.module('app').directive('panel02', ['$rootScope', 'dbUtils', '$anchorScroll', function($rootScope, dbUtils, $anchorScroll) {
	return {
		restrict: 'EA',
		templateUrl:'tpl/templates/chartTemplate.html',
		scope: {
			labelTitle:"@",
			headers: "@",
			props: "@",
			bodyStyle: "@",
			refresh: "=",
			action: "="
		},
		controller: ['$scope', function ($scope, el, attrs) {
			$scope.id = dbUtils.guid();
			if($scope.props){
				$scope.props = JSON.parse($scope.props);
			}else{
				$scope.props = {
						max:100
				}
			}
			if($scope.bodyStyle){
				$scope.bodyStyle = JSON.parse($scope.bodyStyle);
			}else{
				$scope.bodyStyle = {height:"200px"}
			}
			$scope.myChart;
			function initData(){
				$scope.data = {};
				$scope.loading = true;
				if($scope.option && $scope.option.series){
					$scope.option.series[0].data[0].value = null;
					$scope.option.series[0].axisLine.lineStyle.color[0] = [0,color];
					if($scope.myChart)
						$scope.myChart.setOption($scope.option);
				}
				$scope.action.then(function(response){
					if(response.code == SUCCESS_CODE){
						$scope.loading = false;
						$scope.data = response.data[0];
						$scope.headers = Object.keys($scope.data);
						$scope.option.series[0].data[0].name = $scope.headers[0];
						$scope.option.series[0].data[0].value = $scope.data[$scope.headers[0]];
						$scope.option.series[0].axisLine.lineStyle.color[0] = [$scope.data[$scope.headers[0]] / 100,color];
						if($scope.myChart)
							$scope.myChart.setOption($scope.option);
					}else{
						dbUtils.error(response.message,"提示");
					}
				})
			}
			initData();
			//刷新
			let intervalId;
			$scope.$watch('refresh',function(current,old){
				if(current != old){
					if(current != "off"){
						if(intervalId)
							clearInterval(intervalId);
						
						intervalId = setInterval(initData,current);
					}else{
						if(intervalId)
							clearInterval(intervalId);
					}
				}
			})
			let color = new echarts.graphic.LinearGradient(0, 0, 1, 0, [{
			        offset: 0,
			        color: 'rgba(162, 89, 252, 1)' // 0% 处的颜色
			    },
			    {
			        offset: 1,
			        color: 'rgba(22, 220, 247, 1)' // 100% 处的颜色
			    }
			]);
			$scope.option = {
			    backgroundColor: "#ffffff",
			    color: ["#37A2DA", "#32C5E9", "#67E0E3"],
			    series: [{
			        name: '业务指标',type: 'gauge',startAngle: 180,endAngle: 0,radius: '100%',center: ['50%', '70%'],title:{show:true,offsetCenter:[0, '-35%']},
			        detail: {formatter: '{value}%',offsetCenter: [0, '20%']},
			        axisLine: {
		                lineStyle: {
		                    color: [[0, '#5DD1FA'], [1, "#f7f9fc"]],width: 20,shadowColor: '#0093ee', //默认透明
//		                    shadowOffsetX: 0,shadowOffsetY: 0,shadowBlur: 20,opacity: 1,
		                }
		            },
		            axisLabel: {show: false,},axisTick: {show: false},splitLine: {show: false,},itemStyle: {show: false,},pointer: {show: false,},
			        data: [{
			            value: 50,
			            name: '完成率',
			        }]
	
			    }]
			};
		}],

		link: function (scope, elem, attrs) {
			scope.$watch('$viewContentLoaded', function() {  
				let widthValue = $("#"+scope.id).css("width");
				if(widthValue.lastIndexOf("px") != -1){
					$("#"+scope.id).width($("#"+scope.id).width());
				}else if(widthValue.lastIndexOf("%") != -1){
					let percent = $("#"+scope.id).parent().parent().parent().width();
					let tWidth = ($rootScope._screenProp.contentWidth-90)*parseInt($("#"+scope.id).width())*percent/10000;
					$("#"+scope.id).width(tWidth);
				}
				
				scope.myChart = echarts.init(document.getElementById(scope.id));
				scope.myChart.setOption(scope.option);
			}); 
		},
	}
}]);