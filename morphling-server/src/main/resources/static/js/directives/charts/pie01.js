angular.module('app').directive('pie01', ['$rootScope', 'dbUtils', '$anchorScroll', '$timeout', function($rootScope, dbUtils, $anchorScroll, $timeout) {
	//支持action和initData两种模式
	return {
		restrict: 'EA',
		templateUrl:'tpl/templates/chartTemplate.html',
		scope: {
			labelTitle:"@",
			headers: "@",
			props: "@",
			bodyStyle: "@",
			methods: "=?",
			urlParams: "=?",//urlParams = Object.assign(scope.urlParams.urlBody,{currentPage:scope.page.pageNumber, onePageSize:scope.page.pageSize})
			initData:"=?",//初始化数据，如果有初始化数据，就不需要再查询了,优先级高于urlParams
		},
		controller: ['$scope', function ($scope, el, attrs) {		}],

		link: function (scope, elem, attrs) {
			//第一次页面初始化
			$timeout(function () {
				let widthValue = $("#"+scope.id).css("width");
				if(!widthValue)
					return;
				if(widthValue.lastIndexOf("px") != -1){
					$("#"+scope.id).width($("#"+scope.id).width());
				}else if(widthValue.lastIndexOf("%") != -1){
					let percent = $("#"+scope.id).parent().parent().parent().width();
					let tWidth = ($rootScope._screenProp.contentWidth-90)*parseInt($("#"+scope.id).width())*percent/10000;
					$("#"+scope.id).width(tWidth);
				}

				scope.myChart = echarts.init(document.getElementById(scope.id));
				scope.myChart.setOption(scope.option);

				if(scope.urlParams)
					initData();
				if(hasInitData && scope.initData)
					initData();
			})

			//监控initData的变化
			scope.$watch('initData',function (newValue,oldValue) {
				if(newValue != oldValue){
					hasInitData = true;//若定义了scope.initData，则hasInitData为true
					initData();
				}
			},true);

			/*  --------------------------------------------------
                ----------------数据初始化 start ----------------
                --------------------------------------------------*/
			let hasInitData = !scope.urlParams ? true : false;//是否有初始化数据标记
			scope.id = dbUtils.guid();
			if(scope.bodyStyle){
				scope.bodyStyle = JSON.parse(scope.bodyStyle);
			}else{
				scope.bodyStyle = {height:"200px"}
			}
			if(scope.props){
				scope.props = JSON.parse(scope.props);
			}else{
				scope.props = {}
			}
			scope.myChart;
			scope.methods = {
				refreshData: function () {
					initData();
				},
				reloadData: function () {
					initData();
				},
				setLoading: function (flag){
					scope.loading = flag;
				},
				clear: function () {
					if(scope.myChart){
						scope.myChart.clear();
						scope.option.series = [];
						scope.myChart.setOption(scope.option);
					}
				}
			}
			/*  --------------------------------------------------
                ----------------数据初始化 end ----------------
                --------------------------------------------------*/

			function initData(){
				scope.loading = true;
				if(scope.option && scope.option.series){
					scope.option.series[0].data = [];
					if(scope.myChart)
						scope.myChart.setOption(scope.option);
				}
				if(hasInitData){//如果有传入初始数据，则不通过查询url获取数据
					scope.responseData = scope.initData.data[0];
					drawViewHandle();
				}else {
					getAction().then(function (response) {
						if (response.code == SUCCESS_CODE) {
							scope.responseData = response.data[0];
							//绘制表格
							drawViewHandle();
						} else {
							scope.loading = false;
							dbUtils.error(response.message)
						}
					}, function (result) {
						scope.loading = false;
						dbUtils.error(response.message)
					});
				}
			}

			//获取请求promise对象
			function getAction(){
				let action,urlParams = scope.urlParams.urlBody;

				if(!scope.urlParams.method || scope.urlParams.method == "get"){
					action = dbUtils.get(scope.urlParams.url, urlParams)
				}else if(scope.urlParams.method == "post"){
					action = dbUtils.post(scope.urlParams.url, urlParams)
				}else if(scope.urlParams.method == "postBody"){
					action = dbUtils.postBody(scope.urlParams.url, JSON.stringify(urlParams))
				}
				return action;
			}

			//绘制图表
			function drawViewHandle(){
				scope.loading = false;
				scope.data = scope.responseData;
				scope.headers = Object.keys(scope.data);
				scope.option.series[0].data = [];
				for(let i in scope.headers){
					scope.option.series[0].data.push({name:scope.headers[i],value:scope.data[scope.headers[i]]});
				}
				scope.option.legend.data = scope.headers;
				if(scope.myChart)
					scope.myChart.setOption(scope.option);
			}

			//监控父类发送的刷新
			scope.$on('refreshPieData', function(event,data) {methods.refreshData();});
			scope.$on('reloadPieData', function(event,data) {methods.refreshData();});

			let mehtods = {
				refreshData: function () {
					//发射刷新信号
					if(hasInitData)
						scope.$emit("pieRefreshData",{});
					else
						initData();
				}
			}

			var colorList = ['#feb513', '#30ca40','#a147eb','#8b572a','#e0109c','#fc605f', '#50e3c2','#6879ff','#222222'];
			scope.option = {
				grid: {show:false,bottom: 0, bottom: 0,left: 10,right: 0},color:colorList,
				legend: {
					orient: "vertical",x: "left",
					top: "center",
					left: "80%",
					bottom: "0%",
					data: [],
					itemWidth: 8,
					itemHeight: 8,
					itemGap :16,
					formatter :function(name){
						return ''+name
					}
				},
				tooltip: {
					trigger: 'item',
					formatter:function (params){
						return params.data.name+"<br>数量："+ params.data.value+"</br>占比："+ params.percent+"%";
					}
				},
				series: [
					{
						radius: ["40%", "65%"],center: ["45%", "50%"],avoidLabelOverlap: true,type: 'pie',
						itemStyle: {
							normal: {borderColor: '#ffffff',borderWidth: 6,}
						},
						label:{
							normal: {
								show: true,position: 'outter',formatter:function (parms){return parms.data.legendname},//formatter: '{text|{b}}\n{c} ({d}%)',
								rich: {
									text: {color: "#666",fontSize: 14,align: 'center',verticalAlign: 'middle',padding: 8},
									value: {color: "#8693F3",fontSize: 16,align: 'center',verticalAlign: 'middle',},
								}
							},
							emphasis: {
								show: true,textStyle: {fontSize: 16,},formatter: function (parms){return parms.data.legendname}
							}
						},
						labelLine: {
							normal: {show:true,length:10,length2:5,smooth:true,}
						},
						data: [],
					},
				]
			};

		},
	}
}]);