angular.module('app').directive('singleHorizontalBar01', ['$rootScope', 'dbUtils', '$anchorScroll', function($rootScope, dbUtils, $anchorScroll) {
	return {
		restrict: 'EA',
		templateUrl:'tpl/templates/chartTemplate.html',
		scope: {
			labelTitle:"@",
			headers: "@",
			props: "@",
			bodyStyle: "@",
			refresh: "=",
			data: "="
		},
		controller: ['$scope', function ($scope, el, attrs) {
			$scope.id = dbUtils.guid();
			$scope.isHiddenTitle = true;
			if($scope.bodyStyle){
				$scope.bodyStyle = JSON.parse($scope.bodyStyle);
			}else{
				$scope.bodyStyle = {height:"40px",padding:"3px"}
			}
			if($scope.props){
				$scope.props = JSON.parse($scope.props);
			}else{
				$scope.props = {
				}
			}
			$scope.myChart;
			
			$scope.$watch('data',function(current,old){
				if(current != old){
					$scope.option.series[0].data = [current];
					if($scope.myChart)
						$scope.myChart.setOption($scope.option);
				}
			},true)
			
			// 颜色
			var lightBlue = {
			    type: 'linear',x: 0,y: 0,x2: 0,y2: 1,
			    colorStops: [{
			        offset: 0,color: 'rgba(41, 121, 255, 1)' // 0% 处的颜色
			    }, {
			        offset: 1,color: 'rgba(0, 192, 255, 1)' // 100% 处的颜色
			    }],
			    globalCoord: false // 缺省为 false
			}
			
			$scope.option = {
				    tooltip: {show: false},
				    grid: {top: '0%',left: '0%',right: '0%',bottom: '0%',},
				    yAxis: {
				        type: 'category',data: [$scope.title],axisTick: {show: false},axisLine: {show: false},axisLabel: {color: '#fff',fontSize: 12}
				    },
				    xAxis: {
				            type: 'value', min: 0,max: 100,splitLine: {show: false},axisTick: {show: false},axisLine: {show: false},axisLabel: {show: false}
				    },
				    series: [{
				        type: 'bar',label: {normal:{show: true,position:'inside',padding: 10,color: '#ffffff',fontSize: 14,formatter: '{c}%'}},
				        itemStyle: {normal:{color: lightBlue}},barWidth: '100%',data: [$scope.data],z: 10
				    }, {
				        type: 'bar',barGap: '-100%',itemStyle: {normal:{opacity: 0.05}},
				        barWidth: '100%',data: [100],z: 5
				    }, {
				        type: 'bar',barGap: '-100%',itemStyle: {normal:{color: '#536dfe',opacity: 0.2}},
				        barWidth: '100%',data: [100],z: 5
				    }]
				};
		}],

		link: function (scope, elem, attrs) {
			scope.$watch('$viewContentLoaded', function() {
				let widthValue = $("#single-horizontal").parent().css("width");
				let tWidthValue;
				if(widthValue.lastIndexOf("px") != -1){
					tWidthValue = $("#single-horizontal").parent().width();
				}else if(widthValue.lastIndexOf("%") != -1){
					//除2为确定磁盘空间的width，
					//parseInt($("#single-horizontal").parent().width())/100---获取单个柱图的百分比width
					//30为bar两边的padding
					let tWidth = ($rootScope._screenProp.contentWidth-90)/2*parseInt($("#single-horizontal").parent().width())/100-30;
					tWidthValue = tWidth;
				}
				$("#"+scope.id).width(tWidthValue-2*3);//减去2个padding
				
				scope.myChart = echarts.init(document.getElementById(scope.id));
				scope.option.series[0].data = [scope.data];
				scope.myChart.setOption(scope.option);
			}); 
		},
	}
}]);