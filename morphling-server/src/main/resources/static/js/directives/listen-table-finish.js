'use strict';

angular.module('app').directive('listenTableFinish', ['$timeout', function ($timeout) {
    return {
        restrict:"A",
        link: function(scope,element,attr) {
            if (scope.$last === true) {
                $timeout(function(){
                    scope.$emit("TableLoadOver")
                });
            }
        }
    };
}])
