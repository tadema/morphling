angular.module('app').directive('uiInfoShowcase', ['$rootScope', 'dbUtils', '$anchorScroll','$timeout', function($rootScope, dbUtils, $anchorScroll, $timeout) {
	return {
		restrict: 'EA',
		templateUrl:'tpl/templates/uiInfoShowcase.html',
		scope: {
			labelTitle:"@",
			headers: "@",
			props: "@",
			bodyStyle: "@",
			methods: "=?",
			urlParams: "=?",//urlParams = Object.assign(scope.urlParams.urlBody,{currentPage:scope.page.pageNumber, onePageSize:scope.page.pageSize})
			initData:"=?",//初始化数据，如果有初始化数据，就不需要再查询了,优先级高于urlParams
		},
		controller: ['$scope', function ($scope, el, attrs) {	}],
		link: function (scope, elem, attrs) {
			//第一次页面初始化
			$timeout(function () {
				if(scope.urlParams)
					initData();
				if(hasInitData && scope.initData)
					initData();
			})

			//监控initData的变化
			scope.$watch('initData',function (newValue,oldValue) {
				if(newValue != oldValue){
					hasInitData = true;//若定义了scope.initData，则hasInitData为true
					initData();
				}
			},true);

			/*  --------------------------------------------------
                ----------------数据初始化 start ----------------
                --------------------------------------------------*/
			let hasInitData = !scope.urlParams ? true : false;//是否有初始化数据标记
			if(!scope.headers){
				scope.headers = [{"value":"名称"},{"value":"值"},]
			}
			if(scope.props){
				scope.props = JSON.parse(scope.props);
			}
			if(scope.bodyStyle){
				scope.bodyStyle = JSON.parse(scope.bodyStyle);
			}

			scope.methods = {
				refreshData: function () {
					initData();
				},
				reloadData: function () {
					initData();
				},
			}
			/*  --------------------------------------------------
                ----------------数据初始化 end ----------------
                --------------------------------------------------*/

			function initData(){
				scope.loading = true;
				if(hasInitData){//如果有传入初始数据，则不通过查询url获取数据
					scope.responseData = scope.initData;
					drawViewHandle();
				}else {
					getAction().then(function (response) {
						if (response.code == SUCCESS_CODE) {
							scope.responseData = response.data;
							//绘制表格
							drawViewHandle();
						} else {
							scope.loading = false;
							dbUtils.error(response.message)
						}
					}, function (result) {scope.loading = false;});
				}
			}

			//获取请求promise对象
			function getAction(){
				let action,urlParams = scope.urlParams.urlBody;

				if(!scope.urlParams.method || scope.urlParams.method == "get"){
					action = dbUtils.get(scope.urlParams.url, urlParams)
				}else if(scope.urlParams.method == "post"){
					action = dbUtils.post(scope.urlParams.url, urlParams)
				}else if(scope.urlParams.method == "postBody"){
					action = dbUtils.postBody(scope.urlParams.url, JSON.stringify(urlParams))
				}
				return action;
			}

			//绘制图表
			function drawViewHandle(){
				scope.loading = false;
				scope.keys = [];
				scope.data = Object.assign({} , scope.responseData[0]);
				if(!scope.props || scope.props == "" || scope.props.length == 0)
					scope.keys = scope.keys.concat(Object.keys(scope.responseData[0]));
				else{
					scope.props = JSON.parse(scope.props);
					scope.keys = Object.keys(scope.props);
				}
			}

		},
	}
}]);