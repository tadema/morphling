'use strict';

angular.module('app').directive('uiStep', ['dbUtils','$state', 'dateFilter','$cacheFactory', '$compile','$sce',
	function (dbUtils,$state, dateFilter,$cacheFactory,$compile,$sce) {
	return {
		restrict: 'E',
		templateUrl: "tpl/templates/uiStep.html",
		replace: true,
		transclude:true,
		controller: ['$scope', function ($scope, el, attrs) {
			$scope.prevAction = "prev";
			$scope.nextAction = "next";
			$scope.submitAction = "submit";
			
			//修改title样式
			$scope.changeTitleCss = function(){
				if(angular.isUndefined($scope.main) || angular.isUndefined($scope.main.action) || $scope.main.action == null)
					return;
				setTimeout(function(){
					if($scope.main.action == $scope.prevAction){
						$("#nav-step li").eq($scope.main.stateIndex+1).removeClass('active ');
						$("#nav-step li").eq($scope.main.stateIndex).removeClass('finish-step');
						for(var i=1;i<=$scope.main.stateIndex;i++){
							$("#nav-step li").eq(i).addClass('active');
							$("#nav-step li").eq(i-1).addClass('active finish-step');
						}
					}
					if($scope.main.action == $scope.nextAction){
						for(var i=1;i<=$scope.main.stateIndex;i++){
							$("#nav-step li").eq(i).addClass('active');
							$("#nav-step li").eq(i-1).addClass('active finish-step');
						}
					}
				},200)
			}

			$scope.cast = function(action){
				$scope.main.action = action;
				if(action == $scope.prevAction && $scope.canPrev){
					$scope.$broadcast($scope.main.stateObject.state, action);
				}else if(action == $scope.nextAction && $scope.canNext){
					$scope.$broadcast($scope.main.stateObject.state, action);
				}else if(action == $scope.submitAction){
					$scope.$broadcast($scope.main.stateObject.state, action);
				}
			}
			
			$scope.$on("parent", function(event,data) {
				if(data.flag == true){
					if(data.action == $scope.prevAction){
						$scope.prevHandle();
					}else if(data.action == $scope.nextAction){
						$scope.nextHandle();
					}else if(data.action == $scope.submitAction){
						$scope.submit();
					}
				}else{
					if(data.action == $scope.prevAction){
						$scope.prevHandle();
					}else if(data.action == $scope.nextAction){
						dbUtils.warning("请将表单填写完整","提示");
					}
					else if(data.action == $scope.submitAction){
						dbUtils.warning("请将表单填写完整","提示");
					}
				}
			});
			
			//前一步按钮
			$scope.prevHandle = function(){
				$scope.main.stateObject.data = getStateData();
				$scope.main.stateIndex--;
				$scope.main.stateObject = $scope.main.stateObjectArr[$scope.main.stateIndex];
				$state.go($scope.main.stateObject.state,{data:$scope.main,cdata:$scope.main.stateObject.data});//getStateData()
			}
			
			//下一步按钮
			$scope.nextHandle = function(){
				$scope.main.stateObject.data = getStateData();
				$scope.main.stateIndex++;
				$scope.main.stateObject = $scope.main.stateObjectArr[$scope.main.stateIndex];
				$state.go($scope.main.stateObject.state,{data:$scope.main,datas:{},cdata:$scope.main.stateObject.data});//getStateData()
			}
			
			//完成
			$scope.submit = function(){
				if($scope.canPrev == true && $scope.canNext == false){
					$scope.main.stateObject.data = getStateData();
					$scope.parentSubmit();
				}
			}
			
			//取消
			$scope.cancel = function () {
				$scope.clearAllCache();
				$scope.cancelMain();
			};
			
			//获取缓存数据
			function getStateData(){
				var cache = $cacheFactory.get($scope.main.stateObject.state);
				var data = {};
				if(!angular.isUndefined(cache)){
					if(!angular.isUndefined(cache.get('data'))){
						data = cache.get('data');
					}
				}else{
					data = $scope.main.stateObject.data;
				}
				return data;
			}
			
			$scope.stateHandle = function(){
				if($scope.main.stateIndex < $scope.main.begin)
					$scope.main.stateIndex = $scope.main.begin;
				if($scope.main.stateIndex > $scope.main.end)
					$scope.main.stateIndex = $scope.main.end;
				
				if($scope.main.end <= $scope.main.begin){
					$scope.canPrev = false;
					$scope.canNext = false;
				}else if($scope.main.stateIndex == $scope.main.begin && $scope.main.end > $scope.main.stateIndex){
					$scope.canPrev = false;
					$scope.canNext = true;
				}else if($scope.main.stateIndex > $scope.main.begin && $scope.main.end > $scope.main.stateIndex){
					$scope.canPrev = true;
					$scope.canNext = true;
				}else if($scope.main.stateIndex > $scope.main.begin && $scope.main.end == $scope.main.stateIndex){
					$scope.canPrev = true;
					$scope.canNext = false;
				}
			};
		}],
	    link: function (scope, element, attrs, controller) {
	
	    }
	}
}]);