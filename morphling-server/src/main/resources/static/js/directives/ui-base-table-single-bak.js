//基本表格之单选
angular.module('app').directive('uiBaseTableSingleBak', ['$rootScope', '$anchorScroll','dbUtils', '$state', '$sce','$modal','$timeout', function($rootScope, $anchorScroll,dbUtils, $state, $sce,$modal,$timeout) {
	return {
		restrict: 'EA',
		templateUrl:'tpl/templates/uiBaseTableSingle.html',
		scope: {
			//初始化{loading: true,labelTitle:?,bodyStyle:?}
			tableParams:"=?",
			isShowCheckbox: "@",
			headers: "=",
			tooltipData: "=?",
			operationEvents: "=?",
			rowEvents: "=?",
			page: "=?",
			urlParams: "=?",//urlParams = Object.assign(scope.urlParams.urlBody,{currentPage:scope.page.pageNumber, onePageSize:scope.page.pageSize})
			initData:"=?",//初始化数据，如果有初始化数据，就不需要再查询了,优先级高于urlParams
			beforeReload: "=?",//重载前执行函数
			afterReload: "=?",//重载后执行函数
		},
		controller: ['$scope', function ($scope,$parent, el, attrs) {
		}],
		
		link: function (scope, elem, attrs) {
			//第一次页面初始化
			$timeout(function () {
				scope.coverStyle.height = $("#"+scope.t_id).height() + "px";
				initData();
			})

            /*  --------------------------------------------------
                ----------------数据初始化 start ----------------
                --------------------------------------------------*/
			scope.t_id = dbUtils.guid();
            scope.isShowCheckbox = (!scope.isShowCheckbox || scope.isShowCheckbox == "" || scope.isShowCheckbox == "0") ? false : true;
            scope.coverStyle = {"top": "34px", height:"30px"};
            scope.rowEvents = !scope.rowEvents ? [] : scope.rowEvents;

			if(scope.tableParams.labelTitle){
				scope.isHiddenTitle = false;
			}else{
				scope.isHiddenTitle = true;
				scope.mainStyle = {"border":"0px solid black"};
			}
			/*sort 使用footable后，自定义的排序不起作用 了*/
			scope.sort = {title : 'SID' ,desc : false};

			scope.orderIt = function(item){
				return item[scope.sort.title];
			};

			scope.headerClick = function(event,header){
				let flag = !header.desc;
				for(let item of scope.headers){
					item.desc = null;
				}
				header.desc = flag;
				scope.sort.desc = header.desc;
				scope.sort.title = header.field;
			}
			/*sort end*/

			/*表格页码*/
			scope.isShowFooter = false;
			if(scope.page && scope.page.isPagination){
                scope.isShowFooter = true;
                scope.pageSelect = [10, 20, 50, 100];
                if(!scope.page.postgroundPagination) {
                    scope.page.postgroundPagination = false;
                }
            }
			/*表格页码 end*/

            /*  --------------------------------------------------
                ----------------数据初始化 end ----------------
                --------------------------------------------------*/

			function initData(){
				scope.beforeReload && scope.beforeReload();
				scope.tableParams.loading = true;
				$timeout(function () {
					scope.coverStyle.height = $("#"+scope.t_id).height() + 10 + "px";
				})

				if(scope.initData && scope.initData.length > 0){//如果有传入初始数据，则不通过查询url获取数据
					scope.data = scope.responseData = scope.initData;
					if(scope.responseData && !scope.responseData[0].id){//如果id不存在，则遍历赋值
						scope.responseData.forEach((item, index) => {
							item.id = index;
						})
					}
					let totalPage = getTotalPage(scope.data);
					let pageParams = {totalResults:scope.data.length, currentPage:scope.page.pageNumber, onePageSize:scope.page.pageSize, totalPage:totalPage};
					let paginationData = Object.assign(pageParams,{data:scope.data});
					//处理Header
					headerHandle();
					//转换数据
					dataHandle();
					//loading消失
					scope.tableParams.loading = false;
					//分页处理
					if (scope.page && scope.page.isPagination)
						paginationHandle(paginationData);
					//加载后执行函数
					scope.afterReload && scope.afterReload();
				}else {
					getAction().then(function (response) {
						if (response.code == SUCCESS_CODE) {
							scope.responseData = response.data;
							var paginationData;//这个必须是数组数据,用来分页处理时使用
							if (scope.page && scope.page.isPagination && scope.page.postgroundPagination) {//后端分页
								paginationData = Object.assign({}, scope.responseData);
								scope.data = paginationData.data;
							}else if(scope.page && scope.page.isPagination && !scope.page.postgroundPagination){//前端分页
                                if(scope.responseData && scope.responseData[0] && !scope.responseData[0].id){//如果id不存在，则遍历赋值
                                    scope.responseData.forEach((item, index) => {
                                        item.id = index;
                                    })
                                }
								let totalPage = getTotalPage(scope.responseData);
								let pageParams = {totalResults:scope.responseData.length, currentPage:scope.page.pageNumber, onePageSize:scope.page.pageSize, totalPage:totalPage};
								paginationData = Object.assign(pageParams,{data:scope.responseData});
								scope.data = paginationData.data.slice((scope.page.pageNumber-1)*scope.page.pageSize, scope.page.pageNumber*scope.page.pageSize);
							}else {
								scope.data = scope.responseData.slice(0);
							}
							//处理Header
							headerHandle();
							//转换数据
							dataHandle();
							//loading消失
							scope.tableParams.loading = false;
							//分页处理
							if (scope.page && scope.page.isPagination)
								paginationHandle(paginationData);
							//加载后执行函数
							scope.afterReload && scope.afterReload();
						} else {
							scope.tableParams.loading = false;
							dbUtils.error(response.message)
						}
					}, function (result) {
						scope.tableParams.loading = false;
					});
				}
			}

			function getAction(){
				let action,urlParams = scope.urlParams.urlBody;
				if(scope.page)
					urlParams = Object.assign(scope.urlParams.urlBody,{currentPage:scope.page.pageNumber, onePageSize:scope.page.pageSize});

				if(!scope.urlParams.method || scope.urlParams.method == "get"){
					action = dbUtils.get(scope.urlParams.url, urlParams)
				}else if(scope.urlParams.method == "post"){
					action = dbUtils.post(scope.urlParams.url, urlParams)
				}else if(scope.urlParams.method == "postBody"){
					action = dbUtils.postBody(scope.urlParams.url, JSON.stringify(urlParams))
				}
				return action;
			}
			//处理Header
			function headerHandle(){
				if(!scope.headers && scope.data && scope.data.length > 0){
					scope.headers = [];
					let headers = Object.keys(scope.data[0]);
					for(let header of headers){
						scope.headers.push({field:header});
					}
				}
			}

			//转换数据
			function dataHandle(){
				scope.rows = [];
				for(let i in scope.data){
					// let _row = Object.assign({},scope.data[i]);//不往scope.data[i]中添加index属性
					let _row = scope.data[i];
					if(!_row.id)
						_row.id = i;
					_row.index = i;
					let row = {index:_row.index, id:_row.id};
					for(let header of scope.headers){
						var value = _row[header.field];// + ''
						if(header.formatter){
							value = header.formatter(value,_row);
						}
						if(header.compile && !header.directive){
							value += "";
							value = $sce.trustAsHtml(value);
						}
						row[header.field] = value;
					}
					scope.rows.push(row);
				}
			}

			function getTotalPage(data){
				if(!data || data.length == 0)
					return 1;
				let totalPage;
				let i = parseInt(data.length / scope.page.pageSize);//整数部分
				let j = data.length % scope.page.pageSize;//余数部分
				if(i == 0){
					totalPage = ++i;
				}else if(i > 0 && j == 0){
					totalPage = i;
				}else{
					totalPage = ++i;
				}
				return totalPage;
			}

			//监控父类发送的刷新
			scope.$on('refreshData', function(event,data) {scope.operations.refreshData();});
			scope.$on('reloadData', function(event,data) {scope.operations.refreshData();});

			//获取选中的数据
			scope.$on('getSelectedData', function(event,data) {
				scope.$emit('sendSelectedData',getAllSelectRows());
			});

			//获取选中的数据，并且返回入参数据
			scope.$on('getSelectedItem', function(event,data) {
				scope.$emit('sendSelectedItem',Object.assign(data,{rows:getAllSelectRows()}));
			});

			//监听每页数量切换
			/*scope.$watch('page.pageSize',function(current,old){
				if(current != old){
					$("#"+scope.t_id+" #baseTable").attr("data-page-size",current);//动态修改table的pageSize的值
					if(scope.page &&　scope.page.isPagination ){//当有分页时
						setTimeout(function () {  $("#"+scope.t_id+" #baseTable").trigger('footable_redraw');}, 100);
					}
				}
			})*/

            //监听表格最后一行渲染完成
			/*scope.$on("TableLoadOver",function(){
                $timeout(function () {
                    scope.tableParams.loading = false;
                },200)

                scope.coverStyle.height = $("#"+scope.t_id+" tbody").height() + "px";
			})*/

			scope.operations = {
				emit: function (event,data) {
					scope.$emit(event,data);
				},
				refreshData: function(){
					if(scope.page && scope.page.isPagination == true ){
						scope.page.pageNumber = 1;
					}
					//发射刷新信号
					// this.emit("refresh",{});
					initData();
				},
				pageSizeChange: function(){
					if(scope.page && scope.page.isPagination && scope.page.postgroundPagination)//后台分页
						initData();
					else if(scope.page && scope.page.isPagination && !scope.page.postgroundPagination){//前台分页
					    scope.page.pageNumber = 1;
						frontPaginationHandle();
                    }
				},
                queryData: function(){
                    // this.emit("refresh",{});
					initData();
                },
             	// grid上方按钮点击事件
				operationButtonClick: function(clickFun){
					var rows = getAllSelectRows();
					clickFun(rows);
					scope.checkedRow();
				},
				checkAllowSelect: function(){
					scope.checkedRow();
				},
				pageNumberClick: function(pageNumber){
					var prevPage = scope.page.prevPageDisabled;
					if (pageNumber === "prev" && prevPage && prevPage != "") {
						return false;
					}
					var nextPage = scope.page.nextPageDisabled;
					if (pageNumber === "next" && nextPage && nextPage != "") {
						return false;
					}
					if (pageNumber == scope.page.pageNumber) {
						return false;
					}
					if (pageNumber === "...") {
						return false;
					}
					if (pageNumber === "prev") {
						scope.page.pageNumber--;
					} else if (pageNumber === "next") {
						scope.page.pageNumber++;
					} else {
						scope.page.pageNumber = pageNumber;
					}
					//重新查询一次
					//重新查询一次
					if(scope.page && scope.page.isPagination && scope.page.postgroundPagination)//后台分页
						this.queryData();
					else if(scope.page && scope.page.isPagination && !scope.page.postgroundPagination)//前台分页
						frontPaginationHandle();
				}
			}

			//前台分页
			function frontPaginationHandle(){
				let totalPage = getTotalPage(scope.responseData);
				let pageParams = {totalResults:scope.responseData.length, currentPage:scope.page.pageNumber, onePageSize:scope.page.pageSize, totalPage:totalPage};
				let paginationData = Object.assign(pageParams,{data:scope.responseData});
				scope.data = paginationData.data.slice((scope.page.pageNumber-1)*scope.page.pageSize, scope.page.pageNumber*scope.page.pageSize);
				//转换数据
				dataHandle();
				//分页处理
				paginationHandle(paginationData);
				//加载后执行函数
				scope.afterReload && scope.afterReload();
			}

			function paginationHandle(data){
				//分页数据处理
				var pages = {};
				pages.totalRecords = parseInt(data.totalResults);
				pages.pageNumber = parseInt(data.currentPage);
				pages.pageSize = parseInt(data.onePageSize);
				pages.totalPages = parseInt(data.totalPage);
				var totalPage = pages.totalPages;

				//分页算法，页面只显示固定数量的分页按钮。
				var pageNumbers = [];
				var startPage = 1;
				var endPage = totalPage;
				var pageStep = 2;//以当前页为基准，前后各显示的页数量
				if (totalPage >= 6) {
					startPage = pages.pageNumber;
					if (startPage >= pageStep) {
						startPage -= pageStep;
					}
					if (startPage <= 1) {
						startPage = 1;
					}
					endPage = (totalPage - pages.pageNumber) >= pageStep ? pages.pageNumber + pageStep : totalPage;
					if (endPage > totalPage) {
						endPage = totalPage;
					}
					if (startPage != 1) {
						pageNumbers.push({number: "1"});
						if (startPage - 1 != 1) {
							pageNumbers.push({number: "...", disabled: "disabled"});
						}
					}
				}
				for (var i = startPage; i <= endPage; i++) {
					if (i == pages.pageNumber) {
						pageNumbers.push({number: i, active: "active"});
					} else {
						pageNumbers.push({number: i});
					}
				}
				if (endPage != totalPage) {
					if (endPage + 1 != totalPage) {
						pageNumbers.push({number: "...", disabled: "disabled"});
					}
					pageNumbers.push({number: totalPage});
				}
				pages.pageNumbers = pageNumbers;
				if (pages.pageNumber == 1 || pages.totalPages == 0) {
					pages.prevPageDisabled = "disabled";
				}
				if (pages.pageNumber == totalPage || pages.totalPages == 0) {
					pages.nextPageDisabled = "disabled";
				}
				scope.page.prevPageDisabled = pages.prevPageDisabled;
				scope.page.nextPageDisabled = pages.nextPageDisabled;
				angular.extend(scope.page,pages);
			}

			// -----------------------------表格全选/反选  start------------------------------------------
			scope.thead = {};
			//每行点击事件
			scope.checkedRow = function(row){
				let flag = row.checked;
				for (let i=0; i<scope.rows.length; i++) {
					scope.rows[i].checked = false;
				}
				row.checked = flag;
			}

			//获取所有选中的行数据
			function getAllSelectRows() {
				var rows = [];
				angular.forEach(scope.rows, function (row,i) {
					if (row.checked) {
						for(let i=0;i<scope.data.length;i++){
							if(row.id == scope.data[i].id)
								rows.push(scope.data[i]);
						}
					}
				});
				return rows;
			}
			// -----------------------------日志表格全选/反选  end------------------------------------------
		},
	}
}]);