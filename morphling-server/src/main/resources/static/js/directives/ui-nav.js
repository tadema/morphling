/**
 * Created by   on 2016/11/8.
 */
'use strict';

angular.module("app")
    .directive("uiNav", ['$rootScope','$state','$stateParams', function ($rootScope,$state,$stateParams) {
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
   	});
        //这里规则可能会特别复杂
    	var stateUrlObj = {};
        var $menus = [];
        var _tpl = "";
        if($rootScope && $rootScope._userinfo){
            $menus = $rootScope._userinfo.menus;
            for (var i = 0; i < $menus.length; i++) {
                _tpl += _buildBranchTemplate($menus[i]);
            }
        }
        $rootScope.stateUrlObj = stateUrlObj;
        return {
            template: "<ul id=\"menu\" class=\"nav\">" + _tpl + "</ul>",
            controller: ['$scope', function ($scope, el, attrs) {
			    	$scope.addTab = function(route){
			    		let node = stateUrlObj[route];
//			    		var search = $location.search();
			    		var _path = $state.current.templateUrl, title = node.text,from = node.route, theme = '', icon = "", hash = node.route;
			    		if (!icon)icon = "bookmark-outline";
			    		if ($scope.appCfg.openInsetTab == 1) {//_path && title &&
			    			if(route == "app.deploy"){
			    				$state.go("app.selectAppDeploy");
			    			}else if(route == "app.vision"){
			    				$state.go("app.selectAppVision");
			    			}else if(route == "app.logtable"){
			    				$state.go("app.selectAppLogtable");
			    			}else{
			    				$state.go(node.route);
			    			}
//			    			$scope.appCfg.tabManager.addTab(_path, title, icon, hash, theme);
			    		} else if ($scope.appCfg.openInsetTab != 1 && _path) {
			    			$scope.PAGEURL = _path;
			    			if (theme) {
			    				$scope.appCfg.specialTheme = theme;
			    			} else {
			                  $scope.appCfg.specialTheme = '';
			    			}
			    		}
			    	}

            }],
    	    link: function (scope, element, attrs, controller) {
    	    }
        }

        /**
         * 构建父菜单的html
         * @param node
         * @returns {string}
         * @private
         */
        function _buildBranchTemplate(node) {
            if (node.type == 1 && !node.children ) {//生成最底层菜单
                let idTemp = node.route.replace(".","-");
                return "<li ui-sref-active=\"active\" id=\"" + idTemp + "\">" +
                    "<a ng-click=\"addTab('"+node.route+"')\" >" +//ui-sref=\"" + node.route + "\"   ng-click=\"addTab('"+node.route+"')\"
                    "<i class=\"" + node.icon + " text-silver \"></i>" +
                    "<span  " + ((node.translate)?("translate=\""+node.translate+"\""):"")+">"+node.text+"</span>" +
                    "</a>" +
                    "</li>";
            } else if (node.type == 1) {//生成父级菜单
                var _tpl = "";
                _tpl += "<li id=\"" + node.id + "\">" +
                    "<a href class=\"auto\">" +
                    "<span class=\"pull-right text-muted\">" +
                    "<i class=\"fa fa-fw fa-angle-right text\"></i>" +
                    "<i class=\"fa fa-fw fa-angle-down text-active\"></i>" +
                    "</span>" +
                    "<i class=\"" + node.icon + " text-silver \"></i>" +
                    "<span  " + ((node.translate)?("translate=\""+node.translate+"\""):"")+ ">" + node.text + "</span>" +
                    "</a>" +
                    "<ul class=\"nav nav-sub dk\">" +
                    "<li class=\"nav-sub-header\">" +
                    "<a href>" +
                    "<span  " + ((node.translate)?("translate=\""+node.translate+"\""):"")+ ">" + node.text + "</span>" +
                    "</a>" +
                    "</li>";
                var _childrenTpl = "";
                for (var i = 0; i < node.children.length; i++) {
                	stateUrlObj[node.children[i].route] = node.children[i];
                    _childrenTpl += _buildChildrenTemplate(node.children[i]);
                }
                _tpl += _childrenTpl;
                _tpl += "</ul></li>"
                return _tpl;
            } else if (node.type == 2) {
                return "<li class=\"hidden-folded padder m-t m-b-sm text-muted text-xs\">" +
                    "<span  " + ((node.translate)?("translate=\""+node.translate+"\""):"")+ ">" + node.text + "</span>" +
                    "</li>";
            } else if (node.type == 3) {
                // return "<li class=\"line dk\"></li>";
                return "<li></li>";
            } else if (node.type == 4) {
                return node.html;
            }else{
                 return "";
            }
        }

        /**
         * 构建子菜单的html
         * @param node
         * @returns {*}
         * @private
         */
        function _buildChildrenTemplate(node) {
            if (node.type != 0) {
                return _buildBranchTemplate(node);
            } else {
                var _tpl = "<li ui-sref-active=\"active\">" +
                    "<a ui-sref=" + node.route + ">" +
                    "<span  " + ((node.translate)?("translate=\""+node.translate+"\""):"")+ ">" + node.text + "</span>" +
                    "</a></li>";
                return _tpl;
            }
        }

    }]);