'use strict';

app.directive('configFileTabs', function ($rootScope,$state,$compile,$modal,ngTreetableParams) {
    return {
        restrict: 'EA',
        replace: true,
        templateUrl:'tpl/logtable/log_testtabletree.html',
        controller:function($scope,ngTreetableParams){
        	var data = [
                {
                    name: 'Some Folder',
                    type: 'folder',
                    size: '',
                    children: [
                        {
                            name: 'some file.pdf',
                            type: 'folder',
                            size: '500KB',
                            children: [
                            	{
                                    name: 'another_file1.doc',
                                    type: 'file',
                                    size: '44KB'
                                }
                            ]
                        },
                        {
                            name: 'another_file.doc',
                            type: 'file',
                            size: '344KB'
                        }
                    ]
                }
            ];

            $scope.dynamic_params = new ngTreetableParams({
                getNodes: function(parent) {
                	return parent ? parent.children : data;
                },
                getTemplate: function(node) {
                    return 'tree_node';
                },
                options: {
                	initialState: 'collapsed',
                    onNodeExpand: function() {
                        console.log('A node was expanded!');
                    }
                }
            });

            $scope.expanded_params = new ngTreetableParams({
                getNodes: function(parent) {
                    return parent ? parent.children : data;
                },
                getTemplate: function(node) {
                    return 'tree_node';
                },
                options: {
                    initialState: 'expanded'
                }
            });
        }
    }
});
