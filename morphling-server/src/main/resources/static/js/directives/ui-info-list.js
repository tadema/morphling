angular.module('app').directive('uiInfoList', ['$rootScope', 'dbUtils', '$anchorScroll', '$sce','$timeout', function($rootScope, dbUtils, $anchorScroll,$sce,$timeout) {
	return {
		restrict: 'EA',
		templateUrl:'tpl/templates/uiInfoList.html',
		scope: {
			//初始化{loading: true,labelTitle:?,bodyStyle:?}
			listParams: "=?",
            rowClickEvent: "&?",
			filter: "=?",
			headers: "=",
			data: "="
		},
		controller: ['$scope', function ($scope, el, attrs) {
		}],
		link: function (scope, elem, attrs) {
			scope.$watch('data',function(newValue,oldValue){
				if(newValue != oldValue){
					initData();
				}
			},true)

			$timeout(function () {
				scope.coverStyle.height = parseFloat(scope.listParams.bodyStyle.height)+"px";
			})

			/*数据初始化 start*/
			scope.filter = {};
			scope.coverStyle = {"top":"71px","height":parseFloat(scope.listParams.bodyStyle.height)+"px"};
			scope.isShowFooter = true;
			if(scope.isShowFooter){
				scope.page = {pageSize:50};
				scope.pageSelect = [20,50,100];
			}
			/*数据初始化 end*/

			function initData(){
				scope.changeNum = 0;
				scope.rows = scope.data;
				$timeout(function(){
					for(let i=0;i<scope.rows.length;i++){
						if(scope.rows[i].isChange == 1)
							++scope.changeNum;
					}
					// scope.operations.emit("countChange",{total:scope.rows.length,change:scope.changeNum});
					$("#"+scope.t_id+" #baseTable").attr("data-page-size",scope.rows.length);
				},200)
			}
			initData();

			function dataHandle(){
				for(let i in scope.data){
					let _row = scope.data[i];
					if(!_row.id)
						_row.id = i;
					_row.index = i;
					let row = {index:_row.index, idValue:"button"+i};
					//添加选中行样式
					if(_row.isChange){
                        row.class = _row.isChange == 1 ? "cmp-different-left" : "cmp-same";
                        if(_row.isChange == 1)
                        	console.log(_row.filePath);
                    }
					for(let header of scope.headers){
						var value = _row[header.field];// + ''
						if(header.formatter){
							value = header.formatter(value,_row);
						}
						if(header.compile){
							value += "";
							value = $sce.trustAsHtml(value);
						}
						row[header.field] = value;
					}
					scope.rows.push(row);
				}
			}

			//为其他条件赋值
			scope.setCondition = function(labelName, labelValue){
				scope.loading = true;
				scope.filter[labelName] = labelValue;
				$timeout(function(){
					scope.loading = false;
				},200)
			}

			scope.$on("setSelect",function(event,data){
				scope.rows[data.index].isSelected = data.isSelected;
			})

			//定义事件对象
			scope.operations = {
				refreshData: function(){
					this.emit("refresh",{});
				},
				emit: function (event,data) {
					scope.$emit(event,data);
				}
			};

			/*多选框 start*/
			scope.thead = {};
			//每行点击事件
			scope.checkedRow = function(tdata){
				checkAllrow(scope.rows);
			}

			//检测是否全部都选中
			function checkAllrow(data){
				var flag = true;
				if (!data || data.length == 0) {
					flag = false;
				}
				for (var i in data) {
					if(!data[i].checked){
						flag = false;
					}
				}
				scope.thead.allrow = flag;
			}

			//点击全选复选框事件
			scope.allRowCheck = function(){
				for(var i=0;i<scope.rows.length;i++){
					scope.rows[i].checked = scope.thead.allrow;
				}
			}

			//获取所有选中的行数据
			function getAllSelectRows() {
				var rows = [];
				angular.forEach(scope.rows, function (row,i) {
					if (row.checked) {
						for(let i of scope.data){
							if(row.index == i.index)
								rows.push(i);
						}
					}
				});
				return rows;
			}
			/*多选框 end*/
		}
	}
}]);