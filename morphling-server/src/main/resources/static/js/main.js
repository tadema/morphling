'use strict';

/* Controllers */
angular.module('app').controller('AppCtrl', ['$rootScope','$scope', '$translate', '$localStorage','$location', '$state','$window','$modal','AccountService','dbUtils','$timeout','$cookieStore','driver',
	function ($rootScope,$scope, $translate, $localStorage ,$location, $state,$window,$modal,accountService,dbUtils,$timeout,$cookieStore,driver) {
    // add 'ie' classes to html
    var isIE = !!navigator.userAgent.match(/MSIE/i);
    isIE && angular.element($window.document.body).addClass('ie');
    isSmartDevice($window) && angular.element($window.document.body).addClass('smart');
    // config
    $scope.app = {
        name: '统一部署与运维平台',//
        version: version ? version : '2.0.0',//获取version.js中数据
        copyrightYear: 2020,
        color: {
            primary: '#7266ba',
            info: '#23b7e5',
            success: '#27c24c',
            warning: '#fad733',
            danger: '#f05050',
            light: '#e8eff0',
            dark: '#3a3f51',
            black: '#1c2b36'
        },
        settings: {
            themeID: 11,
            navbarHeaderColor: 'bg-black',
            navbarCollapseColor: 'bg-white',//bg-white
            asideColor: 'bg-black',
            navbarHeaderLogo:'img/logo-in-black.png',
            headerFixed: true,
            asideFixed: false,
            asideFolded: false,
            asideDock: false,
            container: false
        }
    }
    
    $rootScope.consts = {
        middlewareType : [{name:"springboot",value:"springboot"},{name:"weblogic",value:"weblogic"},{name:"tomcat",value:"tomcat"},{name:"nginx",value:"nginx"}]
    }

    // save settings to local storage
    if (angular.isDefined($localStorage.settings)) {
        $scope.app.settings = $localStorage.settings;
    } else {
        $localStorage.settings = $scope.app.settings;
    }
    $scope.$watch('app.settings', function () {
        if ($scope.app.settings.asideDock && $scope.app.settings.asideFixed) {
            // aside dock and fixed must set the header fixed.
            $scope.app.settings.headerFixed = true;
        }
        // save to local storage
        $localStorage.settings = $scope.app.settings;
    }, true);
    
    // angular translate
    $scope.lang = {isopen: false};
    //语言设置
    $scope.langs = {cn: 'Chinese', en: 'English', de_DE: 'German', it_IT: 'Italian'};
    $scope.selectLang = $scope.langs[$translate.proposedLanguage()] || "English";
    $scope.setLang = function (langKey, $event) {
        // set the current lang
        $scope.selectLang = $scope.langs[langKey];
        // You can change the language during runtime
        $translate.use(langKey);
        $scope.lang.isopen = !$scope.lang.isopen;
    };

    function isSmartDevice($window) {
        // Adapted from http://www.detectmobilebrowsers.com
        var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
        // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
        return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
    }

    //步骤指引
    $scope.guideHandle = function(event,param){
        event.stopPropagation();
        driver.startGuide(param);
    }
    
    $scope.switchEnv = function (key) {
        if(key == $rootScope._userinfo.currentEnv){
            return;
        }
        var currentEnv = accountService.switchEnv($rootScope._userinfo.envs,key);
        var envName = "";
        for(var i in $rootScope._userinfo.envs){
            if($rootScope._userinfo.envs[i].key == currentEnv){
                envName = $rootScope._userinfo.envs[i].name;
                break;
            }
        }
        $state.go("app.home");
        dbUtils.success("操作成功，环境已经切换为【"+envName+"】!");
        setTimeout(function(){
            window.location.reload();
        },200);
    };
    
    $scope.alarmNum = 0;
    $scope.getAlertInfo = function(){
		accountService.listAlertInfo(1,5).then(function(response){
    		if(response.code == SUCCESS_CODE){
    			$scope.alarmInfos = response.data.data.slice(0,5);
    			$scope.alarmNum = $scope.alarmInfos.length; 
    			for(var i in $scope.alarmInfos){
    				if($scope.alarmInfos[i].severity == 'yellow')
    					$scope.alarmInfos[i].style = {'border-left-color':'#fad733'};
    				else if($scope.alarmInfos[i].severity == 'orange')
    					$scope.alarmInfos[i].style = {'border-left-color':'#f47f7f'};
    				else if($scope.alarmInfos[i].severity == 'red')
    					$scope.alarmInfos[i].style = {'border-left-color':'#f05050'};
    			}
    		}else{
    			dbUtils.error(response.message,"提示");
    		}
    	})
    	
    }
    $scope.$on("alarmQuery", function(event,data) {
    	$scope.getAlertInfo(1,5);
    })
	
	$scope.$watch('alarmNum',function(newValue,oldValue){
		if(oldValue != newValue){
			$("#bell").each(function() {
				// 目前只设置兼容谷歌浏览器
				$(this)[0].addEventListener("webkitAnimationEnd",function(){
			    	// 动画结束就直接去除这个动画
			    	$(this).css("animation","");
				});
			});
			$("#bell").css("animation","alarm .5s ease-in-out 2");
		}
	})
	
    $scope.goAlarmMenu = function(){
    	$state.go('app.alarmManagement');
    }
    
    var me = this;
    $scope.appCfg = {
        title: 'AdmUI',
        loading: false,
        specialTheme: '',
        currentTheme: localStorage.getItem('app_current_theme') || 'white',  //default theme
        isFullScreen: localStorage.getItem('app_full_screen') || '0',
        currentLayout: localStorage.getItem('app_current_layout') || '1',
        footerEnabled: localStorage.getItem('app_footer_enabled') || '1',
        openInsetTab: localStorage.getItem('app_open_inset_tab') || '1',
        sidebarToggle: false,
        themeList: [
            'yellow','bluegrey','teal','grey','green','orange','blue','purple','red','pink','indigo'
        ],
        fullScreen: function () {
            if ($scope.appCfg.isFullScreen == '1') {
                $scope.appCfg.isFullScreen = '0';
                $scope.appCfg.currentLayout = '1';
                $scope.appCfg.sidebarToggle = true;
                $scope.appCfg.footerEnabled = '1';
            } else {
                $scope.appCfg.isFullScreen = '1';
                $scope.appCfg.currentLayout = '0';
                $scope.appCfg.sidebarToggle = false;
                $scope.appCfg.footerEnabled = '0';
                $scope.appCfg.openInsetTab = '1';
            }
        },

        closeSidebar: function (e) {
            $scope.appCfg.sidebarToggle = false;
        },

        switchTheme: function (color) {
            $scope.appCfg.currentTheme = color;
            localStorage.setItem('app_current_theme', color);
        },
        tabManager: TabManager
    };
    
    $scope.goToHome = function(){
    	$state.go("app.home");
    	$scope.appCfg.tabManager.addTab("tpl/user/home.html", "首页", "bookmark-outline", "app.home","", "");
    }
    $rootScope.stateUrlObj = {};
    
	$scope.routeArray = ['app.deploy','app.customDeploy','app.logtable'];
    //处理改变地址栏和前后页的情况
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) { 
    	if(toState.name == "app.home"){
			$scope.appCfg.tabManager.addTab("tpl/user/home.html", "首页", "bookmark-outline", "app.home","", "");
		}else if(!angular.equals({},$rootScope.stateUrlObj)){//当该地址栏的时候会执行
			//当路由为应用发布和自定义应用发布，以及日志管理时，先把之前的页面删除掉
			if($scope.routeArray.indexOf(toState.name) != -1){
				if(toState.name == 'app.logtable'){
					let title = $rootScope.stateUrlObj[toState.name] ? $rootScope.stateUrlObj[toState.name].text : '应用发布';
					let key = $.md5(toState.templateUrl + title);
					let keyParam = $.md5(toState.templateUrl + title + JSON.stringify(toParams));
					let tab = $scope.appCfg.tabManager.findTabByKeyAndParam(key,keyParam);
					if(tab){
						var index = $scope.appCfg.tabManager.TABS.indexOf(tab);
		            	$scope.appCfg.tabManager.TABS.splice(index, 1);
		            	$scope.appCfg.tabManager.SELECTEDINDEX = $scope.appCfg.tabManager.TABS.length-1;
					}
				}else{
					let title = $rootScope.stateUrlObj[toState.name] ? $rootScope.stateUrlObj[toState.name].text : '应用发布';
					let keyParam = $.md5(toState.templateUrl + title + JSON.stringify(toParams));
		    		let tab = $scope.appCfg.tabManager.findTabByRole("c",toParams.roleId);
		    		if(tab && keyParam != tab.keyParam){//当Tab和toParams.name都属于deploy$scope.routeArray，且Tab和toParams.name不相等 && deploy$scope.routeArray.indexOf(toState.name) != -1 &&　toState.name　!= tab.hash  
		        		var index = $scope.appCfg.tabManager.TABS.indexOf(tab);
		            	$scope.appCfg.tabManager.TABS.splice(index, 1);
		            	$scope.appCfg.tabManager.SELECTEDINDEX = $scope.appCfg.tabManager.TABS.length-1;
		    		}
				}
				return;
			}
			
    		let menu = $rootScope.stateUrlObj[toState.name];
    		if(menu){
	    		$scope.appCfg.tabManager.addTab(toState.templateUrl, menu.text, "bookmark-outline", menu.route,toParams, "");
    		}else if(!menu && toState.name == "app.home"){
    			$scope.appCfg.tabManager.addTab("tpl/user/home.html", "首页", "bookmark-outline", "app.home","", "");
    		}else if(!menu){
    			let params = toParams;
    			$scope.appCfg.tabManager.addTab(toState.templateUrl, toParams.title, "bookmark-outline", toState.name,params, "");
    		}
    	}
   	});

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
    	//刷新时，将缓存的Tab页都重新加载一遍（取消了，刷新时，加载js从$scope.appCfg.tabManager.TABS中获取参数信息，所以不需要重新加载了）
    	//不是刷新时，执行的操作 
		//父子页面的情况（这种情况已经被ng-include解决）
    	if(toParams.role){
    		let reverseRole = toParams.role=="c"?"p":"c";
    		let tab = $scope.appCfg.tabManager.findTabByRole(reverseRole,toParams.roleId);
    		if(tab){
    			var index = $scope.appCfg.tabManager.TABS.indexOf(tab);
    			$scope.appCfg.tabManager.TABS.splice(index, 1);
    		}
    	}
        
        //当路由为应用发布和自定义应用发布，以及日志管理时，在这里添加tab
        if($scope.routeArray.indexOf(toState.name) != -1){
        	if(!angular.equals({},$rootScope.stateUrlObj)){
            	let menu = $rootScope.stateUrlObj[toState.name];
        		if(menu){//应用发布，日志管理
        			$scope.appCfg.tabManager.addTab(toState.templateUrl, menu.text, "bookmark-outline", menu.route,toParams, "");
        		}else{//自定义应用发布 
        			$scope.appCfg.tabManager.addTab(toState.templateUrl, toParams.title, "bookmark-outline", toState.name,toParams, "");
        		}
            }
        }
    });

    function isJson(str) {
        if (typeof str == 'string') {
            try {
                var obj=JSON.parse(str);
                if(typeof obj == 'object' && obj ){
                    return true;
                }else{
                    return false;
                }
            } catch(e) {
                return false;
            }
        }
    }
    
    if($window.sessionStorage.getItem("tabs") && isJson($window.sessionStorage.getItem("tabs"))){
    	$scope.appCfg.tabManager.SELECTEDINDEX = parseInt($window.sessionStorage.getItem("tabIndex"));//取的值是字符串类型的数字
    	let tabs = JSON.parse($window.sessionStorage.getItem("tabs"));
    	$scope.appCfg.tabManager.TABS.push(tabs[0]);
    	//刷新后，保留首页和当前页tab，清除其余tab页
    	if($scope.appCfg.tabManager.SELECTEDINDEX > 0){
    		var tab = tabs[$scope.appCfg.tabManager.SELECTEDINDEX];
    		$scope.appCfg.tabManager.TABS.push(tab);
    	}
    }else{
    	$scope.appCfg.tabManager.TABS = [];
    	$scope.appCfg.tabManager.SELECTEDINDEX = 0;	
    	window.location.href = "#";
    }
    
    $scope.$on("deleteCurrentTab",function(event,data){
    	let tab = $scope.appCfg.tabManager.TABS[$scope.appCfg.tabManager.SELECTEDINDEX];
    	$scope.appCfg.tabManager.removeOnly(tab);
    })
    
    $scope.$watch('appCfg.tabManager',function(current,old){
    	if((current.TABS.length + current.SELECTEDINDEX) != (old.TABS.length + old.SELECTEDINDEX)){
    		$window.sessionStorage.removeItem("tabs");
        	$window.sessionStorage.removeItem("tabIndex");
        	$window.sessionStorage.setItem("tabs", JSON.stringify(current.TABS));
        	$window.sessionStorage.setItem("tabIndex", current.SELECTEDINDEX);
    		
    		if ($scope.PAGEURL) return;
    		var tab = $scope.appCfg.tabManager.TABS[$scope.appCfg.tabManager.SELECTEDINDEX];
    		
    		if(tab &&　current.TABS.length == old.TABS.length){//判定为标签内部切换
    			$state.go(tab.hash,tab.params);
                if(driver.getIsRunning() && driver.getWatingClickMenu()){
                    setTimeout(function () {//300ms延时给导航栏跳转预留时间
                        driver.startTourGuide()
                    },300)
                }
    		}
    		
    		if(current.TABS.length == 0 && old.TABS.length > 0){//判定为清楚所有
    			$scope.appCfg.tabManager.addTab("tpl/user/home.html", "首页", "bookmark-outline", "app.home","", "");
    		}
    	}
    },true);

    //监控页面加载成功，driver.js
    $scope.$on('pageLoadSuccess', function(event,data) {
        if(driver.getIsRunning() && driver.getWatingClickMenu()){
            //应用发布页，先进入选择应用页，然后才跳到应用发布页。选择应用页禁用了onNext事件，所以监听用户的点击事件，如果监听到了说明用户点击了应用，否则没有点击应用
            if(driver.getIsWatingClickView())
                driver.clickedViewHandle();//执行stateIndex加一的操作，使程序继续往下进行
            driver.resetDriver(true);//清除指向menu的highlight,200ms延时后再执行definedSteps，不加延时存在二次使用时definedsteps加载不出来的情况
            setTimeout(function () {
                driver.startTourGuide()
            },300)
        }
    })
    
    $scope.$watchGroup(['appCfg.currentLayout', 'appCfg.footerEnabled', 'appCfg.isFullScreen', 'appCfg.openInsetTab'], function (newValues, oldValues, scope) {
        localStorage.setItem('app_current_layout', newValues[0]);
        localStorage.setItem('app_footer_enabled', newValues[1]);
        localStorage.setItem('app_full_screen', newValues[2]);
        localStorage.setItem('app_open_inset_tab', newValues[3]);
        if (newValues[3] != oldValues[3]) {
//            window.location.href = '#';
        }
    });
    
    //tabmenu右键事件
    $scope.showContextMenu = function(event){
    	event = event.target;
    	$(event).siblings("md-menu").find("i").triggerHandler('contextmenu');
    }
    
    $scope.$on('$viewContentLoaded', function() {  
    	 //加屏幕高度，宽度信息 start
    	$rootScope._screenProp = {};
    	$rootScope._screenProp.HeightValue = $window.innerHeight;
    	$rootScope._screenProp.WidthValue = $window.innerWidth;
    	$rootScope._screenProp.bottomValue = 48.6;//parseInt($(".app-content-body").css("padding-bottom"))
    	$rootScope._screenProp.leftValue = parseInt($(".app-content").css("margin-left")); 
    	$rootScope._screenProp.navbarHeightValue = 50;
    	$rootScope._screenProp.titleHeightValue = 32;//菜单栏高度
    	let bodyHeight = $rootScope._screenProp.HeightValue-$rootScope._screenProp.navbarHeightValue-$rootScope._screenProp.titleHeightValue-$rootScope._screenProp.bottomValue;
    	$(".app-content").css("height",bodyHeight+"px");
    	
    	$rootScope._screenProp.contentHeight = Math.floor(bodyHeight);
    	$rootScope._screenProp.contentWidth = Math.floor($rootScope._screenProp.WidthValue - $rootScope._screenProp.leftValue);
    	
//    	$("#"+$scope.t_id+" #footable thead").height() 被替换
    	$rootScope._screenProp.tableUselessHeight1 = 41+43+34+52+20;//前台分页形式；查询-按钮-表头-表尾-空隙
    	$rootScope._screenProp.tableUselessHeight11 = 41+43+27.6+34+52+20;//前台分页形式；查询-提示-按钮-表头-表尾-空隙
    	$rootScope._screenProp.tableUselessHeight2 = 41+43+34+56+20;//后台分页形式；查询-按钮-表头-表尾-空隙
    	$rootScope._screenProp.tableUselessHeight21 = 41+27.6+43+34+56+20;//后台分页形式；查询-提示-按钮-表头-表尾-空隙
    	$rootScope._screenProp.tableUselessHeight3 = 74+41+43+34+56+20;//统计栏后台分页形式；统计栏-查询-按钮-表头-表尾-空隙
    	$rootScope._screenProp.tableUselessHeight31 = 74+41+27.6+43+34+56+20;//统计栏后台分页形式；统计栏-查询-提示-按钮-表头-表尾-空隙
    	$rootScope._screenProp.tableUselessHeight4 = 40+20+41+78+34+52+20;//标题前台分页形式；标题-空隙-查询-（按钮导航栏）-表头-表尾-空隙
    	$rootScope._screenProp.tableUselessHeight41 = 40+20+41+27.6+78+34+52+20;//标题前台分页形式；标题-空隙-查询-提示-（按钮导航栏）-表头-表尾-空隙
    	$rootScope._screenProp.tableUselessHeight5 = 40+20+41+43+34+52+20;//标题前台分页形式；标题-空隙-查询-按钮-表头-表尾-空隙
    	$rootScope._screenProp.tableUselessHeight10 = 40+20+41+27.6+43+34+52+20;//标题前台分页形式；标题-空隙-查询-提示-按钮-表头-表尾-空隙
    	$rootScope._screenProp.tableUselessHeight6 = 162+15+40+20+41+28+34+52+20;//tomcat/weblogic关联；已部署-空隙-panelheader-空隙-查询-提示-表头-表尾-空隙
    	$rootScope._screenProp.tableUselessHeight7 = 125+15+41+43+34+56+20;//应用发布分页形式；卡片-空隙-查询-按钮-表头-表尾-空隙
    	$rootScope._screenProp.tableUselessHeight8 = 41+34+52+20;//前台分页无按钮形式；查询-表头-表尾-空隙
    	$rootScope._screenProp.tableUselessHeight9 = 40+20+41+34+56+20;//标题后台分页形式；标题-空隙-查询-表头-表尾-空隙
        //加屏幕高度，宽度信息 end	
    });
    
}]);

angular.module('app').controller('HeaderCtrl', function ($scope, $mdToast, $mdSidenav, $location, $timeout) {

    $scope.header_search_open = false;

    $scope.closeHeaderSearch = function () {
        $('.search-widget input').val('');
        $scope.header_search_open = false;
    };

    $scope.openHeaderSearch = function (ev) {
        $scope.header_search_open = true;
        $timeout(function () {
            $('.search-widget input').focus();
        }, 300)
    };

    $scope.toggleRight = function () {
        $mdSidenav('right').toggle();
    }

});