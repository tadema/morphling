// config
var SUCCESS_CODE = 1000000;
var app =
    angular.module('app')
        .config(
            ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$httpProvider',
                function ($controllerProvider, $compileProvider, $filterProvider, $provide, $httpProvider) {

                    // lazy controller, directive and service
                    app.controller = $controllerProvider.register;
                    app.directive = $compileProvider.directive;
                    app.filter = $filterProvider.register;
                    app.factory = $provide.factory;
                    app.service = $provide.service;
                    app.constant = $provide.constant;
                    app.value = $provide.value;
                    
                    // 注册httpInterceptor
                    $httpProvider.interceptors.push('httpInterceptor');
//                    $httpProvider.defaults.useXDomain = true;
//                    delete $httpProvider.defaults.headers.common['X-Requested-With'];
                }
            ])
        .config(['$translateProvider', function ($translateProvider) {
            // Register a loader for the static files
            // So, the module will search missing translation tables under the specified urls.
            // Those urls are [prefix][langKey][suffix].
            $translateProvider.useStaticFilesLoader({
                prefix: 'l10n/',
                suffix: '.json'
            });
            // Tell the module what language to use by default
            $translateProvider.preferredLanguage('cn');
            // Tell the module to store the language in the local storage
            $translateProvider.useLocalStorage();
        }]);
/*angular.module('test')
	.provider('testProv', function() {
	  this.backendUrl = "http://localhost:3000";
	  this.setBackendUrl = function(newUrl) {
	    if (url) this.backendUrl = newUrl;
	  }
	  this.$get = function($http) { // injectables go here
	    var self = this;
	    var service = {
	      user: {},
	      setName: function(newName) {
	        service.user['name'] = newName;
	      },
	      setEmail: function(newEmail) {
	        service.user['email'] = newEmail;
	      },
	      save: function() {
	        return $http.post(self.backendUrl + '/users', {
	          user: service.user
	        })
	      }
	    };
	    return service;
	  }
	});*/
