'use strict';


angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
//    'ngTouch',//放开后，f12有警告提示，目前为发现ngTouch有被使用
    'ngStorage',
    'ui.select',
    'toaster',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'EchoLog',
    'multiEchoLog',//factory    多重回显
    'driver',//factory   步骤指引   使用driver.min.js
    'dbUtils',
    'ui.components.form.grid',
    'ui.components.footable',
    'ui.jq',
    'ui.validate',
    'oc.lazyLoad',  
    'pascalprecht.translate',
//    'ngTreetable',
    'wui.date',
    'ui.select2',
//    'ui.router.tabs',
    'ngMaterial',
    'ngFilters',//http请求拦截
    'angularFileUpload',//文件上传
    'angular-transfer-box',//左右穿梭框
    'mgo-angular-wizard',//步骤条
    'ui.grid',
    'ui.grid.treeView',
    'ui.grid.selection',//选择行指令
    'ui.grid.resizeColumns',//表格宽可拉伸指令
    'ui.grid.autoResize',//自动使用div的高度和宽度指令
    'ui.grid.edit',//编辑指令
    'ui.grid.exporter',//导出指令
    'ui.grid.pagination',//分页指令
    'ui.grid.cellNav',//单元格指令
]);