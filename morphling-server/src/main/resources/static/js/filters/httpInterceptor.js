/**
 * 自定义过滤器
 * @type {module}
 */
var ngFilters = angular.module('ngFilters', []);

/**
 * http 权限,响应状态等过滤
 */
ngFilters.factory('httpInterceptor', [ '$q', '$injector' ,function($q, $injector) {
    var httpInterceptor = {
        'responseError': function (response) {
            /*console.log(response, "responseError");
            if(response.status == 401) { // 401 Unauthorized 当前请求需要用户验证
                var rootScope = $injector.get('$rootScope');
                var state = $injector.get('$state');
//                state.go("unauthorized");
            } else if(response.status === 404) { // 404 Not Found
                console.log("404!");
                var state = $injector.get('$state');
                state.go("error404");
            } else if(response.status === 400) { // bad request
            	var rootScope = $injector.get('$rootScope');
                console.log(rootScope.$$ChildScope);
            }*/
            //解决某些场景下，请求报错Loading一直存在的问题
            if($(".modal-backdrop.fade.in:visible")[0] && !$(".modal-dialog")[0]){
                $(".modal-backdrop.fade.in:visible").css("display","none");
            }
            return $q.reject(response);
        },
        'response': function (response) { // response success
            // TODO 后台响应状态过滤 response.data.code 或者在统一的ajax、http做拦截
            /*var urls = ['/client','/client/update','/client/batchUpdate']
            if(response.config.method || response.config.method == "post"){
            	if(urls.indexOf(response.config.url) != -1){
            		let dbUtils = $injector.get('dbUtils');
            	}
            }*/
            return response;
        },
        'request': function (config) {  // request success
//            console.log(config, "request");
        	/*config.headers = config.headers || {};
            if($cookies.get('token')){
                config.headers.authorization = 'Bearer ' + $cookies.get('token');
            }*/

        	//有请求html文件的，需要注意
            return config;
        },
        'requestError': function (config) {
            //console.log(config, "requestError");
            return $q.reject(config);
        }
    }
    return httpInterceptor;
}
]);
