delete from GF_DPS_CODE;
delete from GF_DPS_ENV;
delete from GF_DPS_ENV_ROLE;
delete from GF_DPS_MENU;
delete from GF_DPS_MENU_ROLE;
delete from GF_DPS_USERS;
delete from GF_DPS_USER_ROLE;
delete from GF_DPS_ROLE;
delete from GF_DPS_ALERT_RULES;
delete from GF_DPS_MONITOR_SERVER;


insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (1, '100', '监控类型', '1', 'client');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (2, '100', '监控类型', '2', 'weblogic-jmx');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (3, '100', '监控类型', '3', 'tomcat-jmx');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (4, '100', '监控类型', '4', 'springboot-jmx');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (5, '101', '脚本类别', '01', '运维脚本');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (6, '101', '脚本类别', '02', '巡检脚本');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (7, '102', '脚本类型', '01', 'shell');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (8, '102', '脚本类型', '02', 'python');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (9, '102', '脚本类型', '03', 'perl');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (10, '103', '脚本后缀类型', '01', '.sh');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (11, '103', '脚本后缀类型', '02', '.py');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (12, '103', '脚本后缀类型', '03', '.pl');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (13, '104', 'server端基本信息', 'ip', '192.168.174.6');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (14, '104', 'server端基本信息', 'port', '33330');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (15, '104', 'server端基本信息', 'linuxPort', '22');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (16, '104', 'server端基本信息', 'username', 'deploy');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (17, '104', 'server端基本信息', 'password', 'dell@105B');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (18, '104', 'server端基本信息', 'appPath', '/workspace/app');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (19, '104', 'server端基本信息', 'updatePath', '/workspace/update');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (20, '104', 'server端基本信息', 'javaName', 'jdk-8u191-linux-x64.tar.gz');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (21, '104', 'server端基本信息', 'javaPath', '/app/data/morphling-server/jdk-8u191-linux-x64.tar.gz');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (22, '104', 'server端基本信息', 'javaFolder', 'jdk1.8.0_191');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (23, '104', 'server端基本信息', 'logPath', '/workspace/log');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (24, '104', 'server端基本信息', 'deployPath', '/workspace/deploy');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (25, '104', 'server端基本信息', 'backupPath', '/workspace/backup');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (26, '105', '文件管理的文件类型', '1', '默认');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (27, '105', '文件管理的文件类型', '2', '软件');

insert into GF_DPS_CODE (ID, TYPE, TYPE_NAME, CODE, CODE_VALUE)
values (28, '105', '文件管理的文件类型', '3', '安装包');

insert into GF_DPS_ENV (id, key, name, is_prod)
values (1, 'dev', '开发环境', 0);
insert into GF_DPS_ENV (id, key, name, is_prod)
values (2, 'test', '测试环境', 0);
insert into GF_DPS_ENV (id, key, name, is_prod)
values (4, 'product', '生产环境', 1);


insert into GF_DPS_ENV_ROLE (ID, ROLE_ID, ENV_KEY, STATE)
values (1, 2, 'dev', 1);

insert into GF_DPS_ENV_ROLE (ID, ROLE_ID, ENV_KEY, STATE)
values (2, 2, 'test', 1);

insert into GF_DPS_ENV_ROLE (ID, ROLE_ID, ENV_KEY, STATE)
values (3, 2, 'product', 1);


insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (1, '系统配置', 1, 'icon-settings text-danger', ' ', ' ', 0, ' ', 1001, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (2, '主机资源', 1, ' fa fa-sliders text-info', null, null, 0, ' ', 1002, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (3, '中间件管理', 1, 'fa fa-leaf text-primary', null, null, 0, ' ', 1003, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (4, '应用管理', 1, 'icon-grid text-success ', ' ', ' ', 0, ' ', 1004, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (5, '日志管理', 1, 'icon-calendar text-warning', ' ', ' ', 0, ' ', 1005, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (6, '监控管理', 1, ' fa  fa-life-ring text-danger', ' ', ' ', 0, ' ', 1006, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (7, '自动化运维', 1, 'fa fa-cubes text-info', null, null, 0, ' ', 1007, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (8, '告警管理', 1, 'fa fa-warning text-danger', null, null, 0, ' ', 1008, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (9, '顶部分割线', 3, ' ', ' ', ' ', 0, ' ', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (10, '测试页面1', 11, 'glyphicon glyphicon-paperclip', 'app.test01.view01', ' ', 0, '/test01/**,/monitor/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (11, '用户管理', 1, 'fa fa-users text-success', 'app.users', ' ', 1, '/user/**,/role/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (12, '角色管理', 1, 'fa fa-user text-warning', 'app.roleManager', ' ', 1, '/roleManager/**,/env,/tomcat/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (13, '邮件服务器', 1, 'fa fa-envelope-o', 'app.mailServer', null, 1, '/alertRules/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (14, '组织机构', 1, 'fa fa-sitemap', 'app.organization', null, 1, '/organization/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (21, '主机管理', 1, ' icon-screen-desktop text-success', 'app.client', ' ', 2, '/client/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (31, 'WebLogic配置', 1, 'fa fa-empire text-warning', 'app.domain', ' ', 3, '/env,/domain/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (32, 'Tomcat配置', 1, 'fa fa-gears  text-info', 'app.tomcat', ' ', 3, '/env,/tomcat/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (33, 'nginx管理', 1, 'fa fa-tasks text-success', 'app.nginx', ' ', 3, '/env,/domain/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (41, '应用管理', 1, 'fa fa-cloud text-primary', 'app.manage', ' ', 4, '/env,/app/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (42, '应用发布', 1, ' icon-cloud-upload text-success', 'app.deploy', ' ', 4, '/app/preview/**,/deploy/**,/logs/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (43, '版本管理', 1, 'fa fa-sort-numeric-asc text-info', 'app.vision', ' ', 4, '/app/preview/**,/deploy/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (51, '应用日志', 1, ' fa fa-file-text text-primary', 'app.logtable', ' ', 5, '/app/preview/**,/logs/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (52, '系统日志', 1, ' fa fa-file-text-o', 'app.systemLog', ' ', 5, '/serverLog/**,/logs/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (61, '监控定义', 1, 'fa fa-edit text-warning   ', 'app.monitorDefine', ' ', 6, '/env,/monitor/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (62, '主机监控', 11, 'fa fa-bar-chart text-info', 'app.monitorClient', ' ', 6, '/monitor/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (63, '应用监控', 11, 'fa fa-dashboard text-success', 'app.monitorApp', ' ', 6, '/monitor/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (64, '监控管理', 1, 'fa fa-bar-chart text-info', 'app.monitorManager', ' ', 6, '/monitor/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (65, '数据库监控', 1, 'fa fa-database', 'app.monitorDatabase', null, 6, '/monitor/**,/database/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (66, 'OGG监控', 1, 'fa fa-copy text-success', 'app.monitorOGG', null, 6, '/monitor/**,/goldengate/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (71, '脚本管理', 1, 'fa fa-file-code-o text-warning', 'app.script', null, 7, '/script/**,/logs/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (72, '文件管理', 1, 'fa fa-file-zip-o text-success', 'app.filelist', null, 7, '/script/**,/file/**,/logs/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (73, '软件安装', 1, 'fa fa-cog', 'app.softwareInstall', null, 7, '/script/**,/file/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (81, '告警配置', 1, 'fa fa-wrench text-success', 'app.alarmConfig', null, 8, '/alert/sendMessages,/alertManagerServer/**,/alertSenderConfig/**,/alert/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (82, '告警规则', 1, 'fa fa-gears text-warning', 'app.alarmRule', null, 8, '/alertRules/**', 0, null);

insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (83, '告警信息', 1, 'fa fa-text-width text-info', 'app.alarmManagement', null, 8, '/alert/**', 0, null);

insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (436, 5, 10, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (437, 5, 11, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (438, 5, 12, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (439, 5, 41, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (14, 3, 1, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (15, 3, 2, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (16, 3, 3, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (17, 3, 4, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (61, 4, 10, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (62, 4, 23, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (63, 4, 31, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (64, 4, 41, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (65, 4, 42, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (3411, 8, 11, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (3412, 8, 12, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (3413, 8, 41, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (3414, 8, 42, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (3415, 8, 1, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (3416, 8, 4, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (15456, 2, 12, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (15457, 2, 21, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (15458, 2, 41, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (15459, 2, 1, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (15460, 2, 2, 1);
insert into GF_DPS_MENU_ROLE (id, role_id, menu_id, state)
values (15461, 2, 4, 1);

insert into GF_DPS_ROLE (id, role_name, description)
values (1, 'SUPERADMIN', '超级管理员');
insert into GF_DPS_ROLE (id, role_name, description)
values (2, 'deploy', '应用部署角色');

insert into GF_DPS_USERS (id, name, username, phone, email, password, head_img, last_login_time, last_login_ip, create_time, state, is_admin)
values (1, '管理员', 'admin', '15652222294', '55375829@qq.com', '$2a$10$yQdf.cLpD1XHK7KOfjH0rONz.M23gXVEwjldl.kptmZHiuJfpJcdK', '', to_date('22-07-2019 13:39:03', 'dd-mm-yyyy hh24:mi:ss'), '0:0:0:0:0:0:0:1', to_date('23-11-2017 17:19:16', 'dd-mm-yyyy hh24:mi:ss'), 1, 1);
insert into GF_DPS_USERS (id, name, username, phone, email, password, head_img, last_login_time, last_login_ip, create_time, state, is_admin)
values (27, '东软应用部署', 'neusoft', '24254363463', '424242', '$2a$10$FLMhyCT2lhl22q6.Ivvr..Rg.5n8fh8yucu185dynMO.NESGAQ4KG', ' ', to_date('15-05-2019 17:45:36', 'dd-mm-yyyy hh24:mi:ss'), '0:0:0:0:0:0:0:1', to_date('17-12-2018 15:15:51', 'dd-mm-yyyy hh24:mi:ss'), 1, 0);

insert into GF_DPS_USER_ROLE (id, user_id, role_id, create_time, state)
values (1, 1, 1, to_date('26-07-2018 10:52:33', 'dd-mm-yyyy hh24:mi:ss'), 1);
insert into GF_DPS_USER_ROLE (id, user_id, role_id, create_time, state)
values (3470, 27, 2, to_date('16-05-2019 09:23:57', 'dd-mm-yyyy hh24:mi:ss'), 1);

insert into GF_DPS_ALERT_RULES (ID, GROUP_NAME, ALERT_NAME, EXPR_INFO, FOR_TIME, LABELS_SERVERITY, LABELS_VALUE, ANNOTATIONS_SUMMARY, ANNOTATIONS_DESCRIPTION, ENV)
values (20057, 'alert-info', 'node_cpu_usage', '100-round(avg(rate(node_cpu_seconds_total{mode="idle"}[1m])) by (instance) *100) >=70', '1m', 'red', null, '{{ $labels.instance }} :服务器内存使用率过高', '{{ $labels.instance }} :服务器平均每分钟cpu使用率超过70% , 监控类型: {{ $labels.job }}  ,当前值：({{ $value }}%)', null);

insert into GF_DPS_ALERT_RULES (ID, GROUP_NAME, ALERT_NAME, EXPR_INFO, FOR_TIME, LABELS_SERVERITY, LABELS_VALUE, ANNOTATIONS_SUMMARY, ANNOTATIONS_DESCRIPTION, ENV)
values (21287, 'alert-info', 'node_filesystem_usage', '100-round((node_filesystem_free_bytes{fstype=~"ext4|xfs",mountpoint="/"} / node_filesystem_size_bytes{fstype=~"ext4|xfs",mountpoint="/"}) * 100 )>=80', '1m', 'yellow', null, '{{ $labels.instance }}:服务器硬盘使用率过高', '{{ $labels.instance }} :服务器硬盘使用率超80% , 监控类型: {{ $labels.job }} ,当前值：({{ $value }}%)', null);

insert into GF_DPS_ALERT_RULES (ID, GROUP_NAME, ALERT_NAME, EXPR_INFO, FOR_TIME, LABELS_SERVERITY, LABELS_VALUE, ANNOTATIONS_SUMMARY, ANNOTATIONS_DESCRIPTION, ENV)
values (12950, 'alert-info', 'node_memory_usage', '100-round((node_memory_MemFree_bytes + node_memory_Buffers_bytes + node_memory_Cached_bytes) / node_memory_MemTotal_bytes * 100) >=70', '1m', 'orange', null, '{{ $labels.instance }} :服务器内存使用率过高', '{{ $labels.instance }} :服务器内存使用率超过70% , 监控类型: {{ $labels.job }} ,当前值：({{ $value }}%)', null);

insert into GF_DPS_ALERT_RULES (ID, GROUP_NAME, ALERT_NAME, EXPR_INFO, FOR_TIME, LABELS_SERVERITY, LABELS_VALUE, ANNOTATIONS_SUMMARY, ANNOTATIONS_DESCRIPTION, ENV)
values (12951, 'alert-info', 'instance-down', 'up == 0', '1m', 'red', null, 'Instance {{ $labels.instance }} down', '{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 10 s.', null);


insert into GF_DPS_PERFORMANCE_MONITOR (ID, APP_ID, NAME, IP_PORT, URL, TYPE, CREATE_TIME, ENV, REMARK)
values ('3', null, '主机监控', '192.168.174.6:3000', 'http://192.168.174.6:3000/d/000000001/morphling-system-client?orgId=2', 'client', to_date('15-01-2019 00:16:39', 'dd-mm-yyyy hh24:mi:ss'), 'dev', '各节点服务器硬件监控');

insert into GF_DPS_PERFORMANCE_MONITOR (ID, APP_ID, NAME, IP_PORT, URL, TYPE, CREATE_TIME, ENV, REMARK)
values ('4', null, 'tomcat-jmx监控', '192.168.174.6:3000', 'http://192.168.174.6:3000/d/nshAi8pmz/morphling-jmx-tomcat?orgId=2', 'tomcat-jmx', to_date('15-01-2019 17:49:27', 'dd-mm-yyyy hh24:mi:ss'), 'dev', 'tomcat监控');

insert into GF_DPS_PERFORMANCE_MONITOR (ID, APP_ID, NAME, IP_PORT, URL, TYPE, CREATE_TIME, ENV, REMARK)
values ('5', null, 'weblogic-jmx监控', '192.168.174.6:3000', 'http://192.168.174.6:3000/d/ntsj9SQiz/morphling-jmx-weblogic?orgId=2', 'weblogic-jmx', to_date('15-01-2019 17:50:13', 'dd-mm-yyyy hh24:mi:ss'), 'dev', 'weblogic监控');

insert into GF_DPS_PERFORMANCE_MONITOR (ID, APP_ID, NAME, IP_PORT, URL, TYPE, CREATE_TIME, ENV, REMARK)
values ('6', null, 'springboot监控', '192.168.174.6:3000', 'http://192.168.174.6:3000/d/_Fd2vHQik/morphling-jmx-springboot?orgId=2', 'springboot-jmx', to_date('15-01-2019 17:52:02', 'dd-mm-yyyy hh24:mi:ss'), 'dev', 'springboot监控');


insert into GF_DPS_MONITOR_SERVER (ID, NAME, IP, PORT, STATUS, CREATE_TIME, ENV, REMARK, TYPE_NAME)
values (32557, 'prometheus', '192.168.174.6', 9090, 'DOWN', to_date('07-11-2019 15:01:34', 'dd-mm-yyyy hh24:mi:ss'), 'dev', 'prometheus', 'prometheus');

insert into GF_DPS_MONITOR_SERVER (ID, NAME, IP, PORT, STATUS, CREATE_TIME, ENV, REMARK, TYPE_NAME)
values (33371, 'grafana', '192.168.174.6', 3000, 'DOWN', to_date('24-12-2019 09:51:36', 'dd-mm-yyyy hh24:mi:ss'), 'dev', 'grafana', 'grafana');


-- V2.1.1
insert into GF_DPS_MENU (ID, TEXT, TYPE, ICON, ROUTE, TRANSLATE, PARENT, URL_MATCHES, ORDERED, HTML)
values (67, 'Redis监控', 1, 'iconfont icon-redis', 'app.monitorRedis', null, 6, '/monitor/**', 0, null);

commit;