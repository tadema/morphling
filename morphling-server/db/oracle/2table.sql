
create table GF_DPS_ALERT_INFO
(
  id                 NUMBER(10) not null,
  receiver           VARCHAR2(512 CHAR),
  status             VARCHAR2(512 CHAR),
  alerts_status      VARCHAR2(512 CHAR),
  alert_name         VARCHAR2(512 CHAR),
  ip                 VARCHAR2(512 CHAR),
  port               VARCHAR2(512 CHAR),
  instance           VARCHAR2(512 CHAR),
  job                VARCHAR2(512 CHAR),
  severity           VARCHAR2(512 CHAR),
  description        VARCHAR2(4000 CHAR),
  summary            VARCHAR2(4000 CHAR),
  starts_at          VARCHAR2(4000 CHAR),
  end_at             VARCHAR2(4000 CHAR),
  generatorurl       VARCHAR2(512 CHAR),
  common_annotations VARCHAR2(512 CHAR),
  externalurl        VARCHAR2(512 CHAR),
  version            VARCHAR2(512 CHAR),
  group_key          VARCHAR2(512 CHAR),
  timestamp          VARCHAR2(512 CHAR),
  value              VARCHAR2(512 CHAR),
  is_flag            NUMBER(10)
)
;

create table GF_DPS_ALERT_MANAGER_SERVER
(
  id          NUMBER(10) not null,
  name        VARCHAR2(255 CHAR),
  ip          VARCHAR2(255 CHAR),
  port        NUMBER(10),
  status      VARCHAR2(50 CHAR),
  create_time DATE,
  env         VARCHAR2(50 CHAR),
  remark      VARCHAR2(255 CHAR),
  type_name   VARCHAR2(125 CHAR)
)
;
alter table GF_DPS_ALERT_MANAGER_SERVER
  add constraint PK_ALERT_MANAGER_ID primary key (ID);


create table GF_DPS_ALERT_RULES
(
  id                      NUMBER(10) not null,
  group_name              VARCHAR2(512 CHAR),
  alert_name              VARCHAR2(512 CHAR),
  expr_info               VARCHAR2(4000 CHAR),
  for_time                VARCHAR2(512 CHAR),
  labels_serverity        VARCHAR2(512 CHAR),
  labels_value            VARCHAR2(512 CHAR),
  annotations_summary     VARCHAR2(4000 CHAR),
  annotations_description VARCHAR2(4000 CHAR),
  env                     VARCHAR2(8 CHAR)
)
;
comment on column GF_DPS_ALERT_RULES.group_name
  is '规则组名';
comment on column GF_DPS_ALERT_RULES.alert_name
  is '规则名称';
comment on column GF_DPS_ALERT_RULES.expr_info
  is '规则公式';
comment on column GF_DPS_ALERT_RULES.for_time
  is '持续时长（1s 1m 1h）';
comment on column GF_DPS_ALERT_RULES.labels_serverity
  is '告警重要性等级  red、orange、yello   3、2、1';
comment on column GF_DPS_ALERT_RULES.labels_value
  is '告警公式计算结果';
comment on column GF_DPS_ALERT_RULES.annotations_summary
  is '规则概要';
comment on column GF_DPS_ALERT_RULES.annotations_description
  is '规则描述';
alter table GF_DPS_ALERT_RULES
  add constraint PK_ALERT_RULES_ID primary key (ID);


create table GF_DPS_ALERT_SENDER_CONFIG
(
  id             NUMBER(10) not null,
  app_number     VARCHAR2(128 CHAR),
  type           VARCHAR2(64 CHAR),
  email          VARCHAR2(4000 CHAR),
  mobile         VARCHAR2(4000 CHAR),
  wechat         VARCHAR2(4000 CHAR),
  titlel         VARCHAR2(50 CHAR),
  name           VARCHAR2(128 CHAR),
  ext_msg        VARCHAR2(128 CHAR),
  biz_name       VARCHAR2(128 CHAR),
  biz_number     VARCHAR2(128 CHAR),
  env            VARCHAR2(50 CHAR),
  api_url        VARCHAR2(1024 CHAR),
  responses_code VARCHAR2(64 CHAR)
)
;
comment on column GF_DPS_ALERT_SENDER_CONFIG.id
  is 'ID';
comment on column GF_DPS_ALERT_SENDER_CONFIG.app_number
  is '消息平台接入客户端编号';
comment on column GF_DPS_ALERT_SENDER_CONFIG.type
  is '类型：email 、mobile、wecaht';
comment on column GF_DPS_ALERT_SENDER_CONFIG.email
  is '邮箱账号，多个账号之间逗号隔开';
comment on column GF_DPS_ALERT_SENDER_CONFIG.mobile
  is '手机号，多个账号之间逗号隔开';
comment on column GF_DPS_ALERT_SENDER_CONFIG.wechat
  is '微信号，多个账号之间逗号隔开';
comment on column GF_DPS_ALERT_SENDER_CONFIG.titlel
  is '标题';
comment on column GF_DPS_ALERT_SENDER_CONFIG.name
  is '用户名';
comment on column GF_DPS_ALERT_SENDER_CONFIG.ext_msg
  is '扩展';
comment on column GF_DPS_ALERT_SENDER_CONFIG.api_url
  is 'api';
comment on column GF_DPS_ALERT_SENDER_CONFIG.responses_code
  is '响应码';
alter table GF_DPS_ALERT_SENDER_CONFIG
  add constraint PK_SENDER_CONFIG primary key (ID);


create table GF_DPS_APP
(
  id                   NUMBER(10) not null,
  name                 VARCHAR2(512 CHAR) not null,
  description          VARCHAR2(255 CHAR),
  context_path         VARCHAR2(50 CHAR),
  git_url              VARCHAR2(255 CHAR),
  git_branch           VARCHAR2(50 CHAR),
  git_user             VARCHAR2(50 CHAR),
  git_pwd              VARCHAR2(50 CHAR),
  mvn_module           VARCHAR2(4000 CHAR),
  mvn_param            VARCHAR2(255 CHAR),
  env                  VARCHAR2(20 CHAR),
  service_type         NUMBER(10),
  deploy_type          NUMBER(10),
  current_pack_version VARCHAR2(50 CHAR),
  apollo_id            VARCHAR2(4000 CHAR),
  admin_user           VARCHAR2(100 CHAR),
  create_time          DATE,
  create_userid        NUMBER(10),
  create_username      VARCHAR2(50 CHAR),
  state                NUMBER(10),
  file_type            VARCHAR2(20 CHAR),
  file_name            VARCHAR2(100 CHAR),
  middleware_type      VARCHAR2(30 CHAR),
  developer            VARCHAR2(20 CHAR),
  is_stage             NUMBER(10),
  start_port           VARCHAR2(60 CHAR),
  start_context        VARCHAR2(4000 CHAR),
  param_front          VARCHAR2(4000 CHAR),
  param_behind         VARCHAR2(4000 CHAR),
  param_memory         VARCHAR2(4000 CHAR),
  app_chinese_name     VARCHAR2(255 CHAR),
  promet_port          VARCHAR2(255 CHAR),
  is_monitor           NUMBER(10),
  java_home            VARCHAR2(1024 CHAR),
  exclude_files        VARCHAR2(4000 CHAR)
)
;
comment on column GF_DPS_APP.name
  is '应用英文名称';
comment on column GF_DPS_APP.description
  is '应用描述信息';
comment on column GF_DPS_APP.context_path
  is 'URL请求路径';
comment on column GF_DPS_APP.git_url
  is 'git地址';
comment on column GF_DPS_APP.git_branch
  is 'git分支名称';
comment on column GF_DPS_APP.git_user
  is 'git用户';
comment on column GF_DPS_APP.git_pwd
  is 'git密码';
comment on column GF_DPS_APP.mvn_module
  is '部署命令';
comment on column GF_DPS_APP.mvn_param
  is 'maven install 命令行参数';
comment on column GF_DPS_APP.env
  is '部署环境';
comment on column GF_DPS_APP.service_type
  is '1：网关，2：服务';
comment on column GF_DPS_APP.deploy_type
  is '1：自定义部署字段';
comment on column GF_DPS_APP.apollo_id
  is '用于保存应用扩展信息';
comment on column GF_DPS_APP.file_type
  is 'war/jar/目录';
comment on column GF_DPS_APP.middleware_type
  is 'weblogic/tomcat';
comment on column GF_DPS_APP.is_stage
  is '1:stage;2:no-stage';
comment on column GF_DPS_APP.param_front
  is 'springboot类型java后面的参数';
comment on column GF_DPS_APP.param_behind
  is 'springboot类型java -jar后面的参数';
comment on column GF_DPS_APP.param_memory
  is 'tomcat和weblogic的启动内存参数';
comment on column GF_DPS_APP.app_chinese_name
  is '应用的中文名称';
comment on column GF_DPS_APP.promet_port
  is 'springboot的监控端口';
comment on column GF_DPS_APP.is_monitor
  is 'springboot监控标记位';
comment on column GF_DPS_APP.exclude_files
  is '部署时需要排除的文件 空格隔开，区分大小写 请使用通配符 :*/css/*  */WEB-INF/confg/application.xml';
alter table GF_DPS_APP
  add constraint PRIMARY primary key (ID);
alter table GF_DPS_APP
  add constraint UNI_APP_NAME_CNAME_TWOTYPE unique (NAME, FILE_TYPE, MIDDLEWARE_TYPE, APP_CHINESE_NAME,ENV);


create table GF_DPS_APP_DEPLOY_CUSTOM
(
  id             NUMBER(10) not null,
  app_id         NUMBER(10),
  ip             VARCHAR2(50 CHAR),
  port           NUMBER(8),
  user_name      VARCHAR2(50 CHAR),
  command1       VARCHAR2(4000 CHAR),
  command2       VARCHAR2(4000 CHAR),
  command3       VARCHAR2(4000 CHAR),
  command4       VARCHAR2(4000 CHAR),
  command5       VARCHAR2(4000 CHAR),
  command6       VARCHAR2(4000 CHAR),
  command7       VARCHAR2(4000 CHAR),
  command8       VARCHAR2(4000 CHAR),
  command9       VARCHAR2(4000 CHAR),
  command10      VARCHAR2(4000 CHAR),
  client_id      NUMBER(10),
  file_id        NUMBER(10),
  deploy_version VARCHAR2(512 CHAR),
  app_type       VARCHAR2(50 CHAR),
  type_id        NUMBER(10)
)
;
comment on table GF_DPS_APP_DEPLOY_CUSTOM
  is '自定义部署信息';
comment on column GF_DPS_APP_DEPLOY_CUSTOM.app_id
  is '应用ID';
comment on column GF_DPS_APP_DEPLOY_CUSTOM.ip
  is '主机IP';
comment on column GF_DPS_APP_DEPLOY_CUSTOM.port
  is '端口';
comment on column GF_DPS_APP_DEPLOY_CUSTOM.user_name
  is '服务器用户名称';
comment on column GF_DPS_APP_DEPLOY_CUSTOM.command1
  is '部署';
comment on column GF_DPS_APP_DEPLOY_CUSTOM.command2
  is '启动';
comment on column GF_DPS_APP_DEPLOY_CUSTOM.command3
  is '停止';
comment on column GF_DPS_APP_DEPLOY_CUSTOM.command4
  is '重启';
comment on column GF_DPS_APP_DEPLOY_CUSTOM.command5
  is '回滚';
comment on column GF_DPS_APP_DEPLOY_CUSTOM.client_id
  is '主机 ID';
comment on column GF_DPS_APP_DEPLOY_CUSTOM.file_id
  is '文件ID';
comment on column GF_DPS_APP_DEPLOY_CUSTOM.deploy_version
  is '部署版本号';
comment on column GF_DPS_APP_DEPLOY_CUSTOM.app_type
  is '应用类型';
comment on column GF_DPS_APP_DEPLOY_CUSTOM.type_id
  is '类型的ID';
create unique index PK_APP_ID_IP_PORT on GF_DPS_APP_DEPLOY_CUSTOM (APP_ID, IP, PORT);


create table GF_DPS_APP_DOMAIN
(
  id              NUMBER(10) not null,
  app_id          NUMBER(10) not null,
  domain_id       NUMBER(10) not null,
  current_version VARCHAR2(50 CHAR),
  status          NUMBER(10) not null,
  create_time     DATE not null,
  state           NUMBER(10) not null
)
;
alter table GF_DPS_APP_DOMAIN
  add constraint PRIMARY_1 primary key (ID);


create table GF_DPS_APP_FILE
(
  id            NUMBER(10) not null,
  app_id        NUMBER(10) not null,
  source_name   VARCHAR2(100 CHAR),
  file_name     VARCHAR2(512 CHAR),
  deploy_type   NUMBER(10),
  file_savepath VARCHAR2(200 CHAR),
  is_deploy     NUMBER(10),
  create_time   DATE,
  version       VARCHAR2(50 CHAR),
  deploy_id     VARCHAR2(4000 CHAR),
  upload_user   VARCHAR2(50 CHAR),
  description   VARCHAR2(255 CHAR),
  file_size     VARCHAR2(128 CHAR)
)
;
comment on column GF_DPS_APP_FILE.source_name
  is '文件名和版本名拼接';
comment on column GF_DPS_APP_FILE.deploy_type
  is '1:首次部署 ,2:更新';
comment on column GF_DPS_APP_FILE.is_deploy
  is '0:使用中，1:已删除';
comment on column GF_DPS_APP_FILE.description
  is '文件版本描述信息';
comment on column GF_DPS_APP_FILE.file_size
  is '文件大小';
alter table GF_DPS_APP_FILE
  add constraint PRIMARY_2 primary key (ID);
alter table GF_DPS_APP_FILE
  add constraint UNI_APPID_VERSION unique (APP_ID, VERSION);


create table GF_DPS_APP_ICON
(
  id           NUMBER(10) not null,
  developer    VARCHAR2(50 CHAR) not null,
  company_icon BLOB not null
)
;
alter table GF_DPS_APP_ICON
  add constraint PRIMARY_3 primary key (ID);


create table GF_DPS_APP_INSTANCE
(
  id              NUMBER(10) not null,
  app_id          NUMBER(10) not null,
  client_id       NUMBER(10) not null,
  current_version VARCHAR2(50 CHAR) not null,
  status          NUMBER(10) not null,
  create_time     DATE not null,
  state           NUMBER(10) not null
)
;
comment on column GF_DPS_APP_INSTANCE.status
  is '0:待部署 1：运行中 2已停止';
alter table GF_DPS_APP_INSTANCE
  add constraint PRIMARY_4 primary key (ID);


create table GF_DPS_APP_SPRINGBOOT
(
  id              NUMBER(10) not null,
  app_id          NUMBER(10) not null,
  springboot_port NUMBER(10) not null,
  is_monitor      NUMBER(10),
  promet_port     NUMBER(10),
  springboot_host VARCHAR2(50 CHAR) not null,
  current_version VARCHAR2(512 CHAR),
  status          NUMBER(10) not null,
  create_time     DATE not null,
  state           NUMBER(10) not null,
  start_context   VARCHAR2(512 CHAR),
  deploy_file_id  VARCHAR2(200 CHAR),
  lastest_file    VARCHAR2(512 CHAR),
  deploy_info     VARCHAR2(1024 CHAR),
  create_userid   NUMBER(10),
  env             VARCHAR2(50 CHAR)
)
;
comment on column GF_DPS_APP_SPRINGBOOT.status
  is '0:待部署 1：运行中 2已停止';
alter table GF_DPS_APP_SPRINGBOOT
  add constraint PRIMARY_5 primary key (ID);

create table GF_DPS_APP_TOMCAT
(
  id              NUMBER(10) not null,
  app_id          NUMBER(10) not null,
  tomcat_id       NUMBER(10) not null,
  current_version VARCHAR2(50 CHAR),
  status          NUMBER(10) not null,
  create_time     DATE not null,
  state           NUMBER(10) not null
)
;
comment on column GF_DPS_APP_TOMCAT.status
  is '0:待部署 1：运行中 2已停止';
alter table GF_DPS_APP_TOMCAT
  add constraint PRIMARY_6 primary key (ID);


create table GF_DPS_CLIENT
(
  id               NUMBER(10) not null,
  name             VARCHAR2(200 CHAR) not null,
  host_address     VARCHAR2(200 CHAR) not null,
  port             NUMBER(10),
  performance_port NUMBER(10),
  env              VARCHAR2(20 CHAR),
  remark           VARCHAR2(4000 CHAR),
  pack_version     VARCHAR2(20 CHAR),
  create_userid    NUMBER(10),
  create_username  VARCHAR2(50 CHAR),
  create_time      DATE,
  state            NUMBER(10) default '1',
  username         VARCHAR2(50 CHAR) not null,
  password         VARCHAR2(100 CHAR) not null,
  linux_port       NUMBER(10) not null,
  is_node_monitor  NUMBER(10)
)
;
comment on column GF_DPS_CLIENT.name
  is '服务器名称';
comment on column GF_DPS_CLIENT.host_address
  is '服务器IP地址';
comment on column GF_DPS_CLIENT.port
  is 'Agent端口号';
comment on column GF_DPS_CLIENT.performance_port
  is '主机监控端口号';
comment on column GF_DPS_CLIENT.env
  is '部署环境';
comment on column GF_DPS_CLIENT.remark
  is '备注';
comment on column GF_DPS_CLIENT.create_userid
  is '创建用户ID';
comment on column GF_DPS_CLIENT.create_username
  is '创建用户名';
comment on column GF_DPS_CLIENT.create_time
  is '创建时间';
comment on column GF_DPS_CLIENT.username
  is '主机登录用户';
comment on column GF_DPS_CLIENT.password
  is '主机登录密码';
comment on column GF_DPS_CLIENT.linux_port
  is 'linux的port，一般为22';
comment on column GF_DPS_CLIENT.is_node_monitor
  is '是否监控：1是，0否';
create index PK_IP_PORT on GF_DPS_CLIENT (HOST_ADDRESS, PORT);
alter table GF_DPS_CLIENT
  add constraint PRIMARY_7 primary key (ID);
alter table GF_DPS_CLIENT
  add constraint UNIQ_CLIENT_IP_USERNAME_PORT unique (HOST_ADDRESS, USERNAME, PORT, CREATE_USERNAME)


create table GF_DPS_CLIENT_MIDDLEWARE
(
  id              NUMBER(10) not null,
  client_id       NUMBER(10) not null,
  middleware_id   NUMBER(10) not null,
  create_time     DATE not null,
  status          NUMBER(10) not null,
  middleware_type VARCHAR2(20 CHAR)
)
;
alter table GF_DPS_CLIENT_MIDDLEWARE
  add constraint PRIMARY_8 primary key (ID);


create table GF_DPS_CODE
(
  id         NUMBER(10) not null,
  type       VARCHAR2(255 CHAR),
  type_name  VARCHAR2(255 CHAR),
  code       VARCHAR2(255 CHAR),
  code_value VARCHAR2(255 CHAR)
)
;
comment on column GF_DPS_CODE.type
  is '类别';
comment on column GF_DPS_CODE.type_name
  is '类别名称';
comment on column GF_DPS_CODE.code
  is '代码';
comment on column GF_DPS_CODE.code_value
  is '代码值';
alter table GF_DPS_CODE
  add constraint PRIMARY_9 primary key (ID);


create table GF_DPS_DB_INSTANCE
(
  id              NUMBER(10) not null,
  name            VARCHAR2(256),
  username        VARCHAR2(256),
  password        VARCHAR2(256),
  ip              VARCHAR2(256),
  port            NUMBER(10),
  type            VARCHAR2(256),
  database_name   VARCHAR2(256),
  env             VARCHAR2(20 CHAR),
  create_userid   NUMBER(10),
  create_username VARCHAR2(50 CHAR),
  version         VARCHAR2(512 CHAR),
  jdbc_url        VARCHAR2(512 CHAR)
)
;
comment on column GF_DPS_DB_INSTANCE.id
  is 'ID';
comment on column GF_DPS_DB_INSTANCE.name
  is '名称';
comment on column GF_DPS_DB_INSTANCE.username
  is '数据库用户';
comment on column GF_DPS_DB_INSTANCE.password
  is '密码';
comment on column GF_DPS_DB_INSTANCE.ip
  is '数据库地址';
comment on column GF_DPS_DB_INSTANCE.port
  is '数据库端口';
comment on column GF_DPS_DB_INSTANCE.type
  is '数据库类型：01 oracle , 02 mysql';
comment on column GF_DPS_DB_INSTANCE.database_name
  is '数据库名称';
comment on column GF_DPS_DB_INSTANCE.jdbc_url
  is '数据库连接串';
alter table GF_DPS_DB_INSTANCE
  add constraint PK_DB_INSTANCE_ID primary key (ID);

create table GF_DPS_DOMAIN
(
  id                 NUMBER(10) not null,
  domain_name        VARCHAR2(50 CHAR) not null,
  description        VARCHAR2(255 CHAR),
  weblogic_home      VARCHAR2(200 CHAR),
  domain_path        VARCHAR2(200 CHAR),
  host_name          VARCHAR2(100 CHAR),
  host_ip            VARCHAR2(20 CHAR),
  admin_name         VARCHAR2(200 CHAR),
  admin_ip           VARCHAR2(20 CHAR),
  admin_port         NUMBER(10),
  is_admin_monitor   NUMBER(10),
  promet_port        NUMBER(10),
  is_managed_monitor NUMBER(10),
  admin_is_start     NUMBER(10),
  admin_username     VARCHAR2(40 CHAR),
  admin_password     VARCHAR2(200 CHAR),
  os_username        VARCHAR2(40 CHAR),
  os_password        VARCHAR2(40 CHAR),
  create_time        DATE not null,
  create_userid      NUMBER(10) not null,
  create_username    VARCHAR2(50 CHAR) not null,
  is_deploy          NUMBER(10),
  java_home          VARCHAR2(255 CHAR),
  env                VARCHAR2(50 CHAR),
  weblogic_version   VARCHAR2(4 CHAR)
)
;
alter table GF_DPS_DOMAIN
  add constraint PRIMARY_10 primary key (ID);


create table GF_DPS_ENV
(
  id      NUMBER(10) not null,
  key     VARCHAR2(20 CHAR) not null,
  name    VARCHAR2(50 CHAR) not null,
  is_prod NUMBER(10) not null
)
;
alter table GF_DPS_ENV
  add constraint PRIMARY_11 primary key (ID);


create table GF_DPS_ENV_ROLE
(
  id      NUMBER(10) not null,
  role_id NUMBER(10) not null,
  env_key VARCHAR2(20 CHAR) not null,
  state   NUMBER(10) not null
)
;
alter table GF_DPS_ENV_ROLE
  add constraint PRIMARY_12 primary key (ID);


create table GF_DPS_FILE_INFO
(
  file_id         NUMBER(10) not null,
  file_name       VARCHAR2(128),
  file_size       VARCHAR2(32),
  create_username VARCHAR2(64),
  env             VARCHAR2(8),
  create_time     DATE,
  mdfive_value    VARCHAR2(128),
  file_type       VARCHAR2(64)
)
;
comment on column GF_DPS_FILE_INFO.file_name
  is '文件名称';
comment on column GF_DPS_FILE_INFO.file_size
  is '文件大小';
comment on column GF_DPS_FILE_INFO.env
  is '部署环境';
comment on column GF_DPS_FILE_INFO.mdfive_value
  is '文件md5值';
comment on column GF_DPS_FILE_INFO.file_type
  is '文件类型';
alter table GF_DPS_FILE_INFO
  add constraint FILE_INFO_ID primary key (FILE_ID);


create table GF_DPS_MANAGED_SERVERS
(
  id                 NUMBER(10) not null,
  domain_id          NUMBER(10) not null,
  server_name        VARCHAR2(40 CHAR) not null,
  server_ip          VARCHAR2(20 CHAR) not null,
  server_port        NUMBER(10) not null,
  is_monitor         NUMBER(10),
  promet_port        NUMBER(10),
  server_is_start    NUMBER(10) not null,
  server_domain_name VARCHAR2(40 CHAR),
  deploy_file_id     VARCHAR2(200 CHAR),
  lastest_file       VARCHAR2(50 CHAR),
  deploy_info        VARCHAR2(1024 CHAR),
  is_admin           NUMBER(10)
)
;
comment on column GF_DPS_MANAGED_SERVERS.id
  is 'managedId';
create index PK_IP_PORT_1 on GF_DPS_MANAGED_SERVERS (SERVER_IP, SERVER_PORT);
alter table GF_DPS_MANAGED_SERVERS
  add constraint PRIMARY_13 primary key (ID);


create table GF_DPS_MENU
(
  id          NUMBER(10) not null,
  text        VARCHAR2(100 CHAR) not null,
  type        NUMBER(10) not null,
  icon        VARCHAR2(255 CHAR) not null,
  route       VARCHAR2(255 CHAR),
  translate   VARCHAR2(255 CHAR),
  parent      NUMBER(10) default '0' not null,
  url_matches VARCHAR2(1000 CHAR),
  ordered     NUMBER(10) default '0' not null,
  html        VARCHAR2(200 CHAR)
)
;
alter table GF_DPS_MENU
  add constraint PRIMARY_14 primary key (ID);


create table GF_DPS_MENU_ROLE
(
  id      NUMBER(10) not null,
  role_id NUMBER(10) not null,
  menu_id NUMBER(10) not null,
  state   NUMBER(10) not null
)
;
alter table GF_DPS_MENU_ROLE
  add constraint PRIMARY_15 primary key (ID);


create table GF_DPS_MONITOR_SERVER
(
  id          NUMBER(10) not null,
  name        VARCHAR2(255 CHAR) not null,
  ip          VARCHAR2(255 CHAR) not null,
  port        NUMBER(10) not null,
  status      VARCHAR2(50 CHAR) not null,
  create_time DATE not null,
  env         VARCHAR2(50 CHAR) not null,
  remark      VARCHAR2(255 CHAR),
  type_name   VARCHAR2(125 CHAR)
)
;
comment on column GF_DPS_MONITOR_SERVER.name
  is '监控服务器名称';
comment on column GF_DPS_MONITOR_SERVER.ip
  is '监控服务器IP';
comment on column GF_DPS_MONITOR_SERVER.port
  is '监控服务器端口';
comment on column GF_DPS_MONITOR_SERVER.status
  is '在线状态';
comment on column GF_DPS_MONITOR_SERVER.remark
  is '只有管理员有资格管理此页面，所以不需要create_userid字段';
comment on column GF_DPS_MONITOR_SERVER.type_name
  is '类型名';
alter table GF_DPS_MONITOR_SERVER
  add constraint PRIMARY_16 primary key (ID);


create table GF_DPS_NGINX_SERVER
(
  id              NUMBER(10) not null,
  name            VARCHAR2(50 CHAR),
  ip              VARCHAR2(50 CHAR),
  port            NUMBER(10),
  host_name       VARCHAR2(50 CHAR),
  install_path    VARCHAR2(200 CHAR),
  version         VARCHAR2(50 CHAR),
  description     VARCHAR2(200 CHAR),
  create_time     DATE,
  is_monitor      NUMBER(10),
  monitor_port    NUMBER(10),
  create_username VARCHAR2(50 CHAR),
  create_userid   NUMBER(10),
  env             VARCHAR2(50 CHAR),
  client_id       NUMBER(10)
)
;
alter table GF_DPS_NGINX_SERVER
  add constraint PK_NGINX_SERVER_ID primary key (ID);


create table GF_DPS_OGG_INSTANCE
(
  id              NUMBER(8) not null,
  name            VARCHAR2(256),
  ip              VARCHAR2(128),
  port            NUMBER(8),
  user_name       VARCHAR2(256),
  password        VARCHAR2(256),
  ogg_path        VARCHAR2(256),
  ogg_type        VARCHAR2(8),
  interval        VARCHAR2(256),
  env             VARCHAR2(20),
  create_userid   NUMBER(10),
  create_username VARCHAR2(50 CHAR),
  remark          VARCHAR2(4000 CHAR),
  recever_mails   VARCHAR2(4000 CHAR),
  send_type       VARCHAR2(50 CHAR),
  mail_id         NUMBER(8),
  message_id      NUMBER(8)
);
-- Add comments to the table
comment on table GF_DPS_OGG_INSTANCE
  is 'OGG目标端和源端信息表';
-- Add comments to the columns
comment on column GF_DPS_OGG_INSTANCE.name
  is '名称';
comment on column GF_DPS_OGG_INSTANCE.ip
  is '服务器地址';
comment on column GF_DPS_OGG_INSTANCE.port
  is 'SSH端口';
comment on column GF_DPS_OGG_INSTANCE.user_name
  is '登录用户名';
comment on column GF_DPS_OGG_INSTANCE.password
  is '登录密码';
comment on column GF_DPS_OGG_INSTANCE.ogg_path
  is 'ggsci所在目录路径';
comment on column GF_DPS_OGG_INSTANCE.ogg_type
  is '类型：01源端服务器，02目标端服务器';
comment on column GF_DPS_OGG_INSTANCE.interval
  is '监控周期 ';
comment on column GF_DPS_OGG_INSTANCE.env
  is '部署环境';
comment on column GF_DPS_OGG_INSTANCE.recever_mails
  is '接收者邮箱信息,逗号隔开：xx1@163.com,xx2@163.com';
comment on column GF_DPS_OGG_INSTANCE.send_type
  is '告警发送类型：01邮件服务器， 02告警平台';
comment on column GF_DPS_OGG_INSTANCE.mail_id
  is '邮件服务器 ID';
comment on column GF_DPS_OGG_INSTANCE.message_id
  is '告警平台 ID';
-- Create/Recreate primary, unique and foreign key constraints
alter table GF_DPS_OGG_INSTANCE
  add constraint PK_OGG_INSTANCE_ID primary key (ID)
  using index ;


create table GF_DPS_OPERATE_LOG
(
  id              NUMBER(10) not null,
  type            NUMBER(10) not null,
  service_id      NUMBER(10) not null,
  client_ip       VARCHAR2(4000 CHAR),
  service_name    VARCHAR2(255 CHAR) not null,
  log_id          VARCHAR2(20 CHAR) not null,
  create_userid   NUMBER(10) not null,
  create_username VARCHAR2(20 CHAR) not null,
  create_time     DATE not null,
  env             VARCHAR2(16)
)
;
alter table GF_DPS_OPERATE_LOG
  add constraint PRIMARY_17 primary key (ID);



create table GF_DPS_PERFORMANCE_MONITOR
(
  id          NUMBER(10) not null,
  app_id      NUMBER(10),
  name        VARCHAR2(255 CHAR) not null,
  ip_port     VARCHAR2(50 CHAR) not null,
  url         VARCHAR2(255 CHAR) not null,
  type        VARCHAR2(50 CHAR) not null,
  create_time DATE not null,
  env         VARCHAR2(50 CHAR) not null,
  remark      VARCHAR2(255 CHAR)
)
;
comment on column GF_DPS_PERFORMANCE_MONITOR.remark
  is '只有管理员有资格管理此页面，所以不需要create_userid字段';
create unique index UNIQUE_URL on GF_DPS_PERFORMANCE_MONITOR (URL);
alter table GF_DPS_PERFORMANCE_MONITOR
  add constraint PRIMARY_18 primary key (ID);


create table GF_DPS_ROLE
(
  id          NUMBER(10) not null,
  role_name   VARCHAR2(50 CHAR) not null,
  description VARCHAR2(255 CHAR) not null
)
;
alter table GF_DPS_ROLE
  add constraint PRIMARY_19 primary key (ID);


create table GF_DPS_SCRIPT_INFO
(
  script_id       NUMBER(10) not null,
  script_name     VARCHAR2(128),
  script_category VARCHAR2(8),
  script_type     VARCHAR2(8),
  time_out        VARCHAR2(8),
  script_param    VARCHAR2(128),
  update_date     DATE,
  script_desc     VARCHAR2(512),
  script_content  CLOB,
  save_path       VARCHAR2(512),
  client_ids      VARCHAR2(512),
  create_username VARCHAR2(64),
  env             VARCHAR2(8)
)
;
comment on table GF_DPS_SCRIPT_INFO
  is '脚本信息表';
comment on column GF_DPS_SCRIPT_INFO.script_id
  is 'ID';
comment on column GF_DPS_SCRIPT_INFO.script_name
  is '脚本名称';
comment on column GF_DPS_SCRIPT_INFO.script_category
  is '脚本类别:01 运维脚本 02 巡检脚本';
comment on column GF_DPS_SCRIPT_INFO.script_type
  is '脚本类型: 01 shell、 02python 、03 perl';
comment on column GF_DPS_SCRIPT_INFO.time_out
  is '超时时间：单位秒';
comment on column GF_DPS_SCRIPT_INFO.script_param
  is '脚本参数 空格隔开：p1 p2';
comment on column GF_DPS_SCRIPT_INFO.update_date
  is '更新时间';
comment on column GF_DPS_SCRIPT_INFO.script_desc
  is '脚本说明';
comment on column GF_DPS_SCRIPT_INFO.script_content
  is '脚本内容';
comment on column GF_DPS_SCRIPT_INFO.save_path
  is '脚本保存地址';
comment on column GF_DPS_SCRIPT_INFO.client_ids
  is '脚本同步主机的id集合';
comment on column GF_DPS_SCRIPT_INFO.create_username
  is '脚本的创建用户';
comment on column GF_DPS_SCRIPT_INFO.env
  is '脚本的创建环境';
alter table GF_DPS_SCRIPT_INFO
  add constraint PK_SCRIPT_ID primary key (SCRIPT_ID);
alter table GF_DPS_SCRIPT_INFO
  add constraint PK_SCRIPT_NAME_UNIQ unique (SCRIPT_NAME, SCRIPT_TYPE, ENV, CREATE_USERNAME);


create table GF_DPS_SERVER_INFO
(
  id             NUMBER(10) not null,
  ip             VARCHAR2(50 CHAR),
  port           NUMBER(10),
  username       VARCHAR2(50 CHAR),
  password       VARCHAR2(50 CHAR),
  upload_path    VARCHAR2(100 CHAR),
  workspace_home VARCHAR2(100 CHAR),
  app_path       VARCHAR2(100 CHAR),
  update_path    VARCHAR2(100 CHAR),
  java_name      VARCHAR2(100 CHAR),
  java_path      VARCHAR2(100 CHAR),
  java_folder    VARCHAR2(100 CHAR),
  log_path       VARCHAR2(100 CHAR),
  deploy_path    VARCHAR2(100 CHAR),
  backup_path    VARCHAR2(100 CHAR)
)
;
alter table GF_DPS_SERVER_INFO
  add constraint PRIMARY_20 primary key (ID);


create table GF_DPS_SHELL_LOG
(
  id          NUMBER(19) not null,
  status      VARCHAR2(11 CHAR),
  content     CLOB,
  create_time DATE not null
)
;
alter table GF_DPS_SHELL_LOG
  add constraint PRIMARY_21 primary key (ID);


create table GF_DPS_TOMCAT_MIDDLEWARE
(
  id              NUMBER(10) not null,
  name            VARCHAR2(50 CHAR),
  port            NUMBER(10),
  promet_port     NUMBER(10),
  host_name       VARCHAR2(50 CHAR),
  ip              VARCHAR2(50 CHAR),
  install_path    VARCHAR2(200 CHAR),
  version         VARCHAR2(50 CHAR),
  description     VARCHAR2(200 CHAR) not null,
  create_time     DATE,
  deploy_file_id  VARCHAR2(200 CHAR),
  lastest_file    VARCHAR2(50 CHAR),
  deploy_info     VARCHAR2(1024 CHAR),
  is_monitor      NUMBER(10),
  create_username VARCHAR2(50 CHAR),
  create_userid   NUMBER(10),
  env             VARCHAR2(50 CHAR)
)
;
alter table GF_DPS_TOMCAT_MIDDLEWARE
  add constraint PRIMARY_22 primary key (ID);


create table GF_DPS_USERS
(
  id              NUMBER(10) not null,
  name            VARCHAR2(20 CHAR) not null,
  username        VARCHAR2(20 CHAR) not null,
  phone           VARCHAR2(20 CHAR) not null,
  email           VARCHAR2(50 CHAR) not null,
  password        VARCHAR2(100 CHAR) not null,
  head_img        VARCHAR2(255 CHAR),
  last_login_time DATE,
  last_login_ip   VARCHAR2(50 CHAR),
  create_time     DATE,
  state           NUMBER(10) default '1' not null,
  is_admin        NUMBER(10) not null,
  org_no          VARCHAR2(64)
)
;
create unique index USERNAME_UK on GF_DPS_USERS (USERNAME);
alter table GF_DPS_USERS
  add constraint PRIMARY_25 primary key (ID);

-- Add/modify columns
--alter table GF_DPS_USERS add org_no VARCHAR2(64);
-- Add comments to the columns
-- Add comments to the columns
comment on column GF_DPS_USERS.name
  is '姓名';
comment on column GF_DPS_USERS.username
  is '用户名';
comment on column GF_DPS_USERS.phone
  is '电话';
comment on column GF_DPS_USERS.email
  is '邮件';
comment on column GF_DPS_USERS.password
  is '密码';
comment on column GF_DPS_USERS.last_login_time
  is '上次登录时间';
comment on column GF_DPS_USERS.last_login_ip
  is '上次登录IP';
comment on column GF_DPS_USERS.org_no
  is '单位';


create table GF_DPS_USER_APP
(
  id          NUMBER(10) not null,
  user_id     NUMBER(10) not null,
  app_id      NUMBER(10) not null,
  create_time DATE not null,
  state       NUMBER(10) not null
)
;
alter table GF_DPS_USER_APP
  add constraint PRIMARY_23 primary key (ID);
alter table GF_DPS_USER_APP
  add constraint UNI_USERAPP_USER_ID_APP_ID unique (USER_ID, APP_ID);


create table GF_DPS_USER_ROLE
(
  id          NUMBER(10) not null,
  user_id     NUMBER(10) not null,
  role_id     NUMBER(10) not null,
  create_time DATE not null,
  state       NUMBER(10) not null
)
;
alter table GF_DPS_USER_ROLE
  add constraint PRIMARY_24 primary key (ID);


-- Create table
create table GF_DPS_MAIL_SMTP_CONFIG
(
  id          NUMBER(10) not null,
  mail_name   VARCHAR2(128 CHAR),
  mail_type   VARCHAR2(128 CHAR),
  host        VARCHAR2(16 CHAR),
  port        NUMBER(8),
  user_name   VARCHAR2(128 CHAR),
  password    VARCHAR2(128 CHAR),
  from_user   VARCHAR2(128 CHAR),
  is_enable   VARCHAR2(2 CHAR),
  create_time DATE,
  env         VARCHAR2(128 CHAR)
);
-- Add comments to the table
comment on table GF_DPS_MAIL_SMTP_CONFIG
  is '邮件smtp服务器配置表';
-- Add comments to the columns
comment on column GF_DPS_MAIL_SMTP_CONFIG.mail_name
  is '邮件服务器名称';
comment on column GF_DPS_MAIL_SMTP_CONFIG.mail_type
  is '类型：smtp';
comment on column GF_DPS_MAIL_SMTP_CONFIG.host
  is 'SMTP服务器地址';
comment on column GF_DPS_MAIL_SMTP_CONFIG.port
  is 'SMTP服务器端口';
comment on column GF_DPS_MAIL_SMTP_CONFIG.user_name
  is '用户名';
comment on column GF_DPS_MAIL_SMTP_CONFIG.password
  is '密码';
comment on column GF_DPS_MAIL_SMTP_CONFIG.from_user
  is '发件人邮箱';
comment on column GF_DPS_MAIL_SMTP_CONFIG.is_enable
  is '0否，1是';
comment on column GF_DPS_MAIL_SMTP_CONFIG.create_time
  is '创建时间';
comment on column GF_DPS_MAIL_SMTP_CONFIG.env
  is '环境';
-- Create/Recreate primary, unique and foreign key constraints
alter table GF_DPS_MAIL_SMTP_CONFIG
  add constraint PK_GF_DPS_MAIL_ID primary key (ID)
  using index ;

-- Create table
create table GF_DPS_OGG_INSTANCE_PROGRAM
(
  id               NUMBER(8) not null,
  program          VARCHAR2(256),
  status           VARCHAR2(256),
  group_name       VARCHAR2(256),
  lag_at_chkpt     VARCHAR2(256),
  time_since_chkpt VARCHAR2(256),
  instance_id      NUMBER(8),
  check_count      NUMBER(8),
  check_time       DATE
);
-- Add comments to the table
comment on table GF_DPS_OGG_INSTANCE_PROGRAM
  is 'OGG进程状态信息表';
-- Add comments to the columns
comment on column GF_DPS_OGG_INSTANCE_PROGRAM.program
  is '进程名';
comment on column GF_DPS_OGG_INSTANCE_PROGRAM.status
  is '进程状态';
comment on column GF_DPS_OGG_INSTANCE_PROGRAM.group_name
  is '组名';
comment on column GF_DPS_OGG_INSTANCE_PROGRAM.instance_id
  is 'ogg实例ID';
comment on column GF_DPS_OGG_INSTANCE_PROGRAM.check_count
  is '发送告警次数';
comment on column GF_DPS_OGG_INSTANCE_PROGRAM.check_time
  is '更新时间';
-- Create/Recreate indexes
create index PK_OGG_PROGRAM_ID on GF_DPS_OGG_INSTANCE_PROGRAM (ID);


-- Create table
create table GF_DPS_ORG
(
  org_no        VARCHAR2(64) not null,
  org_name      VARCHAR2(100),
  parent_org_no VARCHAR2(64),
  org_type      VARCHAR2(8),
  org_sort      NUMBER(10),
  org_tree      VARCHAR2(1024),
  org_full_name VARCHAR2(1024),
  org_user      VARCHAR2(100),
  org_phone     VARCHAR2(100),
  org_address   VARCHAR2(1024),
  org_zip_code  VARCHAR2(100),
  org_email     VARCHAR2(300),
  org_status    VARCHAR2(8),
  create_user   VARCHAR2(64),
  create_date   DATE,
  update_date   DATE,
  remarks       NVARCHAR2(500),
  corp_code     VARCHAR2(64),
  corp_name     NVARCHAR2(100),
  extend1       VARCHAR2(1024),
  extend2       VARCHAR2(1024),
  extend3       VARCHAR2(1024),
  extend4       VARCHAR2(1024),
  extend5       VARCHAR2(1024),
  env           VARCHAR2(64)
);
-- Add comments to the columns
comment on column GF_DPS_ORG.org_no
  is '机构编码';
comment on column GF_DPS_ORG.org_name
  is '机构名称';
comment on column GF_DPS_ORG.parent_org_no
  is '上级机构编号';
comment on column GF_DPS_ORG.org_type
  is '机构类别： 00 一级 01 二级 02 三级 03 四级 04五级';
comment on column GF_DPS_ORG.org_sort
  is '本级排序号（递增）';
comment on column GF_DPS_ORG.org_tree
  is '树层次编码：21102|21401';
comment on column GF_DPS_ORG.org_full_name
  is '机构全称';
comment on column GF_DPS_ORG.org_user
  is '机构负责人';
comment on column GF_DPS_ORG.org_phone
  is '机构电话';
comment on column GF_DPS_ORG.org_address
  is '机构地址';
comment on column GF_DPS_ORG.org_zip_code
  is '邮政编码';
comment on column GF_DPS_ORG.org_email
  is '机构邮箱';
comment on column GF_DPS_ORG.org_status
  is '状态（0删除 1正常  2停用）';
comment on column GF_DPS_ORG.create_user
  is '创建者';
comment on column GF_DPS_ORG.create_date
  is '创建时间';
comment on column GF_DPS_ORG.update_date
  is '更新时间';
comment on column GF_DPS_ORG.remarks
  is '备注';
comment on column GF_DPS_ORG.extend1
  is '扩展字段1';
-- Create/Recreate primary, unique and foreign key constraints
alter table GF_DPS_ORG  add primary key (ORG_NO) using index;

-- V2.1.1 add tables
-- Create table
create table GF_DPS_REDIS_CLUSTER
(
  cluster_id               NUMBER(10) not null,
  group_id                 NUMBER(10),
  user_id                  NUMBER(10),
  cluster_token            VARCHAR2(256),
  cluster_name             VARCHAR2(256),
  nodes                    VARCHAR2(4000),
  redis_mode               VARCHAR2(256),
  os                       VARCHAR2(256),
  redis_version            VARCHAR2(256),
  image                    VARCHAR2(256),
  initialized              NUMBER(2),
  total_used_memory        NUMBER(16),
  total_keys               NUMBER(16),
  total_expires            NUMBER(16),
  db_size                  NUMBER(16),
  cluster_state            VARCHAR2(256),
  cluster_slots_assigned   NUMBER(16),
  cluster_slots_ok         NUMBER(16),
  cluster_slots_pfail      NUMBER(16),
  cluster_slots_fail       NUMBER(16),
  cluster_known_nodes      NUMBER(10),
  cluster_size             NUMBER(10),
  sentinel_ok              NUMBER(10),
  sentinel_masters         NUMBER(10),
  master_ok                NUMBER(10),
  redis_password           VARCHAR2(256),
  rule_ids                 VARCHAR2(256),
  channel_ids              VARCHAR2(256),
  cluster_alert            VARCHAR2(256),
  installation_environment VARCHAR2(256),
  installation_type        NUMBER(10),
  update_time              DATE,
  env                      VARCHAR2(20),
  user_name                VARCHAR2(256)
);
-- Add comments to the table
comment on table GF_DPS_REDIS_CLUSTER
  is 'redis 管理';
-- Add comments to the columns
comment on column GF_DPS_REDIS_CLUSTER.cluster_id
  is '集群ID';
comment on column GF_DPS_REDIS_CLUSTER.user_id
  is '用户id';
comment on column GF_DPS_REDIS_CLUSTER.cluster_name
  is 'redis集群名称';
comment on column GF_DPS_REDIS_CLUSTER.nodes
  is 'redis节点: 192.168.174.44:26379,192.168.174.44:26379,192.168.174.44:26379';
comment on column GF_DPS_REDIS_CLUSTER.redis_mode
  is 'redis模式:  standalone, cluster,sentinel';
comment on column GF_DPS_REDIS_CLUSTER.os
  is '操作系统版本';
comment on column GF_DPS_REDIS_CLUSTER.redis_version
  is 'redis版本';
comment on column GF_DPS_REDIS_CLUSTER.total_used_memory
  is '内存占用大小';
comment on column GF_DPS_REDIS_CLUSTER.total_keys
  is 'key的数量';
comment on column GF_DPS_REDIS_CLUSTER.cluster_state
  is '集群状态 HEALTH';
comment on column GF_DPS_REDIS_CLUSTER.cluster_slots_assigned
  is '已分配的哈希槽';
comment on column GF_DPS_REDIS_CLUSTER.cluster_slots_ok
  is '哈希槽状态不是FAIL 和 PFAIL 的数量';
comment on column GF_DPS_REDIS_CLUSTER.cluster_size
  is 'master 数量';
comment on column GF_DPS_REDIS_CLUSTER.redis_password
  is 'redis密码';
-- Create/Recreate primary, unique and foreign key constraints
alter table GF_DPS_REDIS_CLUSTER
  add constraint PK_REDIS_CLUSTER_ID primary key (CLUSTER_ID)
  using index ;


-- Create table
create table GF_DPS_REDIS_NODE
(
  redis_node_id  NUMBER(10) not null,
  group_id       NUMBER(10),
  cluster_id     NUMBER(10),
  node_id        VARCHAR2(256),
  master_id      VARCHAR2(256),
  host           VARCHAR2(256),
  port           NUMBER(10),
  node_role      VARCHAR2(256),
  flags          VARCHAR2(8),
  link_state     VARCHAR2(256),
  in_cluster     NUMBER(2),
  run_status     NUMBER(2),
  slot_range     VARCHAR2(256),
  slot_number    NUMBER(10),
  container_id   NUMBER(10),
  container_name VARCHAR2(256),
  insert_time    DATE,
  update_time    DATE
);
-- Add comments to the table
comment on table GF_DPS_REDIS_NODE
  is 'redis节点管理';
-- Add comments to the columns
comment on column GF_DPS_REDIS_NODE.cluster_id
  is '集群id';
comment on column GF_DPS_REDIS_NODE.node_id
  is '节点id';
comment on column GF_DPS_REDIS_NODE.master_id
  is 'masterid';
comment on column GF_DPS_REDIS_NODE.host
  is 'ip';
comment on column GF_DPS_REDIS_NODE.port
  is '端口';
comment on column GF_DPS_REDIS_NODE.node_role
  is '角色: master,slave';
comment on column GF_DPS_REDIS_NODE.flags
  is ' master,slave';
comment on column GF_DPS_REDIS_NODE.link_state
  is '连接状态 connected，unconnected';
comment on column GF_DPS_REDIS_NODE.in_cluster
  is '是否集群1,0';
comment on column GF_DPS_REDIS_NODE.run_status
  is '运行状态1,0';
comment on column GF_DPS_REDIS_NODE.slot_range
  is '插槽范围0-5460';
comment on column GF_DPS_REDIS_NODE.slot_number
  is '插槽数量';
-- Create/Recreate primary, unique and foreign key constraints
alter table GF_DPS_REDIS_NODE
  add constraint PK_REDIS_NODE_ID primary key (REDIS_NODE_ID)
  using index ;


-- Create table
create table GF_DPS_REDIS_NODE_INFO
(
   info_id                    NUMBER(10) not null,
  cluster_id                 NUMBER(10),
  node                       VARCHAR2(256),
  role                       VARCHAR2(256),
  time_type                  NUMBER(10),
  last_time                  NUMBER(32),
  response_time              NUMBER(32),
  connected_clients          NUMBER(32),
  client_longest_output_list NUMBER(32),
  client_biggest_input_buf   NUMBER(32),
  blocked_clients            NUMBER(32),
  used_memory                NUMBER(32),
  used_memory_rss            NUMBER(32),
  used_memory_overhead       NUMBER(32),
  used_memory_dataset        NUMBER(32),
  used_memory_dataset_perc   NUMBER(10,2),
  mem_fragmentation_ratio    NUMBER(10,2),
  total_connections_received NUMBER(32),
  connections_received       NUMBER(32),
  rejected_connections       NUMBER(32),
  total_commands_processed   NUMBER(32),
  commands_processed         NUMBER(32),
  instantaneous_ops_per_sec  NUMBER(32),
  total_net_input_bytes      NUMBER(32),
  net_input_bytes            NUMBER(32),
  total_net_output_bytes     NUMBER(32),
  net_output_bytes           NUMBER(32),
  sync_full                  NUMBER(32),
  sync_partial_ok            NUMBER(32),
  sync_partial_err           NUMBER(32),
  keyspace_misses            NUMBER(32),
  keyspace_hits              NUMBER(32),
  keyspace_hits_ratio        NUMBER(10,2),
  used_cpu_sys               NUMBER(10,2),
  cpu_sys                    NUMBER(10,2),
  used_cpu_user              NUMBER(10,2),
  cpu_user                   NUMBER(10,2),
  redis_keys                 NUMBER(32),
  expires                    NUMBER(32),
  update_time                DATE
);
-- Add comments to the table
comment on table GF_DPS_REDIS_NODE_INFO
  is 'redis节点指标信息';
-- Add comments to the columns
comment on column GF_DPS_REDIS_NODE_INFO.connected_clients
  is '连接客户端数';
comment on column GF_DPS_REDIS_NODE_INFO.blocked_clients
  is '被阻塞的连接个数';
comment on column GF_DPS_REDIS_NODE_INFO.used_memory
  is '已使用的内存 MB';
comment on column GF_DPS_REDIS_NODE_INFO.used_memory_rss
  is '物理内存的大小 MB';
comment on column GF_DPS_REDIS_NODE_INFO.used_memory_overhead
  is '内存开销 MB';
comment on column GF_DPS_REDIS_NODE_INFO.used_memory_dataset
  is '数据占用的内存大小 MB';
comment on column GF_DPS_REDIS_NODE_INFO.mem_fragmentation_ratio
  is '内存碎片率';
comment on column GF_DPS_REDIS_NODE_INFO.connections_received
  is '新创建连接个数';
comment on column GF_DPS_REDIS_NODE_INFO.rejected_connections
  is ' 拒绝的连接个数';
comment on column GF_DPS_REDIS_NODE_INFO.commands_processed
  is '处理的命令数';
comment on column GF_DPS_REDIS_NODE_INFO.instantaneous_ops_per_sec
  is '每秒执行的命令数';
comment on column GF_DPS_REDIS_NODE_INFO.sync_full
  is '主从完全同步成功次数';
comment on column GF_DPS_REDIS_NODE_INFO.sync_partial_ok
  is '主从部分同步成功次数';
comment on column GF_DPS_REDIS_NODE_INFO.sync_partial_err
  is '主从部分同步失败次数';
comment on column GF_DPS_REDIS_NODE_INFO.keyspace_hits_ratio
  is '请求键的命中率 %';
comment on column GF_DPS_REDIS_NODE_INFO.redis_keys
  is 'key总数';
comment on column GF_DPS_REDIS_NODE_INFO.expires
  is '过期key数';
-- Create/Recreate indexes
create index REDIS_NODE_INFO_CLUSTER on GF_DPS_REDIS_NODE_INFO (CLUSTER_ID);
create index REDIS_NODE_INFO_QUERY on GF_DPS_REDIS_NODE_INFO (TIME_TYPE, UPDATE_TIME, NODE, CLUSTER_ID);
-- Create/Recreate primary, unique and foreign key constraints
alter table GF_DPS_REDIS_NODE_INFO
  add constraint PK_REDIS_NODE_INFO_ID primary key (INFO_ID)
  using index ;


-- Create table
create table GF_DPS_REDIS_SENTINEL_MASTERS
(
  sentinel_master_id      NUMBER(10) not null,
  cluster_id              NUMBER(10),
  group_id                NUMBER(10),
  name                    VARCHAR2(256),
  host                    VARCHAR2(256),
  port                    NUMBER(10),
  flags                   VARCHAR2(256),
  last_master_node        VARCHAR2(256),
  status                  VARCHAR2(256),
  link_pending_commands   NUMBER(32),
  link_refcount           NUMBER(32),
  last_ping_sent          NUMBER(32),
  last_ok_ping_reply      NUMBER(32),
  last_ping_reply         NUMBER(32),
  s_down_time             NUMBER(32),
  o_down_time             NUMBER(32),
  down_after_milliseconds NUMBER(32),
  info_refresh            NUMBER(32),
  role_reported           VARCHAR2(256),
  role_reported_time      NUMBER(32),
  config_epoch            NUMBER(32),
  num_slaves              NUMBER(10),
  sentinels               NUMBER(10),
  quorum                  NUMBER(10),
  failover_timeout        NUMBER(32),
  parallel_syncs          NUMBER(10),
  auth_pass               VARCHAR2(256),
  update_time             DATE
);
-- Create/Recreate primary, unique and foreign key constraints
alter table GF_DPS_REDIS_SENTINEL_MASTERS
  add constraint PK_REDIS_SENTINEL_MASTERS_ID primary key (SENTINEL_MASTER_ID)
  using index ;

alter table GF_DPS_APP modify start_context VARCHAR2(4000 CHAR);
