create or replace view gf_dps_table_ip_port_list as
select ams.id as id ,'gf_dps_alert_manager_server' as table_name,
               ams.ip as ip,
               ams.port as port
          from gf_dps_alert_manager_server ams
          where port is not null and port <>0
        union all
        select adc.id as id , 'gf_dps_app_deploy_custom' as table_name,
               adc.ip as ip,
               adc.port as port
          from gf_dps_app_deploy_custom adc
          where port is not null and port <>0
        union all
        select asb.id as id ,'gf_dps_app_springboot' as table_name,
               asb.springboot_host as ip,
               asb.springboot_port as port
          from gf_dps_app_springboot asb
          where asb.state = '1'
          and springboot_port is not null and springboot_port <>0
        union all
        select asb.id as id, 'gf_dps_app_springboot' as table_name,
               asb.springboot_host as ip,
               asb.promet_port as port
          from gf_dps_app_springboot asb
          where asb.state = '1'
          and promet_port is not null and promet_port <>0
        union all
        select c.id as id, 'gf_dps_client' as table_name, c.HOST_ADDRESS as ip, c.port as port
          from gf_dps_client c
          where port is not null and port <>0
        union all
        select c.id as id , 'gf_dps_client' as table_name,
               c.HOST_ADDRESS as ip,
               c.performance_port as port
          from gf_dps_client c
          where performance_port is not null and performance_port <>0
        union all
        select d.id as id ,'gf_dps_domain' as table_name,
               d.admin_ip as ip,
               d.admin_port as port
          from gf_dps_domain d
          where admin_port is not null and admin_port <>0
        union all
        select d.id as id, 'gf_dps_domain' as table_name,
               d.admin_ip as ip,
               d.promet_port as port
          from gf_dps_domain d
          where promet_port is not null and promet_port <>0
        union all
        select mas.id as id , 'gf_dps_managed_servers' as table_name,
               mas.server_ip as ip,
               mas.server_port as port
          from gf_dps_managed_servers mas
          where server_port is not null and server_port <>0
        union all
        select mas.id as id, 'gf_dps_managed_servers' as table_name,
               mas.server_ip as ip,
               mas.promet_port as port
          from gf_dps_managed_servers mas
          where promet_port is not null and promet_port <>0
        union all
        select  mos.id as id ,'gf_dps_monitor_server' as table_name,
               mos.ip as ip,
               mos.port as port
          from gf_dps_monitor_server mos
          where port is not null and port <>0
        union all
        select  m.id as id ,'gf_dps_tomcat_middleware' as table_name, m.ip as ip, m.port as port
          from gf_dps_tomcat_middleware m
          where port is not null and port <>0
        union all
        select  m.id as id ,'gf_dps_tomcat_middleware' as table_name,
               m.ip as ip,
               m.promet_port as port
          from gf_dps_tomcat_middleware m
          where promet_port is not null and promet_port <>0;
