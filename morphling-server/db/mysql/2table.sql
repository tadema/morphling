/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.174.21
 Source Server Type    : MySQL
 Source Server Version : 50548
 Source Host           : 192.168.174.21:3306
 Source Schema         : morphling

 Target Server Type    : MySQL
 Target Server Version : 50548
 File Encoding         : 65001

 Date: 10/04/2020 14:19:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gf_dps_alert_info
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_alert_info`;
CREATE TABLE `gf_dps_alert_info`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `receiver` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `status` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `alerts_status` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `alert_name` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `ip` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `port` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `instance` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `job` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `severity` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `summary` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `starts_at` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `end_at` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `generatorurl` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `common_annotations` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `externalurl` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `version` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `group_key` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `timestamp` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `is_flag` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 62536 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_alert_manager_server
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_alert_manager_server`;
CREATE TABLE `gf_dps_alert_manager_server`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `port` int(10) DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `env` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `type_name` varchar(125) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gf_dps_alert_manager_server
-- ----------------------------
INSERT INTO `gf_dps_alert_manager_server` VALUES (1, 'alertmanager', '192.168.174.6', 9093, 'DOWN', '2020-03-06 09:15:08', 'dev', 'alertmanager', 'alertmanager');

-- ----------------------------
-- Table structure for gf_dps_alert_rules
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_alert_rules`;
CREATE TABLE `gf_dps_alert_rules`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `group_name` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '规则组名',
  `alert_name` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '规则名称',
  `expr_info` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '规则公式',
  `for_time` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '持续时长（1s 1m 1h）',
  `labels_serverity` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '告警重要性等级  red、orange、yello   3、2、1',
  `labels_value` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '告警公式计算结果',
  `annotations_summary` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '规则概要',
  `annotations_description` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '规则描述',
  `env` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21288 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gf_dps_alert_rules
-- ----------------------------
INSERT INTO `gf_dps_alert_rules` VALUES (12950, 'alert-info', 'node_memory_usage', '100-round((node_memory_MemFree_bytes + node_memory_Buffers_bytes + node_memory_Cached_bytes) / node_memory_MemTotal_bytes * 100) >=70', '1m', 'orange', NULL, '{{ $labels.instance }} :服务器内存使用率过高', '{{ $labels.instance }} :服务器内存使用率超过70% , 监控类型: {{ $labels.job }} ,当前值：({{ $value }}%)', NULL);
INSERT INTO `gf_dps_alert_rules` VALUES (12951, 'alert-info', 'instance-down', 'up == 0', '1m', 'red', NULL, 'Instance {{ $labels.instance }} down', '{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 10 s.', NULL);
INSERT INTO `gf_dps_alert_rules` VALUES (20057, 'alert-info', 'node_cpu_usage', '100-round(avg(rate(node_cpu_seconds_total{mode=\"idle\"}[1m])) by (instance) *100) >=70', '1m', 'red', NULL, '{{ $labels.instance }} :服务器内存使用率过高', '{{ $labels.instance }} :服务器平均每分钟cpu使用率超过70% , 监控类型: {{ $labels.job }}  ,当前值：({{ $value }}%)', NULL);
INSERT INTO `gf_dps_alert_rules` VALUES (21287, 'alert-info', 'node_filesystem_usage', '100-round((node_filesystem_free_bytes{fstype=~\"ext4|xfs\",mountpoint=\"/\"} / node_filesystem_size_bytes{fstype=~\"ext4|xfs\",mountpoint=\"/\"}) * 100 )>=80', '1m', 'yellow', NULL, '{{ $labels.instance }}:服务器硬盘使用率过高', '{{ $labels.instance }} :服务器硬盘使用率超80% , 监控类型: {{ $labels.job }} ,当前值：({{ $value }}%)', NULL);

-- ----------------------------
-- Table structure for gf_dps_alert_sender_config
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_alert_sender_config`;
CREATE TABLE `gf_dps_alert_sender_config`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `app_number` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '消息平台接入客户端编号',
  `type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类型：email 、mobile、wecaht',
  `email` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '邮箱账号，多个账号之间逗号隔开',
  `mobile` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '手机号，多个账号之间逗号隔开',
  `wechat` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '微信号，多个账号之间逗号隔开',
  `titlel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标题',
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户名',
  `ext_msg` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '扩展',
  `biz_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `biz_number` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `env` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `api_url` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'api',
  `responses_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '响应码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39292 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_app
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_app`;
CREATE TABLE `gf_dps_app`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用英文名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '应用描述信息',
  `context_path` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'URL请求路径',
  `git_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'git地址',
  `git_branch` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'git分支名称',
  `git_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'git用户',
  `git_pwd` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'git密码',
  `mvn_module` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '部署命令',
  `mvn_param` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'maven install 命令行参数',
  `env` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '部署环境',
  `service_type` int(10) DEFAULT NULL COMMENT '1：网关，2：服务',
  `deploy_type` int(10) DEFAULT NULL COMMENT '1：自定义部署字段',
  `current_pack_version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `apollo_id` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '用于保存应用扩展信息',
  `admin_user` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_userid` int(10) DEFAULT NULL,
  `create_username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `state` int(10) DEFAULT NULL,
  `file_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'war/jar/目录',
  `file_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `middleware_type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'weblogic/tomcat',
  `developer` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `is_stage` int(10) DEFAULT NULL COMMENT '1:stage;2:no-stage',
  `start_port` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `start_context` text CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `param_front` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'springboot类型java后面的参数',
  `param_behind` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'springboot类型java -jar后面的参数',
  `param_memory` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'tomcat和weblogic的启动内存参数',
  `app_chinese_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '应用的中文名称',
  `promet_port` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'springboot的监控端口',
  `is_monitor` int(10) DEFAULT NULL COMMENT 'springboot监控标记位',
  `java_home` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `exclude_files` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50811 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_app_deploy_custom
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_app_deploy_custom`;
CREATE TABLE `gf_dps_app_deploy_custom`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `app_id` int(10) DEFAULT NULL COMMENT '应用ID',
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '主机IP',
  `port` int(8) DEFAULT NULL COMMENT '端口',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '服务器用户名称',
  `command1` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '部署',
  `command2` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '启动',
  `command3` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '停止',
  `command4` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '重启',
  `command5` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '回滚',
  `command6` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `command7` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `command8` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `command9` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `command10` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `client_id` int(10) DEFAULT NULL COMMENT '主机 ID',
  `file_id` int(10) DEFAULT NULL COMMENT '文件ID',
  `deploy_version` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '部署版本号',
  `app_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '应用类型',
  `type_id` int(10) DEFAULT NULL COMMENT '类型的ID',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `pk_app_id_ip_port`(`app_id`, `port`, `ip`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61751 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '自定义部署信息' ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_app_domain
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_app_domain`;
CREATE TABLE `gf_dps_app_domain`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `app_id` int(10) NOT NULL,
  `domain_id` int(10) NOT NULL,
  `current_version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` int(10) NOT NULL,
  `create_time` datetime NOT NULL,
  `state` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40354 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_app_file
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_app_file`;
CREATE TABLE `gf_dps_app_file`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `app_id` int(10) NOT NULL,
  `source_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文件名和版本名拼接',
  `file_name` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `deploy_type` int(10) DEFAULT NULL COMMENT '1:首次部署 ,2:更新',
  `file_savepath` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `is_deploy` int(10) DEFAULT NULL COMMENT '0:使用中，1:已删除',
  `create_time` datetime DEFAULT NULL,
  `version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `deploy_id` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `upload_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文件版本描述信息',
  `file_size` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文件大小',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 60665 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_app_icon
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_app_icon`;
CREATE TABLE `gf_dps_app_icon`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `developer` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `company_icon` longblob NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2518 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_app_instance
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_app_instance`;
CREATE TABLE `gf_dps_app_instance`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `app_id` int(10) NOT NULL,
  `client_id` int(10) NOT NULL,
  `current_version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` int(10) NOT NULL COMMENT '0:待部署 1：运行中 2已停止',
  `create_time` datetime NOT NULL,
  `state` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gf_dps_app_springboot
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_app_springboot`;
CREATE TABLE `gf_dps_app_springboot`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `app_id` int(10) NOT NULL,
  `springboot_port` int(10) NOT NULL,
  `is_monitor` int(10) DEFAULT NULL,
  `promet_port` int(10) DEFAULT NULL,
  `springboot_host` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `current_version` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `status` int(10) NOT NULL COMMENT '0:待部署 1：运行中 2已停止',
  `create_time` datetime NOT NULL,
  `state` int(10) NOT NULL,
  `start_context` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `deploy_file_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `lastest_file` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `deploy_info` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `create_userid` int(10) DEFAULT NULL,
  `env` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 57895 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_app_tomcat
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_app_tomcat`;
CREATE TABLE `gf_dps_app_tomcat`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `app_id` int(10) NOT NULL,
  `tomcat_id` int(10) NOT NULL,
  `current_version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` int(10) NOT NULL COMMENT '0:待部署 1：运行中 2已停止',
  `create_time` datetime NOT NULL,
  `state` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39417 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_client
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_client`;
CREATE TABLE `gf_dps_client`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '服务器名称',
  `host_address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '服务器IP地址',
  `port` int(10) DEFAULT NULL COMMENT 'Agent端口号',
  `performance_port` int(10) DEFAULT NULL COMMENT '主机监控端口号',
  `env` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '部署环境',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '备注',
  `pack_version` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_userid` int(10) DEFAULT NULL COMMENT '创建用户ID',
  `create_username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建用户名',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `state` int(10) DEFAULT NULL,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主机登录用户',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主机登录密码',
  `linux_port` int(10) NOT NULL COMMENT 'linux的port，一般为22',
  `is_node_monitor` int(10) DEFAULT NULL COMMENT '是否监控：1是，0否',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pk_ip_port`(`host_address`, `port`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 59295 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_client_middleware
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_client_middleware`;
CREATE TABLE `gf_dps_client_middleware`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `client_id` int(10) NOT NULL,
  `middleware_id` int(10) NOT NULL,
  `create_time` datetime NOT NULL,
  `status` int(10) NOT NULL,
  `middleware_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_code
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_code`;
CREATE TABLE `gf_dps_code`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类别',
  `type_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类别名称',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '代码',
  `code_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '代码值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gf_dps_code
-- ----------------------------
INSERT INTO `gf_dps_code` VALUES (1, '100', '监控类型', '1', 'client');
INSERT INTO `gf_dps_code` VALUES (2, '100', '监控类型', '2', 'weblogic-jmx');
INSERT INTO `gf_dps_code` VALUES (3, '100', '监控类型', '3', 'tomcat-jmx');
INSERT INTO `gf_dps_code` VALUES (4, '100', '监控类型', '4', 'springboot-jmx');
INSERT INTO `gf_dps_code` VALUES (5, '101', '脚本类别', '01', '运维脚本');
INSERT INTO `gf_dps_code` VALUES (6, '101', '脚本类别', '02', '巡检脚本');
INSERT INTO `gf_dps_code` VALUES (7, '102', '脚本类型', '01', 'shell');
INSERT INTO `gf_dps_code` VALUES (8, '102', '脚本类型', '02', 'python');
INSERT INTO `gf_dps_code` VALUES (9, '102', '脚本类型', '03', 'perl');
INSERT INTO `gf_dps_code` VALUES (10, '103', '脚本后缀类型', '01', '.sh');
INSERT INTO `gf_dps_code` VALUES (11, '103', '脚本后缀类型', '02', '.py');
INSERT INTO `gf_dps_code` VALUES (12, '103', '脚本后缀类型', '03', '.pl');
INSERT INTO `gf_dps_code` VALUES (13, '104', 'server端基本信息', 'ip', '192.168.174.6');
INSERT INTO `gf_dps_code` VALUES (14, '104', 'server端基本信息', 'port', '33330');
INSERT INTO `gf_dps_code` VALUES (15, '104', 'server端基本信息', 'linuxPort', '22');
INSERT INTO `gf_dps_code` VALUES (16, '104', 'server端基本信息', 'username', 'deploy');
INSERT INTO `gf_dps_code` VALUES (17, '104', 'server端基本信息', 'password', 'dell@105B');
INSERT INTO `gf_dps_code` VALUES (18, '104', 'server端基本信息', 'appPath', '/workspace/app');
INSERT INTO `gf_dps_code` VALUES (19, '104', 'server端基本信息', 'updatePath', '/workspace/update');
INSERT INTO `gf_dps_code` VALUES (20, '104', 'server端基本信息', 'javaName', 'jdk-8u191-linux-x64.tar.gz');
INSERT INTO `gf_dps_code` VALUES (21, '104', 'server端基本信息', 'javaPath', '/app/data/morphling-server/jdk-8u191-linux-x64.tar.gz');
INSERT INTO `gf_dps_code` VALUES (22, '104', 'server端基本信息', 'javaFolder', 'jdk1.8.0_191');
INSERT INTO `gf_dps_code` VALUES (23, '104', 'server端基本信息', 'logPath', '/workspace/log');
INSERT INTO `gf_dps_code` VALUES (24, '104', 'server端基本信息', 'deployPath', '/workspace/deploy');
INSERT INTO `gf_dps_code` VALUES (25, '104', 'server端基本信息', 'backupPath', '/workspace/backup');
INSERT INTO `gf_dps_code` VALUES (26, '105', '文件管理的文件类型', '1', '默认');
INSERT INTO `gf_dps_code` VALUES (27, '105', '文件管理的文件类型', '2', '软件');
INSERT INTO `gf_dps_code` VALUES (28, '105', '文件管理的文件类型', '3', '安装包');

-- ----------------------------
-- Table structure for gf_dps_db_instance
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_db_instance`;
CREATE TABLE `gf_dps_db_instance`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '名称',
  `username` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '数据库用户',
  `password` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '密码',
  `ip` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '数据库地址',
  `port` int(10) DEFAULT NULL COMMENT '数据库端口',
  `type` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '数据库类型：01 oracle , 02 mysql',
  `database_name` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '数据库名称',
  `env` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_userid` int(10) DEFAULT NULL,
  `create_username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `version` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `jdbc_url` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39555 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_domain
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_domain`;
CREATE TABLE `gf_dps_domain`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `domain_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `weblogic_home` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `domain_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `host_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `host_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `admin_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `admin_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `admin_port` int(10) DEFAULT NULL,
  `is_admin_monitor` int(10) DEFAULT NULL,
  `promet_port` int(10) DEFAULT NULL,
  `is_managed_monitor` int(10) DEFAULT NULL,
  `admin_is_start` int(10) DEFAULT NULL,
  `admin_username` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `admin_password` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `os_username` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `os_password` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `create_userid` int(10) NOT NULL,
  `create_username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_deploy` int(10) DEFAULT NULL,
  `java_home` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `env` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `weblogic_version` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 57121 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_env
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_env`;
CREATE TABLE `gf_dps_env`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `key` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_prod` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gf_dps_env
-- ----------------------------
INSERT INTO `gf_dps_env` VALUES (1, 'dev', '开发环境', 0);
INSERT INTO `gf_dps_env` VALUES (2, 'test', '测试环境', 0);
INSERT INTO `gf_dps_env` VALUES (4, 'product', '生产环境', 1);

-- ----------------------------
-- Table structure for gf_dps_env_role
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_env_role`;
CREATE TABLE `gf_dps_env_role`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) NOT NULL,
  `env_key` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `state` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3422 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gf_dps_env_role
-- ----------------------------
INSERT INTO `gf_dps_env_role` VALUES (1, 2, 'dev', 1);
INSERT INTO `gf_dps_env_role` VALUES (2, 2, 'test', 1);
INSERT INTO `gf_dps_env_role` VALUES (3, 2, 'product', 1);
INSERT INTO `gf_dps_env_role` VALUES (4, 8, 'dev', 1);
INSERT INTO `gf_dps_env_role` VALUES (5, 8, 'test', 1);
INSERT INTO `gf_dps_env_role` VALUES (6, 8, 'product', 1);
INSERT INTO `gf_dps_env_role` VALUES (2786, 2785, 'dev', 1);
INSERT INTO `gf_dps_env_role` VALUES (3421, 3420, 'dev', 1);

-- ----------------------------
-- Table structure for gf_dps_file_info
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_file_info`;
CREATE TABLE `gf_dps_file_info`  (
  `file_id` int(10) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文件名称',
  `file_size` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文件大小',
  `create_username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `env` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '部署环境',
  `create_time` datetime DEFAULT NULL,
  `mdfive_value` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文件md5值',
  `file_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文件类型',
  PRIMARY KEY (`file_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48379 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_mail_smtp_config
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_mail_smtp_config`;
CREATE TABLE `gf_dps_mail_smtp_config`  (
  `id` bigint(10) NOT NULL,
  `mail_name` text CHARACTER SET utf8 COLLATE utf8_bin,
  `mail_type` text CHARACTER SET utf8 COLLATE utf8_bin,
  `host` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `port` int(8) DEFAULT NULL,
  `user_name` text CHARACTER SET utf8 COLLATE utf8_bin,
  `password` text CHARACTER SET utf8 COLLATE utf8_bin,
  `from_user` text CHARACTER SET utf8 COLLATE utf8_bin,
  `is_enable` varchar(4) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `env` text CHARACTER SET utf8 COLLATE utf8_bin
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_managed_servers
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_managed_servers`;
CREATE TABLE `gf_dps_managed_servers`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'managedId',
  `domain_id` int(10) NOT NULL,
  `server_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `server_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `server_port` int(10) NOT NULL,
  `is_monitor` int(10) DEFAULT NULL,
  `promet_port` int(10) DEFAULT NULL,
  `server_is_start` int(10) NOT NULL,
  `server_domain_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `deploy_file_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `lastest_file` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `deploy_info` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `is_admin` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pk_ip_port_1`(`server_port`, `server_ip`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 57125 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_menu
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_menu`;
CREATE TABLE `gf_dps_menu`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `text` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` int(10) NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `route` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `translate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `parent` int(10) NOT NULL,
  `url_matches` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `ordered` int(10) NOT NULL,
  `html` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 84 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gf_dps_menu
-- ----------------------------
INSERT INTO `gf_dps_menu` VALUES (1, '系统配置', 1, 'icon-settings text-danger', ' ', ' ', 0, ' ', 1001, NULL);
INSERT INTO `gf_dps_menu` VALUES (2, '主机资源', 1, ' fa fa-sliders text-info', NULL, NULL, 0, ' ', 1002, NULL);
INSERT INTO `gf_dps_menu` VALUES (3, '中间件管理', 1, 'fa fa-leaf text-primary', NULL, NULL, 0, ' ', 1003, NULL);
INSERT INTO `gf_dps_menu` VALUES (4, '应用管理', 1, 'icon-grid text-success ', ' ', ' ', 0, ' ', 1004, NULL);
INSERT INTO `gf_dps_menu` VALUES (5, '日志管理', 1, 'icon-calendar text-warning', ' ', ' ', 0, ' ', 1005, NULL);
INSERT INTO `gf_dps_menu` VALUES (6, '监控管理', 1, ' fa  fa-life-ring text-danger', ' ', ' ', 0, ' ', 1006, NULL);
INSERT INTO `gf_dps_menu` VALUES (7, '自动化运维', 1, 'fa fa-cubes text-info', NULL, NULL, 0, ' ', 1007, NULL);
INSERT INTO `gf_dps_menu` VALUES (8, '告警管理', 1, 'fa fa-warning text-danger', NULL, NULL, 0, NULL, 1008, NULL);
INSERT INTO `gf_dps_menu` VALUES (9, '顶部分割线', 3, ' ', ' ', ' ', 0, ' ', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (10, '测试页面1', 11, 'glyphicon glyphicon-paperclip', 'app.test01.view01', ' ', 0, '/test01/**,/monitor/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (11, '用户管理', 1, 'fa fa-users text-success', 'app.users', ' ', 1, '/user/**,/role/**,/user/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (12, '角色管理', 1, 'fa fa-user text-warning', 'app.roleManager', ' ', 1, '/roleManager/**,/env,/tomcat/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (13, '邮件服务器', 1, 'fa fa-envelope-o', 'app.mailServer', NULL, 1, '/alertRules/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (14, '组织机构', 1, 'fa fa-sitemap', 'app.organization', NULL, 1, '/organization/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (21, '主机管理', 1, ' icon-screen-desktop text-success', 'app.client', ' ', 2, '/client/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (31, 'WebLogic域定义', 1, 'fa fa-empire text-warning', 'app.domain', ' ', 3, '/env,/domain/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (32, 'Tomcat配置', 1, 'fa fa-gears  text-info', 'app.tomcat', ' ', 3, '/env,/tomcat/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (33, 'nginx管理', 1, 'fa fa-tasks text-success', 'app.nginx', ' ', 3, '/env,/domain/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (41, '应用管理', 1, 'fa fa-cloud text-primary', 'app.manage', ' ', 4, '/env,/app/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (42, '应用发布', 1, ' icon-cloud-upload text-success', 'app.deploy', ' ', 4, '/app/preview/**,/deploy/**,/logs/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (43, '版本管理', 1, 'fa fa-sort-numeric-asc text-info', 'app.vision', ' ', 4, '/app/preview/**,/deploy/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (51, '应用日志', 1, ' fa fa-file-text text-primary', 'app.logtable', ' ', 5, '/app/preview/**,/logs/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (52, '系统日志', 1, ' fa fa-file-text-o', 'app.systemLog', ' ', 5, '/serverLog/**,/logs/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (61, '监控定义', 1, 'fa fa-edit text-warning   ', 'app.monitorDefine', ' ', 6, '/env,/monitor/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (62, '主机监控', 11, 'fa fa-bar-chart text-info', 'app.monitorClient', ' ', 6, '/monitor/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (63, '应用监控', 11, 'fa fa-dashboard text-success', 'app.monitorApp', ' ', 6, '/monitor/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (64, '监控管理', 1, 'fa fa-bar-chart text-info', 'app.monitorManager', ' ', 6, '/monitor/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (65, '数据库监控', 1, 'fa fa-database', 'app.monitorDatabase', NULL, 6, '/monitor/**,/database/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (66, 'OGG监控', 1, 'fa fa-copy text-success', 'app.monitorOGG', NULL, 6, '/monitor/**,/goldengate/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (71, '脚本管理', 1, 'fa fa-file-code-o text-warning', 'app.script', NULL, 7, '/script/**,/logs/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (72, '文件管理', 1, 'fa fa-file-zip-o text-success', 'app.filelist', NULL, 7, '/script/**,/file/**,/logs/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (73, '软件安装', 1, 'fa fa-cog', 'app.softwareInstall', NULL, 7, '/script/**,/file/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (81, '告警配置', 1, 'fa fa-wrench text-success', 'app.alarmConfig', NULL, 8, '/alarm/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (82, '告警规则', 1, 'fa fa-gears text-warning', 'app.alarmRule', NULL, 8, '/alarm/**', 0, NULL);
INSERT INTO `gf_dps_menu` VALUES (83, '告警信息', 1, 'fa fa-text-width text-info', 'app.alarmManagement', NULL, 8, '/alarm/**', 0, NULL);

-- ----------------------------
-- Table structure for gf_dps_menu_role
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_menu_role`;
CREATE TABLE `gf_dps_menu_role`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) NOT NULL,
  `menu_id` int(10) NOT NULL,
  `state` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40427 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gf_dps_menu_role
-- ----------------------------
INSERT INTO `gf_dps_menu_role` VALUES (14, 3, 1, 1);
INSERT INTO `gf_dps_menu_role` VALUES (15, 3, 2, 1);
INSERT INTO `gf_dps_menu_role` VALUES (16, 3, 3, 1);
INSERT INTO `gf_dps_menu_role` VALUES (17, 3, 4, 1);
INSERT INTO `gf_dps_menu_role` VALUES (61, 4, 10, 1);
INSERT INTO `gf_dps_menu_role` VALUES (62, 4, 23, 1);
INSERT INTO `gf_dps_menu_role` VALUES (63, 4, 31, 1);
INSERT INTO `gf_dps_menu_role` VALUES (64, 4, 41, 1);
INSERT INTO `gf_dps_menu_role` VALUES (65, 4, 42, 1);
INSERT INTO `gf_dps_menu_role` VALUES (436, 5, 10, 1);
INSERT INTO `gf_dps_menu_role` VALUES (437, 5, 11, 1);
INSERT INTO `gf_dps_menu_role` VALUES (438, 5, 12, 1);
INSERT INTO `gf_dps_menu_role` VALUES (439, 5, 41, 1);
INSERT INTO `gf_dps_menu_role` VALUES (3411, 8, 11, 1);
INSERT INTO `gf_dps_menu_role` VALUES (3412, 8, 12, 1);
INSERT INTO `gf_dps_menu_role` VALUES (3413, 8, 41, 1);
INSERT INTO `gf_dps_menu_role` VALUES (3414, 8, 42, 1);
INSERT INTO `gf_dps_menu_role` VALUES (3415, 8, 1, 1);
INSERT INTO `gf_dps_menu_role` VALUES (3416, 8, 4, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21560, 2, 11, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21561, 2, 12, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21562, 2, 21, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21563, 2, 31, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21564, 2, 32, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21565, 2, 33, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21566, 2, 41, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21567, 2, 42, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21568, 2, 43, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21569, 2, 51, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21570, 2, 64, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21571, 2, 61, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21572, 2, 71, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21573, 2, 72, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21574, 2, 81, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21575, 2, 82, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21576, 2, 83, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21577, 2, 1, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21578, 2, 2, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21579, 2, 3, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21580, 2, 4, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21581, 2, 5, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21582, 2, 6, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21583, 2, 7, 1);
INSERT INTO `gf_dps_menu_role` VALUES (21584, 2, 8, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40403, 2, 11, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40404, 2, 12, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40405, 2, 14, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40406, 2, 21, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40407, 2, 31, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40408, 2, 32, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40409, 2, 33, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40410, 2, 41, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40411, 2, 42, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40412, 2, 43, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40413, 2, 51, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40414, 2, 64, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40415, 2, 65, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40416, 2, 66, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40417, 2, 61, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40418, 2, 71, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40419, 2, 72, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40420, 2, 1, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40421, 2, 2, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40422, 2, 3, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40423, 2, 4, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40424, 2, 5, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40425, 2, 6, 1);
INSERT INTO `gf_dps_menu_role` VALUES (40426, 2, 7, 1);

-- ----------------------------
-- Table structure for gf_dps_monitor_server
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_monitor_server`;
CREATE TABLE `gf_dps_monitor_server`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '监控服务器名称',
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '监控服务器IP',
  `port` int(10) NOT NULL COMMENT '监控服务器端口',
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '在线状态',
  `create_time` datetime NOT NULL,
  `env` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '只有管理员有资格管理此页面，所以不需要create_userid字段',
  `type_name` varchar(125) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类型名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3485 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gf_dps_monitor_server
-- ----------------------------
INSERT INTO `gf_dps_monitor_server` VALUES (1, 'prometheus', '192.168.174.6', 9090, 'UP', '2019-01-15 00:40:42', 'dev', 'prometheus数据源', 'prometheus');
INSERT INTO `gf_dps_monitor_server` VALUES (3484, 'grafana', '192.168.174.6', 3000, 'UP', '2019-05-21 06:05:38', 'dev', 'grafana', 'grafana');

-- ----------------------------
-- Table structure for gf_dps_nginx_server
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_nginx_server`;
CREATE TABLE `gf_dps_nginx_server`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `port` int(10) DEFAULT NULL,
  `host_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `install_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `is_monitor` int(10) DEFAULT NULL,
  `monitor_port` int(10) DEFAULT NULL,
  `create_username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_userid` int(10) DEFAULT NULL,
  `env` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `client_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37593 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_ogg_instance
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_ogg_instance`;
CREATE TABLE `gf_dps_ogg_instance`  (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '名称',
  `ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '服务器地址',
  `port` int(8) DEFAULT NULL COMMENT 'SSH端口',
  `user_name` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '登录用户名',
  `password` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '登录密码',
  `ogg_path` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'ggsci所在目录路径',
  `ogg_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类型：01源端服务器，02目标端服务器',
  `interval` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '监控周期 ',
  `env` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '部署环境',
  `create_userid` int(10) DEFAULT NULL,
  `create_username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `recever_mails` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '接收者邮箱信息,逗号隔开：xx1@163.com,xx2@163.com',
  `send_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `mail_id` int(8) DEFAULT NULL,
  `message_id` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32466 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'OGG目标端和源端信息表' ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_ogg_instance_program
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_ogg_instance_program`;
CREATE TABLE `gf_dps_ogg_instance_program`  (
  `id` int(8) NOT NULL,
  `program` text CHARACTER SET utf8 COLLATE utf8_bin,
  `status` text CHARACTER SET utf8 COLLATE utf8_bin,
  `group_name` text CHARACTER SET utf8 COLLATE utf8_bin,
  `lag_at_chkpt` text CHARACTER SET utf8 COLLATE utf8_bin,
  `time_since_chkpt` text CHARACTER SET utf8 COLLATE utf8_bin,
  `instance_id` int(8) DEFAULT NULL,
  `check_count` int(8) DEFAULT NULL,
  `check_time` datetime DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_operate_log
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_operate_log`;
CREATE TABLE `gf_dps_operate_log`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` int(10) NOT NULL,
  `service_id` int(10) NOT NULL,
  `client_ip` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `service_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `log_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_userid` int(10) NOT NULL,
  `create_username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime NOT NULL,
  `env` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 62085 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;



-- ----------------------------
-- Table structure for gf_dps_org
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_org`;
CREATE TABLE `gf_dps_org`  (
  `org_no` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `org_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `parent_org_no` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `org_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `org_sort` bigint(10) DEFAULT NULL,
  `org_tree` text CHARACTER SET utf8 COLLATE utf8_bin,
  `org_full_name` text CHARACTER SET utf8 COLLATE utf8_bin,
  `org_user` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `org_phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `org_address` text CHARACTER SET utf8 COLLATE utf8_bin,
  `org_zip_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `org_email` text CHARACTER SET utf8 COLLATE utf8_bin,
  `org_status` varchar(8) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `create_user` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `remarks` text CHARACTER SET utf8 COLLATE utf8_bin,
  `corp_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `corp_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `extend1` text CHARACTER SET utf8 COLLATE utf8_bin,
  `extend2` text CHARACTER SET utf8 COLLATE utf8_bin,
  `extend3` text CHARACTER SET utf8 COLLATE utf8_bin,
  `extend4` text CHARACTER SET utf8 COLLATE utf8_bin,
  `extend5` text CHARACTER SET utf8 COLLATE utf8_bin,
  `env` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gf_dps_org
-- ----------------------------
INSERT INTO `gf_dps_org` VALUES ('1', '中国', NULL, '00', 1, NULL, '中国', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-02-24 15:37:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `gf_dps_org` VALUES ('111', '沈阳市', '11', '02', 1, NULL, '沈阳市', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-02-25 10:03:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `gf_dps_org` VALUES ('1111', '浑南区', '111', '03', 1, NULL, '浑南区', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-02-25 10:03:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `gf_dps_org` VALUES ('11', '辽宁省', '1', '01', 1, NULL, '辽宁省', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-02-25 10:02:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for gf_dps_performance_monitor
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_performance_monitor`;
CREATE TABLE `gf_dps_performance_monitor`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `app_id` int(10) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip_port` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime NOT NULL,
  `env` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '只有管理员有资格管理此页面，所以不需要create_userid字段',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_url`(`url`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gf_dps_performance_monitor
-- ----------------------------
INSERT INTO `gf_dps_performance_monitor` VALUES (3, NULL, '主机监控', '192.168.174.6:3000', 'http://192.168.174.6:3000/d/000000001/morphling-system-client?orgId=2', 'client', '2019-01-15 00:16:39', 'dev', '各节点服务器硬件监控');
INSERT INTO `gf_dps_performance_monitor` VALUES (4, NULL, 'tomcat-jmx监控', '192.168.174.6:3000', 'http://192.168.174.6:3000/d/nshAi8pmz/morphling-jmx-tomcat?orgId=2', 'tomcat-jmx', '2019-01-15 17:49:27', 'dev', 'tomcat监控');
INSERT INTO `gf_dps_performance_monitor` VALUES (5, NULL, 'weblogic-jmx监控', '192.168.174.6:3000', 'http://192.168.174.6:3000/d/ntsj9SQiz/morphling-jmx-weblogic?orgId=2', 'weblogic-jmx', '2019-01-15 17:50:13', 'dev', 'weblogic监控');
INSERT INTO `gf_dps_performance_monitor` VALUES (6, NULL, 'springboot监控', '192.168.174.6:3000', 'http://192.168.174.6:3000/d/_Fd2vHQik/morphling-jmx-springboot?orgId=2', 'springboot-jmx', '2019-01-15 17:52:02', 'dev', 'springboot监控');

-- ----------------------------
-- Table structure for gf_dps_role
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_role`;
CREATE TABLE `gf_dps_role`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gf_dps_role
-- ----------------------------
INSERT INTO `gf_dps_role` VALUES (1, 'SUPERADMIN', '超级管理员');
INSERT INTO `gf_dps_role` VALUES (2, 'deploy', '应用部署角色');
INSERT INTO `gf_dps_role` VALUES (8, 'test', '测试');

-- ----------------------------
-- Table structure for gf_dps_script_info
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_script_info`;
CREATE TABLE `gf_dps_script_info`  (
  `script_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `script_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '脚本名称',
  `script_category` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '脚本类别:01 运维脚本 02 巡检脚本',
  `script_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '脚本类型: 01 shell、 02python 、03 perl',
  `time_out` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '超时时间：单位秒',
  `script_param` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '脚本参数 空格隔开：p1 p2',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `script_desc` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '脚本说明',
  `script_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '脚本内容',
  `save_path` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '脚本保存地址',
  `client_ids` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '脚本同步主机的id集合',
  `create_username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '脚本的创建用户',
  `env` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '脚本的创建环境',
  PRIMARY KEY (`script_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40285 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '脚本信息表' ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_server_info
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_server_info`;
CREATE TABLE `gf_dps_server_info`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `port` int(10) DEFAULT NULL,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upload_path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `workspace_home` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `app_path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `update_path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `java_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `java_path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `java_folder` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `log_path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `deploy_path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `backup_path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gf_dps_server_info
-- ----------------------------
INSERT INTO `gf_dps_server_info` VALUES (1, '192.168.174.6', 33330, 'root', '123$%^', ' ', ' ', '/workspace/app', '/workspace/update', 'jdk-8u191-linux-x64.tar.gz', '/app/data/morphling-server/jdk-8u191-linux-x64.tar.gz', 'jdk1.8.0_191', '/workspace/log', '/workspace/deploy', '/workspace/backup');

-- ----------------------------
-- Table structure for gf_dps_shell_log
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_shell_log`;
CREATE TABLE `gf_dps_shell_log`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT,
  `status` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 868998639475888129 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_tomcat_middleware
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_tomcat_middleware`;
CREATE TABLE `gf_dps_tomcat_middleware`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `port` int(10) DEFAULT NULL,
  `promet_port` int(10) DEFAULT NULL,
  `host_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `install_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `deploy_file_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `lastest_file` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `deploy_info` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `is_monitor` int(10) DEFAULT NULL,
  `create_username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_userid` int(10) DEFAULT NULL,
  `env` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61735 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_user_app
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_user_app`;
CREATE TABLE `gf_dps_user_app`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `app_id` int(10) NOT NULL,
  `create_time` datetime NOT NULL,
  `state` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25290 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for gf_dps_user_role
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_user_role`;
CREATE TABLE `gf_dps_user_role`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  `create_time` datetime NOT NULL,
  `state` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37727 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gf_dps_user_role
-- ----------------------------
INSERT INTO `gf_dps_user_role` VALUES (1, 1, 1, '2018-07-26 10:52:33', 1);
INSERT INTO `gf_dps_user_role` VALUES (6, 27, 2, '2018-12-17 15:15:57', 0);
INSERT INTO `gf_dps_user_role` VALUES (1371, 1369, 8, '2019-03-07 08:50:43', 1);
INSERT INTO `gf_dps_user_role` VALUES (3410, 27, 8, '2019-05-15 15:25:14', 0);
INSERT INTO `gf_dps_user_role` VALUES (3435, 27, 8, '2019-05-15 17:50:06', 0);
INSERT INTO `gf_dps_user_role` VALUES (3470, 27, 2, '2019-05-16 09:23:57', 1);
INSERT INTO `gf_dps_user_role` VALUES (37726, 27, 2, '2019-10-24 14:39:48', 1);

-- ----------------------------
-- Table structure for gf_dps_users
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_users`;
CREATE TABLE `gf_dps_users`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `head_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `last_login_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `state` int(10) NOT NULL,
  `is_admin` int(10) NOT NULL,
  `org_no` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username_uk`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gf_dps_users
-- ----------------------------
INSERT INTO `gf_dps_users` VALUES (1, '管理员', 'admin', '15652222294', '55375829@qq.com', '$2a$10$yQdf.cLpD1XHK7KOfjH0rONz.M23gXVEwjldl.kptmZHiuJfpJcdK', 'https://www.gravatar.com/avatar/e74dd6c7719cb166dd3489fccb61ec72?s=800&d=identicon', '2020-04-10 14:17:41', '0:0:0:0:0:0:0:1', '2017-11-23 17:19:16', 1, 1, NULL);
INSERT INTO `gf_dps_users` VALUES (27, '东软应用部署', 'neusoft', '24254363463', '424242', '$2a$10$1zl2iodsF/UD6ftgwQrEtOtqcAzbW2.7wIcxRyuYEULj9ursaxrAe', ' ', '2019-08-30 08:51:51', '192.168.130.98', '2018-12-17 15:15:51', 1, 0, NULL);

SET FOREIGN_KEY_CHECKS = 1;

-- version V2.1.1 add

INSERT INTO `gf_dps_menu` VALUES (67, 'Redis监控', 1, 'iconfont icon-redis', 'app.monitorRedis', null, 6, '/monitor/**', 0, null);
-- ----------------------------
-- Table structure for gf_dps_redis_cluster
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_redis_cluster`;
CREATE TABLE `gf_dps_redis_cluster`  (
  `cluster_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '集群ID',
  `group_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL COMMENT '用户id',
  `cluster_token` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `cluster_name` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'redis集群名称',
  `nodes` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'redis节点: 192.168.174.44:26379,192.168.174.44:26379,192.168.174.44:26379',
  `redis_mode` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'redis模式:  standalone, cluster,sentinel',
  `os` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '操作系统版本',
  `redis_version` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'redis版本',
  `image` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `initialized` int(2) DEFAULT NULL,
  `total_used_memory` int(16) DEFAULT NULL COMMENT '内存占用大小',
  `total_keys` int(16) DEFAULT NULL COMMENT 'key的数量',
  `total_expires` int(16) DEFAULT NULL,
  `db_size` int(16) DEFAULT NULL,
  `cluster_state` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '集群状态 HEALTH',
  `cluster_slots_assigned` int(16) DEFAULT NULL COMMENT '已分配的哈希槽',
  `cluster_slots_ok` int(16) DEFAULT NULL COMMENT '哈希槽状态不是FAIL 和 PFAIL 的数量',
  `cluster_slots_pfail` int(16) DEFAULT NULL,
  `cluster_slots_fail` int(16) DEFAULT NULL,
  `cluster_known_nodes` int(10) DEFAULT NULL,
  `cluster_size` int(10) DEFAULT NULL COMMENT 'master 数量',
  `sentinel_ok` int(10) DEFAULT NULL,
  `sentinel_masters` int(10) DEFAULT NULL,
  `master_ok` int(10) DEFAULT NULL,
  `redis_password` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'redis密码',
  `rule_ids` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `channel_ids` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `cluster_alert` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `installation_environment` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `installation_type` int(10) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `env` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `user_name` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`cluster_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 132559 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'redis 管理' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gf_dps_redis_node
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_redis_node`;
CREATE TABLE `gf_dps_redis_node`  (
  `redis_node_id` int(10) NOT NULL AUTO_INCREMENT,
  `group_id` int(10) DEFAULT NULL,
  `cluster_id` int(10) DEFAULT NULL COMMENT '集群id',
  `node_id` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '节点id',
  `master_id` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'masterid',
  `host` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'ip',
  `port` int(10) DEFAULT NULL COMMENT '端口',
  `node_role` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '角色: master,slave',
  `flags` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' master,slave',
  `link_state` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '连接状态 connected，unconnected',
  `in_cluster` int(2) DEFAULT NULL COMMENT '是否集群1,0',
  `run_status` int(2) DEFAULT NULL COMMENT '运行状态1,0',
  `slot_range` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '插槽范围0-5460',
  `slot_number` int(10) DEFAULT NULL COMMENT '插槽数量',
  `container_id` int(10) DEFAULT NULL,
  `container_name` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `insert_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`redis_node_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 132565 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'redis节点管理' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gf_dps_redis_node_info
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_redis_node_info`;
CREATE TABLE `gf_dps_redis_node_info`  (
  `info_id` int(10) NOT NULL AUTO_INCREMENT,
  `cluster_id` int(10) DEFAULT NULL,
  `node` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `role` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `time_type` int(10) DEFAULT NULL,
  `last_time` bigint(20) DEFAULT NULL,
  `response_time` bigint(20) DEFAULT NULL,
  `connected_clients` bigint(20) DEFAULT NULL COMMENT '连接客户端数',
  `client_longest_output_list` bigint(20) DEFAULT NULL,
  `client_biggest_input_buf` bigint(20) DEFAULT NULL,
  `blocked_clients` bigint(20) DEFAULT NULL COMMENT '被阻塞的连接个数',
  `used_memory` bigint(20) DEFAULT NULL COMMENT '已使用的内存 MB',
  `used_memory_rss` bigint(20) DEFAULT NULL COMMENT '物理内存的大小 MB',
  `used_memory_overhead` bigint(20) DEFAULT NULL COMMENT '内存开销 MB',
  `used_memory_dataset` bigint(20) DEFAULT NULL COMMENT '数据占用的内存大小 MB',
  `used_memory_dataset_perc` bigint(20) DEFAULT NULL,
  `mem_fragmentation_ratio` bigint(20) DEFAULT NULL COMMENT '内存碎片率',
  `total_connections_received` bigint(20) DEFAULT NULL,
  `connections_received` bigint(20) DEFAULT NULL COMMENT '新创建连接个数',
  `rejected_connections` bigint(20) DEFAULT NULL COMMENT ' 拒绝的连接个数',
  `total_commands_processed` bigint(20) DEFAULT NULL,
  `commands_processed` bigint(20) DEFAULT NULL COMMENT '处理的命令数',
  `instantaneous_ops_per_sec` bigint(20) DEFAULT NULL COMMENT '每秒执行的命令数',
  `total_net_input_bytes` bigint(20) DEFAULT NULL,
  `net_input_bytes` bigint(20) DEFAULT NULL,
  `total_net_output_bytes` bigint(20) DEFAULT NULL,
  `net_output_bytes` bigint(20) DEFAULT NULL,
  `sync_full` bigint(20) DEFAULT NULL COMMENT '主从完全同步成功次数',
  `sync_partial_ok` bigint(20) DEFAULT NULL COMMENT '主从部分同步成功次数',
  `sync_partial_err` bigint(20) DEFAULT NULL COMMENT '主从部分同步失败次数',
  `keyspace_misses` bigint(20) DEFAULT NULL,
  `keyspace_hits` bigint(20) DEFAULT NULL,
  `keyspace_hits_ratio` bigint(20) DEFAULT NULL COMMENT '请求键的命中率 %',
  `used_cpu_sys` bigint(20) DEFAULT NULL,
  `cpu_sys` bigint(20) DEFAULT NULL,
  `used_cpu_user` bigint(20) DEFAULT NULL,
  `cpu_user` bigint(20) DEFAULT NULL,
  `redis_keys` bigint(20) DEFAULT NULL COMMENT 'key总数',
  `expires` bigint(20) DEFAULT NULL COMMENT '过期key数',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`info_id`) USING BTREE,
  INDEX `redis_node_info_cluster`(`cluster_id`) USING BTREE,
  INDEX `redis_node_info_query`(`cluster_id`, `node`(255), `time_type`, `update_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 152 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'redis节点指标信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gf_dps_redis_sentinel_masters
-- ----------------------------
DROP TABLE IF EXISTS `gf_dps_redis_sentinel_masters`;
CREATE TABLE `gf_dps_redis_sentinel_masters`  (
  `sentinel_master_id` int(10) NOT NULL AUTO_INCREMENT,
  `cluster_id` int(10) DEFAULT NULL,
  `group_id` int(10) DEFAULT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `host` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `port` int(10) DEFAULT NULL,
  `flags` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `last_master_node` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `status` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `link_pending_commands` int(32) DEFAULT NULL,
  `link_refcount` int(32) DEFAULT NULL,
  `last_ping_sent` int(32) DEFAULT NULL,
  `last_ok_ping_reply` int(32) DEFAULT NULL,
  `last_ping_reply` int(32) DEFAULT NULL,
  `s_down_time` int(32) DEFAULT NULL,
  `o_down_time` int(32) DEFAULT NULL,
  `down_after_milliseconds` int(32) DEFAULT NULL,
  `info_refresh` int(32) DEFAULT NULL,
  `role_reported` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `role_reported_time` int(32) DEFAULT NULL,
  `config_epoch` int(32) DEFAULT NULL,
  `num_slaves` int(10) DEFAULT NULL,
  `sentinels` int(10) DEFAULT NULL,
  `quorum` int(10) DEFAULT NULL,
  `failover_timeout` int(32) DEFAULT NULL,
  `parallel_syncs` int(10) DEFAULT NULL,
  `auth_pass` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`sentinel_master_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
