create or replace view gf_dps_table_ip_port_list as
select rownum id , table_name, ip, port
  from (select ams.id as id ,'gf_dps_alert_manager_server' as table_name,
               ams.ip as ip,
               ams.port as port
          from gf_dps_alert_manager_server ams
        union all
        select adc.id as id , 'gf_dps_app_deploy_custom' as table_name,
               adc.ip as ip,
               CAST(adc.port AS int) as port
          from gf_dps_app_deploy_custom adc
        union all
        select asb.id as id ,'gf_dps_app_springboot' as table_name,
               asb.springboot_host as ip,
               asb.springboot_port as port
          from gf_dps_app_springboot asb where asb.state = '1'
        union all
        select asb.id as id, 'gf_dps_app_springboot' as table_name,
               asb.springboot_host as ip,
               asb.promet_port as port
          from gf_dps_app_springboot asb  where asb.state = '1'
        union all
        select c.id as id, 'gf_dps_client' as table_name, c.HOST_ADDRESS as ip, c.port as port
          from gf_dps_client c
        union all
        select c.id as id , 'gf_dps_client' as table_name,
               c.HOST_ADDRESS as ip,
               c.performance_port as port
          from gf_dps_client c
        union all
        select d.id as id ,'gf_dps_domain' as table_name,
               d.admin_ip as ip,
               d.admin_port as port
          from gf_dps_domain d
        union all
        select d.id as id, 'gf_dps_domain' as table_name,
               d.admin_ip as ip,
               d.promet_port as port
          from gf_dps_domain d
        union all
        select mas.id as id , 'gf_dps_managed_servers' as table_name,
               mas.server_ip as ip,
               mas.server_port as port
          from gf_dps_managed_servers mas
        union all
        select mas.id as id, 'gf_dps_managed_servers' as table_name,
               mas.server_ip as ip,
               mas.promet_port as port
          from gf_dps_managed_servers mas
        union all
        select  mos.id as id ,'gf_dps_monitor_server' as table_name,
               mos.ip as ip,
               mos.port as port
          from gf_dps_monitor_server mos
        union all
        select  m.id as id ,'gf_dps_tomcat_middleware' as table_name, m.ip as ip, m.port as port
          from gf_dps_tomcat_middleware m
        union all
        select  m.id as id ,'gf_dps_tomcat_middleware' as table_name,
               m.ip as ip,
               m.promet_port as port
          from gf_dps_tomcat_middleware m) a
          where a.port is not null and  a.port <>0