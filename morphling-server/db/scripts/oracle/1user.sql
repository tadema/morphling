-- Create the user 
drop user morphling cascade;
create user morphling
  default tablespace USERS
  temporary tablespace TEMP
  identified by morphling
  profile DEFAULT;
-- Grant/Revoke role privileges 
grant connect to morphling;
--grant dba to morphling;
grant resource to morphling with admin option;
-- Grant/Revoke system privileges 
grant alter any table to morphling;
grant alter tablespace to morphling;
grant create any job to morphling;
grant create any procedure to morphling;
grant create any sequence to morphling;
grant create any synonym to morphling;
grant create any table to morphling;
grant create any trigger to morphling;
grant create any view to morphling;
grant create database link to morphling;
grant create public synonym to morphling;
grant create sequence to morphling;
grant create session to morphling;
grant create synonym to morphling;
grant create table to morphling;
grant create view to morphling;
grant debug any procedure to morphling;
grant debug connect session to morphling with admin option;
grant drop any sequence to morphling;
grant execute any procedure to morphling;
grant insert any table to morphling;
grant select any dictionary to morphling with admin option;
grant select any sequence to morphling;
grant select any table to morphling;
grant unlimited tablespace to morphling with admin option;
grant update any table to morphling;
