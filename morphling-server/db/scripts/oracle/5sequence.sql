-- Create sequence
create sequence HIBERNATE_SEQUENCE
minvalue 1
maxvalue 99999999999
start with 32521
increment by 1
cache 20;
